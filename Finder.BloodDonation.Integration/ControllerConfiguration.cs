﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Finder.BloodDonation.Integration
{
	public class ControllerConfiguration
	{
		public int UpdatePeriod { get; private set; }
		public string ConnectionStringMain { get; private set; }
		public string ConnectionStringLocal { get; private set; }
		public int ClientId { get; private set; }

		public ControllerConfiguration()
		{
			Load();
		}

		private void Load()
		{
			UpdatePeriod = Convert.ToInt32(ConfigurationManager.AppSettings.Get("UpdatePeriod"));
			ConnectionStringMain = ConfigurationManager.AppSettings.Get("DefaultConnectionStringMain");
			ConnectionStringLocal = ConfigurationManager.AppSettings.Get("DefaultConnectionStringLocal");
			ClientId = Int32.Parse(ConfigurationManager.AppSettings.Get("ClientId"));
		}
	}
}
