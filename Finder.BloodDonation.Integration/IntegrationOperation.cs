﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Integration
{
	public class IntegrationOperation
	{
		public long OperationId { get; set; }
		public long LocalOperationId { get; set; }
		public DateTime OperationDate { get; set; }
		public int? SourceStoreId { get; set; }
		public int? StatusId { get; set; }
		public int? OperationTypeId { get; set; }
		public bool Synchronized { get; set; }
		public DateTime? SynchronizationDate { get; set; }
		public string BloodDonationNumber { get; set; }
		public string BloodGroup { get; set; }
		public string BloodAttributes { get; set; }
		public string IbsId { get; set; }
		public int SourceId { get; set; }
		public int? TargetStoreId { get; set; }
		public int? UserId { get; set; }
		public long? Uid { get; set; }
		public DateTime? BloodExpirationDate { get; set; }
		public int? BloodCapacity { get; set; }
		public string BloodPhenotype { get; set; }
		public DateTime? BloodCollectionDate { get; set; }
		public string Fractionator { get; set; }
		public int ClientId { get; set; }
	}
}
