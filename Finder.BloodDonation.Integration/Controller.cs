﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Data;
using System.Configuration;
using System.Threading;
using log4net;

namespace Finder.BloodDonation.Integration
{
	public class Controller : IDisposable
	{
		private static readonly ILog ActivityLogger = LogManager.GetLogger("ActivityLogger");
		private const int NotApplicable = -1;

		private const long Existing = -1;
		private const int OperationStoreChange = 1;
		private const int OperationStatusChange = 3;

		private const int SourceM2M = 1;
		private const int SourceIbs = 2;
		private const int OperationChangeStore = 1;

		private Timer _updateTimer;
		private ControllerConfiguration _config;

		public Controller(ControllerConfiguration config)
		{
			_config = config;
		}

		public void Start()
		{
			_updateTimer = new Timer(Synchronize, null, 0, _config.UpdatePeriod);
		}

		public void Stop()
		{
			_updateTimer.Dispose();
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (_updateTimer != null)
				{
					_updateTimer.Dispose();
				}
			}
		}

		private void Synchronize(object state)
		{
			Console.WriteLine("{0}, starting sync", DateTime.Now);

			try
			{
				// -- local phase

				var operations = GetLocalChanges();
				ActivityLogger.Info(String.Format("{0} Local phase, loaded {1} local changes", DateTime.Now, operations.Count));

				var added = AddLocalChangesToIntegration(operations);
				ActivityLogger.Info(String.Format("{0} Local phase, added {1} local changes to integration db", DateTime.Now, added));

				var commited = CommitLocalChanges(operations.Select(o => o.LocalOperationId));
				ActivityLogger.Info(String.Format("{0} Local phase, commited {1} local changes", DateTime.Now, commited));

				// -- integration phase

				// load changes from integration db
				operations = GetIntegrationChanges();
				ActivityLogger.Info(String.Format("{0} Integration phase, loaded {1} integration changes", DateTime.Now, operations.Count));

				// check if prep exists and add if not

				//// add integration changes to the system
				added = AddIntegrationChangesToLocalHistory(operations);
				ActivityLogger.Info(String.Format("{0} Integration phase, added {1} integration changes to local buffer", DateTime.Now, added));

				//// commit integration changes
				commited = CommitIntegrationChanges(operations.Select(o => o.OperationId));
				ActivityLogger.Info(String.Format("{0} Integration phase, commited {1} integration changes", DateTime.Now, commited));
			}
			catch (Exception e)
			{
				ActivityLogger.Info("Error synchronizing in integration", e);
			}
		}

		private IList<IntegrationOperation> GetIntegrationChanges()
		{
			var db = Database.OpenNamedConnection(_config.ConnectionStringMain);
			var integrationChanges = db.LoadChangesForM2M(_config.ClientId);
			var list = new List<IntegrationOperation>();

			foreach (var item in integrationChanges)
			{
				list.Add(ConvertToIntegrationOperation(item, false));
			}

			return list;
		}

		private IList<IntegrationOperation> GetLocalChanges()
		{
			var db = Database.OpenNamedConnection(_config.ConnectionStringLocal);
			var localChanges = db.Integration_LoadLocalChanges(_config.ClientId);
			var list = new List<IntegrationOperation>();

			foreach (var item in localChanges)
			{
				list.Add(ConvertToIntegrationOperation(item, true));
			}

			return list;
		}

		private int AddIntegrationChangesToLocal(IList<IntegrationOperation> operations)
		{
			var added = 0;

			foreach (var o in operations)
			{
				var db = Database.OpenNamedConnection(_config.ConnectionStringLocal);
				var result = db.Integration_AddIntegrationChanges(
					o.OperationDate,
					o.OperationTypeId,
					o.TargetStoreId,
					o.StatusId,
					o.SourceId,
					o.BloodDonationNumber,
					o.BloodGroup,
					o.BloodAttributes,
					o.IbsId,
					o.UserId,
					o.Uid);

				added += result.First().Added;
			}

			return added;
		}

		private int AddIntegrationChangesToLocalHistory(IList<IntegrationOperation> operations)
		{
			var added = 0;
			var statusChanged = 0;

			var db = Database.OpenNamedConnection(_config.ConnectionStringLocal);
			foreach (var o in operations)
			{
				var prepId = CheckAndAddPreps(o);

				if (prepId == Existing)
				{
					switch (o.OperationTypeId)
					{
						case OperationStoreChange:
							//var result = 
							db.Integration_MovePrepByIntegration(
								o.UserId,
								o.BloodDonationNumber,
								o.BloodGroup,
								o.BloodAttributes,
								o.TargetStoreId,
                                o.OperationDate);
							break;
						case OperationStatusChange:
							//var result = 
							db.Integration_ChangeStatus(
								o.UserId,
								o.BloodDonationNumber,
								o.BloodGroup,
								o.BloodAttributes,
								o.StatusId);

							statusChanged++;
							break;
					}
				}

				added++;
			}

			ActivityLogger.Info(String.Format("{0} Integration phase, changed status: {1}", DateTime.Now, statusChanged));

			return added;
		}

		private long CheckAndAddPreps(IntegrationOperation o)
		{
			var db = Database.OpenNamedConnection(_config.ConnectionStringLocal);

			if (o.OperationTypeId == OperationStoreChange)
			{
				var result = db.Integration_CreatePrep(
					o.Uid,
					o.BloodDonationNumber,
					o.BloodGroup,
					o.BloodAttributes,
					o.TargetStoreId,
					o.StatusId,
					o.BloodCollectionDate, // date added
					o.BloodExpirationDate,
					o.BloodCapacity,
					o.BloodPhenotype,
					o.ClientId,
					o.Fractionator,
                    o.OperationDate,
                    o.UserId
					);

				var prepId = result.First().PrepId;

                /*
				if (prepId != Existing)
				{
					db.Integration_CreateFirstHistoryEntry(
						o.UserId,
						prepId,
						o.TargetStoreId,
						o.OperationDate);
				}
                */
				return Convert.ToInt64(prepId);
			}

			return Existing;
		}

		private int AddLocalChangesToIntegration(IList<IntegrationOperation> operations)
		{
			var added = 0;
			foreach (var o in operations)
			{
				o.Synchronized = true;

				var localId = o.LocalOperationId;

				var db = Database.OpenNamedConnection(_config.ConnectionStringMain);

				var result = db.AddLocalChange(
					localId,
					o.OperationDate,
					OperationChangeStore,
					o.SourceStoreId,
					o.StatusId,
					o.BloodDonationNumber,
					o.BloodGroup,
					o.BloodAttributes.Trim(), // + "V00",
					o.TargetStoreId,
					o.UserId,
					o.Uid,
					o.ClientId,
					o.Fractionator);

				added += result.First().Added;
			}

			return added;
		}

		private int CommitIntegrationChanges(IEnumerable<long> ids)
		{
			var commited = 0;

			var idsText = String.Join(",", ids);

			var db = Database.OpenNamedConnection(_config.ConnectionStringMain);
			var results = db.CommitIntegrationChanges(idsText);

			commited = results.First().Updated;

			return commited;
		}

		private int CommitLocalChanges(IEnumerable<long> ids)
		{
			var commited = 0;

			var idsText = String.Join(",", ids);

			var db = Database.OpenNamedConnection(_config.ConnectionStringLocal);
			var results = db.Integration_CommitLocalChanges(idsText);

			commited = results.First().Updated;

			return commited;
		}

		private static IntegrationOperation ConvertToIntegrationOperation(dynamic record, bool local)
		{

			var operation = new IntegrationOperation();
			//{
			//	OperationId = (local) ? NotApplicable : record.Id,
			//	LocalOperationId = (local) ? record.Id : NotApplicable,
			//	OperationDate = record.OperationDate,
			//	OperationTypeId = record.OperationTypeId,
			//	//SourceStoreId = record.SourceStoreId,
			//	StatusId = record.StatusId,
			//	Synchronized = record.Synchronized,
			//	SynchronizationDate = record.SynchronizationDate,
			//	BloodDonationNumber = record.BloodDonationNumber,
			//	BloodGroup = record.BloodGroup,
			//	BloodAttributes = record.BloodAttributes,
			//	IbsId = record.IbsId,
			//	SourceId = local ? SourceM2M : SourceIbs,
			//	TargetStoreId = record.TargetStoreId,
			//	UserId = record.UserId,
			//	Uid = (local) ? record.Uid : unchecked((long)UInt64.Parse(record.Uid.ToString())),
			//	BloodExpirationDate = (local) ? null : record.BloodExpirationDate,
			//	BloodCapacity = (local) ? null : record.BloodCapacity,
			//	BloodPhenotype = (local) ? String.Empty : record.BloodPhenotype
			//};

			operation.OperationId = (local) ? NotApplicable : record.Id;
			operation.LocalOperationId = (local) ? record.Id : NotApplicable;
			operation.OperationDate = record.OperationDate;
			operation.OperationTypeId = record.OperationTypeId;
			operation.StatusId = record.StatusId;
			operation.Synchronized = record.Synchronized;
			operation.SynchronizationDate = record.SynchronizationDate;
			operation.BloodDonationNumber = record.BloodDonationNumber;
			operation.BloodGroup = record.BloodGroup;
			operation.BloodAttributes = record.BloodAttributes;
			operation.IbsId = record.IbsId;
			operation.SourceId = local ? SourceM2M : SourceIbs;
			operation.TargetStoreId = record.TargetStoreId;
			operation.UserId = record.UserId;
			
			if (record.Uid != null)
				operation.Uid = (local) ? record.Uid : unchecked((long)UInt64.Parse(record.Uid.ToString()));
			
			operation.BloodExpirationDate = (local) ? null : record.BloodExpirationDate;
			operation.BloodCapacity = (local) ? null : record.BloodCapacity;
			operation.BloodPhenotype = (local) ? String.Empty : record.BloodPhenotype;
			operation.BloodCollectionDate = (local) ? null : record.BloodCollectionDate;

			if (record.GetType().GetProperty("Fractionator") != null)
				operation.Fractionator = record.Fractionator;
			
			if (record.GetType().GetProperty("_Fractionator") != null)
				operation.Fractionator = record._Fractionator;

			operation.ClientId = record.ClientId;

            ActivityLogger.Info(String.Format("{0} ConvertToIntegrationOperation,  frakcjonator= {1}", DateTime.Now, operation.Fractionator));

			return operation;
		}
	}
}
