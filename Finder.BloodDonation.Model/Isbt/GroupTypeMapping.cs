﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Model.Isbt
{
	public class GroupTypeMapping
	{
		public static int GetByGroup(string group)
		{
			var mapping = new Dictionary<int, int>();
			mapping.Add(6, 12); // a-
			mapping.Add(62, 13); // a+
			mapping.Add(17, 14); // b-
			mapping.Add(73, 15); // b+
			mapping.Add(28, 16); // ab-
			mapping.Add(84, 17); // ab+
			mapping.Add(95, 18); // 0-
			mapping.Add(51, 19); // 0+

			var key = Int32.Parse(group.Substring(0, 2));

			return mapping[key];
		}
	}
}
