﻿using System;
using System.ComponentModel;
using System.Net;

namespace Finder.BloodDonation.Model.Enum
{
    public enum EventSeverity
    {
        [Description("Zdarzenie")]
        Event = 0,
        [Description("Ostrzeżenie")]
        Warning = 1,
        [Description("Alarm")]
        Alarm = 2
    }
}
