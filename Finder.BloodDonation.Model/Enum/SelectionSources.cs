﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Model.Enum
{
    public static class SelectionSources
    {
            public const int MainTree = 0;
            public const int Favorites = 1;
            public const int Hubs = 2;
    }
}
