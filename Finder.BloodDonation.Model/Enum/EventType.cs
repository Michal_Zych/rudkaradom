﻿using System;
using System.ComponentModel;
using System.Net;


namespace Finder.BloodDonation.Model.Enum
{
    public enum EventType
    {
        [Description("Skasowanie")]
        Delete = 0,
        [Description("Utworzenie")]
        Create = 1,
        [Description("Edycja")]
        Update = 2,
        [Description("Przyjęcie do szpitala")]
        RegisterInHostpital = 3,
        [Description("Wypisanie ze szpitala")]
        RegisterOutHostpital = 4,
        [Description("Przyjęcie na oddział")]
        RegisterInDepartment = 5,
        [Description("Wypisanie z oddziału")]
        RegisterOutDepartment = 6,
        [Description("Przyjęcie na salę")]
        RegisterInRoom = 7,
        [Description("Wypisanie z sali")]
        RegisterOutRoom = 8,
        [Description("Przypisanie lokalizatora")]
        BeltAssignment = 9,
        [Description("Bateria OK")]
        BatteryOk = 10,
        [Description("Połączony")]
        Connected = 11,
        [Description("Wejście do strefy")]
        ZoneIn = 12,
        [Description("Wyjście ze strefy zakazanej")]
        NoGoZoneOut = 13,
        [Description("Alarm użytkownika")]
        UserMessage = 14,
        [Description("Niski poziom baterii")]
        LowBattery = 15,
        [Description("Pacjent się porusza")]
        Moving = 16,
        [Description("Pacjent się nie porusza")]
        NoMoving = 17,
        [Description("Opuszczenie strefy")]
        ZoneOut = 18,
        [Description("Alarm z akcelerometru")]
        ACC = 19,
        [Description("Brak połączenia")]
        Disconnected = 20,
        [Description("Wejście do strefy zakazanej")]
        NoGoZoneIn = 21,
        [Description("Alarm SOS")]
        SOS = 22
    }
}
