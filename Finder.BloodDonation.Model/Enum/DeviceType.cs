﻿using System;
using System.ComponentModel;

namespace Finder.BloodDonation.Model.Enum
{
    public enum DeviceType
    {
        [Description("Unknown")]
        UNKNOWN = 0,
        [Description("Termometr")]
        THERMOMETER = 1,
        [Description("Termohigrometr")]
        THERMOHYGROMETER = 2,
		[Description("Terminal")]
		TERMINAL = 101
    }
}
