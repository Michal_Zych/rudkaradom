﻿using System;
using System.ComponentModel;

namespace Finder.BloodDonation.Model.Enum
{
    //Description określa nazwę pliku obrazka skojarzonego z danym typem unit'a
        //Description określa nazwę pliku obrazka skojarzonego z danym typem unit'a
        public enum UnitType
        {
            [Description("Default")]
            DEFAULT = 0,

            [Description("Root")]
            ROOT = 1,

            [Description("Termometr")]
            THERMOMETER = 2,
            [Description("Termohigrometr")]
            THERMOHYGROMETER = 8,

            [Description("Pokój")]
            ROOM = 3,
            [Description("Piętro")]
            FLOOR = 4,
            [Description("Centrum")]
            CENTRE = 5,
            [Description("Oddział")]
            DEPARTMENT = 6,

            [Description("Ulubiony")]
            FAVORITE = 7,

            [Description("Koszyki")]
            BASKET_CONTAINER = 9,

            [Description("Koszyk")]
            BASKET = 10,

            [Description("Mroźnia")]
            WAREHOUSE = 11,

            [Description("Półka")]
            SHELF = 12,

            [Description("Kuweta_karton")]
            BOX = 13,

            [Description("Koncentrator")]
            HUB = 99,

            [Description("Typ preparatu")]
            PREP_TYPE = 50,

            [Description("Fake")]
            FAKE = -1,

            [Description("Hospital department")]
            HOSPITAL_DEPARTMENT = 29
        }
    }

