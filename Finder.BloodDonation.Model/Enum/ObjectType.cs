﻿using System;
using System.ComponentModel;
using System.Net;


namespace Finder.BloodDonation.Model.Dto.Enums
{
    public enum ObjectType
    {
        [Description("Strefa")]
        Unit = 0,
        [Description("Użytkownik")]
        User = 1,
        [Description("Lokalizator")]
        Band = 2,
        [Description("Transmiter")]
        Transmitter = 3,
        [Description("Urządzenie")]
        Patient = 4,
        [Description("Konfiguracja alarmowania dla JO")]
        UnitAlarmConfiguration = 5,
        [Description("Konfiguracja alarmowania dla pacjenta")]
        PatientAlarmConfiguration = 6
    }
}
