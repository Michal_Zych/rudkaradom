﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Model.Enum
{
    public enum ObjectType_
    {
        Unit = 0,
        User = 1,
        Band = 2,
        Transmitter,
        Patient
    }
}
