﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Model.Enum
{
    public enum BandViewType
    {
        [Description("Default")]
        Create = 0,
        Edit = 1,
        Details = 2
    }
}
