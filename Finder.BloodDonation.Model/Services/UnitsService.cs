﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finder.BloodDonation.Model.Domain;
using System.ServiceModel;
using System.ServiceModel.Activation;
using FinderFX.Web.Core.DomainServices;
using NHibernate;
using FinderFX.Web.Core.Communication;
using NHibernate.Transform;
using FinderFX.Web.Core.Logging;
using AutoMapper;
using FinderFX.Web.Core.Authentication;
using NHibernate.Linq;
using NHibernate.Criterion;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Model.Enum;
using FinderFX.Web.Core.Caching;
using log4net;
using Finder.BloodDonation.Model.Services.Tools;
using Finder.BloodDonation.Helpers;
using System.Collections;
using Finder.BloodDonation.Common.Filters;
using System.Text.RegularExpressions;
using Finder.BloodDonation.DataTable;
using System.Timers;
using Finder.BloodDonation.Dialogs.Patients;

namespace Finder.BloodDonation.Model.Services
{
    [ServiceContract]
    public interface IUnitsService
    {
        [OperationContract]
        UnitDTO GetRootUnit();

        //nieuzywane
        [OperationContract]
        UnitDTO GetUnit(int unitID);

        [OperationContract]
        IList<UnitDTO> GetChildUnits(int parentUnitID);

        [OperationContract]
        IList<UnitDTO> GetFavoriteUnits();

        [OperationContract]
        UnitDTO GetRootFavoriteUnits();

        [OperationContract]
        int AddFavoriteUnits(IList<UnitDTO> units);

        [OperationContract]
        bool DeleteFavoriteUnits(IList<UnitDTO> units);

        [OperationContract]
        IList<UnitPreviewDto> GetChildsPreviews(int unit, int selectionSource);

        [OperationContract]
        IList<UnitPreviewDto> GetChildsFavoritePreviews(int unit);

        [OperationContract]
        IList<UnitPreviewDto> GetChildsPreviewsByHub(int hubID);

        [OperationContract]
        IList<UnitPreviewDto> GetPreviewsForUnit(int unit);

        [OperationContract]
        PreviewDto GetUnitPreview(int unit);

        [OperationContract]
        EditSensorDto GetSensorForUnit(int unitId);

        [OperationContract]
        int UpdateSensor(EditSensorDto sensor, string omittedRanges, string smsReceivers, string emailReceivers, string reason, string logEntry);

        [OperationContract]
        IList<SocketDto> GetPossibleSocketsForUnit(int unitId);

        [OperationContract]
        IList<SensorInfoDto> GetSensorsInUnit(int unit);

        [OperationContract]
        string CreateUnit(UnitDTO unit, string reason, string logEntry);

        [OperationContract]
        int UpdateUnit(UnitDTO unit, string reason, string logEntry);

        [OperationContract]
        int DeleteUnit(int unit, string reason);

        [OperationContract]
        int MoveUnit(int unit, int newParentUnit, string reason, string logEntry);

        [OperationContract]
        bool UpdatePreviewUnits(IList<ZonePreviewDto> previews, int unit, int preview);

        [OperationContract]
        IList<EditPreviewDto> GetPreviews();

        [OperationContract]
        UnitDTO GetUnitForUser(int userId);

        [OperationContract]
        UnitDTO GetUnitHierarchy(int localRootId);

        [OperationContract]
        IList<UserInfoDto> GetUsersForUnit(int unitId);

        [OperationContract]
        IList<NotificationReceiverDto> GetNotificationReceiversForUnit(int unitId);

        [OperationContract]
        IList<OmittedAlarmRangeDto> GetOmittedAlarmRangedForUnit(int unitId);

        [OperationContract]
        bool ToggleValueTestAlarm(bool enable, int sensorId, short initialValue, short eventType);

        [OperationContract]
        bool ToggleNoSensorTestAlarm(bool enable, int sensorId, short eventType);

        [OperationContract]
        void SetAlarmTestValue(short value, int sensorId);

        [OperationContract]
        void AddLogEntry(int userId, int unitId, string reason, string log, byte operationType, byte recordType);

        [OperationContract]
        IDictionary<int, int> GetUnitTransmitters();

        [OperationContract]
        IList<BatteryTypeDTO> GetBatteryTypes();

        [OperationContract]
        IList<BandDTO> GetBandConfiguration(int? idBand);

        [OperationContract]
        IList<TransmitterDetailsDto> GetTransmitterConfiguration(int? idTransmitter);

        [OperationContract]
        string SetBandConfiguration(BandDTO band);

        [OperationContract]
        string SetTransmitterConfiguration(TransmitterDetailsDto transmitter);

        [OperationContract]
        LocalizationConfigDTO GetLocalizationConfigForUnit(int UnitId);

        [OperationContract]
        string AssignBandUsingBarCode(string barCode, byte assignForce);

        [OperationContract]
        string SetLocalizationConfiguration(LocalizationConfigDTO localizationConfig);

        [OperationContract]
        PatientConfigurationDTO GetPatientConfiguration(int PatientId);

        [OperationContract]
        string SetPatientConfiguration(PatientConfigurationDTO patientConfig);

        [OperationContract]
        IList<ZonePreviewDto> PreviewForUnitGet(int unitId, int userId);

        [OperationContract]
        IList<PatientInfoDto> GetPatients();

        [OperationContract]
        ResultInfoDto PatientToHospital(PatientDTO patient);

        [OperationContract]
        string PatientFromHospital(int patientId);

        [OperationContract]
        string EditPatient(PatientDTO patient);

        [OperationContract]
        IList<UnitDTO> GetUnitsWithTransmitter();

        [OperationContract]
        IList<ZonesInfoDto> ZonesGet();

        [OperationContract]
        string AssignBandToObject(AssignedBandInfoDto dto, bool force);

        [OperationContract]
        string AssignPatientToWardRoom(PatientDTO patient, string code, string codeType);

        [OperationContract]
        IList<BandInfoDto> GetBandsInfoFiltered(IDictionary<string, string> filters, int pageNum, int pageSize);

        [OperationContract]
        int GetBandsInfoFilteredTotalCount(IDictionary<string, string> filters);

        [OperationContract]
        IList<BandPreviewDto> BandsForUnitGet(int unitId, int userId);

        [OperationContract]
        string TransmitterConfigurationSet(TransmitterDTO transmitter);

        [OperationContract]
        IList<TransmitterDTO> TransmitterConfigurationGet(short TransmitterId);

        [OperationContract]
        IList<BandStatusDto> GetBandsStatuses(long? lastToken);

        [OperationContract]
        IList<PatientDetailsDto> GetPatientDetails(IDictionary<string, string> filters, int pageNum, int pageSize);

        [OperationContract]
        int GetPatientDetailsFilteredTotalCount(IDictionary<string, string> filters);

        [OperationContract]
        IList<EventsDetailsDto> GetEventsDetails(IDictionary<string, string> filters, int pageNum, int pageSize);

        [OperationContract]
        int GetEventsDetailsFilteredTotalCount(IDictionary<string, string> filters);

        [OperationContract]
        IList<TransmitterEventsDto> GetTransmitterEvents(IDictionary<string, string> filters, int pageNum, int pageSize);

        [OperationContract]
        int GetTransmitterEventsFilteredTotalCount(IDictionary<string, string> filters);

        [OperationContract]
        IList<TransmitterDetailsDto> GetTransmitterDetailsFiltered(IDictionary<string, string> filters, int pageNum, int pageSize);

        [OperationContract]
        int GetTransmitterDetailsFilteredTotalCount(IDictionary<string, string> filters);

        [OperationContract]
        string CloseAlarms(EventsDetailsDto alarmToClose);

        [OperationContract]
        string AssignTransmitterToUnit(string macStr, int unitId, string reason);

        [OperationContract]
        string TransmitterRemoveFromUnit(int unitId, string reason);

        [OperationContract]
        string TransmitterDelete(int transmitterId, string reason);

        [OperationContract]
        IList<ArchivePatientsDto> GetArchivePatientsFiltered(IDictionary<string, string> filters, int pageNum, int pageSize);

        [OperationContract]
        int GetArchivePatientsFilteredTotalCount(IDictionary<string, string> filters);

        [OperationContract]
        string GetPatientId(string pesel);

        [OperationContract]
        IList<LogEntryDto> GetSystemEventsFiltered(IDictionary<string, string> filters, int pageNum, int pageSize);

        [OperationContract]
        int GetSystemEventsFilteredTotalCount(IDictionary<string, string> filters);

        [OperationContract]
        IList<EventsDetailsDto> GetEventsForUnitsFiltered(IDictionary<string, string> filters, int pageNum, int pageSize);

        [OperationContract]
        int GetEventsForUnitsFilteredTotalCount(IDictionary<string, string> filters);

        [OperationContract]
        LogEntryExtendedDto GetSystemEventStrings(int eventId);

        [OperationContract]
        ResultInfoDto BandBatteryChange(int bandId, BatteryTypeDTO newBattery, string reason);

        [OperationContract]
        IList<ExternalRecordsInDto> GetExternalRecordsInFiltered(IDictionary<string, string> filters, int pageNum, int pageSize);

        [OperationContract]
        int GetExternalRecordsInTotalCount(IDictionary<string, string> filters);

        [OperationContract]
        IList<ExternalRecordsOutDto> GetExternalRecordsOutFiltered(IDictionary<string, string> filters, int pageNum, int pageSize);

        [OperationContract]
        int GetExternalRecordsOutTotalCount(IDictionary<string, string> filters);

        [OperationContract]
        IList<ExternalUnitMapDto> GetExternalUnitMapFiltered(IDictionary<string, string> filters, int pageNum, int pageSize);

        [OperationContract]
        int GetExternalUnitMapFilteredTotalCount(IDictionary<string, string> filters);

        [OperationContract]
        string MapExternalUnit(string extId, int? m2mId, int operatorId, string reason);

        [OperationContract]
        IList<ServiceStatusDto> GetServiceStatusFiltered(IDictionary<string, string> filters, int pageNum, int pageSize);

        [OperationContract]
        int GetServiceStatusFilteredTotalCount(IDictionary<string, string> filters);

        [OperationContract]
        IList<GroupDto> TransmitterGroupsGet();

        [OperationContract]
        string TransmitterGroupSet(GroupDto dto);

        [OperationContract]
        IList<TransmitterDetailsDto> GetTransmitterRepairDetailsFiltered(IDictionary<string, string> filters, int pageNum, int pageSize);

        [OperationContract]
        int GetTransmitterRepairFilteredTotalCount(IDictionary<string, string> filters);

        [OperationContract]
        PatientDetailsDto GetPatient(int patientId);

        [OperationContract]
        MonitorableObjectStatusDto MonitoredObjectStatusGet(byte objectType, int ObjectId);

        [OperationContract]
        int GetAssignedPatientsCount(int unitId);

        [OperationContract]
        LocationInfoDto Object_GetLocation(int objectId, byte ObjectType);
    }

    [LogDetails]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class UnitsService : IUnitsService, IDomainService
    {
        private const string GetUserUnits = "exec dbo.Units_GetUserUnits :userId";
        private const int MINIMUM_BAND_INDEX = 1;

        private static ILog ActivityLogger = LogManager.GetLogger("Services.Units");

        public UnitDTO GetRootUnit()
        {

            //todo: pobierac roota (unit ktory ma parent ustawiony na null)
            return GetUnitInternal();
        }

        public UnitDTO GetUnitForUser(int userId)
        {
            IList<Unit> units;
            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                units = session.CreateSQLQuery(GetUserUnits)
                    .AddEntity(typeof(Unit))
                    .SetInt32("userId", userId)
                    .SetResultTransformer(new DistinctRootEntityResultTransformer())
                    .List<Unit>()
                    .Where(u => u.UnitType != UnitType.BASKET)
                    .ToList();
            }

            Unit root = GetTree(units[0].ID, units);
            var bam = new BloodAuthenticationManager();
            BloodyUser user = bam.GetUserById(userId);
            if (user != null)
            {
                FilterUnits(root, user);
            }

            UnitDTO dto = Mapper.Map<Unit, UnitDTO>(root);
            dto = SetLevels(dto);
            return dto;
        }

        public IList<UserInfoDto> GetUsersForUnit(int unitId)
        {
            const string GetUsersInfo = "EXEC [dbo].[usr_GetUsersList] :userId, :unitId";
            IList<UserInfoDto> result = null;
            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                var sql = session.CreateSQLQuery(GetUsersInfo)
                    .AddScalar("Id", NHibernateUtil.Int32)
                    .AddScalar("Name", NHibernateUtil.String)
                    .AddScalar("LastName", NHibernateUtil.String)
                    .AddScalar("Login", NHibernateUtil.String)

                    .SetInt32("userId", BloodyUser.Current.ID)
                    .SetInt32("unitId", (int?)null);

                result = sql.SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<UserInfoDto>())
                    .List<UserInfoDto>();

            }
            return result;
        }

        public UnitDTO GetUnit(int unitID)
        {
            //Nieużywane
            return GetUnitInternal();
        }

        public IList<UnitDTO> GetChildUnits(int parentUnitID)
        {
            if (parentUnitID == BloodyUser.FAVORITE_ID)
            {
                return GetFavoriteUnits();
            }

            UnitDTO unit = GetUnit(parentUnitID);

            return unit.Children;
        }

        public UnitDTO GetUnitHierarchy(int localRootId)
        {
            try
            {
                IList<Unit> units;

                using (ISession session = NHibernateHttpModule.OpenSession())
                {
                    var sql = "with Hierachy(ID, Name, UnitTypeID, ParentUnitID, ClientID, UnitTypeIDv2, PictureID)" +
                             " as" +
                             " (" +
                             "   select ID, Name, UnitTypeID, ParentUnitID, ClientID, UnitTypeIDv2, PictureID" +
                             "   from Units e" +
                             "   where e.ID = :id" +
                             "  union all" +
                             "   select e.ID, e.Name, e.UnitTypeID, e.ParentUnitID, e.ClientID, e.UnitTypeIDv2, e.PictureID" +
                             "   from Units e" +
                             "   inner join Hierachy eh" +
                             "      on e.ParentUnitID = eh.ID" +
                             " )" +
                             " select ID, Name, UnitTypeID, ParentUnitID, ClientID, UnitTypeIDv2, PictureID" +
                             " from Hierachy" +
                             " where UnitTypeID not in (10)"; //ignore baskets

                    units = session.CreateSQLQuery(sql)
                        .AddEntity(typeof(Unit))
                        .SetInt32("id", localRootId)
                        .SetResultTransformer(new DistinctRootEntityResultTransformer())
                        .List<Unit>();
                }

                BloodyUser u = User.Current as BloodyUser;

                Unit root = GetTree(localRootId, units);
                if (root.Children.Count > 1)
                    root.Children = root.Children.Reverse().ToList();

                if (u != null)
                {
                    FilterUnits(root, u);
                }

                UnitDTO dto = Mapper.Map<Unit, UnitDTO>(root);
                dto = SetLevels(dto);
                return dto;
            }
            catch (Exception exc)
            {
                ActivityLogger.Error("Exception in GetUnitHierarchy", exc);
                return null;
            }
        }

        internal UnitDTO GetUnitInternal()
        {
            try
            {
                IList<Unit> units;
                BloodyUser u = User.Current as BloodyUser;
                using (ISession session = NHibernateHttpModule.OpenSession())
                {
                    units = session.CreateSQLQuery(GetUserUnits)
                        .AddEntity(typeof(Unit))
                        .SetInt32("userId", u.ID)
                        .SetResultTransformer(new DistinctRootEntityResultTransformer())
                        .List<Unit>();
                }
                Unit root = GetTree(units[0].ID, units);
                //if (root.Children.Count > 1)
                //    root.Children = root.Children.Reverse().ToList();

                if (u != null)
                {
                    FilterUnits(root, u);
                }

                UnitDTO dto = Mapper.Map<Unit, UnitDTO>(root);
                dto = SetLevels(dto);
                return dto;
            }
            catch (Exception exc)
            {
                return null;
            }
        }

        internal UnitDTO SetLevels(UnitDTO unit)
        {
            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                var sql = @"with Hierachy(ID, Level) 
                            as (select ID, 0 as Level
                               from Units e   
                               where e.ID = :id  
                               union all   
                               select e.ID, eh.Level + 1   
                               from Units e   inner join Hierachy eh 
                               on e.ParentUnitID = eh.ID ) 
   
                               select ID, Level
                               from Hierachy";
                var c = session.CreateSQLQuery(sql)
                    .SetInt32("id", unit.ID)
                    .SetResultTransformer(new UnitLevelTransformer())
                    .List<UnitLevel>();

                SetLevels(ref unit, c);
            }
            return unit;
        }

        private static void SetLevels(ref UnitDTO unit, IList<UnitLevel> levels)
        {

            unit.Level = GetLevel(unit.ID, levels);


            for (int i = 0; i < unit.Children.Count; i++)
            {
                var child = unit.Children[i];
                SetLevels(ref child, levels);
                unit.Children[i] = child;
            }
        }

        private static int GetLevel(int unitID, IList<UnitLevel> levels)
        {
            return levels.First(x => unitID == x.UnitId).Level;
        }

        private static UnitDTO SetLevel(UnitDTO unit, int level)
        {
            unit.Level = level;
            return unit;
        }


        /// <summary>
        /// rekurencyjnie sprwadza do ktorych unitow na dostep
        /// uzytkownik i filtruje liste
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        internal static bool FilterUnits(Unit parent, BloodyUser currentUser)
        {

            if (parent.Children == null || parent.Children.Count == 0)
            {
                if (currentUser.GetUnitPermissions(parent.ID) == null || parent.UnitType == UnitType.BASKET)
                    return false;
                else
                    return true;
            }

            bool hasAccessToChildren = false;

            for (int i = parent.Children.Count - 1; i >= 0; i--)
            {
                Unit u = parent.Children[i];

                if (u.UnitType == UnitType.BASKET_CONTAINER || u.UnitType == UnitType.BASKET)
                {
                    hasAccessToChildren = true;
                    continue;
                }

                if (!FilterUnits(u, currentUser))
                {
                    parent.Children.Remove(u);
                }
                else
                {
                    hasAccessToChildren = true;
                }
            }
            parent.Children = parent.Children.OrderBy(c => c.Name).ToList();

            return hasAccessToChildren;

        }

        internal static bool FilterUnits(UnitDTO parent, BloodyUser currentUser)
        {
            if (parent.Children == null || parent.Children.Count == 0)
            {
                if (currentUser.GetUnitPermissions(parent.ID) == null)
                    return false;
                else
                    return true;
            }

            bool hasAccessToChildren = false;

            for (int i = parent.Children.Count - 1; i >= 0; i--)
            {
                UnitDTO u = parent.Children[i];

                if (!FilterUnits(u, currentUser))
                {
                    parent.Children.Remove(u);
                }
                else
                {
                    hasAccessToChildren = true;
                }
            }

            return hasAccessToChildren;

        }

        internal static UnitDTO GetTree(int unitID, IList<UnitDTO> loadedUnits)
        {
            UnitDTO unit = loadedUnits.FirstOrDefault(x => x.ID == unitID);

            unit.Children = GetChildUnits(unit, loadedUnits);

            return unit;
        }

        internal static Unit GetTree(int unitID, IList<Unit> loadedUnits)
        {
            Unit unit = loadedUnits.FirstOrDefault(x => x.ID == unitID);

            IDictionary<int, int> occurences = new Dictionary<int, int>();
            int count = 0;

            unit.Children = GetChildUnits(unit, loadedUnits, count);

            //var sb = new StringBuilder().AppendLine();
            //PrintTreeStructure(sb, unit, 0);
            //ActivityLogger.Debug(sb.ToString());

            return unit;
        }

        //private static IDictionary<int, int> occurences = new Dictionary<int, int>();

        private static IList<Unit> GetChildUnits(Unit parentUnit, IList<Unit> loadedUnits, int count)
        {
            const int MAX_NESTED_LEVEL = 8;

            IList<Unit> children = loadedUnits.Where(x => x.ParentUnitID == parentUnit.ID).ToList();

            count++;
            if (count > MAX_NESTED_LEVEL)
            {
                ActivityLogger.DebugFormat("parent: {0}, count: {1}", parentUnit.Name, count);
                return children;
            }

            //if (occurences[parentUnit.ID] > 3000)
            //	throw new InvalidOperationException(String.Format("loop in hierarchy: {0}", parentUnit.ID));

            if (children != null && children.Count > 0)
            {
                foreach (Unit u in children)
                {
                    u.Children = GetChildUnits(u, loadedUnits, count);
                }
            }

            return children;
        }

        private static void PrintTreeStructure(StringBuilder sb, Unit node, int level)
        {
            level++;
            var line = String.Empty;
            for (var i = 0; i < level; i++) line += "\t";

            sb.Append(line).AppendLine(node.Name);

            if (node.Children != null)
            {
                foreach (var child in node.Children)
                {
                    PrintTreeStructure(sb, child, level);
                }
            }
        }

        private static List<UnitDTO> GetChildUnits(UnitDTO parentUnit, IList<UnitDTO> loadedUnits)
        {
            List<UnitDTO> children = loadedUnits.Where(x => x.ParentUnitID == parentUnit.ID).ToList();

            if (children != null && children.Count > 0)
            {
                foreach (UnitDTO u in children)
                {
                    u.Children = GetChildUnits(u, loadedUnits);
                }
            }

            return children;
        }

        public IList<UnitDTO> GetFavoriteUnits()
        {
            return GetFavoriteUnits(User.Current.ID);
        }

        public IList<UnitDTO> GetFavoriteUnits(int userID)
        {
            var favUnits = new FavoriteUnits().GetFavoriteUnitsList(userID);

            IList<UnitDTO> units = new List<UnitDTO>();
            foreach (var f in favUnits)
            {
                units.Add(Mapper.Map<Unit, UnitDTO>(f));
            }
            return units;

        }

        public UnitDTO GetRootFavoriteUnits()
        {
            var u = GetRootFavoriteUnits(User.Current.ID);
            return u;
        }

        public UnitDTO GetRootFavoriteUnits(int userID)
        {
            var favs = new FavoriteUnits().GetFavoriteUnitsList(userID);
            //var favs = favorites.GetFavoriteUnitsList(userID);
            if (favs.Count > 0)
            {
                Unit root = GetTree(BloodyUser.Current.Root.ID, favs);
                BloodyUser u = User.Current as BloodyUser;
                FilterUnits(root, u);
                return Mapper.Map<Unit, UnitDTO>(root);
            }
            return null;
        }

        public int AddFavoriteUnits(IList<UnitDTO> units)
        {
            int userID = User.Current.ID;

            return AddFavoriteUnits(units, userID);

        }

        public int AddFavoriteUnits(IList<UnitDTO> units, int userID)
        {
            IList<UnitDTO> currentFavorites = GetFavoriteUnits(userID);


            using (ISession session = NHibernateHttpModule.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                BloodyUser user = User.Current as BloodyUser;

                foreach (UnitDTO u in units)
                {
                    IList<UnitDTO> childUnits = user.GetChildUnits(
                        u.ID,
                        PermissionNames.AlarmListing, UnitTypeOrganizer.SensorUnitsList());

                    if (childUnits == null || childUnits.Count == 0)
                    {
                        //jezeli juz ma w ulubionych to nie dodawac
                        if (currentFavorites.FirstOrDefault(x => x.ID == u.ID) != null)
                            continue;

                        FavoriteUnit fav = new FavoriteUnit
                        {
                            UserID = userID,
                            UnitID = u.ID
                        };
                        session.SaveOrUpdate(fav);
                    }
                    else
                    {
                        foreach (UnitDTO c in childUnits)
                        {
                            //jezeli juz ma w ulubionych to nie dodawac
                            if (currentFavorites.FirstOrDefault(x => x.ID == c.ID) != null)
                                continue;

                            FavoriteUnit fav = new FavoriteUnit
                            {
                                UserID = userID,
                                UnitID = c.ID
                            };
                            session.SaveOrUpdate(fav);
                        }
                    }
                }


                transaction.Commit();
            }

            return 1;
        }

        public bool DeleteFavoriteUnits(IList<UnitDTO> units)
        {
            int userID = User.Current.ID;
            return DeleteFavoriteUnits(units, userID);
        }

        public bool DeleteFavoriteUnits(IList<UnitDTO> units, int userID)
        {
            using (ISession session = NHibernateHttpModule.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {

                foreach (UnitDTO u in units)
                {
                    if (u.Children.Count > 0)
                    {
                        DeleteFavoriteUnits(u.Children);
                    }

                    FavoriteUnit fav = new FavoriteUnit
                    {
                        UserID = userID,
                        UnitID = u.ID
                    };

                    session.Delete(fav);
                }

                transaction.Commit();
            }

            return true;
        }

        public IList<UnitPreviewDto> GetChildsPreviews(int unit, int selectionSource)
        {
            /**/

            BloodyUser bu = User.Current as BloodyUser;
            string unitIDs = "";
            if (SelectionSources.Favorites == selectionSource)
            {
                if (UserSettings.FavoriteList_ShowOnlyDevices)
                {
                    return GetChildsFavoritePreviews(unit);
                }
                var favs = new FavoriteUnits().GetFavoriteUnitsList(bu.ID);
                var oneLevel = bu.GetChildUnits(unit, PermissionNames.UnitsListing, null, 1);
                var IDs = from f in favs
                          join o in oneLevel on f.ID equals o.ID
                          select o.ID;
                unitIDs = IDs.ToCommaSeparatedValues();
            }
            else
            {
                unitIDs = bu.GetChildUnits(unit, PermissionNames.UnitsListing, null, 1).Select(c => c.ID).ToCommaSeparatedValues();
            }


            IList<UnitPreview> r = null;

            using (ISession x = NHibernateHttpModule.OpenSession())
            {
                r = x.GetNamedQuery("Units_GetChildsPreviews")
                    .SetString("units", unitIDs)
                    .SetInt32("user", User.Current.ID)
                    .List<UnitPreview>();
            }

            return Mapper.Map<IList<UnitPreview>, IList<UnitPreviewDto>>(r);
        }

        public IList<EditPreviewDto> GetPreviews()
        {
            IList<EditPreview> r;
            int clientID = BloodyUser.Current.ClientID;

            using (ISession x = NHibernateHttpModule.OpenSession())
            {
                r = (from a in x.Linq<EditPreview>() where a.ClientID == clientID select a).ToList();
            }

            return Mapper.Map<IList<EditPreview>, IList<EditPreviewDto>>(r);
        }

        public IList<UnitPreviewDto> GetChildsFavoritePreviews(int unit)
        {
            BloodyUser bu = User.Current as BloodyUser;

            string unitIDs = "";

            List<int> parent_devices = (from c in bu.GetChildUnits(unit, PermissionNames.UnitsListing, UnitTypeOrganizer.SensorUnitsList(), null) select c.ID).ToList();

            IList<UnitDTO> units = GetFavoriteUnits(bu.ID);
            unitIDs = string.Join(",",
                units.Where(w => { return parent_devices.Contains(w.ID); }).Select<UnitDTO, int>(x => x.ID).ToArray());

            IList<UnitPreview> r = null;

            using (ISession x = NHibernateHttpModule.OpenSession())
            {
                r = x.GetNamedQuery("Units_GetChildsPreviews")
                    .SetString("units", unitIDs)
                    .SetInt32("user", User.Current.ID)
                    .List<UnitPreview>();
            }

            return Mapper.Map<IList<UnitPreview>, IList<UnitPreviewDto>>(r);
        }

        public IList<UnitPreviewDto> GetChildsPreviewsByHub(int hubID)
        {
            IList<UnitPreview> r = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                r = session.GetNamedQuery("Units_GetChildsPreviewsByHub")
                   .SetInt32("hubID", hubID)
                   .List<UnitPreview>();
            }

            return Mapper.Map<IList<UnitPreview>, IList<UnitPreviewDto>>(r);
        }

        public IList<UnitDTO> GetUnitsWithTransmitter()
        {
            const string param = "EXEC [dbo].[unt_UnitWithTransmitterGet]";

            IList<Hashtable> results = null;
            IList<UnitDTO> outputs = new List<UnitDTO>();
            IList<Unit> resultList = new List<Unit>();

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                results = s.CreateSQLQuery(param)
                    //.AddEntity(typeof(Unit))
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            foreach (var item in results)
            {
                var unit = new Unit()
                {
                    ID = Convert.ToInt32(item["UnitID"]),
                    Name = Convert.ToString(item["Name"]),
                    ParentUnitID = Convert.ToInt32(item["ParentUnitID"]),
                    ClientID = Convert.ToInt32(item["ClientID"]),
                    UnitTypeIDv2 = Convert.ToInt32(item["UnitTypeIdv2"]),
                    Description = Convert.ToString(item["Description"]),
                };

                resultList.Add(unit);
            }

            for (int i = 0; i < resultList.Count; i++)
            {
                Unit root = GetTree(resultList[i].ID, resultList);

                UnitDTO dto = Mapper.Map<Unit, UnitDTO>(root);
                outputs.Add(dto);
            }

            return outputs;
        }

        public IList<UnitPreviewDto> GetPreviewsForUnit(int unit)
        {
            const string GetPreviewsForUnitswProc = "exec unt_GetPreviewsForUnit :unitId";
            IList<UnitPreviewDto> r = null;
            BloodyUser bu = User.Current as BloodyUser;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                r = s.CreateSQLQuery(GetPreviewsForUnitswProc)
                    .SetInt32("unitId", unit)
                    .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<UnitPreviewDto>())
                    .List<UnitPreviewDto>();
            }

            IList<UnitPreviewDto> result = new List<UnitPreviewDto>();
            for (int i = 0; i < r.Count; i++)
            {
                if (bu.HasPermissions(r[i].ID, PermissionNames.UnitsListing))
                {
                    result.Add(r[i]);
                }
            }

            return result;
        }

        public IList<ZonePreviewDto> PreviewForUnitGet(int unitId, int userId)
        {
            const string procedure = "exec unt_PreviewForUnitGet :unitId,:userId";

            IList<ZonePreviewDto> r = new List<ZonePreviewDto>();
            BloodyUser bu = User.Current as BloodyUser;

            IList<Hashtable> results = null;


            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                results = s.CreateSQLQuery(procedure)
                    .SetInt32("unitId", unitId)
                    .SetInt32("userId", userId)
                    .SetResultTransformer(NHibernate.Transform.Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            if (results != null)
            {
                foreach (var item in results)
                {
                    var zonePreview = new ZonePreviewDto()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Name = Convert.ToString(item["RoomName"]),
                        Location = Convert.ToString(item["Location"]),
                        TLX = Convert.ToDouble(item["TLX"]),
                        TLY = Convert.ToDouble(item["TLY"]),
                        BRX = Convert.ToDouble(item["BRX"]),
                        BRY = Convert.ToDouble(item["BRY"])
                    };

                    r.Add(zonePreview);
                }
            }

            return r;
        }

        private int PREVIEW_WITHOUT_BACKGROUND = -1;

        public bool UpdatePreviewUnits(IList<ZonePreviewDto> previews, int unit, int picture)
        {
            using (ISession x = NHibernateHttpModule.OpenSession())
            {
                using (ITransaction t = x.BeginTransaction())
                {
                    x.CreateSQLQuery("exec unt_UpdatePreview @unitId = :unitId, @pictureId = :pictureId")
                                .SetInt32("unitId", unit)
                                .SetInt32("pictureId", picture)
                                .ExecuteUpdate();

                    for (int i = 0; i < previews.Count; i++)
                    {
                        x.Evict((from e in x.Linq<EditUnitPreview>() where e.PreviewedUnitID == unit && e.UnitID == previews[i].Id select e).FirstOrDefault());

                        EditUnitPreview eup = new EditUnitPreview()
                        {
                            UnitID = previews[i].Id,
                            PreviewedUnitID = unit,
                            TLX = previews[i].TLX,
                            TLY = previews[i].TLY,
                            BRX = previews[i].BRX,
                            BRY = previews[i].BRY
                        };

                        x.Save(eup);
                    }

                    t.Commit();
                }
            }

            return true;
        }

        public PreviewDto GetUnitPreview(int unit)
        {
            const string GetUnitPreviewProc = "exec dbo.unt_GetUnitPreview :unit";
            PreviewDto r = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                r = s.CreateSQLQuery(GetUnitPreviewProc)
                    .SetInt32("unit", unit)
                    .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<PreviewDto>())
                    .List<PreviewDto>().FirstOrDefault();
            }

            return r;
        }

        public EditSensorDto GetSensorForUnit(int unitId)
        {
            const string GetSensorForUnitProc = "exec dbo.unt_GetSensorConfiguration :unitId";
            EditSensorDto sensor = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                sensor = s.CreateSQLQuery(GetSensorForUnitProc)
                    .SetInt32("unitId", unitId)
                    .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<EditSensorDto>())
                    .List<EditSensorDto>().FirstOrDefault();
            }

            return sensor;
        }

        public int UpdateSensor(EditSensorDto sensor, string omittedRanges, string smsReceivers, string emailReceivers, string reason, string logEntry)
        {
            const string UpdateSensorProc =
                "EXEC [dbo].[unt_EditSensor] :Id, :SocketID, :Interval, :LoRange, :UpRange, :GuiRangeDelay, :GuiCommDelay"
            + ", :SmsEnabled, :SmsRangeDelay, :SmsCommDelay"
            + ", :MailEnabled, :MailRangeDelay, :MailCommDelay"
            + ", :AlDisabled, :AlDisabledForHours, :AlDisableHoursFrom1, :AlDisableHoursTo1, :AlDisableHoursFrom2, :AlDisableHoursTo2, :AlDisabledForRanges"
            + ", :MU, :LegendDescription, :LegendShortcut, :LegendRangeDescription"
            + ", :LineColor, :LineWidth, :RangeLineWidth, :LineStyle, :RangeLineStyle, :AlarmColor"
            + ", :Enabled, :Calibration, :operatorId, :logEntry, :reason, :omittedRanges, :smsReceivers, :emailReceivers"
            + ", :IsBitSensor, :IsHighAlarming, :HiStateDesc, :LoStateDesc"
            + ", :GuiRangeRepeat, :GuiRangeRepeatMins, :GuiRangeRepeatBreakable, :GuiCommRepeat, :GuiCommRepeatMins, :GuiCommRepeatBreakable"
            + ", :SmsRangeEnabled, :SmsRangeAfterFinish, :SmsRangeAfterClose, :SmsRangeRepeat, :SmsRangeRepeatMins, :SmsRangeRepeatUntil, :SmsRangeRepeatBreakable"
            + ", :SmsCommEnabled, :SmsCommAfterFinish, :SmsCommAfterClose, :SmsCommRepeat, :SmsCommRepeatMins, :SmsCommRepeatUntil, :SmsCommRepeatBreakable"
            + ", :MailRangeEnabled, :MailRangeAfterFinish, :MailRangeAfterClose, :MailRangeRepeat, :MailRangeRepeatMins, :MailRangeRepeatUntil, :MailRangeRepeatBreakable"
            + ", :MailCommEnabled, :MailCommAfterFinish, :MailCommAfterClose, :MailCommRepeat, :MailCommRepeatMins, :MailCommRepeatUntil, :MailCommRepeatBreakable";

            short editedSensorId = -1;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                editedSensorId = session.CreateSQLQuery(UpdateSensorProc)
                     .SetInt16("Id", sensor.ID)
                     .SetInt32("SocketID", sensor.SocketId)
                     .SetInt32("Interval", sensor.Interval)
                     .SetDouble("LoRange", sensor.LoRange)
                     .SetDouble("UpRange", sensor.UpRange)
                     .SetInt16("GuiRangeDelay", sensor.GuiRangeDelay)
                     .SetInt16("GuiCommDelay", sensor.GuiCommDelay)
                     .SetBoolean("SmsEnabled", sensor.SmsEnabled)
                     .SetInt16("SmsRangeDelay", sensor.SmsRangeDelay)
                     .SetInt16("SmsCommDelay", sensor.SmsCommDelay)
                     .SetBoolean("MailEnabled", sensor.MailEnabled)
                     .SetInt16("MailRangeDelay", sensor.MailRangeDelay)
                     .SetInt16("MailCommDelay", sensor.MailCommDelay)
                     .SetBoolean("AlDisabled", sensor.AlDisabled)
                     .SetBoolean("AlDisabledForHours", sensor.AlDisabledForHours)

                     .SetDateTime("AlDisableHoursFrom1", sensor.AlDisableHoursFrom1)
                     .SetDateTime("AlDisableHoursTo1", sensor.AlDisableHoursTo1)
                     .SetDateTime("AlDisableHoursFrom2", sensor.AlDisableHoursFrom2)
                     .SetDateTime("AlDisableHoursTo2", sensor.AlDisableHoursTo2)

                     .SetBoolean("AlDisabledForRanges", sensor.AlDisabledForRanges)
                     .SetString("MU", sensor.MU)
                     .SetString("LegendDescription", sensor.LegendDescription)
                     .SetString("LegendShortcut", sensor.LegendShortcut)
                     .SetString("LegendRangeDescription", sensor.LegendRangeDescription)
                     .SetString("LineColor", sensor.LineColor)
                     .SetByte("LineWidth", sensor.LineWidth)
                     .SetByte("RangeLineWidth", sensor.RangeLineWidth)
                     .SetString("LineStyle", sensor.LineStyle)
                     .SetString("RangeLineStyle", sensor.RangeLineStyle)
                     .SetString("RangeLineStyle", sensor.RangeLineStyle)
                     .SetString("AlarmColor", sensor.AlarmColor)
                     .SetBoolean("Enabled", sensor.Enabled)
                     .SetDouble("Calibration", sensor.Calibration)
                     .SetInt32("operatorId", BloodyUser.Current.ID)
                     .SetString("logEntry", logEntry)
                     .SetString("reason", reason)
                     .SetString("omittedRanges", omittedRanges)
                     .SetString("smsReceivers", smsReceivers)
                     .SetString("emailReceivers", emailReceivers)
                     .SetBoolean("IsBitSensor", sensor.IsBitSensor)
                     .SetBoolean("IsHighAlarming", sensor.IsHighAlarming)
                     .SetString("HiStateDesc", sensor.HiStateDesc)
                     .SetString("LoStateDesc", sensor.LoStateDesc)
                     .SetBoolean("GuiRangeRepeat", sensor.GuiRangeRepeat)
                     .SetInt32("GuiRangeRepeatMins", sensor.GuiRangeRepeatMins)
                     .SetBoolean("GuiRangeRepeatBreakable", sensor.GuiRangeRepeatBreakable)
                     .SetBoolean("GuiCommRepeat", sensor.GuiCommRepeat)
                     .SetInt32("GuiCommRepeatMins", sensor.GuiCommRepeatMins)
                     .SetBoolean("GuiCommRepeatBreakable", sensor.GuiCommRepeatBreakable)
                     .SetBoolean("SmsRangeEnabled", sensor.SmsRangeEnabled)
                     .SetBoolean("SmsRangeAfterFinish", sensor.SmsRangeAfterFinish)
                     .SetBoolean("SmsRangeAfterClose", sensor.SmsRangeAfterClose)
                     .SetBoolean("SmsRangeRepeat", sensor.SmsRangeRepeat)
                     .SetInt32("SmsRangeRepeatMins", sensor.SmsRangeRepeatMins)
                     .SetString("SmsRangeRepeatUntil", sensor.SmsRangeRepeatUntil)
                     .SetBoolean("SmsRangeRepeatBreakable", sensor.SmsRangeRepeatBreakable)
                     .SetBoolean("SmsCommEnabled", sensor.SmsCommEnabled)
                     .SetBoolean("SmsCommAfterFinish", sensor.SmsCommAfterFinish)
                     .SetBoolean("SmsCommAfterClose", sensor.SmsCommAfterClose)
                     .SetBoolean("SmsCommRepeat", sensor.SmsCommRepeat)
                     .SetInt32("SmsCommRepeatMins", sensor.SmsCommRepeatMins)
                     .SetString("SmsCommRepeatUntil", sensor.SmsCommRepeatUntil)
                     .SetBoolean("SmsCommRepeatBreakable", sensor.SmsCommRepeatBreakable)
                     .SetBoolean("MailRangeEnabled", sensor.MailRangeEnabled)
                     .SetBoolean("MailRangeAfterFinish", sensor.MailRangeAfterFinish)
                     .SetBoolean("MailRangeAfterClose", sensor.MailRangeAfterClose)
                     .SetBoolean("MailRangeRepeat", sensor.MailRangeRepeat)
                     .SetInt32("MailRangeRepeatMins", sensor.MailRangeRepeatMins)
                     .SetString("MailRangeRepeatUntil", sensor.MailRangeRepeatUntil)
                     .SetBoolean("MailRangeRepeatBreakable", sensor.MailRangeRepeatBreakable)
                     .SetBoolean("MailCommEnabled", sensor.MailCommEnabled)
                     .SetBoolean("MailCommAfterFinish", sensor.MailCommAfterFinish)
                     .SetBoolean("MailCommAfterClose", sensor.MailCommAfterClose)
                     .SetBoolean("MailCommRepeat", sensor.MailCommRepeat)
                     .SetInt32("MailCommRepeatMins", sensor.MailCommRepeatMins)
                     .SetString("MailCommRepeatUntil", sensor.MailCommRepeatUntil)
                     .SetBoolean("MailCommRepeatBreakable", sensor.MailCommRepeatBreakable)
                     .UniqueResult<short>();
            }

            return editedSensorId;
        }

        public IList<SocketDto> GetPossibleSocketsForUnit(int unitId)
        {
            const string GetPossibleSocketsForUnitProc = "exec dbo.unt_GetPossibleSocketsForUnit :unitId";
            IList<SocketDto> sockets = null;

            using (var s = NHibernateHttpModule.OpenSession())
            {
                var results = s.CreateSQLQuery(GetPossibleSocketsForUnitProc)
                    .SetInt32("unitId", unitId)
                    .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<SocketDto>())
                    .List<SocketDto>();
                sockets = results;
            }

            return sockets;
        }

        public IList<SensorInfoDto> GetSensorsInUnit(int unit)
        {
            const string GetSensorsInUnitProc = "exec dbo.unt_GetSensorsInUnit :unitId"; ;
            IList<SensorInfoDto> sensors = null;

            using (var s = NHibernateHttpModule.OpenSession())
            {
                var results = s.CreateSQLQuery(GetSensorsInUnitProc)
                    .SetInt32("unitId", unit)
                    .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<SensorInfoDto>())
                    .List<SensorInfoDto>();
                sensors = results;
            }

            return sensors;
        }

        public IList<NotificationReceiverDto> GetNotificationReceiversForUnit(int unitId)
        {
            const string GetNotificationReceiversForUnitProc = "exec dbo.unt_GetNotificationReceiversForUnit :unitId"; ;
            IList<NotificationReceiverDto> receivers = null;

            using (var s = NHibernateHttpModule.OpenSession())
            {
                var results = s.CreateSQLQuery(GetNotificationReceiversForUnitProc)
                    .SetInt32("unitId", unitId)
                    .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<NotificationReceiverDto>())
                    .List<NotificationReceiverDto>();
                receivers = results;
            }

            return receivers;
        }

        public IList<OmittedAlarmRangeDto> GetOmittedAlarmRangedForUnit(int unitId)
        {
            const string GetOmittedAlarmRangesForUnit = "exec dbo.unt_GetOmittedAlarmRangesForUnit :unitId"; ;
            IList<OmittedAlarmRangeDto> ranges = null;

            using (var s = NHibernateHttpModule.OpenSession())
            {
                var results = s.CreateSQLQuery(GetOmittedAlarmRangesForUnit)
                    .SetInt32("unitId", unitId)
                    .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<OmittedAlarmRangeDto>())
                    .List<OmittedAlarmRangeDto>();
                ranges = results;
            }

            return ranges;
        }

        [CacheRefresh("LoadUser")]
        public string CreateUnit(UnitDTO unit, string reason, string logEntry)
        {
            const string CreateUnitProc = "EXEC [dbo].[unt_UnitCreate] :name, :parentUnitId, :unitTypeIdV2, :pictureId, :description, :operator, :reason";

            IList<Hashtable> results = null;
            string result = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                results = session.CreateSQLQuery(CreateUnitProc)
                     .SetString("name", unit.Name)
                     .SetInt32("parentUnitId", unit.ParentUnitID)
                     .SetInt32("unitTypeIdV2", unit.UnitTypeIDv2)
                     .SetInt32("pictureId", unit.PictureID)
                     .SetString("description", unit.Description)
                     .SetInt32("operator", BloodyUser.Current.ID)
                     .SetString("reason", reason)
                     .SetResultTransformer(Transformers.AliasToEntityMap)
                     .List<Hashtable>();

                result = results.GetStringResult("Result", "ErrorMessage");

                return result;
            }
        }

        [CacheRefresh("LoadUser")]
        public int UpdateUnit(UnitDTO unit, string reason, string logEntry)
        {
            const string UpdateUnitProc = "EXEC [dbo].[unt_EditUnit] :Id, :name, :unitTypeIdV2, :pictureId, :description, :operator, :logEntry, :reason";

            int editedUnitId;
            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                editedUnitId = session.CreateSQLQuery(UpdateUnitProc)
                     .SetInt32("Id", unit.ID)
                     .SetString("name", unit.Name)
                     .SetInt32("unitTypeIdV2", unit.UnitTypeIDv2)
                     .SetInt32("pictureId", unit.PictureID)
                     .SetString("description", unit.Description)
                     .SetInt32("operator", BloodyUser.Current.ID)
                     .SetString("logEntry", logEntry)
                     .SetString("reason", reason)
                     .UniqueResult<int>();

                if (editedUnitId > 0)
                {
                    System.Web.HttpContext.Current.Cache.Remove("user_" + User.Current.ID);
                    System.Web.HttpContext.Current.Cache.Remove("LoadUser_" + User.Current.ID);
                }
            }


            return editedUnitId;
        }

        [CacheRefresh("LoadUser")]
        public int DeleteUnit(int unit, string reason)
        {
            const string DeleteUnitProc = "EXEC [dbo].[unt_DeleteUnit] :unitId, :operator, :reason";

            int deletedUnitId;
            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                deletedUnitId = session.CreateSQLQuery(DeleteUnitProc)
                     .SetInt32("unitId", unit)
                     .SetInt32("operator", BloodyUser.Current.ID)
                     .SetString("reason", reason)
                     .UniqueResult<int>();

                if (deletedUnitId > 0)
                {
                    System.Web.HttpContext.Current.Cache.Remove("user_" + User.Current.ID);
                    System.Web.HttpContext.Current.Cache.Remove("LoadUser_" + User.Current.ID);
                }
            }
            return deletedUnitId;
        }

        private string NullIfEmpty(string text)
        {
            if (text != null)
            {
                text = text.Trim();
                if (string.IsNullOrEmpty(text))
                    return null;
            }
            return text;
        }

        [CacheRefresh("LoadUser")]
        public int MoveUnit(int unit, int newParentUnit, string reason, string logEntry)
        {
            const string MoveUnitProc = "EXEC [dbo].[unt_MoveUnit] :unitId, :parentId, :operatorId, :logEntry, :reason";

            int editedUnitId;
            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                editedUnitId = session.CreateSQLQuery(MoveUnitProc)
                     .SetInt32("unitId", unit)
                     .SetInt32("parentId", newParentUnit)
                     .SetInt32("operatorId", BloodyUser.Current.ID)
                     .SetString("logEntry", logEntry)
                     .SetString("reason", reason)
                     .UniqueResult<int>();

                if (editedUnitId > 0)
                {
                    System.Web.HttpContext.Current.Cache.Remove("user_" + User.Current.ID);
                    System.Web.HttpContext.Current.Cache.Remove("LoadUser_" + User.Current.ID);
                }
            }

            return editedUnitId;
        }

        public bool ToggleValueTestAlarm(bool enable, int sensorId, short initialValue, short eventType)
        {
            if (enable)
                return StartAlarmConditionTest(sensorId, initialValue, eventType);
            else
                return StopAlarmConditionTest(sensorId);
        }

        public bool ToggleNoSensorTestAlarm(bool enable, int sensorId, short eventType)
        {
            if (enable)
                return StartNoSensorAlarmConditionTest(sensorId, eventType);
            else
                return StopAlarmConditionTest(sensorId);
        }

        public void SetAlarmTestValue(short value, int sensorId)
        {
            System.Diagnostics.Debug.WriteLine("setting test value: {0}", value);
            SetSensorTestValue(value, sensorId);
        }

        public void AddLogEntry(int userId, int unitId, string reason, string log, byte operationType, byte recordType)
        {
            //AddLogEntryInternal(userId, unitId, reason, log, operationType, recordType);
            Console.WriteLine("logging operation: userid:{0}, unitid:{1}, log:{2}, ot:{3}, rt:{4}", userId, unitId, log, operationType, recordType);
        }

        private static void AddLogEntryInternal(int userId, int unitId, string reason, string log, byte operationType,
            byte recordType)
        {
            const string addLogEntryProc =
                "EXEC [dbo].[log_AddAppLogEntry] :userId, :unitId, :reason, :log, :operationType, :recordType";

            try
            {
                using (ISession session = NHibernateHttpModule.OpenSession())
                {
                    var result = session.CreateSQLQuery(addLogEntryProc)
                        .SetInt32("userId", userId)
                        .SetInt32("unitId", unitId)
                        .SetString("reason", reason)
                        .SetString("log", log)
                        .SetByte("operationType", operationType)
                        .SetByte("recordType", recordType)
                        .UniqueResult<int>();
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("unable to toggle testing value alarm: {0}, error: {1}", true, e.Message);
            }
        }

        private static bool StartAlarmConditionTest(int sensorId, short initialValue, short eventType)
        {
            const string procCall = "EXEC [dbo].[test_AddTest] :sensorId, :value, :eventType";

            try
            {
                using (ISession session = NHibernateHttpModule.OpenSession())
                {
                    var result = session.CreateSQLQuery(procCall)
                        .SetInt32("sensorId", sensorId)
                        .SetInt16("value", initialValue)
                        .SetInt16("eventType", eventType)
                        .UniqueResult<int>();

                    return result != 0;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("unable to toggle testing value alarm: {0}, error: {1}", true, e.Message);
                return false;
            }
        }

        private static bool StartNoSensorAlarmConditionTest(int sensorId, short eventType)
        {
            const string procCall = "EXEC [dbo].[test_AddTest] :sensorId, NULL, :eventType";

            try
            {
                using (ISession session = NHibernateHttpModule.OpenSession())
                {
                    var result = session.CreateSQLQuery(procCall)
                        .SetInt32("sensorId", sensorId)
                        .SetInt16("eventType", eventType)
                        .UniqueResult<int>();

                    return result != 0;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("unable to toggle testing value alarm: {0}, error: {1}", true, e.Message);
                return false;
            }
        }

        private static bool StopAlarmConditionTest(int sensorId)
        {
            const string procCall = "EXEC [dbo].[test_RemoveTest] :sensorId";

            try
            {
                using (ISession session = NHibernateHttpModule.OpenSession())
                {
                    var result = session.CreateSQLQuery(procCall)
                        .SetInt32("sensorId", sensorId)
                        .UniqueResult<int>();

                    return result != 0;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("unable to toggle testing value alarm: {0}, error: {1}", false, e.Message);
                return false;
            }
        }

        private static bool SetSensorTestValue(short value, int sensorId)
        {
            const string procCall = "EXEC [dbo].[test_SetTestValue] :sensorId, :value";

            try
            {
                using (ISession session = NHibernateHttpModule.OpenSession())
                {
                    var result = session.CreateSQLQuery(procCall)
                        .SetInt32("sensorId", sensorId)
                        .SetInt16("value", value)
                        .UniqueResult<int>();

                    return result != 0;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("unable to toggle testing value alarm: {0}, error: {1}", false, e.Message);
                return false;
            }
        }

        public IDictionary<int, int> GetUnitTransmitters()
        {
            return new Dictionary<int, int>();
            /*const string proc = "EXEC [dbo].[unt_GetUnitTransmitters] :clientId";

            IDictionary<int, int> dict = null;
            IList<Hashtable> results = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                results = session.CreateSQLQuery(proc)
                    .SetInt32("clientId", BloodyUser.Current.ClientID)
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            if (results != null)
            {
                dict = new Dictionary<int, int>();
                foreach (var item in results)
                {
                    dict[Convert.ToInt32(item["UnitId"])] = Convert.ToInt32(item["Id"]);
                }
            }


            return dict;*/
        }

        public IList<BatteryTypeDTO> GetBatteryTypes()
        {
            const string proc = "EXEC [dbo].[dev_GettBatteryTypes]";

            IList<BatteryTypeDTO> list = new List<BatteryTypeDTO>();
            IList<Hashtable> results = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                results = session.CreateSQLQuery(proc)
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            if (results != null)
            {

                foreach (var item in results)
                {
                    var batteryType = new BatteryTypeDTO()
                    {
                        Id = Convert.ToByte(item["Id"])
                        ,
                        Name = Convert.ToString(item["Name"])
                        ,
                        LifeMonts = Convert.ToInt16(item["LifeMonths"]),
                        Capacity = Convert.ToInt32(item["Capacity"]),
                        Voltage = (float)Convert.ToDouble(item["Voltage"]),
                        Description = Convert.ToString(item["Description"])
                    };
                    list.Add(batteryType);
                }
            }

            return list;
        }

        public IList<TransmitterDTO> TransmitterConfigurationGet(short transmitterId)
        {
            const string proc = "EXEC [dbo].[dev_TransmitterConfigurationGet] :TransmitterId";

            IList<TransmitterDTO> list = new List<TransmitterDTO>();
            IList<Hashtable> results = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                results = session.CreateSQLQuery(proc)
                    .SetInt16("TransmitterId", transmitterId)
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            if (results != null)
            {
                foreach (var item in results)
                {
                    var transmitter = new TransmitterDTO()
                    {
                        Id = Convert.ToInt16(item["Id"]),
                        AvgCalcTimeS = Convert.ToInt32(item["AvgCalcTimeS"]),
                        BarCode = Convert.ToString(item["BarCode"]),
                        Description = Convert.ToString(item["Description"]),
                        IsBandUpdater = Convert.ToBoolean(item["IsBandUpdater"]),
                        Mac = Convert.ToInt64(item["MacId"]),
                        MacString = Convert.ToString(item["Mac"]),
                        MSISDN = Convert.ToString(item["Msisdn"]),
                        Name = Convert.ToString(item["Name"]),
                        ReportIntervalSec = Convert.ToInt32(item["ReportIntervalSec"]),
                        Sensitivity = Convert.ToInt32(item["Sensitivity"]),
                        SwitchDelayS = Convert.ToInt32(item["SwitchDelayS"]),
                        TransmitterInstallationUnit = Convert.ToString(item["TransmitterUnitLocation"]),
                        Transitions = Convert.ToString(item["Transitions"]),
                        IpAddress = Convert.ToString(item["IpAddress"]),
                        Group = Convert.ToByte(item["Group"]),
                        Type = Convert.ToInt32(item["TypeId"]),
                        UnitId = Convert.ToInt32(item["UnitID"])
                    };

                    if (transmitter.Id < MINIMUM_BAND_INDEX)
                    {
                        transmitter.MacString = String.Empty;
                    }

                    list.Add(transmitter);
                }
            }

            return list;
        }

        public IList<BandDTO> GetBandConfiguration(int? idBand)
        {
            const string proc = "EXEC [dbo].[dev_BandConfigurationGet] :BandId";

            IList<BandDTO> list = new List<BandDTO>();
            IList<Hashtable> results = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                results = session.CreateSQLQuery(proc)
                    .SetInt32("BandId", idBand)
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            if (results != null)
            {
                foreach (var item in results)
                {
                    BandDTO band = new BandDTO();

                    band.Id = Convert.ToInt16(item["Id"]);
                    band.Mac = Convert.ToInt64(item["Mac"]);
                    band.MacString = Convert.ToString(item["MacString"]);
                    band.Uid = QueryParametersParser.ConvertToLong(item["Uid"]);
                    band.SerialNumber = Convert.ToString(item["SerialNumber"]);
                    band.BarCode = Convert.ToString(item["BarCode"]);
                    band.TransmissionPower = Convert.ToInt16(item["TransmissionPower"]);
                    band.AccPercent = Convert.ToByte(item["AccPercent"]);
                    band.SensitivityPercent = Convert.ToByte(item["SensitivityPercent"]);
                    band.BroadcastingIntervalMs = Convert.ToInt32(item["BroadcastingIntervalMs"]);
                    band.Parameters = ListsConverter.ToList(Convert.ToString(item["Parameters"]));
                    band.Description = Convert.ToString(item["Description"]);
                    band.ObjectType = Convert.ToByte(item["ObjectType"]);
                    band.ObjectId = Convert.ToInt32(item["ObjectId"]);
                    band.ObjectDisplayName = Convert.ToString(item["ObjectDisplayName"]);
                    band.UnitId = QueryParametersParser.ConvertToInt(item["ObjectUnitId"]);
                    band.CurrentUnitId = QueryParametersParser.ConvertToInt(item["CurrentUnitId"]);
                    band.CurrentUnitName = Convert.ToString(item["CurrentUnitName"]);
                    band.CurrentUnitLocation = Convert.ToString(item["CurrentUnitLocation"]);
                    band.BatteryTypeId = Convert.ToByte(item["BatteryTypeId"]);
                    band.BatteryInstallationDateUtc = Convert.ToDateTime(item["BatteryInstallationDateUtc"]).ToLocalTime();


                    if (band.Id < MINIMUM_BAND_INDEX)
                    {
                        band.MacString = String.Empty;
                    }

                    string i = ListsConverter.ToString(((BandDTO)band).Parameters);

                    list.Add(band);
                }
            }

            return list;
        }

        public struct Result
        {
            public int ResultId { get; set; }
            public string ErrorCode { get; set; }
            public string Error { get; set; }
        }



        public string TransmitterConfigurationSet(TransmitterDTO transmitter)
        {
            const string createTransmittersProc = "EXEC [dbo].[dev_TransmitterConfigurationSet] :TransmitterId,:Mac,:BarCode,:Name,:TypeId,:ReportIntervalSec,:Sensitivity,:AvgCalcTimeS,:SwitchDelayS,:IsBandUpdater,:Msisdn,:Description,:RssiTreshold,:Group,:IpAddress,:Transitions,:OperatorId,:Reason";

            IList<Hashtable> results = null;
            string result = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                results = session.CreateSQLQuery(createTransmittersProc)
                    .SetInt32("TransmitterId", transmitter.Id)
                    .SetInt64("Mac", transmitter.Mac)
                    .SetString("BarCode", transmitter.BarCode)
                    .SetString("Name", transmitter.Name)
                    .SetInt32("TypeId", transmitter.Type)
                    .SetInt32("ReportIntervalSec", transmitter.ReportIntervalSec)
                    .SetInt32("Sensitivity", transmitter.Sensitivity)
                    .SetInt32("AvgCalcTimeS", transmitter.AvgCalcTimeS)
                    .SetInt32("SwitchDelayS", transmitter.SwitchDelayS)
                    .SetBoolean("IsBandUpdater", transmitter.IsBandUpdater)
                    .SetString("Msisdn", transmitter.MSISDN)
                    .SetString("Description", transmitter.Description)
                    .SetInt16("RssiTreshold", transmitter.RssiTreshold)
                    .SetByte("Group", transmitter.Group)
                    .SetString("IpAddress", transmitter.IpAddress)
                    .SetString("Transitions", transmitter.Transitions)
                    .SetInt32("OperatorId", BloodyUser.Current.ID)
                    .SetString("Reason", String.Empty)
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();

                if (results != null)
                    if (results[0] != null)
                        result += results[0]["ErrorMessage"];

                return result;
            }
        }

        public string SetBandConfiguration(BandDTO band)
        {
            const string createBandProc = "EXEC [dbo].[dev_BandConfigurationSet] :BandId,:Mac,:Uid ,:BarCode,:SerialNumber,:TransmissionPower,:AccPercent,:SensitivityPercent"
                + ",:BroadcastingIntervalMs,:BatteryTypeId,:BatteryInstallationDateUtc,:Description,:Parameters,:UnitId,:OperatorId,:Reason";

            IList<Hashtable> results = null;
            string result = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                results = session.CreateSQLQuery(createBandProc)
                .SetInt32("BandId", band.Id)
                .SetInt64("Mac", band.Mac)
                .SetInt64("Uid", band.Uid)
                .SetString("BarCode", band.BarCode)
                .SetString("SerialNumber", band.SerialNumber)
                .SetInt16("TransmissionPower", band.TransmissionPower)
                .SetByte("AccPercent", band.AccPercent)
                .SetByte("SensitivityPercent", band.SensitivityPercent)
                .SetInt32("BroadcastingIntervalMs", band.BroadcastingIntervalMs)
                .SetByte("BatteryTypeId", band.BatteryTypeId)
                .SetDateTime("BatteryInstallationDateUtc", band.BatteryInstallationDateUtc.ToUniversalTime())
                .SetString("Description", band.Description)
                .SetString("Parameters", ListsConverter.ToString(band.Parameters))
                .SetInt32("UnitId", band.UnitId)
                .SetInt32("OperatorId", BloodyUser.Current.ID)
                .SetString("Reason", band.Reason)
                .SetResultTransformer(Transformers.AliasToEntityMap)
                .List<Hashtable>();

                if (results != null)
                    if (results[0] != null)
                        result += results[0]["errorStr"];

                return result;
            }
        }


        public string AssignBandUsingBarCode(string barCode, byte assignForce)
        {
            const string AssignBandProc = "EXEC [dbo].[dev_BandUsingBarCodeAssign] :BarCode,:AssignForce";

            IList<Hashtable> results = null;
            string result;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                results = session.CreateSQLQuery(AssignBandProc)
                    .SetString("BarCode", barCode)
                    .SetByte("AssignForce", assignForce)
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            result = results.GetStringResult("Result", "ErrorMessage");

            return result;
        }

        public string AssignTransmitterToUnit(string macStr, int unitId, string reason)
        {
            const string AssignTransmitterProc = "EXEC [dbo].[ev_TransmitterAssignToUnit] :MacStr,:UnitId,:OperatorId,:Reason";

            IList<Hashtable> results = null;
            string result;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                var query = session.CreateSQLQuery(AssignTransmitterProc);
                query.SetString("MacStr", macStr);
                query.SetInt32("UnitId", unitId);
                query.SetInt32("OperatorId", BloodyUser.Current.ID);
                query.SetString("Reason", reason);

                results = query.SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            result = results.GetStringResult("Result", "ErrorMessage");

            return result;
        }

        public string AssignPatientToWardRoom(PatientDTO patient, string code, string codeType)
        {
            const string proc =
                "EXEC [dbo].[ev_AssignPatientToWardRoom] :PatientId,:CodeType,:Code,:NewWardId,:NewRoomId,:OperatorId,:Reason";

            IList<Hashtable> results = null;
            string result;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                var query = session.CreateSQLQuery(proc);
                query.SetInt32("PatientId", null);
                query.SetString("CodeType", codeType);
                query.SetString("Code", code != null ? code : patient.Pesel);
                query.SetInt32("NewWardId", patient.DepartmentId == -1 ? (int?)null : patient.DepartmentId);
                query.SetInt32("NewRoomId", patient.RoomId == -1 ? (int?)null : patient.RoomId);
                query.SetInt32("OperatorId", BloodyUser.Current.ID);
                query.SetString("Reason", patient.Reason);

                results = query.SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }
            result = results.GetStringResult("Result", "ErrorMessage");

            return result != null ? result : string.Empty;
        }

        public string EditPatient(PatientDTO patient)
        {
            const string proc =
                "EXEC [dbo].[ev_PatientEdit] :PatientId,:Name,:LastName,:Pesel,:OperatorId,:Reason,:OperationDateUtc";

            string result = String.Empty;
            IList<Hashtable> results = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                var query = session.CreateSQLQuery(proc);
                query.SetString("PatientId", patient.ID.ToString());
                query.SetString("Name", patient.FirstName);
                query.SetString("LastName", patient.LastName);
                query.SetString("Pesel", patient.Pesel);
                query.SetInt32("OperatorId", BloodyUser.Current.ID);
                query.SetString("Reason", patient.Reason);
                query.SetDateTime("OperationDateUtc", null);

                results = query.SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }
            result = results.GetStringResult("Result", "ErrorMessage");

            return result;
        }

        public string PatientFromHospital(int patientId)
        {
            const string proc =
                "EXEC [dbo].[ev_PatientFromHospital] :PatientId,:OperatorId,:Reason";

            string result = String.Empty;
            IList<Hashtable> results = null;
            int bloodyId = BloodyUser.Current.ID;
            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                var query = session.CreateSQLQuery(proc);
                query.SetInt32("PatientId", patientId);
                query.SetInt32("OperatorId", bloodyId);
                query.SetString("Reason", "Pacjent wypisany przez użytkownika " + BloodyUser.Current.Auth_Login);

                results = query.SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            result = results.GetStringResult("Result", "ErrorMessage");
            return result;
        }

        /// <summary>
        /// TODO: Dodać procedurę edycji pacjenta.
        /// </summary>
        /// <param name="patient"></param>
        /// <returns></returns>
        public ResultInfoDto PatientToHospital(PatientDTO patient)
        {
            const string proc =
                "EXEC [dbo].[ev_PatientToHospital] :Name,:LastName,:Pesel,:NewWardId,:NewRoomId,:BandId,:BandCode,:OperatorId,:Reason,:ForceAction,:OperationDateUtc";

            string result = String.Empty;
            IList<Hashtable> results = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                var query = session.CreateSQLQuery(proc);
                query.SetString("Name", patient.FirstName);
                query.SetString("LastName", patient.LastName);
                query.SetString("Pesel", patient.Pesel);
                query.SetInt32("NewWardId", patient.DepartmentId);
                query.SetInt32("NewRoomId", patient.RoomId);
                query.SetInt16("BandId", patient.BandId);
                query.SetString("BandCode", patient.BandBarCode);
                query.SetInt32("OperatorId", BloodyUser.Current.ID);
                query.SetString("Reason", patient.Reason);
                query.SetByte("ForceAction", patient.ForceAction);
                query.SetDateTime("OperationDateUtc", null);

                results = query.SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }
            //result = results.GetStringResult("Result", "ErrorCode");
            return new ResultInfoDto() { Result = Convert.ToString(results[0]["Result"]), ErrorMessage = Convert.ToString(results[0]["ErrorMessage"]), ErrorCode = Convert.ToString(results[0]["ErrorCode"]) };
        }

        public string AssignBandToObject(AssignedBandInfoDto dto, bool force = false)
        {
            const string proc =
                "EXEC [dbo].[ev_AssignBandToObject] :OperatorId,:OperationDate,:Reason,:BandId,:BarCode,:ObjectId,:ObjectType,:IsExternalEvent,:ForceAssign";

            string result = String.Empty;
            IList<Hashtable> results = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                var query = session.CreateSQLQuery(proc);
                query.SetInt32("OperatorId", dto.OperatorId);
                query.SetDateTime("OperationDate", null);
                query.SetString("Reason", dto.Reason);
                query.SetInt16("BandId", dto.BandId);
                query.SetString("BarCode", dto.BarCode);
                query.SetInt32("ObjectId", dto.ObjectId);
                query.SetByte("ObjectType", dto.ObjectType);
                query.SetBoolean("IsExternalEvent", null);
                query.SetBoolean("ForceAssign", force);

                results = query.SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            result = results.GetStringResult("Result", "ErrorMessage");
            return result;
        }

        public string SetLocalizationConfiguration(LocalizationConfigDTO localizationConfig)
        {
            const string createLocalizationConfigProc = "EXEC [dbo].[al_UnitAlarmConfigSet] :UnitId,:IsSOScalling,:IsACCActive,:DMoveWrActive,:DMoveWrMins,:DMoveAlActive,:DMoveAlMins,:DNoMoveWrActive,:DNoMoveWrMins"
                + ",:DNoMoveAlActive,:DNoMoveAlMins,:NMoveWrActive,:NMoveWrMins,:NMoveAlActive,:NMoveAlMins,:NNoMoveWrActive,:NNoMoveWrMins,:NNoMoveAlActive,:NNoMoveAlMins,:OutOfZoneWrActive,:OutOfZoneWrMins,:OutOfZoneAlActive,:OutOfZoneAlMins,:IsNoGoZone,:InNoGoZoneMins,:NoConnectWrActive,:NoConnectWrMins,:NoConnectAlActive,:NoConnectAlMins,:NoTransmitterWrActive,:NoTransmitterWrMins,:NoTransmitterAlActive,:NoTransmitterAlMins,:MoveSettingsPriority,:CallingList,:NoGoZones,:OperatorId,:Reason";

            string result = String.Empty;
            IList<Hashtable> results = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                var query = session.CreateSQLQuery(createLocalizationConfigProc);
                query.SetInt32("UnitId", localizationConfig.Id);
                query.SetBoolean("IsSOScalling", localizationConfig.IsAlarmSOS);
                query.SetBoolean("IsACCActive", localizationConfig.IsAlarmFromAccelerometer);
                query.SetBoolean("DMoveWrActive", localizationConfig.IsSendWarnings_MoveDay);
                query.SetInt16("DMoveWrMins", Convert.ToInt16(localizationConfig.SendWarningsAfter_MoveDay));
                query.SetBoolean("DMoveAlActive", localizationConfig.IsSendAlarm_MoveDay);
                query.SetInt16("DMoveAlMins", Convert.ToInt16(localizationConfig.SendAlarmAfter_MoveDay));
                query.SetBoolean("DNoMoveWrActive", localizationConfig.IsSendWarnings_NotMoveDay);
                query.SetInt16("DNoMoveWrMins", Convert.ToInt16(localizationConfig.SendWarningsAfter_NotMoveDay));
                query.SetBoolean("DNoMoveAlActive", localizationConfig.IsSendAlarm_NotMoveDay);
                query.SetInt16("DNoMoveAlMins", Convert.ToInt16(localizationConfig.SendAlarmAfter_NotMoveDay));
                query.SetBoolean("NMoveWrActive", localizationConfig.IsSendWarnings_MoveNight);
                query.SetInt16("NMoveWrMins", Convert.ToInt16(localizationConfig.SendWarningsAfter_MoveNight));
                query.SetBoolean("NMoveAlActive", localizationConfig.IsSendAlarm_MoveNight);
                query.SetInt16("NMoveAlMins", Convert.ToInt16(localizationConfig.SendAlarmAfter_MoveNight));
                query.SetBoolean("NNoMoveWrActive", localizationConfig.IsSendWarnings_NotMoveNight);
                query.SetInt16("NNoMoveWrMins", Convert.ToInt16(localizationConfig.SendWarningsAfter_NotMoveNight));
                query.SetBoolean("NNoMoveAlActive", localizationConfig.IsSendAlarm_NotMoveNight);
                query.SetInt16("NNoMoveAlMins", Convert.ToInt16(localizationConfig.SendAlarmAfter_NotMoveNight));
                query.SetBoolean("OutOfZoneWrActive", localizationConfig.IsSendWarning_PatientOutRoom);
                query.SetInt16("OutOfZoneWrMins", Convert.ToInt16(localizationConfig.SendWarning_PatientOutRoomValue));
                query.SetBoolean("OutOfZoneAlActive", localizationConfig.IsSendAllarm_PatientOutRoom);
                query.SetInt16("OutOfZoneAlMins", Convert.ToInt16(localizationConfig.SendAlarm_PatientOutRoomValue));
                query.SetBoolean("IsNoGoZone", localizationConfig.IsProhibitedZone);
                query.SetInt16("InNoGoZoneMins", Convert.ToInt16(localizationConfig.SendAlarm_PatientInProhibitedZone));
                query.SetBoolean("NoConnectWrActive", localizationConfig.IsConnectionLost_SendWarning);
                query.SetInt16("NoConnectWrMins", Convert.ToInt16(localizationConfig.ConnectionLost_SendWarning));
                query.SetBoolean("NoConnectAlActive", localizationConfig.IsConnectionLost_SendAlarm);
                query.SetInt16("NoConnectAlMins", Convert.ToInt16(localizationConfig.ConnectionLost_SendAlarm));
                query.SetBoolean("NoTransmitterWrActive", localizationConfig.NoTransmitterWrActive);
                query.SetInt16("NoTransmitterWrMins", localizationConfig.NoTransmitterWrMins);
                query.SetBoolean("NoTransmitterAlActive", localizationConfig.NoTransmitterAlActive);
                query.SetInt16("NoTransmitterAlMins", localizationConfig.NoTransmitterAlMins);
                query.SetBoolean("MoveSettingsPriority", localizationConfig.MoveSettingsPriority);
                query.SetString("CallingList", localizationConfig.PhoneNumbers.ToStringQuery());
                query.SetString("NoGoZones", localizationConfig.ProhibitedZonesList.ToStringQuery());
                query.SetInt32("OperatorId", BloodyUser.Current.ID);
                query.SetString("Reason", localizationConfig.Reason);

                results = query.SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
                //TODO: Set Values to query.
            }

            result = results.GetStringResult("Result", "ErrorMessage");
            return result;
        }

        public string SetPatientConfiguration(PatientConfigurationDTO patientConfig)
        {
            const string createPatientConfigProc = "EXEC [dbo].[al_PatientAlarmConfigSet] :PatientId,:IsSOScalling,:IsACCActive,:DMoveWrActive,:DMoveWrMins,:DMoveAlActive,:DMoveAlMins,:DNoMoveWrActive,:DNoMoveWrMins"
                 + ",:DNoMoveAlActive,:DNoMoveAlMins,:NMoveWrActive,:NMoveWrMins,:NMoveAlActive,:NMoveAlMins,:NNoMoveWrActive,:NNoMoveWrMins,:NNoMoveAlActive,:NNoMoveAlMins,:OutOfZoneWrActive,:OutOfZoneWrMins,:OutOfZoneAlActive,:OutOfZoneAlMins,:InNoGoZoneMins,:NoConnectWrActive,:NoConnectWrMins,:NoConnectAlActive,:NoConnectAlMins,:CallingList,:NoGoZones,:OperatorId,:Reason";


            string result = String.Empty;
            IList<Hashtable> results = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                var query = session.CreateSQLQuery(createPatientConfigProc);
                query.SetInt32("PatientId", patientConfig.PatientId);
                query.SetBoolean("IsSOScalling", patientConfig.IsAlarmSOS);
                query.SetBoolean("IsACCActive", patientConfig.IsAlarmFromAccelerometer);
                query.SetBoolean("DMoveWrActive", patientConfig.IsSendWarnings_MoveDay);
                query.SetInt16("DMoveWrMins", Convert.ToInt16(patientConfig.SendWarningsAfter_MoveDay));
                query.SetBoolean("DMoveAlActive", patientConfig.IsSendAlarm_MoveDay);
                query.SetInt16("DMoveAlMins", Convert.ToInt16(patientConfig.SendAlarmAfter_MoveDay));
                query.SetBoolean("DNoMoveWrActive", patientConfig.IsSendWarnings_NotMoveDay);
                query.SetInt16("DNoMoveWrMins", Convert.ToInt16(patientConfig.SendWarningsAfter_NotMoveDay));
                query.SetBoolean("DNoMoveAlActive", patientConfig.IsSendAlarm_NotMoveDay);
                query.SetInt16("DNoMoveAlMins", Convert.ToInt16(patientConfig.SendAlarmAfter_NotMoveDay));
                query.SetBoolean("NMoveWrActive", patientConfig.IsSendWarnings_MoveNight);
                query.SetInt16("NMoveWrMins", Convert.ToInt16(patientConfig.SendWarningsAfter_MoveNight));
                query.SetBoolean("NMoveAlActive", patientConfig.IsSendAlarm_MoveNight);
                query.SetInt16("NMoveAlMins", Convert.ToInt16(patientConfig.SendAlarmAfter_MoveNight));
                query.SetBoolean("NNoMoveWrActive", patientConfig.IsSendWarnings_NotMoveNight);
                query.SetInt16("NNoMoveWrMins", Convert.ToInt16(patientConfig.SendWarningsAfter_NotMoveNight));
                query.SetBoolean("NNoMoveAlActive", patientConfig.IsSendAlarm_NotMoveNight);
                query.SetInt16("NNoMoveAlMins", Convert.ToInt16(patientConfig.SendAlarmAfter_NotMoveNight));
                query.SetBoolean("OutOfZoneWrActive", patientConfig.IsSendWarning_PatientOutRoom);
                query.SetInt16("OutOfZoneWrMins", Convert.ToInt16(patientConfig.SendWarning_PatientOutRoomValue));
                query.SetBoolean("OutOfZoneAlActive", patientConfig.IsSendAllarm_PatientOutRoom);
                query.SetInt16("OutOfZoneAlMins", Convert.ToInt16(patientConfig.SendAlarm_PatientOutRoomValue));
                query.SetInt16("InNoGoZoneMins", Convert.ToInt16(patientConfig.SendAlarm_PatientInProhibitedZone));
                query.SetBoolean("NoConnectWrActive", patientConfig.IsConnectionLost_SendWarning);
                query.SetInt16("NoConnectWrMins", Convert.ToInt16(patientConfig.ConnectionLost_SendWarning));
                query.SetBoolean("NoConnectAlActive", patientConfig.IsConnectionLost_SendAlarm);
                query.SetInt16("NoConnectAlMins", Convert.ToInt16(patientConfig.ConnectionLost_SendAlarm));
                query.SetString("CallingList", patientConfig.PhoneNumbers.ToStringQuery());
                query.SetString("NoGoZones", patientConfig.ProhibitedZonesList.ToStringQuery());
                query.SetInt32("OperatorId", BloodyUser.Current.ID);
                query.SetString("Reason", patientConfig.Reason);

                results = query.SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
                //TODO: Set Values to query.
            }

            result = results.GetStringResult("Result", "ErrorMessage");
            return result;
        }

        public LocalizationConfigDTO GetLocalizationConfigForUnit(int UnitId)
        {
            const string proc = "EXEC [dbo].[al_UnitAlarmConfigGet] :UnitId";

            LocalizationConfigDTO dto = new LocalizationConfigDTO();
            IList<Hashtable> results = null;

            short _temp;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                results = session.CreateSQLQuery(proc)
                    .SetInt32("UnitId", UnitId)
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            if (results != null)
            {
                foreach (var item in results)
                {
                    LocalizationConfigDTO unitConfig = new LocalizationConfigDTO()
                    {
                        Id = Convert.ToInt16(item["UnitId"]),
                        Mac = Convert.ToInt64(item["Mac"]),
                        MacString = Convert.ToString(item["MacString"]),
                        IsAlarmSOS = Convert.ToBoolean(item["IsSOScalling"]),
                        IsAlarmFromAccelerometer = Convert.ToBoolean(item["IsACCactive"]),
                        IsSendWarnings_MoveDay = Convert.ToBoolean(item["DMoveWrActive"]),
                        SendWarningsAfter_MoveDay = Convert.ToInt32(item["DMoveWrMins"]),
                        IsSendWarnings_NotMoveDay = Convert.ToBoolean(item["DNoMoveWrActive"]),
                        SendWarningsAfter_NotMoveDay = Convert.ToInt32(item["DNoMoveWrMins"]),
                        IsSendAlarm_MoveDay = Convert.ToBoolean(item["DMoveAlActive"]),
                        SendAlarmAfter_MoveDay = Convert.ToInt32(item["DMoveAlMins"]),
                        IsSendAlarm_NotMoveDay = Convert.ToBoolean(item["DNoMoveAlActive"]),
                        SendAlarmAfter_NotMoveDay = Convert.ToInt32(item["DNoMoveAlMins"]),
                        IsSendWarnings_MoveNight = Convert.ToBoolean(item["NMoveWrActive"]),
                        SendWarningsAfter_MoveNight = Convert.ToInt32(item["NMoveWrMins"]),
                        IsSendAlarm_MoveNight = Convert.ToBoolean(item["NMoveAlActive"]),
                        SendAlarmAfter_MoveNight = Convert.ToInt32(item["NMoveAlMins"]),
                        IsSendAlarm_NotMoveNight = Convert.ToBoolean(item["NNoMoveAlActive"]),
                        SendAlarmAfter_NotMoveNight = Convert.ToInt32(item["NNoMoveAlMins"]),
                        IsSendWarning_PatientOutRoom = Convert.ToBoolean(item["OutOfZoneWrActive"]),
                        SendWarning_PatientOutRoomValue = Convert.ToInt32(item["OutOfZoneWrMins"]),
                        IsSendAllarm_PatientOutRoom = Convert.ToBoolean(item["OutOfZoneAlActive"]),
                        SendAlarm_PatientOutRoomValue = Convert.ToInt32(item["OutOfZoneAlMins"]),
                        SendAlarm_PatientInProhibitedZone = Convert.ToInt32(item["InNoGoZoneMins"]),
                        IsConnectionLost_SendWarning = Convert.ToBoolean(item["NoConnectWrActive"]),
                        ConnectionLost_SendWarning = Convert.ToInt32(item["NoConnectWrMins"]),
                        IsConnectionLost_SendAlarm = Convert.ToBoolean(item["NoConnectAlActive"]),
                        ConnectionLost_SendAlarm = Convert.ToInt32(item["NoConnectAlMins"]),
                        NoTransmitterWrActive = Convert.ToBoolean(item["NoTransmitterWrActive"]),
                        SendWarningsAfter_NotMoveNight = Convert.ToInt16(item["NNoMoveWrMins"]),
                        NoTransmitterWrMins = Convert.ToInt16(item["NoTransmitterWrMins"]),
                        NoTransmitterAlActive = Convert.ToBoolean(item["NoTransmitterAlActive"]),
                        NoTransmitterAlMins = Convert.ToInt16(item["NoTransmitterAlMins"]),
                        IsSendWarnings_NotMoveNight = Convert.ToBoolean(item["NNoMoveWrActive"]),
                        PhoneNumbers = Convert.ToString(item["CallingGroup"]).QueryContactToList(),
                        ProhibitedZonesList = Convert.ToString(item["NoGoZones"]).QueryProhibitedZoneToList(),
                        TransmitterId = item["TransmitterId"] != null ? Convert.ToInt16(item["TransmitterId"]) : (short?)null,
                        IsProhibitedZone = Convert.ToBoolean(item["IsNoGoZone"]),
                        MoveSettingsPriority = Convert.ToBoolean(item["MoveSettingsPriority"]),
                        Type = Convert.ToByte(item["TypeId"]),
                        MSISDN = Convert.ToString(item["Msisdn"])
                    };

                    dto = unitConfig;

                    if (unitConfig.Id < MINIMUM_BAND_INDEX)
                    {
                        unitConfig.MacString = String.Empty;
                    }
                }
            }

            return dto;
        }

        public PatientConfigurationDTO GetPatientConfiguration(int PatientId)
        {
            const string proc = "EXEC [dbo].[al_PatientAlarmConfigGet] :PatientId";

            PatientConfigurationDTO dto = new PatientConfigurationDTO();
            IList<Hashtable> results = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                results = session.CreateSQLQuery(proc)
                    .SetInt32("PatientId", PatientId)
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            if (results != null)
            {
                foreach (var item in results)
                {
                    PatientConfigurationDTO patientConfig = new PatientConfigurationDTO()
                    {
                        Id = Convert.ToInt16(item["Id"]),
                        IsAlarmSOS = Convert.ToBoolean(item["IsSOScalling"]),
                        IsAlarmFromAccelerometer = Convert.ToBoolean(item["IsACCactive"]),
                        IsSendWarnings_MoveDay = Convert.ToBoolean(item["DMoveWrActive"]),
                        SendWarningsAfter_MoveDay = Convert.ToInt32(item["DMoveWrMins"]),
                        IsSendWarnings_NotMoveDay = Convert.ToBoolean(item["DNoMoveWrActive"]),
                        SendWarningsAfter_NotMoveDay = Convert.ToInt32(item["DNoMoveWrMins"]),
                        IsSendAlarm_MoveDay = Convert.ToBoolean(item["DMoveAlActive"]),
                        SendAlarmAfter_MoveDay = Convert.ToInt32(item["DMoveAlMins"]),
                        IsSendAlarm_NotMoveDay = Convert.ToBoolean(item["DNoMoveAlActive"]),
                        SendAlarmAfter_NotMoveDay = Convert.ToInt32(item["DNoMoveAlMins"]),
                        IsSendWarnings_MoveNight = Convert.ToBoolean(item["NMoveWrActive"]),
                        SendWarningsAfter_MoveNight = Convert.ToInt32(item["NMoveWrMins"]),
                        IsSendAlarm_MoveNight = Convert.ToBoolean(item["NMoveAlActive"]),
                        SendAlarmAfter_MoveNight = Convert.ToInt32(item["NMoveAlMins"]),
                        IsSendAlarm_NotMoveNight = Convert.ToBoolean(item["NNoMoveAlActive"]),
                        SendAlarmAfter_NotMoveNight = Convert.ToInt32(item["NNoMoveAlMins"]),
                        IsSendWarning_PatientOutRoom = Convert.ToBoolean(item["OutOfZoneWrActive"]),
                        SendWarning_PatientOutRoomValue = Convert.ToInt32(item["OutOfZoneWrMins"]),
                        IsSendAllarm_PatientOutRoom = Convert.ToBoolean(item["OutOfZoneAlActive"]),
                        SendAlarm_PatientOutRoomValue = Convert.ToInt32(item["OutOfZoneAlMins"]),
                        SendAlarm_PatientInProhibitedZone = Convert.ToInt32(item["InNoGoZoneMins"]),
                        IsConnectionLost_SendWarning = Convert.ToBoolean(item["NoConnectWrActive"]),
                        ConnectionLost_SendWarning = Convert.ToInt32(item["NoConnectWrMins"]),
                        IsConnectionLost_SendAlarm = Convert.ToBoolean(item["NoConnectAlActive"]),
                        ConnectionLost_SendAlarm = Convert.ToInt32(item["NoConnectAlMins"]),
                        IsSendWarnings_NotMoveNight = Convert.ToBoolean(item["NNoMoveWrActive"]),
                        PatientId = Convert.ToInt32(item["PatientId"]),
                        PhoneNumbers = Convert.ToString(item["CallingGroup"]).QueryContactToList(),
                        ProhibitedZonesList = Convert.ToString(item["NoGoZones"]).QueryProhibitedZoneToList(),
                        SendWarningsAfter_NotMoveNight = Convert.ToInt32(item["NNoMoveWrMins"])
                    };
                    dto = patientConfig;
                }
            }

            return dto;
        }


        const string FILTERED_FROM = "From";
        const string FILTERED_TO = "To";

        //public IList<PatientInfoDto> GetPatientInfosFiltered(IDictionary<string, string> filters, int pageNum, int pageSize, int unit, int unitSource)
        //{
        //const string sql = "exec dbo.fda_GetPatientInfosFiltered :PageNum,:PageSize,:TreeUnitIds,:IdFrom,:IdTo,:BarCode,:MacString,:UidFrom,:UidTo,:BatteryTypeStr,:BatteryAlarmSeverityIds,:BatteryAlarmSeverity,:BatteryInstallationDateFrom,:BatteryInstallationDateTo,:BatteryDaysToExchangeFrom,:BatteryDaysToExchangeTo,:Description,:ObjectTypeIds,:ObjectType,:ObjectDisplayName,:UnitIds,:BandUnitName,:BandUnitLocation,:CurrentUnitIds,:CurrentUnitName,:CurrentUnitLocation,:SortOrder";
        //}

        public IList<PatientInfoDto> GetPatients()
        {
            const string SQL = "SELECT * from Patients";

            IList<PatientInfoDto> output = new List<PatientInfoDto>();

            /*IList<Hashtable> results = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                results = s.CreateSQLQuery(SQL)
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            if (results != null)
            {
                foreach (var item in results)
                {
                    PatientInfoDto patient = new PatientInfoDto()
                    {
                        Id = Convert.ToInt16(item["Id"]),
                        FirstName = Convert.ToString(item["Name"]),
                        LastName = Convert.ToString(item["LastName"]),
                        Pesel = Convert.ToInt32(item["Pesel"]),
                        HospitalAdmissionDate = Convert.ToDateTime(item["HospitalDateUtc"]),
                        Department = Convert.ToString(item["WardId"]),
                        DepartmentAdmissionDate = Convert.ToDateTime(item["WardDateUtc"]),
                        Room = Convert.ToString(item["RoomId"]),
                        RoomAdmissionDate = Convert.ToDateTime(item["RoomDateUtc"])
                    };

                    output.Add(patient);
                }
            }*/

            return output;
        }

        public IList<ZonesInfoDto> ZonesGet()
        {
            const string sql = "exec dbo.unt_ZonesGet";

            IList<ZonesInfoDto> outputs = new List<ZonesInfoDto>();

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var results = s.CreateSQLQuery(sql)
                    .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<ZonesInfoDto>())
                    .List<ZonesInfoDto>();

                outputs = results;
            }

            return outputs;
        }

        public string PatientCreate(PatientDTO patient)
        {
            const string CREATE_PROCEDURE = "EXEC [dbo].[test_PatientSet] :Id,"
                                                                         + ":FirstName,"
                                                                         + ":LastName,"
                                                                         + ":Pesel,"
                                                                         + ":Department,"
                                                                         + ":DepartmentPath,"
                                                                         + ":Room,"
                                                                         + ":RoomPath,"
                                                                         + ":BandId";

            IList<Hashtable> results = null;
            string result = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                //results = session.CreateSQLQuery(CREATE_PROCEDURE)
                //    .SetInt16("Id", patient.ID);

            }

            return string.Empty;
        }

        public IList<ArchivePatientsDto> GetArchivePatientsFiltered(IDictionary<string, string> filters, int pageNum, int pageSize)
        {
            const string sql = "EXEC [dbo].[fda_GetArchiveDataFiltered]"
                + ":PageNum,:PageSize,:TreeUnitIds,:EventIdFrom,:EventIdTo,:PatientIdFrom,:PatientIdTo,:Name,:LastName,:DisplayName,:Pesel,:DateUtcFrom,:DateUtcTo,:TypeFrom,:TypeTo,"
                + ":TypeDescription,:SeverityFrom,:SeverityTo,:SeverityDescription,:UnitName,:UnitLocation,:ObjectId,:ObjectTypeFrom,:ObjectTypeTo,:ObjectTypeDescription,"
                + ":ObjectName,:ObjectUnitId,:ObjectUnitName,:ObjectUnitLocation,:EndDateUtcFrom,:EndDateUtcTo,:EndingEventIdFrom,:EndingEventIdTo,:OperatorIdFrom,"
                + ":OperatorIdTo,:OperatorName,:OperatorLastName,:EventData,:Reason,:RaisingEventId,:IsClosedFrom,:IsClosedTo,:ClosingDateFrom,:ClosingDateTo,"
                + ":ClosingUserIdFrom,:ClosingUserIdTo,:ClosingUserName,:ClosingUserLastName,:ClosingAlarmId,:ClosingComment,:UserMessageId,"
                + ":SenderIdFrom,:SenderIdTo,:SenderName,:SenderLastName,:Message, :StoreDateUtcFrom, :StoreDateUtcTo,:SortOrder";


            //const string sql = "EXEC [dbo].[fda_GetArchivePatientsFiltered] :PageNum, :PageSize, :TreeUnitIds, :EventIdFrom, :EventIdTo, :PatientIdFrom, :PatientIdTo, :Name, :LastName, :DisplayName, :Pesel, :DateUtcFrom, :DateUtcTo, :TypeFrom, :TypeTo, :TypeDescription, :SeverityFrom, :SeverityTo, :SeverityDescription, :UnitNameFrom, :UnitNameTo, :UnitLocation, :ObjectId, :ObjectTypeFrom, :ObjectTypeTo, :ObjectTypeDescriptionFrom, :ObjectTypeDescriptionTo, :ObjectName, :ObjectUnitId, :ObjectUnitNameFrom, :ObjectUnitNameTo, :ObjectUnitLocation, :EndDateUtc, :EndingEventIdFrom, :EndingEventIdTo, :OperatorIdFrom, :OperatorIdTo, :OperatorNameFrom, :OperatorNameTo, :OperatorLastName, :EventData, :Reason, :RaisingEventId, :IsClosedFrom, :IsClosedTo, :ClosingDateFrom, :ClosingDateTo, :ClosingUserIdFrom, :ClosingUserIdTo, :ClosingUserNameFrom, :ClosingUserNameTo, :ClosingUserLastName, :ClosingAlarmId, :ClosingCommentFrom, :ClosingCommentTo, :UserMessageId, :SenderIdFrom, :SenderIdTo, :SenderNameFrom, :SenderNameTo, :SenderLastName, :Message";

            
            QueryHelper.UpdateFilterTreeUnitIds(filters);
            QueryHelper.UpdateFilters(filters, pageNum, pageSize);

            if (filters["TreeUnitIds"][0] == ',')
                filters["TreeUnitIds"] = filters["TreeUnitIds"].Substring(1);

            IList<Hashtable> results = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);

                results = query
                .SetResultTransformer(Transformers.AliasToEntityMap)
                .List<Hashtable>();
            }

            var y = results.ToList<ArchivePatientsDto>();
            return y;
        }

        public int GetArchivePatientsFilteredTotalCount(IDictionary<string, string> filters)
        {
            const string sql = "EXEC [dbo].[fda_GetArchiveDataFilteredTotalCount]"
                + ":TreeUnitIds,:EventIdFrom,:EventIdTo,:PatientIdFrom,:PatientIdTo,:Name,:LastName,:DisplayName,:Pesel,:DateUtcFrom,:DateUtcTo,:TypeFrom,:TypeTo,"
                + ":TypeDescription,:SeverityFrom,:SeverityTo,:SeverityDescription,:UnitName,:UnitLocation,:ObjectId,:ObjectTypeFrom,:ObjectTypeTo,:ObjectTypeDescription,"
                + ":ObjectName,:ObjectUnitId,:ObjectUnitName,:ObjectUnitLocation,:EndDateUtcFrom,:EndDateUtcTo,:EndingEventIdFrom,:EndingEventIdTo,:OperatorIdFrom,"
                + ":OperatorIdTo,:OperatorName,:OperatorLastName,:EventData,:Reason,:RaisingEventId,:IsClosedFrom,:IsClosedTo,:ClosingDateFrom,:ClosingDateTo,"
                + ":ClosingUserIdFrom,:ClosingUserIdTo,:ClosingUserName,:ClosingUserLastName,:ClosingAlarmId,:ClosingComment,:UserMessageId,"
                + ":SenderIdFrom,:SenderIdTo,:SenderName,:SenderLastName,:Message, :StoreDateUtcFrom, :StoreDateUtcTo";


            //const string sql = "EXEC [dbo].[fda_GetArchivePatientsFiltered] :PageNum, :PageSize, :TreeUnitIds, :EventIdFrom, :EventIdTo, :PatientIdFrom, :PatientIdTo, :Name, :LastName, :DisplayName, :Pesel, :DateUtcFrom, :DateUtcTo, :TypeFrom, :TypeTo, :TypeDescription, :SeverityFrom, :SeverityTo, :SeverityDescription, :UnitNameFrom, :UnitNameTo, :UnitLocation, :ObjectId, :ObjectTypeFrom, :ObjectTypeTo, :ObjectTypeDescriptionFrom, :ObjectTypeDescriptionTo, :ObjectName, :ObjectUnitId, :ObjectUnitNameFrom, :ObjectUnitNameTo, :ObjectUnitLocation, :EndDateUtc, :EndingEventIdFrom, :EndingEventIdTo, :OperatorIdFrom, :OperatorIdTo, :OperatorNameFrom, :OperatorNameTo, :OperatorLastName, :EventData, :Reason, :RaisingEventId, :IsClosedFrom, :IsClosedTo, :ClosingDateFrom, :ClosingDateTo, :ClosingUserIdFrom, :ClosingUserIdTo, :ClosingUserNameFrom, :ClosingUserNameTo, :ClosingUserLastName, :ClosingAlarmId, :ClosingCommentFrom, :ClosingCommentTo, :UserMessageId, :SenderIdFrom, :SenderIdTo, :SenderNameFrom, :SenderNameTo, :SenderLastName, :Message";

            QueryHelper.UpdateFilterTreeUnitIds(filters);

            int totalCount = 0;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                totalCount = query.UniqueResult<int>();
            }

            return totalCount;
        }

        public IList<BandInfoDto> GetBandsInfoFiltered(IDictionary<string, string> filters, int pageNum, int pageSize)
        {
            const string sql = "EXEC fda_GetBandsInfoFiltered "
                    + ":PageNum, :PageSize, :TreeUnitIds, :IdFrom, :IdTo, :BarCode, :MacString, "
                    + ":UidFrom, :UidTo, :SerialNumber, :BatteryTypeIdFrom, :BatteryTypeIdTo, :BatteryTypeName, :BatteryAlarmSeverityIdFrom, "
                    + ":BatteryAlarmSeverityIdTo, :BatteryAlarmSeverityName, :BatteryInstallationDateUtcFrom, :BatteryInstallationDateUtcTo, :BatteryDaysToExchangeFrom, :BatteryDaysToExchangeTo, :Description, "
                    + ":ObjectTypeIdFrom, :ObjectTypeIdTo, :ObjectTypeName, :ObjectDisplayName, :UnitIdFrom, :UnitIdTo, :UnitName, "
                    + ":UnitLocation, :CurrentUnitIdFrom, :CurrentUnitIdTo, :CurrentUnitName, :CurrentUnitLocation, :SortOrder";

            QueryHelper.UpdateFilterTreeUnitIds(filters);
            QueryHelper.UpdateFilters(filters, pageNum, pageSize);

            if (filters["TreeUnitIds"][0] == ',')
                filters["TreeUnitIds"] = filters["TreeUnitIds"].Substring(1);

            IList<Hashtable> results = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                results = query
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            return results.ToList<BandInfoDto>();
        }

        public IList<PatientDetailsDto> GetPatientDetails(IDictionary<string, string> filters, int pageNum, int pageSize)
        {
            const string sql = "EXEC fda_GetPatientDetailsFiltered :PageNum, :PageSize, :TreeUnitIds, :IdFrom, :IdTo, :Name, :LastName, :DisplayName, :Pesel, :WardIdFrom, :WardIdTo, :WardUnitName, :WardUnitLocation, :WardAdmissionDateUtcFrom, :WardAdmissionDateUtcTo, :RoomIdFrom,"
                + ":RoomIdTo, :RoomUnitName, :RoomUnitLocation, :RoomAdmissionDateUtcFrom, :RoomAdmissionDateUtcTo, :BandIdFrom, :BandIdTo, :BandBarCode, :BandSerialNumber, :CurrentUnitName, :CurrentUnitLocation, :SortOrder";

            QueryHelper.UpdateFilterTreeUnitIds(filters);
            QueryHelper.UpdateFilters(filters, pageNum, pageSize);

            if (filters["TreeUnitIds"][0] == ',')
                filters["TreeUnitIds"] = filters["TreeUnitIds"].Substring(1);

            IList<PatientDetailsDto> output = new List<PatientDetailsDto>();

            IList<Hashtable> results = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                results = query
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            return results.ToList<PatientDetailsDto>();
        }

        public int GetPatientDetailsFilteredTotalCount(IDictionary<string, string> filters)
        {

            const string sql = "EXEC fda_GetPatientDetailsFilteredTotalCount :TreeUnitIds, :IdFrom , :IdTo , :Name, :LastName, :DisplayName, :Pesel, :WardIdFrom, :WardIdTo, :WardUnitName, :WardUnitLocation, :WardAdmissionDateUtcFrom, :WardAdmissionDateUtcTo"
                               + ", :RoomIdFrom, :RoomIdTo, :RoomUnitName, :RoomUnitLocation, :RoomAdmissionDateUtcFrom, :RoomAdmissionDateUtcTo, :BandIdFrom, :BandIdTo, :BandBarCode, :BandSerialNumber, :CurrentUnitName, :CurrentUnitLocation";

            QueryHelper.UpdateFilterTreeUnitIds(filters);
            if (filters["TreeUnitIds"][0] == ',')
                filters["TreeUnitIds"] = filters["TreeUnitIds"].Substring(1);

            int totalCount = 0;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                totalCount = query.UniqueResult<int>();
                return totalCount;
            }
        }

        public int GetTransmitterDetailsFilteredTotalCount(IDictionary<string, string> filters)
        {
            const string sql = "EXEC [dbo].[fda_GetTransmitterDetailsFilteredTotalCount] :TreeUnitIds, :IdFrom, :IdTo, :MacFrom, :MacTo, :MacString, :ClientIdFrom, :ClientIdTo, :InstallationUnitIdFrom, :InstallationUnitIdTo, :InstallationUnitName, :Name, :BarCode, :TypeIdFrom, :TypeIdTo, :ReportIntervalSecFrom, :ReportIntervalSecTo, :SensitivityFrom, :SensitivityTo, :AvgCalcTimeSFrom, :AvgCalcTimeSTo, :Msisdn, :IsBandUpdaterFrom, :IsBandUpdaterTo, :Description, :RssiTresholdFrom, :RssiTresholdTo, :GroupIdFrom, :GroupIdTo, :GroupName,:IpAddress,:IsConnectedFrom,:IsConnectedTo,:AlarmOngoingFrom,:AlarmOngoingTo";

            QueryHelper.UpdateFilterTreeUnitIds(filters);

            int totalCount = 0;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                totalCount = query.UniqueResult<int>();
            }

            return totalCount;
        }

        public IList<TransmitterDetailsDto> GetTransmitterDetailsFiltered(IDictionary<string, string> filters, int pageNum, int pageSize)
        {
            const string sql = "EXEC [dbo].[fda_GetTransmitterDetailsFiltered] :PageNum, :PageSize, :TreeUnitIds, :IdFrom, :IdTo, :MacFrom, :MacTo, :MacString, :ClientIdFrom, :ClientIdTo, :InstallationUnitIdFrom, :InstallationUnitIdTo, :InstallationUnitName, :Name, :BarCode, :TypeIdFrom, :TypeIdTo, :ReportIntervalSecFrom, :ReportIntervalSecTo, :SensitivityFrom, :SensitivityTo, :AvgCalcTimeSFrom, :AvgCalcTimeSTo, :Msisdn, :IsBandUpdaterFrom, :IsBandUpdaterTo, :Description, :RssiTresholdFrom, :RssiTresholdTo, :GroupIdFrom, :GroupIdTo, :GroupName,:IpAddress,:IsConnectedFrom,:IsConnectedTo,:AlarmOngoingFrom,:AlarmOngoingTo,:SortOrder";

            QueryHelper.UpdateFilters(filters, pageNum, pageSize);

            IList<TransmitterDetailsDto> output = new List<TransmitterDetailsDto>();
            IList<Hashtable> results = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);

                results = query
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }
            return results.ToList<TransmitterDetailsDto>();
        }

        public int GetSystemEventsFilteredTotalCount(IDictionary<string, string> filters)
        {
            string sql = "EXEC fda_GetSystemEventsFilteredTotalCount :IdFrom,:IdTo,:DateUtcFrom,:DateUtcTo,:TypeFrom,:TypeTo,:TypeDescription,:UnitIdFrom,:UnitIdTo,:UnitLocation,:ObjectIdFrom,:ObjectIdTo,:ObjectTypeFrom,:ObjectTypeTo,:ObjectTypeDescription,:ObjectName,:OperatorIdFrom,:OperatorIdTo,:OperatorName,:EventData,:Reason,:FromArchive";

            QueryHelper.UpdateFilterTreeUnitIds(filters);

            int totalCount = 0;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                totalCount = query.UniqueResult<int>();
            }

            return totalCount;
        }

        public IList<LogEntryDto> GetSystemEventsFiltered(IDictionary<string, string> filters, int pageNum, int pageSize)
        {
            string sql = "EXEC fda_GetSystemEventsFiltered :PageNum,:PageSize,:IdFrom,:IdTo,:DateUtcFrom,:DateUtcTo,:TypeFrom,:TypeTo,:TypeDescription,:UnitIdFrom,:UnitIdTo,:UnitLocation,:ObjectIdFrom,:ObjectIdTo,:ObjectTypeFrom,:ObjectTypeTo,:ObjectTypeDescription,:ObjectName,:OperatorIdFrom,:OperatorIdTo,:OperatorName,:EventData,:Reason,:SortOrder,:FromArchive";

            QueryHelper.UpdateFilters(filters, pageNum, pageSize);

            if (filters["TreeUnitIds"][0] == ',')
                filters["TreeUnitIds"] = filters["TreeUnitIds"].Substring(1);

            IList<LogEntryDto> output = new List<LogEntryDto>();

            IList<Hashtable> results = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                results = query
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            return results.ToList<LogEntryDto>();
        }


        public IList<TransmitterEventsDto> GetTransmitterEvents(IDictionary<string, string> filters, int pageNum, int pageSize)
        {
            const string sql = "EXEC fda_GetEventsDetailsFiltered :PageNum, :PageSize, :TreeUnitIds, :IdFrom , :IdTo , :DateUtcFrom, :DateUtcTo, :TypeFrom, :TypeTo, :TypeDescription, :SeverityFrom, :SeverityTo, :SeverityDescription, :UnitIdFrom, :UnitIdTo, :UnitName, :UnitLocation, :ObjectIdFrom, :ObjectIdTo, :ObjectTypeFrom, :ObjectTypeTo, :ObjectTypeDescription, :ObjectName, :ObjectUnitIdFrom, :ObjectUnitIdTo, :ObjectUnitName, :ObjectUnitLocation, :EndDateUtcFrom, :EndDateUtcTo, :EndingEventIdFrom, :EndingEventIdTo, :OperatorIdFrom, :OperatorIdTo, :OperatorName, :OperatorLastName, :EventData, :Reason, :RaisingEventIdFrom, :RaisingEventIdTo, :IsClosedFrom, :IsClosedTo, :ClosingDateFrom, :ClosingDateTo, :ClosingUserIdFrom, :ClosingUserIdTo, :ClosingUserName, :ClosingUserLastName, :ClosingAlarmIdFrom, :ClosingAlarmIdTo, :ClosingComment, :UserMessageIdFrom, :UserMessageIdTo, :SenderIdFrom, :SenderIdTo, :SenderName, :SenderLastName, :Message, :SortOrder";

            QueryHelper.UpdateFilters(filters, pageNum, pageSize);

            if (filters["TreeUnitIds"][0] == ',')
                filters["TreeUnitIds"] = filters["TreeUnitIds"].Substring(1);

            IList<TransmitterEventsDto> output = new List<TransmitterEventsDto>();

            IList<Hashtable> results = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                results = query
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            return results.ToList<TransmitterEventsDto>();
        }

        public int GetTransmitterEventsFilteredTotalCount(IDictionary<string, string> filters)
        {
            const string sql = "EXEC fda_GetEventsDetailsFilteredTotalCount :TreeUnitIds, :IdFrom , :IdTo , :DateUtcFrom, :DateUtcTo, :TypeFrom, :TypeTo, :TypeDescription, :SeverityFrom, :SeverityTo, :SeverityDescription, :UnitIdFrom, :UnitIdTo, :UnitName, :UnitLocation, :ObjectIdFrom, :ObjectIdTo, :ObjectTypeFrom, :ObjectTypeTo, :ObjectTypeDescription, :ObjectName, :ObjectUnitIdFrom, :ObjectUnitIdTo, :ObjectUnitName, :ObjectUnitLocation, :EndDateUtcFrom, :EndDateUtcTo, :EndingEventIdFrom, :EndingEventIdTo, :OperatorIdFrom, :OperatorIdTo, :OperatorName, :OperatorLastName, :EventData, :Reason, :RaisingEventIdFrom, :RaisingEventIdTo, :IsClosedFrom, :IsClosedTo, :ClosingDateFrom, :ClosingDateTo, :ClosingUserIdFrom, :ClosingUserIdTo, :ClosingUserName, :ClosingUserLastName, :ClosingAlarmIdFrom, :ClosingAlarmIdTo, :ClosingComment, :UserMessageIdFrom, :UserMessageIdTo, :SenderIdFrom, :SenderIdTo, :SenderName, :SenderLastName, :Message";

            QueryHelper.UpdateFilterTreeUnitIds(filters);

            int totalCount = 0;

            if (filters["TreeUnitIds"][0] == ',')
                filters["TreeUnitIds"] = filters["TreeUnitIds"].Substring(1);

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                totalCount = query.UniqueResult<int>();
            }

            return totalCount;
        }

        public IList<EventsDetailsDto> GetEventsDetails(IDictionary<string, string> filters, int pageNum, int pageSize)
        {
            const string sql = "EXEC fda_GetEventsDetailsFiltered :PageNum, :PageSize, :TreeUnitIds, :IdFrom , :IdTo , :DateUtcFrom, :DateUtcTo, :TypeFrom, :TypeTo, :TypeDescription, :SeverityFrom, :SeverityTo, :SeverityDescription, :UnitIdFrom, :UnitIdTo, :UnitName, :UnitLocation, :ObjectIdFrom, :ObjectIdTo, :ObjectTypeFrom, :ObjectTypeTo, :ObjectTypeDescription, :ObjectName, :ObjectUnitIdFrom, :ObjectUnitIdTo, :ObjectUnitName, :ObjectUnitLocation, :EndDateUtcFrom, :EndDateUtcTo, :EndingEventIdFrom, :EndingEventIdTo, :OperatorIdFrom, :OperatorIdTo, :OperatorName, :OperatorLastName, :EventData, :Reason, :RaisingEventIdFrom, :RaisingEventIdTo, :IsClosedFrom, :IsClosedTo, :ClosingDateFrom, :ClosingDateTo, :ClosingUserIdFrom, :ClosingUserIdTo, :ClosingUserName, :ClosingUserLastName, :ClosingAlarmIdFrom, :ClosingAlarmIdTo, :ClosingComment, :UserMessageIdFrom, :UserMessageIdTo, :SenderIdFrom, :SenderIdTo, :SenderName, :SenderLastName, :Message, :SortOrder";

            QueryHelper.UpdateFilters(filters, pageNum, pageSize);

            if (filters["TreeUnitIds"][0] == ',')
                filters["TreeUnitIds"] = filters["TreeUnitIds"].Substring(1);

            IList<EventsDetailsDto> output = new List<EventsDetailsDto>();

            IList<Hashtable> results = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                results = query
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            return results.ToList<EventsDetailsDto>();
        }

        public int GetEventsDetailsFilteredTotalCount(IDictionary<string, string> filters)
        {
            const string sql = "EXEC fda_GetEventsDetailsFilteredTotalCount :TreeUnitIds, :IdFrom , :IdTo , :DateUtcFrom, :DateUtcTo, :TypeFrom, :TypeTo, :TypeDescription, :SeverityFrom, :SeverityTo, :SeverityDescription, :UnitIdFrom, :UnitIdTo, :UnitName, :UnitLocation, :ObjectIdFrom, :ObjectIdTo, :ObjectTypeFrom, :ObjectTypeTo, :ObjectTypeDescription, :ObjectName, :ObjectUnitIdFrom, :ObjectUnitIdTo, :ObjectUnitName, :ObjectUnitLocation, :EndDateUtcFrom, :EndDateUtcTo, :EndingEventIdFrom, :EndingEventIdTo, :OperatorIdFrom, :OperatorIdTo, :OperatorName, :OperatorLastName, :EventData, :Reason, :RaisingEventIdFrom, :RaisingEventIdTo, :IsClosedFrom, :IsClosedTo, :ClosingDateFrom, :ClosingDateTo, :ClosingUserIdFrom, :ClosingUserIdTo, :ClosingUserName, :ClosingUserLastName, :ClosingAlarmIdFrom, :ClosingAlarmIdTo, :ClosingComment, :UserMessageIdFrom, :UserMessageIdTo, :SenderIdFrom, :SenderIdTo, :SenderName, :SenderLastName, :Message";

            QueryHelper.UpdateFilterTreeUnitIds(filters);

            int totalCount = 0;

            if (filters["TreeUnitIds"][0] == ',')
                filters["TreeUnitIds"] = filters["TreeUnitIds"].Substring(1);

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                totalCount = query.UniqueResult<int>();
            }

            return totalCount;
        }

        public int GetBandsInfoFilteredTotalCount(IDictionary<string, string> filters)
        {
            const string sql = "EXEC fda_GetBandsInfoFilteredTotalCount "
                    + ":TreeUnitIds, :IdFrom, :IdTo, :BarCode, :MacString, :UidFrom, :UidTo, "
                    + ":SerialNumber, :BatteryTypeIdFrom, :BatteryTypeIdTo, :BatteryTypeName, :BatteryAlarmSeverityIdFrom, :BatteryAlarmSeverityIdTo, :BatteryAlarmSeverityName, "
                    + ":BatteryInstallationDateUtcFrom, :BatteryInstallationDateUtcTo, :BatteryDaysToExchangeFrom, :BatteryDaysToExchangeTo, :Description, :ObjectTypeIdFrom, :ObjectTypeIdTo, "
                    + ":ObjectTypeName, :ObjectDisplayName, :UnitIdFrom, :UnitIdTo, :UnitName, :UnitLocation, :CurrentUnitIdFrom, "
                    + ":CurrentUnitIdTo, :CurrentUnitName, :CurrentUnitLocati";

            QueryHelper.UpdateFilterTreeUnitIds(filters);

            int totalCount = 0;

            if (filters["TreeUnitIds"][0] == ',')
                filters["TreeUnitIds"] = filters["TreeUnitIds"].Substring(1);

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                totalCount = query.UniqueResult<int>();
            }

            return totalCount;
        }

        public IList<BandPreviewDto> BandsForUnitGet(int unitId, int userId)
        {
            List<BandPreviewDto> previews = new List<BandPreviewDto>();


            return previews;
        }

        public IList<BandStatusDto> GetBandsStatuses(long? lastToken)
        {
            const string sql = "EXEC dbo.al_GetBandsStatuses :SynchroToken";

            IList<Hashtable> results = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql)
                    .SetInt64("SynchroToken", lastToken);
                results = query
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            return results.ToList<BandStatusDto>();
        }

        public string GetPatientId(string pesel)
        {
            const string sql = "EXEC dbo.dev_GetPatientId :Pesel";

            string result = String.Empty;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var results = s.CreateSQLQuery(sql)
                    .SetString("Pesel", pesel)
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();

                result = results.GetStringResult("Result", "ErrorMessage");
            }

            return result;
        }

        public string CloseAlarms(EventsDetailsDto alarmToClose)
        {
            const string sql = "EXEC dbo.ev_CloseAlarms :Ids,:Comment,:Userid";

            string result = String.Empty;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var results = s.CreateSQLQuery(sql)
                    .SetString("Ids", alarmToClose.Id.ToString())
                    .SetString("Comment", alarmToClose.Message)
                    .SetString("Userid", BloodyUser.Current.ID.ToString())
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            return result;
        }

        public IList<TransmitterDetailsDto> GetTransmitterConfiguration(int? idTransmitter)
        {
            throw new NotImplementedException();
        }

        public string SetTransmitterConfiguration(TransmitterDetailsDto transmitter)
        {
            throw new NotImplementedException();
        }

        public IList<EventsDetailsDto> GetEventsForUnitsFiltered(IDictionary<string, string> filters, int pageNum, int pageSize)
        {
            const string sql = "EXEC fda_GetEventsForUnitsFiltered :PageNum, :PageSize, :TreeUnitIds, :IdFrom , :IdTo, :DateUtcFrom, :DateUtcTo, :TypeFrom, :TypeTo, :TypeDescription, :SeverityFrom, :SeverityTo, :SeverityDescription, :UnitIdFrom, :UnitIdTo, :UnitName, :UnitLocation, :ObjectIdFrom, :ObjectIdTo, :ObjectTypeFrom, :ObjectTypeTo, :ObjectTypeDescription, :ObjectName, :ObjectUnitIdFrom, :ObjectUnitIdTo, :ObjectUnitName, :ObjectUnitLocation, :EndDateUtcFrom, :EndDateUtcTo, :EndingEventIdFrom, :EndingEventIdTo, :OperatorIdFrom, :OperatorIdTo, :OperatorName, :OperatorLastName, :EventData, :Reason, :RaisingEventIdFrom, :RaisingEventIdTo , :IsClosedFrom, :IsClosedTo, :ClosingDateUtcFrom, :ClosingDateUtcTo, :ClosingUserIdFrom, :ClosingUserIdTo, :ClosingUserName, :ClosingUserLastName, :ClosingAlarmIdFrom, :ClosingAlarmIdTo, :ClosingComment, :UserMessageIdFrom, :UserMessageIdTo, :SenderIdFrom, :SenderIdTo, :SenderName, :SenderLastName, :Message, :SortOrder";

            QueryHelper.UpdateFilterTreeUnitIds(filters);
            QueryHelper.UpdateFilters(filters, pageNum, pageSize);        

            if (filters["TreeUnitIds"][0] == ',')
                filters["TreeUnitIds"] = filters["TreeUnitIds"].Substring(1);

            IList<EventsDetailsDto> output = new List<EventsDetailsDto>();

            IList<Hashtable> results = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                results = query
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            return results.ToList<EventsDetailsDto>();
        }

        public int GetEventsForUnitsFilteredTotalCount(IDictionary<string, string> filters)
        {
            const string sql = "EXEC fda_GetEventsForUnitsFilteredTotalCount :TreeUnitIds, :IdFrom , :IdTo , :DateUtcFrom, :DateUtcTo, :TypeFrom, :TypeTo, :TypeDescription, :SeverityFrom, :SeverityTo, :SeverityDescription,  :UnitIdFrom, :UnitIdTo, :UnitName, :UnitLocation, :ObjectIdFrom, :ObjectIdTo, :ObjectTypeFrom, :ObjectTypeTo, :ObjectTypeDescription, :ObjectName, :ObjectUnitIdFrom, :ObjectUnitIdTo, :ObjectUnitName, :ObjectUnitLocation, :EndDateUtcFrom, :EndDateUtcTo, :EndingEventIdFrom, :EndingEventIdTo, :OperatorIdFrom, :OperatorIdTo, :OperatorName, :OperatorLastName, :EventData, :Reason, :RaisingEventIdFrom, :RaisingEventIdTo, :IsClosedFrom, :IsClosedTo, :ClosingDateFrom, :ClosingDateTo, :ClosingUserIdFrom, :ClosingUserIdTo, :ClosingUserName, :ClosingUserLastName, :ClosingAlarmIdFrom, :ClosingAlarmIdTo, :ClosingComment, :UserMessageIdFrom, :UserMessageIdTo, :SenderIdFrom, :SenderIdTo, :SenderName, :SenderLastName, :Message";

            QueryHelper.UpdateFilterTreeUnitIds(filters);

            int totalCount = 0;

            if (filters["TreeUnitIds"][0] == ',')
                filters["TreeUnitIds"] = filters["TreeUnitIds"].Substring(1);

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                totalCount = query.UniqueResult<int>();
            }

            return totalCount;
        }

        public LogEntryExtendedDto GetSystemEventStrings(int eventId)
        {
            string proc = "EXEC [dbo].[ev_GetSystemEventStrings] :Id";

            IList<Hashtable> results = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                var query = session.CreateSQLQuery(proc)
                    .SetInt32("Id", eventId);

                results = query
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }
            var r = results.ToList<LogEntryExtendedDto>().FirstOrDefault();

            return r;
        }

        public IList<GroupDto> TransmitterGroupsGet()
        {
            string sql = "EXEC [dbo].[dev_TransmitterGroupsGet]";

            IList<Hashtable> results = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                var query = session.CreateSQLQuery(sql);

                results = query.SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            var r = results.ToList<GroupDto>();

            return r;
        }

        public string TransmitterGroupSet(GroupDto dto)
        {
            string sql = "EXEC [dbo].[dev_TransmitterGroupSet] :Id,:Name,:OperatorId,:Reason";

            string result = String.Empty;
            IList<Hashtable> results;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                results = s.CreateSQLQuery(sql)
                    .SetInt16("Id", dto.Id)
                    .SetString("Name", dto.Name)
                    .SetInt32("OperatorId", BloodyUser.Current.ID)
                    .SetString("Reason", String.Empty)
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();


            }
            result = results.GetStringResult("Result", "ErrorMessage"); ;

            return result;
        }

        public ResultInfoDto BandBatteryChange(int bandId, BatteryTypeDTO newBattery, string reason)
        {
            string sql = "EXEC [dbo].[dev_BandBatteryChange] :BandId,:BatteryTypeId,:OperatorId,:Reason";

            IList<Hashtable> results = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                var query = session.CreateSQLQuery(sql);
                query.SetInt32("BandId", bandId);
                query.SetInt32("BatteryTypeId", newBattery.Id);
                query.SetInt32("OperatorId", BloodyUser.Current.ID);
                query.SetString("Reason", reason);

                results = query
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            return new ResultInfoDto() { Result = Convert.ToString(results[0]["Result"]), ErrorMessage = Convert.ToString(results[0]["ErrorMessage"]), ErrorCode = Convert.ToString(results[0]["ErrorCode"]) };
        }

        public IList<ExternalRecordsInDto> GetExternalRecordsInFiltered(IDictionary<string, string> filters, int pageNum, int pageSize)
        {
            string sql = "[dbo].[fda_GetExternalRecordsInFiltered] :PageNum,:PageSize,:TreeUnitIds,:Type,:IdFrom,:IdTo,:RequestIdFrom,:RequestIdTo,:StoreDateUtcFrom,:StoreDateUtcTo,:RecordType,:RecordId,:Record,:ProcessedFrom,:ProcessedTo,:TryFrom,:TryTo,:ProcessedDateUtcFrom,:ProcessedDateUtcTo,:Error,:SortOrder";

            QueryHelper.UpdateFilters(filters, pageNum, pageSize);

            IList<ExternalRecordsInDto> output = new List<ExternalRecordsInDto>();

            IList<Hashtable> results = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                results = query
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            return results.ToList<ExternalRecordsInDto>();
        }

        public int GetExternalRecordsInTotalCount(IDictionary<string, string> filters)
        {
            string sql = "[dbo].[fda_GetExternalRecordsInFilteredTotalCount] :TreeUnitIds,:Type,:IdFrom,:IdTo,:RequestIdFrom,:RequestIdTo,:StoreDateUtcFrom,:StoreDateUtcTo,:RecordType,:RecordId,:Record,:ProcessedFrom,:ProcessedTo,:TryFrom,:TryTo,:ProcessedDateUtcFrom,:ProcessedDateUtcTo,:Error";

            QueryHelper.UpdateFilterTreeUnitIds(filters);

            int totalCount = 0;

            if (filters["TreeUnitIds"][0] == ',')
                filters["TreeUnitIds"] = filters["TreeUnitIds"].Substring(1);

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                totalCount = query.UniqueResult<int>();
            }

            return totalCount;
        }

        public IList<ExternalRecordsOutDto> GetExternalRecordsOutFiltered(IDictionary<string, string> filters, int pageNum, int pageSize)
        {
            string sql = "EXEC [dbo].[fda_GetExternalRecordsOutFiltered] :PageNum,:PageSize,:TreeUnitIds,:IdFrom,:IdTo,:EventIdFrom,:EventIdTo,:DateUtcFrom,:DateUtcTo,:RecordType,:Record,:ParentIdFrom,:ParentIdTo,:TryFrom,:TryTo,:TryAgainFrom,:TryAgainTo,:SentDateUtcFrom,:SentDateUtcTo,:Error,:SortOrder";

            QueryHelper.UpdateFilters(filters, pageNum, pageSize);

            IList<ExternalRecordsOutDto> output = new List<ExternalRecordsOutDto>();

            IList<Hashtable> results = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                results = query
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            return results.ToList<ExternalRecordsOutDto>();
        }

        public int GetExternalRecordsOutTotalCount(IDictionary<string, string> filters)
        {
            string sql = "[dbo].[fda_GetExternalRecordsOutFilteredTotalCount] :TreeUnitIds,:IdFrom,:IdTo,:EventIdFrom,:EventIdTo,:DateUtcFrom,:DateUtcTo,:RecordType,:Record,:ParentIdFrom,:ParentIdTo,:TryFrom,:TryTo,:TryAgainFrom,:TryAgainTo,:SentDateUtcFrom,:SentDateUtcTo,:Error";

            QueryHelper.UpdateFilterTreeUnitIds(filters);

            int totalCount = 0;

            if (filters["TreeUnitIds"][0] == ',')
                filters["TreeUnitIds"] = filters["TreeUnitIds"].Substring(1);

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                totalCount = query.UniqueResult<int>();
            }

            return totalCount;
        }


        public IList<ExternalUnitMapDto> GetExternalUnitMapFiltered(IDictionary<string, string> filters, int pageNum, int pageSize)
        {
            string sql = "EXEC [dbo].[fda_GetExternalUnitMapFiltered] :PageNum,:PageSize,:TreeUnitIds,:ExtId,:ExtDescription,:M2mIdFrom,:M2mIdTo,:M2mDescription,:SortOrder";

            QueryHelper.UpdateFilters(filters, pageNum, pageSize);

            IList<ExternalUnitMapDto> output = new List<ExternalUnitMapDto>();

            IList<Hashtable> results = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                results = query
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            return results.ToList<ExternalUnitMapDto>();
        }

        public int GetExternalUnitMapFilteredTotalCount(IDictionary<string, string> filters)
        {
            string sql = "EXEC [dbo].[fda_GetExternalUnitMapFilteredTotalCount] :TreeUnitIds,:ExtId,:ExtDescription,:M2mIdFrom,:M2mIdTo,:M2mDescription";

            QueryHelper.UpdateFilterTreeUnitIds(filters);

            int totalCount = 0;

            if (filters["TreeUnitIds"][0] == ',')
                filters["TreeUnitIds"] = filters["TreeUnitIds"].Substring(1);

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                totalCount = query.UniqueResult<int>();
            }

            return totalCount;
        }


        public string MapExternalUnit(string extId, int? m2mId, int operatorId, string reason)
        {
            string sql = "[dbo].[sync_MapExternalUnit] :ExtId,:M2mId,:OperatorId,:Reason";

            IList<Hashtable> results = null;
            string result = String.Empty;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                var query = session.CreateSQLQuery(sql);
                query.SetString("ExtId", extId);
                query.SetInt32("M2mId", m2mId);
                query.SetInt32("OperatorId", BloodyUser.Current.ID);
                query.SetString("Reason", reason);

                results = query
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();

                result = results.GetStringResult("Result", "ErrorMessage");
            }

            return result;
        }


        public IList<ServiceStatusDto> GetServiceStatusFiltered(IDictionary<string, string> filters, int pageNum, int pageSize)
        {
            string sql = "EXEC [dbo].[fda_GetServiceStatusFiltered] :PageNum,:PageSize,:TreeUnitIds,:ServiceName,:Description,:StartTimeUtcFrom,:StartTimeUtcTo,:PingTimeUtcFrom,:PingTimeUtcTo,:PreviousPingTimeUtcFrom,:PreviousPingTimeUtcTo,:HasPartnerFrom,:HasPartnerTo,:PartnerPingTimeUtcFrom,:PartnerPingTimeUtcTo,:InfoTxt,:SortOrder";

            QueryHelper.UpdateFilters(filters, pageNum, pageSize);

            IList<ServiceStatusDto> output = new List<ServiceStatusDto>();

            IList<Hashtable> results = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                results = query
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            return results.ToList<ServiceStatusDto>();
        }

        public int GetServiceStatusFilteredTotalCount(IDictionary<string, string> filters)
        {
            string sql = "EXEC [dbo].[fda_GetServiceStatusFilteredTotalCount] :TreeUnitIds,:ServiceName,:Description,:StartTimeUtcFrom,:StartTimeUtcTo,:PingTimeUtcFrom,:PingTimeUtcTo,:PreviousPingTimeUtcFrom,:PreviousPingTimeUtcTo,:HasPartnerFrom,:HasPartnerTo,:PartnerPingTimeUtcFrom,:PartnerPingTimeUtcTo,:InfoTxt";

            QueryHelper.UpdateFilterTreeUnitIds(filters);

            int totalCount = 0;

            if (filters["TreeUnitIds"][0] == ',')
                filters["TreeUnitIds"] = filters["TreeUnitIds"].Substring(1);

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                totalCount = query.UniqueResult<int>();
            }

            return totalCount;
        }


        public string TransmitterRemoveFromUnit(int unitId, string reason)
        {
            string sql = "EXEC [dbo].[ev_TransmitterRemoveFromUnit] :UnitId,:OperatorId,:Reason,:OperationDateUtc";

            IList<Hashtable> results = null;
            string result = String.Empty;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql)
                    .SetInt32("UnitId", unitId)
                    .SetInt32("OperatorId", BloodyUser.Current.ID)
                    .SetString("Reason", String.Empty)
                    .SetDateTime("OperationDateUtc", DateTime.Now.ToUniversalTime());

                results = query
                        .SetResultTransformer(Transformers.AliasToEntityMap)
                        .List<Hashtable>();

                result = results.GetStringResult("Result", "ErrorMessage");
            }

            return result;
        }

        public string TransmitterDelete(int transmitterId, string reason)
        {
            string sql = "EXEC [dbo].[ev_TransmitterDelete] :TransmitterId,:OperatorId,:Reason,:OperationDateUtc";

            IList<Hashtable> results = null;
            string result = String.Empty;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql)
                    .SetInt32("TransmitterId", transmitterId)
                    .SetInt32("OperatorId", BloodyUser.Current.ID)
                    .SetString("Reason", String.Empty)
                    .SetDateTime("OperationDateUtc", DateTime.Now.ToUniversalTime());

                results = query
                        .SetResultTransformer(Transformers.AliasToEntityMap)
                        .List<Hashtable>();

                result = results.GetStringResult("Result", "ErrorMessage");
            }

            return result;
        }

        public int GetTransmitterRepairFilteredTotalCount(IDictionary<string, string> filters)
        {
            const string sql = "EXEC [dbo].[fda_GetTransmitterDetailsFilteredTotalCount] :TreeUnitIds, :IdFrom, :IdTo, :MacFrom, :MacTo, :MacString, :ClientIdFrom, :ClientIdTo, :InstallationUnitIdFrom, :InstallationUnitIdTo, :InstallationUnitName, :Name, :BarCode, :TypeIdFrom, :TypeIdTo, :ReportIntervalSecFrom, :ReportIntervalSecTo, :SensitivityFrom, :SensitivityTo, :AvgCalcTimeSFrom, :AvgCalcTimeSTo, :Msisdn, :IsBandUpdaterFrom, :IsBandUpdaterTo, :Description, :RssiTresholdFrom, :RssiTresholdTo, :GroupIdFrom, :GroupIdTo, :GroupName,:IpAddress,:IsConnectedFrom,:IsConnectedTo,:AlarmOngoingFrom,:AlarmOngoingTo";

            QueryHelper.UpdateFilterTreeUnitIds(filters);

            if (filters["TreeUnitIds"][0] == ',')
                filters["TreeUnitIds"] = filters["TreeUnitIds"].Substring(1);

            int totalCount = 0;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);
                totalCount = query.UniqueResult<int>();
            }

            return totalCount;
        }

        public IList<TransmitterDetailsDto> GetTransmitterRepairDetailsFiltered(IDictionary<string, string> filters, int pageNum, int pageSize)
        {
            const string sql = "EXEC [dbo].[fda_GetTransmitterDetailsFiltered] :PageNum, :PageSize, :TreeUnitIds, :IdFrom, :IdTo, :MacFrom, :MacTo, :MacString, :ClientIdFrom, :ClientIdTo, :InstallationUnitIdFrom, :InstallationUnitIdTo, :InstallationUnitName, :Name, :BarCode, :TypeIdFrom, :TypeIdTo, :ReportIntervalSecFrom, :ReportIntervalSecTo, :SensitivityFrom, :SensitivityTo, :AvgCalcTimeSFrom, :AvgCalcTimeSTo, :Msisdn, :IsBandUpdaterFrom, :IsBandUpdaterTo, :Description, :RssiTresholdFrom, :RssiTresholdTo, :GroupIdFrom, :GroupIdTo, :GroupName,:IpAddress,:IsConnectedFrom,:IsConnectedTo,:AlarmOngoingFrom,:AlarmOngoingTo,:SortOrder";

            QueryHelper.UpdateFilterTreeUnitIds(filters);
            QueryHelper.UpdateFilters(filters, pageNum, pageSize);

            if (filters["TreeUnitIds"][0] == ',')
                filters["TreeUnitIds"] = filters["TreeUnitIds"].Substring(1);

            IList<TransmitterDetailsDto> output = new List<TransmitterDetailsDto>();
            IList<Hashtable> results = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql).SetParametersFromFilter(filters);

                results = query
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }
            return results.ToList<TransmitterDetailsDto>();
        }


        public PatientDetailsDto GetPatient(int patientId)
        {
            const string sql = "EXEC [dbo].[dev_GetPatient] :PatientId";

            IList<Hashtable> results = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql)
                    .SetInt32("PatientId", patientId);

                results = query
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            return results.ToList<PatientDetailsDto>().FirstOrDefault();
        }


        public MonitorableObjectStatusDto MonitoredObjectStatusGet(byte objectType, int objectId)
        {
            string sql = "EXEC [dbo].[MonitoredObjectStatusGet] :ObjectType,:ObjectId";

            IList<Hashtable> results = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql)
                    .SetByte("ObjectType", objectType)
                    .SetInt32("ObjectId", objectId);

                results = query.SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();

                if (results != null)
                {
                    foreach (var item in results)
                    {
                        MonitorableObjectStatusDto status = new MonitorableObjectStatusDto()
                        {
                            Id = Convert.ToInt16(item["Id"])
                            ,
                            ObjectType = Convert.ToInt32(item["ObjectType"])
                            ,
                            CurrentUnitId = item["CurrentUnitId"] == null ? null : (int?)Convert.ToInt32(item["CurrentUnitId"])
                            ,
                            CurrentUnitName = Convert.ToString(item["CurrentUnitName"])
                            ,
                            CurrentUnitLocation = Convert.ToString(item["CurrentUnitLocation"])
                            ,
                            PreviousUnitId = Convert.ToInt32(item["PreviousUnitId"])
                            ,
                            PreviousUnitName = Convert.ToString(item["PreviousUnitName"])
                            ,
                            PreviousUnitLocation = Convert.ToString(item["PreviousUnitLocation"])
                            ,
                            InCurrentUnitUtc = Convert.ToDateTime(item["InCurrentUnitUtc"])
                            ,
                            EventTypes = Convert.ToInt32(item["TopAlarmSeverityId"]) != null ? (int?)Convert.ToInt32(item["TopAlarmSeverityId"]) : null
                            ,
                            Name = Convert.ToString(item["Name"])
                            ,
                            AlarmType = item["TopAlarmTypeId"] != null ? (int?)Convert.ToInt32(item["TopAlarmTypeId"]) : null
                            ,
                            AlarmInfo = Convert.ToString(item["TopAlarmTypeName"])
                            ,
                            AlarmUnitName = Convert.ToString(item["TopAlarmUnitName"])
                            ,
                            MovedStatus = Convert.ToBoolean(item["IsMoving"]) ? "Pacjent się porusza" : "Pacjent się nie porusza"
                            ,
                            DisplayId = Convert.ToString(item["DispalyId"])
                            ,
                            AssigmentRoomId = Convert.ToInt32(item["AssignmentRoomId"])
                            ,
                            AssigmentRoomName = Convert.ToString(item["AssignmentRoomName"])
                            ,
                            AssigmentDepartmentId = Convert.ToInt32(item["AssignmentWardId"])
                            ,
                            AssigmentDepartmentName = Convert.ToString(item["AssignmentWardName"])
                            ,
                            WarningsCount = Convert.ToInt32(item["OpenAlarmsSeverity1"])
                            ,
                            AlarmsCount = Convert.ToInt32(item["OpenAlarmsSeverity2"])
                            ,
                            RoomOutDateUtc = Convert.ToDateTime(item["RoomOutDateUtc"])
                        };

                        return status;
                    }

                }

                return new MonitorableObjectStatusDto();
            }
        }

        public int GetAssignedPatientsCount(int unitId)
        {
            string sqlCommand = "[dbo].[unt_GetAssignedPatientsCount] :UnitId";
            int result = -1;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sqlCommand)
                    .SetString("UnitId", unitId.ToString());

                result = query
                    .UniqueResult<int>();
            }

            return result;
        }

        public LocationInfoDto Object_GetLocation(int objectId, byte objectType)
        {
            string sqlCommand = "[dbo].[Object_GetLocation] :ObjectId,:ObjectType";

            IList<Hashtable> results = null;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sqlCommand)
                    .SetByte("ObjectType", objectType)
                    .SetInt32("ObjectId", objectId);

                results = query.SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();

                if (results != null)
                {
                    foreach (var item in results)
                    {
                        LocationInfoDto status = new LocationInfoDto()
                        {
                            CurrentUnitId = Convert.ToInt32(item["CurrentUnitId"])
                            ,
                            CurrentUnitName = Convert.ToString(item["CurrentUnitName"])
                            ,
                            CurrentUnitLocation = Convert.ToString(item["CurrentUnitLocation"])
                            ,
                            InCurrent = Convert.ToDateTime(item["InCurrentUtc"]).ToLocalTime()
                            ,
                            PreviousUnitId = Convert.ToInt32(item["PreviousUnitId"])
                            ,
                            PreviousUnitName = Convert.ToString(item["PreviousUnitName"])
                            ,
                            PreviousUnitLocation = Convert.ToString(item["PreviousUnitLocation"])
                            ,
                            InPrevious = Convert.ToDateTime(item["InPreviousUtc"]).ToLocalTime()
                        };

                        return status;
                    }
                }
            }

            return null;
        }


    }
}