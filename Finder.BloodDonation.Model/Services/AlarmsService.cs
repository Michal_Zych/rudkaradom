﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finder.BloodDonation.Model.Domain;
using System.ServiceModel;
using System.ServiceModel.Activation;
using FinderFX.Web.Core.DomainServices;
using NHibernate;
using FinderFX.Web.Core.Communication;
using NHibernate.Transform;
using FinderFX.Web.Core.Logging;
using AutoMapper;
using NHibernate.Linq;
using Finder.BloodDonation.Model.Dto;
using FinderFX.Web.Core.Authentication;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Model.Services.Tools;
using log4net;
using Finder.BloodDonation.Helpers;

namespace Finder.BloodDonation.Model.Services
{
    [ServiceContract]
    public interface IAlarmsService
    {
        [OperationContract]
        IList<AlarmDto> SearchAlarms(int unit, int unitSource, string objectName, DateTime dateStart, DateTime dateEnd);

        [OperationContract]
        IList<AlarmDto> PingAlarms(DateTime dateStart);


        [OperationContract]
        bool CloseAlarms(string alarmIds, string comment);


        [OperationContract]
        IList<DataDto> GetSensorData(int sensor, DateTime date_from, DateTime date_to);


        [OperationContract]
        string LocalizationCloseAlarms(string Ids, string comment);
    }

    [LogDetails]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class AlarmsService : IAlarmsService, IDomainService
    {
        private static ILog Logger = LogManager.GetLogger(typeof(AlarmsService));

        public IList<AlarmDto> SearchAlarms(int unit, int unitSource, string objectName, DateTime dateStart, DateTime dateEnd)
        {
            IList<Alarm> r = null;

            var unitsBuilder = new AlarmsUnitsIDsListBuilder();
            var units = unitsBuilder.GetIDs(unit, unitSource);
                if (!string.IsNullOrEmpty(units))
                {
                    using (ISession x = NHibernateHttpModule.OpenSession())
                    {
                        IQuery query = x.GetNamedQuery(AlarmsQueryProvider.SearchAlarms());
                        query.SetDateTime("date_from", dateStart);
                        query.SetDateTime("date_to", dateEnd);
                        query.SetString("sensor", objectName);
                        query.SetString("units", units);
                        query.SetInt32("user", BloodyUser.Current.ID);
                        r = query.List<Alarm>();
                    }
                }

            return Mapper.Map<IList<Alarm>, IList<AlarmDto>>(r);
        }

        public IList<AlarmDto> PingAlarms(DateTime dateStart)
        {
            IList<Alarm> r = null;
            var unitsBuilder = new AlarmsUnitsIDsListBuilder();
            var units = unitsBuilder.GetIDs(BloodyUser.Current.Root.ID, SelectionSources.MainTree);
            
            if (!string.IsNullOrEmpty(units))
            {
                using (ISession x = NHibernateHttpModule.OpenSession())
                {
                    IQuery query = x.GetNamedQuery(AlarmsQueryProvider.CheckAlarms());
                    query.SetDateTime("date_from", dateStart);
                    query.SetInt32("user", BloodyUser.Current.ID);
                    query.SetString("units", units);
                    r = query.List<Alarm>();
                }
            }

            return Mapper.Map<IList<Alarm>, IList<AlarmDto>>(r);
        }

        public bool CloseAlarm(int alarmId, string comment)
        {
            using (ISession x = NHibernateHttpModule.OpenSession())
            using (ITransaction transaction = x.BeginTransaction())
            {
                Logger.DebugFormat("Zamknięcie alarmu: {0} {1} ", alarmId, comment);

                ISQLQuery query = x.CreateSQLQuery("exec Alarms_CloseAlarm @id=:id, @comment=:comment,@userid=:userid");
                query.SetInt32("id", alarmId);
                query.SetString("comment", comment);
                query.SetInt32("userid", BloodyUser.Current.ID);
                object r = query.UniqueResult();

                transaction.Commit();
            }

            return true;
        }

        public bool CloseAlarms(string alarmIds, string comment)
        {
            using (ISession x = NHibernateHttpModule.OpenSession())
            using (ITransaction transaction = x.BeginTransaction())
            {
                Logger.DebugFormat("Zamknięcie alarmów: {0} {1} ", alarmIds, comment);

                ISQLQuery query = x.CreateSQLQuery("exec Alarms_CloseAlarms @ids=:ids, @comment=:comment, @userid=:userid");
                query.SetString("ids", alarmIds);
                query.SetString("comment", comment);
                query.SetInt32("userid", BloodyUser.Current.ID);
                object r = query.UniqueResult();

                transaction.Commit();
            }

            return true;
        }


        public IList<DataDto> GetSensorData(int sensor, DateTime date_from, DateTime date_to)
        {
            IList<Data> added = null;

                using (ISession x = NHibernateHttpModule.OpenSession())
                {
                    var sql = @"exec Diagnostics_GetData @sensorUnit = :sensor, @date_from = :date_from, @date_to= :date_to";

                    added = x.CreateSQLQuery(sql)
                        .AddEntity(typeof(Data))
                        .SetInt32("sensor", sensor)
                        .SetDateTime("date_from", date_from)
                        .SetDateTime("date_to", date_to)
                        .SetResultTransformer(new DistinctRootEntityResultTransformer())
                        .List<Data>();
                }

            return Mapper.Map<IList<Data>, IList<DataDto>>(added);
        }

        public string LocalizationCloseAlarms(string Ids, string comment)
        {
            const string sql = "EXEC dbo.ev_CloseAlarms :Ids,:Comment,:Userid";

            string result;

            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql)
                    .SetString("Ids", Ids)
                    .SetString("Comment", comment)
                    .SetInt32("Userid", BloodyUser.Current.ID)
                    .UniqueResult<string>();


                result = query;
            }

            return result;
        }
    }
}
