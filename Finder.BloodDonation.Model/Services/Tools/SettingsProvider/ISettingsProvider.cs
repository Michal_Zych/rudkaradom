﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finder.BloodDonation.Model.Dto;

namespace Finder.BloodDonation.Model.Services
{

    interface ISettingsProvider
    {

        IList<SettingDTO> GetSettingsSet(int? clientId, int? userId);

        void SetSetting(int? clientId, int? userId, int? unitId, string setting, string value);
        void SetSettingsSet(int? clientId, int? userId, int? unitId, IDictionary<string, string> settingsSet);
    }
}
