﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finder.BloodDonation.Model.Dto;
using NHibernate;
using FinderFX.Web.Core.Communication;
using NHibernate.Transform;
using AutoMapper;

namespace Finder.BloodDonation.Model.Services
{
    class SettingsReader: ISettingsProvider
    {
        private const string GetUserSettings = "exec dbo.Settings_GetUserSettings :clientId, :userId";

        public IList<SettingDTO> GetSettingsSet(int? clientId, int? userId)
        {
           /* return new List<SettingDTO>(){
                new SettingDTO(){Name="A", Value="b"}
            };
            */
           
           IList<SettingDTO> result = null;
            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                result = session.CreateSQLQuery(GetUserSettings)
                    .AddScalar("Name", NHibernateUtil.String)   //kolumny wyniku - nazwy muszą być takie same jak w DTO
                    .AddScalar("Value", NHibernateUtil.String)
                    .SetInt32("clientId", clientId)             //parametry
                    .SetInt32("userId", userId)
                    .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<SettingDTO>())
                    .List<SettingDTO>();
                }
            return result;
        }

        public void SetSetting(int? clientId, int? userId, int? unitId, string setting, string value)
        {
            throw new NotImplementedException();
        }

        public void SetSettingsSet(int? clientId, int? userId, int? unitId, IDictionary<string, string> settingsSet)
        {
            throw new NotImplementedException();
        }
    }
}
