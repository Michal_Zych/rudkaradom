﻿using Finder.BloodDonation.Model.Dto;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Model.Services.Tools
{
    public static class ServicesExtension
    {
        public static string GetStringResult(this IEnumerable<Hashtable> results, string resultCollumnName = "Result", string errorMessageCollumnName = "ErrorMessage", string errorCodeCollumnName = "ErrorCode")
        {
            int a;

            string result = String.Empty;

            Hashtable item = results.ToList()[0];

            if (item[resultCollumnName] != null &&
               int.TryParse(item[resultCollumnName].ToString(), out a))
            {
                return a.ToString();
            }
            else
            {
                return "Kod błędu: " + item[errorCodeCollumnName] + "/nWiadomość: " + item[errorMessageCollumnName];
            }
        }

        public static string GetStringResult(this IEnumerable<Hashtable> results, string resultCollumnName = "Result", string errorMessageCollumnName = "ErrorMessage")
        {
            int a;

            string result = String.Empty;

            Hashtable item = results.ToList()[0];

            if (item[resultCollumnName] != null && int.TryParse(item[resultCollumnName].ToString(), out a))
            {
                return a.ToString();
            }
            else if(item[errorMessageCollumnName] != null)
            {
                return item[errorMessageCollumnName].ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        public static string GetStringResult(this IEnumerable<Hashtable> results, int errorCode)
        {
            int a;
            string result = String.Empty;

            foreach (Hashtable item in results)
            {
                if (int.TryParse(item["Result"].ToString(), out a))
                    return a.ToString();
               
                if(int.Parse(item["ErrorCode"].ToString()) == errorCode)
                    return "Kod błędu: " + item["ErrorCode"] + " Wiadomość: " + item["ErrorMessage"];
            }

            return String.Empty;
        }

        public static string GetStringResult(this IEnumerable<Hashtable> results)
        {
            string result = String.Empty;

            return results.ToList()[0]["Result"].ToString();
        }
    }
}
