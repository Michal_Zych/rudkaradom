﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finder.BloodDonation.Model.Domain;

namespace Finder.BloodDonation.Model.Services.Tools
{
    public static class AlarmsQueryProvider
    {
        public static string CheckAlarms()
        {
            if (BloodyUser.Current.CanSeeAllAlarms)
                return "Alarms_CheckAlarms";
            else
                return "Alarms_CheckAlarmsWithoutShorts";
        }

        public static string SearchAlarms()
        {
            if (BloodyUser.Current.CanSeeAllAlarms)
                return "Alarms_SearchAlarms";
            else
                return "Alarms_SearchAlarmsWithoutShorts";

        }
    }
}
