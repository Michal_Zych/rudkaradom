﻿using System;
using System.Net;

namespace Finder.BloodDonation.Model.Services.Tools
{
    public static class QueryParametersParser
    {
        public static DateTime? ConvertToDateTime(object value)
        {
            return (value == null) ? (DateTime?)null : Convert.ToDateTime(value);
        }
        public static int? ConvertToInt(object value)
        {
            return (value == null) ? (int?)null : Convert.ToInt32(value);
        }

        public static Nullable<DateTime> ParseDate(string value)
        {
            return (value != null) ? new Nullable<DateTime>(DateTime.Parse(value)) : new Nullable<DateTime>();
        }
        public static int? ParseInt(string value)
        {
            return (value == null) ? (int?)null : Convert.ToInt32(value);
        }

        internal static long? ConvertToLong(object p)
        {
            return (p == null) ? (long?)null : Convert.ToInt64(p);
        }

        internal static object ConvertToObject(long? p)
        {
            return (!p.HasValue) ? null : (object)Convert.ToInt64(p);
        }

        internal static ulong? ConvertToULong(object p)
        {
            return (p == null) ? (ulong?)null : Convert.ToUInt64(p);
        }

        internal static object ConvertToObject(ulong? p)
        {
            return (!p.HasValue) ? null : (object)Convert.ToUInt64(p);
        }
    }
}
