﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Model.Services.Tools
{
    public class DBError
    {
        public string Key { get; set; }
        public string Message { get; set; }
        public string Word { get; set; }

        public DBError() { }
    }
}
