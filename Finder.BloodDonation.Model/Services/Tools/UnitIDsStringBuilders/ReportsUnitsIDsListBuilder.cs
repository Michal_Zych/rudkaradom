﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Model.Domain;
using Finder.BloodDonation.Helpers;

namespace Finder.BloodDonation.Model.Services.Tools
{
    public class ReportsUnitsIDsListBuilder : IDsListBuilder
    {
        protected override IEnumerable<int> GetAccessibleIDs(int topUnitID)
        {
            return BloodyUser.Current.GetChildUnitsWithParent(
                topUnitID, Permission, UnitTypeOrganizer.SensorUnitsList()).Select(u => u.ID);
        }

        protected override string Permission
        {
            get { return PermissionNames.Reports; }
        }
    }
}
