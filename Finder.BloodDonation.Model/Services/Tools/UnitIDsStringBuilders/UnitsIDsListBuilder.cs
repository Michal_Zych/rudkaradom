﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Model.Domain;
using Finder.BloodDonation.Helpers;

namespace Finder.BloodDonation.Model.Services.Tools
{
    public static class UnitsIDsListBuilder
    {
        internal static string GetTreeUnitIds(string topUnitId, string permissionName = PermissionNames.UnitsListing, bool childrenOnly = false)
        {
            string result = BloodyUser.Current.GetChildUnits(Convert.ToInt32(topUnitId), permissionName).Select(u => u.ID).ToCommaSeparatedValues();
            if (!childrenOnly)
            {
                result = result + "," + topUnitId;
            }
            return result;
        }
    }
}
