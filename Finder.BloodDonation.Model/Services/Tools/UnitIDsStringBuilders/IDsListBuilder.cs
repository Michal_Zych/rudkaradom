﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Model.Domain;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Dialogs.MainTree;
using Finder.BloodDonation.Model.Dto;

namespace Finder.BloodDonation.Model.Services.Tools
{
    public abstract class IDsListBuilder
    {
        public string GetIDs(int topUnitID, int sourceTree)
        {
            IEnumerable<int> unitIDs = GetAccessibleIDs(topUnitID);

            if (SelectionSources.Favorites == sourceTree)
            {
                unitIDs = new FavoriteUnits().FilterByFavoriteDevices(unitIDs);
                throw new Exception("FavoriteUnits is unusing in application!");
            }
            //else if(SelectionSources.MainTree == sourceTree)
            //{
                //unitIDs = new MainTree().FilterByMainTree(topUnitID);
            //}

            var result = unitIDs.ToCommaSeparatedValues();
            if (AddTopUnitID)
            {
                if (result == null)
                    result += topUnitID;
                else
                    result += "," + topUnitID;
            }

            

            return result;
        }

        public string GetIDs(UnitDTO unit, int sourceTree)
        {
            IEnumerable<int> unitIDs;

            if (SelectionSources.MainTree == sourceTree)
            {
                unitIDs = new MainTree().FilterByMainTree(unit);


                var result = unitIDs.ToCommaSeparatedValues();

                if (AddTopUnitID)
                {
                    result += "," + AddTopUnitID;
                }

                return result;
            }
            else
                return String.Empty;
        }

        protected bool addTopUnitID;

        public virtual bool AddTopUnitID 
        {
            get { return addTopUnitID; }
            set { addTopUnitID = value; }
        }

        protected abstract string Permission { get; }
        protected abstract IEnumerable<int> GetAccessibleIDs(int topUnitID);
        
    }
}
