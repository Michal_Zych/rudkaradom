﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Model.Domain;
using Finder.BloodDonation.Helpers;

namespace Finder.BloodDonation.Model.Services.Tools
{
    public class RfidUnitsIDsListBuilder : IDsListBuilder
    {
        protected override IEnumerable<int> GetAccessibleIDs(int topUnitID)
        {
            return BloodyUser.Current.GetChildUnits(topUnitID, Permission).Select(u => u.ID);
        }

        public override bool AddTopUnitID
        {
            get { return true; }
        }

        protected override string Permission
        {
            get { return PermissionNames.UnitMonitoring; }
        }
    }
}
