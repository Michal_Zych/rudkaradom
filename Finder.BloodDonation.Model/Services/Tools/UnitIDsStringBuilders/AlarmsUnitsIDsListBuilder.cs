﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Model.Domain;
using Finder.BloodDonation.Helpers;

namespace Finder.BloodDonation.Model.Services.Tools
{
       public class AlarmsUnitsIDsListBuilder : ReportsUnitsIDsListBuilder
    {
        protected override string Permission
        {
            get { return PermissionNames.AlarmListing; }
        }
    }
}
