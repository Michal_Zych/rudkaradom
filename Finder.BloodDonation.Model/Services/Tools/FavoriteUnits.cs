﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finder.BloodDonation.Model.Domain;
using NHibernate;
using FinderFX.Web.Core.Communication;
using NHibernate.Transform;
using Finder.BloodDonation.Model.Dto;
using AutoMapper;

namespace Finder.BloodDonation.Model.Services.Tools
{
    public class FavoriteUnits
    {
        private const string SQLfavoriteIDs = "select UnitID from FavoriteUnits where UserId = :userId";

        public IEnumerable<int> GetFavoriteIDs(int userID)
        {
                using (ISession session = NHibernateHttpModule.OpenSession())
                {
                    var result = session.CreateSQLQuery(SQLfavoriteIDs)
                        .SetInt32("userId", userID)
                        .List<int>();

                    return result;
                }
        }


        public IList<Unit> GetFavoriteUnitsList(int userID)
        {
            IList<Unit> favs = null;
            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                var sql = @"exec Units_GetFavoriteUnits @user_id = :user_id";
                favs = session.CreateSQLQuery(sql)
                    .AddEntity(typeof(Unit))
                    .SetInt32("user_id", userID)
                    .SetResultTransformer(new DistinctRootEntityResultTransformer())
                    .List<Unit>();
            }
            return favs;
        }

        public IList<UnitDTO> GetFavoriteUnitDTOs(int userID)
        {
            IList<FavoriteUnit> favs = null;
            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                favs = session.CreateCriteria<FavoriteUnit>()
                    .Add(NHibernate.Criterion.Expression.Eq("UserID", userID))
                    .SetFetchMode("Unit", FetchMode.Eager)
                    .List<FavoriteUnit>();
            }

            IList<UnitDTO> units = new List<UnitDTO>();
            foreach (FavoriteUnit f in favs)
            {
                units.Add(Mapper.Map<Unit, UnitDTO>(f.Unit));
            }

            return units;
        }


        public IEnumerable<int> FilterByFavoriteDevices(IEnumerable<int> unitIDs)
        {
            var IDs = from f in GetFavoriteIDs(BloodyUser.Current.ID)
                      join u in unitIDs on f equals u
                      select u;
            return IDs;
        }

    }
}
