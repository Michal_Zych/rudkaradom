﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Model.Services.Tools
{
    public class DBErrorParser
    {
        public List<DBError> DBErrorList = new List<DBError>()
        {
            new DBError()
            {
                Key = "2601",
                Word = "IX_Bands_Mac",
                Message = "Błąd! Zduplikowany adres MAC."
            }
        };

        public string GetErrorResult(IList<Hashtable> resultsToCheck,int indexToCheck)
        {
            string result = String.Empty;

            for(int i = 0; i < DBErrorList.Count; i++)
            {
                if(
                    resultsToCheck[indexToCheck]["ErrorCode"].ToString().Equals(DBErrorList[i].Key) &&
                    resultsToCheck[indexToCheck]["ErrorMessage"].ToString().IndexOf(DBErrorList[i].Word) > 0
                    )
                {
                    result = "Błąd! Zduplikowany adres MAC.";
                }
                else
                {
                    result = resultsToCheck[indexToCheck]["ErrorCode"] + " " + resultsToCheck[indexToCheck]["ErrorMessage"].ToString();
                }
            }

            return result;
        }
    }
}
