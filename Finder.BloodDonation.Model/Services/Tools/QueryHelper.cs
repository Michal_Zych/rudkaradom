﻿using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.Model.Authentication;
using NHibernate;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Finder.BloodDonation.DataTable;


namespace Finder.BloodDonation.Model.Services.Tools
{
    public static class QueryHelper
    {

        public static T GetRecord<T>(this Hashtable row) where T : class, new()
        {
            T record = new T();

            var properties = record.GetType().GetProperties(System.Reflection.BindingFlags.Public
                        | System.Reflection.BindingFlags.Instance
                        | System.Reflection.BindingFlags.DeclaredOnly);

            object value;
            string name;

            foreach (var property in properties)
            {
                name = property.Name;
                value = null;

                if (property.PropertyType == typeof(DateTime) || property.PropertyType == typeof(DateTime?))
                {
                    if (!row.ContainsKey(name))
                    {
                        name = name + "Utc";
                    }

                    if (row.ContainsKey(name))
                    {
                        value = UtcToLocalTime(row[name]);
                    }
                }
                else if (row.ContainsKey(property.Name))
                {
                    value = row[property.Name];
                }

                if (value != null)
                {
                    property.SetValue(record, value, null);
                }

            }
            return record;
        }

        public static object UtcToLocalTime(object value)
        {
            DateTime? d = null;

            if (value is DateTime? && (value as DateTime?).HasValue)
            {
                d = (value as DateTime?).Value;
            }
            else if (value is DateTime)
            {
                d = (DateTime)value;
            }
            else return value;

            return d.Value.ToLocalTime();
        }

        public static void UpdateFilterTreeUnitIds(IDictionary<string, string> filters, string permission = PermissionNames.UnitsListing)
        {
            if (filters.ContainsKey(FilterConstant.UnitFromTreeKey))
            {
                filters[FilterConstant.UnitsFromTreeKey] = UnitsIDsListBuilder.GetTreeUnitIds(filters[FilterConstant.UnitFromTreeKey], permission);
            }
        }

        public static void UpdateFilters(IDictionary<string, string> filters, int pageNum, int pageSize)
        {
            UpdateFilterTreeUnitIds(filters);

            filters[FilterConstant.PageNumKey] = pageNum.ToString();
            filters[FilterConstant.PageSizeKey] = pageSize.ToString();
        }

        private static string RemoveUtc(string paramName)
        {
            return Regex.Replace(paramName, "utc", "", RegexOptions.IgnoreCase);
        }


        public static IQuery SetParametersFromFilter(this IQuery query, IDictionary<string, string> _filters)
        {
            var filters = new Dictionary<string, string>(_filters, StringComparer.OrdinalIgnoreCase);

            string filterValue;

            foreach (var paramName in query.NamedParameters)
            {
                if (filters.ContainsKey(paramName))
                {
                    filterValue = filters[paramName];
                }
                else if (filters.ContainsKey(RemoveUtc(paramName)))
                {
                    DateTime d = DateTime.Parse(filters[RemoveUtc(paramName)]);
                    filterValue = d.ToUniversalTime().ToSQLstring();
                }
                else
                {
                    filterValue = null;
                }
                query.SetParameter(paramName, filterValue);
            }
            return query;
        }

        public static IList<T> ToList<T>(this IList<Hashtable> result) where T : class, new()
        {
            IList<T> list = new List<T>();
            if (result != null)
            {
                foreach (var item in result)
                {
                    list.Add(item.GetRecord<T>());
                }
            }
            return list;
        }


    }
}
