﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web.UI;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Model.Domain;
using System.ServiceModel;
using System.ServiceModel.Activation;
using FinderFX.Web.Core.DomainServices;
using Microsoft.SqlServer.Server;
using NHibernate;
using FinderFX.Web.Core.Communication;
using NHibernate.Hql.Ast.ANTLR;
using NHibernate.Transform;
using FinderFX.Web.Core.Logging;
using AutoMapper;
using NHibernate.Linq;
using Finder.BloodDonation.Model.Dto;
using FinderFX.Web.Core.Authentication;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Model.Enum;
using NHibernate.Criterion;
using FinderFX.Web.Core.Caching;
using log4net;
using Finder.BloodDonation.Model.Services.Tools;
using System.Collections;

namespace Finder.BloodDonation.Model.Services
{
    [ServiceContract]
    public interface IUsersService
    {
        #region ISettingsService definition
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //  [ServiceContract]
        //  public interface ISettingsService
        //  {
        [OperationContract]
        IList<SettingDTO> GetSettingsList();
        //}
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #endregion

        [OperationContract]
        IList<string> GetAfterLoginMessages();
       
        [OperationContract]
        AccountDto GetAccount(int account);

        //[OperationContract]
        //AccountDto AddAccount(AccountDto account, int role);
        
        [OperationContract]
        int CreateAccount(AccountDto account, int CreatorHighestUnitId, string reason, string logEntry);

        [OperationContract]
        int EditAccount(AccountDto account, string reason, string logEntry);

        [OperationContract]
        int DeleteAccount(int AccountId, string reason);

        [OperationContract]
        PingResultDto PingUserSession(PingRequestDto request);

        [OperationContract]
        void CreateUserSession(CreateSessionRequestDto request);

        [OperationContract]
        bool ChangePhone(string phone);

        [OperationContract]
        bool ChangeEMail(string eMail);

        [OperationContract]
        bool ChangePassword(string pass, string hash);

        [OperationContract]
        void SaveSettings(string settings);

        [OperationContract]
        string GetLoginNameForId(int userId);

        [OperationContract]
        IList<UserContactInfoDto> GetUsersContactsForUnit(int unitId);

        [OperationContract]
        IList<VersionInfoDto> GetVersions();

        [OperationContract]
        IList<LogEntryDto> GetLogs(DateTime dateStart, DateTime dateEnd);

        [OperationContract]
        IList<MessageDto> GetMessages(DateTime dateStart, DateTime dateEnd);

        [OperationContract]
        IList<ReceiverDto> GetMessageReceivers(int messageId);

        [OperationContract]
        bool DeleteMessages(string messages);

        [OperationContract]
        bool CloseMessages(string messages);

        [OperationContract]
        int CreateMessage(int? clientId, string message, DateTime activeFrom, DateTime activeTo, byte mode);

        [OperationContract]
        bool DeleteReceivers(int message, string receivers);

        [OperationContract]
        IList<SettingDTO> GetSettings();

        [OperationContract]
        bool SetSetting(string name, string value, string logEntry, string reason);

        [OperationContract]
        bool ResetSetting(string name, string reason);

        [OperationContract]
        IList<LoggedUserDto> GetLoggedUsers();

        [OperationContract]
        List<SettingItem> GetSettingsT();

        [OperationContract]
        string SetSettingsT(string param);

        [OperationContract]
        IList<int> CheckUnitsWithDisconnectedTransmitter();

        [OperationContract]
        IList<SystemSettingDto> GetSystemSettings();

        [OperationContract]
        string SetSystemSettings(SystemSettingDto dto);
    }

    [LogDetails]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class UsersService : IUsersService, IDomainService
    {
        private static ILog Logger = LogManager.GetLogger(typeof(UsersService));
        private const string CreateUserBasketProc = "EXEC [dbo].[Rfid_CreateUserBasket] :userId";
        private const string DeactivateUserProc = "EXEC [dbo].[Users_Deactivate] :userId";
        //private const string RemoveUserRfidStatuses = "EXCE [dbo].[Users_RemoveUserRfidStatuses] :userId";
        private const string CreateUserProc = "EXEC [dbo].[usr_CreateUser] :login, :password, :clientId, :name, :lastName, :phone, :eMail, :unit";
        private const string EditUserProc = "EXEC [dbo].[usr_EditUser] :id, :login, :password, :name, :lastName, :phone, :eMail";
        private const string EditUserPhoneProc = "EXEC [dbo].[usr_EditUserPhone] :id, :phone";
        private const string EditUserEMailProc = "EXEC [dbo].[usr_EditUserEMail] :id, :eMail";
        private const string EditUserPasswordProc = "EXEC [dbo].[usr_EditUserPassword] :id, :password";
        private const string DeleteUserProc = "EXEC [dbo].[usr_DeleteUser] :id";

        #region ISettingsService implementation
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private ISettingsProvider SettingsProvider = new SettingsReader();
        private int ClientID
        {
            get
            {
                return BloodyUser.Current.ClientID;
                return 1;
            }
        }

        public IList<SettingDTO> GetSettingsList()
        {

            return SettingsProvider.GetSettingsSet(BloodyUser.Current.ClientID, BloodyUser.Current.ID);
        }



        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #endregion

        public AccountDto GetAccount(int account)
        {
            Account added = null;

            using (ISession x = NHibernateHttpModule.OpenSession())
            {
                added = (from a in x.Linq<Account>() where a.ID == account select a).FirstOrDefault();
            }
            return Mapper.Map<Account, AccountDto>(added);
        }


        public int CreateAccount(AccountDto account, int creatorHighestUnitId, string reason, string logEntry)
         {
            const string CreateAccountProc =  "EXEC [dbo].[usr_CreateUser] :login, :password, :clientId, :name, :lastName, :phone, :eMail, :unit, :operatorId, :logEntry, :reason";

            int newAccount = -1;
                using (ISession session = NHibernateHttpModule.OpenSession())
                {
                    newAccount = session.CreateSQLQuery(CreateAccountProc)
                         .SetString("login", account.Login)
                         .SetString("password", account.Password)
                         .SetInt32("clientId", BloodyUser.Current.ClientID)
                         .SetString("name", account.Name)
                         .SetString("lastName", account.LastName)
                         .SetString("phone", account.Phone)
                         .SetString("eMail", account.EMail)
                         .SetInt32("unit", creatorHighestUnitId)
                         .SetInt32("operatorId", BloodyUser.Current.ID)
                         .SetString("logEntry", logEntry)
                         .SetString("reason", reason)
                         .UniqueResult<int>();
                }

            return newAccount;
        }


        public int EditAccount(AccountDto account, string reason, string logEntry)
         {
             const string EditAccountProc = "EXEC [dbo].[usr_EditUser] :id, :login, :password, :name, :lastName, :phone, :eMail, :operatorId, :logEntry, :reason";

            int editedAccount = -1;
                using (ISession session = NHibernateHttpModule.OpenSession())
                {
                    editedAccount = session.CreateSQLQuery(EditAccountProc)
                        .SetInt32("id", account.ID)
                         .SetString("login", account.Login)
                         .SetString("password", account.Password)
                         .SetString("name", account.Name)
                         .SetString("lastName", account.LastName)
                         .SetString("phone", account.Phone)
                         .SetString("eMail", account.EMail)
                         .SetInt32("operatorId", BloodyUser.Current.ID)
                         .SetString("logEntry", logEntry)
                         .SetString("reason", reason)
                         .UniqueResult<int>();
                }

            return editedAccount;
        }

        public int DeleteAccount(int accountId, string reason)
        {
            const string DeleteAccountProc = "EXEC [dbo].[usr_DeleteUser] :userId, :operatorId, :reason";

            int deletedAccount = -1;

                using (ISession session = NHibernateHttpModule.OpenSession())
                {
                    deletedAccount = session.CreateSQLQuery(DeleteAccountProc)
                        .SetInt32("userId", accountId)
                         .SetInt32("operatorId", BloodyUser.Current.ID)
                         .SetString("reason", reason)
                         .UniqueResult<int>();
                }

                System.Web.HttpContext.Current.Cache.Remove("user_" + accountId);
                System.Web.HttpContext.Current.Cache.Remove("LoadUser_" + accountId);

            return deletedAccount;
        }


        public PingResultDto PingUserSession(PingRequestDto request)
        {
            PingResultDto result = null;
            
            var sw = new Stopwatch();
            sw.Start();

            if (User.Current.Identity.IsAuthenticated)
            {
                SlideUserSession();

                string units = string.Join(",",
                                     (from u in BloodyUser.Current.GetChildUnits(
                                         BloodyUser.Current.Root.ID,
                                         PermissionNames.AlarmListing,
                                         UnitTypeOrganizer.SensorUnitsList())
                                      select u.ID));
                result = PingUserSessionInternal(BloodyUser.Current.ID, request.AlarmDateStart, request.MonitoringDeviceID, request.MonitoringLastDataDate, units);
                result.Alarms = GetNewAlarms(request.AlarmDateStart, units);
                result.AlarmsDetails = GetDetailsAlarms(units);
                result.Messages = GetInstantMessages();
                result.UnitsIdWithDisconnectedTransmitter = CheckUnitsWithDisconnectedTransmitter();


                string localizationUnits = string.Join(",",
                                                    (from u in BloodyUser.Current.GetChildUnits(
                                                        BloodyUser.Current.Root.ID,
                                                        PermissionNames.AlarmListing,
                                                        null)
                                                     select u.ID))
                                                     + "," + BloodyUser.Current.Root.ID.ToString();
 

                result.LocalizationAlarms = GetNewLocalizationAlarms(localizationUnits, request.LowestLocalizationAlarmSeverity);
            }

            System.Diagnostics.Debug.WriteLine("pinguserssession duration: " + sw.Elapsed);

            return result;
        }


      

        private void SlideUserSession()
        {
            using (ISession x = NHibernateHttpModule.OpenSession())
            {
                using (ITransaction t = x.BeginTransaction())
                {
                    x.CreateSQLQuery("exec dbo.Users_SlideUserSession @user = :user")
                     .SetInt32("user", User.Current.ID)
                     .UniqueResult();
                    t.Commit();
                }
            }
        }

        private IList<AlarmDetailsDto> GetDetailsAlarms(string units)
        {
            const string sql = "EXEC [dbo].[al_CheckAlarms] :user,:units";
            IList<Hashtable> results = null;
            IList<AlarmDetailsDto> result = new List<AlarmDetailsDto>();

            using(ISession x = NHibernateHttpModule.OpenSession())
            {
                var query = x.CreateSQLQuery(sql);
                query.SetInt32("user", BloodyUser.Current.ID);
                query.SetString("units", null);

                results = query
                   .SetResultTransformer(Transformers.AliasToEntityMap)
                   .List<Hashtable>();
            }

            if(results != null)
            {
                foreach(var item in results)
                {
                    var s = new AlarmDetailsDto()
                    {
                        Id = Convert.ToInt32(item["Id"]),
                        Date = Convert.ToDateTime(item["DateTimeUtc"]).ToLocalTime(),
                        Type = Convert.ToInt32(item["Type"]),
                        TypeDescription = Convert.ToString(item["TypeDescription"]),
                        Severity = Convert.ToByte(item["Severity"]),
                        UnitId = Convert.ToInt32(item["UnitId"]),
                        UnitLocation = Convert.ToString(item["UnitLocation"]),
                        ObjectId = Convert.ToInt32(item["ObjectId"]),
                        ObjectType = Convert.ToInt32(item["ObjectType"]),
                        ObjectName = Convert.ToString(item["ObjectName"]),
                        ObjectUnitId = Convert.ToInt32(item["ObjectUnitId"]),
                        ObjectUnitLocation = Convert.ToString(item["ObjectUnitLocation"]),
                        EndDate = Convert.ToDateTime(item["EndDateUtc"]).ToLocalTime()
                    };

                    result.Add(s);
                }
            }

            return result;
        }

        private IList<AlarmDto> GetNewAlarms(DateTime alarmsDateStart, string units)
        {
            IList<AlarmDto> result = null;

            if (!string.IsNullOrEmpty(units))
            {
                using (ISession x = NHibernateHttpModule.OpenSession())
                {
                    IQuery query = x.GetNamedQuery(AlarmsQueryProvider.CheckAlarms());
                    query.SetDateTime("date_from", alarmsDateStart);
                    query.SetInt32("user", BloodyUser.Current.ID);
                    query.SetString("units", units);
                    var r = query.List<Alarm>();

                    result = Mapper.Map<IList<Alarm>, IList<AlarmDto>>(r);
                }
            }

            return result;
        }



        private IList<LocalizationAlarmDto> GetNewLocalizationAlarms(string localizationUnits, int? lowestSeverity)
        {
            string sql = "EXEC dbo.Alarms_CheckLocalizationAlarms :UserId, :Units, :LowestSeverity";
            IList<LocalizationAlarmDto> output = new List<LocalizationAlarmDto>();

            IList<Hashtable> results = null;


            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                var query = s.CreateSQLQuery(sql);
                query.SetInt32("UserId", BloodyUser.Current.ID);
                query.SetString("Units", localizationUnits);
                query.SetInt32("LowestSeverity", lowestSeverity);
                
                results = query
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            return results.ToList<LocalizationAlarmDto>();
        }


        [CacheResource(120, true)]
        private static PingResultDto PingUserSessionInternal(int userID, DateTime alarmDateStart,
                int? monitoringDeviceId, DateTime? monitoringLastDataDate, string units)
        {
            var request = new PingRequestDto()
                {
                    AlarmDateStart = alarmDateStart,
                    MonitoringDeviceID = monitoringDeviceId,
                    MonitoringLastDataDate = monitoringLastDataDate
                };

            var result = new PingResultDto();

            IList<DisableAlarmUnit> d = null;
            IList<DisableAlarmUnit> o = null;
            IList<Alarm> r = null;
            try
            {
                using (ISession x = NHibernateHttpModule.OpenSession())
                {

                    #region Sprawdzanie aktualnych danych

                    if (request.MonitoringDeviceID.HasValue)
                    {
 
                    }

                    #endregion

                    #region Sprawdzanie Unitów które aktualnie posiadają wyłaczone alarmowanie

                  
                        d = x.CreateSQLQuery("exec dbo.unt_GetDisableAlarmUnits @units = :units")
                             .AddEntity(typeof(DisableAlarmUnit))
                             .SetString("units", units)
                             .SetResultTransformer(new DistinctRootEntityResultTransformer())
                             .List<DisableAlarmUnit>();

                        result.DisableAlarmUnits = Mapper.Map<IList<DisableAlarmUnit>, IList<DisableAlarmUnitDto>>(d);
                    

                    #endregion

                    #region Sprawdzenie czujników z nieaktualnymi danymi
                        o = x.CreateSQLQuery("exec dbo.unt_GetOldDataUnits @clientID = :clientId, @hyper = :hyper, @units = :units")
                                 .AddEntity(typeof(DisableAlarmUnit))
                                 .SetInt32("clientId", BloodyUser.Current.ClientID)
                                 .SetBoolean("hyper", BloodyUser.Current.IsInRole(PermissionNames.HyperAdmin))
                                 .SetString("units", units)
                                 .SetResultTransformer(new DistinctRootEntityResultTransformer())
                                 .List<DisableAlarmUnit>();

                        result.OldDataUnits = Mapper.Map<IList<DisableAlarmUnit>, IList<DisableAlarmUnitDto>>(o);
                    #endregion

                }
                result.Result = 1;
            }
            catch (TransactionException e)
            {
                Logger.Warn("Sql error pinging user session: " + e.Message);
            }

            return result;
        }

        public void CreateUserSession(CreateSessionRequestDto request)
        {
            if (User.Current.Identity.IsAuthenticated)
            {
                using (ISession x = NHibernateHttpModule.OpenSession())
                {
                    using (ITransaction t = x.BeginTransaction())
                    {
                        UserLog ul = new UserLog()
                        {
                            Monitoring = request.Monitoring,
                            UserID = User.Current.ID
                        };

                        x.Save(ul);
                        
                        x.CreateSQLQuery("update Users set LastPingID = :ping where ID = :id")
                            .SetInt32("id", User.Current.ID)
                            .SetInt32("ping", ul.ID)
                            .UniqueResult();

                        x.CreateSQLQuery("dbo.ev_UserLoggedOn @userId = :userId")
                            .SetInt32("userId", User.Current.ID)
                            .UniqueResult();

                        t.Commit();
                    }
                }
            }
        }

        public bool ChangePhone(string phone)
        {
            Account a = null;
            using (ISession x = NHibernateHttpModule.OpenSession())
            {
                a = x.Get<Account>(User.Current.ID);
                a.Phone = phone;
                x.CreateSQLQuery(EditUserPhoneProc)
                    .SetInt32("id", User.Current.ID)
                    .SetString("phone", phone)
                    .UniqueResult();

            }
            return true;
        }

        public bool ChangeEMail(string eMail)
        {
            Account a = null;

            using (ISession x = NHibernateHttpModule.OpenSession())
            {
                a = x.Get<Account>(User.Current.ID);
                a.EMail = eMail;
                x.CreateSQLQuery(EditUserEMailProc)
                    .SetInt32("id", User.Current.ID)
                    .SetString("eMail", eMail)
                    .UniqueResult();

            }

            return true;
        }

        [CacheRefresh("LoadUser")]
        public bool ChangePassword(string pass, string hash)
        {
            bool result = false;
            Account a = null;
            using (ISession x = NHibernateHttpModule.OpenSession())
            {
                a = x.Get<Account>(User.Current.ID);
                if (HashHelper.CalculateHash(pass, a.Password) == hash)
                {
                    a.Password = pass;

                    x.CreateSQLQuery(EditUserPasswordProc)
                        .SetInt32("id", User.Current.ID)
                        .SetString("password", pass)
                        .UniqueResult();
                    result = true;

                    System.Web.HttpContext.Current.Cache.Remove("user_" + a.ID);
                    System.Web.HttpContext.Current.Cache.Remove("LoadUser_" + a.ID);
                }
            }

            return result;
        }

        public void SaveSettings(string settings)
        {
            using (ISession s = NHibernateHttpModule.OpenSession())
            {
                using (ITransaction t = s.BeginTransaction())
                {
                    var u = s.Get<DBUser>(User.Current.ID);
                    if (u != null)
                    {
                        u.Settings = settings;
                        s.Update(u);
                        t.Commit();
                        System.Web.HttpContext.Current.Cache.Remove("user_" + u.ID);
                        System.Web.HttpContext.Current.Cache.Remove("LoadUser_" + u.ID);
                    }
                }
            }
        }

        public string GetLoginNameForId(int userId)
        {
            Account a = null;
            using (ISession x = NHibernateHttpModule.OpenSession())
            {
                using (ITransaction t = x.BeginTransaction())
                {
                    a = x.Get<Account>(userId);
                    if (a != null)
                    {
                        return a.Login;
                    }
                }
            }

            return null;
        }


        public IList<UserContactInfoDto> GetUsersContactsForUnit(int unitId)
        {
            const string GetUsersContactsForUnitProc = "exec dbo.usr_GetUsersContactsForUnit :unitId"; 
            IList<UserContactInfoDto> ranges = null;
            
                using (var s = NHibernateHttpModule.OpenSession())
                {
                    var results = s.CreateSQLQuery(GetUsersContactsForUnitProc)
                        .SetInt32("unitId", unitId)
                        .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<UserContactInfoDto>())
                        .List<UserContactInfoDto>();
                    ranges = results;
                }
            
            return ranges;
        }

        public IList<VersionInfoDto> GetVersions()
        {
            const string GetVersionsProc = "exec dbo.usr_GetVersions";
            IList<VersionInfoDto> versions = null;
            
                using (var s = NHibernateHttpModule.OpenSession())
                {
                    var results = s.CreateSQLQuery(GetVersionsProc)
                        .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<VersionInfoDto>())
                        .List<VersionInfoDto>();
                    versions = results;
                }
            
            return versions;
        }


        public IList<LogEntryDto> GetLogs(DateTime dateStart, DateTime dateEnd)
        {
            const string GetLogsProc = "exec dbo.usr_GetLogs :clientId, :dateStart, :dateEnd";
            IList<LogEntryDto> logs = null;
            
                using (var s = NHibernateHttpModule.OpenSession())
                {
                    var results = s.CreateSQLQuery(GetLogsProc)
                        .SetInt32("clientId", BloodyUser.Current.ClientID)
                        .SetDateTime("dateStart", dateStart)
                        .SetDateTime("dateEnd", dateEnd)
                        .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<LogEntryDto>())
                        .List<LogEntryDto>();
                    logs = results;
                }
            
            return logs;
        }

        

        private IList<string> GetMessages(string query)
        {
            IList<string> messages = null;

            using (var s = NHibernateHttpModule.OpenSession())
            {
                var results = s.CreateSQLQuery(query)
                    .SetInt32("userId", BloodyUser.Current.ID)
                    .List<string>();
                messages = results;
            }

            return messages;
        }

        public IList<string> GetAfterLoginMessages()
        {
            const string GetMessagesProc = "exec dbo.msg_GetAfterLoginMessages :userId";
            return GetMessages(GetMessagesProc);
        }

        private IList<string> GetInstantMessages()
        {
            const string GetMessagesProc = "exec dbo.msg_GetInstantMessages :userId";
            return GetMessages(GetMessagesProc);
        }


        public IList<MessageDto> GetMessages(DateTime dateStart, DateTime dateEnd)
        {
            const string GetMessagesProc = "exec dbo.msg_GetMessages :clientId, :dateStart, :dateEnd";
            IList<MessageDto> messages = null;

            using (var s = NHibernateHttpModule.OpenSession())
            {
                var results = s.CreateSQLQuery(GetMessagesProc)
                    .SetInt32("clientId", BloodyUser.Current.ClientID)
                    .SetDateTime("dateStart", dateStart)
                    .SetDateTime("dateEnd", dateEnd)
                    .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<MessageDto>())
                    .List<MessageDto>();
                messages = results;
            }

            return messages;
        }

        public IList<ReceiverDto> GetMessageReceivers(int messageId)
        {
            const string GetMessageReceiversProc = "exec dbo.msg_GetMessageReceivers :messageId";
            IList<ReceiverDto> receivers = null;

            using (var s = NHibernateHttpModule.OpenSession())
            {
                var results = s.CreateSQLQuery(GetMessageReceiversProc)
                    .SetInt32("messageId", messageId)
                    .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<ReceiverDto>())
                    .List<ReceiverDto>();
                receivers = results;
            }

            return receivers;
        }

        public bool DeleteMessages(string messages)
        {
            const string DeleteMessagesProc = "EXEC [dbo].[msg_DeleteMessages] :messages";

            bool deleted;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                deleted = session.CreateSQLQuery(DeleteMessagesProc)
                    .SetString("messages", messages)
                     .UniqueResult<bool>();
            }

            return deleted;
        }


        public bool CloseMessages(string messages)
        {
            const string CloseMessagesProc = "EXEC [dbo].[msg_CloseMessages] :messages";

            bool closed;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                closed = session.CreateSQLQuery(CloseMessagesProc)
                    .SetString("messages", messages)
                     .UniqueResult<bool>();
            }

            return closed;
        }

        public int CreateMessage(int? clientId, string message, DateTime activeFrom, DateTime activeTo, byte mode)
        {
            const string CreateMessageProc = "EXEC [dbo].[msg_CreateMessage] :clientId, :message, :activeFrom, :activeTo, :mode";
            
            int newMessage = -1;
            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                newMessage = session.CreateSQLQuery(CreateMessageProc)
                     .SetInt32("clientId", clientId)
                     .SetString("message", message)
                     .SetDateTime("activeFrom", activeFrom)
                     .SetDateTime("activeTo", activeTo)
                     .SetByte("mode", mode)
                     .UniqueResult<int>();
            }

            return newMessage;
        }

        public bool DeleteReceivers(int message, string receivers)
        {
            const string DeleteReceiversProc = "EXEC [dbo].[msg_DeleteReceivers] :message, :receivers";
            
            bool deleted;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                deleted = session.CreateSQLQuery(DeleteReceiversProc)
                    .SetInt32("message", message)
                    .SetString("receivers", receivers)
                     .UniqueResult<bool>();
            }

            return deleted;
        }

        public IList<SettingDTO> GetSettings()
        {
            const string GetSettingsProc = "exec dbo.Settings_GetSettings :clientId";
            IList<SettingDTO> settings = null;

            using (var s = NHibernateHttpModule.OpenSession())
            {
                var results = s.CreateSQLQuery(GetSettingsProc)
                    .SetInt32("clientId", BloodyUser.Current.ClientID)
                    .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<SettingDTO>())
                    .List<SettingDTO>();
                settings = results;
            }

            return settings;
        }

        public bool SetSetting(string name, string value, string logEntry, string reason)
        {
            const string SetSettingValueProc = "EXEC [dbo].[Settings_SetSettingValue] :clientId, :name, :value, :operatorId, :logEntry, :reason";

            bool updated = false;
            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                updated = session.CreateSQLQuery(SetSettingValueProc)
                     .SetInt32("clientId", BloodyUser.Current.ClientID)
                     .SetString("name", name)
                     .SetString("value", value)
                     .SetInt32("operatorId", BloodyUser.Current.ID)
                     .SetString("logEntry", logEntry)
                     .SetString("reason", reason)
                     .UniqueResult<bool>();
            }

            return updated;
        }

        public bool ResetSetting(string name, string reason)
        {
            const string ResetSettingProc = "EXEC [dbo].[Settings_ResetSettingValue] :clientId, :name, :operatorId, :reason";

            bool updated = false;
            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                updated = session.CreateSQLQuery(ResetSettingProc)
                     .SetInt32("clientId", BloodyUser.Current.ClientID)
                     .SetString("name", name)
                     .SetInt32("operatorId", BloodyUser.Current.ID)
                     .SetString("reason", reason)
                     .UniqueResult<bool>();
            }

            return updated;
        }

        public IList<LoggedUserDto> GetLoggedUsers()
        {
            const string GetLogsProc = "exec dbo.usr_GetLoggedUsers :clientId";
            IList<LoggedUserDto> users = null;

            using (var s = NHibernateHttpModule.OpenSession())
            {
                var results = s.CreateSQLQuery(GetLogsProc)
                    .SetInt32("clientId", BloodyUser.Current.ClientID)
                    .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<LoggedUserDto>())
                    .List<LoggedUserDto>();
                users = results;
            }

            return users;
        }

        /*public IList<ConfigurationItemDto> GetConfiguration()
        {
            const string GetSettingsProc = "exec dbo.cfg_GetConfiguration :clientId, :userId";
            IList<ConfigurationItemDto> configuration = null;

            using (var s = NHibernateHttpModule.OpenSession())
            {
                var results = s.CreateSQLQuery(GetSettingsProc)
                    .SetInt32("clientId", BloodyUser.Current.ClientID)
                    .SetInt32("userId", BloodyUser.Current.ID)
                    .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<ConfigurationItemDto>())
                    .List<ConfigurationItemDto>();
                configuration = results;
            }

            return configuration;
        }*/

        // Brak pobierania RegistrationId i UnassignedPatientsId.
        public List<SettingItem> GetSettingsT()
        {
            const string GetSettingsProc = "EXEC [dbo].[cfg_ApplicationSettingsGet]";
            List<SettingItem> output = new List<SettingItem>();

            IList<Hashtable> results = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                results = session.CreateSQLQuery(GetSettingsProc)
                    .SetResultTransformer(Transformers.AliasToEntityMap)
                    .List<Hashtable>();
            }

            if(results != null)
            {
                var collDef = results[0];

                foreach(var collumn in collDef.Keys)
                {
                    output.Add(new SettingItem() { Name = collumn.ToString() });
                }

                int i = 0;
                foreach(var item in collDef.Values)
                {
                    // W wartości domyślnej nie może być null, ponieważ
                    //według tej wartości określany jest typ.
                    output[i].Default = Convert.ToString(item);
                    output[i].Type = Convert.ToString(item.GetType());
                    i++;
                }
                i = 0;

                var valuesDef = results[1];

                foreach(var item in valuesDef.Values)
                {
                    if (item != null)
                    {
                        output[i].Value = Convert.ToString(item);
                    }
                    else
                    {
                        output[i].Value = null;
                    }
                    i++;
                }
            }

            return output;
        }

        public string SetSettingsT(string param)
        {
            ///TODO: Błąd przy wysyłaniu.
            const string createBandProc = "EXEC [dbo].[cfg_SetApplicationSettings] :UserId,:Params";

            string result;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                var query = session.CreateSQLQuery(createBandProc)
                    .SetInt32("UserId", BloodyUser.Current.ID)
                    .SetString("Params", param);

                try
                {
                    result = query.UniqueResult<string>();
                }
                catch (Exception ex)
                {
                    result = ex.Message;
                }

                return result;
            }
        }

        public IList<int> CheckUnitsWithDisconnectedTransmitter()
        {
            const string proc = "EXEC [dbo].[al_CheckUnitsWithDisconnectedTransmitter]";

            IList<Hashtable> results = null;
            IList<int> result = null;

            using(ISession session = NHibernateHttpModule.OpenSession())
            {
                var query = session.CreateSQLQuery(proc);

                    results = query
                        .SetResultTransformer(Transformers.AliasToEntityMap)
                        .List<Hashtable>();
            }

            if(results != null)
            {
                if (result == null)
                    result = new List<int>();

                foreach(var item in results)
                {
                    result.Add(Convert.ToInt32(item["UnitID"]));
                }
            }

            return result;
        }

        public IList<SystemSettingDto> GetSystemSettings()
        {
            const string proc = "EXEC [dbo].[dev_SystemSettingsGet]";

            IList<SystemSettingDto> results = null;

            using(ISession session = NHibernateHttpModule.OpenSession())
            {
                var query = session.CreateSQLQuery(proc);

                results = query
                    .SetResultTransformer(NHibernate.Transform.Transformers.AliasToBean<SystemSettingDto>())
                    .List<SystemSettingDto>();
            }

            return results;
        }

        public string SetSystemSettings(SystemSettingDto dto)
        {
            const string proc = "EXEC [dbo].[dev_SystemSettingsSet] :UserId, :NoConnectionFinishingAlarms, :FullMonitoringUnassignedBands, :WarningAutoCloseMessage,:LoBatteryWrActive,:LoBatteryWrMins,:"
                + "LoBatteryAlActive,:LoBatteryAlMins,:BatteryTermWrDaysBefore,:BatteryTermWrMessage,:BatteryTermAlDaysBefore,:BatteryTermAlMessage,:DayStartHour,:DayEndHour,:NoTransmitterWrActive,:NoTransmitterWrMins,:NoTransmitterAlActive,:NoTransmitterAlMins"
                + ",:FirstBandAssignmentReason,:EventToArchiveAfterDays,:_LocalAdminPhone,:MaxPasswordAgeDays,:ShowReasonField,:PingSessionIntervalSecs,:MonitoringSoundIntervalSecs,:RangeColorIntensityPercent,:MaxRwosLogs,:MaxRwosMessages"
                + ",:DefMessageActivityMinutes,:MaxRowsAlarmsPopup,:MaxRowsAlarms,:MaxRowsRawData,:RoleDefForNewUser,:UnitsLocationSeparator,:MaxDaysReports,:MaxDaysRawData,:SearchMessagesDaysBefore,:SearchMessagesDaysAfter"
                + ",:SearchLogsDaysBefore,:SearchLogsDaysAfter,:SearchChartDataHoursBefore,:SearchChartDataHoursAfter,:ChartImageWidth,:ChartImageHeight,:SearchRawDataDaysBefore,:SearchRawDataDaysAfter,:SearchAlarmsDaysBefore,:SearchAlarmsDaysAfter,:ShowUnitId"
                + ",:EnableMonitoringStatusButton,:OldDataTimeMinutes,:OldDataColor,:SqlLoggingIsOn,:Reason";

            IList<Hashtable> results = null;
            string output = String.Empty;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                var result = session.CreateSQLQuery(proc);
                result.SetInt32("UserId", BloodyUser.Current.ID);
                result.SetBoolean("NoConnectionFinishingAlarms", dto.NoConnectionFinishingAlarms);
                result.SetBoolean("FullMonitoringUnassignedBands", dto.FullMonitoringUnassignedBands);
                result.SetString("WarningAutoCloseMessage", dto.WarningAutoCloseMessage);
                result.SetBoolean("LoBatteryWrActive", dto.LoBatteryWrActive);
                result.SetInt16("LoBatteryWrMins", (short)dto.LoBatteryWrMins);
                result.SetBoolean("LoBatteryAlActive", dto.LoBatteryAlActive);
                result.SetInt16("LoBatteryAlMins", (short)dto.LoBatteryAlMins);
                result.SetInt16("BatteryTermWrDaysBefore", (short)dto.BatteryTermWrDaysBefore);
                result.SetString("BatteryTermWrMessage", dto.BatteryTermWrMessage);
                result.SetInt16("BatteryTermAlDaysBefore", (short)dto.BatteryTermAlDaysBefore);
                result.SetString("BatteryTermAlMessage", dto.BatteryTermAlMessage);
                result.SetDateTime("DayStartHour", dto.DayStartHour);
                result.SetDateTime("DayEndHour", dto.DayEndHour);
                result.SetBoolean("NoTransmitterWrActive", dto.NoTransmitterWrActive);
                result.SetInt16("NoTransmitterWrMins", (short)dto.NoTransmitterWrMins);
                result.SetBoolean("NoTransmitterAlActive", dto.NoTransmitterAlActive);
                result.SetInt16("NoTransmitterAlMins", (short)dto.NoTransmitterAlMins);
                result.SetString("FirstBandAssignmentReason", dto.FirstBandAssignmentReason);
                result.SetByte("EventToArchiveAfterDays", (byte)dto.EventToArchiveAfterDays);
                result.SetString("_LocalAdminPhone", dto._LocalAdminPhone);
                result.SetInt32("MaxPasswordAgeDays", dto.MaxPasswordAgeDays);
                result.SetBoolean("ShowReasonField", dto.ShowReasonField);
                result.SetInt32("PingSessionIntervalSecs", dto.PingSessionIntervalSecs);
                result.SetInt32("MonitoringSoundIntervalSecs", dto.MonitoringSoundIntervalSecs);
                result.SetInt32("RangeColorIntensityPercent", dto.RangeColorIntensityPercent);
                result.SetInt32("MaxRwosLogs", dto.MaxRwosLogs);
                result.SetInt32("MaxRwosMessages", dto.MaxRwosMessages);
                result.SetInt32("DefMessageActivityMinutes", dto.DefMessageActivityMinutes);
                result.SetInt32("MaxRowsAlarmsPopup", dto.MaxRowsAlarmsPopup);
                result.SetInt32("MaxRowsAlarms", dto.MaxRowsAlarms);
                result.SetInt32("MaxRowsRawData", dto.MaxRowsRawData);
                result.SetInt32("RoleDefForNewUser", dto.RoleDefForNewUser);
                result.SetString("UnitsLocationSeparator", dto.UnitsLocationSeparator);
                result.SetInt32("MaxDaysReports", dto.MaxDaysReports);
                result.SetInt32("MaxDaysRawData", dto.MaxDaysRawData);
                result.SetInt32("SearchMessagesDaysBefore", dto.SearchMessagesDaysBefore);
                result.SetInt32("SearchMessagesDaysAfter", dto.SearchMessagesDaysAfter);
                result.SetInt32("SearchLogsDaysBefore", dto.SearchLogsDaysBefore);
                result.SetInt32("SearchLogsDaysAfter", dto.SearchLogsDaysAfter);
                result.SetInt32("SearchChartDataHoursBefore", dto.SearchChartDataHoursBefore);
                result.SetInt32("SearchChartDataHoursAfter", dto.SearchChartDataHoursAfter);
                result.SetInt32("ChartImageWidth", dto.ChartImageWidth);
                result.SetInt32("ChartImageHeight", dto.ChartImageHeight);
                result.SetInt32("SearchRawDataDaysBefore", dto.SearchRawDataDaysBefore);
                result.SetInt32("SearchRawDataDaysAfter", dto.SearchRawDataDaysAfter);
                result.SetInt32("SearchAlarmsDaysBefore", dto.SearchAlarmsDaysBefore);
                result.SetInt32("SearchAlarmsDaysAfter", dto.SearchAlarmsDaysAfter);
                result.SetBoolean("ShowUnitId", dto.ShowUnitId);
                result.SetBoolean("EnableMonitoringStatusButton", dto.EnableMonitoringStatusButton);
                result.SetInt32("OldDataTimeMinutes", dto.OldDataTimeMinutes);
                result.SetString("OldDataColor", dto.OldDataColor);
                result.SetBoolean("SqlLoggingIsOn", dto.SqlLoggingIsOn);
                result.SetString("Reason", dto.Reason);

                results = result.SetResultTransformer(Transformers.AliasToEntityMap)
                     .List<Hashtable>();
            }
                
            output = results.GetStringResult("Result", "ErrorMessage");
            return output;
        }
    }
}
