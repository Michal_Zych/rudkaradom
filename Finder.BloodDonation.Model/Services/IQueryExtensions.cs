﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;

namespace Finder.BloodDonation.Model.Services
{
    public static class IQueryExtensions
    {
        public static IQuery SetInt32(this IQuery query, int position, int? val)
        {
            if (val.HasValue)
            {
                query.SetInt32(position, val.Value);
            }
            else
            {
                query.SetParameter(position, null, NHibernateUtil.Int32);
            }

            return query;
        }

        public static IQuery SetInt32(this IQuery query, string name, int? val)
        {
            if (val.HasValue)
            {
                query.SetInt32(name, val.Value);
            }
            else
            {
                query.SetParameter(name, null, NHibernateUtil.Int32);
            }

            return query;
        }

        public static IQuery SetInt16(this IQuery query, int position, short? val)
        {
            if (val.HasValue)
            {
                query.SetInt16(position, val.Value);
            }
            else
            {
                query.SetParameter(position, null, NHibernateUtil.Int16);
            }

            return query;
        }

        public static IQuery SetInt16(this IQuery query, string name, short? val)
        {
            if (val.HasValue)
            {
                query.SetInt16(name, val.Value);
            }
            else
            {
                query.SetParameter(name, null, NHibernateUtil.Int16);
            }

            return query;
        }




        public static IQuery SetByte(this IQuery query, int position, byte? val)
        {
            if (val.HasValue)
            {
                query.SetByte(position, val.Value);
            }
            else
            {
                query.SetParameter(position, null, NHibernateUtil.Byte);
            }

            return query;
        }

        public static IQuery SetByte(this IQuery query, string name, byte? val)
        {
            if (val.HasValue)
            {
                query.SetByte(name, val.Value);
            }
            else
            {
                query.SetParameter(name, null, NHibernateUtil.Byte);
            }

            return query;
        }








        public static IQuery SetInt64(this IQuery query, int position, long? val)
        {
            if(val.HasValue)
            {
                query.SetInt64(position, val.Value);
            }
            else
            {
                query.SetParameter(position, null, NHibernateUtil.Int64);
            }

            return query;
        }

        public static IQuery SetInt64(this IQuery query, string name, long? val)
        {
            if(val.HasValue)
            {
                query.SetInt64(name, val.Value);
            }
            else
            {
                query.SetParameter(name, null, NHibernateUtil.Int64);
            }

            return query;
        }

        public static IQuery SetUInt64(this IQuery query, int position, ulong? val)
        {
            if (val.HasValue)
            {
                query.SetUInt64(position, val.Value);
            }
            else
            {
                query.SetParameter(position, null, NHibernateUtil.UInt64);
            }

            return query;
        }

        public static IQuery SetUInt64(this IQuery query, string name, ulong? val)
        {
            if (val.HasValue)
            {
                query.SetUInt64(name, val.Value);
            }
            else
            {
                query.SetParameter(name, null, NHibernateUtil.UInt64);
            }

            return query;
        }

        public static IQuery SetUInt32(this IQuery query, string name, uint? val)
        {
            if(val.HasValue)
            {
                query.SetUInt32(name, val.Value);
            }
            else
            {
                query.SetParameter(name, null, NHibernateUtil.UInt32);
            }

            return query;
        }

        public static IQuery SetDateTime(this IQuery query, int position, DateTime? val)
        {
            if (val.HasValue)
            {
                query.SetDateTime(position, val.Value);
            }
            else
            {
                query.SetParameter(position, null, NHibernateUtil.DateTime);
            }

            return query;
        }

        public static IQuery SetDateTime(this IQuery query, string name, DateTime? val)
        {
            if (val.HasValue && val.Value > DateTime.MinValue)
            {
                query.SetDateTime(name, val.Value);
            }
            else
            {
                query.SetParameter(name, null, NHibernateUtil.DateTime);
            }

            return query;
        }

        public static IQuery SetBoolean(this IQuery query, int position, bool? val)
        {
            if (val.HasValue)
            {
                query.SetBoolean(position, val.Value);
            }
            else
            {
                query.SetParameter(position, null, NHibernateUtil.Boolean);
            }

            return query;
        }

        public static IQuery SetBoolean(this IQuery query, string name, bool? val)
        {
            if (val.HasValue)
            {
                query.SetBoolean(name, val.Value);
            }
            else
            {
                query.SetParameter(name, null, NHibernateUtil.Boolean);
            }

            return query;
        }
    }

}
