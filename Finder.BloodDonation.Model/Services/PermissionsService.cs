﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Finder.BloodDonation.Model.Domain;
using NHibernate;
using NHibernate.Linq;
using FinderFX.Web.Core.Communication;
using AutoMapper;
using System.ServiceModel.Activation;
using FinderFX.Web.Core.Logging;
using FinderFX.Web.Core.DomainServices;
using Finder.BloodDonation.Model.Dto;
using FinderFX.Web.Core.Authentication;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Model.Enum;
using NHibernate.Criterion;
using FinderFX.SL.Core.Authentication;
using NHibernate.Mapping;
using NHibernate.Transform;

namespace Finder.BloodDonation.Model.Services
{
    [ServiceContract]
    public interface IPermissionsService
    {
        //[OperationContract]
        //IList<UnitDTO> GetAllUnitsForClient(int clientID);
        //[OperationContract]
        IList<DBRole> GetAllRoles();
        [OperationContract]
        IList<DBRoleDTO> GetAllRolesDTO();

        [OperationContract]
        IList<UserUnitRole> GetUserUnitsRolesByUser();

        //[OperationContract]
        //IList<UserUnitRole> GetUserUnitsRoles(int userID);
        [OperationContract]
        IList<UserUnitRoleDTO> GetUserUnitsRoles(int userID, int roleID);

        [OperationContract]
        DataAdministration GetDataToAdministration();

        [OperationContract]
        IList<DBUserDTO> GetUsers(int clientID, int unitID);

        [OperationContract]
        IList<DBUserDTO> GetUsersById(int[] UsersId);

        [OperationContract]
        bool EditUserPermissions(RequestSetUserPermissions request, string reason, string logInfo);

        [OperationContract]
        bool SetDefaultUser(int userID, int unitID, bool isDefault);

        [OperationContract]
        IList<UserPermissionsResponse> GetPermissions(UserPermissionsRequest[] request);

        [OperationContract]
        IList<UserPermissionsResponse> GetPermissionsByUser(int userId);
    }

    [LogDetails]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class PermissionsService : IPermissionsService, IDomainService
    {
        private const string GetRolesThatCanBeManaged = "EXEC [dbo].[Role_GetRolesThatCanBeManagedByUser] :UserId";
        private const string GetActiveUsersUnitsRolesProc = "EXEC [dbo].[GetActiveUsersUnitsRoles]";
        private const string AddUserRoleProc = "EXEC [dbo].[Users_AddUserUnitRole] :userId, :unitId, :roleId";
        private const string GetUserDescendantsAndSiblingsProc = "EXEC [dbo].[User_GetDescendantsAndSiblingsProc] :userId";
        private const string GetClientUnitsProc = "EXEC [dbo].[Units_GetClientUnits] :clientId";
        private const string EditUserPermissionsPros = "EXEC [dbo].[usr_EditUserPermissions] :userId, :roleId, :units";

        public IList<UnitDTO> GetAllUnitsForClient(int clientID)
        {
            IList<Unit> units;

            //			using (ISession session = NHibernateHttpModule.OpenSession())
            //			{
            //				units = session.CreateCriteria<Unit>()
            //    //                .Add(Expression.Eq("Active", true))
            //					.SetFetchMode("ParentUnit", FetchMode.Eager)
            //					.List<Unit>();
            //			}


            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                units = session.CreateSQLQuery(GetClientUnitsProc)
                    .AddEntity(typeof(Unit))
                    .SetInt32("clientId", clientID)
                    .SetResultTransformer(new DistinctRootEntityResultTransformer())
                    .List<Unit>();
            }

            foreach (Unit u in units)
            {
                UnitsService.GetTree(u.ID, units);
            }

            return Mapper.Map<IList<Unit>, IList<UnitDTO>>(units);
        }

        public IList<DBRole> GetAllRoles()
        {
            return GetAllRoles(true);
        }

        public IList<DBRole> GetAllRoles(bool filterRoles)
        {
            IList<DBRole> roles;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                roles = (from r in session.Linq<DBRole>()
                         where r.ApplicationID == 1
                         select r).ToList();

                if (filterRoles)
                {
                    List<int> availableRoles = BloodyUser.Current.GetUserDBRoles();
                    List<int> availableRolesWithChildren = availableRoles.ToList();

                    foreach (int r in availableRoles)
                    {
                        GetChildRoles(r, availableRolesWithChildren, roles);
                    }


                    //zwracaj tylko role, ktore ma dany uzytkownik
                    for (int i = roles.Count - 1; i >= 0; i--)
                    {
                        if (!availableRolesWithChildren.Contains(roles[i].ID))
                        {
                            roles.RemoveAt(i);
                        }
                    }
                }

                foreach (DBRole r in roles)
                {
                    //eager loading :)
                    int count = r.Permissions.Count;
                }
            }

            return roles;

        }

        private void GetChildRoles(int parentRoleID, List<int> availableRoles, IList<DBRole> allRoles)
        {
            IEnumerable<DBRole> roles = allRoles.Where(x => x.ParentRoleID.HasValue && x.ParentRoleID == parentRoleID);
            if (roles == null)
                return;

            foreach (DBRole r in roles)
            {
                if (!availableRoles.Contains(r.ID))
                {
                    availableRoles.Add(r.ID);
                    GetChildRoles(r.ID, availableRoles, allRoles);
                }
            }
        }

        public DataAdministration GetDataToAdministration()
        {
            DataAdministration toReturn = null;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                IList<DBRole> roles = GetRoleToAdministration(session);

                toReturn = new DataAdministration()
                               {
                                   AllRoles = Mapper.Map<IList<DBRole>, IList<DBRoleDTO>>(roles),
                               };
            }
            return toReturn;
        }

        private IList<DBRole> GetRoleToAdministration(ISession session)
        {
            IList<DBRole> roles = null;
            roles = session.CreateSQLQuery(GetRolesThatCanBeManaged)
                .SetInt32("UserId", BloodyUser.Current.ID)
                .SetResultTransformer(new DbRoleTransformer())
                .List<DBRole>().ToList();

            return roles; //Mapper.Map<IList<DBRole>, IList<DBRoleDTO>>(roles);
        }


        public IList<DBRoleDTO> GetAllRolesDTO()
        {
            return Mapper.Map<IList<DBRole>, IList<DBRoleDTO>>(GetAllRoles());
        }

        public IList<UserUnitRole> GetUserUnitsRolesByUser()
        {
            IList<UserUnitRole> unitRoles = null;
            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                /*string sql = "EXEC [dbo].[usr_GetUserUnitsRoles] :userId";
                    unitRoles = session.CreateSQLQuery(sql)
                    .SetInt32("userId", BloodyUser.Current.ID)
					.SetResultTransformer(Transformers.AliasToBean<UserUnitRole>())
					.List<UserUnitRole>();
                */
                unitRoles = session.CreateSQLQuery(GetActiveUsersUnitsRolesProc)
                    .SetResultTransformer(Transformers.AliasToBean<UserUnitRole>())
                    .List<UserUnitRole>();

            }
            return unitRoles;
        }

        public IList<UserUnitRole> GetUserUnitsRoles(int userID)
        {
            IList<UserUnitRole> unitRoles;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                unitRoles = (from r in session.Linq<UserUnitRole>()
                             where r.UserID == userID
                             select r).ToList();
            }

            return unitRoles;
        }

        public IList<UserUnitRoleDTO> GetUserUnitsRoles(int userID, int roleID)
        {
            IList<UserUnitRole> unitRoles;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                unitRoles = (from r in session.Linq<UserUnitRole>()
                             where r.UserID == userID && r.RoleID == roleID
                             select r).ToList();
            }

            return Mapper.Map<IList<UserUnitRole>, IList<UserUnitRoleDTO>>(unitRoles);
        }

        [LogDetails(Message = "Błąd podczas pobierania użytkowników", AttributeReplace = true)]
        public IList<DBUserDTO> GetUsers(int clientID, int unitID)
        {
            BloodyUser user = User.Current as BloodyUser;
            return GetUsers(clientID, unitID, user);
        }

        public IList<DBUserDTO> GetUsers(int clientID, int unitID, BloodyUser user)
        {
            IList<DBUser> users;

            UnitDTO unit = user.GetUnit(unitID, PermissionNames.UnitsPermissions);
            /*
            if (unit == null)
                throw new InvalidOperationException("Unit cannot be null");
            if (unit.UnitType != Enum.UnitType.ROOT && unit.UnitType != Enum.UnitType.CENTRE
                && unit.UnitType != Enum.UnitType.DEPARTMENT)
                throw new InvalidOperationException("Unit must be of type Root, Centre or Department");


            IList<UnitDTO> departments;
            if (unit.UnitType == UnitType.DEPARTMENT)
            {
                departments = new List<UnitDTO> { unit };
            }
            else
            {
                departments = user
                    .GetChildUnits(unitID, PermissionNames.UnitsPermissions, new List<UnitType>() { UnitType.DEPARTMENT });
            };

            */

            IList<UnitDTO> departments = null;

            if (departments == null)
                departments = new List<UnitDTO>();


            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                //users = (from r in session.Linq<DBUser>()
                //             where r.ClientID == clientID && r.UnitID == unitID
                //             select r).ToList();

                users = session.CreateCriteria<DBUser>()
                    //niepotrzebny warunek
                    //.Add(Expression.Eq("ClientID", clientID))
                    .Add(Expression.In("UnitID",
                        departments.Select<UnitDTO, int>(x => x.ID).ToArray()))
                    .List<DBUser>();


                //eager fetching :)
                foreach (DBUser u in users)
                {
                    //jezeli pytamy o uzytkownkow oddzialu
                    //to zwroc dodatkowo domyslne jednostki
                    if (unit.UnitType == UnitType.DEPARTMENT)
                    {
                        int count = u.DefaultUnits.Count;

                        DefaultUsersUnits def = u.DefaultUnits.FirstOrDefault(x => x.UnitID == unitID);
                        List<DefaultUsersUnits> list = new List<DefaultUsersUnits>();
                        if (def != null)
                            list.Add(def);
                        u.DefaultUnits = list;
                    }
                    foreach (DBRole r in u.Roles)
                    {
                        int count2 = r.Permissions.Count;
                    }
                }
            }

            users = users.Where(x => x.ID != user.ID).ToList();

            return Mapper.Map<IList<DBUser>, IList<DBUserDTO>>(users);
        }

        public class InnerUserRole
        {
            public int UserID { get; set; }
            public int RoleID { get; set; }
        }


        public bool SetDefaultUser(int userID, int unitID, bool isDefault)
        {
            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                DefaultUsersUnits def = new DefaultUsersUnits();
                def.UnitID = unitID;
                def.UserID = userID;

                if (isDefault)
                {
                    session.Save(def);
                }
                else
                {
                    session.Delete(def);
                }

                session.Flush();
            }

            return true;
        }

        public IList<DBUserDTO> GetUsersById(int[] UsersId)
        {
            IList<DBUserDTO> toReturn = new List<DBUserDTO>();

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                foreach (var id in UsersId)
                {
                    var user = session.Get<DBUser>(id);
                    toReturn.Add(Mapper.Map<DBUser, DBUserDTO>(user));
                }
            }

            return toReturn;
        }


        public bool EditUserPermissions(RequestSetUserPermissions request, string reason, string logInfo)
        {
            const string EditUserPermissionssProc = "EXEC [dbo].[usr_EditUserPermissions] :userId, :roleId, :units, :operatorId, :logEntry, :reason";

            bool result = false;

            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                session.CreateSQLQuery(EditUserPermissionssProc)
                        .SetInt32("userId", request.UserId)
                        .SetInt32("roleId", request.RoleId)
                        .SetString("units", request.Units.ToCommaSeparatedValues())
                        .SetInt32("operatorId", BloodyUser.Current.ID)
                        .SetString("logEntry", logInfo)
                        .SetString("reason", reason)
                        .UniqueResult();

                System.Web.HttpContext.Current.Cache.Remove("user_" + request.UserId);
                System.Web.HttpContext.Current.Cache.Remove("LoadUser_" + request.UserId);

                result = true;
            }


            return result;
        }


        public IList<UserPermissionsResponse> GetPermissions(UserPermissionsRequest[] request)
        {
            if (request == null || !request.Any())
                return null;
            IList<UserPermissionsResponse> responses = new List<UserPermissionsResponse>();


            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                UserPermissionsResponse response;
                foreach (var r in request)
                {
                    response = new UserPermissionsResponse();
                    var user = session.Get<Account>(r.UserId);
                    var role = session.Get<DBRole>(r.RoleId);

                    r.UnitId.ToList().ForEach(unit => response.Units.Add(Mapper.Map<Unit, UnitDTO>(session.Get<Unit>(unit))));

                    response.User = Mapper.Map<Account, AccountDto>(user);
                    response.Role = Mapper.Map<DBRole, DBRoleDTO>(role);

                    responses.Add(response);
                }
            }
            return responses;
        }

        public IList<UserPermissionsResponse> GetPermissionsByUser(int userId)
        {
            IList<UserPermissionsResponse> responses = new List<UserPermissionsResponse>();
            using (ISession session = NHibernateHttpModule.OpenSession())
            {
                var usersUnitsRoles = session.CreateSQLQuery(GetUserDescendantsAndSiblingsProc)
                    .SetInt32("userId", userId)
                    .SetResultTransformer(Transformers.AliasToBean<UserUnitRole>())
                    .List<UserUnitRole>();


                UserPermissionsResponse response;
                foreach (var r in usersUnitsRoles)
                {
                    try
                    {
                        //.SetResultTransformer(Transformers.AliasToBean<PrepHistoryDto>())
                        response = new UserPermissionsResponse();
                        var user = session.Get<Account>(r.UserID);
                        var role = session.Get<DBRole>(r.RoleID);

                        var list = usersUnitsRoles.Where(uur => uur.UserID == r.UserID)
                            .Select(uur => uur.UnitID)
                            .ToList();


                        var sql = "SELECT ID, Name, UnitTypeID, ParentUnitID,ClientID, UnitTypeIDv2, PictureID, Description, dbo.unt_IsMonitored(ID) as IsMonitored FROM dbo.Units WHERE Id = :unitID";
                        Unit unit;
                        foreach (var unitID in list)
                        {
                            unit = session.CreateSQLQuery(sql)
                                .AddEntity(typeof(Unit))
                                .SetInt32("unitID", unitID)
                                .SetResultTransformer(new DistinctRootEntityResultTransformer())
                                .UniqueResult<Unit>();
                            response.Units.Add(Mapper.Map<Unit, UnitDTO>(unit));
                        }


                        response.User = Mapper.Map<Account, AccountDto>(user);
                        response.Role = Mapper.Map<DBRole, DBRoleDTO>(role);

                        responses.Add(response);
                    }
                    catch (Exception e)
                    {
                        //throw new Exception("User: " + r.UserID.ToString() + " hasło nie może być puste", e);
                    }
                }
            }
            return responses;
        }
    }
}