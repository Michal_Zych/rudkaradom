﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using FinderFX.Web.Core.Logging;
using System.ServiceModel.Activation;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Model.Domain;
using NHibernate;
using FinderFX.Web.Core.Communication;
using AutoMapper;
using FinderFX.Web.Core.DomainServices;
using FinderFX.Web.Core.Authentication;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Model.Enum;
using NHibernate.Criterion;
using NHibernate.Transform;
using NHibernate.Linq;

namespace Finder.BloodDonation.Model.Services
{
    [ServiceContract]
    public interface IHubsService
    {
                [OperationContract]
        UnitDTO GetUnitsForHubs();




        [OperationContract]
        IList<CommunicationLogDto> GetCommunicationLogs(int hub, DateTime date_from, DateTime date_to, string find);

        [OperationContract]
        IList<int> GetHubTakenAddresses(int hubID);
    }

    [LogDetails]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class HubsService : IHubsService, IDomainService
    {
       

        

        public UnitDTO GetUnitsForHubs()
        {
            BloodyUser user = User.Current as BloodyUser;

            return GetUnitsForHubs(user);
        }

        private UnitDTO GetUnitsForHubs(BloodyUser user)
        {
            return null;
            /*
            UnitDTO root = user.Root;

            //podajac liste typow uwazac na to by typy na drzewku byly
            //obok siebie inaczej nie zbuduje poprawnie drzewa 
            IList<UnitDTO> list = user.GetChildUnits(
                root.ID,
                PermissionNames.HubsListing,
                new List<UnitType> { UnitType.DEPARTMENT });

            UnitsService proxy = new UnitsService();
            List<UnitDTO> centres = new List<UnitDTO>();
            foreach (UnitDTO u in list)
            {
                UnitDTO centre = centres.FirstOrDefault(x => x.ID == u.ParentUnitID.Value);
                if (centre == null)
                {
                    centre = proxy.GetUnitHierarchy(u.ParentUnitID.Value);
                    centres.Add(centre);
                }
            }

            foreach (UnitDTO u in centres)
            {
                list.Add(u);
            }

            list.Add(root);

            root = UnitsService.GetTree(root.ID, list);

            return root;
             */
        }

        public IList<int> GetHubTakenAddresses(int hubID)
        {
            IList<int> taken = null;

            using (ISession x = NHibernateHttpModule.OpenSession())
            {
                taken = (from d in x.Linq<Device>()
                         where d.HubID == hubID
                         select d.Address).ToList();
            }

            return taken;
        }


        public IList<CommunicationLogDto> GetCommunicationLogs(int hub, DateTime date_from, DateTime date_to, string find)
        {
            IList<CommunicationLog> added = null;

            using (ISession x = NHibernateHttpModule.OpenSession())
            {
                var sql = @"exec Hubs_GetCommunicationLogs @hub = :hub, @date_from = :date_from, @date_to= :date_to, @find = :find";

                added = x.CreateSQLQuery(sql)
                    .AddEntity(typeof(CommunicationLog))
                    .SetInt32("hub", hub)
                    .SetDateTime("date_from", date_from)
                    .SetDateTime("date_to", date_to)
                    .SetString("find", find)
                    .SetResultTransformer(new DistinctRootEntityResultTransformer())
                    .List<CommunicationLog>();
            }

            return Mapper.Map<IList<CommunicationLog>, IList<CommunicationLogDto>>(added);
        }
    }
}
