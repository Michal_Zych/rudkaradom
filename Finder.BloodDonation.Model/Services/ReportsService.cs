﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finder.BloodDonation.Model.Domain;
using System.ServiceModel;
using System.ServiceModel.Activation;
using FinderFX.Web.Core.DomainServices;
using NHibernate;
using FinderFX.Web.Core.Communication;
using NHibernate.Transform;
using FinderFX.Web.Core.Logging;
using AutoMapper;
using NHibernate.Linq;
using Finder.BloodDonation.Model.Dto;
using FinderFX.Web.Core.Authentication;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Model.Services.Tools;
using Finder.BloodDonation.Helpers;

namespace Finder.BloodDonation.Model.Services
{
    [ServiceContract]
    public interface IReportsService
    {
        [OperationContract]
        IList<ReportDto> GetReportsList();

        [OperationContract]
        UserReportDto PrepareReport(int report, int objectId, DateTime date_from, DateTime date_to, bool hideCharts, bool isMono);

        [OperationContract]
        UserReportDto PrepareReportForPrep(int report, long prepId, DateTime date_from, DateTime date_to, bool hideCharts);

	    [OperationContract]
        UserReportDto PrepareReportForUnit(int report, int unitId, int unitSource, DateTime dateFrom, DateTime dateTo, bool hideCharts);

    }

    [LogDetails]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class ReportsService : IReportsService, IDomainService
    {

       // private IUnitsIDsListBuilder unitsBuilder = new UnitsIDsListBuilder();

        public IList<ReportDto> GetReportsList()
        {
            IList<Report> r = null;

            using (ISession x = NHibernateHttpModule.OpenSession())
            {
                r = (from h in x.Linq<Report>() select h).ToList();
            }

            return Mapper.Map<IList<Report>, IList<ReportDto>>(r);
        }

        UserReport ur = null;

        public UserReportDto PrepareReport(int report, int objectId, DateTime date_from, DateTime date_to, bool hideCharts, bool isMono)
        {
                using (ISession x = NHibernateHttpModule.OpenSession())
                {
                    var unitsBuilder = new ReportsUnitsIDsListBuilder();
                    //var units = unitsBuilder.GetIDs(ObjectId, unitSource);

                    using (ITransaction t = x.BeginTransaction())
                    {
                        ISQLQuery query = x.CreateSQLQuery("exec rep_PrepareReport @userid=:userid, @reportid=:reportid,@objectId=:objectId, @dateFrom=:dateFrom, @dateTo=:dateTo, @hideCharts=:hideCharts, @isMono=:isMono");
                        query.SetInt32("userid", BloodyUser.Current.ID);
                        query.SetInt32("reportid", report);
                        query.SetInt32("objectId", objectId);
                        query.SetDateTime("dateFrom", date_from);
                        query.SetDateTime("dateTo", date_to);
                        query.SetBoolean("hideCharts", hideCharts);
                        query.SetBoolean("isMono", isMono);
                        object r = query.UniqueResult();
                        t.Commit();
                    }

                    ur = (from u in x.Linq<UserReport>() where u.UserID == BloodyUser.Current.ID && u.ReportID == report select u).FirstOrDefault();
                }
            return Mapper.Map<UserReport, UserReportDto>(ur);
        }

        public UserReportDto PrepareReportForUnit(int report, int unitId, int unitSource, DateTime dateFrom, DateTime dateTo, bool hideCharts)
		{
            var unitsBuilder = new ReportsUnitsIDsListBuilder();
            var units = unitsBuilder.GetIDs(unitId, unitSource);
            
            using (var x = NHibernateHttpModule.OpenSession())
			{
            	using (var t = x.BeginTransaction())
				{
                    var query = x.CreateSQLQuery("exec Reports_PrepareReportForUnit @userid=:userid, @reportid=:reportid,@units=:units, @date_from=:date_from, @date_to=:date_to, @hide_charts=:hide_charts");
					query.SetInt32("userid", BloodyUser.Current.ID);
					query.SetInt32("reportid", report);
					query.SetString("units", units);
					query.SetDateTime("date_from", dateFrom);
					query.SetDateTime("date_to", dateTo);
                    query.SetBoolean("hide_charts", hideCharts);
					var r = query.UniqueResult();
					t.Commit();
				}

				ur = (from u in x.Linq<UserReport>() where u.UserID == BloodyUser.Current.ID && u.ReportID == report select u).FirstOrDefault();
			}

			return Mapper.Map<UserReport, UserReportDto>(ur);
		}

        public UserReportDto PrepareReportForPrep(int report, long prepId, DateTime date_from, DateTime date_to, bool hideCharts)
        {
            using (ISession x = NHibernateHttpModule.OpenSession())
            {
                using (ITransaction t = x.BeginTransaction())
                {
                    ISQLQuery query = x.CreateSQLQuery("exec Reports_PrepareReportForPrep @userid=:userid, @reportid=:reportid,@preps=:preps, @date_from=:date_from, @date_to=:date_to, @hide_charts=:hide_charts");
                    query.SetInt32("userid", User.Current.ID);
                    query.SetInt32("reportid", report);
                    query.SetString("preps", prepId.ToString());
                    query.SetDateTime("date_from", date_from);
                    query.SetDateTime("date_to", date_to);
                    query.SetBoolean("hide_charts", hideCharts);
                    object r = query.UniqueResult();
                    t.Commit();
                }

                ur = (from u in x.Linq<UserReport>() where u.UserID == BloodyUser.Current.ID && u.ReportID == report select u).FirstOrDefault();
            }

            return Mapper.Map<UserReport, UserReportDto>(ur);
        }
    }
}
