﻿using Finder.BloodDonation.Model.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Model.Services
{
    public static class ListsConverter
    {
        public static List<BandParameter> ToList(string input)
        {
            const string SEPARATOR_ESCAPE = "&comma;";
            const string SEPARATOR = ",";


            List<BandParameter> parameters = new List<BandParameter>();

            if (!String.IsNullOrEmpty(input))
            {
                var text = input.Split(new string[] { SEPARATOR }, StringSplitOptions.None);

                for (int i = 0; i < text.Length - 1; i += 4)
                {
                    BandParameter bandParam = new BandParameter()
                    {
                        Key = text[i + 1].Replace(SEPARATOR_ESCAPE, SEPARATOR),
                        Description = text[i + 2].Replace(SEPARATOR_ESCAPE, SEPARATOR),
                        Value = text[i + 3].Replace(SEPARATOR_ESCAPE, SEPARATOR)
                        
                    };

                    parameters.Add(bandParam);
                }
            }

            return parameters;
        }

        public static List<Contact> QueryContactToList(this string input)
        {
            const string SEPARATOR_ESCAPE = "&comma;";
            const string SEPARATOR = ",";

            List<Contact> contacts = new List<Contact>();

            if (!String.IsNullOrEmpty(input))
            {
                var text = input.Split(new string[] { SEPARATOR }, StringSplitOptions.None);

                for (int i = 0; i < text.Length - 1; i += 3)
                {
                    Contact contact = new Contact();
                    contact.SequenceNr = Convert.ToByte(text[i + 0].Replace(SEPARATOR_ESCAPE, SEPARATOR));
                    contact.Phone = text[i + 1].Replace(SEPARATOR_ESCAPE, SEPARATOR);
                    contact.Description = text[i + 2].Replace(SEPARATOR_ESCAPE, SEPARATOR);

                    contacts.Add(contact);
                }
            }

            return contacts;
        }

        public static List<UnitDTO> QueryProhibitedZoneToList(this string input)
        {
            const string SEPARATOR_ESCAPE = "&comma;";
            const string SEPARATOR = ",";


            List<UnitDTO> prohibitedZones = new List<UnitDTO>();

            if (!String.IsNullOrEmpty(input))
            {
                var text = input.Split(new string[] { SEPARATOR }, StringSplitOptions.None);

                for (int i = 0; i < text.Length - 1; i ++)
                {
                    UnitDTO prohibitedZone = new UnitDTO()
                    {
                        ID = Convert.ToInt32(text[i].Replace(SEPARATOR_ESCAPE, SEPARATOR)),
                        //Name = text[i + 2].Replace(SEPARATOR_ESCAPE, SEPARATOR),
                        //Description = text[i + 4].Replace(SEPARATOR_ESCAPE, SEPARATOR)
                    };

                    prohibitedZones.Add(prohibitedZone);
                }
            }

            return prohibitedZones;
        }

        public static string ToStringQuery(this List<Contact> input)
        {
            const string SEPARATOR = ",";
            const string SEPARATOR_ESCAPE = "&comma;";

            StringBuilder result = new StringBuilder();

            if (input.Count > 0)
            {
                foreach (Contact contact in input)
                {
                    result.Append(contact.Phone).Append(SEPARATOR)
                          .Append(contact.Description.Replace(SEPARATOR, SEPARATOR_ESCAPE))
                          .Append(SEPARATOR);
                }
                
                if (!result.Equals(String.Empty))
                    result.Remove(result.Length - 1, 1);
            }

            return result.ToString();
        }

        public static string ToStringQuery(this List<UnitDTO> input)
        {
            const string SEPARATOR = ",";
            const string SEPARATOR_ESCAPE = "&comma;";
            StringBuilder result = new StringBuilder();
            
            if (input.Count > 0)
            {
                foreach (UnitDTO unit in input)
                {
                    result//.//Append(unit.ID).Append(SEPARATOR).Append(SEPARATOR)
                          //.Append(unit.Name.Replace(SEPARATOR, SEPARATOR_ESCAPE)).Append(SEPARATOR)
                          //.Append("4").Append(SEPARATOR)
                          .Append(unit.ID).Append(SEPARATOR)
                          //.Append(unit.Description.Replace(SEPARATOR, SEPARATOR_ESCAPE))
                          .Append(SEPARATOR);
                }

                //TODO: Poprawić..

                if (!result.Equals(String.Empty))
                    result.Remove(result.Length - 1, 1);
            }

            return result.ToString();
        }

        public static string ToString(List<BandParameter> input)
        {
            const string SEPARATOR = ",";
            const string SEPARATOR_ESCAPE = "&comma;";

            var items = from item in input
                        select item.Key.Replace(SEPARATOR, SEPARATOR_ESCAPE) + SEPARATOR 
                        + item.Description.Replace(SEPARATOR, SEPARATOR_ESCAPE) + SEPARATOR
                        + item.Value.Replace(SEPARATOR, SEPARATOR_ESCAPE);


            string output = string.Join(SEPARATOR, items);

            return output;
        }

        public static string ToList(List<Contact> input)
        {
            const string SEPARATOR = ",";
            const string SEPARATOR_ESCAPE = "&comma;";

            var items = from item in input
                        select item.SequenceNr.ToString().Replace(SEPARATOR, SEPARATOR_ESCAPE) + SEPARATOR
                        + item.Description.Replace(SEPARATOR, SEPARATOR_ESCAPE) + SEPARATOR
                        + item.Phone.Replace(SEPARATOR, SEPARATOR_ESCAPE);

            string output = string.Join(SEPARATOR, items);

            return output;
        }
    }
}
