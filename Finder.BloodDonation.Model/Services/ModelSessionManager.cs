﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FinderFX.Web.Core.Communication;
using Finder.BloodDonation.Model.Domain;
using AutoMapper;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using System.Configuration;
using Finder.BloodDonation.Model.Dto;

namespace Finder.BloodDonation.Model.Services
{
    public class ModelSessionManager : SessionManagerBase
    {
        public override void SetMappings(FluentNHibernate.Cfg.MappingConfiguration m)
        {
            Mapper.CreateMap<Unit, UnitDTO>()
                .ForMember(dest => dest.Children, opts => opts.ResolveUsing<NHibernateResolver>()
                         .FromMember(src => src.Children))
                ;
            Mapper.CreateMap<UnitDTO, Unit>()
                .ForMember(dest => dest.Children, opts => opts.Ignore());

            Mapper.CreateMap<DefaultUsersUnits, DefaultUsersUnitsDTO>();


            Mapper.CreateMap<DBUser, DBUserDTO>()
                //.ForMember(dest => dest.Roles, opts => opts.Ignore())
                .ForMember(dest => dest.DefaultUnits, opts => opts.ResolveUsing<NHibernateResolver>()
                         .FromMember(src => src.DefaultUnits))
                ;
            Mapper.CreateMap<DBUserDTO, DBUser>();

            Mapper.CreateMap<DBRole, DBRoleDTO>();

            Mapper.CreateMap<DBRole, DBRoleDTO>();
            Mapper.CreateMap<DBPermission, DBPermissionDTO>();

            Mapper.CreateMap<Alarm, AlarmDto>();
            Mapper.CreateMap<AlarmDto, Alarm>();

            Mapper.CreateMap<CurrentData, CurrentDataDto>();
            Mapper.CreateMap<CurrentDataDto, CurrentData>();

            Mapper.CreateMap<Data, DataDto>();
            Mapper.CreateMap<DataDto, Data>();

            Mapper.CreateMap<UserUnitRole, UserUnitRoleDTO>();
            Mapper.CreateMap<UserUnitRoleDTO, UserUnitRole>();

            Mapper.CreateMap<FavoriteUnit, FavoriteUnitDTO>();
            Mapper.CreateMap<FavoriteUnitDTO, FavoriteUnit>();

            Mapper.CreateMap<Preview, PreviewDto>();
            Mapper.CreateMap<PreviewDto, Preview>();

            Mapper.CreateMap<DisableAlarmUnit, DisableAlarmUnitDto>();
            Mapper.CreateMap<DisableAlarmUnitDto, DisableAlarmUnit>();

            Mapper.CreateMap<UnitPreview, UnitPreviewDto>();
            Mapper.CreateMap<UnitPreviewDto, UnitPreview>();

            Mapper.CreateMap<Report, ReportDto>();
            Mapper.CreateMap<ReportDto, Report>();

            Mapper.CreateMap<EditPreview, EditPreviewDto>();
            Mapper.CreateMap<EditPreviewDto, EditPreview>();

            Mapper.CreateMap<UserReport, UserReportDto>();
            Mapper.CreateMap<UserReportDto, UserReport>();

            Mapper.CreateMap<Account, AccountDto>();
            Mapper.CreateMap<AccountDto, Account>();

            Mapper.CreateMap<OmmitedAlarmRange, OmittedAlarmRangeDto>();
            Mapper.CreateMap<OmittedAlarmRangeDto, OmmitedAlarmRange>();


            Mapper.CreateMap<CommunicationLog, CommunicationLogDto>();
            Mapper.CreateMap<CommunicationLogDto, CommunicationLog>();

                        m.FluentMappings.AddFromAssemblyOf<Unit>();
            m.HbmMappings.AddFromAssemblyOf<Unit>();
        }

        public override FluentNHibernate.Cfg.FluentConfiguration GetConfigurationWithoutMappings()
        {
            FluentConfiguration cfg = Fluently.Configure().
               Database(MsSqlConfiguration.MsSql2005.ConnectionString(
               c => c.FromConnectionStringWithKey(ConfigurationManager.AppSettings["NHibernateConnection"])).
               DefaultSchema("dbo"));

            return cfg;
        }
    }
}
