﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using FinderFX.Web.Core.Logging;
using System.ServiceModel.Activation;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Model.Domain;
using NHibernate;
using FinderFX.Web.Core.Communication;
using AutoMapper;
using FinderFX.Web.Core.DomainServices;
using FinderFX.Web.Core.Authentication;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Model.Enum;
using NHibernate.Criterion;
using NHibernate.Transform;
using NHibernate.Linq;

namespace Finder.BloodDonation.Model.Services
{
    [ServiceContract]
    public interface ICommService
    {
        [OperationContract]
        CommActionResultDto CommAction(CommActionRequestDto request);
    }

    [LogDetails]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class CommService : ICommService, IDomainService
    {
        public CommActionResultDto CommAction(CommActionRequestDto request)
        {
            CommActionResultDto response = new CommActionResultDto();

            response.Result = CommController.InvokeCommAction(request.Action, request.Parameter);

            return response;
        }
    }
}
