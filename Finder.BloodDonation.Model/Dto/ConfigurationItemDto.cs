﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;


namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class ConfigurationItemDto
    {
        [DataMember]
        public virtual int Id { get; set; }
        [DataMember]
        public virtual string Section { get; set; }
        [DataMember]
        public virtual string Name { get; set; }
        [DataMember]
        public virtual string Label { get;set; }
        [DataMember]
        public virtual string Description { get; set; }
        [DataMember]
        public virtual string Value { get; set; }
    }
}
