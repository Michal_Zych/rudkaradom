﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    public class PrepParamValueDto
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
