﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class PrepParamDefinitionDto
    {
        [DataMember]
        public virtual int TypeId { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual string Code { get; set; }

        [DataMember]
        public virtual string Type { get; set; }

        [DataMember]
        public virtual string Format { get; set; }
    }
}
