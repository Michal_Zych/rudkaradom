﻿using System;
using System.Runtime.Serialization;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class UnitPreviewDto
    {
        [DataMember]
        public virtual int ID { get; set; }
        [DataMember]
        public virtual string Name { get; set; }
        [DataMember]
        public virtual int? ParentUnitID { get; set; }
        [DataMember]
        public virtual UnitType UnitType { get; set; }
        [DataMember]
        public virtual double PreviewTLX { get; set; }
        [DataMember]
        public virtual double PreviewTLY { get; set; }
        [DataMember]
        public virtual double PreviewBRX { get; set; }
        [DataMember]
        public virtual double PreviewBRY { get; set; }

        [DataMember]
        public virtual string Location { get; set; }

        [DataMember]
        public virtual int UnitTypeV2 { get; set; }

        [DataMember]
        public virtual int? PictureID { get; set; }
        [DataMember]
        public virtual string MeasureUnit { get; set; }
        [DataMember]
        public virtual double? Value { get; set; }
        [DataMember]
        public virtual string LoRange { get; set; }
        [DataMember]
        public virtual string UpRange { get; set; }
        [DataMember]
        public virtual DateTime? MeasureTime { get; set; }

    }
}