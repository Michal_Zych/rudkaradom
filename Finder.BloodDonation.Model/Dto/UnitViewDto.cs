﻿using System;
using System.Runtime.Serialization;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class UnitViewDto
    {
        [DataMember]
        public virtual int ID { get; set; }
        [DataMember]
        public virtual string Name { get; set; }
        [DataMember]
        public virtual int? ParentUnitID { get; set; }
        [DataMember]
        public virtual UnitType UnitType { get; set; }


        [DataMember]
        public virtual int DeviceID { get; set; }
        [DataMember]
        public virtual double A1L { get; set; }
        [DataMember]
        public virtual double A1H { get; set; }
        [DataMember]
        public virtual double A2L { get; set; }
        [DataMember]
        public virtual double A2H { get; set; }
        [DataMember]
        public virtual double T1 { get; set; }
        [DataMember]
        public virtual double T2 { get; set; }
    }
}