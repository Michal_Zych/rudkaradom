﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class StatusDto
    {

        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string NameResourceKey { get; set; }
    }
}
