﻿using System;
using System.Data.Linq.Mapping;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using FluentNHibernate.Mapping;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class SettingDTO
    {
        [DataMember]
        public virtual string Name { get; set; }
        [DataMember]
        public virtual string Value { get; set; }
    }

    public class Setting
    {
        public virtual string Name { get; set; }
        public virtual string Value { get; set; }
    }

   /*public class SettingMap: ClassMap<Setting>
    {
        public SettingMap()
        {
            Table("Settings");
            Map(x => x.Name);
            Map(x => x.Value);
        }
    }
    */
}

