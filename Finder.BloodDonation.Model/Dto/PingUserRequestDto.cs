﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using FinderFX.SL.Core.MVVM;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class PingRequestDto
    {
        [DataMember]
        public DateTime AlarmDateStart { get; set; }

        [DataMember]
        public DateTime? MonitoringLastDataDate { get; set; }

        [DataMember]
        public int? MonitoringDeviceID { get; set; }

        [DataMember]
        public int? LowestLocalizationAlarmSeverity { get; set; }
    }
}