﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    public interface IDataTableDto
    {
        string Identity { get; }
        IEnumerable<int> ShowInUnits { get; }
    }
}
