﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class AssignedBandInfoDto
    {
        [DataMember]
        public int OperatorId { get; set; }
        [DataMember]
        public string Reason { get; set; }
        [DataMember]
        public short? BandId { get; set; }
        [DataMember]
        public string BarCode { get; set; }
        [DataMember]
        public int ObjectId { get; set; }
        [DataMember]
        public byte ObjectType { get; set; }
    }
}
