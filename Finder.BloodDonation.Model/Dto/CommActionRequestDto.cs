﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using FinderFX.SL.Core.MVVM;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class CommActionRequestDto
    {
        [DataMember]
        public string Action { get; set; }

        [DataMember]
        public string Parameter { get; set; }
    }
}