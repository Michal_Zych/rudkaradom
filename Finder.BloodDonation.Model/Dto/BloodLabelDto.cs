﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    public class BloodLabelDto
    {
        [DataMember]
        public long ID { get; set; }
        [DataMember]
        public long? Uid { get; set; }
        [DataMember]
        public int Inversion { get; set; }
        [DataMember]
        public  string DonationNumber { get; set; }

        [DataMember]
        public  string Producer { get; set; }

        [DataMember]
        public string CollectionDate { get; set; }
        [DataMember]
        public string PrepDate { get; set; }
        [DataMember]
        public  string FinalOrClassAndDivision { get; set; }

        [DataMember]
        public  string Description { get; set; }

        [DataMember]
        public string Capacity { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public string Code { get; set; }
        [DataMember]
        public string Symbol { get; set; }
        [DataMember]
        public string RhDesc { get; set; }
        [DataMember]
        public string ValidCode { get; set; }
        [DataMember]
        public string ValidDate { get; set; }
        [DataMember]
        public string AdditionalCode { get; set; }
        [DataMember]
        public string Fenotyp { get; set; }
        [DataMember]
        public  string Qualification { get; set; }

        [DataMember]
        public DateTime? DataValidUntil { get; set; }
        [DataMember]
        public int? DataCapacity {get; set;}
        [DataMember]
        public int DataUnitID { get; set; }
    }
}
