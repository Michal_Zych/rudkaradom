﻿using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class PatientDTO : IDataTableDto
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }        //icon

        [DataMember]
        public string Pesel { get; set; }

        [DataMember]
        public int DepartmentId { get; set; }

        [DataMember]
        public string DepartmentName { get; set; }

        [DataMember]
        public int? RoomId { get; set; }

        [DataMember]
        public string RoomName { get; set; }

        [DataMember]
        public short? BandId { get; set; }

        [DataMember]
        public DateTime? InHospitalDate { get; set; }

        [DataMember]
        public bool? IsActive { get; set; } //;nie;tak              icon

        [DataMember]
        public virtual int BezGenerowania { get; set; }

        public virtual IEnumerable<int> ShowInUnits //unity w których obiekt ma być pokazywany podczas filtrowaniana drzewie
        {
            get
            {
                return new int[] { RoomId ?? DepartmentId };
            }
        }

        public virtual string Identity
        {
            get { return ID.ToString(); }
        }


        [DataMember]
        public virtual string Reason { get; set; }
        [DataMember]
        public virtual string BandBarCode { get; set; }
        [DataMember]
        public virtual byte ForceAction { get; set; }
    }
}
