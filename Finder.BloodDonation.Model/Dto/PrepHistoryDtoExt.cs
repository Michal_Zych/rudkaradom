﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Finder.BloodDonation.Model.Dto
{
	[DataContract]
	public class PrepHistoryDtoExt //: PrepHistoryDto
	{
		[DataMember]
		public virtual string DonationNumber { get; set; }

		[DataMember]
		public virtual string Group { get; set; }

		[DataMember]
		public virtual string Attributes { get; set; }

		[DataMember]
		public virtual DateTime ArrivalDate { get; set; }

		[DataMember]
		public virtual DateTime? DepartureDate { get; set; }

		[DataMember]
		public virtual string UnitName { get; set; }
	}
}
