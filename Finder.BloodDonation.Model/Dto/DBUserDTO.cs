﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
	public class DBUserDTO
	{
		public virtual int ID { get; set; }
		public virtual string Login { get; set; }
		public virtual string Password { get; set; }
		public virtual int ClientID { get; set; }

		public virtual int UnitID { get; set; }

		public virtual IList<DBRoleDTO> Roles { get; set; }
		public virtual IList<DefaultUsersUnitsDTO> DefaultUnits { get; set; }
	}

	public class DBRoleDTO
	{
		public virtual int ID { get; set; }
		public virtual string Name { get; set; }

	}

	public class DBPermissionDTO
	{
		public virtual int ID { get; set; }
		public virtual string Name { get; set; }
	}
}
