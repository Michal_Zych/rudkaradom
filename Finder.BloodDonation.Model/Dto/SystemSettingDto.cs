﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class SystemSettingDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public DateTime StartDateUtc { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public bool NoConnectionFinishingAlarms { get; set; }

        [DataMember]
        public bool FullMonitoringUnassignedBands { get; set; }

        [DataMember]
        public string WarningAutoCloseMessage { get; set; }

        [DataMember]
        public bool LoBatteryWrActive { get; set; }

        [DataMember]
        public int LoBatteryWrMins { get; set; }

        [DataMember]
        public bool LoBatteryAlActive { get; set; }

        [DataMember]
        public int LoBatteryAlMins { get; set; }

        [DataMember]
        public int BatteryTermWrDaysBefore { get; set; }

        [DataMember]
        public string BatteryTermWrMessage { get; set; }

        [DataMember]
        public int BatteryTermAlDaysBefore { get; set; }

        [DataMember]
        public string BatteryTermAlMessage { get; set; }

        [DataMember]
        public DateTime DayStartHour { get; set; }

        [DataMember]
        public DateTime DayEndHour { get; set; }

        [DataMember]
        public bool NoTransmitterWrActive { get; set; }

        [DataMember]
        public int NoTransmitterWrMins { get; set; }

        [DataMember]
        public bool NoTransmitterAlActive { get; set; }

        [DataMember]
        public int NoTransmitterAlMins { get; set; }

        [DataMember]
        public string FirstBandAssignmentReason { get; set; }

        [DataMember]
        public int EventToArchiveAfterDays { get; set; }

        [DataMember]
        public string _LocalAdminPhone { get; set; }

        [DataMember]
        public int MaxPasswordAgeDays { get; set; }

        [DataMember]
        public bool ShowReasonField { get; set; }

        [DataMember]
        public int PingSessionIntervalSecs { get; set; }

        [DataMember]
        public int MonitoringSoundIntervalSecs { get; set; }

        [DataMember]
        public int RangeColorIntensityPercent { get; set; }

        [DataMember]
        public int MaxRwosLogs { get; set; }

        [DataMember]
        public int MaxRwosMessages  { get; set; }

        [DataMember]
        public int DefMessageActivityMinutes { get; set; }

        [DataMember]
        public int MaxRowsAlarmsPopup { get; set; }

        [DataMember]
        public int MaxRowsAlarms { get; set; }

        [DataMember]
        public int MaxRowsRawData { get; set; }

        [DataMember]
        public int RoleDefForNewUser { get; set; }

        [DataMember]
        public string UnitsLocationSeparator { get; set; }

        [DataMember]
        public int MaxDaysReports { get; set; }

        [DataMember]
        public int MaxDaysRawData { get; set; }

        [DataMember]
        public int SearchMessagesDaysBefore { get; set; }

        [DataMember]
        public int SearchMessagesDaysAfter { get; set; }

        [DataMember]
        public int SearchLogsDaysBefore { get; set; }

        [DataMember]
        public int SearchLogsDaysAfter { get; set; }

        [DataMember]
        public int SearchChartDataHoursBefore { get; set; }

        [DataMember]
        public int SearchChartDataHoursAfter { get; set; }

        [DataMember]
        public int ChartImageWidth { get; set; }

        [DataMember]
        public int ChartImageHeight { get; set; }

        [DataMember]
        public int SearchRawDataDaysBefore { get; set; }

        [DataMember]
        public int SearchRawDataDaysAfter { get; set; }

        [DataMember]
        public int SearchAlarmsDaysBefore { get; set; }

        [DataMember]
        public int SearchAlarmsDaysAfter { get; set; }

        [DataMember]
        public bool ShowUnitId { get; set; }

        [DataMember]
        public bool EnableMonitoringStatusButton { get; set; }

        [DataMember]
        public int OldDataTimeMinutes { get; set; }

        [DataMember]
        public string OldDataColor { get; set; }

        [DataMember]
        public bool? SqlLoggingIsOn { get; set; }

        [DataMember]
        public string Reason { get; set; }
    }
}
