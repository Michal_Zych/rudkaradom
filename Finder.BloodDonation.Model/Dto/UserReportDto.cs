﻿using System;
using System.Runtime.Serialization;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class UserReportDto
    {
        [DataMember]
        public virtual int ID { get; set; }
        [DataMember]
        public virtual int UserID { get; set; }
        [DataMember]
        public virtual int ReportID { get; set; }
        [DataMember]
        public virtual DateTime DateFrom { get; set; }
        [DataMember]
        public virtual DateTime DateTo { get; set; }
        [DataMember]
        public virtual int UnitID { get; set; }
        [DataMember]
        public virtual bool HideCharts { get; set; }
        [DataMember]
        public virtual bool Mono { get; set; }
        [DataMember]
        public virtual bool HyperPermission { get; set; }
    }
}