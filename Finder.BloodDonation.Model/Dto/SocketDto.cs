﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class SocketDto
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string Name { get; set; }
    }
}
