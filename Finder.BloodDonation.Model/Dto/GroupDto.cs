﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class GroupDto
    {
        [DataMember]
        public byte? Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Reason { get; set; }
    }
}
