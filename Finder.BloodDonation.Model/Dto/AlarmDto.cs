﻿using System;
using System.Runtime.Serialization;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract(Name="xx")]
    public class AlarmDto
    {
        [DataMember(Name="xa")]
        public virtual int ID { get; set; }

        [DataMember(Name="xb")]
        public virtual byte EventType { get; set; }
        [DataMember(Name="xc")]
        public virtual DateTime AlarmTime { get; set; }

        [DataMember(Name="xd")]
        public virtual short SensorID { get; set; }
        [DataMember(Name="xe")]
        public virtual string SensorName { get; set; }
        [DataMember(Name="xf")]
        public virtual int UnitID { get; set; }
        [DataMember(Name = "xg")]
        public virtual string Location { get; set; }
        [DataMember(Name  = "xh")]
        public virtual string MU {get; set;}

        [DataMember(Name="xi")]
        public virtual double LoRange { get; set; }
        [DataMember(Name="xj")]
        public virtual double UpRange { get; set; }
        [DataMember(Name="xk")]
        public virtual double Value { get; set; }
        
        [DataMember(Name = "xl")]
        public virtual DateTime? DateEnd { get; set; }
        [DataMember(Name="xm")]
        public virtual DateTime? AlarmCloseDate { get; set; }
        [DataMember(Name="xn")]
        public virtual int? AlarmCloseUserID { get; set; }
        [DataMember(Name="xo")]
        public virtual string AlarmCloseUserName { get; set; }
        [DataMember(Name="xp")]
        public virtual string AlarmCloseComment { get; set; }
        [DataMember(Name = "xq")]
        public virtual bool IsBitSensor { get; set; }
        [DataMember(Name = "xr")]
        public virtual string LoStateDesc { get; set; }
        [DataMember(Name = "xs")]
        public virtual string HiStateDesc { get; set; }
    }
}