﻿using System;
using System.Runtime.Serialization;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class PreviewDto
    {
        [DataMember]
        public virtual int ID { get; set; }
        [DataMember]
        public virtual string Path { get; set; }
        [DataMember]
        public virtual int UnitID { get; set; }
        [DataMember]
        public virtual UnitType UnitType { get; set; }
        [DataMember]
        public virtual int PictureID { get; set; }
    }
}