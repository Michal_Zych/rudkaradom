﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class PatientDetailsDto : IDataTableDto
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string DisplayName { get; set; }

        [DataMember]
        public string Pesel { get; set; }

        [DataMember]
        public virtual int WardId { get; set; }

        [DataMember]
        public string WardUnitName { get; set; }

        [DataMember]
        public string WardUnitLocation { get; set; }

        [DataMember]
        public DateTime WardAdmissionDate { get; set; }

        [DataMember]
        public virtual int? RoomId { get; set; }

        [DataMember]
        public string RoomUnitName { get; set; }

        [DataMember]
        public string RoomUnitLocation { get; set; }

        [DataMember]
        public DateTime RoomAdmissionDate { get; set; }

        [DataMember]
        public int? BandId { get; set; }

        [DataMember]
        public string BandBarCode { get; set; }

        [DataMember]
        public string BandSerialNumber { get; set; }

        [DataMember]
        public string CurrentUnitName { get; set; }

        [DataMember]
        public string CurrentUnitLocation { get; set; }

        public virtual IEnumerable<int> ShowInUnits //unity w których obiekt ma być pokazywany podczas filtrowaniana drzewie
        {
            get
            {
                return new int[] { RoomId ?? WardId };
            }
        }

        public virtual string Identity
        {
            get { return Id.ToString(); }
        }
    }
}
