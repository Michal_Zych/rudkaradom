﻿using System;
using System.Runtime.Serialization;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class EditPreviewDto
    {
        [DataMember]
        public virtual int ID { get; set; }
        [DataMember]
        public virtual string Path { get; set; }
        [DataMember]
        public virtual string Name { get; set; }
        [DataMember]
        public virtual int ClientID { get; set; }
    }
}