﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class ExternalUnitMapDto : IDataTableDto
    {
        [DataMember]
        public string ExtId { get; set; }

        [DataMember]
        public string ExtDescription { get; set; }

        [DataMember]
        public int M2mId { get; set; }

        [DataMember]
        public string M2mDescription { get; set; }

        public string Identity
        {
            get { return "1"; }
        }

        public IEnumerable<int> ShowInUnits
        {
            get { return null; }
        }
    }
}
