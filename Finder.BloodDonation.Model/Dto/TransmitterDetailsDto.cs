﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class TransmitterDetailsDto : IDataTableDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public long Mac { get; set; }

        [DataMember]
        public string MacString { get; set; }

        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int? InstallationUnitId { get; set; }

        [DataMember]
        public string InstallationUnitName { get; set; }

        [DataMember]
        public string BarCode { get; set; }

        [DataMember]
        public int TypeId { get; set; }

        [DataMember]
        public int ReportIntervalSec { get; set; }

        [DataMember]
        public int Sensitivity { get; set; }

        [DataMember]
        public int AvgCalcTimeS { get; set; }

        [DataMember]
        public string Msisdn { get; set; }

        [DataMember]
        public bool? IsBandUpdater { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public short? RssiTreshold { get; set; }

        [DataMember]
        public byte GroupId { get; set; }

        [DataMember]
        public string GroupName { get; set; }

        [DataMember]
        public string IpAddress { get; set; }

        [DataMember]
        public bool IsConnected { get; set; }

        [DataMember]
        public bool AlarmOngoing { get; set; }

        [DataMember]
        public string Transitions { get; set; }

        public string Identity
        {
            get { return Id.ToString(); }
        }

        public IEnumerable<int> ShowInUnits
        {
            get { 
                return new int[] { InstallationUnitId != null ? (int)InstallationUnitId : -1 }; 
            }                                       
        }
    }
}
