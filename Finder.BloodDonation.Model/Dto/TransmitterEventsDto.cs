﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class TransmitterEventsDto : IDataTableDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public DateTime DateUtc { get; set; }

        [DataMember]
        public virtual int Type { get; set; }

        [DataMember]
        public string TypeDescription { get; set; }

        [DataMember]
        public virtual int Severity { get; set; }

        [DataMember]
        public string SeverityDescription { get; set; }

        [DataMember]
        public virtual int UnitId { get; set; }

        [DataMember]
        public string UnitName { get; set; }

        [DataMember]
        public string UnitLocation { get; set; }

        [DataMember]
        public virtual int ObjectId { get; set; }

        [DataMember]
        public virtual string ObjectName { get; set; }

        [DataMember]
        public virtual int ObjectType { get; set; }

        [DataMember]
        public string ObjectTypeDescription { get; set; }

        [DataMember]
        public virtual int ObjectUnitId { get; set; }

        [DataMember]
        public string ObjectUnitLocation { get; set; }

        [DataMember]
        public DateTime StoreDateUtc { get; set; }

        [DataMember]
        public DateTime EndDateUtc { get; set; }

        [DataMember]
        public virtual int EndingEventId { get; set; }

        [DataMember]
        public virtual int OperatorId { get; set; }

        [DataMember]
        public virtual string EventData { get; set; }

        [DataMember]
        public string ClosingUserName { get; set; }

        [DataMember]
        public string ClosingUserLastName { get; set; }

        [DataMember]
        public string ClosingComment { get; set; }

        [DataMember]
        public string SenderName { get; set; }

        [DataMember]
        public string SenderLastName { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string Reason { get; set; }

        public string Identity
        {
            get { return Id.ToString(); }
        }

        public IEnumerable<int> ShowInUnits
        {
            get { return new int[] { 0 }; }
        }
    }
}
