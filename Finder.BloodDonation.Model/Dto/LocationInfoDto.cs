﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class LocationInfoDto
    {
        [DataMember]
        public int CurrentUnitId { get; set; }

        [DataMember]
        public string CurrentUnitName { get; set; }

        [DataMember]
        public string CurrentUnitLocation { get; set; }

        [DataMember]
        public DateTime InCurrent { get; set; }

        [DataMember]
        public int PreviousUnitId { get; set; }

        [DataMember]
        public string PreviousUnitName { get; set; }

        [DataMember]
        public string PreviousUnitLocation { get; set; }

        [DataMember]
        public DateTime InPrevious { get; set; }
    }
}
