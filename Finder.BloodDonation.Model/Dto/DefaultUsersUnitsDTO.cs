﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
	public class DefaultUsersUnitsDTO
	{
		public virtual int UserID { get; set; }
		public virtual int UnitID { get; set; }

	}
}
