﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using FinderFX.SL.Core.MVVM;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class CreateSessionRequestDto
    {
        [DataMember]
        public virtual bool Monitoring { get; set; }

        [DataMember]
        public virtual string Phone { get; set; }
    }
}