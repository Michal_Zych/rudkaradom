﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class LoggedUserDto
    {
        [DataMember]
        public int ClientId {get; set;}

        [DataMember]
        public string Unit { get; set; }

        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public string Login {get; set;}

        [DataMember]
        public string User{get; set;}

        [DataMember] 
        public DateTime LoggingDate{get;set; }

        [DataMember]
        public DateTime LastPingDate{get;set;}

        [DataMember]
        public string Phone{get;set;}

    }
}
