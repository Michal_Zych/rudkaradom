﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using FinderFX.SL.Core.MVVM;
using System.Collections.Generic;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class CommActionResultDto
    {
        [DataMember]
        public int Result { get; set; }
    }
}