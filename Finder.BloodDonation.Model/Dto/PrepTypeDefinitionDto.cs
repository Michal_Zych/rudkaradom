﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class PrepTypeDefinitionDto
    {
        [DataMember]
        public virtual int ID { get; set; }

        [DataMember]
        public virtual int? ParentID { get; set; }

        [DataMember]
        public virtual string NameResourceKey { get; set; }

        [DataMember]
        public virtual int PrepCount { get; set; }

        [DataMember]
        public virtual IList<PrepParamDefinitionDto> Parameters { get; set; }

        [DataMember]
        public virtual string Code { get; set; }

        [DataMember]
        public virtual string Symbol { get; set; }

        public override string ToString()
        {
            return String.Format("id: {0} parent: {1} name: {2}", ID, ParentID, NameResourceKey);
        }
    }
}
