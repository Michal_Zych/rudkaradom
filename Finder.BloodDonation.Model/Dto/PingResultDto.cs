﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using FinderFX.SL.Core.MVVM;
using System.Collections.Generic;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class PingResultDto
    {
        [DataMember]
        public int Result { get; set; }

        [DataMember]
        public IList<AlarmDto> Alarms { get; set; }

        [DataMember]
        public IList<AlarmDetailsDto> AlarmsDetails { get; set; }

        [DataMember]
        public IList<DisableAlarmUnitDto> DisableAlarmUnits { get; set; }

        [DataMember]
        public IList<DisableAlarmUnitDto> OldDataUnits { get; set;}

        [DataMember]
        public bool MonitoringDeviceRefreshData { get; set; }

        [DataMember]
        public IList<int> UnitsIdWithDisconnectedTransmitter { get; set; }

        [DataMember]
        public IList<string> Messages { get; set; }

        [DataMember]
        public IList<LocalizationAlarmDto> LocalizationAlarms { get; set; }
    }
}