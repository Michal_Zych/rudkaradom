﻿using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class BandDTO : IDataTableDto
    {
        [DataMember]
        public virtual int? Id { get; set; }

        [DataMember]
        public virtual long Mac { get; set; }

        [DataMember]
        public virtual string MacString { get; set; }

        [DataMember]
        public virtual long? Uid { get; set; }

        [DataMember]
        public virtual string BarCode { get; set; }

        [DataMember]
        public virtual short TransmissionPower { get; set; }

        [DataMember]
        public virtual byte  AccPercent { get; set; }

        [DataMember]
        public virtual byte SensitivityPercent { get; set; }

        [DataMember]
        public virtual int BroadcastingIntervalMs { get; set; }

        [DataMember]
        public virtual string Description { get; set; }

        [DataMember]
        public int? UnitId { get; set; }

        [DataMember]
        public virtual byte ObjectType { get; set; }
        [DataMember]
        public virtual int ObjectId { get; set; }
        [DataMember]
        public virtual string ObjectDisplayName { get; set; }

        [DataMember]
        public virtual List<BandParameter> Parameters { get; set; }

        [DataMember]
        public virtual DateTime BatteryInstallationDateUtc { get; set; }

        [DataMember]
        public virtual byte BatteryTypeId { get; set; }

        [DataMember]
        public virtual int? CurrentUnitId { get; set; }
        [DataMember]
        public virtual string CurrentUnitName { get; set; }
        [DataMember]
        public virtual string CurrentUnitLocation { get; set; }

        [DataMember]
        public virtual bool IsBatteryAlarm { get; set; }

        [DataMember]
        public virtual string Reason { get; set; }

        [DataMember]
        public virtual string SerialNumber { get; set; }


        public string Identity
        {
            get { return string.Empty; }
        }

        public IEnumerable<int> ShowInUnits
        {
            get { return null; }
        }
    }
}

public class BandParameter
{
    public string Key { get; set; }
    public string Value { get; set; }
    public string Description { get; set; }

    public BandParameter()
    {

    }

    public override string ToString()
    {
        return Key + " " + Value + " " + Description;
    }
}