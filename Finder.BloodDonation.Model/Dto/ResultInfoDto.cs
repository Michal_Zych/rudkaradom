﻿using System;
using System.Net;

namespace Finder.BloodDonation.Dialogs.Patients
{
    public class ResultInfoDto
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string Result { get; set; }
    }
}
