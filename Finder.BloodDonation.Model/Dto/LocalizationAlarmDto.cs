﻿using System;
using System.Runtime.Serialization;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract(Name = "lz")]
    public class LocalizationAlarmDto : IDataTableDto
    {
        [DataMember(Name = "la")]
        public int AlarmId { get; set; }

        [DataMember(Name = "lb")]
        public int Severity { get; set; }

        [DataMember(Name = "lc")]
        public int ObjectType {get; set;}

        [DataMember(Name = "ld")]
        public string ObjectTypeName { get; set; }

        [DataMember(Name = "le")]
        public string ObjectName { get; set; }

        [DataMember(Name = "lf")]
        public int AlarmType { get; set; }

        [DataMember(Name = "lg")]
        public string AlarmDescription { get; set; }

        [DataMember(Name = "lh")]
        public string AlarmLocation { get; set; }

        [DataMember(Name = "li")]
        public DateTime AlarmDate { get; set; }

        [DataMember(Name = "lj")]
        public DateTime? AlarmEndDate { get; set; }


        public string Identity
        {
            get { return AlarmId.ToString(); }
        }

        public System.Collections.Generic.IEnumerable<int> ShowInUnits
        {
            get { return null /*return new int[] { 0 }*/; }
        }
    }
}
