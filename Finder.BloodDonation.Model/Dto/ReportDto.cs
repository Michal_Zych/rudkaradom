﻿using System;
using System.Runtime.Serialization;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class ReportDto
    {
        [DataMember]
        public virtual int ID { get; set; }
        [DataMember]
        public virtual string Name { get; set; }
        [DataMember]
        public virtual string Description { get; set; }
		[DataMember]
		public virtual int Type { get; set; }
        [DataMember]
        public virtual string Picture { get; set; }
    }
}