﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class PrepDto
    {
        #region FiltersValues

        public const string AllGroupsFilterValue = null;
        public const string BloodGroupsFilterValue = "1";
        public const string DerivativesFilterValue = "27";
        public const string PlasmaGroupsFilterValue = "2";
        public const string PlateletsGroupsFilterValue = "26";
        public const string ReagentsGroupsFilterValue = "28";

        public const string GetReagentsAndProductsFilterValue = "reagents";

        #endregion FiltersValues

        #region Fields

        //public const string CapacityField = "CP";
        //public const string DonationNumberField = "DN";
        //public const string GroupField = "GR";
        //public const string TypeField = "TP";
        //public const string ValidUntilField = "VU";

        #endregion Fields




        //public const string DonationNumberFilterName = "dn";
        //public const string GroupFilterName = "gr"; //examples: A-, A+, A
        //public const string GroupParentFilterName = "pId"; 
        //public const string Phenotype = "ph";
        //public const string StatusFilterName = "statuses";
        //public const string TypeFilterName = "tp"; //full name
        //public const string TypeIdFilterName = "tpId";
        //public const string ValidUntilFromFilterName = "vuFrom";
        //public const string ValidUntilToFilterName = "vuTo";
        //public const string GetReagentsFilterName = "getReagents";
        //public const string KellFilterName = "ke";

        //public const string SerialNumberFilterName = "sn";
        //public const string NameFilterName = "n";

        public PrepDto()
        {
            Parameters = new List<PrepParamValueDto>();
        }

        [DataMember]
        public virtual DateTime? AddedDate { get; set; }

        [DataMember]
        public string BloodType { get; set; }

        [DataMember]
        public int? Capacity { get; set; }

        [DataMember]
        public string DonationNumber { get; set; }

        [DataMember]
        public string FullType { get; set; }

        [DataMember]
        public string Group { get; set; }

        public string GroupPrefix { get { return Group.Substring(0, 2); } }

        [DataMember]
        public virtual long Id { get; set; }

        [DataMember]
        public string Kell { get; set; }  //phenotype

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public virtual IList<PrepParamValueDto> Parameters { get; set; }

        public string ParametersSummary
        {
            get
            {
                var sb = new StringBuilder();
                foreach (var p in Parameters)
                {
                    sb.Append(p.Value).Append(" ");
                }

                return sb.ToString();
            }
        }

        [DataMember]
        public string ProductCode { get; set; }

        [DataMember]
        public string SerialNumber { get; set; }

        [DataMember]
        public virtual int StatusId { get; set; }

        public string Summary
        {
            get
            {
                return ToString();
            }
        }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public virtual int? TypeId { get; set; }

        public string TypePrefix { get { return Type.Substring(0, 5); } }

        [DataMember]
        public virtual long Uid { get; set; }

        [DataMember]
        public virtual int UnitId { get; set; }

        [DataMember]
        public DateTime? ValidUntil { get; set; }

        //R.Sz. pola dodane 2014-02
        [DataMember]
        public string Class { get; set; }

        [DataMember]
        public string ClassDesc { get; set; }

        [DataMember]
        public string Division { get; set; }

        [DataMember]
        public string BloodUnits { get; set; }

        [DataMember]
        public DateTime? CollectionDate { get; set; }

        [DataMember]
        public string Fractionatr { get; set; }

        [DataMember]
        public string Location { get; set; }

        [DataMember]
        public int? Position { get; set; }

        [DataMember]
        public string Order { get; set; }

        [DataMember]
        public string OrderDesc { get; set; }




        public static IEnumerable<int> GetByGroup(string group)
        {
            var mapping = new List<KeyValuePair<int, int>>();
            mapping.Add(new KeyValuePair<int, int>(6, 12)); // a-
            mapping.Add(new KeyValuePair<int, int>(62, 13)); // a+
            mapping.Add(new KeyValuePair<int, int>(17, 14)); // b-
            mapping.Add(new KeyValuePair<int, int>(73, 15)); // b+

            mapping.Add(new KeyValuePair<int, int>(28, 16)); // ab-
            mapping.Add(new KeyValuePair<int, int>(84, 17)); // ab+
            mapping.Add(new KeyValuePair<int, int>(95, 18)); // 0-
            mapping.Add(new KeyValuePair<int, int>(51, 19)); // 0+

            // plasma

            mapping.Add(new KeyValuePair<int, int>(66, 22)); // a
            mapping.Add(new KeyValuePair<int, int>(77, 23)); // b
            mapping.Add(new KeyValuePair<int, int>(88, 24)); // ab
            mapping.Add(new KeyValuePair<int, int>(55, 25)); // 0

            // platelets
            mapping.Add(new KeyValuePair<int, int>(6, 31)); // a-
            mapping.Add(new KeyValuePair<int, int>(62, 32)); // a+
            mapping.Add(new KeyValuePair<int, int>(17, 33)); // b-
            mapping.Add(new KeyValuePair<int, int>(73, 34)); // b+

            mapping.Add(new KeyValuePair<int, int>(28, 35)); // ab-
            mapping.Add(new KeyValuePair<int, int>(84, 36)); // ab+
            mapping.Add(new KeyValuePair<int, int>(95, 37)); // 0-
            mapping.Add(new KeyValuePair<int, int>(51, 38)); // 0+

            //products and reagents
            mapping.Add(new KeyValuePair<int, int>(99, 30)); // inhibitor
            mapping.Add(new KeyValuePair<int, int>(98, 29)); // catalyzer

            int key = 0;
            if (group.Length > 2)
                key = Int32.Parse(group.Substring(0, 2)); //others (blood, plasma etc.)
            else
                key = Int32.Parse(group); //reagents products

            return mapping.Where(m => m.Key == key).Select(m => m.Value);
        }

        public static string GetComplexFilter(params string[] values)
        {
            return string.Join(",", values);
        }

        public string SerializeParams()
        {
            var sb = new StringBuilder();

            sb.Append("<pdata>");

            foreach (var item in Parameters)
            {
                sb.Append(String.Format("<{0}>{1}</{0}>", item.Name.ToLowerInvariant(), item.Value));
            }

            sb.Append("</pdata>");

            return sb.ToString();
        }

        public override string ToString()
        {
            return String.Format("Id:{0} TypeId:{1} AddedDate:{2}", Id, TypeId, AddedDate);
        }
    }
}