﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class AlarmDetailsDto
    {
        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public virtual int Type { get; set; }

        [DataMember]
        public string TypeDescription { get; set; }

        [DataMember]
        public virtual byte Severity { get; set; }

        [DataMember]
        public virtual int UnitId { get; set; }

        [DataMember]
        public string UnitLocation { get; set; }

        [DataMember]
        public string ObjectName { get; set; }

        [DataMember]
        public virtual int ObjectType { get; set; }

        [DataMember]
        public virtual int ObjectId { get; set; }

        [DataMember]
        public virtual int ObjectUnitId { get; set; }

        [DataMember]
        public string ObjectUnitLocation { get; set; }

        [DataMember]
        public string AlarmCloseComment { get; set; }

        [DataMember]
        public DateTime EndDate { get; set; }
    }
}