﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class UnitPrepNameCountDto : UnitPrepCountDto
    {
        [Display(Name="Nazwa")]
        [DataMember]
        public string Name { get; set; }

    }
}
