﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finder.BloodDonation.Model.Enum;
using FinderFX.SL.Core.MVVM;
using System.Runtime.Serialization;

namespace Finder.BloodDonation.Model.Dto
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.ComponentModel.DataAnnotations;
	using System.Runtime.Serialization;
	using System.Collections.ObjectModel;

	[System.Runtime.Serialization.DataContractAttribute(Name="b")]
	public partial class UnitDTO : MementoEditableObject<UnitDTO>,
		System.ComponentModel.INotifyPropertyChanged
	{
		#region INotifyPropertyChanged Members

		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected void RaisePropertyChanged(string propertyName)
		{
			System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
			if ((propertyChanged != null))
			{
				propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}

		#endregion

		
		private int IDField;
		[DataMember(Name = "c")]
		public virtual int ID
		{
			get
			{
				return this.IDField;
			}
			set
			{
				if ((this.IDField.Equals(value) != true))
				{
					Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "ID" });
					this.IDField = value;
					this.RaisePropertyChanged("ID");
				}
			}
		}


		private string NameField;
		[DataMember(Name = "d")]
		public virtual string Name
		{
			get
			{
				return this.NameField;
			}
			set
			{
				if ((object.ReferenceEquals(this.NameField, value) != true))
				{
					Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "Name" });
					this.NameField = value;
					this.RaisePropertyChanged("Name");
				}
			}
		}

		private int? ParentUnitIDField;
		[DataMember(Name = "e")]
		public virtual int? ParentUnitID
		{
			get
			{
				return this.ParentUnitIDField;
			}
			set
			{
				if ((object.ReferenceEquals(this.ParentUnitIDField, value) != true))
				{
					Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "ParentUnitID" });
					this.ParentUnitIDField = value;
					this.RaisePropertyChanged("ParentUnitID");
				}
			}
		}

		
		private UnitType UnitTypeField;
		[DataMember(Name = "f")]
		public virtual UnitType UnitType
		{
			get
			{
				return this.UnitTypeField;
			}
			set
			{
				if ((this.UnitTypeField.Equals(value) != true))
				{
					Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "UnitType" });
					this.UnitTypeField = value;
					this.RaisePropertyChanged("UnitType");
				}
			}
		}

		private List<UnitDTO> ChildrenField;
		[DataMember(Name = "g")]
		public virtual List<UnitDTO> Children
		{
			get
			{
				return this.ChildrenField;
			}
			set
			{
				if ((object.ReferenceEquals(this.ChildrenField, value) != true))
				{
					Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "Children" });
					this.ChildrenField = value;
					this.RaisePropertyChanged("Children");
				}
			}
		}

	    private int _level;

        [DataMember(Name = "l")]
	    public virtual int Level
	    {
            get
            {
                return this._level;
            }
            set
            {
                if ((object.ReferenceEquals(this.ChildrenField, value) != true))
                {
                    Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "Level" });
                    this._level = value;
                    this.RaisePropertyChanged("Level");
                }
            }
	    }


        private int UnitClientID;

        [DataMember(Name = "n")]
        public virtual int ClientID
        {
            get
            {
                return this.UnitClientID;
            }
            set
            {
                if ((object.ReferenceEquals(this.UnitClientID, value) != true))
                {
                    Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "ClientID" });
                    this.UnitClientID = value;
                    this.RaisePropertyChanged("ClientID");
                }
            }
        }

        private int _UnitTypeIDv2;
        [DataMember(Name = "o")]
        public virtual int UnitTypeIDv2
        {
            get
            {
                return this._UnitTypeIDv2;
            }
            set
            {
                if ((object.ReferenceEquals(this._UnitTypeIDv2, value) != true))
                {
                    Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "UnitTypeIDv2" });
                    this._UnitTypeIDv2 = value;
                    this.RaisePropertyChanged("UnitTypeIDv2");
                }
            }
        }

        private int? _PictureID;
        [DataMember(Name = "p")]
        public virtual int? PictureID
        {
            get
            {
                return this._PictureID;
            }
            set
            {
                if ((object.ReferenceEquals(this._PictureID, value) != true))
                {
                    Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "PictureID" });
                    this._PictureID = value;
                    this.RaisePropertyChanged("PictureID");
                }
            }
        }
  
        private string DescriptionField;
        [DataMember(Name = "r")]
        public virtual string Description
        {
            get
            {
                return this.DescriptionField;
            }
            set
            {
                if ((object.ReferenceEquals(this.DescriptionField, value) != true))
                {
                    Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "Description" });
                    this.DescriptionField = value;
                    this.RaisePropertyChanged("Description");
                }
            }

        }

        private bool IsMonitoredField;
        [DataMember(Name = "s")]
        public virtual bool IsMonitored
        {
            get
            {
                return this.IsMonitoredField;
            }
            set
            {
                if ((object.ReferenceEquals(this.IsMonitoredField, value) != true))
                {
                    Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "IsMonitored" });
                    this.IsMonitoredField = value;
                    this.RaisePropertyChanged("IsMonitored");
                }
            }
        }

        //public 

		public UnitDTO Clone(bool withoutChildren)
		{
			UnitDTO clone = new UnitDTO
			{
				ID = this.ID,
				Name = this.Name,
				ParentUnitID = this.ParentUnitID,
				UnitType = this.UnitType,
                ClientID = this.ClientID,
                UnitTypeIDv2 = this.UnitTypeIDv2,
                PictureID = this.PictureID,
                /*Description = this.Description,
                IsMonitored = this.IsMonitored
                 */
               // Active = this.Active
			};

			if (!withoutChildren && this.Children != null)
			{
				clone.Children = new List<UnitDTO>();
				foreach (UnitDTO c in this.Children)
				{
					clone.Children.Add(c.Clone());
				}
			}

			return clone;
		}

		public UnitDTO Clone()
		{
			return Clone(false);
		}
	}
}
