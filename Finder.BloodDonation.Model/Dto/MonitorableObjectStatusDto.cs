﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class MonitorableObjectStatusDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int ObjectType { get; set; }

        [DataMember]
        public int? BandId { get; set; }

        [DataMember]
        public int? CurrentUnitId { get; set; }

        [DataMember]
        public string CurrentUnitName { get; set; }

        [DataMember]
        public string CurrentUnitLocation { get; set; }

        [DataMember]
        public DateTime? InCurrentUnitUtc { get; set; }

        [DataMember]
        public int? PreviousUnitId { get; set; }

        [DataMember]
        public string PreviousUnitName { get; set; }

        [DataMember]
        public string PreviousUnitLocation { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int? EventTypes { get; set; }

        [DataMember]
        public DateTime? AlarmDate { get; set; }

        [DataMember]
        public int? AlarmType { get; set; }

        [DataMember]
        public string AlarmUnitName { get; set; }

        [DataMember]
        public string AlarmUnitLocation { get; set; }

        [DataMember]
        public string DisplayId { get; set; }

        [DataMember]
        public string DisplayLocation { get; set; }

        [DataMember]
        public string AlarmInfo { get; set; }

        [DataMember]
        public int? AssigmentRoomId { get; set; }

        [DataMember]
        public string AssigmentRoomName { get; set; }

        [DataMember]
        public int? AssigmentDepartmentId { get; set; }

        [DataMember]
        public string AssigmentDepartmentName { get; set; }

        [DataMember]
        public int AlarmsCount { get; set; }

        [DataMember]
        public int WarningsCount { get; set; }

        [DataMember]
        public string MovedStatus { get; set; }

        [DataMember]
        public DateTime? RoomOutDateUtc { get; set; }
    }
}
