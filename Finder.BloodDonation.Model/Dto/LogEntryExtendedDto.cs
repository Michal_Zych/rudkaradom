﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class LogEntryExtendedDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string EventData { get; set; }

        [DataMember]
        public string Reason { get; set; }
    }
}
