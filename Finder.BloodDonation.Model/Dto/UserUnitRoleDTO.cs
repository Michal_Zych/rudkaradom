﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
	public partial class UserUnitRoleDTO
	{
		public virtual int UserID { get; set; }
		public virtual int UnitID { get; set; }
		public virtual int RoleID { get; set; }


		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}

}
