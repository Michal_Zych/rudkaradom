﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class PrepTypeStatisticsDto
    {
        [DataMember]
        public virtual int ID { get; set; }

        [DataMember]
        public virtual long Count { get; set; }

        [DataMember]
        public virtual int ParentID { get; set; }

        [DataMember]
        public virtual string NameResourceKey { get; set; }

		[DataMember]
		public virtual string FilterValue { get; set; }

		[DataMember]
		public virtual int TotalCapacity { get; set; }

        public override string ToString()
        {
            return String.Format("Typ: {0} liczba: {1}", ID, Count);
        }
    }
}
