﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class PatientInfoDto : IDataTableDto
    {
        [DataMember]
        public virtual short? Id { get; set; }

        [DataMember]
        public virtual string FirstName { get; set; }

        [DataMember]
        public virtual string LastName { get; set; }

        [DataMember]
        public virtual string DisplayName { get; set; }

        [DataMember]
        public virtual int Pesel { get; set; }

        [DataMember]
        public virtual DateTime HospitalAdmissionDate { get; set; }

        [DataMember]
        public virtual string Department { get; set; }

        [DataMember]
        public virtual string DepartmentPath { get; set; }

        [DataMember]
        public virtual DateTime DepartmentAdmissionDate { get; set; }

        [DataMember]
        public virtual string Room { get; set; }

        [DataMember]
        public virtual string RoomPath { get; set; }

        [DataMember]
        public virtual DateTime RoomAdmissionDate { get; set; }

        [DataMember]
        public virtual short? BandId { get; set; }

        [DataMember]
        public virtual string CurrentlyDepartment { get; set; }

        [DataMember]
        public virtual string CurrentlyDepartmentPath { get; set; }

        [DataMember]
        public virtual string CurrentlyRoom { get; set; }

        [DataMember]
        public virtual string CurrentlyRoomPath { get; set; }

        public string Identity
        {
            get { return Id == null ? "" : Id.Value.ToString(); }
        }

        public IEnumerable<int> ShowInUnits
        {
            get { return new int[] { 0 }; }
        }
    }
}
