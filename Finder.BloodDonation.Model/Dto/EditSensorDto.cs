﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class EditSensorDto
    {
        [DataMember]
        public virtual short ID { get; set; }

        [DataMember]
        public virtual int? SocketId { get; set; }

        [DataMember]
        public virtual int Interval { get; set; }

        [DataMember]
        public virtual double Calibration { get; set; }

        [DataMember]
        public virtual bool Enabled { get; set; }

        [DataMember]
        public virtual string MU { get; set; }

        [DataMember]
        public virtual double LoRange { get; set; }

        [DataMember]
        public virtual double UpRange { get; set; }

        [DataMember]
        public virtual string LegendDescription { get; set; }

        [DataMember]
        public virtual string LegendShortcut { get; set; }

        [DataMember]
        public virtual string LegendRangeDescription { get; set; }

        [DataMember]
        public virtual string LineColor { get; set; }

        [DataMember]
        public virtual byte LineWidth { get; set; }

        [DataMember]
        public virtual string LineStyle { get; set; }

        [DataMember]
        public virtual byte RangeLineWidth { get; set; }

        [DataMember]
        public virtual string RangeLineStyle { get; set; }

        [DataMember]
        public virtual string AlarmColor { get; set; }

        [DataMember]
        public virtual string AlarmCommColor { get; set; }

        [DataMember]
        public virtual bool AlDisabled { get; set; }

        [DataMember]
        public virtual bool AlDisabledForRanges { get; set; }

        [DataMember]
        public virtual bool AlDisabledForHours { get; set; }

        [DataMember]
        public virtual DateTime? AlDisableHoursFrom1 { get; set; }

        [DataMember]
        public virtual DateTime? AlDisableHoursTo1 { get; set; }

        [DataMember]
        public virtual DateTime? AlDisableHoursFrom2 { get; set; }

        [DataMember]
        public virtual DateTime? AlDisableHoursTo2 { get; set; }

        [DataMember]
        public virtual short GuiRangeDelay { get; set; }

        [DataMember]
        public virtual short GuiCommDelay { get; set; }

        [DataMember]
        public virtual bool SmsEnabled { get; set; }

        [DataMember]
        public virtual short? SmsRangeDelay { get; set; }

        [DataMember]
        public virtual short? SmsCommDelay { get; set; }

        [DataMember]
        public virtual bool MailEnabled { get; set; }

        [DataMember]
        public virtual short? MailRangeDelay { get; set; }

        [DataMember]
        public virtual short? MailCommDelay { get; set; }

        [DataMember]
        public virtual bool IsBitSensor { get; set; }

        [DataMember]
        public virtual bool IsHighAlarming { get; set; }

        [DataMember]
        public virtual string HiStateDesc { get; set; }

        [DataMember]
        public virtual string LoStateDesc { get; set; }

        [DataMember]
        public virtual bool GuiRangeRepeat { get; set; }
        [DataMember]
        public virtual int GuiRangeRepeatMins { get; set; }
        [DataMember]
        public virtual bool GuiRangeRepeatBreakable { get; set; }
        [DataMember]
        public virtual bool GuiCommRepeat { get; set; }
        [DataMember]
        public virtual int GuiCommRepeatMins { get; set; }
        [DataMember]
        public virtual bool GuiCommRepeatBreakable { get; set; }

        [DataMember]
        public virtual bool SmsRangeEnabled { get; set; }

        [DataMember]
        public virtual bool SmsRangeAfterFinish { get; set; }
        [DataMember]
        public virtual bool SmsRangeAfterClose { get; set; }
        [DataMember]
        public virtual bool SmsRangeRepeat { get; set; }
        [DataMember]
        public virtual int SmsRangeRepeatMins { get; set; }
        [DataMember]
        public virtual string SmsRangeRepeatUntil { get; set; }
        [DataMember]
        public virtual bool SmsRangeRepeatBreakable { get; set; }
        [DataMember]
        public virtual bool SmsCommEnabled { get; set; }

        [DataMember]
        public virtual bool SmsCommAfterFinish { get; set; }
        [DataMember]
        public virtual bool SmsCommAfterClose { get; set; }
        [DataMember]
        public virtual bool SmsCommRepeat { get; set; }
        [DataMember]
        public virtual int SmsCommRepeatMins { get; set; }
        [DataMember]
        public virtual string SmsCommRepeatUntil { get; set; }
        [DataMember]
        public virtual bool SmsCommRepeatBreakable { get; set; }
        [DataMember]
        public virtual bool MailRangeEnabled { get; set; }

        [DataMember]
        public virtual bool MailRangeAfterFinish { get; set; }
        [DataMember]
        public virtual bool MailRangeAfterClose { get; set; }
        [DataMember]
        public virtual bool MailRangeRepeat { get; set; }
        [DataMember]
        public virtual int MailRangeRepeatMins { get; set; }
        [DataMember]
        public virtual string MailRangeRepeatUntil { get; set; }
        [DataMember]
        public virtual bool MailRangeRepeatBreakable { get; set; }
        [DataMember]
        public virtual bool MailCommEnabled { get; set; }

        [DataMember]
        public virtual bool MailCommAfterFinish { get; set; }
        [DataMember]
        public virtual bool MailCommAfterClose { get; set; }
        [DataMember]
        public virtual bool MailCommRepeat { get; set; }
        [DataMember]
        public virtual int MailCommRepeatMins { get; set; }
        [DataMember]
        public virtual string MailCommRepeatUntil { get; set; }
        [DataMember]
        public virtual bool MailCommRepeatBreakable { get; set; }
        [DataMember]
        public virtual short? TestAlarmEventType { get; set; }
        [DataMember]
        public virtual short? TestAlarmValue { get; set; }
    }
}
