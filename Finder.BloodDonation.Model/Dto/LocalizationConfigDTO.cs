﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class LocalizationConfigDTO
    {
        [DataMember]
        public short? Id { get; set; }

        [DataMember]
        public long Mac { get; set; }
        [DataMember]
        public string MacString { get; set; }
        [DataMember]
        public string Ip { get; set; }
        [DataMember]
        public byte Type { get; set; }
        [DataMember]
        public string InstallationDescription { get; set; }
        [DataMember]
        public string MSISDN { get; set; }
        [DataMember]
        public bool? BandUpdater { get; set; }
        [DataMember]
        public string InstalationPlace { get; set; }
        [DataMember]
        public int Sensitivity { get; set; }
        [DataMember]
        public int AvgCalcTimeS { get; set; }
        [DataMember]
        public int SwitchDelayS { get; set; }
        [DataMember]
        public string Comment { get; set; }
        [DataMember]
        public bool IsAlarmSOS { get; set; }
        [DataMember]
        public bool IsAlarmFromAccelerometer { get; set; }
        [DataMember]
        public bool IsSendWarnings_MoveDay { get; set; }
        [DataMember]
        public bool IsSendAlarm_MoveDay { get; set; }
        [DataMember]
        public bool IsSendWarnings_NotMoveDay { get; set; }
        [DataMember]
        public bool IsSendAlarm_NotMoveDay { get; set; }
        [DataMember]
        public bool IsSendWarnings_MoveNight { get; set; }
        [DataMember]
        public bool IsSendAlarm_MoveNight { get; set; }
        [DataMember]
        public bool IsSendWarnings_NotMoveNight { get; set; }
        [DataMember]
        public bool IsSendAlarm_NotMoveNight { get; set; }
        [DataMember]
        public bool IsSendWarning_PatientOutRoom { get; set; }
        [DataMember]
        public bool IsSendAllarm_PatientOutRoom { get; set; }
        [DataMember]
        public bool IsConnectionLost_SendWarning { get; set; }
        [DataMember]
        public bool IsConnectionLost_SendAlarm { get; set; }
        [DataMember]
        public bool IsMostImportand { get; set; }

        [DataMember]
        public int SendWarningsAfter_MoveDay { get; set; }
        [DataMember]
        public int SendAlarmAfter_MoveDay { get; set; }
        [DataMember]
        public int SendWarningsAfter_NotMoveDay { get; set; }
        [DataMember]
        public int SendAlarmAfter_NotMoveDay { get; set; }
        [DataMember]
        public int SendWarningsAfter_MoveNight { get; set; }
        [DataMember]
        public int SendAlarmAfter_MoveNight { get; set; }
        [DataMember]
        public int SendWarningsAfter_NotMoveNight { get; set; }
        [DataMember]
        public int SendAlarmAfter_NotMoveNight { get; set; }
        [DataMember]
        public int SendWarning_PatientOutRoomValue { get; set; }
        [DataMember]
        public int SendAlarm_PatientOutRoomValue { get; set; }
        [DataMember]
        public int SendAlarm_PatientInProhibitedZone { get; set; }
        [DataMember]
        public int ConnectionLost_SendWarning { get; set; }
        [DataMember]
        public int ConnectionLost_SendAlarm { get; set; }

        
        
        
        [DataMember]
        public bool NoTransmitterWrActive { get; set; }

        [DataMember]
        public short NoTransmitterWrMins { get; set; }

        [DataMember]
        public bool NoTransmitterAlActive { get; set; }

        [DataMember]
        public short NoTransmitterAlMins { get; set; }

        [DataMember]
        public bool MoveSettingsPriority { get; set; }

        [DataMember]
        public short? TransmitterId { get; set; }

        [DataMember]
        public List<Contact> PhoneNumbers { get; set; }

        [DataMember]
        public bool IsProhibitedZone { get; set; }

        [DataMember]
        public List<UnitDTO> ProhibitedZonesList { get; set; }

        [DataMember]
        public string Reason { get; set; }
    }
}
