﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class UnitStatisticsDto
    {
        [DataMember]
        public virtual int UnitId { get; set; }

        [DataMember]
        public virtual string UnitName { get; set; }

		[DataMember]
		public virtual UnitType UnitType { get; set; }

        [DataMember]
        public virtual int PrepCountOfType { get; set; }
    }
}
