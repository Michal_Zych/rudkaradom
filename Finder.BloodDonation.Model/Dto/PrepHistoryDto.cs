﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Finder.BloodDonation.Model.Enum;
using System.ComponentModel.DataAnnotations;
namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class PrepHistoryDto
    {
        [DataMember]
        public virtual long PrepId { get; set; }

        [DataMember]
        [DisplayAttribute(Name = "Umieszczono")]
        public virtual DateTime ArrivalDate { get; set; }

        [DataMember]
        [DisplayAttribute(Name = "Wyjęto")]
        public virtual DateTime? DepartureDate { get; set; }

        [DataMember]
        public virtual int UnitId { get; set; }

        [DataMember]
        public virtual string UnitName { get; set; }

		[DataMember]
		public virtual UnitType UnitType { get; set; }

        [DisplayAttribute(Name = "T(min)")]
        [DataMember]
        public virtual double? MinTemp { get; set; }

        [DisplayAttribute(Name = "T(max)")]
        [DataMember]
        public virtual double? MaxTemp { get; set; }

        [DisplayAttribute(Name = "T(śr)")]
        [DataMember]
        public virtual double? AvgTemp { get; set; }

        [DisplayAttribute(Name = "Użytkownik")]
        [DataMember]
        public virtual string InUserName { get; set; }

        [DisplayAttribute(Name = "Użytkownik")]
        [DataMember]
        public virtual string OutUserName { get; set; }

        [DisplayAttribute(Name = "Numer donacji")]
        [DataMember]
        public virtual string Donation { get; set; }

        [DisplayAttribute(Name = "Typ")]
        [DataMember]
        public virtual string Type { get; set; }

        public override string ToString()
        {
            return String.Format("pid:{0} in:{1} out:{2} min:{3} max:{4} avg:{5} don:{6} type{7}",
                PrepId,
                ArrivalDate,
                DepartureDate,
                MinTemp,
                MaxTemp,
                AvgTemp,
                Donation,
                Type);
        }
    }
}
