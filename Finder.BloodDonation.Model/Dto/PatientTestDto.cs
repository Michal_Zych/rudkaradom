﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class PatientTestDto
    {
        [DataMember]
        public virtual int Id { get; set; } 
        [DataMember]
        public virtual string LastName { get; set; }
        [DataMember]
        public virtual string Name { get; set; }
        [DataMember]
        public virtual int DepartmentId { get; set; }
        [DataMember]
        public virtual string Department { get; set; }
        [DataMember]
        public virtual int RoomId { get; set; }
        [DataMember]
        public virtual string Room { get; set; }
        [DataMember]
        public virtual DateTime InDate { get; set; }
        [DataMember]
        public virtual bool IsAlarm { get; set; } //tak;nie;nie wybrano /*Nazwa kolumy*/
        [DataMember]
        public virtual string _DoNotShow { get; set; }
    }
}
