﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class LogEntryDto : IDataTableDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public virtual int Type { get; set; }

        [DataMember]
        public string TypeDescription { get; set; }

        [DataMember]
        public virtual int UnitId { get; set; }

        [DataMember]
        public string UnitLocation { get; set; }

        [DataMember]
        public virtual int ObjectId { get; set; }

        [DataMember]
        public virtual int ObjectType { get; set; }

        [DataMember]
        public string ObjectTypeDescription { get; set; }

        [DataMember]
        public string ObjectName { get; set; }

        [DataMember]
        public virtual int OperatorId { get; set; }

        [DataMember]
        public string OperatorName { get; set; }

        [DataMember]
        public string EventData { get; set; }

        [DataMember]
        public string Reason { get; set; }

        public string Identity
        {
            get { return "0"; }
        }

        public IEnumerable<int> ShowInUnits
        {
            get { return null; }
        }
    }
}
