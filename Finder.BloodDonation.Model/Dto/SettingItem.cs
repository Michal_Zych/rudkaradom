﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    public class SettingItem
    {
        public string Name { get; set; }
        public string Default { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
    }
}
