﻿using Finder.BloodDonation.Model.Dto.Enums;
using Finder.BloodDonation.Model.Enum;
using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;


namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class BandTestDto
    {
        [DataMember]
        public virtual short Id { get; set; }

        [DataMember]
        public virtual string BarCode { get; set; }

        [DataMember]
        public virtual string MacString { get; set; }

        [DataMember]
        public virtual long? Uid { get; set; }

        [DataMember]
        public virtual string BatteryTypeStr { get; set; }

        [DataMember]
        public virtual EventSeverity? BatteryAlarmSeverity { get; set; }

        [DataMember]
        public virtual DateTime BatteryInstallationDateUtc { get; set; }

        [DataMember]
        public virtual int BatteryDaysToExchange { get; set; }

        [DataMember]
        public virtual string Description { get; set; }

        [DataMember]
        public virtual int ObjectType { get; set; }

        [DataMember]
        public virtual string ObjectDisplayName { get; set; }

        [DataMember]
        public virtual string ObjectUnitName { get; set; }
        [DataMember]
        public virtual string ObjectUnitLocation { get; set; }

        [DataMember]
        public virtual string CurrentUnitName { get; set; }
        [DataMember]
        public virtual string CurrentUnitLocation { get; set; }



    }
}
