﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class VersionInfoDto
    {
        [DataMember]
        public string Version {get; set;}      
        
        [DataMember]
        public DateTime IssueDate {get; set;}
        
        [DataMember]
        public DateTime InstallationDate {get; set;}

        [DataMember]
        public string Description {get; set;}
    }
}
