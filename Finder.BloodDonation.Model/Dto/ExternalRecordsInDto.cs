﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class ExternalRecordsInDto : IDataTableDto
    {
        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int? RequestId { get; set; }

        [DataMember]
        public DateTime StoreDateUtc { get; set; }

        [DataMember]
        public string RecordType { get; set; }

        [DataMember]
        public string RecordId { get; set; }

        [DataMember]
        public string Record { get; set; }

        [DataMember]
        public int? Processed { get; set; }

        [DataMember]
        public int? Try { get; set; }

        [DataMember]
        public DateTime? ProcessedDateUtc { get; set; }

        [DataMember]
        public string Error { get; set; }

        public string Identity
        {
            get { return "1"; }
        }

        public IEnumerable<int> ShowInUnits
        {
            get { return null; }
        }
    }
}
