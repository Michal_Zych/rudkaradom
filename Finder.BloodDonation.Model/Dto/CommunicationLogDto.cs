﻿using System;
using System.Runtime.Serialization;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class CommunicationLogDto
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public int HubID { get; set; }
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public string Level { get; set; }
        [DataMember]
        public string Message { get; set; }
    }
}