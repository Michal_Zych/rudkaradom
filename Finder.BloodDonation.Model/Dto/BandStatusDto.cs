﻿using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class BandStatusDto : IDataTableDto
    {
        [DataMember]
        public int BandId { get; set; }
        [DataMember]
        public virtual byte ObjectType { get; set; }
        [DataMember]
        public string ObjectTypeName { get; set; }
        [DataMember]
        public virtual int ObjectId { get; set; }
        [DataMember]
        public string ObjectDisplayName { get; set; }
        [DataMember]
        public virtual DateTime AssignDateUtc { get; set; }
        [DataMember]
        public virtual int? ObjectUnitId { get; set; }
        [DataMember]
        public string ObjectUnitName { get; set; }
        [DataMember]
        public string ObjectUnitLocation { get; set; }
        [DataMember]
        public virtual int? CurrentUnitId { get; set; }
        [DataMember]
        public string CurrentUnitName { get; set; }
        [DataMember]
        public string CurrentUnitLocation { get; set; }
        [DataMember]
        public DateTime? InCUrrentUnit { get; set; }
        [DataMember]
        public virtual int? PreviousUnitId { get; set; }
        [DataMember]
        public virtual string PreviousUnitName { get; set; }
        [DataMember]
        public virtual string PreviousUnitLocation { get; set; }
        [DataMember]
        public virtual DateTime? InPreviousUnit { get; set; }
        [DataMember]
        public virtual int TopAlarmId { get; set; }
        [DataMember]
        public virtual byte TopAlarmType { get; set; }
        [DataMember]
        public string TopAlarmTypeName { get; set; }
        [DataMember]
        public virtual byte TopAlarmSeverity { get; set; }
        [DataMember]
        public string TopAlarmSevertityName { get; set; }
        [DataMember]
        public string TopAlarmMessage { get; set; }
        [DataMember]
        public DateTime? TopAlarmDate { get; set; }
        [DataMember]
        public virtual DateTime? TopEventDate { get; set; }
        [DataMember]
        public int Severity1Count { get; set; }
        [DataMember]
        public int Severtity2Count { get; set; }
        [DataMember]
        public virtual bool? AlarmNoGoZoneOngoing { get; set; }
        [DataMember]
        public virtual bool? AlarmDisconnectedOngoing { get; set; }
        [DataMember]
        public virtual bool? AlarmZoneOutOngoing { get; set; }
        [DataMember]
        public virtual bool? AlarmNoMovingOngoing { get; set; }
        [DataMember]
        public virtual bool? AlarmMovingOngoing { get; set; }
        [DataMember]
        public virtual
        bool? AlarmLowBatteryOngoing { get; set; }
        [DataMember]
        public virtual long SynchroToken { get; set; }




        public string Identity
        {
            get { return BandId.ToString(); }
        }

        public IEnumerable<int> ShowInUnits
        {
            get
            {
                if (ObjectUnitId.HasValue)
                {
                    return new int[] { ObjectUnitId.Value, (CurrentUnitId ?? PreviousUnitId ?? ObjectUnitId).Value };
                }
                return null;
            }
        }
    }
}
