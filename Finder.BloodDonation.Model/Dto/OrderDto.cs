﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class OrderDto
    {
        public OrderDto()
        {
        }
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int? TypeId { get; set; }

        [DataMember]
        public string Group { get; set; }

        [DataMember]
        public string DonationNumber { get; set; }

        [DataMember]
        public string FinalCode { get; set; }

        [DataMember]
        public string FinalCodeDesc { get; set; }

        [DataMember]
        public string Class { get; set; }

        [DataMember]
        public string ClassDesc { get; set; }

        [DataMember]
        public string Division { get; set; }

        [DataMember]
        public string Location { get; set; }

        [DataMember]
        public int? Position { get; set; }

        [DataMember]
        public string Action { get; set; }

        [DataMember]
        public int? ForUsesrID { get; set; }

        [DataMember]
        public string ForUser { get; set; }

        [DataMember]
        public string ForUserFullName { get; set; }

        [DataMember]
        public DateTime CreatedDate { get; set; }

        [DataMember]
        public int? CreatedById { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public string CreatedByFullName { get; set; }

        [DataMember]
        public DateTime? ExecutedDate { get; set; }

        [DataMember]
        public int? ExecutedById { get; set; }

        [DataMember]
        public string ExecutedBy { get; set; }

        [DataMember]
        public string ExecutedByFullName { get; set; }

        [DataMember]
        public bool? Active { get; set; }

        public override string ToString()
        {
            return String.Format("Id:{0} Created:{1} Action:{2}", Id, CreatedDate, Action);
        }
    }
}