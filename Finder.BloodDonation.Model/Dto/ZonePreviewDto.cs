﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class ZonePreviewDto
    {
        [DataMember]
        public int Id { get; set; }
        
        [DataMember]
        public string Name { get; set; }
        
        [DataMember]
        public string Location { get; set; }

        [DataMember]
        public virtual double TLX { get; set; }

        [DataMember]
        public virtual double TLY { get; set; }
        
        [DataMember]
        public virtual double BRX { get; set; }

        [DataMember]
        public virtual double BRY { get; set; }
    }
}
