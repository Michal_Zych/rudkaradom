﻿using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class BandInfoDto : IDataTableDto
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string BarCode { get; set; }
        [DataMember]
        public string MacString { get; set; }
        [DataMember]
        public long Uid { get; set; }
        [DataMember]
        public string SerialNumber { get; set; }
        [DataMember]
        public virtual byte BatteryTypeId { get; set; }
        [DataMember]
        public string BatteryTypeName { get; set; }
        [DataMember]
        public virtual int BatteryAlarmSeverityId { get; set; }
        [DataMember]
        public string BatteryAlarmSeverityName { get; set; }
        [DataMember]
        public DateTime BatteryInstallationDateUtc { get; set; }
        [DataMember]
        public int BatteryDaysToExchange { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public virtual byte ObjectTypeId { get; set; }
        [DataMember]
        public string ObjectTypeName { get; set; }
        [DataMember]
        public string ObjectDisplayName { get; set; }
        [DataMember]
        public int UnitId { get; set; }
        [DataMember]
        public string UnitName { get; set; }
        [DataMember]
        public string UnitLocation { get; set; }
        [DataMember]
        public int CurrentUnitId { get; set; }
        [DataMember]
        public string CurrentUnitName { get; set; }
        [DataMember]
        public string CurrentUnitLocation { get; set; }





        public virtual string Identity
        {
            get { return Id.ToString(); }
        }

        public virtual IEnumerable<int> ShowInUnits
        {
            get { return new int[] { UnitId }; }
        }
    }
}
