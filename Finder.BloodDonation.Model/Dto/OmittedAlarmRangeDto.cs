﻿using System;
using System.Runtime.Serialization;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract(Name = "oxx")]
    public class OmittedAlarmRangeDto
    {
        [DataMember(Name = "oxc")]
        public virtual DateTime DateStart { get; set; }
        [DataMember(Name = "oxd")]
        public virtual DateTime DateEnd { get; set; }
    }
}