﻿using System;
using System.Runtime.Serialization;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public partial class CurrentDataDto
    {
        [DataMember]
        public virtual int ID { get; set; }
        [DataMember]
        public virtual int DeviceID { get; set; }
        [DataMember]
        public virtual string DeviceName { get; set; }
        [DataMember]
        public virtual DeviceType DeviceType { get; set; }
        [DataMember]
        public virtual int? UnitTypeV2 { get; set; }
        [DataMember]
        public virtual double A1L { get; set; }
        [DataMember]
        public virtual double A1H { get; set; }
        [DataMember]
        public virtual double A2L { get; set; }
        [DataMember]
        public virtual double A2H { get; set; }
        [DataMember]
        public virtual DateTime DataTime { get; set; }
        [DataMember]
        public virtual double? Temp1 { get; set; }
        [DataMember]
        public virtual double? Temp2 { get; set; }
    }
}