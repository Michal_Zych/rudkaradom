﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class ZonesInfoDto
    {
        [DataMember]
        public int UnitId { get; set; }
        [DataMember]
        public int TransmitterId { get; set; }
        [DataMember]
        public bool IsNoGoZone { get; set; }
    }
}
