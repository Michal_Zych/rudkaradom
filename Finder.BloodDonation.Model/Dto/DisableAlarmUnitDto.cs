﻿using System;
using System.Runtime.Serialization;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class DisableAlarmUnitDto
    {
        [DataMember]
        public int UnitID { get; set; }
        /*
        [DataMember]
        public bool DisableAlarms { get; set; }
        [DataMember]
        public bool DisableAlarmsDate { get; set; }
        [DataMember]
        public DateTime? DisableAlarmsDateFrom { get; set; }
        [DataMember]
        public DateTime? DisableAlarmsDateTo { get; set; }
        [DataMember]
        public bool DisableAlarmsOmmitedRanges { get; set; }
         */
    }
}