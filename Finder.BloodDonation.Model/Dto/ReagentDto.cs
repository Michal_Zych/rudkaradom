﻿using System;
using System.Runtime.Serialization;

namespace Finder.BloodDonation.Model.Dto
{
    [KnownType(typeof(PrepDto))]
    [KnownType(typeof(ProductDto))]
    [DataContract]
    public abstract class PrepBaseDto
    {
        public const string StatusFilterName = "statuses";
        public const string ValidUntilFromFilterName = "vuFrom";
        public const string ValidUntilToFilterName = "vuTo";
        public const string TypeField = "TP";
        public const string ValidUntilField = "VU";
        public const string CapacityField = "CP";

        [DataMember]
        public virtual DateTime AddedDate { get; set; }

        [DataMember]
        public int Capacity { get; set; }

        [DataMember]
        public virtual long Id { get; set; }

        [DataMember]
        public virtual int StatusId { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public virtual int TypeId { get; set; }

        [DataMember]
        public virtual long Uid { get; set; }

        [DataMember]
        public virtual int UnitId { get; set; }

        [DataMember]
        public DateTime ValidUntil { get; set; }
    }

    [DataContract]
    public class ProductDto : PrepBaseDto
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string ProductCode { get; set; }

        [DataMember]
        public string SerialNumber { get; set; }
    }
}