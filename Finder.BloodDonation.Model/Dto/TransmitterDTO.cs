﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class TransmitterDTO
    {
        [DataMember]
        public int? Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public long Mac { get; set; }

        [DataMember]
        public string BarCode { get; set; }

        [DataMember]
        public string MacString { get; set; }
        
        [DataMember]
        public int Type { get; set; }
        
        [DataMember]
        public string MSISDN { get; set; }

        [DataMember]
        public int ReportIntervalSec { get; set; }

        [DataMember]
        public int Sensitivity{ get; set; }
        
        [DataMember]
        public int AvgCalcTimeS { get; set; }

        [DataMember]
        public int SwitchDelayS { get; set; }

        [DataMember]
        public bool? IsBandUpdater { get; set; }
        
        [DataMember]
        public string TransmitterInstallationUnit { get; set; }
        
        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int? UnitId { get; set; }

        [DataMember]
        public int? AssignUnitId { get; set; }

        [DataMember]
        public short? RssiTreshold{get; set;}
        
        [DataMember]
        public byte Group{get; set;}
        
        [DataMember]
        public string IpAddress{get; set;}

        [DataMember]
        public string Transitions { get; set; }

    }
}
