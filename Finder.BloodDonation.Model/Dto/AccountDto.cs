﻿using System;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using FinderFX.SL.Core.MVVM;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class AccountDto :
        MementoEditableObject<AccountDto>,
        System.ComponentModel.INotifyPropertyChanged

    {
        #region INotifyPropertyChanged Members

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null))
            {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        private int IDField;
        [Editable(false)]
        [Key]
        [Display(AutoGenerateField = false)]
        [DataMember]
        public virtual int ID
        {
            get
            {
                return this.IDField;
            }
            set
            {
                if ((this.IDField.Equals(value) != true))
                {
                    Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "ID" });
                    this.IDField = value;
                    this.RaisePropertyChanged("ID");
                }
            }
        }

        private string LoginField;
        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole jest wymagane")]
        [Display(Name = "Login")]        
        [DataMember]
        public virtual string Login
        {
            get
            {
                return this.LoginField;
            }
            set
            {
                if ((object.ReferenceEquals(this.LoginField, value) != true))
                {
                    Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "Login" });
                    this.LoginField = value;
                    this.RaisePropertyChanged("Login");
                }
            }
        }

        private string PasswordField;

        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole jest wymagane")]
        [Display(Name = "Hasło")]
        [DataType(DataType.Password)]
        [DataMember]
        public virtual string Password
        {
            get
            {
                return this.PasswordField;
            }
            set
            {
                if ((object.ReferenceEquals(this.PasswordField, value) != true))
                {
                    Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "Password" });
                    this.PasswordField = value;
                    this.RaisePropertyChanged("Password");
                }
            }
        }

        private string NameField;
        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole jest wymagane")]
        [Display(Name = "Imię")]
        [DataMember]
        public virtual string Name
        {
            get
            {
                return this.NameField;
            }
            set
            {
                if ((object.ReferenceEquals(this.NameField, value) != true))
                {
                    Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "Name" });
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }

        private string LastNameField;
        [Required(AllowEmptyStrings = false, ErrorMessage = "Pole jest wymagane")]
        [Display(Name = "Nazwisko")]
        [DataMember]
        public virtual string LastName
        {
            get
            {
                return this.LastNameField;
            }
            set
            {
                if ((object.ReferenceEquals(this.LastNameField, value) != true))
                {
                    Validator.ValidateProperty(value, new ValidationContext(this, null, null)
                                                          {
                                                              MemberName = "LastName"
                                                          });
                    this.LastNameField = value;
                    this.RaisePropertyChanged("LastName");
                }
            }
        }

        private string PhoneField;        
        [Display(Name = "Telefon")]
        [DataMember]
        public virtual string Phone
        {
            get
            {
                return this.PhoneField;
            }
            set
            {
                if ((object.ReferenceEquals(this.PhoneField, value) != true))
                {
                    Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "Phone" });
                    this.PhoneField = value;
                    this.RaisePropertyChanged("Phone");
                }
            }
        }

        private long? RfidUidField;
        [Display(Name = "RfidUid", AutoGenerateField = false)]
        [Editable(false)]
        [DataMember]
        public virtual long? RfidUid
        {
            get
            {
                return this.RfidUidField;
            }
            set
            {
                if ((object.ReferenceEquals(this.RfidUidField, value) != true))
                {
                    Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "RfidUid" });
                    this.RfidUidField = value;
                    this.RaisePropertyChanged("RfidUid");
                }
            }
        }


        private int ClientIdField;
        [Display(AutoGenerateField = false)]
        [Editable(false)]
        [DataMember]
        public virtual int ClientId
        {
            get
            {
                return this.ClientIdField;
            }

            set
            {
                if ((object.ReferenceEquals(this.ClientIdField, value) != true))
                {
                    Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "ClientId" });
                    this.ClientIdField = value;
                    this.RaisePropertyChanged("ClientId");
                }
            }
        }

        private string EMailField;
        [Display(Name = "e-mail")]
        [DataMember]
        public virtual string EMail
        {
            get
            {
                return this.EMailField;
            }
            set
            {
                if ((object.ReferenceEquals(this.EMailField, value) != true))
                {
                    Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "EMail" });
                    this.EMailField = value;
                    this.RaisePropertyChanged("EMail");
                }
            }
        }

        private DateTime PasswordChangeDateField;
        [Display(AutoGenerateField = false)]
        [Editable(false)]
        [DataMember]
        public virtual DateTime PasswordChangeDate
        {
            get
            {
                return this.PasswordChangeDateField;
            }
            set
            {
                if ((object.ReferenceEquals(this.PasswordChangeDateField, value) != true))
                {
                    Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "PasswordChangeDate" });
                    this.PasswordChangeDateField = value;
                    this.RaisePropertyChanged("PasswordChangeDate");
                }
            }
        }


        //private bool ActiveField;
        //[Editable(false)]
        //[DataMember]
        //public virtual bool Acitve
        //{
        //    get
        //    {
        //        return this.ActiveField;
        //    }

        //    set
        //    {
        //        if ((object.ReferenceEquals(this.ActiveField, value) != true))
        //        {
        //            Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "Acitve" });
        //            this.ActiveField = value;
        //            this.RaisePropertyChanged("Acitve");
        //        }
        //    }
        //}
    }
}