﻿using System;
using System.Runtime.Serialization;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class NotificationReceiverDto
    {
        [DataMember]
        public virtual int UserId { get; set; }

        [DataMember]
        public virtual bool Sms { get; set; }

        [DataMember]
        public virtual bool Email { get; set; }

    }
}