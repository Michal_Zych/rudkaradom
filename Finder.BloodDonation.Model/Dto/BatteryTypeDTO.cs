﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    public class BatteryTypeDTO
    {
        [DataMember]
        public virtual byte Id { get; set; }
        [DataMember]
        public virtual string Name { get; set; }
        [DataMember]
        public virtual short LifeMonts { get; set; }
        [DataMember]
        public virtual int Capacity { get; set; }
        [DataMember]
        public virtual float Voltage { get; set; }
        [DataMember]
        public virtual string Description { get; set; }
    }
}
