﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace Finder.BloodDonation.Model.Dto
{
     [DataContract]
	public class UnitPrepCountDto
	{
         [Display(AutoGenerateField=false)]
         [DataMember]
		public int UnitId { get; set; }

         [Display(Name="Ilość preparatów")]
         [DataMember]
		public int PrepCount { get; set; }

		 public override string ToString()
		 {
			 return String.Format("id:{0}/{1}", UnitId, PrepCount);
		 }
	}
}
