﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class MessageDto
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public byte MessageType { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime EndDate { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}
