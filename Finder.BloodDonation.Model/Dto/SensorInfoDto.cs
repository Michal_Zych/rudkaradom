﻿using System;
using System.Runtime.Serialization;



namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class SensorInfoDto
    {
        [DataMember]
        public virtual short ID { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual string Location { get; set; }

    }
}
