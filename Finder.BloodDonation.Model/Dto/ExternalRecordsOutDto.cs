﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class ExternalRecordsOutDto : IDataTableDto
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public int? EventId { get; set; }

        [DataMember]
        public DateTime DateUtc { get; set; }

        [DataMember]
        public string RecordType { get; set; }

        [DataMember]
        public string Record { get; set; }

        [DataMember]
        public int? ParentId { get; set; }

        [DataMember]
        public int? Try { get; set; }

        [DataMember]
        public bool? TryAgain { get; set; }

        [DataMember]
        public DateTime? SentDateUtc { get; set; }

        public string Error { get; set; }

        public string Identity
        {
            get { return Id.ToString(); }
        }

        public IEnumerable<int> ShowInUnits
        {
            get { return null; }
        }
    }
}
