﻿using System;
using System.Runtime.Serialization;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract(Name="a1")]
    public class DataDto
    {
		[DataMember(Name = "b1")]
        public virtual short SensorID { get; set; }
		[DataMember(Name = "c1")]
        public virtual DateTime Time { get; set; }
		[DataMember(Name = "d1")]
        public virtual double? Value { get; set; }

    }
}