﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Dto
{
    [DataContract]
    public class ServiceStatusDto : IDataTableDto
    {
        [DataMember]
        public string ServiceName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public DateTime StartTimeUtc { get; set; }

        [DataMember]
        public DateTime PingTimeUtc { get; set; }

        [DataMember]
        public DateTime PreviousPingTimeUtc { get; set; }

        [DataMember]
        public bool HasPartner { get; set; }

        [DataMember]
        public DateTime PartnerPingTimeUtc { get; set; }

        [DataMember]
        public string InfoTxt { get; set; }

        public string Identity
        {
            get { return "0"; }
        }

        public IEnumerable<int> ShowInUnits
        {
            get { return null; }
        }
    }
}
