﻿using System;
using System.Text;
using FinderFX.Web.Core.Authentication;
using FinderFX.SL.Core.Authentication;
using System.Runtime.Serialization;
using FluentNHibernate.Mapping;

namespace Finder.BloodDonation.Model.Domain
{
    public class Data
    {
        public virtual short SensorID { get; set; }
        public virtual DateTime Time { get; set; }
        public virtual double? Value { get; set; }


        public override bool Equals(object obj)
        {
            var other = obj as Data;

            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return this.SensorID == other.SensorID &&
                this.Time == other.Time;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = GetType().GetHashCode();
                hash = (hash * 31) ^ SensorID.GetHashCode();
                hash = (hash * 31) ^ Time.GetHashCode();

                return hash;
            }
        }

    }

    public class DataMap : ClassMap<Data>
    {
        public DataMap()
        {
            Table("Measurements");
            CompositeId().KeyProperty(x => x.SensorID).KeyProperty(x => x.Time, "MeasurementTime");
            Map(x => x.Value,"Value");
        }


    }
}
