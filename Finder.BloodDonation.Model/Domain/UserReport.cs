﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Domain
{
    public class UserReport
    {
        public virtual int ID { get; set; }
        public virtual int UserID { get; set; }
        public virtual int ReportID { get; set; }
        public virtual Report Report { get; set; }
        public virtual DateTime DateFrom { get; set; }
        public virtual DateTime DateTo { get; set; }
        public virtual int UnitID { get; set; }
        public virtual bool HideCharts { get; set; }
        public virtual bool Mono { get; set; }
        public virtual bool HyperPermission {get; set;}
    }

    public class UserReportMap : ClassMap<UserReport>
    {
        public UserReportMap()
        {
            Table("UsersReports");
            Id(x => x.ID);
            Map(x => x.UserID);
            Map(x => x.ReportID);
            Map(x => x.DateFrom);
            Map(x => x.DateTo);
            Map(x => x.UnitID);
            Map(x => x.HideCharts);
            Map(x => x.Mono);
            Map(x => x.HyperPermission);
            References(x => x.Report).Column("ReportID").Not.Insert().Not.Update().LazyLoad();
        }
    }
}
