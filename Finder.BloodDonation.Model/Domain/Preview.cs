﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Domain
{
    public class Preview
    {
        public virtual int ID { get; set; }
        public virtual string Path { get; set; }
        public virtual int UnitID { get; set; }
        public virtual UnitType UnitType { get; set; }
        public virtual int PictureID { get; set; }
    }

    public class PreviewMap : ClassMap<Preview>
    {
        public PreviewMap()
        {
            Table("Previews");
            Id(x => x.ID);
            Map(x => x.Path);
            Map(x => x.UnitID);
            Map(x => x.UnitType).CustomType<UnitType>();
            Map(x => x.PictureID);
        }
    }
}
