﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FinderFX.Web.Core.Authentication;
using FinderFX.SL.Core.Authentication;
using System.Runtime.Serialization;
using FluentNHibernate.Mapping;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Domain
{
    public class CurrentData
    {
        public virtual long ID { get; set; }
        public virtual int DeviceID { get; set; }
        public virtual string DeviceName { get; set; }
        public virtual DeviceType DeviceType { get; set; }
        public virtual int? UnitTypeV2 { get; set; }
        public virtual double A1L { get; set; }
        public virtual double A1H { get; set; }
        public virtual double A2L { get; set; }
        public virtual double A2H { get; set; }
        public virtual DateTime DataTime { get; set; }
        public virtual double? Temp1 { get; set; }
        public virtual double? Temp2 { get; set; }
    }

    public class CurrentDataMap : ClassMap<CurrentData>
    {
        public CurrentDataMap()
        {
            Table("CurrentData");
            Id(x => x.ID);
            Map(x => x.DeviceID);
            Map(x => x.DeviceName);
            Map(x => x.DeviceType).CustomType<DeviceType>();
            Map(x => x.UnitTypeV2);
            Map(x => x.A1L);
            Map(x => x.A1H);
            Map(x => x.A2L);
            Map(x => x.A2H);
            Map(x => x.DataTime);
            Map(x => x.Temp1, "T1");
            Map(x => x.Temp2, "T2");
        }
    }
}
