﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Domain
{
    public class EditPreview
    {
        public virtual int ID { get; set; }
        public virtual string Path { get; set; }
        public virtual string Name { get; set; }
        public virtual int ClientID { get; set; }
    }

    public class EditPreviewMap : ClassMap<EditPreview>
    {
        public EditPreviewMap()
        {
            Table("Pictures");
            Id(x => x.ID);
            Map(x => x.Path);
            Map(x => x.Name);
            Map(x => x.ClientID);
        }
    }
}
