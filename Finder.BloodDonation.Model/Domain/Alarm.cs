﻿using System;
using System.Text;
using FinderFX.Web.Core.Authentication;
using FinderFX.SL.Core.Authentication;
using System.Runtime.Serialization;
using FluentNHibernate.Mapping;

namespace Finder.BloodDonation.Model.Domain
{
    public class Alarm
    {
        public virtual int ID { get; set; }

        public virtual byte EventType { get; set; }
        public virtual DateTime AlarmTime { get; set; }

        public virtual short SensorID { get; set; }
        public virtual string SensorName { get; set; }
        public virtual int UnitID { get; set; }
        public virtual string Location { get; set; }
        public virtual string MU { get; set; }


        public virtual double LoRange { get; set; }
        public virtual double UpRange { get; set; }
        public virtual double? Value { get; set; }

        public virtual DateTime? DateEnd { get; set; }
        public virtual DateTime? AlarmCloseDate { get; set; }
        public virtual int? AlarmCloseUserID { get; set; }
        public virtual string AlarmCloseUserName { get; set; }
        public virtual string AlarmCloseComment { get; set; }
        public virtual bool IsBitSensor { get; set; }
        public virtual string LoStateDesc { get; set; }
        public virtual string HiStateDesc { get; set; }

    }

    public class AlarmMap : ClassMap<Alarm>
    {
        public AlarmMap()
        {
            Table("Alarms");
            Id(x => x.ID);


            Map(x => x.EventType);
            Map(x => x.AlarmTime, "Timestamp");
            
            Map(x => x.SensorID);
            Map(x => x.SensorName);
            Map(x => x.UnitID);
            Map(x => x.Location);
            Map(x => x.MU);

            Map(x => x.LoRange);
            Map(x => x.UpRange);
            Map(x => x.Value);

            Map(x => x.DateEnd);
            Map(x => x.AlarmCloseDate);
            Map(x => x.AlarmCloseUserID);
            Map(x => x.AlarmCloseUserName);
            Map(x => x.AlarmCloseComment);
            Map(x => x.IsBitSensor);
            Map(x => x.LoStateDesc);
            Map(x => x.HiStateDesc);
        }
    }
}
