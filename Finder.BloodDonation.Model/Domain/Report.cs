﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Domain
{
    public class Report
    {
        public virtual int ID { get; set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string RDL { get; set; }
		public virtual int Type { get; set; }
        public virtual string Picture { get; set; }
    }

    public class ReportMap : ClassMap<Report>
    {
        public ReportMap()
        {
            Table("Reports");
            Id(x => x.ID);
            Map(x => x.Name);
            Map(x => x.Description);
            Map(x => x.RDL);
	        Map(x => x.Type);
            Map(x => x.Picture);
        }
    }
}
