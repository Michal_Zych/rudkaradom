﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;

namespace Finder.BloodDonation.Model.Domain
{
	public class Client
	{
		public virtual int ID { get; set; }
		public virtual string Name { get; set; }
		public virtual int ApplicationID { get; set; }
		public virtual string HostName { get; set; }
		public virtual int? DefaultUserID { get; set; }
	}

	public class ClientMap : ClassMap<Client>
	{
		public ClientMap()
		{
			Table("Clients");
			Id(x => x.ID);
			Map(x => x.Name);
			Map(x => x.ApplicationID);
			Map(x => x.HostName);
			Map(x => x.DefaultUserID);
		}
	}
}
