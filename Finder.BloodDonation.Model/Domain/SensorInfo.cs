﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FinderFX.Web.Core.Authentication;
using FinderFX.SL.Core.Authentication;
using System.Runtime.Serialization;
using FluentNHibernate.Mapping;
using Finder.BloodDonation.Model.Enum;
namespace Finder.BloodDonation.Model.Domain
{
    public class SensorInfo
    {
        public virtual short ID { get; set; }
        public virtual string Name { get; set; }
        public virtual string Location { get; set; }
    }

    public class SensorInfoMap : ClassMap<SensorInfo>
    {
        public SensorInfoMap()
        {
            Table("Sensors");
            Id(x => x.ID);
            Map(x => x.Name);
            Map(x => x.Location);
        }
    }
}
