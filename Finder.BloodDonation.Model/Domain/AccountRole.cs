﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Domain
{
    public class AccountRole
    {
        public virtual int UserID { get; set; }
        public virtual int RoleID { get; set; }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class AccountRoleMap : ClassMap<AccountRole>
    {
        public AccountRoleMap()
        {
            Table("UsersRoles");
            CompositeId()
                .KeyProperty(x => x.UserID)
                .KeyProperty(x => x.RoleID);
        }
    }
}
