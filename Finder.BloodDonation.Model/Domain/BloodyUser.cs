﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FinderFX.Web.Core.Authentication;
using FinderFX.SL.Core.Authentication;
using System.Runtime.Serialization;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Model.Authentication;

namespace Finder.BloodDonation.Model.Domain
{
    [DataContract]
    public class BloodyUser : User
    {
        public static BloodyUser Current
        {
            get
            {
                return User.Current as BloodyUser;
            }
        }

        public string RfidUidHash { get; set; }

        public static readonly int FAVORITE_ID = -1;

        private Dictionary<int, IList<DBRole>> _unitsRoles;
        private UnitDTO _root;

        public BloodyUser(bool isAuthenticated, string name, string[] roles, int id, string passwordHash, int clientID, string theme, string skin)
            : base(isAuthenticated, name, roles, id, passwordHash, clientID, theme, skin)
        {
        }


        internal void SetUnitsRoles(Dictionary<int, IList<DBRole>> unitsRoles)
        {
            _unitsRoles = unitsRoles;
        }

        internal void SetUnits(UnitDTO root)
        {
            _root = root;
        }

        #region CloneUser

        public override User CloneUser(bool includePassword)
        {
            BloodyUser user = new BloodyUser(
                this.Identity.IsAuthenticated,
                this.Identity.Name,
                this.Roles.ToArray(),
                this.ID,
                includePassword ? this.PasswordHash : "",
                this.ClientID,
                this.Theme,
                this.Skin);

            user.SetUnitsRoles(this._unitsRoles);

            foreach (KeyValuePair<string, string> pair in this._customProperites)
            {
                user.SetProperty(pair.Key, pair.Value);
            }

            return user;
        }

        #endregion CloneUser

        #region ToString

        private readonly string MAIN_SEPARATOR = "%^&";

        public override string ToString()
        {
            string json = base.ToString();

            StringBuilder builder = new StringBuilder();

            Dictionary<string, List<string>> rolesDictionary = new Dictionary<string, List<string>>();

            Dictionary<string, List<string>> permissionsDictionary = new Dictionary<string, List<string>>();

            foreach (KeyValuePair<int, IList<DBRole>> pair in _unitsRoles)
            {
                List<string> rolesList = new List<string>();
                permissionsDictionary.Add(pair.Key.ToString(), rolesList);
                foreach (DBRole role in pair.Value)
                {
                    if (!rolesDictionary.ContainsKey(role.ID.ToString()))
                        rolesDictionary.Add(role.ID.ToString(), role.GetPermissionsList());

                    rolesList.Add(role.ID.ToString());
                }
            }


            string rolesString = JsonHelper.Serialize(rolesDictionary);
            string permissionsString = JsonHelper.Serialize(permissionsDictionary);
            string customProps = JsonHelper.Serialize(_customProperites);
            //Dictionary<string, List<string>> rolesDictionary2 = JsonHelper.Deserialize<Dictionary<string, List<string>>>(rolesString);
            //Dictionary<string, List<string>> permissionsDictionary2 = JsonHelper.Deserialize<Dictionary<string, List<string>>>(permissionsString);



            return json + MAIN_SEPARATOR +
                    rolesString + MAIN_SEPARATOR +
                    permissionsString + MAIN_SEPARATOR +
                    customProps;
        }

        #endregion ToString

        public IList<string> GetUnitPermissions(int unitID)
        {
            if (_unitsRoles.ContainsKey(unitID))
            {
                IList<string> permissions = new List<string>();
                foreach (DBRole r in _unitsRoles[unitID])
                {
                    foreach (DBPermission p in r.Permissions)
                    {
                        if (!permissions.Contains(p.Name))
                            permissions.Add(p.Name);
                    }
                }

                return permissions;
            }
            else
                return null;
        }

        public bool HasPermissions(int unitID, string permission)
        {
            if (!_unitsRoles.ContainsKey(unitID))
                return false;

            foreach (DBRole r in _unitsRoles[unitID])
            {
                foreach (DBPermission p in r.Permissions)
                {
                    if (p.Name == permission)
                        return true;
                }
            }

            return false;
        }

        public List<int> GetUserDBRoles()
        {
            List<int> roles = new List<int>();

            if (_unitsRoles == null)
                return roles;

            foreach (KeyValuePair<int, IList<DBRole>> pair in _unitsRoles)
            {
                if (pair.Value == null)
                    continue;

                foreach (DBRole r in pair.Value)
                {
                    if (!roles.Contains(r.ID))
                    {
                        roles.Add(r.ID);
                    }
                }
            }

            return roles;
        }

        public bool CanSeeAllAlarms
        {
            get
            {
                return IsInRole(PermissionNames.CanSeeAllAlarms);
            }
        }


        private UnitDTO GetUnit(UnitDTO parentUnit, int unitID)
        {
            if (parentUnit.ID == unitID)
                return parentUnit;

            if (parentUnit.Children == null || parentUnit.Children.Count == 0)
                return null;

            foreach (UnitDTO u in parentUnit.Children)
            {
                UnitDTO child = GetUnit(u, unitID);
                if (child != null)
                    return child;
            }

            return null;
        }

        //todo: dodac permissions
        private UnitDTO GetUnit(int unitID)
        {
            if (this.Root.ID == unitID)
                return this.Root;

            foreach (UnitDTO u in this._root.Children)
            {
                UnitDTO un = GetUnit(u, unitID);
                if (un != null)
                    return un;
            }

            return null;
        }

        public UnitDTO GetUnit(int unitID, string permission)
        {
            if (!this.HasPermissions(unitID, permission))
                return null;

            return GetUnit(unitID);
        }

        public UnitDTO GetParentUnit(int unitID, string permission)
        {
            UnitDTO childUnit = GetUnit(unitID, permission);

            if (childUnit == null)
                return null;

            if (!childUnit.ParentUnitID.HasValue)
                return null;

            return GetUnit(childUnit.ParentUnitID.Value, permission);
        }

        public UnitDTO Root
        {
            get
            {
                return _root.Clone(true);
            }
        }

        public IList<UnitDTO> GetChildUnits(int unitID, string permission)
        {
            return GetChildUnits(unitID, permission, null);
        }

        public IList<UnitDTO> GetChildUnits(int unitID, string permission, IList<int> unitType, int? depth)
        {
            if (depth.HasValue && depth.Value <= 0)
                throw new ArgumentOutOfRangeException("Depth must be greater than 0.");

            IList<UnitDTO> children = new List<UnitDTO>();

            UnitDTO unit = GetUnit(_root, unitID);
            if (unit == null)
                return children;

            int currentDepth = 0;


            FindUnits(unit, permission, unitType, children, currentDepth + 1, depth);

            return children;

        }

        public IList<UnitDTO> GetChildUnitsWithParent(int unitID, string permission, IList<int> unitType)
        {
            UnitDTO parent = GetUnit(unitID, permission);
            IList<UnitDTO> list = GetChildUnits(unitID, permission, unitType);

            if (parent != null)
            {
                if (list == null)
                    list = new List<UnitDTO>();

                list.Insert(0, parent);
            }

            return list;
        }

        public IList<UnitDTO> GetChildUnits(int unitID, string permission, IList<int> unitType)
        {
            return GetChildUnits(unitID, permission, unitType, null);
        }

        private void FindUnits(UnitDTO parent, string permission, IList<int> unitType, IList<UnitDTO> listToAdd, int currentDepth, int? maxDepth)
        {
            if (parent.Children == null)
                return;

            if (maxDepth.HasValue && currentDepth > maxDepth.Value)
                return;

            foreach (UnitDTO u in parent.Children)
            {
                bool hasPermission = HasPermissions(u.ID, permission);
                bool isProperUnitType = unitType == null ?
                    true :
                    unitType.Contains(u.UnitTypeIDv2) ? true : false;

                if (hasPermission && isProperUnitType)
                {
                    UnitDTO clone = u.Clone();
                    clone.Children = null;
                    listToAdd.Add(clone);
                }

                FindUnits(u, permission, unitType, listToAdd, currentDepth + 1, maxDepth);
            }
        }

        #region Custom properties

        internal Dictionary<string, string> _customProperites = new Dictionary<string, string>();

        public void SetProperty(string key, string value)
        {
            if (_customProperites.ContainsKey(key))
            {
                _customProperites[key] = value;
            }
            else
            {
                _customProperites.Add(key, value);
            }
        }

        #endregion Custom properties
    }
}
