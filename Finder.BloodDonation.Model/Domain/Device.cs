﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FinderFX.Web.Core.Authentication;
using FinderFX.SL.Core.Authentication;
using System.Runtime.Serialization;
using FluentNHibernate.Mapping;

namespace Finder.BloodDonation.Model.Domain
{
    public class Device
    {
        public virtual int ID { get; set; }
        public virtual string Name { get; set; }
        public virtual int Address { get; set; }
        public virtual int DeviceType { get; set; }
        public virtual int CalibrationRequest { get; set; }
        public virtual int Timeout { get; set; }
        public virtual double Calibration1 { get; set; }
        public virtual double Calibration2 { get; set; }
        public virtual double A1L { get; set; }
        public virtual double A1H { get; set; }
        public virtual double A2L { get; set; }
        public virtual double A2H { get; set; }
        public virtual int ReadInterval { get; set; }
        public virtual int Parameters { get; set; }
        public virtual int HubID { get; set; }
        public virtual long IsCurrentConfig { get; set; }
        public virtual int? PreviousConfigurationID { get; set; }
        public virtual int? ValidationProfileID { get; set; }
		public virtual int AlarmRangeID { get; set; }
        //public virtual int ValidationUser { get; set; }
        //public virtual DateTime ValidationStartDate { get; set; }
		public virtual int DeviceModelID { get; set; }
    }

    public class DeviceMap : ClassMap<Device>
    {
        public DeviceMap()
        {
            Table("Devices");
            Id(x => x.ID);
            Map(x => x.Address);
            Map(x => x.DeviceType);
            Map(x => x.CalibrationRequest);
            Map(x => x.Timeout);
            Map(x => x.Calibration1);
            Map(x => x.Calibration2);
            Map(x => x.A1L);
            Map(x => x.A1H);
            Map(x => x.A2L);
            Map(x => x.A2H);
            Map(x => x.ReadInterval);
            Map(x => x.Parameters);
            Map(x => x.HubID);
            Map(x => x.IsCurrentConfig);
            Map(x => x.Name);
			Map(x => x.DeviceModelID);
            Map(x => x.PreviousConfigurationID);
            Map(x => x.ValidationProfileID);
			Map(x => x.AlarmRangeID);
            //Map(x => x.ValidationUser);
            //Map(x => x.ValidationStartDate);
        }
    }
}
