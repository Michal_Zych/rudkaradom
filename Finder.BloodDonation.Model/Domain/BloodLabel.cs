﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Model.Domain
{
    public class BloodLabel
    {
        public virtual long ID { get; set; }
        public virtual long? Uid { get; set; }
        public virtual int Inversion { get; set; }
        public virtual string DonationNumber { get; set; }
        
        //schemat, podział wiersza to |
        public virtual string Producer { get; set; }

        public virtual string CollectionDate { get; set; }
        public virtual string PrepDate { get; set; }
        public virtual string FinalOrClassAndDivision { get; set; }

        //schemat, podział wiesza na | i coś jeszcze
        public virtual string Description { get; set; }

        public virtual string Capacity { get; set; }
        public virtual string Comments { get; set; }
        public virtual string Code { get; set; }
        public virtual string Symbol { get; set; }
        public virtual string RhDesc { get; set; }
        public virtual string ValidCode { get; set; }
        public virtual string ValidDate { get; set;}
        public virtual string AdditionalCode { get; set; }
        public virtual string Fenotyp { get; set; }
        public virtual string Qualification { get; set; }

        //Dane nieprzetworzone - do wpisania do taga
        public virtual DateTime? DataValidUntil { get; set; }
        public virtual int? DataCapacity { get; set; }
        public virtual int DataUnitID { get; set; }

    }

    public class BloodLabelMap : ClassMap<BloodLabel>
    {
        public BloodLabelMap()
        {
            Table("ASDF");
            Schema("dbo");
            Id(x => x.ID);
            Map(x => x.Uid, "Uid");
            Map(x => x.Inversion, "Inversion");
            Map(x => x.DonationNumber, "DonationNumber");
            Map(x => x.Producer, "Producer");
            Map(x => x.CollectionDate, "CollectionDate");
            Map(x => x.PrepDate, "PrepDate");
            Map(x => x.FinalOrClassAndDivision, "FinalOrClassAndDivision");
            Map(x => x.Description, "Description");
            Map(x => x.Capacity, "Capacity");
            Map(x => x.Comments, "Comments");
            Map(x => x.Code, "Code");
            Map(x => x.Symbol, "Symbol");
            Map(x => x.RhDesc, "RhDesc");
            Map(x => x.ValidCode, "ValidCode");
            Map(x => x.AdditionalCode, "AdditionalCode");
            Map(x => x.Fenotyp, "Fenotyp");
            Map(x => x.Qualification, "Qualification");

            Map(x => x.DataValidUntil, "DataValidUntil");
            Map(x => x.DataCapacity, "DataCapacity");
            Map(x => x.DataUnitID, "DataUnitID");
        }
    }
}
