﻿using System;
using System.Text;
using FinderFX.Web.Core.Authentication;
using FinderFX.SL.Core.Authentication;
using System.Runtime.Serialization;
using FluentNHibernate.Mapping;

namespace Finder.BloodDonation.Model.Domain
{
    public class CommunicationLog
    {
        public virtual int ID { get; set; }
        public virtual int HubID { get; set; }
        public virtual DateTime Date { get; set; }
        public virtual string Level { get; set; }
        public virtual string Message { get; set; }
    }

    public class CommunicationLogMap : ClassMap<CommunicationLog>
    {
        public CommunicationLogMap()
        {
            Table("CommunicationLogs");
            Id(x => x.ID);
            Map(x => x.HubID);
            Map(x => x.Date);
            Map(x => x.Level);
            Map(x => x.Message);
        }
    }
}
