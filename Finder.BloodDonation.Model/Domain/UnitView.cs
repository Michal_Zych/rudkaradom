﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Domain
{
    public class UnitView
    {
        public virtual int ID { get; set; }
        public virtual string Name { get; set; }
        public virtual int? ParentUnitID { get; set; }
        public virtual UnitType UnitType { get; set; }
        
        public virtual int DeviceID { get; set; }
        public virtual double A1L { get; set; }
        public virtual double A1H { get; set; }
        public virtual double A2L { get; set; }
        public virtual double A2H { get; set; }
        public virtual double T1 { get; set; }
        public virtual double T2 { get; set; }
    }


    public class UnitViewMap : ClassMap<UnitView>
    {
        public UnitViewMap()
        {
            Table("Units");
            Id(x => x.ID);
            Map(x => x.Name);
            Map(x => x.ParentUnitID);
            Map(x => x.UnitType).CustomType<UnitType>();

            Map(x => x.DeviceID);
            Map(x => x.A1L);
            Map(x => x.A1H);
            Map(x => x.A2L);
            Map(x => x.A2H);
            Map(x => x.T1);
            Map(x => x.T2);
        }
    }
}
