﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Domain
{
    public class EditUnitPreview
    {
        public virtual int PreviewedUnitID { get; set; }
        public virtual int UnitID { get; set; }
        public virtual double TLX { get; set; }
        public virtual double TLY { get; set; }
        public virtual double BRX { get; set; }
        public virtual double BRY { get; set; }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    public class EditUnitPreviewMap : ClassMap<EditUnitPreview>
    {
        public EditUnitPreviewMap()
        {
            Table("PreviewsUnits");
            CompositeId()
                .KeyProperty(x => x.PreviewedUnitID)
                .KeyProperty(x => x.UnitID);
                
            Map(x => x.TLX);
            Map(x => x.TLY);
            Map(x => x.BRX);
            Map(x => x.BRY);
        }
    }
}
