﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;
using NHibernate.Transform;

namespace Finder.BloodDonation.Model.Domain
{
    [Serializable]
    public class DBUser
    {
        public virtual int ID { get; set; }
        public virtual string Login { get; set; }
        public virtual string Password { get; set; }
        public virtual string Settings { get; set; }
        public virtual int ClientID { get; set; }
        public virtual long? RfidUid { get; set; }

        public virtual IList<DBRole> Roles { get; set; }

        public virtual IList<DefaultUsersUnits> DefaultUnits { get; set; }

        public virtual bool Active { get; set; }
    }

    public class DBUserMap : ClassMap<DBUser>
    {
        public DBUserMap()
        {
            Table("Users");
            Id(x => x.ID);
            Map(x => x.Login);
            Map(x => x.Password);
            Map(x => x.ClientID);
            Map(x => x.Settings);
            Map(x => x.RfidUid);
            Map(x => x.Active);

            HasManyToMany<DBRole>(x => x.Roles)
                .Table("UsersRoles")
                .ParentKeyColumn("UserID")
                .ChildKeyColumn("RoleID").Not.LazyLoad();

            HasMany<DefaultUsersUnits>(x => x.DefaultUnits)
                .KeyColumn("UserID");

        }
    }

    public class DBRole
    {
        public virtual int ID { get; set; }
        public virtual string Name { get; set; }
        public virtual int ApplicationID { get; set; }
        public virtual int? ParentRoleID { get; set; }

        public virtual IList<DBPermission> Permissions { get; set; }

        protected internal virtual List<string> GetPermissionsList()
        {
            List<string> list = new List<string>();
            foreach (DBPermission perm in Permissions)
            {
                list.Add(perm.Name);
            }

            return list;
        }
    }

    public class DBRoleMap : ClassMap<DBRole>
    {
        public DBRoleMap()
        {
            Table("Roles");
            Id(x => x.ID);
            Map(x => x.Name);
            Map(x => x.ApplicationID);
            Map(x => x.ParentRoleID);

            HasManyToMany<DBPermission>(x => x.Permissions)
                .Table("RolesPermissions")
                .ParentKeyColumn("RoleID")
                .ChildKeyColumn("PermissionID");
        }
    }

    public class DbRoleTransformer : IResultTransformer
    {
        #region Implementation of IResultTransformer

        public object TransformTuple(object[] tuple, string[] aliases)
        {
            DBRole result = null;
            if (tuple != null && aliases != null && tuple.Any() && aliases.Any())
            {
                result = new DBRole()
                {
                    ID = (int)tuple[0],
                    Name = (string)tuple[1]
                };
            }
            return result;
        }

        public IList TransformList(IList collection)
        {
            return collection;
        }

        #endregion
    }

    public class DBPermission
    {
        public virtual int ID { get; set; }
        public virtual string Name { get; set; }
        public virtual int ApplicationID { get; set; }
    }

    public class DBPermissionMap : ClassMap<DBPermission>
    {
        public DBPermissionMap()
        {
            Table("Permissions");
            Id(x => x.ID);
            Map(x => x.Name);
            Map(x => x.ApplicationID);
        }
    }


}
