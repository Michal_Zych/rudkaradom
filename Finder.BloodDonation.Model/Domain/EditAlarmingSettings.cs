﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Finder.BloodDonation.Model.Domain
{
    public class EditAlarmingSettings
    {
        //konfiguracja alarmowania
        public virtual int DeviceID { get; set; }
        public virtual bool Disabled { get; set; }
        public virtual DateTime? DisabledFrom { get; set; }
        public virtual bool DisabledForRanges { get; set; }
        public virtual bool DisabledForHours { get; set; }
        public virtual DateTime? DisableHoursFrom1 { get; set; }
        public virtual DateTime? DisableHoursTo1 { get; set; }
        public virtual DateTime? DisableHoursFrom2 { get; set; }
        public virtual DateTime? DisableHoursTo2 { get; set; }
        public virtual bool SmsEnabled { get; set; }
        public virtual bool ServiceEnabled { get; set; }
        public virtual short? SmsT1RangeDelay { get; set; }
        public virtual short? SmsT2RangeDelay { get; set; }
        public virtual short? SmsT1TechDelay { get; set; }
        public virtual short? SmsT2TectDelay { get; set; }
        public virtual short? ServiceT1RangeDelay { get; set; }
        public virtual short? ServiceT2RangeDelay { get; set; }
        public virtual short? ServiceT1TechDelay { get; set; }
        public virtual short? ServiceT2TectDelay { get; set; }
        public virtual short GuiT1RangeDelay { get; set; }
        public virtual short GuiT2RangeDelay { get; set; }
        public virtual short GuiT1TechDelay { get; set; }
        public virtual short GuiT2TectDelay { get; set; }
    }

    public class EditAlarmingSettingsMap : ClassMap<EditAlarmingSettings>
    {
        public EditAlarmingSettingsMap()
        {
            Table("AlarmConfigs");
            Id(x => x.DeviceID);
            Map(x => x.DeviceID);
            Map(x => x.Disabled);
            Map(x => x.DisabledFrom);
            Map(x => x.DisabledForRanges);
            Map(x => x.DisabledForHours);
            Map(x => x.DisableHoursFrom1);
            Map(x => x.DisableHoursTo1);
            Map(x => x.DisableHoursFrom2);
            Map(x => x.DisableHoursTo2);
            Map(x => x.SmsEnabled);
            Map(x => x.ServiceEnabled);
            Map(x => x.SmsT1RangeDelay);
            Map(x => x.SmsT2RangeDelay);
            Map(x => x.SmsT1TechDelay);
            Map(x => x.SmsT2TectDelay);
            Map(x => x.ServiceT1RangeDelay);
            Map(x => x.ServiceT2RangeDelay);
            Map(x => x.ServiceT1TechDelay);
            Map(x => x.ServiceT2TectDelay);
            Map(x => x.GuiT1RangeDelay);
            Map(x => x.GuiT2RangeDelay);
            Map(x => x.GuiT1TechDelay);
            Map(x => x.GuiT2TectDelay);

        }
    }
}
