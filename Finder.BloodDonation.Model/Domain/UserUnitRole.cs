﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Finder.BloodDonation.Model.Domain
{
	public class UserUnitRole
	{
		public virtual int UserID { get; set; }
		public virtual int UnitID { get; set; }
		public virtual int RoleID { get; set; }


		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}

	public class UserUnitRoleMap : ClassMap<UserUnitRole>
	{
		public UserUnitRoleMap()
		{
			Table("UsersUnitsRoles");
			CompositeId()
				.KeyProperty(x=> x.UnitID)
				.KeyProperty(x => x.UserID)
				.KeyProperty(x => x.RoleID);

			//Id(x => x.UserID);
			//Map(x => x.UnitID);
			//Map(x => x.RoleID);
		}
	}
}
