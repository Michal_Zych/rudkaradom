﻿using System;
using System.Text;
using FinderFX.Web.Core.Authentication;
using FinderFX.SL.Core.Authentication;
using System.Runtime.Serialization;
using FluentNHibernate.Mapping;

namespace Finder.BloodDonation.Model.Domain
{
    public class OmmitedAlarmRange
    {
        public virtual int ID { get; set; }

        public virtual int DeviceID { get; set; }

        public virtual DateTime DateStart { get; set; }
        public virtual DateTime DateEnd { get; set; }
    }

    public class OmmitedAlarmRangeMap : ClassMap<OmmitedAlarmRange>
    {
        public OmmitedAlarmRangeMap()
        {
            Table("OmmitedAlarmRanges");
            Id(x => x.ID);
            Map(x => x.DeviceID);
            Map(x => x.DateStart);
            Map(x => x.DateEnd);
        }
    }
}
