﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Domain
{
	public class UnitDTO
	{
		public virtual int ID { get; set; }
		public virtual string Name { get; set; }
		public virtual int? ParentUnitID { get; set; }
        public virtual UnitType UnitTypeID { get; set; }

		public virtual IList<UnitDTO> Children { get; set; }
		//public virtual UnitDTO ParentUnit { get; set; }
	}
}
