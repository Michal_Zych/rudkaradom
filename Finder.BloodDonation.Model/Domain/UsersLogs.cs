﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Domain
{
    public class UserLog
    {
        public virtual int ID { get; set; }
        public virtual int UserID { get; set; }
        //public virtual DateTime LoggingDate { get; set; }
        //public virtual DateTime LastPingDate{ get; set; }
        public virtual bool Monitoring { get; set; }
    }

    public class UserLogMap : ClassMap<UserLog>
    {
        public UserLogMap()
        {
            Table("UsersLogs");
            Id(x => x.ID);
            Map(x => x.UserID);
            //Map(x => x.LastPingDate).Nullable();
            //Map(x => x.LoggingDate).Nullable();
            Map(x => x.Monitoring);
        }
    }
}
