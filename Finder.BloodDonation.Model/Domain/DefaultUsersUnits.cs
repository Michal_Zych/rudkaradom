﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Finder.BloodDonation.Model.Domain
{
	public class DefaultUsersUnits
	{
		public virtual int UserID { get; set; }
		public virtual int UnitID { get; set; }

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}

	public class DefaultUsersUnitsMap : ClassMap<DefaultUsersUnits>
	{
		public DefaultUsersUnitsMap()
		{
			Table("DefaultUsersUnits");
			CompositeId()
				.KeyProperty(x => x.UnitID)
				.KeyProperty(x => x.UserID);
		}
	}
}
