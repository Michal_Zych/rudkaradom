﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace Finder.BloodDonation.Model.Domain
{
	public class FavoriteUnit
	{
		
		public virtual int UserID { get; set; }
		public virtual int UnitID { get; set; }

		public virtual Unit Unit { get; set; }

		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}

	public class FavoriteUnitMap : ClassMap<FavoriteUnit>
	{
		public FavoriteUnitMap()
		{
			Table("FavoriteUnits");
			CompositeId()
				.KeyProperty(x => x.UnitID)
				.KeyProperty(x => x.UserID);

			References<Unit>(x => x.Unit, "UnitID")
				.Not.Insert();
		}
	}
}
