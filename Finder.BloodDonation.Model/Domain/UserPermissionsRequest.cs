﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Finder.BloodDonation.Model.Dto;

namespace Finder.BloodDonation.Model.Domain
{
    [DataContract]
    public class UserPermissionsRequest
    {
        [Required, DataMember]
        public int UserId { get; set; }

        [Required, DataMember]
        public int RoleId { get; set; }

        [Required, DataMember]
        public IList<int> UnitId { get; set; }
    }

    [DataContract]
    public class UserPermissionsResponse
    {
        [Required, DataMember]
        public List<UnitDTO> Units { get; set; }

        [Required, DataMember]
        public DBRoleDTO Role { get; set; }

        [Required, DataMember]
        public AccountDto User { get; set; }

        [Required, DataMember]
        public UserPermissionsRequest Request { get; set; }

        public UserPermissionsResponse()
        {
            this.Units = new List<UnitDTO>();
        }
    }
}
