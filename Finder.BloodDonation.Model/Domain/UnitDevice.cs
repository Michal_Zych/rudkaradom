﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Domain
{
      public class UnitDevice
    {
        public virtual int ID { get; set; }
        public virtual string Name { get; set; }
        public virtual int? ParentUnitID { get; set; }
        public virtual int? DeviceID { get; set; }
        public virtual UnitType UnitType { get; set; }
    }

    public class UnitDeviceMap : ClassMap<UnitDevice>
    {
        public UnitDeviceMap()
        {
            Table("Units");
            Id(x => x.ID);
            Map(x => x.Name);
            Map(x => x.ParentUnitID);
            Map(x => x.DeviceID);
            Map(x => x.UnitType, "UnitTypeID").CustomType<UnitType>();
        }
    }
}
