﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Model.Domain
{
	public class FavoriteUnitDTO
	{
		public virtual int UserID { get; set; }
		public virtual int UnitID { get; set; }
	}
}
