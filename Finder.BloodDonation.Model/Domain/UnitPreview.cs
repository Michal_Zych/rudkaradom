﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Domain
{
    public class UnitPreview
    {
        public virtual int ID { get; set; }
        public virtual string Name { get; set; }
        public virtual int? ParentUnitID { get; set; }
        public virtual UnitType UnitType { get; set; }

        public virtual double PreviewTLX { get; set; }
        public virtual double PreviewTLY { get; set; }
        public virtual double PreviewBRX { get; set; }
        public virtual double PreviewBRY { get; set; }

        public virtual string Location { get; set; }

        public virtual int UnitTypeV2 { get; set; }

        public virtual int? PictureID { get; set; }
        public virtual string MeasureUnit { get; set; }
        public virtual double? Value { get; set; }
        public virtual string LoRange { get; set; }
        public virtual string UpRange { get; set; }
        public virtual DateTime? MeasureTime { get; set; }
    }


    public class UnitPreviewMap : ClassMap<UnitPreview>
    {
        public UnitPreviewMap()
        {
            Table("UnitPreview");
            Id(x => x.ID);
            Map(x => x.Name);
            Map(x => x.ParentUnitID);
            Map(x => x.UnitType).CustomType<UnitType>();
            Map(x => x.PreviewTLX);
            Map(x => x.PreviewTLY);
            Map(x => x.PreviewBRX);
            Map(x => x.PreviewBRY);

            Map(x => x.Location);

            Map(x => x.UnitTypeV2);

            Map(x => x.PictureID);
            Map(x => x.MeasureUnit);
            Map(x => x.Value);
            Map(x => x.LoRange);
            Map(x => x.UpRange);
            Map(x => x.MeasureTime);
        }
    }
}
