﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using NHibernate.Transform;

namespace Finder.BloodDonation.Model.Domain
{
    public class UnitLevel
    {
        public virtual int UnitId { get; set; }
        public virtual int Level { get; set; }
    }

    public class UnitLevelTransformer : IResultTransformer
    {

        public System.Collections.IList TransformList(System.Collections.IList collection)
        {
            return collection;
        }

        public object TransformTuple(object[] tuple, string[] aliases)
        {
            UnitLevel result = null;
            if (tuple != null && aliases != null && tuple.Any() && aliases.Any())
            {
                result = new UnitLevel()
                             {
                                 UnitId = (int)tuple[0],
                                 Level = (int)tuple[1]
                             };
            }
            return result;

        }
    }


}
