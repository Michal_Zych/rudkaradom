﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Finder.BloodDonation.Model.Dto;

namespace Finder.BloodDonation.Model.Domain
{
    public class AttributeTypeMap
    {
        public virtual int NameCode { get; set; }
        public virtual string PrepType { get; set; }   
    }

    //public class AttributeTypeMapMap : ClassMap<AttributeTypeMap>
    //{
    //    public AttributeTypeMapMap()
    //    {
    //        Table("RfidNcPrepType");
    //        Map(x => x.NameCode);
    //        Map(x => x.PrepType);
    //    }
    //}
}
