﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Domain
{
    public class Unit
    {
        public virtual int ID { get; set; }
        public virtual string Name { get; set; }
        public virtual int? ParentUnitID { get; set; }
        public virtual UnitType UnitType { get; set; }
        
        public virtual Unit ParentUnit { get; set; }
        public virtual IList<Unit> Children { get; set; }
        public virtual Unit Parent { get; set; }

        public virtual int ClientID { get; set; }
        public virtual int UnitTypeIDv2 { get; set; }
        public virtual int? PictureID { get; set; }
        
        public virtual string Description { get; set; }
        public virtual bool IsMonitored { get; set; }
        
  //      public virtual bool? Active { get; set; }
    }


    public class UnitMap : ClassMap<Unit>
    {
        public UnitMap()
        {
            Table("Units");
            Id(x => x.ID);
            Map(x => x.Name);
            Map(x => x.ParentUnitID);
            Map(x => x.UnitType,"UnitTypeID").CustomType<UnitType>();
            Map(x => x.ClientID);
            Map(x => x.UnitTypeIDv2);
            Map(x => x.PictureID);
            Map(x => x.Description);
            Map(x => x.IsMonitored);

  //          Map(x => x.Active);
            
            //References<Unit>(x => x.ParentUnit, "ParentUnitID")
            //    .Not.Insert()
            //    .Not.Update();

            HasMany<Unit>(x => x.Children)
                .KeyColumn("ParentUnitID")
                .ForeignKeyConstraintName("FK_Units_Units")
                .Cascade.AllDeleteOrphan();

            References<Unit>(x => x.ParentUnit, "ParentUnitID").ReadOnly();

            //HasOne<Unit>(x => x.ParentUnit)
            //    .ForeignKey("ID");
        }
    }
}
