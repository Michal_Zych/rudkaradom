﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Finder.BloodDonation.Model.Dto;

namespace Finder.BloodDonation.Model.Domain
{
    [DataContract]
    public class DataAdministration
    {
        [DataMember]
        public IList<DBRoleDTO> AllRoles { get; set; }

    }
}
