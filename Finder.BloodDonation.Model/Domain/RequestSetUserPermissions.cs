﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Model.Domain
{
    [DataContract]
    public class RequestSetUserPermissions
    {
        [DataMember]
        public virtual int UserId { get; set; }
        [DataMember]
        public virtual int RoleId { get; set; }
        [DataMember]
        public virtual IList<int> Units { get; set; }
        [DataMember]
        public virtual IList<int> RfidStatuses { get; set; } 
    }
}
