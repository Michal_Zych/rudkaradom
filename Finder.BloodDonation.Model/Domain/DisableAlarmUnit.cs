﻿using System;
using System.Text;
using FinderFX.Web.Core.Authentication;
using FinderFX.SL.Core.Authentication;
using System.Runtime.Serialization;
using FluentNHibernate.Mapping;

namespace Finder.BloodDonation.Model.Domain
{
    public class DisableAlarmUnit
    {
        public virtual int UnitID { get; set; }
        /*
        public virtual bool? DisableAlarms { get; set; }
        public virtual bool? DisableAlarmsDate { get; set; }
        public virtual DateTime? DisableAlarmsDateFrom { get; set; }
        public virtual DateTime? DisableAlarmsDateTo { get; set; }
        public virtual bool? DisableAlarmsOmmitedRanges { get; set; }
        public virtual DateTime? DisableAlarmsOmmitedDateFrom { get; set; }
        public virtual DateTime? DisableAlarmsOmmitedDateTo { get; set; }
         */
    }

    public class DisableAlarmUnitMap : ClassMap<DisableAlarmUnit>
    {
        public DisableAlarmUnitMap()
        {
            Table("Units");
            Id(x => x.UnitID);
         /*
            Map(x => x.DisableAlarms).Nullable();
            Map(x => x.DisableAlarmsDate).Nullable();
            Map(x => x.DisableAlarmsDateFrom).Nullable();
            Map(x => x.DisableAlarmsDateTo).Nullable();
            Map(x => x.DisableAlarmsOmmitedRanges).Nullable();
            Map(x => x.DisableAlarmsOmmitedDateFrom).Nullable();
            Map(x => x.DisableAlarmsOmmitedDateTo).Nullable();
          */
        }
    }
}
