﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Model.Domain
{
    public class Account
    {
        public virtual int ID { get; set; }
        //public virtual int UnitID { get; set; }
        public virtual string Login { get; set; }
        public virtual string Password { get; set; }
        public virtual string Name { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Phone { get; set; }
		public virtual long? RfidUid { get; set; }
        public virtual int Active { get; set; }
        public virtual int ClientID { get; set; }
        public virtual string EMail { get; set; }
        public virtual DateTime PasswordChangeDate { get; set; }
    }

    public class AccountMap : ClassMap<Account>
    {
        public AccountMap()
        {
            Table("Users");
            Id(x => x.ID);
            Map(x => x.Name);
            //Map(x => x.UnitID);
            Map(x => x.LastName);
            Map(x => x.Login);
            Map(x => x.Password);
            Map(x => x.Phone);
			Map(x => x.RfidUid);
            Map(x => x.ClientID);
            Map(x => x.EMail);
            Map(x => x.PasswordChangeDate);
        }
    }
}
