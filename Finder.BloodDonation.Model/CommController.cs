﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Configuration;

namespace Finder.BloodDonation.Model
{
    /*internal enum CommAction
    {
        GetReport = 0,
        RefreshConfiguration = 1
    }*/

    internal static class CommController
    {
        private static string CommActionAddresFormat
        {
            get
            {
                return ConfigurationManager.AppSettings["CommURL"] + "?{0}-{1}";
            }
        }

        public static int InvokeCommAction(string action, string parameter)
        {
            try
            {
                WebRequest action_request = WebRequest.Create(
                    string.Format(CommActionAddresFormat, action, parameter));
                action_request.Method = "GET";
                action_request.Timeout = 5000;
                WebResponse action_response = action_request.GetResponse();

                //TODO: check HttpWebResponse

                return 1;
            }
            catch (Exception exc)
            {

            }

            return -1;
        }
    }
}
