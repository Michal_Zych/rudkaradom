﻿using System;

namespace Finder.BloodDonation.Model
{
    public class ConfigurationsKeys
    {   
        //Ustawienia z kolumny Users.Settings - zaczynające się od _ nie będą zapisywane w bazie
        public const string SELECTED_OPTION = "so";
        public const string SELECTED_UNIT = "su";
        public const string SELECTED_TAB = "st";
        public const string SELECTED_SUBVIEW = "ss";
        public const string DEFAULT_UNIT = "du";
        public const string DEFAULT_TAB = "dt";
        public const string SPLITTER_HEIGHT = "sh";

        public const string SERVICES_DOMAIN = "ServicesDomains";
        
        //Ustawienia tylko do odczytu - nie są zapisywane w bazie - muszą zaczynać się od podkreślenia (aby nie były zapisywane do bazy)
        public const string CHART_SERVER_PORT = "_ChartsServerPort";
        public const string BASKET_UNIT_ID = "_BasketUnitId";
        public const string ROOT_UNIT_ID = "_RootUnitId";
        
        //Stałe - nie ustawienia
        public const string CLIENT_PARAM_NAME = "client";
        public const string COOKIE_NAME = "loginM2M";
        public const string PREVIEWS_FOLDER = "Previews";
        public const string PROFILES_FOLDER = "Profiles";
        public const string APP_VERSION = "1.0.1";
    }
}
