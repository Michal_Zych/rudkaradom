﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Model.Authentication
{
    public static class PermissionNames
    {
        public const string UnitsListing = "UnitsListing";
        public const string UnitsPlanning = "UnitsPlanning";
        public const string UnitMonitoring = "UnitMonitoring";
        public const string AlarmListing = "AlarmListing";
        public const string UnitConfiguration = "UnitConfiguration";
        public const string Reports = "Reports";
        public const string HubsListing = "HubsListing";
        public const string Validation = "Validation";
        public const string AddUnit = "AddUnit";
        public const string RemoveUnit = "RemoveUnit";
        public const string UnitsRearranging = "UnitsRearranging";
        public const string UnitsPermissions = "UnitsPermissions";
        public const string UnitsManagement = "UnitsManagement";
        public const string Diagnostics = "Diagnostics";
        public const string HubsLogs = "HubsLogs";
        public const string UnitConfigurationAdvance = "UnitConfigurationAdvance";
        public const string UnitContentView = "UnitContentView";
        public const string UnitContentManagement = "UnitContentManagement";
        public const string BasketManagement = "BasketManagement";
        public const string AllStatusesSearch = "AllStatusesSearch";
        public const string DeletePrep = "DeletePrep";
        public const string AllStatusesAccess = "AllStatusesAccess";
        public const string AdministrationAccess = "AdministrationAccess";
        public const string TypeAttributes = "TypeAttributesAccess";
        public const string SummaryBasketsPreview = "SummaryBasketsPreview";
        public const string CanSeeAllAlarms = "CanSeeAllAlarms";
        public const string HyperAdmin = "HyperAdmin";
        public const string Admin = AdministrationAccess;
        public const string SuperUser = "SuperUser";

        public const string ToolsTransmitters = "ToolsTransmitters";
        public const string ToolsBands = "ToolsBands";
        public const string ToolsExternalSystems = "ToolsExternalSystems";

        public const string Archive = "Archive";

        public const string AdministrationPersonell = "AdministrationPersonell";

        public const string AdministrationMessages = "AdministrationMessages";
        public const string AdministrationSystemSettings = "AdministrationSystemSettings";
        public const string AdministrationLoggedUsers = "AdministrationLoggedUsers";
        public const string AdministrationSystemEvents = "AdministrationSystemEvents";
        public const string AdministrationVersions = "AdministrationVersions";

        public const string PatientDetails = "PatientDetails";
        public const string PatientEvents = "PatientEvents";
        public const string PatientReports = "PatientReports";
        public const string PatientConfiguration = "PatientConfiguration";

        public const string UnitManager = "UnitManager";
        public const string UnitPatients = "UnitPatients";
        public const string UnitLocalization = "UnitLocalization";
        public const string UnitEvents = "UnitEvents";
        public const string UnitReports = "UnitReports";
        public const string UnitPlanning = "UnitPlanning";

        public const string UnitTransmitters = "UnitTransmitters";
        public const string UnitBands = "UnitBands";
        public const string AdministrationServiceStatuses = "AdministrationServiceStatuses";
    }
}
