﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FinderFX.Web.Core.Authentication;
using NHibernate;
using FinderFX.Web.Core.Communication;
using NHibernate.Linq;
using System.Configuration;
using Finder.BloodDonation.Model.Domain;
using Finder.BloodDonation.Model.Services;
using Finder.BloodDonation.Model.Dto;
using System.Diagnostics;
using FinderFX.Web.Core.Caching;
using AutoMapper;
using Finder.BloodDonation.Model.Enum;
using System.Security;
using log4net;
using Finder.BloodDonation.Helpers;

namespace Finder.BloodDonation.Model.Authentication
{
    public class BloodAuthenticationManager : AuthenticationManager
    {
        
        private static ILog Logger = LogManager.GetLogger(typeof(BloodAuthenticationManager));

        
        public BloodAuthenticationManager()
        {
            this.DisableInnerCache = true;
        }

        public BloodyUser GetUserById(int userId)
        {
            return (GetUser(userId) as BloodyUser);
        }

        private User ConvertUser(DBUser dbUser)
        {
            List<string> permissions = new List<string>();
            foreach (DBRole role in dbUser.Roles)
            {
                foreach (DBPermission p in role.Permissions)
                {
                    if (!permissions.Contains(p.Name))
                        permissions.Add(p.Name);
                }
            }

            BloodyUser user = new BloodyUser(
                true,
                dbUser.Login,
                permissions.ToArray(),
                dbUser.ID,
                dbUser.Password,
                dbUser.ClientID,
                "Default",
                "");
			user.RfidUidHash = dbUser.RfidUid.HasValue ? HashHelper.CalculateHash(dbUser.RfidUid.Value.ToString()) : (string)null;

            IList<UnitDTO> allUnits = GetAllUnits(user.ClientID);

            user.SetUnitsRoles(GetUnitsPermissions(dbUser, allUnits));
            user.SetUnits(GetUnitsTreeBasedOnPermissions(allUnits, user));

            user.SetProperty(ConfigurationsKeys.CHART_SERVER_PORT, ConfigurationManager.AppSettings["ChartsServerPort"]);
            user.SetProperty(ConfigurationsKeys.SERVICES_DOMAIN, ConfigurationManager.AppSettings["ServicesDomains"]);

			var layoutSelections = GetLayoutSelections(dbUser.Settings);
			if (layoutSelections != null)
			{
				foreach (var s in layoutSelections)
				{
					user.SetProperty(s.Key, s.Value);
				}
			}
            
            var rootUnitId = GetRootUnitIdForClient(dbUser.ClientID);
            var registartionUnitId = GetRegistrationWardId(dbUser.ClientID);
            var unassignedPatientsUnitId = GetUnassignedPatientsUnitId(dbUser.ClientID);

            UnitTypeOrganizer.SetRegistrationAndUnassignedPatientsUnitId(registartionUnitId, unassignedPatientsUnitId);
            user.SetProperty(ConfigurationsKeys.ROOT_UNIT_ID, rootUnitId.ToString());

            return user;
        }

        private static int GetRegistrationWardId(int clientId)
        {
            var unitId = -1;

            using (var s = NHibernateHttpModule.OpenSession())
            {
                var sql = @"
                    select c.RegistrationWardId as unitId
                    from dbo.Clients c
                    where Id = :clientId
                ";

                unitId = s.CreateSQLQuery(sql)
                    .AddScalar("unitId", NHibernateUtil.Int32)
                    .SetInt32("clientId", clientId)
                    .UniqueResult<int>();
            }

            return unitId;
        }

        private static int GetUnassignedPatientsUnitId(int clientId)
        {
            var unitId = -1;

            using (var s = NHibernateHttpModule.OpenSession())
            {
                var sql = @"
                    select c.UnassignedPatientsUnitId as unitId
                    from dbo.Clients c
                    where Id = :clientId
                ";

                unitId = s.CreateSQLQuery(sql)
                    .AddScalar("unitId", NHibernateUtil.Int32)
                    .SetInt32("clientId", clientId)
                    .UniqueResult<int>();
            }

            return unitId;
        }

        private static int GetRootUnitIdForClient(int clientId)
		{
			var unitId = -1;
			using (var s = NHibernateHttpModule.OpenSession())
			{
				var sql = @"
					select c.RootUnitId as unitId
					from dbo.Clients c
					where Id = :clientId
				";

				unitId = s.CreateSQLQuery(sql)
					.AddScalar("unitId", NHibernateUtil.Int32)
					.SetInt32("clientId", clientId)
					.UniqueResult<int>();
			}

			return unitId;
		}

        private static UnitDTO GetUnitsTreeBasedOnPermissions(IList<UnitDTO> allUnits, BloodyUser user)
        {
            UnitDTO tempUnit = allUnits.FirstOrDefault(x => x.ParentUnitID == null);

            UnitDTO root = UnitsService.GetTree(tempUnit.ID, allUnits);

            UnitsService.FilterUnits(root, user);

            return root;
        }

        private static IList<UnitDTO> GetAllUnits(int clientID)
        {
            PermissionsService proxy = new PermissionsService();
            IList<UnitDTO> allUnits = proxy.GetAllUnitsForClient(clientID);

            return allUnits;
        }

        private static Dictionary<int, IList<DBRole>> GetUnitsPermissions(DBUser user, IList<UnitDTO> allUnits)
        {
            PermissionsService proxy = new PermissionsService();
            IList<DBRole> allRoles = proxy.GetAllRoles(false);

            IList<UserUnitRole> unitsRoles = proxy.GetUserUnitsRoles(user.ID);

            Dictionary<int, IList<DBRole>> unitsPermissions = new Dictionary<int, IList<DBRole>>();
            foreach (UserUnitRole uur in unitsRoles)
            {
                DBRole role = allRoles.First(x => x.ID == uur.RoleID);

                IList<DBRole> roles = GetRoleList(uur.UnitID, unitsPermissions);
                if (!roles.Contains(role))
                    roles.Add(role);

                //dodaj rowniez role dla wszystkich dzieci
                UnitDTO parent = allUnits.First(x => x.ID == uur.UnitID);
                IList<UnitDTO> children = GetChildUnits(parent);
                foreach (UnitDTO child in children)
                {
                    roles = GetRoleList(child.ID, unitsPermissions);
                    if (!roles.Contains(role))
                        roles.Add(role);
                }
            }


            return unitsPermissions;
        }

        private static IList<DBRole> GetRoleList(int unitID, Dictionary<int, IList<DBRole>> unitsPermissions)
        {
            if (!unitsPermissions.ContainsKey(unitID))
            {
                unitsPermissions.Add(unitID, new List<DBRole>());
            }
            return unitsPermissions[unitID];
        }

        private static IList<UnitDTO> GetChildUnits(UnitDTO parentUnit)
        {
            List<UnitDTO> children = new List<UnitDTO>();

            if (parentUnit.Children == null || parentUnit.Children.Count == 0)
                return children;

            children.AddRange(parentUnit.Children);

            foreach (UnitDTO c in parentUnit.Children)
            {
                children.AddRange(GetChildUnits(c));
            }

            return children;

        }

        [CacheResource(300, true)] //5 minut
        public override User LoadUser(int id)
        {
            try
            {
                //Stopwatch stopper = new Stopwatch();
                //stopper.Start();
                DBUser user;
                using (ISession session = NHibernateHttpModule.OpenSession())
                {
                    user = session.Get<DBUser>(id);

                    if (user == null)
                        return User.AnonymousUser;

                    foreach (DBRole r in user.Roles)
                    {
                        int count = r.Permissions.Count;
                    }
                }


                User u = ConvertUser(user);

                //stopper.Stop();
                //Debug.WriteLine("User {0} loaded in {1}.", id, stopper.ElapsedMilliseconds);

                return u;
            }
            catch (Exception exc)
            {
                return User.AnonymousUser;
            }
        }

        public override User LoadUser(string login)
        {
            var s = LoginClientCoder.DecodeLoginClient(login);
            login = s[0];
            string clientStr = s[1];
            int client;
            int.TryParse(clientStr, out client);
            try
            {
                DBUser user = null;
                using (ISession session = NHibernateHttpModule.OpenSession())
                {
                    user = (from u in session.Linq<DBUser>()
                            where u.Login == login && u.ClientID == client
                            select u).ToList().FirstOrDefault();

                    if (user == null || user.Active == false)
                        return User.AnonymousUser;

                    //foreach (DBRole r in user.Roles)
                    //{
                    //    int count = r.Permissions.Count;
                    //}

                }
                return LoadUser(user.ID);

                //return ConvertUser(user);
            }
            catch (Exception exc)
            {
                Logger.Fatal("Exception at logon", exc);
                return User.AnonymousUser;
            }
        }

        public User LoadUserNoCache(int id)
        {
            try
            {
                Stopwatch stopper = new Stopwatch();
                stopper.Start();
                DBUser user;
                using (ISession session = NHibernateHttpModule.OpenSession())
                {
                    user = session.Get<DBUser>(id);

                    if (user == null)
                        return User.AnonymousUser;

                    foreach (DBRole r in user.Roles)
                    {
                        int count = r.Permissions.Count;
                    }
                }


                User u = ConvertUser(user);

                stopper.Stop();
                Debug.WriteLine("User {0} loaded in {1}.", id, stopper.ElapsedMilliseconds);

                return u;
            }
            catch (Exception exc)
            {
                return User.AnonymousUser;
            }
        }

        public override User GetDefaultUser()
        {
            int defaultUserID = 0;


            int? def = GetDefaultUserFromHostName();
            if (def.HasValue)
                defaultUserID = def.Value;

            int cfgId = 0;
            if (int.TryParse(ConfigurationManager.AppSettings["DefaultUserID"], out cfgId))
                defaultUserID = cfgId;

            return LoadUser(defaultUserID);
        }

        private int? GetDefaultUserFromHostName()
        {
            string hostName = HttpContext.Current.Request.Url.Host.ToLower();

                using (ISession session = NHibernateHttpModule.OpenSession())
                {
                    Client client = (from c in session.Linq<Client>()
                                     where c.HostName == hostName
                                     select c).FirstOrDefault();

                    if (client != null)
                        return client.DefaultUserID;

                }
   
            return null;
        }

		private static IDictionary<string, string> GetLayoutSelections(string settings)
		{
			if (settings == null)
				return null;

			var parts = settings.Split(";".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            var layoutSelections = new Dictionary<string, string>();

            foreach (var part in parts)
            {                                   
                var key = part.Substring(0, part.IndexOf("="));
                var val = part.Substring(part.IndexOf("=") + 1);
                if (!layoutSelections.ContainsKey(key))
                {
							layoutSelections.Add(key, val);
                }
            }
            return layoutSelections;
		}

		public override User CheckCredentials(int auth_id, int user_id, Guid guid, string hash)
		{
			var auth_user = GetUser(auth_id) as BloodyUser;

			if (auth_user == null || auth_user == User.AnonymousUser)
			{
				throw new SecurityException(
					string.Format("Threre is no user with id {0}.", auth_id));
			}

			var user_user = GetUser(user_id) as BloodyUser;

			if (user_user == null || user_user == User.AnonymousUser)
			{
				throw new SecurityException(
					string.Format("Threre is no user with id {0}.", user_id));
			}

			if (!VerifyCredentials(
							string.Format("{0}{1}{2}",
							auth_id,
							User.AUTH_SEPARATOR,
							user_id),
					guid,
					hash,
					auth_user.Auth_PasswordHash))
			{
				if (auth_user.RfidUidHash == null || !VerifyCredentials(
						string.Format("{0}{1}{2}",
						auth_id,
						User.AUTH_SEPARATOR,
						user_id),
					guid,
					hash,
					auth_user.RfidUidHash))
				{
					return User.AnonymousUser;
				}
			}

			return user_user;

			//return VerifyCredentials(
			//            string.Format("{0}{1}{2}",
			//                auth_id,
			//                User.AUTH_SEPARATOR,
			//                user_id),
			//            guid, hash, auth_user.Auth_PasswordHash) ?
			//            user_user :
			//            User.AnonymousUser;
		}

		public override User CheckCredentials(string login, Guid guid, string hash, bool is_first_auth)
		{
			string auth_login = login;
			string user_login = login;

			if (!string.IsNullOrEmpty(login))
			{
				if (login.Contains(User.AUTH_SEPARATOR))
				{
					string[] s = login.Split(new string[1] { User.AUTH_SEPARATOR }, StringSplitOptions.RemoveEmptyEntries);
					auth_login = s[0];
					user_login = s[1];
				}
			}

			#region AUTH LOGIN
			var auth_user = GetUser(auth_login) as BloodyUser;

			if (auth_user == null || auth_user == User.AnonymousUser)
			{
				throw new SecurityException(
                    string.Format("There is no user with login {0}, or the user is deactivated", auth_login));
			}

			try
			{
				#region walidacja HASHa (podwojna)
				
				if (!VerifyCredentials(
							string.Format("{0}{1}{2}",
							auth_login,
							User.AUTH_SEPARATOR,
							user_login),
					guid,
					hash,
					auth_user.Auth_PasswordHash))
				{
					if (auth_user.RfidUidHash == null || !VerifyCredentials(
							string.Format("{0}{1}{2}",
							auth_login,
							User.AUTH_SEPARATOR,
							user_login),
						guid,
						hash,
						auth_user.RfidUidHash))
					{
						return User.AnonymousUser;
					}
				}

				#endregion

				#region walidacja auth usera
				User user_validated = ValidateUser(auth_user, is_first_auth);

				if (user_validated == null || !user_validated.Identity.IsAuthenticated)
				{
					return user_validated == null ? User.AnonymousUser : user_validated;
				}

				if (!user_validated.Identity.IsAuthenticated && user_validated.ID > 0)
				{
					return User.AnonymousUser;
				}
				#endregion

				return user_validated;
			}
			catch (Exception exc)
			{
                Logger.Fatal("CheckCredentials", exc);
			}
			#endregion

			return User.AnonymousUser;
		}
    }
}
