﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Model.Authentication
{
    public static class LoginClientCoder
    {
        public static string LOGIN_CLIENT_SEPARATOR = "^";
        
        public static string EncodeLoginClient(string login, string client)
        {
            return login + LOGIN_CLIENT_SEPARATOR + client;
        }

        public static string[] DecodeLoginClient(string code)
        {
            string[] res = new string[2];
            res[0] = code;
            res[1] = "";
            if (code.Contains(LOGIN_CLIENT_SEPARATOR))
            {
                string[] s = code.Split(new string[1] { LOGIN_CLIENT_SEPARATOR }, StringSplitOptions.RemoveEmptyEntries);
                res[0] = s[0];
                res[1] = s[1];
            }
            return res;
        }
    }
}
