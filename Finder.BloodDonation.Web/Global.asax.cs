﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using FinderFX.Web.Core.Configuration;
using FinderFX.Web.Core.Communication;
using FinderFX.Web.Core.Modules;
using Finder.BloodDonation.Model.Services;
using log4net;

namespace Finder.BloodDonation.Web
{
    public class Global : System.Web.HttpApplication
    {
        private static ILog logger = LogManager.GetLogger(typeof(Global));

        protected void Application_Start(object sender, EventArgs e)
        {
            GlobalConfiguration.Initialize(new BloodConfiguration());
            
            //GlobalConfiguration.Current.ModuleLoader.LoadWebModule("Domain");
            log4net.Config.XmlConfigurator.Configure();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}