﻿function attachManagedEvents() {
    var c = document.getElementById("comInterop");
    c.attachEvent("FireSomeEvent", handleTagDataReceived);
}

function handleTagDataReceived(result) {

//    alert("receiving incoming data...");

    var c = document.getElementById("silverlightControl");
    c.Content.Bridge.AcquireTagInfo(result);

}

function enableRfidTransmission() {
//    alert("listening...");

    comInterop.RunOtherThread();
}

function disableRfidTransmission() {

}
