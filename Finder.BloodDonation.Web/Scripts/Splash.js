﻿function onSourceDownloadProgressChanged(sender, eventArgs) {
    sender.findName("LoaderText").Text = Math.round(eventArgs.progress * 100) + "%";
    sender.findName("LoaderBar").Width = eventArgs.progress * 494;
}