﻿
function AttachIE11Event(obj, _strEventId, _functionCallback) {
        var nameFromToStringRegex = /^function\s?([^\s(]*)/;
        var paramsFromToStringRegex = /\(\)|\(.+\)/;
        var params = _functionCallback.toString().match(paramsFromToStringRegex)[0];
        var functionName = _functionCallback.name || _functionCallback.toString().match(nameFromToStringRegex)[1];
        var handler;
        try {
            handler = document.createElement("script");
            handler.setAttribute("for", obj.id);
        }
        catch(ex) {
            handler = document.createElement('<script for="' + obj.id + '">');
        }
        handler.event = _strEventId + params;
        handler.appendChild(document.createTextNode(functionName + params + ";"));
        document.body.appendChild(handler);
    };


function attachManagedEvents() {

	var c = document.getElementById("readerControl");

if(!!window.MSStream)	//czy IE11
{
				AttachIE11Event(c,"Started", handleStarted);
				AttachIE11Event(c,"TagRead", handleTagRead);
				AttachIE11Event(c,"TagWritten", handleTagWritten);
				AttachIE11Event(c,"TagLogin", handleTagLogin);
				AttachIE11Event(c,"StatusChanged", handleStatusChanged);
				AttachIE11Event(c,"ReportError", handleReportError);
}

else
if (c.attachEvent) {
				c.attachEvent("Started", handleStarted);
				c.attachEvent("TagRead", handleTagRead);
				c.attachEvent("TagWritten", handleTagWritten);
				c.attachEvent("TagLogin", handleTagLogin);
				c.attachEvent("StatusChanged", handleStatusChanged);
				c.attachEvent("ReportError", handleReportError);
				}
else if (c.addEventListener) {
				c.addEventListener("Started", handleStarted, false);
				c.addEventListener("TagRead", handleTagRead, false);
				c.addEventListener("TagWritten", handleTagWritten, false);
				c.addEventListener("TagLogin", handleTagLogin, false);
				c.addEventListener("StatusChanged", handleStatusChanged, false);
				c.addEventListener("ReportError", handleReportError, false);
				
				}

	//updateEventData("none", "initializing...");
}

function startButton_click() {
	readerControl.Start();
}

function enableRfidTransmission() {
	readerControl.Start();
}

function disableRfidTransmission() {
	readerControl.Stop();
}

function setTagData(args) {
	readerControl.Write(args);
}

function setUserTag(userId, userName) {
	readerControl.WriteUserTag(userId, userName);
}

function setIsbtTag(tagParams) {
	readerControl.WritePrepTag(tagParams);
}

function clearTag() {
    readerControl.ClearTag();
}

function reloadTags() {
    readerControl.ReloadTags();
}

function updateEventData(eventName, args) {
//	var d = document.getElementById("eventData");
	//	d.innerHTML = d.innerHTML + "<p>[" + eventName + "] " + args + "</p>";

	var result = eventName + ";" + args;

	var c = document.getElementById("silverlightControl");
	c.Content.Bridge.AcquireTagInfo(result);
}

function handleStarted(args) {
	updateEventData("Started", args);
}

function handleTagRead(args) {
	updateEventData("TagRead", args);
}

function handleTagWritten(args) {
	updateEventData("TagWritten", args);
}

function handleTagLogin(args) {
	updateEventData("TagLogin", args);
}

function handleStatusChanged(args) {
	//updateEventData("StatusChanged", args);
}

function handleReportError(args) {
	updateEventData("ReportError", args);
}

function readerControl_onunload() {
}
