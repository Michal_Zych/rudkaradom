﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FinderFX.Web.Core.Communication;
using Finder.BloodDonation.Model.Domain;
using FinderFX.Web.Core.Configuration;
using FinderFX.Web.Core.Modules;
using System.Web;
using System.Configuration;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Finder.BloodDonation.Model.Authentication;

namespace Finder.BloodDonation.Model.Services
{

	public class BloodConfiguration : GlobalConfiguration
	{
		public override FinderFX.Web.Core.Authentication.IAuthenticationManager GetAuthenticationManager()
		{
			return new BloodAuthenticationManager();
		}

		public override SessionManagerBase GetSessionManager()
		{
			return new ModelSessionManager();
		}

		public override FinderFX.Web.Core.Modules.IModuleMetaDataLoader GetModuleMetaDataLoader()
		{
			string path = "";
			if (HttpContext.Current == null)
				path = ConfigurationManager.AppSettings["GlobalConfigurationPath"];
			else
				path = HttpContext.Current.Server.MapPath(
				ConfigurationManager.AppSettings["GlobalConfigurationPath"]);

			return new XmlModuleMetaDataLoader(path);

		}

		public override bool EnableXapCache
		{
			get
			{
				try
				{
					bool cache = false;
					if (bool.TryParse(ConfigurationManager.AppSettings["EnableXapCache"], out cache))
					{
						return cache;
					}
				}
				catch
				{

				}

				return true;
			}
		}

		public override bool CacheEnabled
		{
			get
			{
				return true;
			}
		}
	}
}
