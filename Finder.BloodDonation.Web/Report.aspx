﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Report.aspx.cs" Inherits="Finder.BloodDonation.Web.Report" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Raport</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <table align="center" style="width: 100%; height: 100%">
        <tr>
            <td>
                <rsweb:ReportViewer ID="reportView" runat="server" Height="100%" Width="100%" ProcessingMode="Remote"
                    ShowParameterPrompts="false" ShowFindControls="false" ShowRefreshButton="true" AsyncRendering="false"
                    ShowToolBar="true" ShowExportControls="true" ShowPageNavigationControls="true" ShowPrintButton="true" ShowBackButton="false"
                    ShowZoomControl="true" >
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
        
    </form>
</body>
</html>
