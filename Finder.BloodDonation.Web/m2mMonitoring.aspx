﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="m2mMonitoring.aspx.cs" Inherits="Finder.BloodDonation.Web.m2mMonitoring" Debug="true"%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lokalizator m2mTeam</title>
    <link rel="RCKiK" type="image/x-icon" href="/favicon.ico" />
    <script src="/Scripts/Splash.js" type="text/javascript"></script>
    
    <%-- old reader interop--%>
    <%--<script src="/Scripts/ComInterop.js" type="text/javascript"></script>--%>
    <script src="/Scripts/ComInteropV2.js" type="text/javascript"></script>

    <style type="text/css">
    html, body {
        height: 100%;
        overflow: auto;
    }
    body {
        padding: 0;
        margin: 0;
    }
    #silverlightControlHost {
        height: 100%;
        text-align:center;
    }
    </style>
    <script type="text/javascript" src="Silverlight.js"></script>
    <script type="text/javascript">
        function onSilverlightError(sender, args) {
            var appSource = "";
            if (sender != null && sender != 0) {
              appSource = sender.getHost().Source;
            }
            
            var errorType = args.ErrorType;
            var iErrorCode = args.ErrorCode;

            if (errorType == "ImageError" || errorType == "MediaError") {
              return;
            }

            var errMsg = "Unhandled Error in Silverlight Application " +  appSource + "\n" ;

            errMsg += "Code: "+ iErrorCode + "    \n";
            errMsg += "Category: " + errorType + "       \n";
            errMsg += "Message: " + args.ErrorMessage + "     \n";

            if (errorType == "ParserError") {
                errMsg += "File: " + args.xamlFile + "     \n";
                errMsg += "Line: " + args.lineNumber + "     \n";
                errMsg += "Position: " + args.charPosition + "     \n";
            }
            else if (errorType == "RuntimeError") {           
                if (args.lineNumber != 0) {
                    errMsg += "Line: " + args.lineNumber + "     \n";
                    errMsg += "Position: " +  args.charPosition + "     \n";
                }
                errMsg += "MethodName: " + args.methodName + "     \n";
            }

            throw new Error(errMsg);
        }

        function onLoaded(sender, args) {
            //debug only
            //sender.getHost().settings.EnableFramerateCounter = true;
            //sender.getHost().settings.EnableRedrawRegions = true;
        }
    </script>
</head>
<body onload="attachManagedEvents();" style="overflow: hidden; scroll: no;">

    <form id="form1" runat="server" style="height:100%">
        
    <div id="silverlightControlHost">
        <object id="silverlightControl" data="data:application/x-silverlight-2," type="application/x-silverlight-2" width="100%" height="100%">
        <% 
            var source = @"ClientBin/Finder.BloodDonation.xap";
            string param;
            if (System.Diagnostics.Debugger.IsAttached)
                param = string.Format("<param name=\"source\" value=\"{0}?theme=Default\" />", source);
            else
            {
                var path = HttpContext.Current.Server.MapPath(string.Empty) + "\\" + source;
                var xapCreatedAt = System.IO.File.GetLastWriteTime(path);
                param = string.Format("<param name=\"source\" value=\"{0}?theme=Default&version={1}\" />",
                    source,
                    xapCreatedAt.ToString("yyyyMMddTHHmmssfff"));
            }
            Response.Write(param);

            string client = Request["client"];
            //if (client == null) client = "1";
            client = "1";
            param = string.Format("<param name=\"splashscreensource\" value=\"Splash{0}.xaml\"/>", client);
            Response.Write(param);

            param = SerializeConfiguration();
            Response.Write(param);

            //param = string.Format("<param name=\"initParams\" value=\"client={0}\"/>", client);
            //Response.Write(param);
            
         %>    

           <%--<param name="source" value="ClientBin/Finder.BloodDonation.xap?theme=Default&skin=--%>
          <param name="onError" value="onSilverlightError" />
          <param name="background" value="white" />
          <param name="minRuntimeVersion" value="4.0.50401.0" />
          <param name="autoUpgrade" value="true" />
          <%-- R.Sz. <param name="splashscreensource" value="Splash.xaml"/>--%>
          <param name="onSourceDownloadProgressChanged" value="onSourceDownloadProgressChanged" />
          <param name="onload" value="onLoaded"/>
          <param name="windowless" value="false"/>
          <param name="maxframerate" value="15"/>

          <a href="http://go.microsoft.com/fwlink/?LinkID=149156&v=4.0.50401.0" style="text-decoration:none">
              <img src="http://go.microsoft.com/fwlink/?LinkId=161376" alt="Get Microsoft Silverlight" style="border-style:none"/>
          </a>
        </object><iframe id="_sl_historyFrame" style="visibility:hidden;height:0px;width:0px;border:0px"></iframe>
    </div>
    </form>

    <%--Old rfid reader (CP02)--%>
    <%--<object id="comInterop" name="comInterop" classid="CLSID:6CDDBE2A-B518-473D-A074-AAA0035A4393" />--%>

    <%--<object id="readerControl" name="readerControl" classid="CLSID:AE1A5E9C-0A67-4723-8D80-A148F087AB44" />--%>
   <object id="readerControl" name="readerControl" classid="CLSID:74CA64A5-7674-40FD-BECE-1D965A2EFE97" />

</body>
</html>