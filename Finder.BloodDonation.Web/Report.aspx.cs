﻿using System;
using System.Collections.Generic;
using System.Linq;
using NHibernate.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using NHibernate;
using FinderFX.Web.Core.Communication;
using Finder.BloodDonation.Model.Domain;
using System.Threading;
using Microsoft.Reporting.WebForms;

namespace Finder.BloodDonation.Web
{
    public partial class Report : System.Web.UI.Page
    {
        private IList<ReportParameter> _parameters;

        private string ReportType
        {
            get
            {
                if (string.IsNullOrEmpty(Request.QueryString["Type"]))
                {
                    return "EXCEL";
                }

                return Request.QueryString["Type"];
            }
        }

        private int UserReportID
        {
            get
            {
                int r = 0;
                if (string.IsNullOrEmpty(Request.QueryString["Id"]) || !int.TryParse(Request.QueryString["Id"], out r))
                {
                    throw new ArgumentException("Błędny identyfikator raportu");
                }

                return r;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Finder.BloodDonation.Model.Domain.Report report = null;

                using (ISession x = NHibernateHttpModule.OpenSession())
                {
                    UserReport ur = (from r in x.Linq<UserReport>()
                                     where r.ID == UserReportID
                                     select r).FirstOrDefault();

                    if (ur == null)
                    {
                        throw new ArgumentNullException("Brak ustawień raportu.");
                    }

                    report = ur.Report;
                    string rdl = report.RDL;
                }

                reportView.ServerReport.ReportServerUrl = new Uri(ConfigurationManager.AppSettings["ReportServerUri"]);
                reportView.ServerReport.ReportPath =
                        string.Format("/{0}/{1}", ConfigurationManager.AppSettings["ReportServerFolder"], report.RDL);

                InitializeReportsParameters();

                switch (ReportType.ToLower())
                {
                    case "html":
                        ShowReportHTML();
                        break;

                    case "excel":
                        ExportReport(report, "application/vnd.ms-excel", "xls");
                        break;

                    case "pdf":
                        ExportReport(report, "application/x-pdf", "pdf");
                        break;
                }
            }
        }

        private void InitializeReportsParameters()
        {
            try
            {
                _parameters = new List<ReportParameter>();
                _parameters.Add(new ReportParameter("UserReportID", UserReportID.ToString()));
                reportView.ServerReport.SetParameters(_parameters);
            }
            catch (Exception exc)
            {
            }
        }

        private void ShowReportHTML()
        {
            try
            {
                // codeproject tip
                while (reportView.ServerReport.IsDrillthroughReport)
                {
                    reportView.PerformBack();
                }

                reportView.ServerReport.Refresh();
            }
            catch (Exception exc)
            {
                //Debug.Fail(exc.Message, msg);
            }
        }

        private void ExportReport(Finder.BloodDonation.Model.Domain.Report report, string contentType, string fileExtension)
        {
            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;
            string deviceInfo;

            try
            {
                while (reportView.ServerReport.IsDrillthroughReport)
                {
                    reportView.PerformBack();
                }

                deviceInfo =
                            "<DeviceInfo>" +
                                "<SimplePageHeaders>true</SimplePageHeaders>" +
                            "</DeviceInfo>";

                Response.BufferOutput = true;

                byte[] bytes = reportView.ServerReport.Render(
                           ReportType.ToLower(),
                           deviceInfo,
                           out mimeType,
                           out encoding,
                           out extension,
                           out streamids,
                           out warnings);

                Response.ContentType = contentType;
                Response.OutputStream.Write(bytes, 0, bytes.Length);
                string httpHeader = String.Format("attachment; filename=\"{0}.{1}\"",
                    report.Name, fileExtension);
                Response.AddHeader("Content-Disposition", httpHeader);

                Response.Flush();

                Response.Close();
            }
            catch (ThreadAbortException)
            {
                //Igorujemy. Wyjątek rzucany jest przez Response.End() i nie powoduje (raczej) niczego złego,
                //jest tylko sygnałem dla ASP.NET Runtime, że wykonywanie strony zostało przerwane.
            }
            catch (Exception exc)
            {
            }
        }
    }
}