﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using NHibernate;
using FinderFX.Web.Core.Communication;
using NHibernate.Linq;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using System.Configuration;
using Finder.BloodDonation.Model.Domain;
using NHibernate.Transform;

namespace Finder.BloodDonation.Web
{
	public class ModelConfiguration : SessionManagerBase
	{
		public override void SetMappings(FluentNHibernate.Cfg.MappingConfiguration m)
		{
			//base.SetMappings(m);

			m.FluentMappings.AddFromAssemblyOf<Unit>();
		}

		public override FluentConfiguration GetConfigurationWithoutMappings()
		{
			FluentConfiguration cfg = Fluently.Configure().
			   Database(MsSqlConfiguration.MsSql2005.ConnectionString(
			   c => c.FromConnectionStringWithKey(ConfigurationManager.AppSettings["NHibernateConnection"])).
			   DefaultSchema("dbo"));

			return cfg;
		}

	}

	public partial class Test : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			try
			{
				IList<Unit> units;

				using (ISession session = NHibernateHttpModule.OpenSession())
				{
					//IList<Unit> units = (from u in session.Linq<Unit>()
					//                     select u).ToList();

					//units = session.CreateQuery("select u from Unit u join fetch u.Children where u.ID = :p")
					//    .SetInt32("p", 1)
					//    .SetResultTransformer(new DistinctRootEntityResultTransformer())
					//    .List<Unit>();

					var sql = "with Hierachy(ID, Name, ParentUnitID, Level)" +
							 " as" +
							 " (" +
							 "   select ID, Name, ParentUnitID, 0 as Level" +
							 "   from Units e" +
							 "   where e.ID = :id" +
							 "  union all" +
							 "   select e.ID, e.Name, e.ParentUnitID, eh.Level + 1" +
							 "   from Units e" +
							 "   inner join Hierachy eh" +
							 "      on e.ParentUnitID = eh.ID" +
							 " )" +
							 " select ID, Name, ParentUnitID" +
							 " from Hierachy";
					units = session.CreateSQLQuery(sql)
						.AddEntity(typeof(Unit))
						.SetInt32("id", 1)
						.SetResultTransformer(new DistinctRootEntityResultTransformer())
						.List<Unit>();


					int i = 234;

					
				}

				Unit root = GetTree(1, units);


				int a = 234;

			}
			catch (Exception exc)
			{

			}
		}

		private Unit GetTree(int unitID, IList<Unit> loadedUnits)
		{
			Unit unit = loadedUnits.FirstOrDefault(x => x.ID == unitID);

			unit.Children = GetChildUnits(unit, loadedUnits);


			return unit;
		}

		private IList<Unit> GetChildUnits(Unit parentUnit, IList<Unit> loadedUnits)
		{
			IList<Unit> children = loadedUnits.Where(x => x.ParentUnitID == parentUnit.ID).ToList();

			if (children != null && children.Count > 0)
			{
				foreach (Unit u in children)
				{
					u.Children = GetChildUnits(u, loadedUnits);
				}
			}

			return children;
		}
	}
}