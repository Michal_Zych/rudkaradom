﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

namespace Finder.BloodDonation.Web
{
    public class m2mMonitoring : System.Web.UI.Page
    {

        public string SerializeConfiguration()
        {
            string client = "1";

            NameValueCollection appSettings = ConfigurationManager.AppSettings;

            StringBuilder SB = new StringBuilder();
            SB.Append("<param name=\"initParams\" value=\"");

            int SettingCount = appSettings.Count;
            for (int Idex = 0; Idex < SettingCount; Idex++)
            {
                SB.Append(appSettings.GetKey(Idex));
                SB.Append("=");
                SB.Append(appSettings[Idex]);
                SB.Append(",");
            }
            SB.Append("client");
            SB.Append("=");
            SB.Append(client);
            SB.Append("\" />");

            return SB.ToString();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
    }
}