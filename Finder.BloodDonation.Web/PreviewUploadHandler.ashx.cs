﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Diagnostics;
using System.Web.Hosting;
using log4net;
using NHibernate;
using Finder.BloodDonation.Model.Domain;
using FinderFX.Web.Core.Communication;
using Finder.BloodDonation.Model;

namespace Finder.BloodDonation.Web
{
    public class PreviewUploadHandler : IHttpHandler
    {
        private const string UploadPreviewProc = "exec dbo.unt_UploadPreviewPicture :name, :path, :clientId";


        private static ILog logger = LogManager.GetLogger(typeof(PreviewUploadHandler));
        private HttpContext _httpContext;
        private string _tempExtension = "_temp";
        private string _fileName;
        private string _parameters;
        private bool _lastChunk;
        private bool _firstChunk;
        private long _startByte;

        StreamWriter _debugFileStreamWriter;
        TextWriterTraceListener _debugListener;

        public void ProcessRequest(HttpContext context)
        {
            _httpContext = context;

            if (context.Request.InputStream.Length == 0)
                throw new ArgumentException("No file input");

            try
            {
                GetQueryStringParameters();


                string tempFileName = _fileName + _tempExtension;

                string folder = System.Configuration.ConfigurationManager.AppSettings["UploadFolder"]
                    +  "\\" + ConfigurationsKeys.CLIENT_PARAM_NAME + _parameters;

                string full_temp_file_path = Path.Combine(folder, ConfigurationsKeys.PREVIEWS_FOLDER, tempFileName);
                string full_file_path = Path.Combine(folder, ConfigurationsKeys.PREVIEWS_FOLDER, _fileName);

                if (_firstChunk)
                {
                    if (File.Exists(full_file_path))
                    {
                        File.Delete(full_file_path);
                       // throw new Exception("Plik już istnieje.");
                    }

                    if (File.Exists(full_temp_file_path))
                    {
                        File.Delete(full_temp_file_path);
                    }
                }

                using (FileStream fs = File.Open(full_temp_file_path, FileMode.Append))
                {
                    SaveFile(context.Request.InputStream, fs);
                    fs.Close();
                }

                int clientId ;
                int.TryParse(_parameters, out clientId);
                    
                if (_lastChunk)
                {
                    using (ISession x = NHibernateHttpModule.OpenSession())
                    {
                        var path = "\\" + ConfigurationsKeys.CLIENT_PARAM_NAME + _parameters +
                                   "\\" + ConfigurationsKeys.PREVIEWS_FOLDER +
                                   "\\" + _fileName;
                        var sql = x.CreateSQLQuery(UploadPreviewProc)
                            .SetString("name", _fileName)
                            .SetString("path", path)
                            .SetInt32("clientId", clientId)
                            .ExecuteUpdate();
                           
                        /*using (ITransaction t = x.BeginTransaction())
                        {
                            EditPreview ds = new EditPreview()
                            {
                                Name = _fileName,
                                Path = path,
                                ClientID = clientId
                            };
                            x.SaveOrUpdate(ds);
                            //x.Save(ds);

                            t.Commit();
                        }*/
                    }

                    File.Move(full_temp_file_path, full_file_path);
                }
            }
            catch (Exception e)
            {
                logger.Error(e);
                throw;
            }
            finally
            {
            }

        }

        private void GetQueryStringParameters()
        {
            _fileName = _httpContext.Request.QueryString["file"];
            _parameters = _httpContext.Request.QueryString["param"];
            _lastChunk = string.IsNullOrEmpty(_httpContext.Request.QueryString["last"]) ? true : bool.Parse(_httpContext.Request.QueryString["last"]);
            _firstChunk = string.IsNullOrEmpty(_httpContext.Request.QueryString["first"]) ? true : bool.Parse(_httpContext.Request.QueryString["first"]);
            _startByte = string.IsNullOrEmpty(_httpContext.Request.QueryString["offset"]) ? 0 : long.Parse(_httpContext.Request.QueryString["offset"]); ;
        }

        private void SaveFile(Stream stream, FileStream fs)
        {
            byte[] buffer = new byte[4096];
            int bytesRead;
            while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) != 0)
            {
                fs.Write(buffer, 0, bytesRead);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}