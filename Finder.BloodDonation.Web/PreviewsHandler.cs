﻿using System;
using System.Linq;
using System.Web;
using NHibernate;
using NHibernate.Linq;
using FinderFX.Web.Core.Communication;
using Finder.BloodDonation.Model.Domain;
using NHibernate.Transform;
using System.IO;
using System.Web.Hosting;

namespace Finder.BloodDonation.Web
{
    public class PreviewsHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            int id_preview = int.Parse(context.Request.Url.Segments[context.Request.Url.Segments.Length - 1].Split('.')[0]);
            Preview ur = null;
            using (ISession x = NHibernateHttpModule.OpenSession())
            {
                var sql = @"select p.ID, p.Path, 1 as UnitID, 1 as UnitType, 1 as PictureID
                            from dbo.Pictures p
                            where p.ID = :id";

                ur = x.CreateSQLQuery(sql)
                    .AddEntity(typeof(Preview))
                    .SetInt32("id", id_preview)
                    .SetResultTransformer(new DistinctRootEntityResultTransformer())
                    .List<Preview>().FirstOrDefault();

                if (ur == null)
                {
                    throw new ArgumentNullException("Brak podglądu w bazie danych");
                }
            }

            context.Response.ContentType = "image/jpg";
            if (File.Exists(System.Configuration.ConfigurationManager.AppSettings["UploadFolder"] + ur.Path))
            {
                context.Response.WriteFile(System.Configuration.ConfigurationManager.AppSettings["UploadFolder"] + ur.Path);
            }
            else
            {
                context.Response.WriteFile(HostingEnvironment.MapPath("~/ClientBin") + "/" + System.Configuration.ConfigurationManager.AppSettings["FileNotExistPicture"]);
            }

        }
    }
}
