﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Reporting.WebForms;

namespace Finder.BloodDonation.Web
{
    public class ReportViewerMessages : IReportViewerMessages
    {
        public ReportViewerMessages()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region IReportViewerMessages Members

        public string BackButtonToolTip
        {
            get { return null; }
        }

        public string ChangeCredentialsText
        {
            get { return null; }
        }

        public string ChangeCredentialsToolTip
        {
            get { return null; }
        }

        public string CurrentPageTextBoxToolTip
        {
            get { return null; }
        }

        public string DocumentMap
        {
            get { return null; }
        }

        public string DocumentMapButtonToolTip
        {
            get { return null; }
        }

        public string ExportButtonText
        {
            get { return null; }
        }

        public string ExportButtonToolTip
        {
            get { return null; }
        }

        public string ExportFormatsToolTip
        {
            get { return null; }
        }

        public string FalseValueText
        {
            get { return null; }
        }

        public string FindButtonText
        {
            get { return null; }
        }

        public string FindButtonToolTip
        {
            get { return null; }
        }

        public string FindNextButtonText
        {
            get { return null; }
        }

        public string FindNextButtonToolTip
        {
            get { return null; }
        }

        public string FirstPageButtonToolTip
        {
            get { return null; }
        }

        public string InvalidPageNumber
        {
            get { return null; }
        }

        public string LastPageButtonToolTip
        {
            get { return null; }
        }

        public string NextPageButtonToolTip
        {
            get { return null; }
        }

        public string NoMoreMatches
        {
            get { return null; }
        }

        public string NullCheckBoxText
        {
            get { return null; }
        }

        public string NullValueText
        {
            get { return null; }
        }

        public string PageOf
        {
            get { return null; }
        }

        public string ParameterAreaButtonToolTip
        {
            get { return null; }
        }

        public string PasswordPrompt
        {
            get { return null; }
        }

        public string PreviousPageButtonToolTip
        {
            get { return null; }
        }

        public string PrintButtonToolTip
        {
            get { return null; }
        }

        public string ProgressText
        {
            get { return "Trwa generowanie raportu"; }
        }

        public string RefreshButtonToolTip
        {
            get { return null; }
        }

        public string SearchTextBoxToolTip
        {
            get { return null; }
        }

        public string SelectAValue
        {
            get { return null; }
        }

        public string SelectAll
        {
            get { return null; }
        }

        public string SelectFormat
        {
            get { return null; }
        }

        public string TextNotFound
        {
            get { return null; }
        }

        public string TodayIs
        {
            get { return null; }
        }

        public string TrueValueText
        {
            get { return null; }
        }

        public string UserNamePrompt
        {
            get { return null; }
        }

        public string ViewReportButtonText
        {
            get { return null; }
        }

        public string ZoomControlToolTip
        {
            get { return null; }
        }

        public string ZoomToPageWidth
        {
            get { return null; }
        }

        public string ZoomToWholePage
        {
            get { return null; }
        }

        #endregion
    }
}