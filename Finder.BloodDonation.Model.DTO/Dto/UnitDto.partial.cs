﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using FinderFX.SL.Core.MVVM;

namespace Finder.BloodDonation.Model.Dto
{
    public partial class UnitDTO 
    {
        public UnitDTO()
        {
            this._isSelected = false;
        }


        private UnitDTO _parentField;

        public virtual UnitDTO Parent
        {
            get { return this._parentField; }
            set
            {
                if ((object.ReferenceEquals(this._parentField, value) != true))
                {
                    Validator.ValidateProperty(value, new ValidationContext(this, null, null) { MemberName = "Parent" });
                    this._parentField = value;
                    this.RaisePropertyChanged("Parent");
                }
            }
        }

        private bool? _isSelected;

        public virtual bool? IsSelected
        {
            get { return this._isSelected; }
            set
            {
                if (this._reentrancyCheck)
                    return;
                this._reentrancyCheck = true;
                this._isSelected = value;
                UpdateCheckState();
                this.RaisePropertyChanged("IsSelected");
                this._reentrancyCheck = false;
            }
        }

        public bool HasChildren()
        {
            return this.Children != null && this.Children.Count > 0;
        }

        public static UnitDTO FindUnitDto(UnitDTO root, int unitId)
        {
            if (root.ID == unitId)
                return root;
            
            var unit = root.Children.Where(u => u.ID == unitId).Select(u => u).FirstOrDefault();
            if (unit != null)
                return unit;
            else
            {
                foreach (var userDto in root.Children)
                {
                    if (userDto.HasChildren())
                    {
                        return FindUnitDto(userDto, unitId);
                    }
                }
            }
            return null;
        }

        private bool HasChildFirstLevel(int unitId)
        {
            if (this.HasChildren())
            {
                return this.Children.Any(u => u.ID == unitId);
            }
            return false;
        }

        public void SetParrentsForChildren()
        {
            var crtUnit = this;
            foreach (var child in this.Children)
            {
                child.Parent = crtUnit;
                child.SetParrentsForChildren();
            }
        }

        public bool AnyParrentSelected()
        {
            if (this.Parent == null)
                return false;
            if (this.Parent.IsSelected.HasValue && this.Parent.IsSelected.Value)
                return true;
            else
                this.Parent.AnyParrentSelected();
            return false;
        }

        public void SelectUnits(List<int> selectedUnitIds)
        {
            if (selectedUnitIds == null) throw new ArgumentNullException();
            _selectedUnitIds = selectedUnitIds;
            SelectUnit(this);
            _selectedUnitIds = null;
            _selectedUnits = 0;
        }

        private static List<int> _selectedUnitIds;
        private static int _selectedUnits = 0;
        private bool _reentrancyCheck;

        private static void SelectUnit(UnitDTO unit)
        {
            if (_selectedUnitIds.Count == _selectedUnits)
                return;
            unit.IsSelected = null;
            if (_selectedUnitIds.Any(u => u == unit.ID))
            {
                unit.IsSelected = true;
                _selectedUnits++;
            }
            else
                unit.IsSelected = false;

            foreach (var child in unit.Children)
                SelectUnit(child);
        }

        private bool? DetermineCheckState()
        {
            bool allChildrenChecked = this.Children.Count(x => x.IsSelected == true) == this.Children.Count;
            if (allChildrenChecked)
            {
                return true;
            }

            bool allChildrenUnchecked = this.Children.Count(x => x.IsSelected == false) == this.Children.Count;
            if (allChildrenUnchecked)
            {
                return false;
            }

            return null;
        }

        private void UpdateChildrenCheckState()
        {
            foreach (var item in this.Children)
            {
                if (this.IsSelected != null)
                {
                    item.IsSelected = this.IsSelected;
                }
            }
        }

        private void UpdateCheckState()
        {
            // update all children:
            if (this.Children != null && this.Children.Count != 0)
            {
                this.UpdateChildrenCheckState();
            }
            //update parent item
            if (this.Parent != null)
            {
                bool? parentIsChecked = this.Parent.DetermineCheckState();
                this.Parent.IsSelected = parentIsChecked;
            }
        }

        public void SetAllIsSelectedProperty(bool? value, bool refreshCheckState = false)
        {
            if (refreshCheckState)
                this.IsSelected = true;
            else
                this._isSelected = value;    
            
            foreach (var child in this.Children)
            {
                child.SetAllIsSelectedProperty(value);
            }
        }

        public UnitDTO FindUnit(int unitId)
        {
            if (this.ID == unitId)
                return this;

            if (Children != null)
            {
                for (int i = 0; i < Children.Count; i++)
                {
                    UnitDTO u = Children[i].FindUnit(unitId);
                    if (u != null)
                        return u;
                }
            }

            return null;
        }

        public UnitDTO NextSiblings()
        {
            var par = this.Parent;
            if (par == null) return null;

            UnitDTO toReturn = null;
            for (int i = 0; i < par.Children.Count; i++)
            {
                if (par.Children[i].ID == this.ID)
                {
                    if (i + 1 < par.Children.Count)
                    {
                        toReturn = par.Children[i + 1];
                        break;
                    }
                }
            }
            return toReturn;
        }


        public string Path
        {
            get 
            { 
                return GetPath("/");
            }
        }


        public string GetPath(string separator)
        {
            List<string> tmp = new List<string>();
            tmp.Add(this.Name);
            UnitDTO parent = this.Parent;
            
            if (parent == null)
                return this.Name;

            tmp.Add(parent.Name);
            while ((parent = parent.Parent) != null)
            {
                tmp.Add(parent.Name);
            }
            tmp.Reverse();
            return string.Join(separator, tmp);
        }

        public void ForEachDescendant(Action<UnitDTO> callback)
        {
            if (this.Children != null)
            {
                if (this.Children != null)
                    foreach (var child in this.Children)
                    {
                        child.ForEachDescendant(callback);
                        callback(child);
                    }
            }
        }

        public void ForEachChild(Action<UnitDTO> callback)
        {
            if (this.Children != null)
                foreach (var child in this.Children)
                    callback(child);
        }
    }
}
