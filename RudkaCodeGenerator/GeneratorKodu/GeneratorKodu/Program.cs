﻿using GeneratorKodu.Parsers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeneratorKodu
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("Kolumny widoku będą generowane tylko dla niewirtualnych właściwości");
            Console.WriteLine(" słowo Icon w komentarzu utworzy kolumnę na bazie tabeli z możliwością wyświetlania obrazka");
            Console.WriteLine(" dla pół logicznych komentarz zawira opisy wartości logicznych oddzielone średnikami:");
            Console.WriteLine("     brakWartości;false;true");
            Console.WriteLine();
            Console.WriteLine("Enter aby wybrać DTO");
            Console.ReadLine();


            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Plik Dto";
            if(ofd.ShowDialog() == DialogResult.OK)
            {
                var dtoFile = File.ReadAllLines(ofd.FileName);
                var dtoList = new List<string>(dtoFile);

                var dto = new Dto(dtoList);

                Console.WriteLine("Podaj nazwę zakładki(folder/przedrostek klas):");
                dto.TabName = Console.ReadLine();

                Console.WriteLine(dto);

                var contextMenu = new GeneratorKodu.Parsers.ContextMenu(dto);
                SaveFile(contextMenu.Name, "cs", contextMenu.Content);

                var dataTable = new DataTable(dto);
                SaveFile(dataTable.Name, "cs", dataTable.Content);
                
                //do poprawy
                var detailsListView = new DetailsListView(dto);
                SaveFile(detailsListView.Name, "xaml", detailsListView.Content);

                var detailsListViewCodeBehind = new DetailsListViewCodeBehind(dto);
                SaveFile(detailsListViewCodeBehind.Name, "cs", detailsListViewCodeBehind.Content);
                
                var detailsListViewModel = new DetailsListViewModel(dto);
                SaveFile(detailsListViewModel.Name, "cs", detailsListViewModel.Content);

                var filter = new Filter(dto);
                SaveFile(filter.Name, "cs", filter.Content);

                var itemViewModel = new ItemViewModel(dto);
                SaveFile(itemViewModel.Name, "cs", itemViewModel.Content);

                var remotePagedDataSourde = new RemotePagedDataSource(dto);
                SaveFile(remotePagedDataSourde.Name, "cs", remotePagedDataSourde.Content);

                var view = new Parsers.View(dto);
                SaveFile(view.Name, "xaml", view.Content);

                var viewCodeBehind = new ViewCodeBehind(dto);
                SaveFile(viewCodeBehind.Name, "cs", viewCodeBehind.Content);

                var viewModel = new ViewModel(dto);
                SaveFile(viewModel.Name, "cs", viewModel.Content);



                /*
                    if (dto.PropertiesCount("bool") > 0)
                    {
                        var boolConverter = new BoolValueConverter(dto);
                        SaveFile(boolConverter.Name, "cs", boolConverter.Content);

                    }

                    var detailsListView = new DetailsListView(dto);
                    SaveFile(detailsListView.Name, "xaml", detailsListView.Content);

                    var detailsListViewCodeBehind = new DetailsListViewCodeBehind(dto);
                    SaveFile(detailsListViewCodeBehind.Name, ".cs", detailsListViewCodeBehind.Content);


                    var detailsListViewModel = new DetailsListViewModel(dto);
                    SaveFile(detailsListViewModel.Name, ".cs", detailsListViewModel.Content);

                    var itemViewModel = new ItemViewModel(dto);
                    SaveFile(itemViewModel.Name, ".cs", itemViewModel.Content);

                    var contextMenu = new GeneratorKodu.Parsers.ContextMenu(dto);
                    SaveFile(contextMenu.Name, ".cs", contextMenu.Content);

                    var filter = new Filter(dto);
                    SaveFile(filter.Name, ".cs", filter.Content);

                    var dataSource = new DataSource(dto);
                    SaveFile(dataSource.Name, ".cs", dataSource.Content);

                    var db = new Db(dto);
                    SaveFile(db.Name, ".cs", db.Content);

                    var pagedDataSourde = new PagedDataSource(dto);
                    SaveFile(pagedDataSourde.Name, ".cs", pagedDataSourde.Content);

                    Console.Write(dto);
                */
            }

            Console.ReadLine();
        }

        private static void SaveFile(string name, string ext, string text)
        {
            var path = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "\\Output\\" + name + "." + ext;
            new FileInfo(path).Directory.Create();
            File.WriteAllText(path, text);
        }


        
    }
}
