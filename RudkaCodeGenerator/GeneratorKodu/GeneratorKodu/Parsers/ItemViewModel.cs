﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorKodu.Parsers
{
    class ItemViewModel: BaseParser
    {
        public ItemViewModel(Dto dto)
            : base(dto)
        { }

        public override string ParserName
        {
            get { return "ItemViewModel"; }
        }


    }
}
