﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorKodu.Parsers
{
    class ViewCodeBehind : BaseParser
    {
        public ViewCodeBehind(Dto dto)
            : base(dto)
        { }
        public override string ParserName
        {
            get { return "View.xaml"; }
        }



    }
}
