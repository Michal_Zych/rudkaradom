﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorKodu.Parsers
{
    public class DetailsListView : BaseParser
    {
        public DetailsListView(Dto dto)
            : base(dto)
        { }

        public override string ParserName
        {
            get { return "DetailsListView"; }
        }

        public override IDictionary<string, Func<string>> Parsers
        {
            get
            {
                return new Dictionary<string, Func<string>>()
                {
                    {"FilterColumns", GenerateFilterColumns},
                    {"FilterControls", GenerateFilterControls},
                    {"FilterSplitters", GenerateFilterSplitters},
                    {"HeadersColumns", GenerateHederColumns},
                    {"HeaderButtons", GenerateHeaderButtons},
                    {"ListBoxColumns", GenerateListBoxColumns},
                    {"ListTemplate", GenerateListTemplate}
                };
            }
        }

        private string GenerateFilterColumns()
        {
            var sb = new StringBuilder();

            for (int i = 0; i <= dto.PropertiesCount(); i++)
            {
                sb.AppendFormat("<ColumnDefinition Width=\"{{Binding DataTable.ColumnWidths[{0}], Mode=TwoWay}}\" />", i).AppendLine();
            }

            return sb.ToString();
        }

        private string GenerateFilterControls()
        {
            var sb = new StringBuilder();

            for (int i = 0; i < dto.PropertiesCount(); i++)
            {
                var item = dto.Columns[i];

                if (item.Type.StartsWith("int"))
                {
                    AppendIntFilterControl(sb, i, item.Name);
                }
                else if(item.Type.StartsWith("short"))
                {
                    AppendIntFilterControl(sb, i, item.Name);
                }
                else if (item.Type.StartsWith("long"))
                {
                    AppendIntFilterControl(sb, i, item.Name);
                }
                else if (item.Type.StartsWith("string"))
                {
                    AppendStringFilterControl(sb, i, item.Name);
                }
                else if (item.Type.StartsWith("DateTime"))
                {
                    AppendDateTimeFilterCOntrol(sb, i, item.Name);
                }
                else if (item.Type.StartsWith("bool"))
                {
                    AppendBoolFilterColntrol(sb, i, item.Name);
                }
                else
                {
                    throw new Exception("Nieobsługiwany typ właściwości w dot: " + item.Type);
                }
            }

            return sb.ToString();
        }

        private string GenerateFilterSplitters()
        {
            var sb = new StringBuilder();
            for (int i = 1; i <= dto.PropertiesCount(); i++)
            {
                sb.AppendFormat("<sdk:GridSplitter Grid.Column=\"{0}\" />", i).AppendLine();
            }
            return sb.ToString();
        }

        private string GenerateHederColumns()
        {
            var sb = new StringBuilder();

            for (int i = 0; i <= dto.PropertiesCount(); i++)
            {
                sb.AppendFormat("<ColumnDefinition Width=\"{{Binding  DataTable.ColumnWidths[{0}], Mode=TwoWay}}\" />", i).AppendLine();
            }

            return sb.ToString();
        }

        private string GenerateHeaderButtons()
        {
            var sb = new StringBuilder();

            for (int i = 0; i < dto.PropertiesCount(); i++)
            {
                var name = dto.Columns[i].Name;
                sb.AppendLine()
                    .AppendFormat("<Button Grid.Column=\"{{Binding DataTable.Columns, Converter={{StaticResource getColumnNumber}}, ConverterParameter={0}}}\" Command=\"{{Binding HeaderClicked}}\" CommandParameter=\"{0}\" >", name).AppendLine()
                    .AppendLine("<StackPanel Orientation=\"Horizontal\" Background=\"Transparent\">")
                    .AppendFormat("<Polygon Points=\"3, 0 6, 6 0, 6 3, 0\" Visibility=\"{{Binding DataTable.SortOrder, Converter={{StaticResource sortIndicatorConverter}}, ConverterParameter=+{0} }}\"/>", name).AppendLine()
                    .AppendFormat("<Polygon Points=\"3, 6 6, 0 0, 0 3, 6\" Visibility=\"{{Binding DataTable.SortOrder, Converter={{StaticResource sortIndicatorConverter}}, ConverterParameter=-{0} }}\"/>", name).AppendLine()
                    .AppendFormat("<TextBlock Text=\"{{Binding DataTable, Converter={{StaticResource getDataTableColumnName}}, ConverterParameter={0} }}\" />", name).AppendLine()
                    .AppendLine("</StackPanel>")
                    .AppendLine("</Button>")
                    .AppendFormat("<sdk:GridSplitter Grid.Column=\"{{Binding DataTable.Columns, Converter={{StaticResource getColumnNumber}}, ConverterParameter={0}}}\"", name).AppendLine()
                    .AppendFormat("IsEnabled=\"{{Binding DataTable.Columns, Converter={{StaticResource getColumnCanBeResized}}, ConverterParameter={0}}}\" />", name).AppendLine();
            }

            return sb.AppendLine().ToString();
        }


      
        private string GenerateListBoxColumns()
        {
            var sb = new StringBuilder();

            for (int i = 0; i <= dto.PropertiesCount(); i++)
            {
                sb.AppendFormat("<ColumnDefinition Width=\"{{Binding DataTable.ColumnWidths[{0}], Mode=TwoWay}}\" />", i).AppendLine();
            }

            return sb.ToString();
        }

        private string GenerateListTemplate()
        {
            var sb = new StringBuilder();

            for (int i = 0; i < dto.PropertiesCount(); i++)
            {
                string converter = "";

                if (dto.Columns[i].Type.StartsWith("DateTime"))
                {
                    converter = ", Converter={StaticResource nullableDateConverter}, ConverterParameter=dateTime";
                }
                else if (dto.Columns[i].Type.StartsWith("bool"))
                {
                    converter = String.Format(", Converter={{StaticResource bitValueConverter}}, ConverterParameter='{0}' ", dto.Columns[i].Comment);
                }
                if (dto.Columns[i].ShowIcon)
                {
                    AppendDataTableCell_IconTable(sb, dto.Columns[i].Name, converter);
                }
                else
                {
                    AppendDataTableCell_TextBlock(sb, dto.Columns[i].Name, converter);
                }
                sb.AppendLine();
            }
            return sb.ToString();
        }

        private void AppendDataTableCell_IconTable(StringBuilder sb, string name, string converter)
        {
            sb.AppendFormat("<Grid Grid.Column=\"{{Binding DataTable.Columns, Converter={{StaticResource getColumnNumber}}, ConverterParameter={0}}}\"  >", name).AppendLine()
                .Append("<Grid.ColumnDefinitions>").AppendLine()
                .Append("<ColumnDefinition Width=\"Auto\"/>").AppendLine()
                .Append("<ColumnDefinition Width=\"*\"/>").AppendLine()
                .Append("</Grid.ColumnDefinitions>").AppendLine()
                .AppendFormat("<Canvas Grid.Column=\"0\" Background=\"{{Binding Item, Converter={{StaticResource getColumnIcon}}, ConverterParameter={0}}}\" ", name).AppendLine()
                .AppendFormat("Width=\"{{Binding Item, Converter={{StaticResource getColumnStatusIconSize}}, ConverterParameter={0}}}\"  />", name).AppendLine()
                .Append("<TextBlock Grid.Column=\"1\" ").AppendLine()
                .AppendFormat("Text=\"{{Binding Item.{0} {1}}}\" ", name, converter).AppendLine()
                .AppendFormat("TextAlignment=\"{{Binding DataTable.Columns, Converter={{StaticResource getColumnAlignment}}, ConverterParameter={0} }}\"", name).AppendLine()
                .AppendFormat("Foreground=\"{{Binding Item, Converter={{StaticResource getColumnTextColor}}, ConverterParameter={0}}}\"", name).AppendLine()
                .AppendFormat("FontStyle=\"{{Binding Item, Converter={{StaticResource getColumnTextFontStyle}}, ConverterParameter={0}}}\" ", name).AppendLine()
                .AppendFormat("FontWeight=\"{{Binding Item, Converter={{StaticResource getColumnTextFontWeight}}, ConverterParameter={0}}}\"", name).AppendLine()
                //.AppendFormat("FontSize=\"{{Binding Item, Converter={{StaticResource getColumnTextFontWeight}}, ConverterParameter={0}}}\"", name).AppendLine()
                .Append("/>").AppendLine()
                .Append("</Grid>").AppendLine();
        }

        private void AppendDataTableCell_TextBlock(StringBuilder sb, string name, string converter)
        {
            sb.AppendFormat("<TextBlock Grid.Column=\"{{Binding DataTable.Columns, Converter={{StaticResource getColumnNumber}}, ConverterParameter={0}}}\" ", name).AppendLine()
                .AppendFormat("Text=\"{{Binding Item.{0} {1}}}\" ", name, converter).AppendLine()
                .AppendFormat("TextAlignment=\"{{Binding DataTable.Columns, Converter={{StaticResource getColumnAlignment}}, ConverterParameter={0} }}\"", name).AppendLine()
                .AppendFormat("Foreground=\"{{Binding Item, Converter={{StaticResource getColumnTextColor}}, ConverterParameter={0}}}\"", name).AppendLine()
                .AppendFormat("FontStyle=\"{{Binding Item, Converter={{StaticResource getColumnTextFontStyle}}, ConverterParameter={0}}}\" ", name).AppendLine()
                .AppendFormat("FontWeight=\"{{Binding Item, Converter={{StaticResource getColumnTextFontWeight}}, ConverterParameter={0}}}\"", name).AppendLine()
              //  .AppendFormat("FontSize=\"{{Binding Item, Converter={{StaticResource getColumnTextFontWeight}}, ConverterParameter={0}}}\"", name).AppendLine()
                .Append("/>").AppendLine();
        }




        private void AppendStringFilterControl(StringBuilder sb, int i, string name)
        {
            sb.AppendFormat("<!-- DataColumn {0} -->", name).AppendLine()
                .AppendFormat("<Border Grid.Column=\"{{Binding DataTable.Columns, Converter={{StaticResource getColumnNumber}}, ConverterParameter={0}}}\">", name).AppendLine()
                .AppendFormat("<TextBox Text=\"{{Binding DataTable.Filter.{0}, Mode=TwoWay, Converter={{StaticResource nullableValueConverter}}}}\" Margin=\"1\" KeyUp=\"TextBox_KeyUp\" >", name).AppendLine();
            AppendToolTip(sb, name);
            AppendKeyUpEvent(sb);
            sb.AppendLine("</TextBox>")
            .AppendLine("</Border>");
        }

        private void AppendIntFilterControl(StringBuilder sb, int i, string name)
        {
            AppendStringFilterControl(sb, i, name);
        }

        private void AppendDateTimeFilterCOntrol(StringBuilder sb, int i, string name)
        {
            sb.AppendFormat("<!-- DataColumn {0} -->", name).AppendLine()
                .AppendFormat("<Border Grid.Column=\"{{Binding DataTable.Columns, Converter={{StaticResource getColumnNumber}}, ConverterParameter={0}}}\">", name).AppendLine();
            AppendToolTip(sb, name);
            sb.AppendLine("<Grid>")
                .AppendLine("<Grid.ColumnDefinitions>")
                .AppendLine("<ColumnDefinition Width=\"*\"/>")
                .AppendLine("<ColumnDefinition Width=\"3\"/>")
                .AppendLine("<ColumnDefinition Width=\"*\"/>")
                .AppendLine("</Grid.ColumnDefinitions>");
            AppendDateTimePicker(sb, name + "From", 0);
            AppendDateTimePicker(sb, name + "To", 2);
            sb.AppendLine("</Grid>")
                .AppendLine("</Border>");

        }

        private void AppendBoolFilterColntrol(StringBuilder sb, int i, string name)
        {
            sb.AppendFormat("<!-- DataColumn {0} -->", name).AppendLine()
                .AppendFormat("<Border Grid.Column=\"{{Binding DataTable.Columns, Converter={{StaticResource getColumnNumber}}, ConverterParameter={0}}}\">", name).AppendLine()
                .AppendFormat("<ComboBox IsTabStop=\"False\" SelectedIndex=\"{{Binding DataTable.Filter.{0}, Mode=TwoWay, Converter={{StaticResource nullableBoolConverter}}}}\">", name).AppendLine();

            var desc = GetBoolDescriptions(dto.Columns[i].Comment);
            foreach (var s in desc)
            {
                sb.AppendFormat("<ComboBoxItem Content=\"{0}\"/>", s).AppendLine();
            }

            AppendToolTip(sb, name);
            sb.AppendLine("</ComboBox>")
                .AppendLine("</Border>");
        }

        private void AppendKeyUpEvent(StringBuilder sb)
        {
            sb.AppendLine("<i:Interaction.Triggers>")
                .AppendLine("\t<i:EventTrigger EventName=\"KeyUp\">")
                .AppendLine("\t\t<MvvmLight_Command:EventToCommand Command=\"{Binding OnKeyUpOnFilter}\" PassEventArgsToCommand=\"True\"/>")
                .AppendLine("\t</i:EventTrigger>")
                .AppendLine("</i:Interaction.Triggers>");
        }
        private void AppendToolTip(StringBuilder sb, string name)
        {
            sb.AppendLine("<ToolTipService.ToolTip>")
                .AppendFormat("\t<ContentControl ContentTemplate=\"{{StaticResource FilterTipTemplate}}\" Content=\"{{Binding DataTable, Converter={{StaticResource getFilterDescription}}, ConverterParameter={0} }}\" />", name).AppendLine()
                .AppendLine("</ToolTipService.ToolTip>");
        }
        private void AppendDateTimePicker(StringBuilder sb, string fieldName, int col)
        {
            sb.AppendFormat("<telerik:RadDateTimePicker Grid.Column=\"{0}\" SelectedValue=\"{{Binding DataTable.Filter.{1}, Mode=TwoWay}}\" InputMode=\"DateTimePicker\" DateTimeWatermarkContent=\"wprowadź datę\" Margin=\"1\" KeyUp=\"TextBox_KeyUp\">", col, fieldName).AppendLine();
            AppendKeyUpEvent(sb);
            sb.AppendLine("</telerik:RadDateTimePicker>");
        }




    }
}
