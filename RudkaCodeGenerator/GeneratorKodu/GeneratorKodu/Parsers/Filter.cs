﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorKodu.Parsers
{
    class Filter: BaseParser
    {
        public Filter(Dto dto)
            : base(dto)
        { }

        public override string ParserName
        {
            get { return "Filter"; }
        }

        public override IDictionary<string, Func<string>> Parsers
        {
            get
            {
                return new Dictionary<string, Func<string>>()
                {
                    {"Fields", GenerateFields},
                };
            }
        }


        

        private object GetFilterType(string p)
        {
            if (p.StartsWith("int")) return "Int";
            if (p.StartsWith("double")) return "Double";
            if (p.StartsWith("DateTime")) return "Date";
            if (p.StartsWith("bool")) return "Bool";
            if (p.StartsWith("string")) return "String";

            throw new Exception("Nieobsługiwany typ " + p);
        }

        private string GenerateFields()
        {
            var sb = new StringBuilder();
            foreach(var item in dto.Columns)
            {
                if(item.Type.StartsWith("int"))
                {
                    GenerateTypeFilter(sb, item.Name, "string");
                }
                else if(item.Type.StartsWith("short"))
                {
                    GenerateTypeFilter(sb, item.Name, "string");
                }
                else if(item.Type.StartsWith("long"))
                {
                    GenerateTypeFilter(sb, item.Name, "string");
                }
                else if(item.Type.StartsWith("double"))
                {
                    GenerateTypeFilter(sb, item.Name, "string");
                }
                else if(item.Type.StartsWith("DateTime"))
                {
                    GenerateRangeFilter(sb, item.Name, "DateTime?");
                }
                else if(item.Type.StartsWith("bool"))
                {
                    GenerateTypeFilter(sb, item.Name, "bool?");
                }
                else if (item.Type.StartsWith("string"))
                {
                    GenerateTypeFilter(sb, item.Name, "string");
                }
                else
                {
                    throw new Exception("Nieobsługiwany typ właściwości: " + item.Type);
                }
                sb.AppendLine();
            }

            return sb.ToString();
        }

        private void GenerateTypeFilter(StringBuilder sb, string name, string typ)
        {
            sb.AppendLine("[RaisePropertyChanged]")
                .AppendFormat("public {0} {1} {{ get; set; }}", typ, name).AppendLine();
        }

        private void GenerateRangeFilter(StringBuilder sb, string name, string typ)
        {
            sb.AppendLine("[RaisePropertyChanged]")
                .AppendFormat("public {0} {1}From {{ get; set; }}", typ, name).AppendLine()
                .AppendLine("[RaisePropertyChanged]")
                .AppendFormat("public {0} {1}To {{ get; set; }}", typ, name).AppendLine();
        }

        
      
    }
}
