﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorKodu.Parsers
{
    class BoolValueConverter: BaseParser
    {
        public BoolValueConverter(Dto dto)
            : base(dto)
        { }

        public override string ParserName
        {
            get { return "BoolValueConverter"; }
        }

        public override IDictionary<string, Func<string>> Parsers
        {
            get {
                return new Dictionary<string, Func<string>>()
                {
                    {"Specific1", GenerateSpecific1},
                };
            }
        }



        

        private string GenerateSpecific1()
        {
            var sb = new StringBuilder();
            foreach (var item in dto.Columns)
            {
                if (!item.Type.StartsWith("bool")) continue;

                sb.AppendFormat("if(p.ToUpper() == \"{0}\")", item.Name.ToUpper()).AppendLine()
                    .AppendLine("{");


                var desc = GetBoolDescriptions(item.Comment);

                sb.AppendFormat("if (b == null) return \"{0}\";", desc[0]).AppendLine()
                    .AppendFormat("else if (b.Value) return \"{0}\";", desc[2]).AppendLine()
                    .AppendFormat("else return \"{0}\";", desc[1]).AppendLine()
                    .AppendLine("}").AppendLine();
            };
            return sb.ToString();
        }



    }
}
