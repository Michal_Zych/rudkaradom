﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorKodu.Parsers
{
    class View: BaseParser
    {
        public View(Dto dto)
            : base(dto)
        { }

        public override string ParserName
        {
            get { return "View"; }
        }
    }
}
