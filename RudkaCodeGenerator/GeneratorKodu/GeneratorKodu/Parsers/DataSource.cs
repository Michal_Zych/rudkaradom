﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorKodu.Parsers
{
    class DataSource : BaseParser
    {
        public DataSource(Dto dto)
            : base(dto)
        { }

        public override string ParserName
        {
            get { return "DataSource"; }
        }

       
    }
}
