﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorKodu.Parsers
{
    class DataTable: BaseParser
    {
        public DataTable(Dto dto)
            : base(dto)
        { }

        public override string ParserName
        {
            get { return "DataTable"; }
        }

       
        public override IDictionary<string, Func<string>> Parsers
        {
            get
            {
                return new Dictionary<string, Func<string>>()
                {
                    {"Columns", GenerateColumns},
                };
            }
        }


        private string GenerateColumns()
        {
            var sb = new StringBuilder();

            foreach(var prop in dto.Columns)
            {
                sb.AppendFormat(".Add(100, \"{0}\", \"{1}\", FilterDataType.{2})", prop.Name, prop.Name, prop.Type.First().ToString().ToUpper() + prop.Type.Substring(1)).AppendLine();
            }

            sb.Length = sb.Length - Environment.NewLine.Length;
            return sb.ToString();
        }

    }
}
