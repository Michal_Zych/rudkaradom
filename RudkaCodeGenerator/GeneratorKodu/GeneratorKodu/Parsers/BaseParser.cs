﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorKodu.Parsers
{
    public abstract class BaseParser
    {
        private const string patternsFolder = @"Szablony\";
        private const string patternExtension = ".txt";

        private const string EntityPattern = "$EntityName$";
        private const string DtoNamePattern = "$DtoName$";


        internal Dto dto;
        public string Name { get { return dto.TabName  + ParserName; } }

        public abstract string ParserName  {get;}
        public string PatternFileName {get{return patternsFolder + ParserName + patternExtension;}}

        private string content;
        public string Content { get { return content; } }

        public virtual IDictionary<string, Func<string>> Parsers { get { return new Dictionary<string, Func<string>>(); } }

        public BaseParser(Dto dto)
        {
            this.dto = dto;
            GenerateContent();
        }



        private void GenerateContent()
        {
            var s = File.ReadAllText(PatternFileName);
            s = s.Replace(EntityPattern, dto.TabName).Replace(DtoNamePattern, dto.DtoName);
            

            foreach(var item in Parsers)
            {
                s = s.Replace("$" + item.Key + "$",  item.Value());
            }

            content = s;
        }

        public string[] GetBoolDescriptions(string comment)
        {
            var result = new string[]{"", "nie", "tak"};
            if (!String.IsNullOrWhiteSpace(comment))
                {
                    var i = comment.Split(';');
                    if (i.Length < 2)
                    {

                        result[1] = i[0]; result[2] = i[1];
                    }
                    if (i.Length > 1)
                    {
                        result[0] = i[0]; result[1] = i[1]; result[2] = i[2];
                    }
                }
            return result;
        }

    }
}
