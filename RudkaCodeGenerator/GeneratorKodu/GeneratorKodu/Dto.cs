﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GeneratorKodu
{
    public class Dto
    {
        public string TabName { get; set; }
        public string DtoName { get; set; }


        public IList<DtoItem> Columns;

        private string ICON_TOKEN = "ICON";


        public Dto(IList<string> dtoList)
        {
            var classNameLine = dtoList.Where(x => x.Contains("public class")).FirstOrDefault();
            DtoName = Tokenizer.GetEntityName(classNameLine);
            TabName = Regex.Replace(DtoName, "dto", "", RegexOptions.IgnoreCase);


            Columns = new List<DtoItem>();
            foreach (var line in dtoList)
            {
                if (line.Contains("public") && !line.Contains("virtual") && line.Contains("get"))
                {
                    var words = Tokenizer.GetWords(line);

                    bool showIcon = false;
                    var comment = Tokenizer.GetComment(line);
                    var i = comment.ToUpper().IndexOf(ICON_TOKEN);
                    if (i >= 0)
                    {
                        showIcon = true;
                        comment = comment.Remove(i, ICON_TOKEN.Length);
                    }
                    comment = comment.Trim();


                    Columns.Add(new DtoItem() { Type = words[1], Name = words[2], Comment = comment, ShowIcon = showIcon });
                }
            }
        }


        public override string ToString()
        {
            var s = "class prefix " + TabName;
            foreach (var item in Columns)
            {
                s = s + Environment.NewLine + "  " + item.Type + " " + item.Name + (item.Comment == "" ? "" : " //" + item.Comment);
            }
            return s;
        }




        internal int PropertiesCount(string t = "")
        {
            if (t == "") return Columns.Count;

            var result = 0;
            foreach (var c in Columns)
            {
                if (c.Type.ToUpper().StartsWith(t.ToUpper())) result++;
            }
            return result;
        }
    }


    public class DtoItem
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string Comment { get; set; }
        public bool ShowIcon { get; set; }
    }
}
