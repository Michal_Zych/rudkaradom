﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratorKodu
{
    public static class Tokenizer
    {
        private const string CLASS_TOKEN = "class";
        public static string GetEntityName(string classNameLine)
        {
            return FirstTokenAfter(classNameLine, CLASS_TOKEN);
        }

        public static IList<string> GetWords(string text)
        {
            text = text.Replace('\t', ' ').Trim();
            while (text.Contains("  ")) text = text.Replace("  ", " ");

            var punctuation = text.Where(x => Char.IsPunctuation(x) && x != '_').Distinct().ToArray();

            var result = text.Split().Select(x => x.Trim(punctuation)).ToList();
            return result.Where(x => x != "").ToList();
        }


        private static string FirstTokenAfter(string line, string token)
        {
            var l = line.ToUpper();
            token = token.ToUpper();
            var i = l.IndexOf(token);
            var s = line.Substring(i + token.Length);
            s = s.Trim();
            i = s.IndexOfAny(new char[] { ' ', '\t', (char)13, (char)10 });
            if (i > 0)
            {
                s = s.Substring(0, i);
            }
            return s;
        }

        internal static string GetComment(string line)
        {
            var i = line.IndexOf("//");
            if (i > 0) return line.Substring(i + 2);
            return "";
        }
    }
}
