﻿using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.$EntityName$
{
    public class $EntityName$Filter : SortedFilter
    {
       $Fields$

        public $EntityName$Filter(IUnityContainer container)
            : base(container)
        {

        }
    }
}
