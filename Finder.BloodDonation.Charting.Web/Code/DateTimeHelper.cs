﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.DataVisualization.Charting;

namespace Finder.BloodDonation.Charting.Web.Code
{
    public static class DateTimeHelper
    {
        public static DateTimeIntervalType GetScale(DateTime tmin, DateTime tmax)
        {
            TimeSpan span = tmax - tmin;
            if (span.TotalDays > 5) return DateTimeIntervalType.Hours;
            return DateTimeIntervalType.Minutes;
        }


        public static double GetTimeSpan(this DateTimeIntervalType scale, DateTime tmin, DateTime tmax)
        {
            TimeSpan span = tmax - tmin;
            switch (scale)
            {
                case DateTimeIntervalType.Minutes:
                    return span.TotalMinutes;
                    break;
                case DateTimeIntervalType.Hours:
                    return span.TotalHours;
                    break;
            }

            return span.TotalDays;
        }

        public static DateTime AddTimeUnits(this DateTimeIntervalType scale, DateTime t, int units)
        {
            switch (scale)
            {
                case DateTimeIntervalType.Minutes:
                    return t.AddMinutes(units);
                    break;
                case DateTimeIntervalType.Hours:
                    return t.AddHours(units);
                    break;
            }
            return t.AddDays(units);
        }

    }
}
