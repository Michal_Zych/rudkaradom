﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Data;

namespace Finder.BloodDonation.Charting.Web.Code
{
    public static class MyAlarms
    {
        public static void Create(Chart c, DB chartData)
        {
            if (!Configurator.Instance.AlarmsVisible) return;

            DateTime tmax = chartData.MaxX.Value;
            DateTime tmin = chartData.MinX.Value;

            DateTimeIntervalType scale = DateTimeHelper.GetScale(tmin, tmax);
            //DateTimeIntervalType scale = DateTimeIntervalType.Minutes;

            for (int i = DB.ALARMS_IN_SERIE; i < chartData.Ds.Tables.Count - 2; i += DB.TABLES_PER_SERIE)
            {
                var configRow = chartData.Ds.Tables[i + 1].Rows[0];
                var color = StringToValue.GetColor(configRow[DB.ALARM_COLOR].ToString(), Configurator.Instance.AlarmsColor);
                color = Configurator.Instance.MonoIfNeeded(color);

                DrawAlarms(c, chartData.Ds.Tables[i], tmax, tmin, scale, color);
            }

            //alarmy braku łączności
            var _configRow = chartData.Ds.Tables[chartData.Ds.Tables.Count - 1].Rows[0];
            var _color = StringToValue.GetColor(_configRow[DB.ALARM_COLOR].ToString(), Configurator.Instance.AlarmsColor);
            _color = Configurator.Instance.MonoIfNeeded(_color);
            DrawAlarms(c, chartData.Ds.Tables[chartData.Ds.Tables.Count - 2], tmax, tmin, scale, _color);

        }

        public static void DrawAlarms(Chart c, DataTable table, DateTime tmax, DateTime tmin, DateTimeIntervalType scale, Color color)
        {
            DateTime? _tStart, _tEnd;
            DateTime tStart, tEnd;

            foreach (DataRow row in table.Rows)
            {
                _tStart = row.Field<DateTime?>(DB.COL_DATE_START);
                _tEnd = row.Field<DateTime?>(DB.COL_DATE_END);

                tStart = ((!_tStart.HasValue) || (_tStart.Value < tmin)) ? scale.AddTimeUnits(tmin, -1) : _tStart.Value;
                tEnd = ((!_tEnd.HasValue) || (_tEnd.Value > tmax)) ? scale.AddTimeUnits(tmax, 1) : _tEnd.Value;
                
                if (tStart > tmax || tStart > tEnd)
                {
                    return;
                }

                var alarm = new StripLine();
                alarm.BackColor = color;
                alarm.Interval = -1;
                alarm.IntervalType = scale;

                alarm.IntervalOffsetType = scale;
                alarm.StripWidthType = scale;

                alarm.IntervalOffset = scale.GetTimeSpan(tmin, tStart) + 1;
                alarm.StripWidth = scale.GetTimeSpan(tStart, tEnd);

                // Add strip line to the chart
                c.ChartAreas[0].AxisX.StripLines.Add(alarm);
            }
        }

    }
}
