﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

namespace Finder.BloodDonation.Charting.Web.Code
{
    public static class MyBorder
    {
        public static void Create(Chart c)
        {
            c.BackColor = Configurator.Instance.BackColor;
            c.BackSecondaryColor = Configurator.Instance.BackSecondaryColor;
            c.BackGradientStyle = Configurator.Instance.BackGradientStyle;
            
            c.BorderlineColor = Configurator.Instance.BorderlineColor;
            c.BorderlineWidth = Configurator.Instance.BorderlineWidth;
            c.BorderlineDashStyle = Configurator.Instance.BorderlineDashStyle;
            c.BorderSkin.SkinStyle = Configurator.Instance.BorderSkin;

            c.ChartAreas[0].BackColor = Configurator.Instance.InnerBackColor;
            c.ChartAreas[0].BackSecondaryColor = Configurator.Instance.InnerBackSecondaryColor;
            c.ChartAreas[0].BackGradientStyle = Configurator.Instance.InnerBackGradientStyle;

        }


    }
}
