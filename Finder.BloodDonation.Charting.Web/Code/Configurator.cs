﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Web.UI.DataVisualization.Charting;
using System.Configuration;
using System.Web;

namespace Finder.BloodDonation.Charting.Web.Code
{
    public class Configurator
    {
        private const int DEFAULT_WIDTH = 800;
        private const int DEFAULT_HEIGHT = 600;

        private bool _isMono;
        private int _width;
        private int _height;
        private DateTime _dateFrom;
        private DateTime _dateTo;
        private string _fileName;
        private string _directory;
        private bool _alarmsVisible;
        private string _showAllAlarms;

        public List<SerieDescription> Series;

        private static Configurator _instance;
        private Configurator() { }

        public static Configurator Instance
        {
            get{
                if (_instance == null)
                {
                    _instance = new Configurator();
                }
                return _instance;
            }
    }



        public void Initialize(HttpContext context)
        {
            Series = new List<SerieDescription>();

            _dateFrom = Convert.ToDateTime(context.Request.QueryString["From"]);
            _dateTo = Convert.ToDateTime(context.Request.QueryString["To"]);


            var sensors = context.Request.QueryString["Sensors"];
            int i = 0;
            foreach (var param in sensors.Split(','))
            {
                Series.Add(new SerieDescription(param));
                if (Series[Series.Count - 1].ColorNotDefined)
                {
                    Series[Series.Count - 1].Color = Configurator.Instance.GetDefColor(i++);
                }
            }


            string monoKey;
            _isMono = false;
            if ((monoKey = context.Request.QueryString.Get("mono")) != null)
            {
                _isMono = Int32.Parse(monoKey) == 1;
            }

            
            _showAllAlarms = context.Request.QueryString["ShowAllAlarms"];
            if (string.IsNullOrEmpty(_showAllAlarms))
            {
                _showAllAlarms = "0";
            }

            _directory = ConfigurationManager.AppSettings["ImageChacheFolder"] + sensors + "\\";
            _fileName = _directory + GetNameFromDate(_dateFrom) + "-" + GetNameFromDate(_dateTo) + ".png";


            int.TryParse(context.Request.QueryString["Width"], out _width);
            int.TryParse(context.Request.QueryString["Height"], out _height);

            bool isReport = context.Request.QueryString.Get("Width") == null;
            if (0 == _width || 0 == _height)
            {

                string widthStr = (isReport) ? "ReportImageWidth" : "ImageWidth";
                string heightStr = (isReport) ? "ReportImageHeight" : "ImageHeight";

                int.TryParse(ConfigurationManager.AppSettings[widthStr], out _width);
                int.TryParse(ConfigurationManager.AppSettings[heightStr], out _height);
            }

            SetMinMaxSize(isReport);
        }


        public string SensorsString
        {
            get
            {
                var s = "";
                foreach (var serie in Series)
                {
                    s = s + serie.SensorID + ",";
                }
                return s;
            }
        }


        private string GetNameFromDate(DateTime date)
        {
            return date.Year.ToString() + '_'
                 + date.Month.ToString() + '_'
                 + date.Day.ToString() + '_'
                 + date.Hour.ToString() + '_'
                 + date.Minute.ToString();
        }

        private void SetMinMaxSize(bool chartForReport)
        {
            if (!chartForReport)
            {
                int w, h;
                int.TryParse(ConfigurationManager.AppSettings["MinImageWidth"], out w);
                int.TryParse(ConfigurationManager.AppSettings["MinImageHeight"], out h);
                if (_width < w) _width = w;
                if (_height < h) _height = h;

                int.TryParse(ConfigurationManager.AppSettings["MaxImageWidth"], out w);
                int.TryParse(ConfigurationManager.AppSettings["MaxImageHeight"], out h);
                if (w > 0 && h > 0)
                {
                    if (_width > w) _width = w;
                    if (_height > h) _height = h;
                }
            }

            if (0 == _width) _width = DEFAULT_WIDTH;
            if (0 == _height) _height = DEFAULT_HEIGHT;
        }

        public string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["ChartConnection"]].ConnectionString;
            }
        }

        public string Directory
        {
            get
            {
                return _directory;
            }
        }

        public string FileName
        {
            get
            {
                return _fileName;
            }
        }


        public DateTime DateFrom
        {
            get
            {
                return _dateFrom;
            }
        }

        public DateTime DateTo
        {
            get
            {
                return _dateTo;
            }
        }


        public bool AlarmsVisible
        {
            get
            {
                return _alarmsVisible;
            }
            set
            {
                _alarmsVisible = value;
            }
        }


        public string ShowAllAlarms
        {
            get
            {
                return _showAllAlarms;
            }
            set
            {
                _showAllAlarms = value;
            }
        }


        public bool UseCache
        {
            get
            {
                bool use_cache = false;
                bool.TryParse(ConfigurationManager.AppSettings["UseCache"], out use_cache);
                return use_cache;
            }
        }


        public int Width { get { return _width; } }
        public int Height { get { return _height; } }


        public Color AlarmsColor
        {
            get
            {
                return _isMono ?
                    Color.FromArgb(233, 233, 233)
                    : StringToValue.GetColor(ConfigurationManager.AppSettings["AlarmsColor"], Color.FromArgb(252, 198, 203));
            }
        }


        public float RangeColorIntensity
        {
            get
            {
                var i = 100 - StringToValue.GetInt(ConfigurationManager.AppSettings["RangeColorIntensity"], 50); //w konfigu wartość wyrażona w %
                if (i < 0 || i > 100) i = 50;
                return (float)i / 100;
            }
        }



        public Color LegendItemShadowColor
        {
            get
            {
                return _isMono ?
                    Color.FromArgb(123, 123, 123)
                    : StringToValue.GetColor(ConfigurationManager.AppSettings["LegendItemShadowColor"], Color.FromArgb(123, 123, 123));
            }
        }

        public int LegendItemShadowOffset
        {
            get
            {
                return StringToValue.GetInt(ConfigurationManager.AppSettings["LegendItemShadowOffset"], 3);
            }
        }

        public Color LegendColor
        {
            get
            {
                return _isMono ?
                    Color.FromArgb(255, 255, 255)
                    : StringToValue.GetColor(ConfigurationManager.AppSettings["LegendColor"], Color.FromArgb(233, 233, 233));
            }
        }
        public Color LegendBorderColor
        {
            get
            {
                return _isMono ?
                    Color.FromArgb(142, 142, 142)
                    : StringToValue.GetColor(ConfigurationManager.AppSettings["LegendBorderColor"], Color.FromArgb(142, 142, 142));
            }
        }
        public int LegendBorderWidth
        {
            get
            {
                return StringToValue.GetInt(ConfigurationManager.AppSettings["LegendBorderWidth"], 1);
            }
        }
        public Color LegendBorderShadowColor
        {
            get
            {
                return _isMono ?
                    Color.FromArgb(123, 123, 123)
                    : StringToValue.GetColor(ConfigurationManager.AppSettings["LegendBorderShadowColor"], Color.FromArgb(123, 123, 123));
            }
        }
        public int LegendBorderShadowOffset
        {
            get
            {
                return StringToValue.GetInt(ConfigurationManager.AppSettings["LegendBorderShadowOffset"], 3);
            }
        }
        public string LegendFont
        {
            get
            {
                return ConfigurationManager.AppSettings["LegendFont"];
            }
        }
        public float LegendFontSize
        {
            get
            {
                return StringToValue.GetInt(ConfigurationManager.AppSettings["LegendFontSize"], 8);
            }
        }

        public Color AxisColor
        {
            get
            {
                return _isMono ?
                    Color.FromArgb(189, 189, 189)
                    : StringToValue.GetColor(ConfigurationManager.AppSettings["AxisColor"], Color.FromArgb(189, 189, 189));
            }
        }
        public int AxisWidth
        {
            get
            {
                return StringToValue.GetInt(ConfigurationManager.AppSettings["AxisWidth"], 2);
            }
        }
        public int AxisGridWidth
        {
            get
            {
                return StringToValue.GetInt(ConfigurationManager.AppSettings["AxisGridWidth"], 1);
            }
        }
        public Color AxisGridColor
        {
            get
            {
                return _isMono ?
                    Color.FromArgb(189, 189, 189)
                    : StringToValue.GetColor(ConfigurationManager.AppSettings["AxisGridColor"], Color.FromArgb(189, 189, 189));
            }
        }
        public int RangesFontSize
        {
            get
            {
                return StringToValue.GetInt(ConfigurationManager.AppSettings["RangesFontSize"], 8);
            }
        }
        public bool AxisXGrid
        {
            get
            {
                return StringToValue.GetBool(ConfigurationManager.AppSettings["AxisXGrid"], false);
            }
        }
        public bool AxisYGrid
        {
            get
            {
                return StringToValue.GetBool(ConfigurationManager.AppSettings["AxisYGrid"], true);
            }
        }

        public Color BackColor
        {
            get
            {
                return _isMono ?
                    Color.FromArgb(255, 255, 255)
                    : StringToValue.GetColor(ConfigurationManager.AppSettings["BackColor"], Color.FromArgb(211, 223, 240));
            }
        }
        public Color BackSecondaryColor
        {
            get
            {
                return _isMono ?
                    Color.FromArgb(255, 255, 255)
                    : StringToValue.GetColor(ConfigurationManager.AppSettings["BackSecondaryColor"], Color.FromArgb(255, 255, 255));
            }
        }
        public GradientStyle BackGradientStyle
        {
            get
            {
                return _isMono ?
                    GradientStyle.None
                    : StringToValue.GetEnumValue<GradientStyle>(ConfigurationManager.AppSettings["BackGradientStyle"], GradientStyle.TopBottom);
            }
        }

        public Color InnerBackColor
        {
            get
            {
                return _isMono ?
                    Color.FromArgb(255, 255, 255)
                    : StringToValue.GetColor(ConfigurationManager.AppSettings["InnerBackColor"], Color.FromArgb(211, 223, 240));
            }
        }
        public Color InnerBackSecondaryColor
        {
            get
            {
                return _isMono ?
                    Color.FromArgb(255, 255, 255)
                    : StringToValue.GetColor(ConfigurationManager.AppSettings["InnerBackSecondaryColor"], Color.FromArgb(245, 245, 245));
            }
        }
        public GradientStyle InnerBackGradientStyle
        {
            get
            {
                return _isMono ?
                    GradientStyle.None
                    : StringToValue.GetEnumValue<GradientStyle>(ConfigurationManager.AppSettings["InnerBackGradientStyle"], GradientStyle.TopBottom);
            }
        }
        public Color BorderlineColor
        {
            get
            {
                return _isMono ?
                    Color.FromArgb(0, 0, 0)
                    : StringToValue.GetColor(ConfigurationManager.AppSettings["BorderlineColor"], Color.FromArgb(26, 59, 105));
            }
        }
        public int BorderlineWidth
        {
            get
            {
                return _isMono ?
                    1
                    : StringToValue.GetInt(ConfigurationManager.AppSettings["BorderlineWidth"], 2);
            }
        }
        public ChartDashStyle BorderlineDashStyle
        {
            get
            {
                return StringToValue.GetEnumValue<ChartDashStyle>(ConfigurationManager.AppSettings["BorderlineDashStyle"], ChartDashStyle.Solid);
            }
        }
        public BorderSkinStyle BorderSkin
        {
            get
            {
                return _isMono ?
                    BorderSkinStyle.None
                    : StringToValue.GetEnumValue<BorderSkinStyle>(ConfigurationManager.AppSettings["BorderSkin"], BorderSkinStyle.Emboss);
            }
        }

 
        internal Color MonoIfNeeded(Color color)
        {
            if (_isMono)
            {
                int x = (color.R + color.G + color.B) / 3;

                return Color.FromArgb(color.A, x, x, x);
            }
            
            return color;
        }

        public int DefLineWidth
        {
            get
            {
                return StringToValue.GetInt(ConfigurationManager.AppSettings["SeriesWidth"], 3);
            }
        }


        public int DefRangeLineWidth
        {
            get
            {
                return StringToValue.GetInt(ConfigurationManager.AppSettings["RangeWidth"], 2);
            }
        }

        public ChartDashStyle DefLineStyle
        {
            get
            {
                return StringToValue.GetEnumValue<ChartDashStyle>(ConfigurationManager.AppSettings["SeriesStyle"], ChartDashStyle.Solid);
            }
        }

        public ChartDashStyle DefRangeLineStyle
        {
            get
            {
                return StringToValue.GetEnumValue<ChartDashStyle>(ConfigurationManager.AppSettings["RangeStyle"], ChartDashStyle.Dash);
            }
        }

        public Color GetDefColor(int i)
        {
            Color c;
            try
            {
                c = StringToValue.GetColor(ConfigurationManager.AppSettings["DefColor" + i.ToString()], Color.FromArgb(0, 0, 0));
            }
            catch
            {
                Random rnd = new Random(0);
                c = Color.FromArgb(rnd.Next(255), rnd.Next(255), rnd.Next(255));
            }
            return c;
        }


        public float TwoStateSensorsAreaSizePercent
        {
            get
            {
                float result = 30;
                try
                {
                    result = float.Parse(ConfigurationManager.AppSettings["TwoStateSensorsAreaSizePercent"]);
                }
                catch { }
                return result / 100;
            }
        }

        public float TwoStateSensorsMarginSizePercent
        {
            get
            {
                float result =50;
                try
                {
                    result = float.Parse(ConfigurationManager.AppSettings["TwoStateSensorsMarginSizePercent"]);
                }
                catch { }
                return result / 100;
            }
        }

    }
}
