﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Data;
using System.Web.UI.DataVisualization.Charting;

namespace Finder.BloodDonation.Charting.Web.Code
{
    public static class MyAxis
    {
        private static void SetLines(Axis a)
        {
            a.MajorGrid.Enabled = false;
            a.MinorGrid.Enabled = false;
            a.LineColor = Configurator.Instance.AxisColor;
            a.LineWidth = Configurator.Instance.AxisWidth;
            a.MajorTickMark.LineColor = Configurator.Instance.AxisGridColor;
            a.MinorTickMark.LineColor = Configurator.Instance.AxisGridColor;
        }
        private static void HideLabels(Axis a)
        {
            a.MajorTickMark.Enabled = false;
            a.MinorTickMark.Enabled = false;
            a.LabelStyle.Enabled = false;
        }

        private static void SetGrid(Axis a)
        {
            a.MajorGrid.Enabled = true;
            //a.MinorGrid.Enabled = true;
            a.MajorGrid.LineColor = Configurator.Instance.AxisGridColor;
            a.MinorGrid.LineColor = Configurator.Instance.AxisGridColor;
            a.MajorGrid.LineWidth = Configurator.Instance.AxisGridWidth;
            a.MinorGrid.LineWidth = Configurator.Instance.AxisGridWidth;
        }

        public static void Create(Chart c, DB chartData)
        {
            const double MinMaxLabelArea = 200;
            var X = c.ChartAreas[0].AxisX;
            var Y = c.ChartAreas[0].AxisY;
            var X2 = c.ChartAreas[0].AxisX2;
            var Y2 = c.ChartAreas[0].AxisY2;

            SetLines(X);
            if (Configurator.Instance.AxisXGrid) SetGrid(X);
            SetLines(X2);
            HideLabels(X2);
            X2.Enabled = AxisEnabled.True;

            

            SetLines(Y);
            if (Configurator.Instance.AxisYGrid) SetGrid(Y);
            if (chartData.OnlyBitSensors)
            {
                HideLabels(Y); 
                
            }
            SetLines(Y2);
            HideLabels(Y2);
            Y2.Enabled = AxisEnabled.True;
            Y2.LabelStyle.Enabled = true;



            

            //opisy na prawej Y
            var d = RangesDescriptor.GetValues(chartData);
            Y2.CustomLabels.Clear();
            Y2.LabelAutoFitMaxFontSize = Configurator.Instance.RangesFontSize;

            foreach (var row in d)
            {
                var lbl = new CustomLabel();
                lbl.FromPosition = row.Key - MinMaxLabelArea;
                lbl.ToPosition = row.Key + MinMaxLabelArea;
                lbl.Text = row.Value;
                lbl.RowIndex = 0;
                Y2.CustomLabels.Add(lbl);
            }
            /*
            X.Interval = 1;
            X.IntervalType = DateTimeHelper.GetScale(chartData.MinX.Value, chartData.MaxX.Value);
            X.LabelStyle.Format = (X.IntervalType==DateTimeIntervalType.Days) ? "yyyy-MM-dd": "yyyy-MM-dd HH:mm";
            */
            X.LabelStyle.Format = "yyyy-MM-dd HH:mm";

            CorrectScaleForBitSensors(c, chartData, Y, Y2);

           
        }

        private static void CorrectScaleForBitSensors(Chart c, DB chartData, Axis Y, Axis Y2)
        {
            if (chartData.HasBitSensor)
            {
                var min = double.MaxValue;
                var max = double.MinValue;

                foreach (var serie in c.Series)
                {
                    foreach (var dp in serie.Points)
                    {
                        var value = dp.YValues[0];
                        if (value < min) min = value;
                        if (value > max) max = value;
                    }
                }

                c.ChartAreas[0].RecalculateAxesScale();

                if (Y.Maximum == max)
                {
                    Y.Maximum = Y.Maximum + 0.2;
                    Y2.Maximum = Y.Maximum;
                }

                if (Y.Minimum == min)
                {
                    Y.Minimum = Y.Minimum - 0.2;
                    Y2.Minimum = Y.Minimum;
                }
            }
        }

        
        public static void Createold(Chart c, DB chartData)
        {
            const double MinMaxLabelArea = 200;
            var X = c.ChartAreas[0].AxisX;
            var Y = c.ChartAreas[0].AxisY;
            var X2 = c.ChartAreas[0].AxisX2;
            var Y2 = c.ChartAreas[0].AxisY2;

            SetLines(X);
            if(Configurator.Instance.AxisXGrid) SetGrid(X);
            SetLines(X2);
            HideLabels(X2);
            X2.Enabled = AxisEnabled.True;


            SetLines(Y);
            if(Configurator.Instance.AxisYGrid) SetGrid(Y);
            SetLines(Y2);
            HideLabels(Y2);
            Y2.Enabled = AxisEnabled.True;
            Y2.LabelStyle.Enabled = true;

            //opisy na prawej Y
            var d = RangesDescriptor.GetValues(chartData);
            Y2.CustomLabels.Clear();
            Y2.LabelAutoFitMaxFontSize = Configurator.Instance.RangesFontSize;

            foreach (var row in d)
            {
                var lbl = new CustomLabel();
                lbl.FromPosition = row.Key - MinMaxLabelArea;
                lbl.ToPosition = row.Key + MinMaxLabelArea;
                lbl.Text = row.Value;
                lbl.RowIndex = 0;
                Y2.CustomLabels.Add(lbl);
            }
            /*
            X.Interval = 1;
            X.IntervalType = DateTimeHelper.GetScale(chartData.MinX.Value, chartData.MaxX.Value);
            X.LabelStyle.Format = (X.IntervalType==DateTimeIntervalType.Days) ? "yyyy-MM-dd": "yyyy-MM-dd HH:mm";
            */
            X.LabelStyle.Format = "yyyy-MM-dd HH:mm";
        }

    }
}
