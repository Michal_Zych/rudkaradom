﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Web.UI.DataVisualization.Charting;

namespace Finder.BloodDonation.Charting.Web.Code
{
    public static class StringToValue
    {
        public static Color GetColor(string s, Color defColor)
        {
            var str = s.Split(new Char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

            try
            {
                if (str.Length == 4)
                {
                    return Color.FromArgb(int.Parse(str[0]), int.Parse(str[1]), int.Parse(str[2]), int.Parse(str[3]));
                }
                else
                {
                    return Color.FromArgb(int.Parse(str[0]), int.Parse(str[1]), int.Parse(str[2]));
                }
            }
            catch
            {
                return defColor;
            }
        }

        public static int GetInt(string s, int defI)
        {
            try
            {
                return int.Parse(s);
            }
            catch
            {
                return defI;
            }
        }

        public static bool GetBool(string s, bool defBool)
        {
            try
            {
                return bool.Parse(s);
            }
            catch
            {
                return defBool;
            }
        }

        public static T GetEnumValue<T>(string s, T def)
        {
            try
            {
                return (T)Enum.Parse(typeof(T), s);
            }
            catch
            {
                return def;
            }
        }

        
    }
}
