﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI.DataVisualization.Charting;

namespace Finder.BloodDonation.Charting.Web.Code
{
    public class SerieDescription
    {
        public string SensorID;
        public string Name;
        public string ShortName;
        public string RangeName;
        public int LineWidth;
        public int RangeLineWidth;
        public ChartDashStyle LineStyle;
        public ChartDashStyle RangeLineStyle;



        public SerieDescription(string s)
        {
            _colorNotDefined = true;

            int sensor;
            var i = s.IndexOf("(");
            if (i > -1)
            {
                SensorID = s.Substring(0, i);
                var c = "#" + s.Substring(i + 1, s.Length - i - 2);
                var converter = new ColorConverter();
                Color = (Color)converter.ConvertFromString(c);
            }
            else
            {
                SensorID = s;
            }
        }



        private Color LightColor(Color color)
        {
            float correctionFactor = Configurator.Instance.RangeColorIntensity;
            float red = (255 - color.R) * correctionFactor + color.R;
            float green = (255 - color.G) * correctionFactor + color.G;
            float blue = (255 - color.B) * correctionFactor + color.B;
            return Color.FromArgb(color.A, (int)red, (int)green, (int)blue);
        }



        private bool _colorNotDefined;
        public bool ColorNotDefined {
            get
            {
                return _colorNotDefined;
            }
        }

        private Color _color;
        public Color Color
        {
            get
            {
                return Configurator.Instance.MonoIfNeeded(_color);
            }

            set
            {
                _color = value;
                _colorNotDefined = false;
            }
        }

        public Color RangeColor
        {
            get
            {
                return Configurator.Instance.MonoIfNeeded(LightColor(_color));
            }
        }

        private Color _alarmColor;
        public Color AlarmColor
        {
            get
            {
                return Configurator.Instance.MonoIfNeeded(_alarmColor);
            }

            set
            {
                _alarmColor = value;
            }
        }

    }
}