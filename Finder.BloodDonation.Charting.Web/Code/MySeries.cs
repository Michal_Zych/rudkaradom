﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Data;

namespace Finder.BloodDonation.Charting.Web.Code
{
    public static class MySeries
    {
        public static void Create(Chart c, DB chartData)
        {
            c.Series.Clear();

            //zakresy alarmowe
            for(int i = 0; i < Configurator.Instance.Series.Count; i++)
            {
                var serie = Configurator.Instance.Series[i];
                CreateSerie(c
                    , chartData.Ds.Tables[DB.RANGES_IN_SERIE + i * DB.TABLES_PER_SERIE]
                    , DB.COL_DATE_TIME
                    , DB.COL_LOW_RANGE
                    , serie.RangeColor
                    , serie.RangeLineWidth
                    , serie.RangeLineStyle);
                CreateSerie(c
                    , chartData.Ds.Tables[DB.RANGES_IN_SERIE + i * DB.TABLES_PER_SERIE]
                    , DB.COL_DATE_TIME
                    , DB.COL_HI_RANGE
                    , serie.RangeColor
                    , serie.RangeLineWidth
                    , serie.RangeLineStyle);
            }

            //wartości
            for (int i = 0; i < Configurator.Instance.Series.Count; i++)
            {
                var serie = Configurator.Instance.Series[i];
                CreateSerie(c
                    , chartData.Ds.Tables[DB.VALUES_IN_SERIE + i * DB.TABLES_PER_SERIE]
                    , DB.COL_DATE_TIME
                    , DB.COL_VALUE
                    , serie.Color
                    , serie.LineWidth
                    , serie.LineStyle);
            }
        }


        private static void CreateSerie(Chart c, DataTable table, string xColumnName, string yColumnName, 
                Color color, int lineWidth, ChartDashStyle style)
        {
            var serie = new Series
            {
                ChartType = SeriesChartType.FastLine,
                XValueType = ChartValueType.DateTime,
                //Name = name,
                BorderWidth = lineWidth,
                ShadowColor = Color.Black,
                ShadowOffset = 5,
                Color = color,
                BorderDashStyle = style,
                IsVisibleInLegend = false
            };

            var dv = new DataView(table);
            try
            {
                serie.Points.DataBindXY(dv, xColumnName, dv, yColumnName);
                c.Series.Add(serie);
            }
            finally
            {
                dv.Dispose();
            }
        }
        
    }
}
