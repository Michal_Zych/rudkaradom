﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

namespace Finder.BloodDonation.Charting.Web.Code
{
    public static class MyLegend
    {
        public static void Create(Chart c, DB chartData)
        {
            c.Legends.Clear();

            c.Legends.Add(new Legend()
            {
                Docking = Docking.Top,
                Alignment = StringAlignment.Center,
                BackColor = Configurator.Instance.LegendColor,
                BorderColor = Configurator.Instance.LegendBorderColor,
                BorderWidth = Configurator.Instance.LegendBorderWidth,
                ShadowColor = Configurator.Instance.LegendBorderShadowColor,
                ShadowOffset = Configurator.Instance.LegendBorderShadowOffset,
                Font = new Font(Configurator.Instance.LegendFont, Configurator.Instance.LegendFontSize)
            });


            for (int i = 0; i < Configurator.Instance.Series.Count; i++)
            {
                var serie = Configurator.Instance.Series[i];
                c.Legends[0].CustomItems.Add(new LegendItem(serie.Name, serie.Color, string.Empty));
                c.Legends[0].CustomItems.Add(new LegendItem(serie.RangeName, serie.RangeColor, string.Empty));

                if (Configurator.Instance.Series.Count < 2)//dla 2 czujników rysuj odstępy
                    c.Legends[0].CustomItems.Add(new LegendItem(string.Empty, Color.Transparent, string.Empty));
            }

            /*
            for (int i = DB.CONFIG_IN_SERIE; i < chartData.Ds.Tables.Count - 2; i += DB.TABLES_PER_SERIE)
            {
                var configRow = chartData.Ds.Tables[i].Rows[0];
                var rangeDesc = configRow[DB.LEGEND_RANGE_DESCRIPTION].ToString();
                var rangeColor = StringToValue.GetColor(configRow[DB.RANGE_COLOR].ToString(), Configurator.Instance.T0RangeColor);
                var desc = configRow[DB.LEGEND_DESCRIPTION].ToString();
                var color = StringToValue.GetColor(configRow[DB.COLOR].ToString(), Configurator.Instance.T0RangeColor);

                rangeColor = Configurator.Instance.MonoIfNeeded(rangeColor);
                color = Configurator.Instance.MonoIfNeeded(color);

                c.Legends[0].CustomItems.Add(new LegendItem(rangeDesc, rangeColor, string.Empty));
                c.Legends[0].CustomItems.Add(new LegendItem(desc, color, string.Empty));
                if(chartData.Ds.Tables.Count<=10)//dla 2 czujników rysuj odstępy
                    c.Legends[0].CustomItems.Add(new LegendItem(string.Empty, Color.Transparent, string.Empty));
            }
            */
            if (Configurator.Instance.AlarmsVisible)
            {
                var color = Configurator.Instance.MonoIfNeeded(Configurator.Instance.Series[0].AlarmColor);
                c.Legends[0].CustomItems.Add(new LegendItem("Alarmy", color, string.Empty));
            }

            foreach (LegendItem item in c.Legends[0].CustomItems)
            {
                if (item.Color == Color.Transparent)
                {
                    item.BorderColor = Color.Transparent;
                }
                else
                {
                    item.ShadowColor = Configurator.Instance.LegendItemShadowColor;
                    item.ShadowOffset = Configurator.Instance.LegendItemShadowOffset;
                }
            }

        }

    }
}
