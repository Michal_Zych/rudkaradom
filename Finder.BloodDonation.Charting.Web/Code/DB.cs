﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.UI.DataVisualization.Charting;

namespace Finder.BloodDonation.Charting.Web.Code
{

    public class DB
    {
        //nazwy kolumn konfiguracji
        public const string COL_DATE_TIME = "TimeStamp";
        public const string COL_VALUE = "Value";
        public const string COL_LOW_RANGE = "AL";
        public const string COL_HI_RANGE = "AH";
        public const string COL_DATE_START = "DateStart";
        public const string COL_DATE_END = "DateEnd";


        public const string LEGEND_DESCRIPTION = "LegendDescription";
        public const string LEGEND_SHORTCUT = "LegendShortcut";
        public const string LEGEND_RANGE_DESCRIPTION = "LegendRangeDescription";
        public const string COLOR = "LineColor";
        public const string RANGE_COLOR = "RangeColor";
        public const string ALARM_COLOR = "AlarmColor";
        public const string LINE_WIDTH = "LineWidth";
        public const string RANGE_LINE_WIDTH = "RangeLineWidth";
        public const string LINE_STYLE = "LineStyle";
        public const string RANGE_LINE_STYLE = "RangeLineStyle";
        public const string ALARMCOLOR = "AlarmColor";
        public const string IS_BIT_SENSOR = "IsBitSensor";
        public const string LO_STATE_DESC = "LoStateDesc";
        public const string HI_STATE_DESC = "HiStateDesc";

        public const int TABLES_PER_SERIE = 4;
        public const int VALUES_IN_SERIE = 0;
        public const int RANGES_IN_SERIE = 1;
        public const int ALARMS_IN_SERIE = 2;
        public const int CONFIG_IN_SERIE = 3;

        //private const string SelectCmdString = @"EXEC crt_DeviceChart @device_id = {0}, @date_from = N'{1}', @date_to = N'{2}'";
        private const string SelectCmdString = @"EXEC dbo.crt_GetChartData @dateFrom = N'{0}', @dateTo = N'{1}', @sensors = '{2}', @showAllAlarms = '{3}', @rowCount = '{4}'";


        private DataSet _ds;

        public DB()
        {
            string cmd = string.Format(SelectCmdString,
                    Configurator.Instance.DateFrom.ToShortDateString() + " " + Configurator.Instance.DateFrom.ToShortTimeString(),
                    Configurator.Instance.DateTo.ToShortDateString() + " " + Configurator.Instance.DateTo.ToShortTimeString(),
                    Configurator.Instance.SensorsString,
                    Configurator.Instance.ShowAllAlarms,
                    Configurator.Instance.Width
                    );

            _ds = ReadData(cmd, Configurator.Instance.ConnectionString);

            ScaleBitSensors();


            Configurator.Instance.AlarmsVisible = AlarmsVisible;
            UpdateConfig();
        }

        private void ScaleBitSensors()
        {
            if (HasBitSensor)
            {
                float min, max;
                if (OnlyBitSensors)
                {
                    if (BitSensorsCount == 1)
                    {
                        return;
                    }
                    min = 0;
                    max = 1;
                }
                else
                {
                    min = float.MaxValue;
                    max = float.MinValue;
                    for (int i = 0; i <= _ds.Tables.Count - 2; i += TABLES_PER_SERIE)
                    {
                        var configRow = _ds.Tables[i + 3].Rows[0];
                        var isBitSensor = Convert.ToBoolean(configRow[DB.IS_BIT_SENSOR]);
                        if (!isBitSensor)
                        {
                            for (int j = 0; j < _ds.Tables[i].Rows.Count; j++)
                            {
                                var value = _ds.Tables[i].Rows[j].Field<float?>(COL_VALUE);
                                if (value.HasValue)
                                {
                                    if (value > max) max = value.Value;
                                    if (value < min) min = value.Value;
                                }
                            }

                            for (int j = 0; j < _ds.Tables[i + 1].Rows.Count; j++)
                            {
                                var loValue = _ds.Tables[i + 1].Rows[j].Field<float?>(COL_LOW_RANGE);
                                var hiValue = _ds.Tables[i + 1].Rows[j].Field<float?>(COL_HI_RANGE);
                                if (loValue.HasValue)
                                {
                                    if (loValue > max) max = loValue.Value;
                                    if (loValue < min) min = loValue.Value;
                                }
                                if (hiValue.HasValue)
                                {
                                    if (hiValue > max) max = hiValue.Value;
                                    if (hiValue < min) min = hiValue.Value;
                                }
                            }
                        }
                    }
                }
                var bitSensorscount = BitSensorsCount;



                float pr10 = (max - min) * (Configurator.Instance.TwoStateSensorsAreaSizePercent / (float)3);
                max = min;
                if (bitSensorscount == 1)
                {
                    min = min - pr10;
                }
                else if (bitSensorscount == 2)
                {
                    min = min - pr10 * 2;
                }
                else
                {
                    min = min - pr10 * 3;
                }

                float h = (max - min) / bitSensorscount;
                int sensorNr = 0;
                float hiState = max;
                float loState = hiState - h;

                for (int i = 0; i < _ds.Tables.Count; i += DB.TABLES_PER_SERIE)
                {
                    var configRow = _ds.Tables[i + 3].Rows[0];
                    var isBitSensor = Convert.ToBoolean(configRow[DB.IS_BIT_SENSOR]);
                    if (isBitSensor)
                    {
                        hiState = hiState - (float)(h * (float)Configurator.Instance.TwoStateSensorsMarginSizePercent);
                        for (int j = 0; j < _ds.Tables[i].Rows.Count; j++)
                        {
                            if (_ds.Tables[i].Rows[j].Field<float?>(COL_VALUE).HasValue)
                            {
                                if (_ds.Tables[i].Rows[j].Field<float?>(COL_VALUE).Value == 0)
                                {
                                    _ds.Tables[i].Rows[j][COL_VALUE] = loState;
                                }
                                else
                                {
                                    _ds.Tables[i].Rows[j][COL_VALUE] = hiState;
                                }
                            }
                        }
                        for (int j = 0; j < _ds.Tables[i + 1].Rows.Count; j++)
                        {
                            _ds.Tables[i + 1].Rows[j][COL_HI_RANGE] = hiState;
                            _ds.Tables[i + 1].Rows[j][COL_LOW_RANGE] = loState;
                        }

                        sensorNr = sensorNr + 1;
                        hiState = max - h * (float)sensorNr;
                        loState = hiState - h;
                    }
                }

            }

        }



        private void UpdateConfig()
        {
            int serie = 0;
            int table = CONFIG_IN_SERIE;
            while (table < _ds.Tables.Count)
            {
                var desc = Configurator.Instance.Series[serie];
                var row = _ds.Tables[table].Rows[0];

                desc.Name = row[LEGEND_DESCRIPTION].ToString();
                desc.ShortName = row[LEGEND_SHORTCUT].ToString();
                desc.RangeName = row[LEGEND_RANGE_DESCRIPTION].ToString();
                desc.Color = StringToValue.GetColor(row[COLOR].ToString(), Configurator.Instance.GetDefColor(serie));
                desc.LineWidth = StringToValue.GetInt(row[LINE_WIDTH].ToString(), Configurator.Instance.DefLineWidth);
                desc.RangeLineWidth = StringToValue.GetInt(row[RANGE_LINE_WIDTH].ToString(), Configurator.Instance.DefRangeLineWidth);
                desc.AlarmColor = StringToValue.GetColor(row[ALARMCOLOR].ToString(), Configurator.Instance.AlarmsColor);

                desc.LineStyle = StringToValue.GetEnumValue<ChartDashStyle>(row[LINE_STYLE].ToString(), Configurator.Instance.DefLineStyle);
                desc.RangeLineStyle = StringToValue.GetEnumValue<ChartDashStyle>(row[RANGE_LINE_STYLE].ToString(), Configurator.Instance.DefRangeLineStyle);

                table += TABLES_PER_SERIE;
                serie += 1;
            }
        }

        private DataSet ReadData(string command, string connectionString)
        {
            DataSet ds = null;

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                using (SqlDataAdapter adap = new SqlDataAdapter(command, con))
                {
                    ds = new DataSet();
                    adap.Fill(ds, "Data");
                }
                con.Close();
            }

            return ds;
        }


        public int DeviceType
        {
            get
            {
                int res = 0;
                try
                {
                    res = _ds.Tables[3].Rows[0].Field<int>("DeviceType");
                }
                catch { };
                return res;
            }
        }

        public bool AlarmsVisible
        {
            get
            {
                bool res = false;
                int i = 2;
                try
                {
                    while (!res && i < _ds.Tables.Count)
                    {
                        if (_ds.Tables[i].Rows.Count > 0)
                            res = true;
                        i += TABLES_PER_SERIE;
                    }
                    if (!res)
                        if (_ds.Tables[_ds.Tables.Count - 2].Rows.Count > 0)
                            res = true;
                }
                catch { };
                return res;
            }
        }

        public DataSet Ds
        {
            get
            {
                return _ds;
            }
        }

        public DateTime? MinX
        {
            get
            {
                DateTime? res = DateTime.MaxValue;

                try
                {
                    for (int i = 0; i <= _ds.Tables.Count - 2; i += TABLES_PER_SERIE)
                    {
                        DateTime? d1;
                        if (_ds.Tables[i].Rows.Count > 0)
                        {
                            d1 = _ds.Tables[i].Rows[0].Field<DateTime?>(COL_DATE_TIME);
                            if (d1 < res)
                                res = d1;
                        }
                        if (_ds.Tables[i + 1].Rows.Count>0)
                        {
                        d1 = _ds.Tables[i + 1].Rows[0].Field<DateTime?>(COL_DATE_TIME);
                        if (d1 < res)
                            res = d1;
                        }
                    }
                }
                catch { };
                if (res == DateTime.MaxValue)
                    return null;
                else
                    return res;
            }
        }

        public DateTime? MaxX
        {
            get
            {
                DateTime? res = DateTime.MinValue;

                try
                {
                    for (int i = 0; i <= _ds.Tables.Count - 2; i += TABLES_PER_SERIE)
                    {
                        DateTime? d1;
                        if (_ds.Tables[i].Rows.Count > 0)
                        {
                            d1 = _ds.Tables[i].Rows[_ds.Tables[i].Rows.Count - 1].Field<DateTime?>(COL_DATE_TIME);
                            if (d1 > res)
                                res = d1;
                        }
                        if (_ds.Tables[i + 1].Rows.Count > 0)
                        {
                            d1 = _ds.Tables[i + 1].Rows[_ds.Tables[i + 1].Rows.Count - 1].Field<DateTime?>(COL_DATE_TIME);
                            if (d1 > res)
                                res = d1;
                        }
                    }
                }
                catch { };
                if (res == DateTime.MinValue)
                    return null;
                else
                    return res;
            }
        }

        public double GetMaxRange(int tabIdx, string colName)
        {
            double res = 0.0;
            try
            {
                res = Convert.ToDouble(_ds.Tables[tabIdx].Rows[_ds.Tables[tabIdx].Rows.Count - 1][colName]);
            }
            catch { };
            return res;
        }


        public bool OnlyBitSensors
        {
            get
            {
                return SensorsCoutn == BitSensorsCount;
            }
        }

        public bool HasBitSensor
        {
            get
            {
                return BitSensorsCount > 0;
            }
        }

        public int SensorsCoutn
        {
            get
            {
                return _ds.Tables.Count / DB.TABLES_PER_SERIE;
            }
        }

        public int BitSensorsCount
        {
            get
            {
                int result = 0;
                for (int i = DB.RANGES_IN_SERIE; i < _ds.Tables.Count - 2; i += DB.TABLES_PER_SERIE)
                {
                    var configRow = _ds.Tables[i + 2].Rows[0];
                    var isBitSensor = Convert.ToBoolean(configRow[DB.IS_BIT_SENSOR]);
                    if (isBitSensor)
                    {
                        result = result + 1;
                    }
                }
                return result;
            }
        }

    }

}