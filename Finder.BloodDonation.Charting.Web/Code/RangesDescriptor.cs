﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Charting.Web.Code
{
    public static class RangesDescriptor
    {
        public static Dictionary<double, string> GetValues(DB chartData)
        {
            var res = new Dictionary<double, string>();
            double value;
            string desc;


            for (int i = DB.RANGES_IN_SERIE; i < chartData.Ds.Tables.Count - 2; i += DB.TABLES_PER_SERIE)
            {
                var configRow = chartData.Ds.Tables[i + 2].Rows[0];
                var shortcut = configRow[DB.LEGEND_SHORTCUT].ToString();
                var isBitSensor = Convert.ToBoolean(configRow[DB.IS_BIT_SENSOR]);
                var loDesc = configRow[DB.LO_STATE_DESC].ToString();
                var hiDesc = configRow[DB.HI_STATE_DESC].ToString();


                value = chartData.GetMaxRange(i, DB.COL_HI_RANGE);
                if (isBitSensor)
                {
                    desc = shortcut + ": " + hiDesc;
                }
                else
                {
                    desc = shortcut + "max " + Format(value);
                }
                InsertItem(res, value, desc);

                value = chartData.GetMaxRange(i, DB.COL_LOW_RANGE);
                if (isBitSensor)
                {
                    desc = shortcut + ": " + loDesc;
                }
                else
                {
                    desc = shortcut + "min " + Format(value);
                }
                InsertItem(res, value, desc);
            }

            return res;
        }
        
        private static void InsertItem(Dictionary<double, string> d, double key, string value)
        {
            if (d.ContainsKey(key))
            {
                var s = d[key] + @"\n" + value;
                d[key] = s;
            }
            else
            {
                d.Add(key, value);
            }
        }

        private static string Format(double value)
        {
            return value.ToString("0.0");
        }
    }
}
