﻿/*
http://localhost:58304/DeviceChart.ashx?DeviceID=366&From=2013-03-03%2000:00&To=2013-03-04%2023:00&mono=1
*/

using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Configuration;
using log4net.Core;
using log4net;
using System.IO;
using System.Drawing.Drawing2D;
using System.Web.UI.DataVisualization.Charting;
using Finder.BloodDonation.Charting.Web.Code;

namespace Finder.BloodDonation.Charting.Web
{
    public class DeviceChart : IHttpHandler
    {
        private static ILog logger = LogManager.GetLogger(typeof(DeviceChart));

        private Chart c;
        private DB chartData;

        public void ProcessRequest(HttpContext context)
        {
            logger.Debug("Charting url: " + context.Request.QueryString.ToString());
            try
            {
                Configurator.Instance.Initialize(context);

                if (!File.Exists(Configurator.Instance.FileName) || !Configurator.Instance.UseCache)
                {
                    chartData = new DB();
                    c = GetDefChart();

                    MyBorder.Create(c);
                    MySeries.Create(c, chartData);
                    MyLegend.Create(c, chartData);
                    MyAlarms.Create(c, chartData);
                    MyAxis.Create(c, chartData);

                    if (string.IsNullOrEmpty(context.Request.QueryString["Guid"]))
                    {
                        if (!Directory.Exists(
                            Configurator.Instance.Directory
                            ))
                        {
                            Directory.CreateDirectory(Configurator.Instance.Directory);
                        }

                        c.SaveImage(Configurator.Instance.FileName, ChartImageFormat.Png);
                    }

                    //c.SaveImage(@"C:\!Projects\a.png", ChartImageFormat.Png);

                    byte[] buffer;
                    using (MemoryStream mem = new MemoryStream())
                    {
                        c.SaveImage(mem, ChartImageFormat.Png);
                        buffer = mem.ToArray();
                    }

                   context.Response.ContentType = "image/png";
                   context.Response.BinaryWrite(buffer);
                   context.Response.Flush();

                   buffer=null;
                   c=null;
                   chartData = null;
                   
                    GC.Collect();
                   GC.WaitForPendingFinalizers();
                }
                else
                {
                    context.Response.ContentType = "image/png";
                    context.Response.WriteFile(Configurator.Instance.FileName);
                }
                return;
            }
            catch (Exception exc)
            {

                logger.Error(context.Request.RawUrl, exc);
                //throw;
            }

            context.Response.ContentType = "image/pnpg";
            context.Response.WriteFile(context.Server.MapPath("error2.png"));
        }



        public bool IsReusable
        {
            get
            {
                return false;
            }
        }


        private Chart GetDefChart()
        {
            var c = new Chart();
            c.ChartAreas.Add(new ChartArea());
            c.AntiAliasing = AntiAliasingStyles.All;
            c.TextAntiAliasingQuality = TextAntiAliasingQuality.High;
            c.BackColor = Color.Transparent;
            c.BorderSkin.BackColor = Color.Transparent;
            c.BorderSkin.PageColor = Color.Transparent;

            c.Width = Configurator.Instance.Width;
            c.Height = Configurator.Instance.Height;

            return c;
        }

    }


}
