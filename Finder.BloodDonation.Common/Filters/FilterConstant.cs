﻿using System;
using System.Net;


namespace Finder.BloodDonation.Common.Filters
{
    public static class FilterConstant
    {
        public const char AscendingSign = '+';
        public const char DescendingSign = '-';
        public const string SortOrderKey = "SortOrder";
        public const string UnitFromTreeKey = "TreeUnitId";
        public const string UnitsFromTreeKey = "TreeUnitIds";
        public const string PageNumKey = "PageNum";
        public const string PageSizeKey = "PageSize";
    }
}
