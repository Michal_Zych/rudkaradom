﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Common.Filters
{
    public interface IDataTableColumn
    {
        int ColumnNumber { get; set; }
        int Width { get; set; }
        bool IsFixed { get; set; }

        string Name { get; set; }
        string Label { get; set; }
        FilterDataType DataType { get; set; }
        bool IsHidden { get; set; }
        bool IsVisible { get; set; }
        TextAlignment Alignment { get; set; }

        int LastWidth { get; set; }

        IDataTableColumn Copy();
    }
}
