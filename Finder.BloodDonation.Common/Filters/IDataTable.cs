﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using Finder.BloodDonation.Common.Filters;

namespace Finder.BloodDonation.Common.Filters
{
    public interface IDataTable : IFilter
    {
        ObservableCollection<IDataTableColumn> Columns { get; set; }
        ObservableCollection<GridLength> ColumnWidths { get; set; }

        IDataTableColumn this[string propertyName] { get; }
        IDataTableColumn this[int columnIndex] { get; }

        void SaveColumns();

        string SortOrder { get; set; }
        ISortedFilter Filter { get; set; }

        void SetColumnVisibility(string name, bool value);

        void MoveColumn(string name, int direction);

        void SetColumnAlignment(string name, TextAlignment alignment);


        void ClearFilter();
    }
}
