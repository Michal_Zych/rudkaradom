﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Common.Filters
{
    public enum FilterDataType
    {
        @String, @Int, @DateTime, @Bool, @NullableBool, @Short, @Byte, @Long
    };

    public static class FilterDatatTypeHelper
    {
        public static string GetText(this FilterDataType dataType)
        {
            switch (dataType)
            {
                case FilterDataType.String: return "*%_?";
                case FilterDataType.Int: return "*-";
                case FilterDataType.DateTime: return "";
                case FilterDataType.Bool: return "";
                case FilterDataType.NullableBool: return "";
                case FilterDataType.Short: return "*-";
                case FilterDataType.Long: return "*-";
                case FilterDataType.Byte: return "*-";
            }
            return "";
        }
    }

}
