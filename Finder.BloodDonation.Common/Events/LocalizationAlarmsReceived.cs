﻿using System;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Model.Dto;
using System.Collections.Generic;

namespace Finder.BloodDonation.Common.Events
{
    public class LocalizationAlarmsReceived : DynamicApplicationEvent<List<LocalizationAlarmDto>>
    {

    }
}
