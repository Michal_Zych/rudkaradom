﻿using FinderFX.SL.Core.MVVM;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Common.Events
{
    public class ObjectMouseAction
    {
        public MouseActionEnum Action { get; set; }
        public int ObjectID { get; set; }
        public Type Invoker { get; set; }
    }

    public class ObjectMouseActionEvent : DynamicApplicationEvent<ObjectMouseAction>
    {

    }
}
