﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Common.Layout;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Model.Dto;

namespace Finder.BloodDonation.Common.Events
{
    public class SelectedItemChangedEvent<T> : DynamicApplicationEvent<IItemViewModel<T>> where T : IDataTableDto
    {

    }
}
