﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;

namespace Finder.BloodDonation.Common.Events
{
    public class TagInfoReceivedEvent : DynamicApplicationEvent<RfidInfo>
    {

    }

    public class RfidInfo
    {
        public string ActiveTarget { get; set; }
        public string Type { get; set; }
        public long Uid { get; set; }
        public string Content { get; set; }

        public int? UserId { get; set; }

        public override string ToString()
        {
            return String.Format("activetarget={0};type={1};uid={2};content={3}", ActiveTarget, Type, Uid, Content);
        }
    }
}
