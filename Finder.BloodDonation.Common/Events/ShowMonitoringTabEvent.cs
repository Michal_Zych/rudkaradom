﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Model.Dto;
using FinderFX.SL.Core.MVVM;

namespace Finder.BloodDonation.Common.Events
{
    public class MonitoringTab
    {
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
	    public int? UnitId { get; set; }
    }

    public class ShowMonitoringTabEvent : DynamicApplicationEvent<MonitoringTab>
    {

    }
}
