﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Model.Dto;

namespace Finder.BloodDonation.Common.Events
{
    public enum MouseActionEnum
    {
        SET,
        UNSET
    }

    public class UnitMouseAction
    {
        public MouseActionEnum Action { get; set; }
        public int UnitID { get; set; }
        public Type Invoker { get; set; }
    }

    public class UnitMouseActionEvent : DynamicApplicationEvent<UnitMouseAction>
    {

    }
}
