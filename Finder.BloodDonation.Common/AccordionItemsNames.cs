﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Common
{
    public class AccordionItemsNames
    {
        public const string UNITS = "units";
        public const string ADMINISTRATION = "admin";
        public const string ARCHIVE = "archive";
        public const string TOOLS = "tools";
    }
}
