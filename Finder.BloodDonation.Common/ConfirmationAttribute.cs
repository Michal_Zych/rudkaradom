﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using PostSharp.Aspects;

namespace Finder.BloodDonation.Common
{
	public class ConfirmationAttribute : OnMethodBoundaryAspect
	{
		public string Caption { get; set; }
		public string Message { get; set; }

		public ConfirmationAttribute()
		{
			Caption = "Potwierdzenie";
			Message = "Czy jesteś pewien?";
		}

        public override void OnEntry(MethodExecutionArgs eventArgs)
		{
			MessageBoxResult result = MessageBox.Show(
				Message,
				Caption,
				MessageBoxButton.OKCancel);

			if (result != MessageBoxResult.OK)
			{
				eventArgs.FlowBehavior = FlowBehavior.Return;
			}
		}
	}
}
