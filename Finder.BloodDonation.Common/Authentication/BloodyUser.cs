﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.Authentication;
using System.Collections.Generic;
using FinderFX.SL.Core.Configuration;
using FinderFX.SL.Core.Communication;
using System.IO;
using Finder.BloodDonation.Model.Dto;
using System.Linq;
using Finder.BloodDonation.Model.Enum;
using System.Text;
using Finder.BloodDonation.Model.Authentication;

namespace Finder.BloodDonation.Common.Authentication
{
    public class BloodyUser : User
    {
        public static BloodyUser Current
        {
            get
            {
                return User.Current as BloodyUser;
            }
        }

        //User IsInRole - nie sprawdza roli bazodanowej tylko Permission
        public bool IsInRoles(IEnumerable<string> roles)
        {
            foreach (var s in roles)
            {
                if (IsInRole(s))
                {
                    return true;
                }
            }
            return false;
        }


        public string SerializedSettings { get; private set; }

        private readonly int FAVORITE_ID = -1;

        Dictionary<string, List<string>> _rolesDictionary;
        Dictionary<string, List<string>> _permissionsDictionary;

        internal Dictionary<string, List<string>> GetRolesDictionary()
        {
            return _rolesDictionary;
        }

        internal Dictionary<string, List<string>> GetPermissionsDictionary()
        {
            return _permissionsDictionary;
        }

        private Dictionary<string, string> _customProperites = new Dictionary<string, string>();

        public void SetPermissions(Dictionary<string, List<string>> rolesDictionary, Dictionary<string, List<string>> permissionsDictionary)
        {
            _rolesDictionary = rolesDictionary;
            _permissionsDictionary = permissionsDictionary;
        }


        public bool HasPermission(int unitID, string permission)
        {
            if (unitID == FAVORITE_ID)
                return true;

            if (_permissionsDictionary.ContainsKey(unitID.ToString()))
            {
                List<string> roles = _permissionsDictionary[unitID.ToString()];

                foreach (string r in roles)
                {
                    if (_rolesDictionary[r].Contains(permission))
                        return true;
                }
            }

            return false;
        }
        public bool CanSeeAllAlarms
        {
            get
            {
                return IsInRole(PermissionNames.CanSeeAllAlarms);
            }
        }
        public BloodyUser(bool isAuthenticated, string name, string[] roles, string[] rfidStatuses, int id, string passwordHash, int clientID, string theme, string skin)
            : base(isAuthenticated, name, roles, id, passwordHash, clientID, theme, skin, name, id, rfidStatuses)
        {

        }

        /// <summary>
        /// Odświeżenie uprawnień użytkownika.
        /// </summary>
        public void ReloadPermissions()
        {
            ExtendedWebClient client = new ExtendedWebClient();
            client.OpenReadCompleted += new OpenReadCompletedEventHandler(client_OpenReadCompleted);
            
            client.OpenReadAsync(
                new Uri("Auth_reload.html", UriKind.Relative)
                , this);
        }

        void client_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        {
            StreamReader reader = new StreamReader(e.Result);
            string userJsonString = reader.ReadToEnd();
            reader.Close();

            AuthenticationManager authManager = GlobalConfiguration.Current.AuthenticationManager as AuthenticationManager;
            if (authManager == null)
                throw new InvalidOperationException("Błąd podczas odświeżania uprawnień użytkownika.");

            BloodyUser user = authManager.DeserializeUser(userJsonString) as BloodyUser;
            if (user == null)
                throw new InvalidOperationException("Błąd podczas odświeżania uprawnień użytkownika.");
            ;
            this.SetPermissions(
                user.GetRolesDictionary(),
                user.GetPermissionsDictionary());
        }

        public string GetCustomProperty(string key)
        {
            if (_customProperites.ContainsKey(key))
                return _customProperites[key];

            return null;
        }

        public void SetCustomProperty(string key, string value)
        {
            if (_customProperites.ContainsKey(key))
            {
                _customProperites[key] = value;
            }
            else
            {
                _customProperites.Add(key, value);
            }

            SerializeCustomProperties();
        }

        private void SerializeCustomProperties()
        {
            var sb = new StringBuilder();

            foreach (var p in _customProperites)
            {
                if (p.Key != "ServicesDomains")
                {
                    sb.Append(p.Key).Append("=").Append(p.Value).Append(";");
                }
            }

            SerializedSettings = sb.ToString();
        }
    }
}
