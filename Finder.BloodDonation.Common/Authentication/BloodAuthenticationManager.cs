﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.Authentication;
using System.Collections.Generic;
using Finder.BloodDonation.Model.Dto;

namespace Finder.BloodDonation.Common.Authentication
{
    public class BloodAuthenticationManager : AuthenticationManager
    {
        private readonly string MAIN_SEPARATOR = "%^&";

        public override User DeserializeUser(string userString)
        {
            string[] parts = userString.Split(new string[] { MAIN_SEPARATOR }, StringSplitOptions.None);

            string userStr = parts[0];
           

            UserDTO userDTO = JsonHelper.Deserialize<UserDTO>(userStr);
            User user = User.Anonymous;
            if (userDTO.Identity.IsAuthenticated == true)
            {
                BloodyUser userTemp = new BloodyUser(
                    userDTO.Identity.IsAuthenticated,
                    userDTO.Identity.Name,
                    userDTO.Roles.ToArray(),
                    userDTO.RfidStatuses.ToArray(),
                    userDTO.ID,
                    //domylsnie haslo nie jest przesylane w DTO, jezeli jest 
                    //tzn ze jesty to logowanie uzytkownika domyslnego
					string.IsNullOrEmpty(userDTO.PasswordHash) ? User.Current.Auth_PasswordHash : userDTO.PasswordHash,
                    userDTO.ClientID,
                    userDTO.Theme,
                    userDTO.Skin);

				if (parts.Length == 4)
				{
					string rolesString = parts[1];
					string permissionsString = parts[2];
					string customProperitesString = parts[3];

					Dictionary<string, List<string>> rolesDictionary = JsonHelper.Deserialize<Dictionary<string, List<string>>>(rolesString);
					Dictionary<string, List<string>> permissionsDictionary = JsonHelper.Deserialize<Dictionary<string, List<string>>>(permissionsString);

					Dictionary<string, string> customProperites = JsonHelper.Deserialize<Dictionary<string, string>>(customProperitesString);


					userTemp.SetPermissions(rolesDictionary, permissionsDictionary);
					userTemp.SetCustomProperites(customProperites);
                    foreach (string key in customProperites.Keys)
                    {
                        if (!key.StartsWith("_"))
                        {
                            userTemp.SetCustomProperty(key, customProperites[key]);
                        }
                    }

				}

                user = userTemp;
            }

            return user;
        }
    }
}
