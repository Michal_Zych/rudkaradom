﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Common.Layout
{
    public static class ParentVisualStates
    {
        public static readonly DependencyProperty CurrentStateProperty =
         DependencyProperty.RegisterAttached("CurrentState", typeof(String), typeof(ParentVisualStates), new PropertyMetadata(TransitionToState));

        public static string GetCurrentState(DependencyObject obj)
        {
            return (string)obj.GetValue(CurrentStateProperty);
        }

        public static void SetCurrentState(DependencyObject obj, string value)
        {
            obj.SetValue(CurrentStateProperty, value);
        }

        private static void TransitionToState(object sender, DependencyPropertyChangedEventArgs args)
        {
            FrameworkElement fe = sender as FrameworkElement;
            if (fe != null)
            {
                Control c = VisualTreeHelper.GetParent(fe) as Control;

                if (c != null)
                {
                    VisualStateManager.GoToState(c, (string)args.NewValue, true);
                }
                else
                {
                    Control fe_c = VisualTreeHelper.GetParent(fe.Parent) as Control;
                    if (fe_c != null)
                    {
                        VisualStateManager.GoToState(fe_c, (string)args.NewValue, true);
                        //throw new ArgumentException("CurrentState is only supported on the Control type");
                    }
                }
            }
        }
    }
}
