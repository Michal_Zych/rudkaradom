﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Common.Layout
{
    public static class BloodIdMapping
    {
        //column Type1 and BloodType
        static Dictionary<int, string> IdTypes = new Dictionary<int,string>(){
                {12,"A-"},
                {13,"A+"},
                {14,"B-"},
                {15,"B+"},
                {16,"AB-"},
                {17,"AB+"},
                {18,"0-"},
                {19,"0+"},
                {22,"A"},
                {23,"B"},
                {24,"AB"},
                {25,"0"},

                //platelets
                {31,"A-"},
                {32,"A+"},
                {33,"B-"},
                {34,"B+"}, 
                {35,"AB-"}, 
                {36,"AB+"},
                {37,"0-"}, 
                {38,"0+"},

                //products and reagents
                {29, "Catalyzer"},
                {30, "Inhibitor"}
        };

        public static string GetBloodType(int id)
        {
            if (IdTypes.ContainsKey(id))
                return IdTypes[id];
            return null;
        }
    }
}
