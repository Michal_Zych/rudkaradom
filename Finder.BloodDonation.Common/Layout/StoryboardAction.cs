﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Common.Layout
{
    public static class StoryboardAction
    {
        public static readonly DependencyProperty CurrentStateProperty =
         DependencyProperty.RegisterAttached("CurrentState", typeof(String), typeof(VisualStates), new PropertyMetadata(TransitionToState));

        public static string GetCurrentState(DependencyObject obj)
        {
            return (string)obj.GetValue(CurrentStateProperty);
        }

        public static void SetCurrentState(DependencyObject obj, string value)
        {
            obj.SetValue(CurrentStateProperty, value);
        }

        private static void TransitionToState(object sender, DependencyPropertyChangedEventArgs args)
        {
            FrameworkElement fe = sender as FrameworkElement;
            if (fe != null)
            {
                if (fe.Resources != null && fe.Resources.Count > 0 && fe.Resources.Contains((string)args.NewValue))
                {
                    Storyboard sb = fe.Resources[(string)args.NewValue] as Storyboard;

                    sb.Begin();
                }
            }
        }
    }
}
