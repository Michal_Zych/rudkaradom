﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Common.Layout
{
	public enum ViewModelState
	{
		Loaded,
		Loading,
		Normal
	}

	public interface ITabViewModel
	{
		void Refresh(IUnit unit);
		event EventHandler LoadingCompleted;
		ViewModelState State { get; }
		object View { get; }

        string Title { get; }
        bool ExitButtonVisible { get; }
    }
}
