﻿using Finder.BloodDonation.Common.Filters;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Model.Dto;

namespace Finder.BloodDonation.Common.Layout
{
    public interface IItemViewModel<DTO> where DTO : IDataTableDto
    {
        DTO Item { get; set; }
        IDataTable DataTable { get; set; }
    }
}
