﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Common.Layout
{
    public class TabNames
    {
        public const string AlarmsList = "AlarmsList";
        public const string Reports = "Reports";
        
        public const string Configuration = "Configuration";
        public const string DiagnosticsData = "DiagnosticsData";
        public const string HubsCommunicationLogs = "HubsCommunicationLogs";
		public const string HubsList = "HubsList";
		public const string HubDetails = "HubDetails";
        public const string PreviewEdit = "PreviewEdit";

        public const string UnitPlan = "UnitPlan";


        //Not implemented tab
        public const string NotImplemented = "Jeszcze nie oprogramowana";


        //Unit
        public const string Patients = "Pacjenci";
        public const string Monitoring = "Monitorowanie";
        public const string Localization ="Lokalizacja";
        public const string Events = "Zdarzenia";
        public const string LocalizationReports = "Raporty lokalizacyjne";
        public const string LocalizationConfig = "Konfiguracja lokalizacji";
        public const string LocalizationPlan = "Planowanie lokalizacji";
        public const string Transmitters = "Transmitery";
        public const string Bands = "Lokalizatory";

        //Administracji
        public const string Permissions = "Personel";
        public const string Messages = "Komunikaty";
        public const string Settings = "Konfiguracja systemu";
        public const string LoggedUsers = "Zalogowani użytkownicy";
        public const string Logs = "Zdarzenia systemowe";
        public const string ServiceStatus = "Podsystemy";
        public const string Versions = "Wersje";

        //Narzędzia
        public const string ToolTransmitters = "Transmitery ";
        public const string ToolBands = "Lokalizatory ";
        public const string ExternalSystems = "Systemy zewnętrzne";
        public const string Asseco = "Asseco";
        public const string AssecoOut = "Wysyłanie";
        public const string AssecoIn = "Odbiór";
        public const string AssecoMapping = "Mapowanie";
        public const string Backup = "Kopie bezpieczeństwa";

        //Archiwum
        public const string ArchivePatients = "Dane archiwalne";



        public static string ExternalEvents = "Zdarzenia zewnętrzne";
    }
}
