﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Events;

namespace Finder.BloodDonation.Common.Layout
{
	public interface ITab : IDisposable
	{
		object Header { get; }
		string Name { get; }
		ITabViewModel TabViewModel { get; set; }
		event EventHandler ContentLoaded;
		string RequiredPermission { get; }
	}
}
