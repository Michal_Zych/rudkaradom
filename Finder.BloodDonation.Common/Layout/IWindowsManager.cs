﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Common.Layout
{
    public interface IWindowsManager
    {
        void RegisterWindow(string key, string name, UserControl form, Size size, bool isModal, bool canResize = true);
        void RegisterWindowIfNotRegister(string key, string name, UserControl form, Size size, bool isModal, bool canResize = true);
        void CloseWindow(string key);
        void ShowWindow(string key);
        void ShowWindow(string key, object data);
        void ShowWindow(string key, object data, bool? editMode);
        bool CheckIfWindowIsShowed(string key);
    }
}