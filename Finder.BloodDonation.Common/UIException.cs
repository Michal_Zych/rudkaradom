﻿#define DEBUG

using System;
using DanielVaughan.Logging;
using System.Collections.Generic;
using FinderFX.SL.Core.Authentication;
using PostSharp.Aspects;

namespace Finder.BloodDonation.Common
{
    public class UIException : OnMethodBoundaryAspect
    {
        public override void OnEntry(MethodExecutionArgs event_args)
        {
            //Logging.SetParameter("method_name", event_args.Method.Name);
            //Logging.SetParameter("class_name", event_args.Instance.GetType().ToString());
            //Logging.PushContext(event_args.MethodExecutionTag);
            //Logging.SetParameter("parameters", ParametersToString(event_args));
            //Logging.Info("Called " + event_args.Method);
        }

        public override void OnExit(MethodExecutionArgs event_args)
        {
            //Logging.SetParameter("parameters", ParametersToString(event_args));
            ///Logging.SetParameter("method_name", event_args.Method.Name);
            //Logging.SetParameter("class_name", event_args.Instance.GetType().ToString());
            //Logging.Info("Finished " + event_args.Method);
        }

        public override void OnException(MethodExecutionArgs event_args)
        {
            ILog log = LogManager.GetLog(event_args.Instance.GetType());
            Dictionary<string, object> properties = new Dictionary<string, object>();
            properties.Add("parameters", ParametersToString(event_args));
            properties.Add("method_name", event_args.Method.Name);
            properties.Add("class_name", event_args.Instance.GetType().ToString());
            properties.Add("current_user_id", User.Current.ID);
            //properties.Add("current_user_displayname", User.Current.Properties.Get<string>(UserProperties.DisplayName));
            log.Error("", event_args.Exception, properties);

            // polykanie wyjatkow tylko na produkcji (w release)
            #if DEBUG
            event_args.FlowBehavior = FlowBehavior.RethrowException;
            #else
            event_args.FlowBehavior = FlowBehavior.Continue;
            #endif
        }

        //helpers
        private static String ParametersToString(MethodExecutionArgs event_args)
        {
            String output = "";
            if (event_args.Arguments != null)
            {
                for (int i = 0; i < event_args.Arguments.Count; i++)
                {
                    output += String.Format("[{0} = {1}]", event_args.Method.GetParameters()[i].Name, event_args.Arguments[i]);
                }
            }
            return output;
        }
    }
}
