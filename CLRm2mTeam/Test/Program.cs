﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using m2mTeam.Monitoring.CLR;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            int sensor = 1;
            DateTime dateFrom = new DateTime(2014, 3, 14, 9, 0, 0);
            DateTime dateTo = new DateTime(2014, 3, 15, 5, 11, 0);
            int mode = 2;
            int rowCount = 800;

            crt_GeneralizedSensorData(sensor, dateFrom, dateTo, mode, rowCount);
        }


        private const string SelectCmdString = @"EXEC dbo.crt_GetSensorData @sensorID = {1}, @dateFrom = N'{2}', @dateTo = N'{3}'";

        public static void crt_GeneralizedSensorData(int? sensor, DateTime? dateFrom, DateTime? dateTo,
            int? mode, int? rowCount)
        {
            var generalizationMode = GeneralizationMode.NoNulls;
            if (mode.Value == 1)
                generalizationMode = GeneralizationMode.ConsiderNulls;
            if (mode.Value == 2)
                generalizationMode = GeneralizationMode.ShowNullsRanges;



            DataTable inTable = new DataTable();

            using (SqlConnection connection = new SqlConnection(ConnectionString()))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("crt_GetSensorData", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("sensorID", sensor.Value));
                command.Parameters.Add(new SqlParameter("dateFrom", dateFrom.Value));
                command.Parameters.Add(new SqlParameter("dateTo", dateTo.Value));

                using (command)
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        inTable.Load(reader);
                    }
                }
            }


            Console.WriteLine("odczytano wierszy: " + inTable.Rows.Count.ToString());
            
            var generalizator = new TableGeneralizator(inTable, rowCount.Value, GeneralizatorFactory.GetGeneralizator(generalizationMode));

            var result = generalizator.Execute();

            Console.WriteLine("po przetworzeniu: " + result.Rows.Count.ToString());
            Console.ReadLine();
        }



        public static string ConnectionString()
        {
            return ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["DatabaseConnection"]].ConnectionString;
        }
    }
}
