﻿using m2mTeam.Monitoring.CLR;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;

namespace TestMeasureArray
{
    class Program
    {
        static void Main(string[] args)
        {
            int UserReportId = 7;
            DateTime dateFrom = new DateTime(2014, 3, 14, 9, 0, 0);
            DateTime dateTo = new DateTime(2014, 3, 15, 5, 11, 0);
           


            rep_MeasureArray(UserReportId, 1);

        }


        public static void rep_MeasureArray(SqlInt32 UserReportID, SqlInt32 range)
        {
            DataTable dtData = new DataTable();

            using (SqlConnection connection = new SqlConnection(ConnectionString()))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("rep_GetMeasureArrayData", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("UserReportID", UserReportID.Value));

                using (command)
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        dtData.Load(reader);
                    }
                }
            }
            Console.WriteLine("odczytano wierszy: " + dtData.Rows.Count.ToString());

            MeasureArrayLogic logic = new MeasureArrayLogic();
            DataTable result = logic.Execute(range.Value, dtData);

            Console.WriteLine("po przetworzeniu: " + result.Rows.Count.ToString());
            Console.WriteLine("struktura dla range = " + range.ToString());
            foreach (var i in result.Columns)
            {
                Console.Write(i.ToString() + " | ");
            }
            Console.ReadLine();
        }


        
        public static string ConnectionString()
        {
            return ConfigurationManager.ConnectionStrings[ConfigurationManager.AppSettings["DatabaseConnection"]].ConnectionString;
        }
    }
}
