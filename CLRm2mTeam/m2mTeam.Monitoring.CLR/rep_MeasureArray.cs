using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using m2mTeam.Monitoring.CLR;

public partial class StoredProcedures
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void rep_MeasureArray(SqlInt32 UserReportID, SqlInt32 range)
    {
        DataTable dtData = new DataTable();

        using (SqlConnection connection = new SqlConnection("context connection=true;"))
        {
            connection.Open();

            SqlCommand command = new SqlCommand("rep_GetMeasureArrayData", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("UserReportID", UserReportID.Value));

            using (command)
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    dtData.Load(reader);
                }
            }
        }

        MeasureArrayLogic logic = new MeasureArrayLogic();
        DataTable result = logic.Execute(range.Value, dtData);

        SendDataTableOverPipe(result);
    }
}
