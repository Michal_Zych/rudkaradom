using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace m2mTeam.Monitoring.CLR
{
    public class MeasureArrayLogic
    {
        private const string SENSOR_ID = "SensorID";
        private const string DATEDAY = "Date";
        private const string MONTH = "Month";
        private const string DAY = "Day";
        private const string HOUR = "Hour";

        private const string TIMESTAMP = "MeasurementTime";
        

        private const string VALUE = "Value";
        /*
        public DataTable ExecuteByMnutes(int range, DataTable dt_data)
        {
            DataTable dt_result = new DataTable();
            dt_result.Columns.Add(DEVICE_ID, typeof(int));
            dt_result.Columns.Add(MONTH, typeof(int));
            dt_result.Columns.Add(DAY, typeof(int));
            dt_result.Columns.Add(HOUR, typeof(int));

            int i = 0;
            do
            {
                dt_result.Columns.Add("T0" + i + "h", typeof(decimal));
                dt_result.Columns.Add("T1" + i + "h", typeof(decimal));
                i = i + 5;
            } while (i < 60);
            dt_result.Columns.Add(CHART_URL, typeof(string));

            try
            {
                if (dt_data != null && dt_data.Rows.Count > 0)
                {
                    int current_device_id = 0;
                    int current_year = 0;
                    int current_month = 0;
                    int current_day = 0;
                    int current_minutes = 0;


                    double current_sum_t0 = 0;
                    double current_sum_t1 = 0;
                    int current_count_t0 = 0;
                    int current_count_t1 = 0;
                    string current_chart_url = string.Empty;
                    DataRow current_row = null;
                    int current_hour = 0;
                    DateTime date_from = (DateTime)dt_data.Rows[0]["DateFrom"];
                    DateTime date_to = (DateTime)dt_data.Rows[0]["DateTo"];

                    for (i = 0; i < dt_data.Rows.Count; i++)
                    {
                        int device_id = (int)dt_data.Rows[i][DEVICE_ID];
                        DateTime timestamp = (DateTime)dt_data.Rows[i][TIMESTAMP];
                        int minutes = (int)(Math.Floor((double)timestamp.Minute / (double)range) * range);
                        int hour = ((DateTime)dt_data.Rows[i][TIMESTAMP]).Hour;

                        if (device_id != current_device_id) //zmieniamy device
                        {
                            //4chart hack
                            if (current_device_id > 0)
                            {
                                //missing days for month and last device
                                AddMissingDays(dt_result, current_day, current_device_id, current_month, 32, current_chart_url);
                            }

                            current_device_id = device_id;
                            current_chart_url = GetChartByMinutesURL(current_device_id, timestamp);
                            current_year = timestamp.Year;
                            current_month = timestamp.Month;
                            current_day = timestamp.Day;
                            current_minutes = minutes;
                            current_hour = hour;
                            current_count_t0 = 0;
                            current_count_t1 = 0;
                            current_sum_t0 = 0;
                            current_sum_t1 = 0;

                            AddMissingDays(dt_result, 0, current_device_id, current_month, current_day, current_chart_url);

                            current_row = AddDeviceDayRow(dt_result, current_device_id, current_month, current_day, current_hour, current_chart_url);

                            UpdateRowMinutes(current_row, current_minutes, Convert.ToDouble(dt_data.Rows[i][T0]), Convert.ToDouble(dt_data.Rows[i][T1]), ref current_sum_t0, ref current_sum_t1, ref current_count_t0, ref current_count_t1);
                        }
                        else
                        {
                            //analizujemy ten sam dzie�
                            if (current_year == timestamp.Year && current_month == timestamp.Month && current_day == timestamp.Day && current_hour == timestamp.Hour && current_minutes != minutes)
                            {
                                current_minutes = minutes;
                                current_count_t0 = 0;
                                current_count_t1 = 0;
                                current_sum_t0 = 0;
                                current_sum_t1 = 0;
                                UpdateRowMinutes(current_row, current_minutes, Convert.ToDouble(dt_data.Rows[i][T0]), Convert.ToDouble(dt_data.Rows[i][T1]), ref current_sum_t0, ref current_sum_t1, ref current_count_t0, ref current_count_t1);
                            }
                            else if (current_year == timestamp.Year && current_month == timestamp.Month && current_day == timestamp.Day && current_hour == timestamp.Hour && current_minutes == minutes)
                            {
                                UpdateRowMinutes(current_row, current_minutes, Convert.ToDouble(dt_data.Rows[i][T0]), Convert.ToDouble(dt_data.Rows[i][T1]), ref current_sum_t0, ref current_sum_t1, ref current_count_t0, ref current_count_t1);
                            }
                            // analizujemy inna godzine
                            else if (current_year == timestamp.Year && current_month == timestamp.Month && current_day == timestamp.Day && current_hour != timestamp.Hour)
                            {
                                //missing days for current month
                                AddMissingDays(dt_result, current_day, current_device_id, current_month, timestamp.Day, current_chart_url);

                                current_minutes = minutes;
                                current_hour = hour;
                                current_count_t0 = 0;
                                current_count_t1 = 0;
                                current_sum_t0 = 0;
                                current_sum_t1 = 0;

                                //new day
                                current_chart_url = GetChartByMinutesURL(current_device_id, timestamp);
                                current_row = AddDeviceDayRow(dt_result, current_device_id, current_month, current_day, current_hour, current_chart_url);
                                UpdateRowMinutes(current_row, current_minutes, Convert.ToDouble(dt_data.Rows[i][T0]), Convert.ToDouble(dt_data.Rows[i][T1]), ref current_sum_t0, ref current_sum_t1, ref current_count_t0, ref current_count_t1);
                            }
                            //analizujemy inny dzie�
                            else if (current_year == timestamp.Year && current_month == timestamp.Month && current_day != timestamp.Day)
                            {
                                //missing days for current month
                                AddMissingDays(dt_result, current_day, current_device_id, current_month, timestamp.Day, current_chart_url);

                                current_minutes = minutes;
                                current_day = timestamp.Day;
                                current_hour = timestamp.Hour;
                                current_count_t0 = 0;
                                current_count_t1 = 0;
                                current_sum_t0 = 0;
                                current_sum_t1 = 0;

                                //new day
                                current_chart_url = GetChartByMinutesURL(current_device_id, timestamp);
                                current_row = AddDeviceDayRow(dt_result, current_device_id, current_month, current_day, current_hour, current_chart_url);
                                UpdateRowMinutes(current_row, current_minutes, Convert.ToDouble(dt_data.Rows[i][T0]), Convert.ToDouble(dt_data.Rows[i][T1]), ref current_sum_t0, ref current_sum_t1, ref current_count_t0, ref current_count_t1);
                            }
                            //przeskakujemy do innego miesi�ca/roku
                            else
                            {
                                if (current_year != timestamp.Year || current_month != timestamp.Month)
                                {
                                    AddMissingDays(dt_result, current_day, current_device_id, current_month, 32, current_chart_url);

                                    current_year = timestamp.Year;
                                    current_month = timestamp.Month;
                                    current_day = timestamp.Day;
                                    current_hour = hour;
                                    current_count_t0 = 0;
                                    current_count_t1 = 0;
                                    current_sum_t0 = 0;
                                    current_sum_t1 = 0;

                                    current_chart_url = GetChartByMinutesURL(current_device_id, timestamp);

                                    //new day
                                    current_row = AddDeviceDayRow(dt_result, current_device_id, current_month, current_day, current_hour, current_chart_url);
                                    UpdateRowMinutes(current_row, current_minutes, Convert.ToDouble(dt_data.Rows[i][T0]), Convert.ToDouble(dt_data.Rows[i][T1]), ref current_sum_t0, ref current_sum_t1, ref current_count_t0, ref current_count_t1);
                                }
                            }
                        }
                    }

                    AddMissingDays(dt_result, current_day, current_device_id, current_month, 32, current_chart_url);
                }
            }
            catch (Exception exc)
            {

            }

            return dt_result;
        }
        */
        public DataTable Execute(int range, DataTable dt_data)
        {
            DataTable dt_result = new DataTable();
            dt_result.Columns.Add(SENSOR_ID, typeof(short));
            dt_result.Columns.Add(DATEDAY, typeof(DateTime));

            int i = 0;
            do
            {
                dt_result.Columns.Add(VALUE + i + "h", typeof(short));
                i = i + range;
            } while (i < 24);
            try
            {
                if (dt_data != null && dt_data.Rows.Count > 0)
                {
                    short current_sensor_id = 0;
                    int current_year = 0;
                    int current_month = 0;
                    int current_day = 0;
                    long current_sum_value = 0;
                    int current_count_value = 0;
                    DataRow current_row = null;
                    byte current_hour = 0;

                    for (i = 0; i < dt_data.Rows.Count; i++)
                    {
                        short sensor_id = (short)dt_data.Rows[i][SENSOR_ID];
                        DateTime timestamp = (DateTime)dt_data.Rows[i][TIMESTAMP];
                        byte hour = (byte)(Math.Floor((double)timestamp.Hour / (double)range) * range);

                        if (sensor_id != current_sensor_id) //zmieniamy sensor
                        {
                            //4chart hack
                            if (current_sensor_id > 0)
                            {
                                //missing days for month and last device
                                AddMissingDays(dt_result, current_year, current_month, current_day, current_sensor_id, 32);
                            }

                            current_sensor_id = sensor_id;
                            current_year = timestamp.Year;
                            current_month = timestamp.Month;
                            current_day = timestamp.Day;
                            current_hour = hour;
                            current_count_value = 0;
                            current_sum_value = 0;

                            AddMissingDays(dt_result, current_year, current_month, current_day, current_sensor_id, 0);

                            current_row = AddSensorDayRow(dt_result, current_sensor_id, current_year, current_month, current_day);

                            UpdateRowHour(current_row, current_hour, (short)dt_data.Rows[i][VALUE], ref current_sum_value, ref current_count_value);
                        }
                        else
                        {
                            //analizujemy ten sam dzie�
                            if (current_year == timestamp.Year && current_month == timestamp.Month && current_day == timestamp.Day && current_hour != hour)
                            {
                                current_hour = hour;
                                current_count_value = 0;
                                current_sum_value = 0;
                                UpdateRowHour(current_row, current_hour, (short)(dt_data.Rows[i][VALUE]), ref current_sum_value, ref current_count_value);
                            }
                            else if (current_year == timestamp.Year && current_month == timestamp.Month && current_day == timestamp.Day && current_hour == hour)
                            {
                                UpdateRowHour(current_row, current_hour, (short)(dt_data.Rows[i][VALUE]), ref current_sum_value, ref current_count_value);
                            }
                            //analizujemy inny dzie�
                            else if (current_year == timestamp.Year && current_month == timestamp.Month && current_day != timestamp.Day)
                            {
                                //missing days for current month
                                AddMissingDays(dt_result, current_year, current_month, current_day, current_sensor_id, timestamp.Day);

                                current_day = timestamp.Day;
                                current_hour = hour;
                                current_count_value = 0;
                                current_sum_value = 0;

                                //new day
                                current_row = AddSensorDayRow(dt_result, current_sensor_id, current_year, current_month, current_day);
                                UpdateRowHour(current_row, current_hour, (short)(dt_data.Rows[i][VALUE]), ref current_sum_value, ref current_count_value);
                            }
                            //przeskakujemy do innego miesi�ca/roku
                            else
                            {
                                if (current_year != timestamp.Year || current_month != timestamp.Month)
                                {
                                    AddMissingDays(dt_result, current_year, current_month, current_day, current_sensor_id, 32);

                                    current_year = timestamp.Year;
                                    current_month = timestamp.Month;
                                    current_day = timestamp.Day;
                                    current_hour = hour;
                                    current_count_value = 0;
                                    current_sum_value = 0;


                                    //new day
                                    current_row = AddSensorDayRow(dt_result, current_sensor_id, current_year, current_month, current_day);
                                    UpdateRowHour(current_row, current_hour, (short)(dt_data.Rows[i][VALUE]), ref current_sum_value, ref current_count_value);
                                }
                            }
                        }
                    }

                    AddMissingDays(dt_result, current_year, current_month, current_day, current_sensor_id, 32);
                }
            }
            catch (Exception exc)
            {

            }

            return dt_result;
        }


        private DateTime GetLastDayMonthDate(DateTime dt)
        {
            DateTime d = dt;
            d = d.AddMonths(1);
            d = d.AddDays(-(d.Day));
            return d;
        }

        private void UpdateRowMinutes(DataRow current_row, int current_minutes, double t0, double t1, ref  double sum_t0, ref double sum_t1, ref int count_t0, ref int count_t1)
        {
            count_t0++;
            count_t1++;
            sum_t0 += t0;
            sum_t1 += t1;
            current_row["T0" + current_minutes + "h"] = sum_t0 / count_t0;
            current_row["T1" + current_minutes + "h"] = sum_t1 / count_t1;
        }

        private void UpdateRowHour(DataRow current_row, byte current_hour, short value, ref long sum_value, ref int count_value)
        {
            count_value++;
            sum_value += value;
            current_row[VALUE + current_hour + "h"] = sum_value / count_value;
        }

        private void AddMissingDays(DataTable dt_result, int current_year, int current_month, int current_day, short current_sensor_id, int to_days)
        {
            if (current_month == 0)
            {
                current_month = 1;
            }

            if (current_sensor_id > 0)
            {
                //for (int m = current_day + 1; m < to_days; m++)
                {
                    //missing days in current month
                    //AddDeviceDayRow(dt_result, current_device_id, current_month, m, chart);
                }
            }
        }

        private DataRow AddSensorDayRow(DataTable dt, short sensor, int year, int month, int day)
        {
            DataRow row = dt.NewRow();

            row[SENSOR_ID] = sensor;
            row[DATEDAY] = new DateTime(year, month, day);

            dt.Rows.Add(row);

            return row;
        }

        private DataRow AddSensorDayRow(DataTable dt, short sensor, int year, int month, int day, int hour)
        {
            DataRow row = dt.NewRow();

            row[SENSOR_ID] = sensor;
            row[DATEDAY]= new DateTime(year, month, day);
            row[HOUR] = hour;

            dt.Rows.Add(row);

            return row;
        }
    }
}
