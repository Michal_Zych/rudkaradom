//------------------------------------------------------------------------------
// <copyright file="CSSqlStoredProcedure.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Collections.Generic;
using m2mTeam.Monitoring.CLR;

public partial class StoredProcedures
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void crt_GeneralizedSensorData(SqlInt16 sensor, SqlDateTime dateFrom, SqlDateTime dateTo,
            SqlInt32 mode, SqlInt32 rowCount)
    {
        if (sensor == SqlInt16.Null || dateFrom == SqlDateTime.Null || dateTo == SqlDateTime.Null)
            return;

        var generalizationMode = GeneralizationMode.NoNulls;
        if (mode.Value == 1)
            generalizationMode = GeneralizationMode.ConsiderNulls;
        if (mode.Value == 2)
            generalizationMode = GeneralizationMode.ShowNullsRanges;

        if (rowCount == SqlInt32.Null)
            rowCount = 1000;


        DataTable inTable = new DataTable();

        using (SqlConnection connection = new SqlConnection("context connection=true;"))
        {
            connection.Open();

            SqlCommand command = new SqlCommand("crt_GetSensorData", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("sensorID", sensor.Value));
            command.Parameters.Add(new SqlParameter("dateFrom", dateFrom.Value));
            command.Parameters.Add(new SqlParameter("dateTo", dateTo.Value));

            using (command)
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    inTable.Load(reader);
                }
            }
        }

        var generalizator = new TableGeneralizator(inTable, rowCount.Value, GeneralizatorFactory.GetGeneralizator(generalizationMode));

        var result = generalizator.Execute();
        SendDataTableOverPipe(result);
    }

    

    


}
