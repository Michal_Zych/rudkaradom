//------------------------------------------------------------------------------
// <copyright file="CSSqlClassFile.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace m2mTeam.Monitoring.CLR
{
    public interface IRangeGeneralizator
    {
        void InsertGeneralizedRows(DataTable inTable, int rangeStart, int rangeEnd, DataTable outTable);
        DataTable GetGeneralizedTable(DataTable inTable);
    }
}
