//------------------------------------------------------------------------------
// <copyright file="CSSqlClassFile.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace m2mTeam.Monitoring.CLR
{
    public static class DatesCalculator
    {
        public static DateTime MidDateTime(DateTime start, DateTime end)
        {
            if (start > end)
            {
                var d = start;
                start = end;
                end = d;
            }

            TimeSpan range = end - start;
            range = TimeSpan.FromMilliseconds(range.TotalMilliseconds / 2);

            return start + range;
        }
    }
}
