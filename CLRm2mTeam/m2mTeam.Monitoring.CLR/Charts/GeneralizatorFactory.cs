//------------------------------------------------------------------------------
// <copyright file="CSSqlClassFile.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace m2mTeam.Monitoring.CLR
{
    public class GeneralizatorFactory
    {
        public static IRangeGeneralizator GetGeneralizator(GeneralizationMode mode)
        {
            switch (mode)
            {
                case  GeneralizationMode.ConsiderNulls :
                    return new ConsiderNulls();
                    break;
                case GeneralizationMode .ShowNullsRanges :
                    return new ShowNullsRanges();
                    break;
            }
            return new IgnoreNulls();
        }
    }
}
