//------------------------------------------------------------------------------
// <copyright file="CSSqlClassFile.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace m2mTeam.Monitoring.CLR
{
    public enum GeneralizationMode
    {
        NoNulls, ConsiderNulls, ShowNullsRanges
    }
}
