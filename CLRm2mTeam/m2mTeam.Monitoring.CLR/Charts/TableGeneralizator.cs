﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlTypes;

namespace m2mTeam.Monitoring.CLR
{
    public class TableGeneralizator
    {
        public const int TimeColumn = 0;
        public const int ValueColumn = 1;

        private const string TIMESTAMP = "MeasurementTime";
        private const string VALUE = "Value";

        private DataTable inTable;
        private DataTable outTable;
        private int targetRowCount;
        private IRangeGeneralizator rowGeneralizator;

        public TableGeneralizator(DataTable inTable, int rowCount, IRangeGeneralizator generalizator)
        {
            this.inTable = inTable;
            this.targetRowCount = rowCount;
            this.rowGeneralizator = generalizator;
        }
        
        public static DataTable resultTable()
        {
            var result = new DataTable();
            result.Columns.Add(TIMESTAMP, typeof(DateTime));
            result.Columns.Add(VALUE, typeof(short));

            return result;
        }

        public DataTable Execute()
        {
            var outTable = resultTable();
            try
            {
                if (inTable != null)
                {
                    int count = inTable.Rows.Count;

                    if (count <= targetRowCount)
                    {
                        return rowGeneralizator.GetGeneralizedTable(inTable);
                    }
                    else
                    {
                        double rangeSize = (double)count / targetRowCount;

                        int range = 1;
                        int rstart = 0;
                        int rend;
                        while (range <= targetRowCount)
                        {
                            rend = (int)Math.Round(range * rangeSize);

                            rowGeneralizator.InsertGeneralizedRows(inTable, rstart, rend, outTable);

                            range = range + 1;
                            rstart = rend + 1;
                        }
                    }
                }
            }
            catch (Exception)
            {

            }

            return outTable;
        }
    }
}
