using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace m2mTeam.Monitoring.CLR
{
    public static class DataTableExtension
    {
        public static void AddRow(this DataTable destination, DataRow row)
        {
            var dr = destination.NewRow();
            for (int i = 0; i < destination.Columns.Count; i++)
            {
                dr[i] = row[i];
            }
            destination.Rows.Add(dr);
        }

        public static void AddNullForRange(this DataTable destination, DataTable source, int start, int end)
        {
            var dr = destination.NewRow();
            dr[TableGeneralizator.TimeColumn] = DatesCalculator.MidDateTime(
                Convert.ToDateTime(source.Rows[start][TableGeneralizator.TimeColumn]),
                Convert.ToDateTime(source.Rows[end][TableGeneralizator.TimeColumn]));
            dr[TableGeneralizator.ValueColumn] = DBNull.Value;
            destination.Rows.Add(dr);
        }

    }
}


///nie wiedzie� czemu kompilator wyrzuca b��d - nie mo�e utworzy� extensions method
///poni�ej rozwi�zanie znalezione w sieci
namespace System.Runtime.CompilerServices
{
    public class ExtensionAttribute : Attribute { }
}