using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace m2mTeam.Monitoring.CLR
{
    public class IgnoreNulls : IRangeGeneralizator
    {
        public void InsertGeneralizedRows(System.Data.DataTable inTable, int rangeStart, int rangeEnd, System.Data.DataTable outTable)
        {
            double sum = 0;
            int count = 0;

            for (int i = rangeStart; i <= rangeEnd; i++)
            {
                if (!DBNull.Value.Equals(inTable.Rows[i][TableGeneralizator.ValueColumn]))
                {
                    sum += Convert.ToDouble(inTable.Rows[i][TableGeneralizator.ValueColumn]);
                    count += 1;
                }
            }

            if (count == 0) return;

            double avg = sum / count;

            double maxDeviation = double.MinValue;
            int maxDeviationIndex = -1;

            for (int i = rangeStart; i <= rangeEnd; i++)
            {
                if (!DBNull.Value.Equals(inTable.Rows[i][TableGeneralizator.ValueColumn]))
                {
                    double deviation = Math.Abs(Convert.ToDouble(inTable.Rows[i][TableGeneralizator.ValueColumn]) - avg);
                    if (deviation > maxDeviation)
                    {
                        maxDeviation = deviation;
                        maxDeviationIndex = i;
                    }
                }
            }

            if (maxDeviationIndex != -1)
            {
                outTable.AddRow(inTable.Rows[maxDeviationIndex]);
            }
        }


        public DataTable GetGeneralizedTable(DataTable inTable)
        {
            var outTable = TableGeneralizator.resultTable();
            for (int i = 0; i < inTable.Rows.Count; i++)
            {
                if (!DBNull.Value.Equals(inTable.Rows[i][TableGeneralizator.ValueColumn]))
                {
                    outTable.AddRow(inTable.Rows[i]);
                }
            }
            return outTable;
        }
    }
}
