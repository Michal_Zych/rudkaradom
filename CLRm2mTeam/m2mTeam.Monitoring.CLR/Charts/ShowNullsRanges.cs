//------------------------------------------------------------------------------
// <copyright file="CSSqlClassFile.cs" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace m2mTeam.Monitoring.CLR
{
    public class ShowNullsRanges : IRangeGeneralizator
    {
        IgnoreNulls generalizator = new IgnoreNulls();
        public void InsertGeneralizedRows(DataTable inTable, int rangeStart, int rangeEnd, DataTable outTable)
        {
            
            int start, end;
            bool isNullRange;
            //je�li rangeStart = 0 postaw punkt
            if(rangeStart == 0)
                outTable.AddRow(inTable.Rows[rangeStart]);
            //start = range start
            start = rangeStart;
            //powtarzaj dop�ki start <= rangeEnd
            while(start <= rangeEnd)
            {
                //wyznacz typ przedzia�u nullowy lub warto�ciowy
                isNullRange = DBNull.Value.Equals(inTable.Rows[start][TableGeneralizator.ValueColumn]);
                //oblicz koniec przedzia�u - end
                end = GetRangeEnd(inTable, start, rangeEnd, isNullRange);
                
                if(isNullRange)
                    outTable.AddNullForRange(inTable, rangeStart, rangeEnd);
                else //je�li przedzia� nienulowy - generalizuj w zakresie start,end
                    generalizator.InsertGeneralizedRows(inTable, start, end, outTable);
                //je�li end < rangeEnd lub end = ostatni wiersz - postaw punkt
                if(end < rangeEnd || end == inTable.Rows.Count - 1)
                    outTable.AddRow(inTable.Rows[rangeStart]);
                //nast�pny przedzia� rozpocznij za bie��cym
                start = end + 1;
            }
        }

        private int GetRangeEnd(DataTable inTable, int start, int rangeEnd, bool isNullRange)
        {
            int result = start;
            while (result < rangeEnd)
            {
                if(isNullRange == DBNull.Value.Equals(inTable.Rows[result][TableGeneralizator.ValueColumn]))
                    result += 1;
                else
                    break;
            }
            return result;
        }

        public DataTable GetGeneralizedTable(DataTable inTable)
        {
            return inTable;
        }
    }
}
