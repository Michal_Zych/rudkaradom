﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation
{
    public class ServicesUri
    {
        public const string UnitsService = "Finder-BloodDonation-Model-Services-UnitsService.svc";
        public const string AlarmsService = "Finder-BloodDonation-Model-Services-AlarmsService.svc";
        public const string ReportsService = "Finder-BloodDonation-Model-Services-ReportsService.svc";
        public const string UsersService = "Finder-BloodDonation-Model-Services-UsersService.svc";
		public const string HubsService = "Finder-BloodDonation-Model-Services-HubsService.svc";
        public const string ValidationService = "Finder-BloodDonation-Model-Services-ValidationService.svc";
		public const string PermissionsService = "Finder-BloodDonation-Model-Services-PermissionsService.svc";
        public const string CommService = "Finder-BloodDonation-Model-Services-CommService.svc";
        public const string RfidService = "Finder-BloodDonation-Model-Services-RfidService.svc";
    }
}
