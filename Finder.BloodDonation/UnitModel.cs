﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Model.Dto;
using System.Collections.Generic;
using Finder.BloodDonation.Common.Layout;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Events;

namespace Finder.BloodDonation
{
    public class AlarmUnitModel
    {
        private IUnityContainer Container;
        IDynamicEventAggregator EventAggregator;

        public bool IsAlarm { get; set; }

        public UnitDTO Unit { get; private set; }

        public List<AlarmUnitModel> Children { get; private set; }

        public event EventHandler<AlarmNotifyEventArgs> OnAlarmNotify;

        public AlarmUnitModel(IUnityContainer container)
        {
            Container = container;
            EventAggregator = container.Resolve<IDynamicEventAggregator>();
        }

        public void SetUnit(UnitDTO unit)
        {
            Unit = unit;

            if (unit.Children != null)
            {
                Children = new List<AlarmUnitModel>();
                foreach (UnitDTO u in unit.Children)
                {
                    AlarmUnitModel vm = Container.Resolve<AlarmUnitModel>();
                    vm.SetUnit(u);
                    vm.OnAlarmNotify += new EventHandler<AlarmNotifyEventArgs>(vm_OnAlarmNotify);
                    Children.Add(vm);
                }
            }
        }

        void vm_OnAlarmNotify(object sender, AlarmNotifyEventArgs e)
        {
            if (e.State) //ustawienie alarmu
            {
                if (!IsAlarm)
                {
                    IsAlarm = true;
                    if (OnAlarmNotify != null)
                    {
                        OnAlarmNotify(sender, e);
                    }
                    EventAggregator.GetEvent<AlarmsUnitAlarmSet>().Publish(Unit.ID);
                }
            }
            else //odwołanie alarmu
            {
                if (IsAlarm)
                {
                    IsAlarm = false;
                    if (OnAlarmNotify != null)
                    {
                        OnAlarmNotify(sender, e);
                    }
                    EventAggregator.GetEvent<AlarmsUnitAlarmDispose>().Publish(Unit.ID);
                }
            }
        }

        public void SetAlarm(int[] units)
        {
            if (units.Contains(Unit.ID))
            {
                if (!IsAlarm)
                {
                    IsAlarm = true;
                    PublishAlarmNotify();
                }
            }
            else // nie ma aktywnego alarmu ale jeżeli jest ustawiony stan to znaczy że trzeba odwołać
            {
                if (IsAlarm)
                {
                    IsAlarm = false;
                    PublishAlarmNotify();
                }
            }

            for (int i = 0; i < Children.Count; i++)
            {
                Children[i].SetAlarm(units);
            }
        }

        private void PublishAlarmNotify()
        {
            if (OnAlarmNotify != null)
            {
                OnAlarmNotify(this, new AlarmNotifyEventArgs() { State = IsAlarm });
            }

            if (IsAlarm)
            {
                EventAggregator.GetEvent<AlarmsUnitAlarmSet>().Publish(Unit.ID);
            }
            else
            {
                EventAggregator.GetEvent<AlarmsUnitAlarmDispose>().Publish(Unit.ID);
            }
        }

        public bool IsUnitAlarm(int unit)
        {
            if (Unit.ID == unit)
            {
                return IsAlarm;
            }

            for (int i = 0; i < Children.Count; i++)
            {
                if (Children[i].IsUnitAlarm(unit))
                {
                    return true;
                }
            }

            return false;
        }

    }
}
