﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Finder.BloodDonation.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class TypeNames {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal TypeNames() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Finder.BloodDonation.Resources.TypeNames", typeof(TypeNames).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0-.
        /// </summary>
        public static string _0_minus {
            get {
                return ResourceManager.GetString("_0_minus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 0+.
        /// </summary>
        public static string _0_plus {
            get {
                return ResourceManager.GetString("_0_plus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to A-.
        /// </summary>
        public static string _a_minus {
            get {
                return ResourceManager.GetString("_a_minus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to A+.
        /// </summary>
        public static string _a_plus {
            get {
                return ResourceManager.GetString("_a_plus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AB-.
        /// </summary>
        public static string _ab_minus {
            get {
                return ResourceManager.GetString("_ab_minus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AB+.
        /// </summary>
        public static string _ab_plus {
            get {
                return ResourceManager.GetString("_ab_plus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to B-.
        /// </summary>
        public static string _b_minus {
            get {
                return ResourceManager.GetString("_b_minus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to B+.
        /// </summary>
        public static string _b_plus {
            get {
                return ResourceManager.GetString("_b_plus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Osocze 0.
        /// </summary>
        public static string _plasma_0 {
            get {
                return ResourceManager.GetString("_plasma_0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Osocze A.
        /// </summary>
        public static string _plasma_a {
            get {
                return ResourceManager.GetString("_plasma_a", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Osocze AB.
        /// </summary>
        public static string _plasma_ab {
            get {
                return ResourceManager.GetString("_plasma_ab", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Osocze B.
        /// </summary>
        public static string _plasma_b {
            get {
                return ResourceManager.GetString("_plasma_b", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Krew.
        /// </summary>
        public static string blood {
            get {
                return ResourceManager.GetString("blood", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Produkty krwiopochodne.
        /// </summary>
        public static string blood_derivs {
            get {
                return ResourceManager.GetString("blood_derivs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Katalizatory.
        /// </summary>
        public static string catalyzer {
            get {
                return ResourceManager.GetString("catalyzer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Inhibitory.
        /// </summary>
        public static string inhibitor {
            get {
                return ResourceManager.GetString("inhibitor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Osocze.
        /// </summary>
        public static string plasma {
            get {
                return ResourceManager.GetString("plasma", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Płytki krwi.
        /// </summary>
        public static string platelets {
            get {
                return ResourceManager.GetString("platelets", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Odczynniki.
        /// </summary>
        public static string reagents {
            get {
                return ResourceManager.GetString("reagents", resourceCulture);
            }
        }
    }
}
