﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;


namespace Finder.BloodDonation.Resources
{
    public class CustomLocalizationManager : LocalizationManager
    {
        public override string GetStringOverride(string key)
        {
            switch (key)
                {
                    case "Close":
                        return "Zamknij";
                    case "Clock":
                        return "Godzina";

                }
            return base.GetStringOverride(key);
        }
    }
}
