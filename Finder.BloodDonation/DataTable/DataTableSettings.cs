﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.DataTable
{
    public static class DataTableSettings
    {
        public static bool AfterSortingKeepSelection = true;

        public static bool AfterFilteringKeepSelection = true;

        public static bool AfterFilteringSelectFirst = true;
    }
}
