﻿using Finder.BloodDonation.Common.Filters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.Tools;

namespace Finder.BloodDonation.DataTable
{
    public static class FilterParser
    {
        public const string ERROR = "error";
        public const string NULL_SYMBOL = "?";
        public const int NULL_VALUE = 0;
        public const char OR_SYMBOL = '|';
        public const char RANGE_SYMBOL = '-';
        private const string FILTER_ERROR_MSG = "Podano niewłaściwą wartość filtra {0}: '{1}'";
        private const string LO_RANGE_SUFIX = "From";
        private const string HI_RANGE_SUFIX = "To";
        private const string IGNORABLE_PREFIX = "_";




        public static void ClearFilterValues(this IFilter filter)
        {
            var properties = filter.GetType().GetProperties(System.Reflection.BindingFlags.Public
                                                        | System.Reflection.BindingFlags.Instance
                                                        | System.Reflection.BindingFlags.DeclaredOnly);
            foreach (var property in properties)
            {
                if (property.Name.StartsWith(IGNORABLE_PREFIX)) continue;

                if (Nullable.GetUnderlyingType(property.PropertyType) != null)
                {
                    property.SetValue(filter, null, null);
                }
                else if (property.PropertyType == typeof(string))
                {
                    property.SetValue(filter, null, null);
                }
                else if(property.PropertyType == typeof(BoolFilterValue))
                {
                    property.SetValue(filter, BoolFilterValue.NotUsed, null);
                }
            }
        }


        public static bool IsNumericType(Type t)
        {
            if (Nullable.GetUnderlyingType(t) != null) t = Nullable.GetUnderlyingType(t);
            switch (Type.GetTypeCode(t))
            {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    return true;
                default:
                    return false;
            }

        }

        public static IDictionary<string, string> GetFilterDictionary(this IDataTable dataTable)
        {
            string value;
            bool conversionResult = false;
            var result = new Dictionary<string, string>();


            foreach (var column in dataTable.Columns)
            {
                if (column.IsHidden || column.Name.Equals(String.Empty)) continue;
                value = String.Empty;
                switch (column.DataType)
                {
                    case FilterDataType.String:
                        conversionResult = StringValueToDictionary(result, dataTable, column.Name);
                        break;

                    case FilterDataType.Bool:
                        conversionResult = BoolRangeToDictionary(result, dataTable, column.Name);
                        break;

                    case FilterDataType.NullableBool:
                        conversionResult = NullableBoolRangeToDictionary(result, dataTable, column.Name);
                        break;

                    case FilterDataType.DateTime:
                        conversionResult = DateRangeToDictionary(result, dataTable, column.Name);
                        break;
                    default:
                        conversionResult = NumericRangeToDictionary(result, dataTable, column.Name, column.DataType);
                        break;
                }

                if (conversionResult == false)
                {
                    MsgBox.Error("Niewłaściwa wartość filtru dla pola " + column.Label);
                    return null;
                }
            }


            return result;
        }



        

        private static bool NumericRangeToDictionary(Dictionary<string, string> dict, IDataTable dataTable, string propertyName, FilterDataType dataType)
        {
            var property = dataTable.Filter.GetType().GetProperty(propertyName, System.Reflection.BindingFlags.Public
                        | System.Reflection.BindingFlags.Instance
                        | System.Reflection.BindingFlags.DeclaredOnly);
            if (property != null)
            {
                var value = property.GetValue(dataTable.Filter, null);
                if (value != null)
                {
                    string filterStr = value.ToString();
                    if (filterStr == "?")
                        return false;

                    double? min, max;
                    bool properRange = ParseDoubleRangeNew(filterStr, out min, out max);
                    if (!properRange)
                        return false;
                    object minValue = null, maxValue = null;
                    bool result = false;
                    switch (dataType)
                    {
                        case FilterDataType.Byte:
                            result = DoubleToByteObject(min, out minValue);
                            if (result) result = DoubleToByteObject(max, out maxValue);
                            break;
                        case FilterDataType.Short:
                            result = DoubleToShortObject(min, out minValue);
                            if (result) result = DoubleToShortObject(max, out maxValue);
                            break;
                        case FilterDataType.Int:
                            result = DoubleToIntObject(min, out minValue);
                            if (result) result = DoubleToIntObject(max, out maxValue);
                            break;
                        case FilterDataType.Long:
                            result = DoubleToLongObject(min, out minValue);
                            if (result) result = DoubleToLongObject(max, out maxValue);
                            break;
                        default:
                            minValue = min;
                            maxValue = max;
                            break;
                    }
                    if (result == false) return result;

                    if (minValue != null) dict[propertyName + LO_RANGE_SUFIX] = minValue.ToString();
                    if (maxValue != null) dict[propertyName + HI_RANGE_SUFIX] = maxValue.ToString();
                }
            }
            return true;
        }

        private static bool DoubleToLongObject(double? d, out object value)
        {
            value = null;
            if (d.HasValue)
            {
                if (!((d.Value % 1) == 0)) return false;
                try
                {
                    value = Convert.ToInt64(d.Value);
                }
                catch
                {
                    return false;
                }
            }
            return true;
        }

        private static bool DoubleToIntObject(double? d, out object value)
        {
            value = null;


            if (d.HasValue)
            {
                if (!((d.Value % 1) == 0)) return false;

                try
                {
                    value = Convert.ToInt32(d.Value);
                }
                catch
                {
                    return false;
                }
            }
            return true;
        }

        private static bool DoubleToShortObject(double? d, out object value)
        {
            value = null;
            if (d.HasValue)
            {
                if (!((d.Value % 1) == 0)) return false;
                try
                {
                    value = Convert.ToInt16(d.Value);
                }
                catch
                {
                    return false;
                }
            }
            return true;
        }

        private static bool DoubleToByteObject(double? d, out object value)
        {
            value = null;
            if (d.HasValue)
            {
                if (!((d.Value % 1) == 0)) return false;
                try
                {
                    value = Convert.ToByte(d.Value);
                }
                catch
                {
                    return false;
                }
            }
            return true;
        }

        private static bool ParseDoubleRangeNew(string filterStr, out double? min, out double? max)
        {
            if (filterStr == NULL_SYMBOL)
            {
                min = null;
                max = NULL_VALUE;
                return true;
            }
            if (String.IsNullOrEmpty(filterStr))
            {
                min = null;
                max = null;
                return true;
            }
            double value1, value2;
            filterStr = filterStr.Replace(',', '.');

            if (double.TryParse(filterStr, NumberStyles.Any, CultureInfo.InvariantCulture, out value1))
            {
                min = value1;
                max = null;
                return true;
            }

            var splitPosition = GetSplitPosition(filterStr);
            if (splitPosition > 0)
            {
                if (double.TryParse(filterStr.Substring(0, splitPosition), NumberStyles.Any, CultureInfo.InvariantCulture, out value1))
                {
                    if (double.TryParse(filterStr.Substring(splitPosition + 1), NumberStyles.Any, CultureInfo.InvariantCulture, out value2))
                    {
                        if (value1 == value2)
                        {
                            min = value1;
                            max = null;
                            return true;
                        }
                        else if (value1 > value2)
                        {
                            min = value2;
                            max = value1;
                            return true;
                        }
                        min = value1;
                        max = value2;
                        return true;
                    }
                }
            }
            min = null;
            max = null;
            return false;
        }

        private static int GetSplitPosition(string filterStr)
        {
            int rSymbolCount = filterStr.Count(x => x == RANGE_SYMBOL);
            int splitPosition = -1;
            if (rSymbolCount == 1)
            {
                splitPosition = filterStr.IndexOf(RANGE_SYMBOL);
            }
            else if (RANGE_SYMBOL == '-')
            {
                if (rSymbolCount == 3)
                {
                    splitPosition = filterStr.IndexOf(RANGE_SYMBOL);
                    splitPosition = filterStr.IndexOf(RANGE_SYMBOL, splitPosition + 1);
                }
                else if (rSymbolCount == 2)
                {
                    splitPosition = filterStr.IndexOf(RANGE_SYMBOL);
                    double value1;
                    if (!double.TryParse(filterStr.Substring(0, splitPosition), NumberStyles.Any, CultureInfo.InvariantCulture, out value1))
                    {
                        splitPosition = filterStr.IndexOf(RANGE_SYMBOL, splitPosition + 1);
                    }
                }
            }
            return splitPosition;
        }


        private static bool DateRangeToDictionary(Dictionary<string, string> dict, IDataTable dataTable, string propertyName)
        {
            bool result = DateValueToDictionary(dict, dataTable, propertyName + LO_RANGE_SUFIX);
            if (result)
                result = DateValueToDictionary(dict, dataTable, propertyName + HI_RANGE_SUFIX);
            return result;
        }

        private static bool DateValueToDictionary(Dictionary<string, string> dict, IDataTable dataTable, string propertyName)
        {
            var property = dataTable.Filter.GetType().GetProperty(propertyName);
            if (property != null)
            {
                var date = (DateTime?)property.GetValue(dataTable.Filter, null);
                if (date.HasValue)
                {
                    dict[propertyName] = date.Value.ToSQLstring();
                }
            }
            return true;
        }

        private static bool StringValueToDictionary(IDictionary<string, string> dict, IDataTable dataTable, string propertyName)
        {
            var property = dataTable.Filter.GetType().GetProperty(propertyName);
            if (property != null)
            {
                var value = property.GetValue(dataTable.Filter, null);
                if (value != null)
                {
                    dict[propertyName] = value.ToString();
                }
            }
            return true;
        }


        private static bool BoolRangeToDictionary(Dictionary<string, string> dict, IDataTable dataTable, string propertyName)
        {
            var property = dataTable.Filter.GetType().GetProperty(propertyName);
            if (property != null)
            {
                var b = (bool?)property.GetValue(dataTable.Filter, null);
                if (b.HasValue)
                {
                    dict[propertyName + LO_RANGE_SUFIX] = b.Value ? "1" : "0";
                    dict[propertyName + HI_RANGE_SUFIX] = dict[propertyName + LO_RANGE_SUFIX];
                }
            }
            return true;
        }

        private static bool NullableBoolRangeToDictionary(Dictionary<string, string> dict, IDataTable dataTable, string propertyName)
        {
            var property = dataTable.Filter.GetType().GetProperty(propertyName);
            if (property != null)
            {
                var b = (BoolFilterValue)property.GetValue(dataTable.Filter, null);
                
                switch(b)
                {
                    case BoolFilterValue.No:
                            dict[propertyName + LO_RANGE_SUFIX] = "0";
                            dict[propertyName + HI_RANGE_SUFIX] = dict[propertyName + LO_RANGE_SUFIX];
                            break;
                    case BoolFilterValue.Yes:
                                                    dict[propertyName + LO_RANGE_SUFIX] = "1";
                            dict[propertyName + HI_RANGE_SUFIX] = dict[propertyName + LO_RANGE_SUFIX];
                            break;
                    case BoolFilterValue.NullValue:
                            dict[propertyName + HI_RANGE_SUFIX] = "1";
                            break;
                }
            }
            return true;
        }




        private static bool BoolValueToDictionary(IDictionary<string, string> dict, IDataTable dataTable, string propertyName)
        {
            var property = dataTable.Filter.GetType().GetProperty(propertyName);
            if (property != null)
            {
                var b = (bool?)property.GetValue(dataTable.Filter, null);
                if (b.HasValue)
                {
                    dict[propertyName] = b.Value ? "1" : "0";
                }
            }
            return true;
        }


        public static string GetPropertyName(string filterName)
        {
            if (filterName.EndsWith(LO_RANGE_SUFIX))
                return filterName.Substring(0, filterName.Length - LO_RANGE_SUFIX.Length);
            if (filterName.EndsWith(HI_RANGE_SUFIX))
                return filterName.Substring(0, filterName.Length - HI_RANGE_SUFIX.Length);
            return filterName;
        }

        public static string GetFromPropertyName(string propertyName)
        {
            return propertyName + LO_RANGE_SUFIX;
        }

        public static string GetToPropertyName(string propertyName)
        {
            return propertyName + HI_RANGE_SUFIX;
        }

        public static string[] SplitByOrConditions(string filterValue)
        {
            return filterValue.Split(OR_SYMBOL);
        }
    }
}
