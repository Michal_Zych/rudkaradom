﻿using System;
using System.Globalization;
using System.Net;


namespace Finder.BloodDonation.DataTable
{
    public static class DbDataExtension
    {
        private const string SQL_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

        public static string ToSQLstring(this DateTime value)
        {
            return value.ToString(SQL_DATETIME_FORMAT);
        }

        public static DateTime? FromSQLstring(string value)
        {
            DateTime? result = null;
            try
            {
                result = DateTime.ParseExact(value, SQL_DATETIME_FORMAT, CultureInfo.InvariantCulture);
            }
            catch { };

            return result;
        }

        public static string NullIfEmpty(this string source)
        {
            if (source == null)
                return null;

            source = source.Trim();
            if (string.IsNullOrEmpty(source))
                return null;
            return source;
        }

    }
}
