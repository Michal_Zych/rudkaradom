﻿using Finder.BloodDonation.Common.Filters;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;


namespace Finder.BloodDonation.DataTable
{
    public static class ColumnsHelper
    {
        public static ObservableCollection<IDataTableColumn> Add(this ObservableCollection<IDataTableColumn> columns, int width)
        {
            return columns.Add(width, "",true);
        }


        public static ObservableCollection<IDataTableColumn> Add(this ObservableCollection<IDataTableColumn> columns, int width, string name)
        {
            return columns.Add(width, name, name, FilterDataType.Int);
        }
        public static ObservableCollection<IDataTableColumn> Add(this ObservableCollection<IDataTableColumn> columns, int width, string name, bool isFixed)
        {
            var c = columns.Add(width, name, name, FilterDataType.Int);
            c[c.Count - 1].IsFixed = isFixed;
            return c;
        }
        public static ObservableCollection<IDataTableColumn> Add(this ObservableCollection<IDataTableColumn> columns, int width, string name, string label)
        {
            return columns.Add(width, name, label, FilterDataType.String);
        }

        public static ObservableCollection<IDataTableColumn> Add(this ObservableCollection<IDataTableColumn> columns, int width, string name, string label, FilterDataType dataType)
        {
            return columns.Add(width, name, label, dataType, GetAlignmentForType(dataType));
        }

        public static ObservableCollection<IDataTableColumn> Add(this ObservableCollection<IDataTableColumn> columns, int width, string name, string label, FilterDataType dataType, TextAlignment alignment)
        {
            return columns.Add(width, name, label, dataType, alignment, false);
        }
        public static ObservableCollection<IDataTableColumn> Add(this ObservableCollection<IDataTableColumn> columns, int width, string name, string label, FilterDataType dataType, TextAlignment alignment, bool isHidden)
        {
            columns.Add(new DataTableColumn()
            {
                ColumnNumber = columns.Count,
                Width = width,
                IsFixed = false,
                Name = name,
                Label = label,
                DataType = dataType,
                IsHidden = isHidden,
                IsVisible = true,
                Alignment = alignment
            });
            return columns;
        }

        public static ObservableCollection<IDataTableColumn> Add(this ObservableCollection<IDataTableColumn> columns, int width, string name, string label, FilterDataType dataType, TextAlignment alignment, bool isHidden, bool isVisible)
        {

            DataTableColumn column = new DataTableColumn()
            {
                ColumnNumber = columns.Count,
                Width = width,
                IsFixed = false,
                Name = name,
                Label = label,
                DataType = dataType,
                IsHidden = isHidden,
                IsVisible = isVisible,
                Alignment = alignment
            };

            if(!isVisible)
            {
                column.Width = 0;
                column.LastWidth = width;

            }

            columns.Add(column);
            return columns;
        }

        private static TextAlignment GetAlignmentForType(FilterDataType dataType)
        {
            switch (dataType)
            {
                case FilterDataType.Bool: return TextAlignment.Center; break;
                case FilterDataType.DateTime: return TextAlignment.Center; break;
                case FilterDataType.String: return TextAlignment.Left; break;
                default: return TextAlignment.Right; break;
            }
        }
    }
}
