﻿using Finder.BloodDonation.Common.Filters;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Net;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.DataTable
{
    [KnownType(typeof(DataTableColumn))]
    public class DataTableColumn : IDataTableColumn
    {
        public int ColumnNumber { get; set; }
        public int Width { get; set; }
        public bool IsFixed { get; set; }

        public string Name { get; set; }
        public string Label { get; set; }
        public FilterDataType DataType { get; set; }
        public bool IsHidden { get; set; }
        public bool IsVisible { get; set; }
        public TextAlignment Alignment { get; set; }

        public int LastWidth { get; set; }

        public DataTableColumn()
        {

        }

        public IDataTableColumn Copy()
        {
            return (IDataTableColumn)this.MemberwiseClone();
        }

    }
}
