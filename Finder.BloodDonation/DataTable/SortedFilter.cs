﻿using Finder.BloodDonation.Common.Filters;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.DataTable
{
    public class SortedFilter : NotifyViewModelBase, ISortedFilter
    {


        private string _sortOrder;


        public int UnitId { get; set; }

        [RaisePropertyChanged]
        public string SortOrder
        {
            get
            {
                return _sortOrder;
            }
            set
            {
                string current = _sortOrder ?? String.Empty;

                if (current.IndexOf(value) > 0)
                {
                    if (current[0] == FilterConstant.AscendingSign)
                    {
                        _sortOrder = FilterConstant.DescendingSign + value;
                        return;
                    }
                }

                _sortOrder = FilterConstant.AscendingSign + value;
            }
        }


        public SortedFilter(IUnityContainer container)
            : base(container)
        {
            Clear();
        }



        public virtual IDictionary<string, string> GetFilterDictionary()
        {
            var d = new Dictionary<string, string>();
            d.Add(FilterConstant.SortOrderKey, SortOrder);
            d.Add(FilterConstant.UnitFromTreeKey, UnitId.ToString());
            return d;
        }

        public virtual void Clear()
        {
        }

    }
}
