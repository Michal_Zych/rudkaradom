﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.DataTable;
using Finder.BloodDonation.Dialogs.Helpers;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Common.Filters;

namespace Finder.BloodDonation.DataTable
{
    public abstract class BaseDataTable : NotifyViewModelBase, IDataTable
    {
        private readonly IWindowsManager _windowsManager;
        private ObservableCollection<IDataTableColumn> columns;

        [RaisePropertyChanged]
        public ObservableCollection<IDataTableColumn> Columns
        {
            get
            {
                return columns;
            }
            set
            {
                columns = value;
            }
        }

        [RaisePropertyChanged]
        public ObservableCollection<GridLength> ColumnWidths { get; set; }

        public ISortedFilter Filter { get; set; }

        public BaseDataTable(IUnityContainer container)
            : base(container)
        {
            ReloadColumns();

            ColumnWidths = GetColumnWidths();

            Filter = GetFilter(container);

            _windowsManager = this.Container.Resolve<IWindowsManager>();
            var viewFactory = container.Resolve<ViewFactory>();
            _windowsManager.RegisterWindow(WindowsKeys.ColumnEditorDialog, "Edycja kolumn", viewFactory.CreateView<ColumnEditorDialog>(), new Size(410, 310), true);
        }

        private void ReloadColumns()
        {
            var defaultColumns = GetColumns();

            if (StartWithDefaultColumns)
            {
                Columns = defaultColumns;
                return;
            }

            var cols = LoadColumns();
            if (cols == null)
            {
                cols = GetColumns();
            }
            else
            {
                foreach (var column in defaultColumns)
                {
                    if (!cols.ContainColumn(column.Name))
                    {
                        cols.Add(column.Width, column.Name, column.Label, column.DataType, column.Alignment, column.IsHidden);
                    }
                    else
                    {
                        var c = cols.Where(x => x.Name == column.Name).First();
                        c.IsHidden = column.IsHidden;
                        c.IsFixed = column.IsFixed;
                        if (!c.IsHidden && c.IsVisible && c.Width == 0)
                        {
                            c.Width = column.Width;
                        }
                    }
                }
            }

            Columns = cols;
        }

        public virtual bool StartWithDefaultColumns
        {
            get
            {
                return false;
            }
        }

        private ObservableCollection<GridLength> GetColumnWidths()
        {
            var columnWidths = new ObservableCollection<GridLength>();
            foreach (var column in Columns.OrderBy(x => x.ColumnNumber))
            {
                columnWidths.Add(new GridLength(column.IsHidden ? 0 : column.Width));
            }
            return columnWidths;
        }

        public abstract ObservableCollection<IDataTableColumn> GetColumns();

        public abstract ISortedFilter GetFilter(IUnityContainer container);


        private string storageKey()
        {
            return this.GetType().Name + BloodyUser.Current.ID.ToString();
        }

        public void SaveColumns()
        {
            foreach (var column in Columns)
            {
                column.Width = (int)ColumnWidths[column.ColumnNumber].Value;
            }

            LocalStorageManager.SaveSettingsCollection<IDataTableColumn>(storageKey(), Columns);
        }


        private ObservableCollection<IDataTableColumn> LoadColumns()
        {
            var i = LocalStorageManager.LoadSettingsCollection<IDataTableColumn>(storageKey());
            if (i != null && i.Count == 0) return null;
            return i;
        }


        public IDataTableColumn this[string propertyName]
        {
            get
            {
                foreach (var column in Columns)
                {
                    if (column.Name.Equals(propertyName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return column;
                    }
                }
                return null;
            }
        }


        public IDataTableColumn this[int columnIndex]
        {
            get
            {
                return Columns[columnIndex];
            }
        }

        [RaisePropertyChanged]
        public string SortOrder
        {
            get
            {
                return Filter.SortOrder;
            }
            set
            {
                if (value == String.Empty)
                {
                    this._windowsManager.ShowWindow(WindowsKeys.ColumnEditorDialog, this);
                }
                else Filter.SortOrder = value;
            }
        }

        public IDictionary<string, string> GetFilterDictionary()
        {
            var fromSortedDict = Filter.GetFilterDictionary();

            var dict = FilterParser.GetFilterDictionary(this);
            if (dict != null && fromSortedDict != null && fromSortedDict.Count > 0)
            {
                foreach (var pair in fromSortedDict)
                {
                    dict[pair.Key] = pair.Value;
                }
            }

            return dict;
        }


        public void Clear()
        {
            Filter.ClearFilterValues();
        }

        public void SetColumnVisibility(string name, bool visible)
        {
            var column = this[name];
            if (column.IsVisible == visible) return;
            if (visible)
            {
                column.Width = column.LastWidth;
            }
            else
            {
                column.LastWidth = column.Width;
                column.Width = 0;
            }
            column.IsVisible = visible;
            ColumnWidths[column.ColumnNumber] = new GridLength(column.Width);
        }

        public void SetColumnAlignment(string name, TextAlignment alignment)
        {
            var column = this[name];
            if (column.Alignment == alignment) return;
            column.Alignment = alignment;
            RefreshColumns();
        }

        private void RefreshColumns()
        {
            var newCols = new ObservableCollection<IDataTableColumn>(Columns);
            Columns = newCols;
        }


        public void MoveColumn(string name, int direction)
        {
            var columnToMove = this[name];
            var columnToReplace = GetSiblingColumn(columnToMove.ColumnNumber, direction);
            if (columnToReplace == null) return;


            var colToMoveWidth = ColumnWidths[columnToMove.ColumnNumber].Value;
            var colToReplaceWidth = ColumnWidths[columnToReplace.ColumnNumber].Value;


            var i = columnToReplace.ColumnNumber;
            columnToReplace.ColumnNumber = columnToMove.ColumnNumber;
            columnToMove.ColumnNumber = i;

            ColumnWidths[columnToMove.ColumnNumber] = new GridLength(colToMoveWidth);
            ColumnWidths[columnToReplace.ColumnNumber] = new GridLength(colToReplaceWidth);

            RefreshColumns();
        }

        private IDataTableColumn GetSiblingColumn(int colNumber, int direction)
        {
            var newNr = colNumber + direction;
            var column = Columns.Where(x => x.ColumnNumber == newNr).FirstOrDefault();

            while (column != null && (!column.IsVisible || column.Name.Equals(String.Empty, StringComparison.InvariantCulture)))
            {
                newNr = column.ColumnNumber + direction;
                column = Columns.Where(x => x.ColumnNumber == newNr).FirstOrDefault();
            }
            return column;
        }

        public void ClearFilter()
        {
            Clear();
        }
    }
}
