﻿using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.Tools;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.DataTable
{
    public static class DataTableExtensions
    {
        public static int SearchItem<VM>(this IDataTable dataTable, ServerSidePagedCollectionView<VM> items, VM selectedItem, string textToFind) where VM : class
        {
            if (items.Count == 0 || String.IsNullOrWhiteSpace(textToFind)) return -1;

            int startIndex = 0;
            if (selectedItem != null)
            {
                while (items[startIndex] != selectedItem)
                {
                    startIndex++;
                }
                startIndex++;
                if (startIndex == items.Count)
                {
                    startIndex = 0;
                }
            }

            bool found = false;
            int currentIndex = startIndex - 1;

            while (!found && startIndex != currentIndex)
            {
                if (startIndex >= 0)
                {
                    var item = items[startIndex].GetType().GetProperty("Item").GetValue(items[startIndex], null);

                    foreach (var column in dataTable.Columns)
                    {
                        if (!column.IsVisible || column.Name.Equals(String.Empty, StringComparison.InvariantCulture)) continue;
                        var property = item.GetType().GetProperty(column.Name);
                        var value = property.GetValue(item, null);
                        if (value != null)
                        {
                            if (CultureInfo.CurrentCulture.CompareInfo.IndexOf(value.ToString(), textToFind, CompareOptions.IgnoreCase) >= 0)
                            {
                                found = true;
                                break;
                            }
                        }
                    }
                }
                if (!found)
                {
                    startIndex = startIndex + 1;
                    if (startIndex == items.Count) startIndex = -1;
                }
            }

            if (found)
            {
                return startIndex;
            }
            else
            {
                MsgBox.Warning("Nie znaleziono");
            }


            return -1;
        }

        public static bool ContainColumn(this  ObservableCollection<IDataTableColumn> columns, string name)
        {
            foreach (var column in columns)
            {
                if (column.Name.Equals(name, StringComparison.Ordinal)) return true;
            }
            return false;
        }
    }
}
