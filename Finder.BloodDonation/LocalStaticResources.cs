﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation
{
    public class LocalStaticResources
    {
        private static int minSize = 50;

        public static int MinSize { get { return minSize; } set { minSize = value; } }
    }
}
