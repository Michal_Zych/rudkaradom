﻿using Finder.BloodDonation.Controls;
using Finder.BloodDonation.DBBuffers;
using Finder.BloodDonation.Themes;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace Finder.BloodDonation.Tools
{
    public static class MsgBox
    {
        public enum Buttons
        {
            YesNo, OkCancel
        }
        public static bool DatesInRange(int rangeDays, DateTime startDate, DateTime endDate)
        {
            if ((endDate - startDate).TotalDays > rangeDays)
            {
                MsgBox.Show("Błędny zakres dat",
                       string.Format("Maksymalna liczba dni ujętych w zestawieniu nie może przekraczać {0}.", rangeDays));
                return false;
            }
            return true;
        }


        private static void Show(string title, string message, bool isPrompt = false)
        {
            if(isPrompt)
            {
                RadWindow.Prompt(new DialogParameters()
                {
                    Header = new ContentPresenter() { Content = title },
                    Content = new ContentPresenter() { Content = message }
                });
            }

            RadWindow.Alert(new DialogParameters()
            {
                Header = new ContentPresenter() { Content = title },
                Content = new ContentPresenter() { Content = message }
            });
        }

        public static void Warning(string message)
        {
            Show("Uwaga", message);
        }

        public static void Error(string message)
        {
            Show("Błąd", message);
        }

        public static void Prompt(string title, string message, EventHandler<WindowClosedEventArgs> onClose)
        {
            //RadWindow.Prompt(message, onClose, String.Empty);

            RadWindow.Prompt(
                new DialogParameters()
                {
                    Header = new ContentPresenter() { Content = title },
                    Content = new ContentPresenter() { Content = message },
                    Closed = onClose,
                    OkButtonContent = new ContentPresenter() { Content = "Ok" },
                    CancelButtonContent = new ContentPresenter() { Content = "Anuluj" }
                });
        }

        public static void Confirm(string message, EventHandler<WindowClosedEventArgs> onClose, Buttons buttons)
        {
            string yesTxt;
            string noTxt;

            if (buttons == Buttons.YesNo)
            {
                yesTxt = "Tak";
                noTxt = "Nie";
            }
            else
            {
                yesTxt = "OK";
                noTxt = "Anuluj";
            }

            RadWindow.Confirm(new DialogParameters()
            {
                Header = new ContentPresenter() { Content = "Potwierdź" },
                Content = new ContentPresenter() { Content = message },
                OkButtonContent = new ContentPresenter() { Content = yesTxt },
                CancelButtonContent = new ContentPresenter() { Content = noTxt },
                Closed = onClose
            });
        }

        public static void Succes(string message)
        {
            var theme = new CustomRadAlertTheme();

            RadWindow.Alert(new DialogParameters()
            {
                Theme = theme,
                Header = new ContentPresenter() { Content = "Sukces" },
                Content = new ContentPresenter() { Content = message },
                OkButtonContent = new ContentPresenter() { Content = "Ok"},
                IconContent = new ContentPresenter()
                {
                    Content = new FinderImage()
                    {
                        DisplayBaseImageColor = DBBuffers.BitmapMode.Green,
                        IsBaseImageFromResources = true,
                        BaseImageFolderInResources = DBBuffers.ImagesFolder.OthersIcon,
                        BaseImageNameInResources = "Button_OK.png",
                        HorizontalTextPosition = HorizontalTextPosition.Right,
                    }
                }
            });
        }

        public static void Confirm(string message, EventHandler<WindowClosedEventArgs> onClose)
        {
            Confirm(message, onClose, Buttons.YesNo);
        }


    }
}
