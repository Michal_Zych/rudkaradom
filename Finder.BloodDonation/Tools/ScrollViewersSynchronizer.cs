﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;

namespace Finder.BloodDonation.Tools
{
    public class ScrollViewersSynchronizer
    {
        private static Dictionary<ScrollViewer, string> _registeredScrolls;
        
        public static void Register(ScrollViewer scroll, string group)
        {
            const string propertyName =  "HorizontalOffset";

            if (_registeredScrolls == null) _registeredScrolls = new Dictionary<ScrollViewer, string>();

            if (_registeredScrolls.ContainsKey(scroll))
                return;

            _registeredScrolls.Add(scroll, group);
            
            Binding b = new Binding(propertyName) { Source = scroll };
            var prop = System.Windows.DependencyProperty.RegisterAttached("ListenAttached" + propertyName,
                    typeof(object),
                    typeof(UserControl),
                    new System.Windows.PropertyMetadata((d,e)=> UpdateScrolls((ScrollViewer)d,(double)e.NewValue)));
            scroll.SetBinding(prop, b);
        }

        public static void Register(ListBox listBox, string group)
        {
            var scrollViewer = GetDescendantByType(listBox, typeof(ScrollViewer)) as ScrollViewer;
            if (scrollViewer != null)
                Register(scrollViewer, group);
        }



        private static void UpdateScrolls(ScrollViewer source, double value)
        {
            var gr = _registeredScrolls[source];

            var scrolls = _registeredScrolls.Where(r => (r.Value == gr) && ((ScrollViewer)r.Key != source) && (((ScrollViewer)r.Key).ScrollableWidth > 0)).ToList();
            foreach (var k in scrolls)
            {
                if ((ScrollViewer)k.Key != source)
                {
                    if (value > scrolls.Min(x => x.Key.ScrollableWidth))
                        value = scrolls.Min(x => x.Key.ScrollableWidth);

                    var target = ((ScrollViewer)k.Key);

                    target.ScrollToHorizontalOffset(value);

                }
            }
        }

        public static UIElement GetDescendantByType(UIElement element, Type type)
        {
            if (element == null)
            {
                return null;
            }
            if (element.GetType() == type)
            {
                return element;
            }
            UIElement foundElement = null;
            if (element is Control)
            {
                (element as Control).ApplyTemplate();
            }
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(element); i++)
            {
                UIElement visual = VisualTreeHelper.GetChild(element, i) as UIElement;
                foundElement = GetDescendantByType(visual, type);
                if (foundElement != null)
                {
                    break;
                }
            }
            return foundElement;
        }


        public static bool IsGroupRegistered(string key)
        {
            if (_registeredScrolls == null)
            {
                return false;
            }
            return _registeredScrolls.Values.Contains(key);
        }
    }
}
