﻿using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Tools
{
    public class UsersInfoBuffer
    {
        private static IList<UserInfoDto> _usersList;

        public static IList<UserInfoDto> Get()
        {
            return _usersList;
        }

        public static void Refresh(Action<IList<UserInfoDto>> callback)
        {
            var proxy = ServiceFactory.GetService<UnitsServiceClient>(ServicesUri.UnitsService);
            var items = new List<UserInfoDto>();

            proxy.GetUsersForUnitCompleted += (s, e) =>
                {
                    _usersList = e.Result;
                    callback(e.Result);
                };
            proxy.GetUsersForUnitAsync(1);
        }

        public static void Refresh()
        {
            Refresh(u => u = u);
        }

    }
}
