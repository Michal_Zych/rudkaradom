﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.DragDrop;

namespace Finder.BloodDonation.Behaviors
{
	public class UnitTreeDragDropBehavior : Behavior<FrameworkElement>
	{
	    private const bool IsDragable = false;

		protected override void OnAttached()
		{
			base.OnAttached();

			RadDragAndDropManager.AddDragQueryHandler(this.AssociatedObject, OnDragQuery);
			RadDragAndDropManager.AddDragInfoHandler(this.AssociatedObject, OnDragInfo);

			RadDragAndDropManager.AddDropQueryHandler(this.AssociatedObject, OnDropQuery);
			RadDragAndDropManager.AddDropInfoHandler(this.AssociatedObject, OnDropInfo);

			//(AssociatedObject).AddHandler(RadDragAndDropManager.DragQueryEvent, new EventHandler<DragDropQueryEventArgs>(OnDragQuery), true);
		}

		private void OnDragQuery(object sender, DragDropQueryEventArgs e)
		{
		    
		        System.Diagnostics.Debug.WriteLine("OnDragQuery {0}/{1}", e.Options.Payload, e.Options.Destination);

		        if (e.Options.Status == DragStatus.DragQuery && sender != null)
		        {
		            System.Diagnostics.Debug.WriteLine("on drag query status: drag query, sender: {0}", sender.GetType());
		        }
		    
		}

	    private void OnDragInfo(object sender, DragDropEventArgs e)
	    {
	        System.Diagnostics.Debug.WriteLine("OnDragInfo {0}/{1} status:{2}", e.Options.Payload, e.Options.Destination,
	            e.Options.Status);

	        if (e.Options.Status == DragStatus.DragInProgress)
	        {
	            if (e.Options.Destination != null)
	                System.Diagnostics.Debug.WriteLine("check if dropping is possible on {0} of type {1}",
	                    e.Options.Destination, e.Options.Destination.GetType());
	            else
	            {
	                System.Diagnostics.Debug.WriteLine("on drag info progress destination null");
	            }
	        }

	        if (e.Options.Status == DragStatus.DragComplete)
	        {
	            if (e.Options.Destination == null) return;

	            System.Diagnostics.Debug.WriteLine("ondraginfo drag complete here in {0}", e.Options.Destination);
	            // move units here

	            if (e.Options.Destination.ToString().Contains("05"))
	            {
	                e.Handled = true;
	                return;
	            }
	        }
	    }

	    private void OnDropQuery(object sender, DragDropQueryEventArgs e)
		{
			System.Diagnostics.Debug.WriteLine("OnDropQuery {0}/{1} status:{2}", e.Options.Payload, e.Options.Destination, e.Options.Status);
		}

	    private void OnDropInfo(object sender, DragDropEventArgs e)
	    {
	        System.Diagnostics.Debug.WriteLine("OnDropInfo {0}/{1}", e.Options.Payload, e.Options.Destination);

	        if (e.Options.Status == DragStatus.DropComplete)
	        {
	            System.Diagnostics.Debug.WriteLine("drop complete (info only) on " + e.Options.Destination);
	        }

	    }
	}
}
