﻿using System;
using System.Windows.Interactivity;
using System.Windows.Controls;
using System.Windows;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Common.Authentication;
using FinderFX.SL.Core.Authentication;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Tabs.UnitsList;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Behaviors
{
    public class RoomMoveBehavior : Behavior<Room>
    {
        private static Brush BorderBrush = new SolidColorBrush(Colors.Yellow);
        private static SolidColorBrush blueBrush = new SolidColorBrush(Colors.Blue);
        private static SolidColorBrush whiteBrush = new SolidColorBrush(Colors.White);
        private static SolidColorBrush redBrush = new SolidColorBrush(Colors.Red);

        private Ellipse BottomRightEllipse = null;
        private Ellipse BottomRightEllipse_ = null;
        private Border OverBorder = null;
        private Rectangle redRectangle = null;

        private bool moving = false;
        private bool moving_tl = false;
        private bool deleting = false;

        private static bool s_moving = false;
        private static bool s_moving_tl = false;

        private double offSetX;
        private double offSetY;

        private double offSetXTL;
        private double offSetYTL;

        private double offSetWidth;
        private double offSetHeight;

        public RoomMoveBehavior()
            : base()
        {

        }

        protected override void OnAttached()
        {
            base.OnAttached();

            this.AssociatedObject.MouseEnter += new System.Windows.Input.MouseEventHandler(AssociatedObject_MouseEnter);
            this.AssociatedObject.MouseLeave += new System.Windows.Input.MouseEventHandler(AssociatedObject_MouseLeave);
            this.AssociatedObject.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(AssociatedObject_MouseLeftButtonDown);

            Grid MainGrid = this.AssociatedObject.MainGrid;
            if (MainGrid == null)
            {
                Canvas c = this.AssociatedObject.Parent as Canvas;
                MainGrid = c.Parent as Grid;
            }

            MainGrid.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(AssociatedObjectMainGrid_MouseLeftButtonUp);
            MainGrid.MouseMove += new System.Windows.Input.MouseEventHandler(AssociatedObjectMainGrid_MouseMove);
        }

        void AssociatedObjectMainGrid_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (moving)
            {
                Canvas parent = (Canvas)this.AssociatedObject.Parent;
                Point p = e.GetPosition(parent);
                double x = p.X - offSetX;
                double y = p.Y - offSetY;

                this.AssociatedObject.SetValue(Canvas.LeftProperty, x);
                this.AssociatedObject.SetValue(Canvas.TopProperty, y);
                ((RoomPlanViewModel)this.AssociatedObject.DataContext).SizeLocationChanged_Command();
            }

            if (moving_tl)
            {
                Canvas parent1 = this.AssociatedObject.Parent as Canvas;
                Point p = e.GetPosition(parent1);
                double x = p.X - offSetXTL;
                double y = p.Y - offSetYTL;
                offSetXTL = p.X;
                offSetYTL = p.Y;
                this.AssociatedObject.SetValue(Canvas.LeftProperty, Canvas.GetLeft(this.AssociatedObject) + x);
                this.AssociatedObject.SetValue(Canvas.TopProperty, Canvas.GetTop(this.AssociatedObject) + y);

                if (this.AssociatedObject.Width - x > 0)
                {
                    this.AssociatedObject.Width -= x;
                }

                if (this.AssociatedObject.Height - y > 0)
                {
                    this.AssociatedObject.Height -= y;
                }
                ((RoomPlanViewModel)this.AssociatedObject.DataContext).SizeLocationChanged_Command();
            }
        }

        void AssociatedObjectMainGrid_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                Canvas canvas = this.AssociatedObject.Parent as Canvas;

                if (canvas != null)
                {
                    if (moving || moving_tl)
                    {
                        Point tl = new Point(Canvas.GetTop(this.AssociatedObject), Canvas.GetLeft(this.AssociatedObject));
                        Point br = new Point(tl.X + this.AssociatedObject.Height, tl.Y + this.AssociatedObject.Width);

                        tl.X = tl.X / (canvas).Height;
                        tl.Y = tl.Y / (canvas).Width;

                        br.X = br.X / (canvas).Height;
                        br.Y = br.Y / (canvas).Width;

                        ((RoomPlanViewModel)this.AssociatedObject.DataContext).MoveUnit_Command(
                            tl,
                            br);
                    }
                    moving = false;
                    moving_tl = false;
                    (this.AssociatedObject.MainGrid).ReleaseMouseCapture();

                    if (clear_marks)
                    {
                        ClearMarkers();
                    }

                    clear_marks = false;
                }
            }
            catch (Exception exc)
            {

            }
        }

        void AssociatedObject_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (deleting)
            {
                deleting = false;
                return;
            }
            (this.AssociatedObject.MainGrid).CaptureMouse();

            moving = true;
            Point offset = e.GetPosition(this.AssociatedObject);
            offSetX = offset.X;
            offSetY = offset.Y;
        }

        void AssociatedObject_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (!s_moving_tl && !s_moving)
            {
                ClearMarkers();
            }
            else
            {
                clear_marks = true;
            }
        }

        private bool clear_marks = false;
        void ClearMarkers()
        {
            this.AssociatedObject.MainGrid.Children.Remove(OverBorder);
            this.AssociatedObject.MainGrid.Children.Remove(BottomRightEllipse);
            this.AssociatedObject.MainGrid.Children.Remove(BottomRightEllipse_);
            this.AssociatedObject.MainGrid.Children.Remove(redRectangle);
        }

        void AssociatedObject_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (!s_moving && !s_moving_tl)
            {
                clear_marks = false;
                OverBorder = new Border();
                OverBorder.BorderThickness = new Thickness(4);
                OverBorder.BorderBrush = BorderBrush;
                this.AssociatedObject.MainGrid.Children.Add(OverBorder);

                BottomRightEllipse = new Ellipse();
                BottomRightEllipse.Height = 13;
                BottomRightEllipse.Width = 13;
                BottomRightEllipse.StrokeThickness = 2;
                BottomRightEllipse.Stroke = whiteBrush;
                BottomRightEllipse.Fill = blueBrush;
                BottomRightEllipse.VerticalAlignment = VerticalAlignment.Top;
                BottomRightEllipse.HorizontalAlignment = HorizontalAlignment.Left;
                BottomRightEllipse.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(TopLeftEllipse_MouseLeftButtonDown);

                this.AssociatedObject.MainGrid.MouseMove += new System.Windows.Input.MouseEventHandler(AssociatedObjectMainGrid_MouseMove);
                this.AssociatedObject.MainGrid.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(AssociatedObjectMainGrid_MouseLeftButtonUp);
                this.AssociatedObject.MainGrid.Children.Add(BottomRightEllipse);

                /*redRectangle = new Rectangle();
                redRectangle.Height = 13;
                redRectangle.Width = 13;
                redRectangle.StrokeThickness = 2;
                redRectangle.Stroke = whiteBrush;
                redRectangle.Fill = redBrush;
                redRectangle.VerticalAlignment = VerticalAlignment.Top;
                redRectangle.HorizontalAlignment = HorizontalAlignment.Right;
                redRectangle.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(redRectangle_MouseLeftButtonDown);
                this.AssociatedObject.MainGrid.Children.Add(redRectangle);*/
            }
        }

        void redRectangle_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            //moving = false;
            //moving_tl = false;
            //s_moving = false;
            //s_moving_tl = false;
            moving = false;
            moving_tl = false;
            (this.AssociatedObject.MainGrid).ReleaseMouseCapture();
            deleting = true;
            ((RoomPlanViewModel)this.AssociatedObject.DataContext).RemoveUnit_Command();
        }

        #region Top Left Resizer
        void TopLeftEllipse_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            moving_tl = true;
            Point offset = e.GetPosition(this.AssociatedObject.Parent as Canvas);
            offSetXTL = offset.X;
            offSetYTL = offset.Y;
            //offSetWidth = this.AssociatedObject.Width;
            //offSetHeight = this.AssociatedObject.Height;

            e.Handled = true;
        }
        #endregion

        protected override void OnDetaching()
        {
            base.OnDetaching();


        }

        public void RefreshControlState()
        {

        }
    }
}
