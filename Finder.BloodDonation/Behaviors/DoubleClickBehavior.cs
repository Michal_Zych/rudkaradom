﻿namespace Finder.BloodDonation.Behaviors
{
    using System;
    using System.Net;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Documents;
    using System.Windows.Ink;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Animation;
    using System.Windows.Shapes;
    using System.Windows.Interactivity;
    using System.Windows.Data;

    public class DoubleClickBehavior : Behavior<FrameworkElement>
    {
        public static readonly DependencyProperty CommandProperty =
                            DependencyProperty.RegisterAttached("Command", typeof(ICommand),
                            typeof(DoubleClickBehavior), new PropertyMetadata(null));


        public static ICommand GetCommand(DependencyObject obj)
        {
            return obj.GetValue(CommandProperty) as ICommand;
        }

        public static void SetCommand(DependencyObject obj, ICommand value)
        {
            obj.SetValue(CommandProperty, value);
        }

        private MouseClickManager click_manager = null;

        protected override void OnAttached()
        {
            click_manager = new MouseClickManager(300);
            click_manager.DoubleClick += new MouseButtonEventHandler(click_manager_DoubleClick);
            this.AssociatedObject.MouseLeftButtonDown += click_manager.HandleClick;
        }

        void click_manager_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            ICommand confirmationCommand = GetCommand(this.AssociatedObject);
            if (confirmationCommand != null)
            {
                confirmationCommand.Execute(null);
            }
        }
    }
}
