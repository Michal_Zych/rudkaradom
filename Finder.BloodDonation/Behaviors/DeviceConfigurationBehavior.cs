﻿using System;
using System.Windows.Interactivity;
using System.Windows.Controls;
using System.Windows;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Common.Authentication;
using FinderFX.SL.Core.Authentication;
using Finder.BloodDonation.Model.Authentication;

namespace Finder.BloodDonation.Behaviors
{
    public class DeviceConfigurationBehavior : Behavior<FrameworkElement>
    {
        public static readonly DependencyProperty RequiredPermissionProperty =
            DependencyProperty.Register(
            "RequiredPermission", typeof(string),
            typeof(DeviceConfigurationBehavior),
            null
        );

        public string RequiredPermission
        {
            get { return (string)GetValue(RequiredPermissionProperty); }
            set { SetValue(RequiredPermissionProperty, value); }
        }

        public static readonly DependencyProperty DeviceUnitProperty =
            DependencyProperty.Register(
            "DeviceUnit", typeof(IUnit),
            typeof(DeviceConfigurationBehavior),
            new PropertyMetadata(DeviceUnitPropertyChanged)
        );

        public IUnit DeviceUnit
        {
            get { return (IUnit)GetValue(DeviceUnitProperty); }
            set { SetValue(DeviceUnitProperty, value); }
        }

        public DeviceConfigurationBehavior()
            : base()
        {

        }

        protected override void OnAttached()
        {
            base.OnAttached();
            RefreshControlState();
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
        }

        public void RefreshControlState()
        {
            if (DeviceUnit != null && !string.IsNullOrEmpty(RequiredPermission))
            {
                BloodyUser user = User.Current as BloodyUser;
                if (user.HasPermission(DeviceUnit.Identity, RequiredPermission))
                {
                    //this.AssociatedObject.IsEnabled = true;
                    this.AssociatedObject.Visibility = Visibility.Visible;
                }
                else
                {
                    //this.AssociatedObject.IsEnabled = false;
                    this.AssociatedObject.Visibility = Visibility.Collapsed;
                }
            }
            else
            {
                //this.AssociatedObject.IsEnabled = true;
                this.AssociatedObject.Visibility = Visibility.Visible;
            }
        }

        private static void DeviceUnitPropertyChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            DeviceConfigurationBehavior dcb = sender as DeviceConfigurationBehavior;

            if (dcb != null)
            {
                dcb.RefreshControlState();
            }
        }
    }
}
