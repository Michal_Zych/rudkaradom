﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Interactivity;
using Finder.BloodDonation.Model.Enum;
using Telerik.Windows.Controls.DragDrop;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Telerik.Windows.Controls.TreeView;
using Finder.BloodDonation.Tabs.Contents;
using Finder.BloodDonation.Layout;
using FinderFX.SL.Core.Authentication;
using Finder.BloodDonation.Common.Layout;
using Telerik.Windows.DragDrop;
using Finder.BloodDonation.Helpers;

namespace Finder.BloodDonation.Behaviors
{
    public class CommandDragDropBehavior : Behavior<FrameworkElement>
    {
        private bool IsDragable = false;

        protected override void OnAttached()
        {
            base.OnAttached();

            RadDragAndDropManager.DragCueOffset = new Point(200, 0);

            RadDragAndDropManager.AddDragQueryHandler(this.AssociatedObject, OnDragQuery);
            RadDragAndDropManager.AddDragInfoHandler(this.AssociatedObject, OnDragInfo);

            RadDragAndDropManager.AddDropQueryHandler(this.AssociatedObject, OnDropQuery);
            RadDragAndDropManager.AddDropInfoHandler(this.AssociatedObject, OnDropInfo);

            DragDropManager.AddDragInitializeHandler(this.AssociatedObject, OnDragInitialized);
        }

        public void OnDragQuery(object sender, DragDropQueryEventArgs e)
        {
            if (IsDragable)
            {
                bool res = false;
                var listBox = sender as System.Windows.Controls.ListBox;

                if (listBox != null && listBox.Items.Any())
                {

                    res = (e.Options.Payload as IList).Count > 0;
                }

                e.QueryResult = true;
                e.QueryResult = res;
                //e.Handled = true;

            }
            //System.Diagnostics.Debug.WriteLine("first step OnDragQuery2 " + e.Options.Status + " selected items: " + ((selectedItems != null)? selectedItems.Count.ToString() : "-"));
        }

        // dragging away from listbox
        public void OnDragInfo(object sender, DragDropEventArgs e)
        {
            if (IsDragable)
            {
                if (e.Options.Payload == null) return;
                var listBox = sender as System.Windows.Controls.ListBox;
                var draggedItems = e.Options.Payload as IEnumerable;
                int draggedCount;


                draggedCount = (e.Options.Payload as IList).Count;

                if (e.Options.Status == DragStatus.DragInProgress)
                {
                    if (e.Options.DragCue == null)
                    {
                        TreeViewDragCue dragCue = new TreeViewDragCue();

                        dragCue.ItemTemplate = DragCueTemplate;

                        dragCue.ItemsSource = draggedItems;
                        if (draggedCount == 1)
                        {
                            dragCue.ItemsSource = new List<string>() {"Przenoszenie preparatu"};
                        }
                        else
                        {
                            dragCue.ItemsSource = new List<string>()
                            {
                                String.Format("Przenoszenie {0} preparatów", draggedCount)
                            };
                        }
                        dragCue.DragTooltipContentTemplate = null;
                        //dragCue.DragTooltipContent = "-";

                        e.Options.DragCue = dragCue;
                    }
                }

                if (e.Options.Status == DragStatus.DragComplete)
                {
                    if (e.Options.Destination == null) return;

                    if (!(e.Options.Destination.DataContext is IUnit))
                    {
                        System.Diagnostics.Debug.WriteLine("draginfo2 dest.datacontext not iunit" +
                                                           e.Options.Destination.DataContext.GetType());
                        return;
                    }
                    var destinationItem = e.Options.Destination.DataContext as UnitViewModel;


                    //R.Sz. - poprawka w Bydgoszczy - nie moznabyło przenosić preparatów do Bufora
                    //if (destinationItem.UnitType.CanBeDragged())
                    //if (destinationItem != null && (destinationItem.UnitType.CanBeDragged()))
                    if (destinationItem != null &&
                        destinationItem.UnitType.IsStore()
                        //(destinationItem.UnitType == UnitType.THERMOMETER ||
                        //destinationItem.UnitType == UnitType.THERMOHYGROMETER ||
                        //destinationItem.UnitType == UnitType.BASKET ||
                        //destinationItem.UnitType == UnitType.BOX)
                    )
                    {

                        var count = 0;
                        IList source = listBox.ItemsSource as IList;
                        foreach (object draggedItem in draggedItems)
                        {
                            source.Remove(draggedItem);
                            count++;
                        }
                        System.Diagnostics.Debug.WriteLine("removing items from source: " + count);

                        var userId = User.Current.ID;


                        bool notMoved = true;

                        if (!(e.Source is ListBox))
                        {
                            DependencyObject parent = VisualTreeHelper.GetParent((e.Source as ListBoxItem));
                            var p = parent as ListBox;

                            ((e.Source as ListBoxItem).Parent as ListBox).UpdateLayout();
                        }
                        else
                        {
                            (e.Source as ListBox).UpdateLayout();
                        }

                        //if (e.Options.Source.DataContext is ContentDetailsViewModel)
                        //{
                        //    (e.Options.Source.DataContext as ContentDetailsViewModel).Refresh();
                        //}


                    }
                }

                System.Diagnostics.Debug.WriteLine("draginfo2 " + e.Options.Status);
            }
        }

        // when dropping onto listbox
        public void OnDropQuery(object sender, DragDropQueryEventArgs e)
        {
            //var treeViewItem = e.Options.Destination as Telerik.Windows.Controls.RadTreeViewItem;
            //if (treeViewItem != null && treeViewItem.DropPosition != Telerik.Windows.Controls.DropPosition.Inside)
            //{
            //	e.QueryResult = false;
            //}
        }

        public void OnDropInfo(object sender, DragDropEventArgs e)
        {
            if (IsDragable)
            {
                var payload = e.Options.Payload as ICollection;

                var cue = e.Options.DragCue as TreeViewDragCue;
                cue.DragTooltipContentTemplate = null;

                if (e.Options.Status == DragStatus.DropPossible)
                {
                    cue.DragActionContent = null;
                    cue.IsDropPossible = true;

                    System.Diagnostics.Debug.WriteLine("drop possible on " + e.Options.Destination);
                }
                else if (e.Options.Status == DragStatus.DropImpossible)
                {
                    cue.DragActionContent = null;
                    cue.IsDropPossible = false;
                    System.Diagnostics.Debug.WriteLine("drop impossible on " + e.Options.Destination);
                }
                else if (e.Options.Status == DragStatus.DropComplete)
                {
                    System.Diagnostics.Debug.WriteLine("drop complete on " + e.Options.Destination);
                }

                System.Diagnostics.Debug.WriteLine("OnDropInfo2 " + e.Options.Status);
            }
        }

        private void OnDragInitialized(object sender, DragInitializeEventArgs e)
        {
            if (IsDragable)
            {
                var p = new Point() {X = 100, Y = 40};
                //var p = new Point() { X = e.RelativeStartPoint.X + 30, Y = e.RelativeStartPoint.Y + 30 };
                e.DragVisualOffset = p;
            }
        }

        public ICommand DragCommand
        {
            get { return (ICommand)GetValue(DragCommandProperty); }
            set { SetValue(DragCommandProperty, value); }
        }

        public static readonly DependencyProperty DragCommandProperty =
            DependencyProperty.Register("DragCommand", typeof(ICommand), typeof(CommandDragDropBehavior), new PropertyMetadata(null));

        public ICommand DropCommand
        {
            get { return (ICommand)GetValue(DropCommandProperty); }
            set { SetValue(DropCommandProperty, value); }
        }

        public static readonly DependencyProperty DropCommandProperty =
            DependencyProperty.Register("DropCommand", typeof(ICommand), typeof(CommandDragDropBehavior), new PropertyMetadata(null));

        public DataTemplate DragCueTemplate
        {
            get { return (DataTemplate)GetValue(DragCueTemplateProperty); }
            set { SetValue(DragCueTemplateProperty, value); }
        }

        public static readonly DependencyProperty DragCueTemplateProperty =
            DependencyProperty.Register("DragCueTemplate", typeof(DataTemplate), typeof(CommandDragDropBehavior), new PropertyMetadata(null));
    }

    public class DragDropParameter
    {
        public object DraggedItem { get; set; }
        public IEnumerable ItemsSource { get; set; }
        public Telerik.Windows.Controls.DragDrop.DragStatus DragStatus { get; set; }
    }
}
