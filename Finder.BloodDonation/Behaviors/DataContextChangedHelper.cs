﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace Finder.BloodDonation.Behaviors
{
	public interface IDataContextChangedHandler<T> where T : FrameworkElement
	{
		void DataContextChangedm(T sender, DependencyPropertyChangedEventArgs e);
	}

	public static class DataContextChangedHelper<T> where T : FrameworkElement, IDataContextChangedHandler<T>
	{
		private const string INTERNAL_CONTEXT = "InternalDataContext";

		public static readonly DependencyProperty InternalDataContextProperty =
			DependencyProperty.Register(INTERNAL_CONTEXT,
										typeof(Object),
										typeof(T),
										new PropertyMetadata(_DataContextChanged));

		private static void _DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
		{
			T control = (T)sender;
			control.DataContextChangedm(control, e);
		}

		public static void Bind(T control)
		{
			control.SetBinding(InternalDataContextProperty, new Binding());
		}
	}

}
