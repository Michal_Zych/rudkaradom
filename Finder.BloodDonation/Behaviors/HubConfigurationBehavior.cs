﻿using System;
using System.Windows.Interactivity;
using System.Windows.Controls;
using System.Windows;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Common.Authentication;
using FinderFX.SL.Core.Authentication;
using Finder.BloodDonation.Model.Authentication;

namespace Finder.BloodDonation.Behaviors
{
    public class HubConfigurationBehavior : Behavior<FrameworkElement>
    {
        public static readonly DependencyProperty RequiredPermissionProperty =
            DependencyProperty.Register(
            "RequiredPermission", typeof(string),
            typeof(HubConfigurationBehavior),
            null
        );

        public string RequiredPermission
        {
            get { return (string)GetValue(RequiredPermissionProperty); }
            set { SetValue(RequiredPermissionProperty, value); }
        }

        public static readonly DependencyProperty HubUnitProperty =
            DependencyProperty.Register(
            "HubUnit", typeof(IUnit),
            typeof(HubConfigurationBehavior),
            new PropertyMetadata(HubUnitPropertyChanged)
        );

        public IUnit HubUnit
        {
            get { return (IUnit)GetValue(HubUnitProperty); }
            set { SetValue(HubUnitProperty, value); }
        }

        public HubConfigurationBehavior()
            : base()
        {

        }

        protected override void OnAttached()
        {
            base.OnAttached();
            RefreshControlState();
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
        }

        public void RefreshControlState()
        {
            if (HubUnit != null && !string.IsNullOrEmpty(RequiredPermission))
            {
                BloodyUser user = User.Current as BloodyUser;
                if (user.HasPermission(HubUnit.Identity, RequiredPermission))
                {
                    //this.AssociatedObject.IsEnabled = true;
                    this.AssociatedObject.Visibility = Visibility.Visible;
                }
                else
                {
                    //this.AssociatedObject.IsEnabled = false;
                    this.AssociatedObject.Visibility = Visibility.Collapsed;
                }
            }
            else
            {
                //this.AssociatedObject.IsEnabled = true;
                this.AssociatedObject.Visibility = Visibility.Visible;
            }
        }

        private static void HubUnitPropertyChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            HubConfigurationBehavior dcb = sender as HubConfigurationBehavior;

            if (dcb != null)
            {
                dcb.RefreshControlState();
            }
        }
    }
}
