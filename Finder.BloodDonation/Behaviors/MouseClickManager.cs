﻿namespace Finder.BloodDonation.Behaviors
{
    using System;
    using System.Windows.Input;
    using System.Windows.Controls;
    using System.Threading;
    using System.Diagnostics;
    using DanielVaughan.Logging;
    using System.Windows.Media.Animation;

    /// <summary>
    /// Class to attach mouse click events to UIElements
    /// </summary>
    public class MouseClickManager
    {
        private static ILog logger = LogManager.GetLog(typeof(MouseClickManager));

        #region Private members
        private event MouseButtonEventHandler _click;
        private event MouseButtonEventHandler _doubleClick;
        private object _sender;
        private MouseButtonEventArgs _e;
        private Storyboard click_delay = null;
        private bool isClicked = false;
        private int DoubleClickTimeout { get; set; }
        #endregion

        public MouseClickManager(int doubleClickTimeout)
        {
            this.DoubleClickTimeout = doubleClickTimeout;
            this.click_delay = new Storyboard();
            this.click_delay.Duration = new System.Windows.Duration(new TimeSpan(0, 0, 0, 0, doubleClickTimeout));
            this.click_delay.Completed += new EventHandler(click_delay_Completed);
        }

        #region Events
        public event MouseButtonEventHandler Click
        {
            add { _click += value; }
            remove { _click -= value; }
        }

        public event MouseButtonEventHandler DoubleClick
        {
            add { _doubleClick += value; }
            remove { _doubleClick -= value; }
        }
        #endregion

        void click_delay_Completed(object sender, EventArgs e)
        {
            isClicked = false;
        }

        public void HandleClick(object sender, MouseButtonEventArgs e)
        {
            this._sender = sender;
            this._e = e;

            if (!isClicked)
            {
                isClicked = true;
                if (_click != null)
                {
                    _click(this._sender, this._e);
                    e.Handled = true;
                }
            }
            else
            {
                if (_doubleClick != null)
                {
                    _doubleClick(sender, e);
                    e.Handled = true;
                }
            }

            this.click_delay.Stop();
            this.click_delay.Begin();
        }


    }
}
