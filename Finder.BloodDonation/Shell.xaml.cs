﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Login;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.UsersService;
using FinderFX.SL.Core.Communication;

namespace Finder.BloodDonation
{
    public partial class Shell : UserControl, IRootControl
    {
        private UsersServiceClient Proxy { get; set; }

        public Shell()
        {
            InitializeComponent();

        }

        #region IRootControl Members

        public void ShowContentControl(UIElement contentControl)
        {
            this.LayoutRoot.Children.Clear();
            this.LayoutRoot.Children.Add(contentControl);
        }

        public void ShowLoginControl(ILoggingControl loginControl)
        {
            this.LayoutRoot.Children.Clear();
            this.LayoutRoot.Children.Add(loginControl as UIElement);
            

			LoginView login = loginControl as LoginView;
			if (loginControl != null)
			{
				login.ClearCredentials();
			}
        }

        #endregion

        #region IErrorNotifier Members

       public void ShowErrorNotification(string message)
        {
            return;
           Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                ErrorWindow.CreateNew(message).Show();
            });
        }
       
        #endregion

		public void LoadingModuleCompleted(string moduleName)
		{
			throw new NotImplementedException();
		}

		public void LoadingModuleStarting(string moduleName)
		{
			throw new NotImplementedException();
		}
	}
}
