﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using Finder.BloodDonation.Model.Dto;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.UsersService;
using System.Linq;
using System.Diagnostics;
using Finder.BloodDonation.DBBuffers;
using Finder.BloodDonation.Helpers;


namespace Finder.BloodDonation.Settings
{
    public class Configuration
    {
        private Dictionary<string, SettingItem> configuration;
        private static Configuration current;
        public static Configuration Current
        {
            get
            {
                if (current == null)
                {
                    current = new Configuration();
                }
                return current;
            }
        }

        public void Reload_(Action action)
        {
            BitmapBuffer.Initialize();
            var proxy = ServiceFactory.GetService<UsersServiceClient>(ServicesUri.UsersService);
            proxy.GetSettingsTCompleted += new EventHandler<GetSettingsTCompletedEventArgs>(proxy_GetConfigurationtCompleted);

            proxy.GetSettingsTAsync(action);
        }


        private void proxy_GetConfigurationtCompleted(object sender, GetSettingsTCompletedEventArgs e)
        {
            configuration = new Dictionary<string, SettingItem>();

            if(e.Result != null)
                foreach(var item in e.Result)
                {
                    if (item.Name.Equals("RegistrationWardId"))
                        UnitTypeOrganizer.SetRegistrationUnitId(int.Parse(item.Value));
                    if (item.Name.Equals("UnassignedPatientsUnitId"))
                        UnitTypeOrganizer.SetUnassignedPatientsUnitId(int.Parse(item.Value));

                    configuration[item.Name.ToUpper()] = item;
                }
                if (e.UserState != null)
                {
                    Action callBack = (Action)e.UserState;
                    callBack();
                }

            /*configuration = new Dictionary<string, ConfigurationItemDto>();
            if (e.Result != null)
            {
                foreach (var item in e.Result)
                {
                    configuration[item.Name.ToUpper()] = item;
                }
            }
            if (e.UserState != null)
            {
                Action callBack = (Action)e.UserState;
                callBack();
            }*/
        }

        /*public IEnumerable<string> GetSections()
        {
            return  from x in configuration.Values
                    orderby x.Section
                    select x.Section;
        }*/

        public string Get(string key)
        {
            key = key.ToUpper();
            if(configuration.ContainsKey(key))
            {
                return configuration[key].Value;
            }
            return null;
        }
        
        public string this[string setting]
        {
            get { return Get(setting); }
            set { throw new NotImplementedException(); }
        }

        public SettingItem GetItem(string key)
        {
            key = key.ToUpper();
            if(configuration.ContainsKey(key))
            {
                return configuration[key];
            }
            return null;
        }

        public int GetInt(string key)
        {
            bool res;
            int result;
            var s = Get(key);

            if(bool.TryParse(s, out res))
            {
                if (res == false)
                    return 0;
                else
                    return 1;
            }
            else if (int.TryParse(s, out result))
            {
                return result;
            }
            throw new Exception("Nieznana konfiuracja " + key);
            Debug.WriteLine("*** Nierozpoznana wartość int = '" + s + "' dla parametru: " + key);
            return 0;
        }
    }
}
    