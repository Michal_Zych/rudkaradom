﻿using System;
using System.IO;
using System.IO.IsolatedStorage;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Finder.BloodDonation.Common.Authentication;
using System.Collections.Generic;
using System.Linq;
using Finder.BloodDonation.DataTable;

namespace Finder.BloodDonation.Settings
{
    public static class LocalStorageManager
    {
        private static IsolatedStorageFile _isoStore;
        public static IsolatedStorageFile IsoStore
        {
            get { return _isoStore ?? (_isoStore = IsolatedStorageFile.GetUserStoreForApplication()); }
        }

        private static string FolderName
        {
            get { return BloodyUser.Current.ID.ToString(); }
        }

        private static string FileName(string key)
        {
            return string.Format("{0}\\{1}.txt", FolderName, key);
        }

        private static void CreateSettingsDirectory()
        {
            try
            {
                if (!IsoStore.DirectoryExists(FolderName))
                {
                    IsoStore.CreateDirectory(FolderName);
                }
            }
            catch { }
        }

        public static void SaveSettingsCollection<T>(string key, ObservableCollection<T> dataList) where T : class
        {
            CreateSettingsDirectory();
            try
            {
                using (IsolatedStorageFileStream stream = new IsolatedStorageFileStream(FileName(key), FileMode.Create, IsoStore))
                {
                    DataContractSerializer dcs = new DataContractSerializer(typeof(ObservableCollection<T>));
                    dcs.WriteObject(stream, dataList);
                }
            }
            catch { }
        }

        public static void SaveSettings<T>(string key, IList<T> dataList)
        {
            string s = dataList.ToCommaSeparatedValues();
            SaveSettings(key, s);
        }

        public static void SaveSettings(string key, string value)
        {
            CreateSettingsDirectory();
            using (var stream = IsoStore.CreateFile(FileName(key)))
            using (var writer = new StreamWriter(stream))
            {
                writer.Write(value);
            }
        }

        public static ObservableCollection<T> LoadSettingsCollection<T>(string key) where T : class
        {
            ObservableCollection<T> retval = new ObservableCollection<T>();

            CreateSettingsDirectory();

            try
            {
                using (IsolatedStorageFileStream stream = new IsolatedStorageFileStream(FileName(key), FileMode.OpenOrCreate, IsoStore))
                {
                    if (stream.Length > 0)
                    {
                        var t = new List<Type>();
                        t.Add(typeof(DataTableColumn));

                        DataContractSerializer dcs = new DataContractSerializer(typeof(ObservableCollection<T>), t);
                        retval = dcs.ReadObject(stream) as ObservableCollection<T>;
                    }
                }
            }
            catch
            {

            }
            return retval;
        }

        public static IList<string> LoadSettingsList(string key)
        {
            var s = LoadSettings(key);
            return s.Split(',').ToList();
        }

        public static string LoadSettings(string key)
        {
            string result = null;
            if (IsoStore.FileExists(FileName(key)))
            {
                using (var stream = IsoStore.OpenFile(FileName(key), FileMode.Open))
                using (var reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
            }
            return result;
        }


    }
}
