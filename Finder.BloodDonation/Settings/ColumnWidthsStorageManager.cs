﻿using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Settings
{
    public static class ColumnWidthsStorageManager
    {
        public static ObservableCollection<GridLength> LoadColumnWidths(ObservableCollection<GridLength> columnWidths, string key)
        {
            try
            {
                var list = LocalStorageManager.LoadSettingsList(key);
                if ((list != null) && (list.Count == columnWidths.Count))
                {
                    ObservableCollection<GridLength> collection = new ObservableCollection<GridLength>();
                    foreach (var s in list)
                    {
                        var len = new GridLength(Convert.ToDouble(s));
                        collection.Add(len);
                    }
                    return collection;
                }
            }
            catch { }
            return columnWidths;
        }

        public static void SaveColumnWidths(ObservableCollection<GridLength> columnWidths, string key)
        {
            LocalStorageManager.SaveSettings<GridLength>(key, columnWidths);
        }

    }
}
