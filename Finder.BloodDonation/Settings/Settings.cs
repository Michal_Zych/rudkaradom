﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Common.Authentication;
using Microsoft.Expression.Interactivity.Media;
using System.Diagnostics;
using Finder.BloodDonation;
using Finder.BloodDonation.UsersService;
using FinderFX.SL.Core.Communication;
using Telerik.Windows.Controls;

namespace Finder.BloodDonation.Settings
{
    public interface ISettings
    {
        void Reload(Action a);

        string Get(string setting);
        string this[string setting] { get; set; }
        IEnumerable<string> GetList(string settings);
        bool GetBool(string setting);
        int GetInt(string setting);
        Color GetColor(string setting);
        Brush OldDataBrush { get;}
    }

    public class Settings : ISettings
    {
        public const string BLOOD_LABEL_LINE_SEPARATOR = "|";

        private Dictionary<string, string> _currentSettings;
        private static Settings _current;
        public static ISettings Current
        {
            get
            {
                if (_current == null)
                {
                    _current = new Settings();
                }
                return _current;
            }
        }

        public string Get(string setting)
        {
            if (_currentSettings.ContainsKey(setting))
            {
                return _currentSettings[setting];
            }
            if (defaultSettings.ContainsKey(setting))
            {
                return defaultSettings[setting];
            }
            return "";
        }
        public string this[string setting]
        {
            get { return Get(setting); }
            set { throw new NotImplementedException(); }
        }


        public IEnumerable<string> GetList(string settings)
        {
            var setting = Get(settings);
            return setting.Split(',').ToList();
        }

        public bool GetBool(string setting)
        {
            bool result;
            var s = Get(setting);
            if (bool.TryParse(s, out result))
            {
                return result;
            }
            Debug.WriteLine("*** Nierozpoznana wartość bool = '" + s + "' dla parametru: " + setting);
            return false;
        }
        public int GetInt(string setting)
        {
            int result;
            var s = Get(setting);
            if (int.TryParse(s, out result))
            {
                return result;
            }
            Debug.WriteLine("*** Nierozpoznana wartość int = '" + s + "' dla parametru: " + setting);
            return 0;
        }

        public Color GetColor(string setting)
        {
            var s = Get(setting);
            try
            {
                return GetColorFromString(Get(setting));
            }
            catch
            {
                return GetColorFromString(defaultSettings[setting]);
            }
            
        }

        private Color GetColorFromString(string colorString)
        {
            var sections = colorString.Split(',');
            var isOK = true;
            byte i;
            foreach (var s in sections)
            {
                if (!byte.TryParse(s.Trim(), out i))
                {
                    isOK = false;
                    break;
                }
            }
            if (isOK)
            {
                if (sections.Length > 3)
                {
                    return Color.FromArgb(byte.Parse(sections[0]), byte.Parse(sections[1]), byte.Parse(sections[2]), byte.Parse(sections[3]));
                }
                else if (sections.Length > 2)
                {
                    return Color.FromArgb(255, byte.Parse(sections[0]), byte.Parse(sections[1]), byte.Parse(sections[2]));
                }

            }
            throw new ArgumentException();
        }



        public Settings()
        {
            Debug.WriteLine("************************************* Settings constructor");
            _currentSettings = new Dictionary<string, string>(defaultSettings);
            oldDataBrush = new SolidColorBrush(Colors.Yellow);
        }

        public void Reload(Action a)
        {
            Debug.WriteLine("************************************* Settings Reload");
            var proxy = ServiceFactory.GetService<UsersServiceClient>(ServicesUri.UsersService);
            proxy.GetSettingsListCompleted += new EventHandler<GetSettingsListCompletedEventArgs>(proxy_GetSettingsListCompleted);
            proxy.GetSettingsListAsync(a);
        }


        private void proxy_GetSettingsListCompleted(object sender, GetSettingsListCompletedEventArgs e)
        {
            _currentSettings = new Dictionary<string, string>(defaultSettings);
            if (e.Result != null)
            {
                foreach (var item in e.Result)
                {
                    if (_currentSettings.ContainsKey(item.Name))
                    {
                        _currentSettings[item.Name] = item.Value;
                    }
                    else
                    {
                        _currentSettings.Add(item.Name, item.Value);
                    }
                }
            }
            oldDataBrush = new SolidColorBrush(GetColor(ConfigurationKeys.OldDataColor));
            if (e.UserState != null)
            {
                Action callBack = (Action)e.UserState;
                callBack();
            }
        }


        private static readonly Dictionary<string, string> defaultSettings = new Dictionary<string, string>
        {
            {"ImagesFolder", @"/Finder.BloodDonation;component/Images/"},
            {"UnitImagesFolder", @"/Finder.BloodDonation;component/Images/Units/"},
            {"ImagesExtention", ".png"},
            {"ImagesSuffixNormal", ""},
            {"ImagesSuffixHover", "_Hover"},
            {"ImagesSuffixAlarm",  "_Alarm"},
        //Ustawienia autowylogowania
            {"AdminAutoLogoff", "false"},
            {"PermissionsForAdminSessionDuration" , "AdministrationAccess"},
            {"AdminSessionDurationHours", "0"},
            {"AdminSessionDurationMinutes", "10"},
            {"AdminSessionDurationSeconds", "0"},
    
            {"UserAutoLogoff", "false"},
            {"UserSessionDurationHours", "0"},
            {"UserSessionDurationMinutes", "10"},
            {"UserSessionDurationSeconds", "0"},

        //Edycja unitów
            {"UnitNewlyCreatedName", "<Zmień nazwę>"},
            {ConfigurationKeys.UnitsLocationSeparator, " > "},
       
            //Ustawienia alarmowania
            {"PermissionsForShortAlarms" , "AdministrationAccess"},

        //Ustawienia zakresu danych
            {ConfigurationKeys.MaxDaysRawData, "5"},
            {ConfigurationKeys.MaxDaysReports, "33"},

            {"MinPingSessionIntervalSecs", "5"},
            {ConfigurationKeys.PingSessionIntervalSecs, "20"},
            {"MinMonitoringSoundIntervalSecs", "15"},
            {ConfigurationKeys.MonitoringSoundIntervalSecs, "30"},
            {ConfigurationKeys.RangeColorIntensityPercent, "50"},


            {ConfigurationKeys.SearchMessagesDaysBefore, "2"},
            {ConfigurationKeys.SearchMessagesDaysAfter, "1"},
            {ConfigurationKeys.SearchLogsDaysBefore, "2"},
            {ConfigurationKeys.SearchLogsDaysAfter, "1"},
            {ConfigurationKeys.SearchChartDataHoursBefore, "24"},
            {ConfigurationKeys.SearchChartDataHoursAfter, "24"},
            {ConfigurationKeys.ChartImageWidth, "800"},
            {ConfigurationKeys.ChartImageHeight, "600"},
            {ConfigurationKeys.SearchRawDataDaysBefore, "1"},
            {ConfigurationKeys.SearchRawDataDaysAfter, "1"},
            {ConfigurationKeys.SearchAlarmsDaysBefore, "2"},
            {ConfigurationKeys.SearchAlarmsDaysAfter, "1"},
            {ConfigurationKeys.MaxRowsAlarmsPopup, "200"},
            {ConfigurationKeys.ShowReasonField, "1"},
            {ConfigurationKeys.ShowUnitId, "1"},
            {ConfigurationKeys.EnableMonitoringStatusButton, "0"},
            {ConfigurationKeys.OldDataTimeMinutes, "10"},
            {ConfigurationKeys.OldDataColor, "255,255,127,39" }



        };


        private static Brush oldDataBrush;

        public Brush OldDataBrush
        {
            get { return oldDataBrush; }
        }
    }
}
