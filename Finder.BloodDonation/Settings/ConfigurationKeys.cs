﻿
namespace Finder.BloodDonation.Settings
{
    public class ConfigurationKeys
    {
        public const int ENABLED = 1;
        public const string BLOOD_LABEL_LINE_SEPARATOR = "|";
        public const int SYSTEM_ADMIN_ID = 1;


        public const string RootUnitId = "RootUnitId";
        public const string WaitingRoomUnitId = "WaitingRoomUnitId";
        public const string RegistrationUnitId = "RegistrationUnitId";

        public const string MaxPasswordAgeDays = "MaxPasswordAgeDays";
        public const string ShowReasonField = "ShowReasonField";
        public const string PingSessionIntervalSecs = "PingSessionIntervalSecs";
        public const string MonitoringSoundIntervalSecs = "MonitoringSoundIntervalSecs";
        public const string RangeColorIntensityPercent = "RangeColorIntensityPercent";
        public const string DefMessageActivityMinutes = "DefMessageActivityMinutes";
        public const string UnitsLocationSeparator = "UnitsLocationSeparator";
        public const string SearchMessagesDaysBefore = "SearchMessagesDaysBefore";
        public const string SearchMessagesDaysAfter = "SearchMessagesDaysAfter";
        public const string SearchLogsDaysBefore = "SearchLogsDaysBefore";
        public const string SearchLogsDaysAfter = "SearchLogsDaysAfter";
        public const string SearchChartDataHoursBefore = "SearchChartDataHoursBefore";
        public const string SearchChartDataHoursAfter = "SearchChartDataHoursAfter";
        public const string ChartImageWidth = "ChartImageWidth";
        public const string ChartImageHeight = "ChartImageHeight";
        public const string SearchRawDataDaysBefore = "SearchRawDataDaysBefore";
        public const string SearchRawDataDaysAfter = "SearchRawDataDaysAfter";
        public const string MaxDaysRawData = "MaxDaysRawData";
        public const string SearchAlarmsDaysBefore = "SearchAlarmsDaysBefore";
        public const string SearchAlarmsDaysAfter = "SearchAlarmsDaysAfter";
        public const string MaxRowsAlarmsPopup = "MaxRowsAlarmsPopup";
        public const string MaxDaysReports = "MaxDaysReports";
        public const string ShowUnitId = "ShowUnitId";
        public const string EnableMonitoringStatusButton = "EnableMonitoringStatusButton";
        public const string OldDataTimeMinutes = "OldDataTimeMinutes";
        public const string OldDataColor = "OldDataColor";
    }
}
