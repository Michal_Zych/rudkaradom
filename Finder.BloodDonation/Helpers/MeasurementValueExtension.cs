﻿using Finder.BloodDonation.Converters;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Helpers
{
    public static class MeasurementValueExtension
    {
        public const string NoSensor = "---";
        public const string NoDevice = "Err";
        public const string NA = "N/A";

        public static string ToDisplayString(this double value)
        {
            return value.ToString("0.0");
        }

        public static string ToDisplayString(this double? value)
        {
            if (value == null)
                return NoSensor;

            return ((double)value).ToDisplayString();
        }

        public static string ToDisplayString(this byte value)
        {

            return AlarmEventDescriptor.GetShortDescription(value);

        }


    }
}
