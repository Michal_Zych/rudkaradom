﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Helpers
{
    public static class StringLoggingExtension
    {
        private const string FieldSeparatorTemplate = ", ";
        private const string EditFieldTemplate = "{0} z: {1} na: {2}";
        private const string CreateFieldTemplate = "{0}: {1}";
        private const string IdFieldTEmplate = "<ID: {0}>";
        private const string NULL = "null";
        private const string EditPasswordTemplate = "Zmieniono hasło";

        public static string LogPasswordChanged(this string log)
        {
            return log + (string.IsNullOrEmpty(log) ? "" : FieldSeparatorTemplate)
                            + EditPasswordTemplate;
        }

        public static string LogEdit(this string log, string fieldName, string oldValue, string newValue)
        {
            if (oldValue == newValue) return log;
            return log + (string.IsNullOrEmpty(log) ? "" : FieldSeparatorTemplate)
                + String.Format(EditFieldTemplate, fieldName, oldValue, newValue);
        }

        public static string LogEdit(this string log, string fieldName, string oldValue, int oldId, string newValue, int newId)
        {
            if (oldValue == newValue) return log;

            var template = EditFieldTemplate.Replace("1}", "1}" + String.Format(IdFieldTEmplate, oldId))
                            .Replace("2}", "2}" + String.Format(IdFieldTEmplate, newId));

            return log + (string.IsNullOrEmpty(log) ? "" : FieldSeparatorTemplate)
                + String.Format(template, fieldName, oldValue, newValue);
        }

        public static string LogEdit(this string log, string fieldName, string oldValue, int? oldId, string newValue, int? newId)
        {
            if (oldValue == newValue) return log;

            var template = EditFieldTemplate.Replace("1}", "1}" + String.Format(IdFieldTEmplate, (oldId == null) ? NULL : oldId.Value.ToString()))
                .Replace("2}", "2}" + String.Format(IdFieldTEmplate, (newId == null) ? NULL : newId.Value.ToString()));

            return log + (string.IsNullOrEmpty(log) ? "" : FieldSeparatorTemplate)
                + String.Format(template, fieldName, oldValue, newValue);
        }



        public static string LogCreate(this string log, string fieldName, string value)
        {
            if (string.IsNullOrWhiteSpace(value)) return log;
            return log + (string.IsNullOrEmpty(log) ? "" : FieldSeparatorTemplate)
                + String.Format(CreateFieldTemplate, fieldName, value);

        }

        public static string LogCreate(this string log, string fieldName, string value, int Id)
        {
            if (string.IsNullOrWhiteSpace(value)) return log;

            var template = CreateFieldTemplate.Replace("1}", "1}" + String.Format(IdFieldTEmplate, Id));

            return log + (string.IsNullOrEmpty(log) ? "" : FieldSeparatorTemplate)
                + String.Format(template, fieldName, value);
        }
    }
}
