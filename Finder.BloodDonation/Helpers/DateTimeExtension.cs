﻿using System;
using System.Globalization;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Helpers
{
    public static class DateTimeExtension
    {
        private const string SQL_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

        public static string ToSQLstring(this DateTime value)
        {
            return value.ToString(SQL_DATETIME_FORMAT);
        }

        public static DateTime? FromSQLstring(string value)
        {
            DateTime? result = null;
            try
            {
                result = DateTime.ParseExact(value, SQL_DATETIME_FORMAT, CultureInfo.InvariantCulture);
            }
            catch { };
            
            return result;
        }
    }
}
