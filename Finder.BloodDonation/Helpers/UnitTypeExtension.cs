﻿using System;
using System.Linq;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Model.Enum;
using System.Collections.Generic;


namespace Finder.BloodDonation.Helpers
{
    public static class UnitTypeExtension
    {

        public static bool IsStore(this UnitType value) //posiada cechę magazyn
        {
            return UnitTypeOrganizer.Stores.Contains(value);
        }

        public static bool IsMovable(this UnitType value) //posiada cechę przenoszenie
        {
            return UnitTypeOrganizer.Movables.Contains(value);
        }


        public static bool IsMonitoredDevice(this UnitType value)
        {
            return UnitTypeOrganizer.MonitoredDevices.Contains(value);
        }

        public static bool IsNotMonitoredDevice(this UnitType value)
        {
            return !IsMonitoredDevice(value);
        }
       
        public static bool IsConfigurableDevice(this UnitType value)
        {
            return UnitTypeOrganizer.ConfigurableDevices.Contains(value);
        }

        public static bool CanBeDragged_(this UnitType value)
        {
            return UnitTypeOrganizer.DraggableUnits_.Contains(value);
        }

        public static bool CanBeDraggedByEveryone(this UnitType value)
        {
            return UnitTypeOrganizer.DraggableByEveryoneUnits.Contains(value);
        }

		public static bool CanBeDroppedOn(this UnitType value)
		{
			return UnitTypeOrganizer.DroppableOnUnits.Contains(value);
		}

        public static bool CanContainMonitoredDevice(this UnitType value)
        {
            return UnitTypeOrganizer.MonitoredDevicesContainersList().Contains(value);
        }

        public static string SensorDescription(this UnitType value, string sensor)
        {
            if (value.IsNotMonitoredDevice())
            {
                throw new ArgumentException("Only monitored devices can have sensors");
            }
            switch (sensor)
            {
                case "T1":
                    if (value == UnitType.THERMOMETER)
                    {
                        return "Glicerol";
                    }
                    else
                    {
                        return "Wilgotność";
                    }
                case "T2":
                    return "Powietrze";
            }
            return "Error";
        }

        public static DeviceType ToDeviceType(this UnitType value)
        {
            if (value == UnitType.THERMOMETER)
                return DeviceType.THERMOMETER;
            if(value== UnitType.THERMOHYGROMETER)
                return DeviceType.THERMOHYGROMETER;
            return DeviceType.UNKNOWN;
        }


        public static int getNumberValue(this UnitType value)
        {
            return (int)value;
        }

    }
}
