﻿using System.Linq;

namespace System.Collections.Generic
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<T> Distinct<T>(this IEnumerable<T> source, Func<T, object> uniqueCheckerMethod)
        {
            return source.Distinct(new GenericComparer<T>(uniqueCheckerMethod));
        }

        class GenericComparer<T> : IEqualityComparer<T>
        {
            public GenericComparer(Func<T, object> uniqueCheckerMethod)
            {
                this._uniqueCheckerMethod = uniqueCheckerMethod;
            }

            private Func<T, object> _uniqueCheckerMethod;

            bool IEqualityComparer<T>.Equals(T x, T y)
            {
                return this._uniqueCheckerMethod(x).Equals(this._uniqueCheckerMethod(y));
            }

            int IEqualityComparer<T>.GetHashCode(T obj)
            {
                return this._uniqueCheckerMethod(obj).GetHashCode();
            }
        }


        public static string ToCommaSeparatedValues<T>(this IEnumerable<T> values)
        {
            return (/*values != null ||*/ values.Count() > 0) ? String.Join(",", values) : (string)null;
        }


    }
}
