﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Helpers
{
    public static class StringNullIfEmptyExtension
    {
        public static string NullIfEmpty(this string source)
        {
            if (source == null)
                return null;

            source = source.Trim();
            if (string.IsNullOrEmpty(source))
                return null;
            return source;
        }
    }
}
