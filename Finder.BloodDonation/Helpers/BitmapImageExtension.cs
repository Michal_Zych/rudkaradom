﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Helpers
{

    public delegate int ConvertPixel(int pixel);

    public static class BitmapImageExtension
    {
        public static BitmapSource ConvertBitmap(this BitmapImage bitmapImage, ConvertPixel convertionMethod)
        {
            if (bitmapImage == null || bitmapImage.PixelWidth == 0)
            {
                return null;
            }

            WriteableBitmap bitmap = new WriteableBitmap(bitmapImage);
            for (int y = 0; y < bitmap.PixelHeight; y++)
            {
                for (int x = 0; x < bitmap.PixelWidth; x++)
                {
                    int pixelLocation = bitmap.PixelWidth * y + x;
                    int pixel = bitmap.Pixels[pixelLocation];
                    bitmap.Pixels[pixelLocation] = convertionMethod(pixel);
                }
            }
            return bitmap;
        }

        public static BitmapSource ConvertBitmap(this BitmapImage bitmapImage, double[] filter)
        {
            if (bitmapImage == null || bitmapImage.PixelWidth == 0)
            {
                return null;
            }

            WriteableBitmap bitmap = new WriteableBitmap(bitmapImage);
            for (int y = 0; y < bitmap.PixelHeight; y++)
            {
                for (int x = 0; x < bitmap.PixelWidth; x++)
                {
                    int pixelLocation = bitmap.PixelWidth * y + x;
                    int pixel = bitmap.Pixels[pixelLocation];
                    bitmap.Pixels[pixelLocation] = IntenseColor(pixel, filter[0], filter[1], filter[2]);
                }
            }
            return bitmap;
        }


        public static BitmapSource MakeRed(this BitmapImage bitmapImage)
        {
            return ConvertBitmap(bitmapImage, MakeRedPixel);
        }

        public static BitmapSource MakeGrayScale(this BitmapImage bitmapImage)
        {
            return ConvertBitmap(bitmapImage, MakeGrayScale);
        }

        public static BitmapSource MakeColorsIntense(this BitmapImage bitmapImage, double[] filter)
        {
            return ConvertBitmap(bitmapImage, filter);
        }

        private static int IntenseColor(int pixel, double red, double green, double blue)
        {
            byte[] pixelBytes = BitConverter.GetBytes(pixel);
            pixelBytes[0] = (byte)(blue * pixelBytes[0]);
            pixelBytes[1] = (byte)(green * pixelBytes[1]);
            pixelBytes[2] = (byte)(red * pixelBytes[2]);
            return BitConverter.ToInt32(pixelBytes, 0);
        }


        private static int MakeRedPixel(int pixel)
        {
            byte[] pixelBytes = BitConverter.GetBytes(pixel);
            pixelBytes[0] = (byte)(.5 * pixelBytes[0]);
            pixelBytes[1] = (byte)(.5 * pixelBytes[1]);
            pixelBytes[2] = (byte)(1 * pixelBytes[2]);
            return BitConverter.ToInt32(pixelBytes, 0);
        }

        private static int MakeGrayScale(int pixel)
        {
            byte[] pixelBytes = BitConverter.GetBytes(pixel);
            byte bwPixel = (byte)(.299 * pixelBytes[2] + .587 * pixelBytes[1] + .114 * pixelBytes[0]);
            pixelBytes[0] = bwPixel;
            pixelBytes[1] = bwPixel;
            pixelBytes[2] = bwPixel;
            return BitConverter.ToInt32(pixelBytes, 0);
        }
    }
}
