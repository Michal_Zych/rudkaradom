﻿using System.Collections.Generic;
using System.Linq;
using Finder.BloodDonation.Model.Enum;
using System;
using Finder.BloodDonation.DataManagers;

namespace Finder.BloodDonation.Helpers
{
    public static class UnitTypeOrganizer
    {
        public const int DEFAULT_UNIT_TYPE = -1;
        public const int DEFAULT_SENSOR_TYPE = 19;

        static int[] SensorUnits = { 1 };
        static int[] BitSensorUnits = { 27, 28 };

        private static int registrationsUnitId;

        private static int unassignedPatientUnitId;

        private static bool forceBandAssignment;

        public static bool IsSensorUnit(int unitTypeIDv2)
        {
            return Array.IndexOf(SensorUnits, unitTypeIDv2) > -1;
        }

        public static bool IsBitSensorUnit(int unitTypeIDv2)
        {
            return Array.IndexOf(BitSensorUnits, unitTypeIDv2) > -1;
        }
        public static bool IsMovableUnit(int unitTypeIDv2)
        {
            return !IsSensorUnit(unitTypeIDv2);
        }

        public static IList<int> SensorUnitsList()
        {
            return new List<int>(SensorUnits);
        }

        public static IList<int> BitSensorUnitsList()
        {
            return new List<int>(BitSensorUnits);
        }

        public static bool CanUnitContainOther(int parentUnitType, int childUnitType)
        {
            bool result = true;

            if(IsSensorUnit(parentUnitType)) result = false;

            return result;
        }

        public static IList<int> CanBeInFavoritesList()
        {
            var result = new List<int>();
            foreach(var k in UnitPicturesMap.Keys)
            {
                if( (k > 0) && (!IsSensorUnit(k)))
                {
                    result.Add(k);
                }
            }

            return result;
        }

        public static void SetRegistrationAndUnassignedPatientsUnitId(int registrationUnitId, int unassignedPatientsUnitId)
        {
            registrationsUnitId = registrationUnitId;
            unassignedPatientUnitId = unassignedPatientsUnitId;
        }

        public static void SetRegistrationUnitId(int registrationUnitId)
        {
            registrationsUnitId = registrationUnitId;
        }
        public static void SetUnassignedPatientsUnitId(int unassignedPatientUnitID)
        {
            unassignedPatientUnitId = unassignedPatientUnitID;
        }

        public static void SetForceBandAssignment(bool value)
        {
            forceBandAssignment = value;
        }

        public static int GetRegistartionUnitId()
        {
            return registrationsUnitId;
        }

        public static int GetUnassignedPatientsUnitId()
        {
            return unassignedPatientUnitId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static bool GetForceBandAssignment()
        {
            return forceBandAssignment;
        }

        public static IList<int> CanBeParent()
        {
            var result = new List<int>();
            foreach (var k in UnitPicturesMap.Keys)
            {
                if ((k > 0) && (!IsSensorUnit(k)))
                {
                    result.Add(k);
                }
            }

            return result;
        }

        public static IList<int> GetUnitTypes()
        {
            var result = new List<int>(UnitPicturesMap.Keys);

            return result;
        }

        public static IList<int> GetPictureIDs()
        {
            var result = new List<int>(UnitPicturesMap.Keys);

            return result;
        }

        public static string GetTypeName(int id)
        {
            if (UnitPicturesMap.ContainsKey(id))
                return UnitPicturesMap[id];

            return UnitPicturesMap[-1];
        }

        public static string GetPictureName(int id)
        {
            if (UnitPicturesMap.ContainsKey(id))
                return UnitPicturesMap[id];

            return UnitPicturesMap[-1];
        }

        public static bool IsHospitalDepartment(int unitTypeV2)
        {
            if (unitTypeV2 == (int)UnitTypeV2.HospitalDepartment /*|| unitTypeV2 == (int)UnitType.DEPARTMENT*/)
                return true;
            else
                return false;
        }

        private static IDictionary<int, string> UnitPicturesMap = new Dictionary<int, string>()
            {
                {-1, "Bug"},
                {1, "Box"},
                {2, "Budynek"},
                {3, "Dzial"},
                {4, "Kuweta"},
                {5, "Lodowka"},
                {6, "LodowkaTemp"},
                {7, "Mroznia"},
                {8, "Oddział"},
                {9, "Paleta"},
                {10, "Pietro"},
                {11, "PojemnikTB"},
                {12, "PojemnikTBTemp"},
                {13, "Pokoj"},
                {14, "PokojTemp"},
                {15, "PokojTHG"},
                {16, "Polka"},
                {17, "Regal"},
                {18, "Szuflada"},
                {19, "Termohigrometr"},
                {20, "Termometr"},
                {21, "Zamrazarka"},
                {22, "ZamrazarkaTemp"},
                {23, "Ambulance"},
                {24, "Archiwum"},
                {25, "Basket"},
                {26, "Zasilanie"},
                {27, "Pstryczek"},
                {28, "Przełącznik"},
            };

        /// <summary>
        /// /////////////////////////////////////////////////////////////
        /// </summary>
        internal static readonly UnitType[] Stores = { UnitType.THERMOMETER, UnitType.BASKET, UnitType.WAREHOUSE, UnitType.SHELF, UnitType.ROOM, UnitType.BOX };//posiadają cechę magazyn
        internal static readonly UnitType[] Movables = { UnitType.BOX };//posiadają cechę przenoszenie

        internal static readonly UnitType[] MonitoredDevices = { UnitType.THERMOMETER, UnitType.THERMOHYGROMETER/*, UnitType.BOX, */};

        internal static readonly UnitType[] ConfigurableDevices = { UnitType.THERMOMETER, UnitType.THERMOHYGROMETER };

        internal static readonly UnitType[] DraggableUnits_ = { UnitType.THERMOMETER, UnitType.THERMOHYGROMETER };

        internal static readonly UnitType[] DraggableByEveryoneUnits = { UnitType.BOX };

	    internal static readonly UnitType[] DroppableOnUnits = {UnitType.ROOM, UnitType.FLOOR, UnitType.CENTRE, };

        internal static readonly UnitType[] MonitoredDevicesContainers = { UnitType.ROOT, UnitType.ROOM, UnitType.FLOOR, UnitType.CENTRE, UnitType.DEPARTMENT, UnitType.WAREHOUSE };

        internal static readonly UnitType[] FavoritableUnits =
        {
            UnitType.ROOT, UnitType.DEPARTMENT, UnitType.CENTRE, UnitType.ROOM, UnitType.FLOOR, UnitType.THERMOHYGROMETER,
            UnitType.THERMOMETER, UnitType.WAREHOUSE,  UnitType.SHELF, UnitType.BOX
        };

        internal static readonly UnitType[] RootElements = { UnitType.ROOT };

        internal static readonly UnitType[] DepartmentsContainters = { UnitType.CENTRE };

        internal static readonly UnitType[] FloorsContainters = { UnitType.DEPARTMENT };

        internal static readonly UnitType[] RoomsContainers = { UnitType.DEPARTMENT, UnitType.FLOOR };

        internal static readonly UnitType[] ThermometersContainers = { UnitType.ROOM };

        internal static readonly UnitType[] ThermohygrometersContainers = { UnitType.ROOM };

        internal static readonly UnitType[] EditableUnits =
        {
            UnitType.ROOM, UnitType.DEPARTMENT, UnitType.FLOOR,
            UnitType.THERMOHYGROMETER, UnitType.THERMOMETER
        };

        public static List<UnitType> MonitoredDevicesContainersList()
        {
            return ThermeterContaintersList().Concat(ThermohygrometerContainersList()).Distinct().ToList();
        }

        public static List<UnitType> EditableUnitsList()
        {
            return new List<UnitType>(EditableUnits);
        }

        public static List<UnitType> ThermohygrometerContainersList()
        {
            return new List<UnitType>(ThermohygrometersContainers);
        }

        public static List<UnitType> ThermeterContaintersList()
        {
            return new List<UnitType>(ThermometersContainers);
        }

        public static List<UnitType> RoomContainersList()
        {
            return new List<UnitType>(RoomsContainers);
        }

        public static List<UnitType> FloorContainersList()
        {
            return new List<UnitType>(FloorsContainters);
        }

        public static List<UnitType> DepartmentContainersList()
        {
            return new List<UnitType>(DepartmentsContainters);
        }

        private static List<UnitType> MonitoredDevicesList()
        {
            return new List<UnitType>(MonitoredDevices);
        }
    }
}
