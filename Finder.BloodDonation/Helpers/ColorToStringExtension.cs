﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Helpers
{
    public static class ColorToStringExtension
    {
        public static string ToRGBString(this Color color)
        {
            return color.R.ToString() + ","
                + color.G.ToString() + ","
            + color.B.ToString();
        }
    }
}
