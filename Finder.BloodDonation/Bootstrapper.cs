﻿using Finder.BloodDonation.Common.Authentication;
using FinderFX.SL.Core.MVVM;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Practices.Composite.Modularity;
using Finder.BloodDonation.Foo;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Tabs;
using Finder.BloodDonation.Tabs.Alarms;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.Tabs.Reports;
using Finder.BloodDonation.Tabs.Configuration;
using Finder.BloodDonation.HubsProxy;
using Finder.BloodDonation.Tabs.Hubs;
using FinderFX.SL.Core.Authentication;
using Finder.BloodDonation.UsersService;
using Finder.BloodDonation.Login;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.PermissionsProxy;
using Finder.BloodDonation.Tabs.Diagnostics;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Tabs.UnitsList;
using Finder.BloodDonation.Tabs.Contents;
using Finder.BloodDonation.JsInterop;
using Finder.BloodDonation.Tabs.RefreshTools;
using System.Diagnostics;
using Finder.BloodDonation.Tabs.MenuTools.Transmitters;
using Finder.BloodDonation.Tabs.MenuArchive.Patients;
using Finder.BloodDonation.Tabs.MenuTools.Bands;
using Finder.BloodDonation.Tabs.MenuTools.Externals;
using Finder.BloodDonation.Tabs.MenuTools.Backup;
using Finder.BloodDonation.Tabs.MenuAdministration.LoggedUsers;
using Finder.BloodDonation.Tabs.MenuAdministration.Logs;
using Finder.BloodDonation.Tabs.MenuAdministration.Versions;
using Finder.BloodDonation.Tabs.MenuAdministration.Settings;
using Finder.BloodDonation.Tabs.MenuAdministration.Messages;
using Finder.BloodDonation.Tabs.MenuAdministration.Permissions;
using Finder.BloodDonation.Tabs.Localization;
using Finder.BloodDonation.Tabs.Events;
using Finder.BloodDonation.Tabs.LocalizationReports;
using Finder.BloodDonation.Tabs.LocalizationConfig;
using Finder.BloodDonation.Tabs.LocalizationPlan;
using Finder.BloodDonation.Tabs.PatientTests;
using Finder.BloodDonation.Tabs.BandInfo;
using Finder.BloodDonation.Tabs.PatientInfo;
using Finder.BloodDonation.Tabs.BandStatus;
using Finder.BloodDonation.Tabs.PatientDetails;
using Finder.BloodDonation.Tabs.ArchivePatients;
using Finder.BloodDonation.Tabs.SystemEvents;
using Finder.BloodDonation.Tabs.ToolsBands;
using Finder.BloodDonation.Tabs.ToolsTransmitters;
using Finder.BloodDonation.Tabs.ExternalRecordsIn;
using Finder.BloodDonation.Tabs.ExternalRecordsOut;
using Finder.BloodDonation.Tabs.ExternalUnitMap;
using Finder.BloodDonation.Tabs.ServiceStatus;
using Finder.BloodDonation.Tabs.ExternalSystemInfo;

namespace Finder.BloodDonation
{
    public class Bootstrapper : BootstrapperBase
    {
        //TODO: wykorzystywane przez LoginStatus, USUNĄć !!!!!
        public static IUnityContainer StaticContainer { get; set; }

        public static bool PlanningPreview { get; set; }

        //gdy true wyłacza zakładki list i monitoring po dodaniu do ulubionych
        //public static bool FavoritesSelected { get; set; }

        public static bool UserMonitoringState
        {
            get
            {
                return monitoring;
            }
        }

        public static bool monitoring;

        protected override FinderFX.SL.Core.Configuration.GlobalConfiguration GetConfiguration()
        {
            StaticContainer = this.Container;
            return new BloodGlobalConfiguration(this.Container);
        }

        protected override Microsoft.Practices.Composite.Modularity.IModuleCatalog GetModuleCatalog()
        {
            ModuleCatalog catalog = new ModuleCatalog();

            return catalog;
        }

        MainPage Main { get; set; }
        protected override System.Windows.UIElement GetContentControl()
        {
            if (Main == null)
            {
                StaticContainer = this.Container;
                MainPage main = this.Container.Resolve<MainPage>();

                UnitTreeView tree = this.Container.Resolve<ViewFactory>().CreateView<UnitTreeView>();
                TabContainerView tabContainer = this.Container.Resolve<ViewFactory>().CreateView<TabContainerView>();

                main.TreePresenter.Content = tree;
                main.TabPresenter.Content = tabContainer;

                Main = main;
            }
            return Main;
        }

        protected override ILoggingControl GetLoggingControl()
        {
            StaticContainer = this.Container;
            return this.Container.Resolve<LoginView>();
        }

        protected override IRootControl GetRootControl()
        {
            StaticContainer = this.Container;

            this.Container.Resolve<IDynamicEventAggregator>()
                .GetEvent<LoadTabContentRequestEvent>()
                .Subscribe(OnLoadTabContentRequestEvent);

            ServiceFactory.RegisterServiceUri<UnitsServiceClient>(ServicesUri.UnitsService);
            ServiceFactory.RegisterServiceUri<HubsServiceClient>(ServicesUri.HubsService);
            ServiceFactory.RegisterServiceUri<PermissionsServiceClient>(ServicesUri.PermissionsService);
            ServiceFactory.RegisterServiceUri<UsersServiceClient>(ServicesUri.UsersService);

            var bridge = this.Container.Resolve<Bridge>();

            IUserSessionManager am = new EmptyUserSessionManager();
            this.Container.RegisterInstance<IUserSessionManager>(am);
            this.Container.RegisterInstance<ITabManager>(new EmptyTabManager());
            this.Container.RegisterInstance<IJsComBridge>(bridge);

            User.CurrentUserChanged += new EventHandler(User_CurrentUserChanged);

            return this.Container.Resolve<Shell>();
        }


        private UsersServiceClient Service
        {
            get
            {
                UsersServiceClient proxy = ServiceFactory.GetService<UsersServiceClient>(ServicesUri.UsersService);
                proxy.CreateUserSessionCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(proxy_CreateUserSessionCompleted);
                return proxy;
            }
        }

        void User_CurrentUserChanged(object sender, EventArgs e)
        {
            IUserSessionManager iam = this.Container.Resolve<IUserSessionManager>();
            if (iam != null)
            {
                iam.Dispose();
            }
            if (User.Current != User.Anonymous && User.Current.Identity.IsAuthenticated == true)
            {
                var domains_s = User.Current.GetProperty(Finder.BloodDonation.Model.ConfigurationsKeys.SERVICES_DOMAIN);
                if (!string.IsNullOrEmpty(domains_s))
                {
                    string[] domains = domains_s.Split(new char[1] { ';' });
                    for (int i = 0; i < domains.Length; i++)
                    {
                        string domain = domains[i];
                        ServiceFactory.RegisterServiceDomain(domain);
                    }
                }
                //tworzenie następuje po utworzeniu sesji
                //IAlarmsManager am = new AlarmsManager(this.Container);
                //this.Container.RegisterInstance<IAlarmsManager>(am);

                Settings.Configuration.Current.Reload_(() =>
                {
                    Service.CreateUserSessionAsync(new CreateSessionRequestDto() { Monitoring = monitoring });
                });
            }
            else
            {

            }
        }

        void proxy_CreateUserSessionCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                IUserSessionManager am = new UserSessionManager(this.Container);
                this.Container.RegisterInstance<IUserSessionManager>(am);

                IRefreshManager rm = new RefreshManager();
                this.Container.RegisterInstance<IRefreshManager>(rm);
            }
        }

        public void OnLoadTabContentRequestEvent(ITab tab)
        {
            switch (tab.Name)
            {
                case TabNames.UnitPlan:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<PreviewEdit>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;
                        break;
                    }

                case TabNames.PreviewEdit:
                    {
                        //UnitMonitoringView view = this.Container.Resolve<ViewFactory>().CreateView<UnitMonitoringView>();
                        var view = this.Container.Resolve<ViewFactory>().CreateView<PreviewEdit>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }

                case TabNames.AlarmsList:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<AlarmsListView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;
                        break;
                    }
                case TabNames.Reports:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<ReportsListView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;
                        break;
                    }
                case TabNames.Configuration:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<ConfigurationView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;
                        break;
                    }
                case TabNames.HubsList:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<HubView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;
                        break;
                    }
                case TabNames.HubDetails:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<HubView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;
                        break;
                    }

                case TabNames.HubsCommunicationLogs:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<CommLogsView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;
                        break;
                    }

                case TabNames.DiagnosticsData:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<DataListView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;
                        break;
                    }
                //Unit Localization
                case TabNames.Patients:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<PatientDetailsView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }

                case TabNames.Monitoring:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<BandStatusView>(); 
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }

                case TabNames.Localization:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<UnitPlan>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }

                case TabNames.Events:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<EventsView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }

                case TabNames.LocalizationReports:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<LocalizationReportsView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }

                case TabNames.LocalizationConfig:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<LocalizationConfigView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }

                case TabNames.LocalizationPlan:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<LocalizationPlanView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }

                case TabNames.Transmitters:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<Finder.BloodDonation.Tabs.TransmitterDetails.TransmitterDetailsView>(); //.PatientTestView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }

                case TabNames.Bands:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<Finder.BloodDonation.Tabs.BandInfo.BandInfosView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }


                //Administration
                case TabNames.Permissions:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<PermissionsView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }

                case TabNames.Messages:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<MessagesView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }

                case TabNames.Settings:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<SettingsView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }

                case TabNames.LoggedUsers:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<LoggedUsersView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }

                case TabNames.Logs:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<SystemEventsView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }
                case TabNames.ServiceStatus:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<ServiceStatusView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }
                
                case TabNames.Versions:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<VersionsView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }
                    
                //Tools
                case TabNames.ToolTransmitters:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<ToolsTransmittersView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }
                case TabNames.ToolBands:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<ToolsBandsView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }

                case TabNames.ExternalSystems:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<ExternalSystemInfoView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }
                case TabNames.Asseco:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<AssecoView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }

                case TabNames.AssecoOut:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<ExternalRecordsOutView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }
                case TabNames.AssecoIn:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<ExternalRecordsInView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }

                case TabNames.AssecoMapping:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<ExternalUnitMapView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }
                case TabNames.Backup:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<BackupView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }

                //Archive
                case TabNames.ArchivePatients:
                    {
                        var view = this.Container.Resolve<ViewFactory>().CreateView<Finder.BloodDonation.Tabs.ArchivePatients.ArchivePatientsView>();
                        tab.TabViewModel = view.DataContext as ITabViewModel;

                        break;
                    }


                default:
                    {
                        FooControl view = this.Container.Resolve<ViewFactory>().CreateView<FooControl>();

                        tab.TabViewModel = view.DataContext as ITabViewModel;
                        break;
                    }
            }
        }

        protected override string MainAssemblyName
        {
            get { throw new System.NotImplementedException(); }
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();

            //ServiceFactory.RegisterServiceDomain("http://rckik1-preprod.finderonline.eu:54111/");
            //ServiceFactory.RegisterServiceDomain("http://rckik2-preprod.finderonline.eu:54111/");
            //ServiceFactory.RegisterServiceDomain("http://rckik3-preprod.finderonline.eu:54111/");

            //ServiceFactory.RegisterServiceDomain("http://rckik1-preprod.local:54111/");
            //ServiceFactory.RegisterServiceDomain("http://rckik2-preprod.local:54111/");
            //ServiceFactory.RegisterServiceDomain("http://rckik3-preprod.local:54111/");

            //ServiceFactory.RegisterServiceDomain("http://192.168.139.17:54121/");
            //ServiceFactory.RegisterServiceDomain("http://rckik2-preprod.local:54121/");
            //ServiceFactory.RegisterServiceDomain("http://rckik3-preprod.local:54121/");

            //ServiceFactory.RegisterServiceDomain("http://rckik1:54111/");
            //ServiceFactory.RegisterServiceDomain("http://rckik2:54111/");
            //ServiceFactory.RegisterServiceDomain("http://rckik3:54111/");

        }
    }
}
