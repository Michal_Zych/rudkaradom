﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Common.Events.HotKeysEvents;
using Finder.BloodDonation.Layout;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Tabs.Alarms;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Model.Dto;
using DanielVaughan.Logging;
using Finder.BloodDonation.Common;
using System.Windows.Media.Imaging;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Common.Authentication;
using FinderFX.SL.Core.Authentication;
using Finder.BloodDonation.Login;
using Telerik.Windows.Controls;
using System.Windows.Threading;
using Finder.BloodDonation.Dialogs.Alarms;
using Finder.BloodDonation.Tabs.AlarmDetails;
using Finder.BloodDonation.Resources;
using Finder.BloodDonation.Tabs.AlarmsLocalization;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Tabs.AlarmsLocalization.Dialogs;

namespace Finder.BloodDonation
{
    [UIException]
    public partial class MainPage : UserControl
    {
        private static ILog logger = LogManager.GetLog(typeof(MainPage));

        IUnityContainer Container;
        IDynamicEventAggregator EventAggregator;

        private Dictionary<string, FloatableWindow> FloatableWindows = new Dictionary<string, FloatableWindow>();

        private double _splitterPosition;

        public double SplitterPosition 
        {
            get
            {
                return _splitterPosition;
            }
            set
            {
                treeColumn.Width = new GridLength(value);
                _splitterPosition = value;
            }
        }

        public MainPage(IUnityContainer container)
        {
            LocalizationManager.Manager = new CustomLocalizationManager();

            Container = container;
            EventAggregator = container.Resolve<IDynamicEventAggregator>();
            
            InitializeComponent();

            string client = App.Current.Resources["client"].ToString();
            
            ClientName.Text = User.Current.GetProperty("ClientName") + "Klient #" + client;
            if (client == "1") ClientName.Text = "Rudka";
            if (client == "4") ClientName.Text = "m2mTeam";
            ClientName.Text = "Radom";

            this.Loaded += new RoutedEventHandler(MainPage_Loaded);

            EventAggregator.GetEvent<ShowWindow>().Subscribe(ShowWindow_Event);
            EventAggregator.GetEvent<AlarmsCloseAlarms>().Subscribe(CloseAlarms_Event);
            EventAggregator.GetEvent<PlayAlarmSound>().Subscribe(PlayAlarmSound_Event);
            EventAggregator.GetEvent<PlayNewMessage>().Subscribe(PlayNewMessage_Event);
            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(OnCloseEditAlarm);
            EventAggregator.GetEvent<ChangeVisibilityUserInterfaceEvent>().Subscribe(OnChangeVisibilityUserInterface);

            container.RegisterInstance<IWindowsManager>(this);

            this.MouseRightButtonDown += new MouseButtonEventHandler(MainPage_MouseRightButtonDown);

            imgHelp.MouseLeftButtonDown += new MouseButtonEventHandler(imgHelp_MouseLeftButtonDown);
            elLogo.MouseLeftButtonDown += new MouseButtonEventHandler(elLogo_MouseLeftButtonDown);
            finderLogo.MouseLeftButtonDown += finderLogo_MouseLeftButtonDown;

            Application.Current.RootVisual = this;
            Application.Current.RootVisual.KeyUp += new KeyEventHandler(RootVisual_KeyUp);
			Application.Current.RootVisual.MouseMove += RootVisual_MouseMove;
        }

        public void OnChangeVisibilityUserInterface(bool visibilityState)
        {
            if (visibilityState)
            {
                SplitterPosition = 300d;
                TopBar.Visibility = Visibility.Visible;
                imgLogo.Visibility = Visibility.Visible;
                finderLogo.Visibility = Visibility.Visible;
                elLogo.Visibility = Visibility.Visible;
                borderElLogo.Visibility = System.Windows.Visibility.Visible;
                topBarRow.Height = new GridLength(35d);
            }
            else
            {
                SplitterPosition = 0d;
                TopBar.Visibility = Visibility.Collapsed;
                imgLogo.Visibility = Visibility.Collapsed;
                finderLogo.Visibility = System.Windows.Visibility.Collapsed;
                elLogo.Visibility = Visibility.Collapsed;
                borderElLogo.Visibility = System.Windows.Visibility.Collapsed;
                topBarRow.Height = new GridLength(0d);
            }
        }

        void finderLogo_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            SelectDefaultUnit();
        }

		void RootVisual_MouseMove(object sender, MouseEventArgs mouseEventArgs)
        {
            EventAggregator.GetEvent<RestartLogoutTimer>().Publish(0);
        }

        void RootVisual_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.OriginalSource.ToString().ToLower().Contains("textbox"))
                return;

            System.Diagnostics.Debug.WriteLine("<Global>Handle key: " + e.Key.ToString());
            var tabManager = this.Container.Resolve<ITabManager>();
            AccordionMode selectedAccordion =
                (AccordionMode)(this.Container.Resolve<IAccordionManager>().GetSelectedTreeIndex());
            var ContentTab = false;
            {
                switch (e.Key)
                {
                    case Key.W:
                        tabManager.SelectTab(TabNames.UnitPlan, null); //users, permission
                        break;
                    case Key.P:
                        tabManager.SelectTab(TabNames.PreviewEdit, null); //tab list
                        break;
                    case Key.A:
                        tabManager.SelectTab(TabNames.AlarmsList, null); //tab alarm
                        break;
                    case Key.R:
                        tabManager.SelectTab(TabNames.Reports, null); //tab reports
                        break;
                    case Key.B:
                        EventAggregator.GetEvent<SelectBasketEvent>().Publish(null); //go to basket
                        break;
                    case Key.J:
                        EventAggregator.GetEvent<ExpandAccordionEvent>().Publish(AccordionItemsNames.UNITS); //jednostki
                        break;
                    case Key.Q:
                        EventAggregator.GetEvent<ExpandAccordionEvent>().Publish(AccordionItemsNames.ADMINISTRATION); //administracja
                        break; 
                    case Key.I:
                        if (ContentTab)
                            EventAggregator.GetEvent<ShowStocktakingEvent>().Publish(null); //inwentory
                        break;
                    case Key.N:
                        if (ContentTab)
                            EventAggregator.GetEvent<ShowTagGenerationEvent>().Publish(null); //new tag
                        break;
                    case Key.S:
                        if (ContentTab)
                            EventAggregator.GetEvent<ShowPrepCreationEvent>().Publish(null); //search
                        break;
                    case Key.G:
                        if (ContentTab)
                            EventAggregator.GetEvent<ShowGroupsSubEvent>().Publish(null); //show groups
                        break;
                    case Key.T:
                        if (ContentTab)
                            EventAggregator.GetEvent<ShowGroupsByAttributeSubEvent>().Publish(null); //attributes
                        break;
                    case Key.D:
                        if (ContentTab)
                            EventAggregator.GetEvent<ShowDetailsSubEvent>().Publish(null); //details
                        break;
                    default:
                        break;
                }
            }
        }

        void imgHelp_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if ((Keyboard.Modifiers & ModifierKeys.Control) != 0)
            {
                EventAggregator.GetEvent<TestEvent>().Publish(null);
            }
            else
            {
                System.Windows.Browser.HtmlPage.Window.Navigate(
                    new Uri("help_pl.pdf", UriKind.RelativeOrAbsolute),
                    "__blank");
            }
        }

        void elLogo_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            SelectDefaultUnit();
        }

        void imgLogo_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            SelectDefaultUnit();
        }

        void SelectDefaultUnit()
        {
            EventAggregator.GetEvent<SetUserDefaultUnitEvent>().Publish(true);
        }

        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            CreateWindow("alarcomedit", this.Container.Resolve<ViewFactory>().CreateView<EditAlarmComent>(), "Podaj komentarz do alarmów", 400, 400);
            CreateWindow("alaredit", this.Container.Resolve<ViewFactory>().CreateView<EditDetailsAlarm>(), "Edycja alarmu", 400, 400);
            //CreateWindow("alarnew", this.Container.Resolve<ViewFactory>().CreateView<AlarmDetailsNotify>(), "Nowe alarmy", 800, 400);
            CreateWindow("passwd", this.Container.Resolve<ViewFactory>().CreateView<ChangePasswd>(), "Zmiana hasła", 350, 300);
            CreateWindow(WindowsKeys.LocalizationAlarmsPopup, this.Container.Resolve<ViewFactory>().CreateView<LocalizationAlarmNotify>(), "Nowe alarmy", 800, 400);
        }

        void MainPage_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }

        void Fullscreen_Click(object sender, RoutedEventArgs e)
        {
        
        }

        public void ShowWindow_Event(string id)
        {
            if (FloatableWindows.ContainsKey(id))
            {
                FloatableWindows[id].Show();
            }
        }

        public void OnCloseEditAlarm(string obj)
        {
            if (FloatableWindows.ContainsKey("alaredit"))
            {
                FloatableWindows["alaredit"].Close();
            }
        }

        public void CloseAlarms_Event(IList<AlarmDto> a)
        {
            if (FloatableWindows.ContainsKey("alaredit"))
            {
                FloatableWindows["alaredit"].Close();
            }
        }

        public void PlayAlarmSound_Event(int a)
        {
            if (a > 0)
            {
                alarm_sound.Position = new TimeSpan(0, 0, 0);
                alarm_sound.Play();
            }
        }

        public void PlayNewMessage_Event(int a)
        {
            message_sound.Position = new TimeSpan(0, 0, 0);
            message_sound.Play();
        }


        private void CreateWindow(string id, UserControl content, string Title, int width, int height)
        {
            if (!FloatableWindows.ContainsKey(id))
            {
                FloatableWindow fw = new FloatableWindow();
                fw.Title = Title;
                fw.Height = height;
                fw.Width = width;
                fw.Content = content;
                fw.ParentLayoutRoot = LayoutRootOuter;
                FloatableWindows.Add(id, fw);
            }
        }

        private void TreeView1_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
        }

    }
}
