﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Diagnostics;
using PostSharp.Aspects;

namespace Diagnostics
{


	public class ExecutionTimeAttribute : OnMethodBoundaryAspect
	{
		public int MinMilliseconds { get; set; }

		Stopwatch stopwatch = new Stopwatch();

        public override void OnEntry(MethodExecutionArgs eventArgs)
		{
			stopwatch.Start();
		}

        public override void OnExit(MethodExecutionArgs eventArgs)
		{
			stopwatch.Stop();

			if (stopwatch.ElapsedMilliseconds >= MinMilliseconds)
			{

				Debug.WriteLine("{0}:{1} Elapsed: {2}",
					eventArgs.Method.ReflectedType.FullName,
					eventArgs.Method.Name,
					stopwatch.ElapsedMilliseconds);
			}
		}
	}
}
