﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Model.Dto;
using System.Collections.Generic;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using System.Windows.Media.Animation;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Authentication;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.UsersService;

namespace Finder.BloodDonation
{
    public class EmptyUserSessionManager : IUserSessionManager
    {
        public bool IsUnitAlarm(int unit)
        {
            return false;
        }

        public bool IsUnitAlarmingDisabled(int unit)
        {
            return false;
        }

        public void Dispose()
        {
        }

        public void SetMonitoringDevice(int device, DateTime? last_data)
        {

        }

        public void SetMonitoringDevice(int device, DateTime? last_data, Guid g)
        {

        }

        public void UpdateMonitoringDevice(int device, DateTime? last_data, Guid g)
        {
        }


        public bool IsOldDataUnit(int unit)
        {
            return false;
        }
    }
}
