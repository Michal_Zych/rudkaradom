﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Model.Dto;
using System.Collections.Generic;
using Finder.BloodDonation.Common.Layout;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Events;

namespace Finder.BloodDonation
{
    public class AlarmUnitModel
    {
        private IUnityContainer Container;
        IDynamicEventAggregator EventAggregator;

        public bool IsAlarm { get; set; }

        public bool IsDisableAlarm { get; set; }

        public bool IsTransmitterDisconnected { get; set; }

        private bool _isOldData;
        public bool IsOldData { 
            get
            {
                if(IsAlarm || IsDisableAlarm)
                {
                    return false;
                }
                else
                {
                    return _isOldData;
                }
            }
            set
            {
                _isOldData = value;
            }
        }

        public UnitDTO Unit { get; private set; }

        public List<AlarmUnitModel> Children { get; private set; }

        public event EventHandler<AlarmNotifyEventArgs> OnAlarmNotify;

        public event EventHandler<AlarmNotifyEventArgs> OnDisableAlarmNotify;

        public event EventHandler<AlarmNotifyEventArgs> OnOldDataNotify;

        public event EventHandler<AlarmNotifyEventArgs> OnTransmitterDisconnectedNotify;

        public AlarmUnitModel(IUnityContainer container)
        {
            Container = container;
            EventAggregator = container.Resolve<IDynamicEventAggregator>();
        }

        public void SetUnit(UnitDTO unit)
        {
            Unit = unit;

            if (unit.Children != null)
            {
                Children = new List<AlarmUnitModel>();
                foreach (UnitDTO u in unit.Children)
                {
                    AlarmUnitModel vm = Container.Resolve<AlarmUnitModel>();
                    vm.SetUnit(u);
                    vm.OnAlarmNotify += new EventHandler<AlarmNotifyEventArgs>(vm_OnAlarmNotify);
                    vm.OnDisableAlarmNotify += new EventHandler<AlarmNotifyEventArgs>(vm_OnDisableAlarmNotify);
                    vm.OnOldDataNotify += new EventHandler<AlarmNotifyEventArgs>(vm_OnOldDataNotify);
                    vm.OnTransmitterDisconnectedNotify += new EventHandler<AlarmNotifyEventArgs>(vm_OnTransmitterDisconnectedNotify);
                    Children.Add(vm);
                }
            }
        }


        void vm_OnOldDataNotify(object sender, AlarmNotifyEventArgs e)
        {
            if (e.State) //ustawienie alarmu
            {
                if (!IsOldData)
                {
                    IsOldData = true;
                    if (OnOldDataNotify != null)
                    {
                        OnOldDataNotify(sender, e);
                    }
                    EventAggregator.GetEvent<AlarmsUnitOldDataSet>().Publish(Unit.ID);
                }
            }
            else //odwołanie alarmu
            {
                if (IsOldData)
                {
                    IsOldData = false;
                    if (OnOldDataNotify != null)
                    {
                        OnOldDataNotify(sender, e);
                    }
                    EventAggregator.GetEvent<AlarmsUnitOldDataDispose>().Publish(Unit.ID);
                }
            }
        }

        void vm_OnDisableAlarmNotify(object sender, AlarmNotifyEventArgs e)
        {
            if (e.State) //ustawienie alarmu
            {
                if (!IsDisableAlarm)
                {
                    IsDisableAlarm = true;
                    if (OnDisableAlarmNotify != null)
                    {
                        OnDisableAlarmNotify(sender, e);
                    }
                    EventAggregator.GetEvent<AlarmsUnitAlarmDisableSet>().Publish(Unit.ID);
                }
            }
            else //odwołanie alarmu
            {
                if (IsDisableAlarm)
                {
                    IsDisableAlarm = false;
                    if (OnDisableAlarmNotify != null)
                    {
                        OnDisableAlarmNotify(sender, e);
                    }
                    EventAggregator.GetEvent<AlarmsUnitAlarmDisableDispose>().Publish(Unit.ID);
                }
            }
        }

        void vm_OnTransmitterDisconnectedNotify(object sender, AlarmNotifyEventArgs e)
        {
            if(e.State) // brak łączności transmitera
            {
                if(!IsTransmitterDisconnected)
                {
                    IsTransmitterDisconnected = true;
                    if(OnTransmitterDisconnectedNotify != null)
                    {
                        OnTransmitterDisconnectedNotify(sender, e);
                    }
                    EventAggregator.GetEvent<AlarmsTransmitterDisconnectedSet>().Publish(Unit.ID);
                }

            }
            else  // powrót łączności
            {
                if(IsTransmitterDisconnected)
                {
                    IsTransmitterDisconnected = false;
                    if(OnTransmitterDisconnectedNotify != null)
                    {
                        OnTransmitterDisconnectedNotify(sender, e);
                    }
                    EventAggregator.GetEvent<AlarmsTransmitterDisconnectedDispose>().Publish(Unit.ID);
                }
            }
        }



        void vm_OnAlarmNotify(object sender, AlarmNotifyEventArgs e)
        {
            if (e.State) //ustawienie alarmu
            {
                if (!IsAlarm)
                {
                    IsAlarm = true;
                    if (OnAlarmNotify != null)
                    {
                        OnAlarmNotify(sender, e);
                    }
                    EventAggregator.GetEvent<AlarmsUnitAlarmSet>().Publish(Unit.ID);
                }
            }
            else //odwołanie alarmu
            {
                if (IsAlarm)
                {
                    IsAlarm = false;
                    if (OnAlarmNotify != null)
                    {
                        OnAlarmNotify(sender, e);
                    }
                    EventAggregator.GetEvent<AlarmsUnitAlarmDispose>().Publish(Unit.ID);
                }
            }
        }

        public void SetAlarm(int[] units)
        {
            if (units.Contains(Unit.ID))
            {
                if (!IsAlarm)
                {
                    IsAlarm = true;
                    PublishAlarmNotify();
                }
            }
            else // nie ma aktywnego alarmu ale jeżeli jest ustawiony stan to znaczy że trzeba odwołać
            {
                if (IsAlarm)
                {
                    IsAlarm = false;
                    PublishAlarmNotify();
                }
            }

            for (int i = 0; i < Children.Count; i++)
            {
                Children[i].SetAlarm(units);
                if (Children[i].IsAlarm)
                {
                    IsAlarm = true;
                    PublishAlarmNotify();
                }
            }
        }

        public void SetDisableAlarm(int[] units)
        {
            if (units.Contains(Unit.ID))
            {
                if (!IsDisableAlarm)
                {
                    IsDisableAlarm = true;
                    PublishDisableAlarmNotify();
                }
            }
            else 
            {
                if (IsDisableAlarm)
                {
                    IsDisableAlarm = false;
                    PublishDisableAlarmNotify();
                }
            }
            
            
            for (int i = 0; i < Children.Count; i++)
            {
                Children[i].SetDisableAlarm(units);
            }
        }


        public void SetTransmitterDisconnected(int[] units)
        {
            if (units.Contains(Unit.ID))
            {
                if (!IsTransmitterDisconnected)
                {
                    IsTransmitterDisconnected = true;
                    PublishTransmitterDisconnectedNotify();
                }
            }
            else
            {
                if (IsTransmitterDisconnected)
                {
                    IsTransmitterDisconnected = false;
                    PublishTransmitterDisconnectedNotify();
                }
            }


            for (int i = 0; i < Children.Count; i++)
            {
                Children[i].SetTransmitterDisconnected(units);
            }
        }



        public void SetOldData(int[] units)
        {
           if (units.Contains(Unit.ID))
            {
                if (!IsOldData)
                {
                    IsOldData = true;
                    PublishOldDataNotify();
                }
            }
            else
            {
                if (IsOldData)
                {
                    IsOldData = false;
                    PublishOldDataNotify();
                }
            }

            for (int i = 0; i < Children.Count; i++)
            {
                Children[i].SetOldData(units);
                if (Children[i].IsOldData)
                {
                    IsOldData = true;
                    PublishOldDataNotify();
                }
            }
        }


        private void PublishAlarmNotify()
        {
            if (OnAlarmNotify != null)
            {
                OnAlarmNotify(this, new AlarmNotifyEventArgs() { State = IsAlarm });
            }

            if (IsAlarm)
            {
                EventAggregator.GetEvent<AlarmsUnitAlarmSet>().Publish(Unit.ID);
            }
            else
            {
                EventAggregator.GetEvent<AlarmsUnitAlarmDispose>().Publish(Unit.ID);
            }
        }

        private void PublishDisableAlarmNotify()
        {
            if (OnAlarmNotify != null)
            {
                //OnAlarmNotify(this, new AlarmNotifyEventArgs() { State = IsDisableAlarm });
            }

            if (IsDisableAlarm)
            {
                EventAggregator.GetEvent<AlarmsUnitAlarmDisableSet>().Publish(Unit.ID);
            }
            else
            {
                EventAggregator.GetEvent<AlarmsUnitAlarmDisableDispose>().Publish(Unit.ID);
            }
        }

        private void PublishTransmitterDisconnectedNotify()
        {
            if(OnTransmitterDisconnectedNotify != null)
            {

            }
            if(IsTransmitterDisconnected)
            {
                EventAggregator.GetEvent<AlarmsTransmitterDisconnectedSet>().Publish(Unit.ID);
            }
            else
            {
                EventAggregator.GetEvent<AlarmsTransmitterDisconnectedDispose>().Publish(Unit.ID);
            }
        }


        private void PublishOldDataNotify()
        {
            
            if (IsOldData)
            {
                EventAggregator.GetEvent<AlarmsUnitOldDataSet>().Publish(Unit.ID);
            }
            else
            {
                EventAggregator.GetEvent<AlarmsUnitOldDataDispose>().Publish(Unit.ID);
            }
        }


        public bool IsUnitAlarm(int unit)
        {
            if (Unit.ID == unit)
            {
                return IsAlarm;
            }

            for (int i = 0; i < Children.Count; i++)
            {
                if (Children[i].IsUnitAlarm(unit))
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsUnitAlarmingDisabled(int unit)
        {
            if (Unit.ID == unit)
            {
                return IsDisableAlarm;
            }

            for (int i = 0; i < Children.Count; i++)
            {
                if (Children[i].IsUnitAlarmingDisabled(unit))
                {
                    return true;
                }
            }

            return false;
        }



        public bool IsOldDataUnit(int unit)
        {

            if (Unit.ID == unit)
            {
                return IsOldData;
            }

            for (int i = 0; i < Children.Count; i++)
            {
                if (Children[i].IsOldDataUnit(unit))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
