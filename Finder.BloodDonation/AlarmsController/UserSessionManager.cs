﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Model.Dto;
using System.Collections.Generic;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using System.Windows.Media.Animation;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Authentication;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.UsersService;
using Finder.BloodDonation.Common.Layout;
using DanielVaughan.Logging;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.DynamicData.DBs;

namespace Finder.BloodDonation
{
    public class UserSessionManager : IUserSessionManager
    {
        private static ILog Logger = LogManager.GetLog(typeof(UserSessionManager));

        IUnityContainer _container;
        IDynamicEventAggregator _eventAggregator;
        private bool _disposing = false;

        private List<AlarmUnitModel> _availableUnits;
        public List<AlarmUnitModel> AvailableUnits 
        { 
            get
            {
                return _availableUnits;
            }
            set
            {
                _availableUnits = value;
            }
        }

        public List<AlarmDto> Alarms { get; set; }
        public List<LocalizationAlarmDto> LocalizationAlarms { get; set; }
        public List<int> UnitIdWithDisconnectedTransmitter { get; set; }

        public List<DisableAlarmUnitDto> DisableAlarmUnits { get; set; }
        public List<DisableAlarmUnitDto> OldDataUnits { get; set; }

        public List<AlarmDetailsDto> AlarmsDetails { get; set; }

        public int? LowestLocalizationAlarmSeverity { get; set; }



        private DateTime _lastPing = DateTime.Now;

        private DateTime? _monitoringDeviceLastData;
        private int? _monitoringDeviceDeviceId;
        private Guid _monitoringDeviceViewModelGuid;



        private int _pingCount = 0;
        private int _cyclicCount = 0;

        private Storyboard _pingStoryboard;

        private Storyboard _soundStoryboard;

        private Storyboard _cyclicActionsStoryboard;

        private UsersServiceClient LoadServiceUsers
        {
            get
            {
                UsersServiceClient proxy = ServiceFactory.GetService<UsersServiceClient>(ServicesUri.UsersService);
                proxy.PingUserSessionCompleted += proxy_PingUserSessionCompleted;
                return proxy;
            }
        }

        private UnitsServiceClient LoadServiceUnits
        {
            get
            {
                UnitsServiceClient proxy = ServiceFactory.GetService<UnitsServiceClient>(ServicesUri.UnitsService);
                proxy.GetRootUnitCompleted += GetRootUnitCompletedEventHandler;
                return proxy;
            }
        }

        public UserSessionManager(IUnityContainer container)
        {
            _container = container;
            _eventAggregator = container.Resolve<IDynamicEventAggregator>();

            //EventAggregator.GetEvent<AlarmsNewReceived>().Subscribe(NewAlarms_Event);
            _eventAggregator.GetEvent<AlarmsCloseAlarms>().Subscribe(CloseAlarms_Event);
            _eventAggregator.GetEvent<UnitChangedEvent>().Subscribe(UnitChangedEvent_Event);

            LoadServiceUnits.GetRootUnitAsync();
        }

        public void UnitChangedEvent_Event(IUnit u)
        {
            _monitoringDeviceDeviceId = null;
            _monitoringDeviceLastData = null;
        }

        void ping_story_Completed(object sender, EventArgs e)
        {
            if (_pingStoryboard != null)
            {
                _pingStoryboard.Stop();
            }
            if (!_disposing)
            {
                _pingCount++;
               
                    LoadServiceUsers.PingUserSessionAsync(new PingRequestDto()
                {
                    AlarmDateStart = _lastPing,
                    MonitoringLastDataDate = _monitoringDeviceLastData,
                    MonitoringDeviceID = _monitoringDeviceDeviceId,
                    LowestLocalizationAlarmSeverity = LowestLocalizationAlarmSeverity
                });
                _pingStoryboard.Begin();
            }
        }

        void proxy_PingUserSessionCompleted(object sender, PingUserSessionCompletedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("ping user session completed at {0}", DateTime.Now);

            if (e.Result != null && !_disposing)
            {
                _lastPing = DateTime.Now;

                if (e.Result.Alarms != null)
                {
                    Alarms = new List<AlarmDto>(e.Result.Alarms);
                }
                else
                {
                    Alarms = new List<AlarmDto>();
                }

                if (e.Result.DisableAlarmUnits != null)
                {
                    if (DisableAlarmUnits != null)
                    {
                        DisableAlarmUnits.Clear();
                    }
                    DisableAlarmUnits = new List<DisableAlarmUnitDto>(e.Result.DisableAlarmUnits);
                }

                if(e.Result.AlarmsDetails != null)
                {
                    if(AlarmsDetails != null)
                    {
                        AlarmsDetails.Clear();
                    }
                    AlarmsDetails = new List<AlarmDetailsDto>(e.Result.AlarmsDetails);
                }



                if(e.Result.LocalizationAlarms != null)
                {
                    LocalizationAlarms = new List<LocalizationAlarmDto>(e.Result.LocalizationAlarms);
                }
                else
                {
                    LocalizationAlarms = new List<LocalizationAlarmDto>();
                }

                if (e.Result.UnitsIdWithDisconnectedTransmitter != null)
                {
                    if(UnitIdWithDisconnectedTransmitter != null)
                    {
                        UnitIdWithDisconnectedTransmitter.Clear();
                    }
                    UnitIdWithDisconnectedTransmitter = new List<int>(e.Result.UnitsIdWithDisconnectedTransmitter);
                }
                else
                {
                    UnitIdWithDisconnectedTransmitter = new List<int>();
                }

                if (e.Result.OldDataUnits != null)
                {
                    OldDataUnits = new List<DisableAlarmUnitDto>(e.Result.OldDataUnits);
                }
                else
                {
                    OldDataUnits = new List<DisableAlarmUnitDto>();
                }
                

                if (Alarms != null)
                {
                    Logger.Debug(string.Format("Aktualna lista alarmów: {0}", string.Join(",", (from a in Alarms select a.UnitID).ToArray())));

                    for (int i = 0; i < AvailableUnits.Count; i++)
                    {
                        AvailableUnits[i].SetAlarm((from a in Alarms select a.UnitID).ToArray());
                    }
                }

                if (DisableAlarmUnits != null)
                {
                    for (int i = 0; i < AvailableUnits.Count; i++)
                    {
                        AvailableUnits[i].SetDisableAlarm((from a in DisableAlarmUnits select a.UnitID).ToArray());
                    }
                }

                if(OldDataUnits != null)
                {
                    //System.Diagnostics.Debug.WriteLine(string.Format("Aktualna lista przestarzałych danych: {0}", string.Join(",", (from a in OldDataUnits select a.UnitID).ToArray())));

                    for(int i = 0; i < AvailableUnits.Count; i++)
                    {
                        AvailableUnits[i].SetOldData((from a in OldDataUnits select a.UnitID).ToArray());
                    }
                }

                if (UnitIdWithDisconnectedTransmitter != null)
                {
                    for (int i = 0; i < AvailableUnits.Count; i++)
                    {
                        AvailableUnits[i].SetTransmitterDisconnected(UnitIdWithDisconnectedTransmitter.ToArray());
                    }
                }


                _eventAggregator.GetEvent<AlarmsNewReceived>().Publish(Alarms);
                _eventAggregator.GetEvent<AlarmsDetailsRecived>().Publish(AlarmsDetails);
                _eventAggregator.GetEvent<UnitIdWithDisconnectedTransmitterReceived>().Publish(UnitIdWithDisconnectedTransmitter);
                _eventAggregator.GetEvent<LocalizationAlarmsReceived>().Publish(LocalizationAlarms);


                //r.sz. -impuls czasowy tylko gdy pojawiły się nowe dane do wykresu
                /*
                if (e.Result.MonitoringDeviceRefreshData && _monitoringDeviceDeviceId.HasValue)
                {
                    _eventAggregator.GetEvent<RefreshDeviceDataEvent>().Publish(_monitoringDeviceDeviceId.Value);
                }
                 */
                _eventAggregator.GetEvent<RefreshDeviceDataEvent>().Publish(-1);

                
                if (e.Result.Messages != null && e.Result.Messages.Count > 0)
                {
                    _eventAggregator.GetEvent<PlayNewMessage>().Publish(e.Result.Messages.Count);
                    foreach (var s in e.Result.Messages)
                    {
                        MsgBox.Warning(s);
                    }
                }

                _eventAggregator.GetEvent<PingUserSessionEvent>().Publish(string.Empty);
                
            }

            // 8 minut
            if ((_pingCount % 24) == 0)
            {
                if (_monitoringDeviceDeviceId.HasValue)
                {
                    _eventAggregator.GetEvent<RefreshDeviceHistoryEvent>().Publish(_monitoringDeviceDeviceId.Value);
                }
            }
        }

        public void CloseAlarms_Event(IList<AlarmDto> o)
        {
            ping_story_Completed(null, null);
        }

        void GetRootUnitCompletedEventHandler(object sender, GetRootUnitCompletedEventArgs e)
        {
            if (!_disposing)
            {
                AvailableUnits = new List<AlarmUnitModel>();
                AvailableUnits.Add(ConvertUnit(e.Result));

                CyclicActionsEventHandler(null, null);
                StartCyclicAction();

                int seconds = Math.Max(Settings.Settings.Current.GetInt("MinPingSessionIntervalSecs")
                                        ,Settings.Settings.Current.GetInt(ConfigurationKeys.PingSessionIntervalSecs));

                var bu = User.Current as BloodyUser;
                _pingStoryboard = new Storyboard()
                {
                    Duration = new System.Windows.Duration(new TimeSpan(0, 0, seconds))
                };
                _pingStoryboard.Completed += ping_story_Completed;
                _pingStoryboard.Begin();

                ping_story_Completed(null, null);

                if (Bootstrapper.UserMonitoringState)
                {
                    seconds = Math.Max(Settings.Settings.Current.GetInt("MinMonitoringSoundIntervalSecs")
                                        ,Settings.Settings.Current.GetInt(ConfigurationKeys.MonitoringSoundIntervalSecs));
                    _soundStoryboard = new Storyboard()
                    {
                        Duration = new System.Windows.Duration(new TimeSpan(0, 0, seconds))
                    };
                    _soundStoryboard.Completed += new EventHandler(ping_sound_story_Completed);
                    _soundStoryboard.Begin();
                }
                
            }
        }

        private void StartCyclicAction()
        {
            DbBandStatus.Initialize(_container);
            _cyclicActionsStoryboard = new Storyboard()
            {
                Duration = new System.Windows.Duration(new TimeSpan(0, 0, 15))
            };
            _cyclicActionsStoryboard.Completed += CyclicActionsEventHandler;
            _cyclicActionsStoryboard.Begin();
        }

        private void CyclicActionsEventHandler(object sender, EventArgs e)
        {
            if (_cyclicActionsStoryboard != null)
            {
                _cyclicActionsStoryboard.Stop();
            }

            if (!_disposing)
            {
                _eventAggregator.GetEvent<AutoBandStatusesRefreshEvent>().Publish(_cyclicCount++);

                if (_cyclicActionsStoryboard != null)
                    _cyclicActionsStoryboard.Begin();
            }
        }

        void ping_sound_story_Completed(object sender, EventArgs e)
        {
            if (!_disposing)
            {
                int alarmsCount = 0;
                if (LocalizationAlarms != null) alarmsCount = LocalizationAlarms.Count;
                //if (Alarms != null) alarmsCount = Alarms.Count;

                if (alarmsCount > 0 && Bootstrapper.UserMonitoringState)
                {
                    _eventAggregator.GetEvent<PlayAlarmSound>().Publish(alarmsCount);
                }
                _soundStoryboard.Begin();
            }
        }

        private AlarmUnitModel ConvertUnit(UnitDTO unit)
        {
            if (!_disposing)
            {
                AlarmUnitModel vm = this._container.Resolve<AlarmUnitModel>();
                vm.SetUnit(unit);
                return vm;
            }

            return null;
        }

        public void NewAlarms_Event(List<AlarmDto> a)
        {
            if (AvailableUnits != null && a != null)
            {
                for (int i = 0; i < AvailableUnits.Count; i++)
                {
                    AvailableUnits[i].SetAlarm((from u in a select u.UnitID).ToArray());
                }
            }
        }

        public bool IsUnitAlarm(int unit)
        {
            if (AvailableUnits != null)
            {
                for (int i = 0; i < AvailableUnits.Count; i++)
                {
                    if (AvailableUnits[i].IsUnitAlarm(unit))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool IsUnitAlarmingDisabled(int unit)
        {
            if (AvailableUnits != null)
            {
                for (int i = 0; i < AvailableUnits.Count; i++)
                {
                    if (AvailableUnits[i].IsUnitAlarmingDisabled(unit))
                    {
                        return true;
                    }
                }
            }
            return false;
        }


        public void Dispose()
        {
            _disposing = true;
            if (_pingStoryboard != null)
            {
                _pingStoryboard.Stop();
            }
            _pingStoryboard = null;
            _container = null;
            _eventAggregator = null;
            if (AvailableUnits != null)
            {
                AvailableUnits.Clear();
            }
            AvailableUnits = null;
            if (Alarms != null)
            {
                Alarms.Clear();
            }
            Alarms = null;
        }

        public void SetMonitoringDevice(int device, DateTime? last_data)
        {
            _monitoringDeviceDeviceId = device;
            _monitoringDeviceLastData = last_data;
        }

        public void SetMonitoringDevice(int device, DateTime? last_data, Guid g)
        {
            _monitoringDeviceDeviceId = device;
            _monitoringDeviceLastData = last_data;
            _monitoringDeviceViewModelGuid = g;
        }

        public void UpdateMonitoringDevice(int device, DateTime? last_data, Guid g)
        {
            if (_monitoringDeviceViewModelGuid == g && _monitoringDeviceDeviceId == device)
            {
                _monitoringDeviceLastData = last_data;
            }
        }


        public bool IsOldDataUnit(int unit)
        {
            if (AvailableUnits != null)
            {
                for (int i = 0; i < AvailableUnits.Count; i++)
                {
                    if (AvailableUnits[i].IsOldDataUnit(unit))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
