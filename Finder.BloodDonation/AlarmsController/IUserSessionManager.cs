﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation
{
    public interface IUserSessionManager : IDisposable
    {
        bool IsUnitAlarm(int unit);

        bool IsUnitAlarmingDisabled(int unit);

        bool IsOldDataUnit(int unit); 

        void SetMonitoringDevice(int device, DateTime? last_data);

        void SetMonitoringDevice(int device, DateTime? last_data, Guid g);

        void UpdateMonitoringDevice(int device, DateTime? last_data, Guid g);
    }
}
