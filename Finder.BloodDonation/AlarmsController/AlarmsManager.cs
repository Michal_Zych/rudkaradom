﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Model.Dto;
using System.Collections.Generic;
using Finder.BloodDonation.ModelService;
using FinderFX.SL.Core.Communication;
using System.Windows.Media.Animation;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Authentication;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.UsersService;
using Finder.BloodDonation.Common.Layout;
using DanielVaughan.Logging;

namespace Finder.BloodDonation
{
    public class AlarmsManager : IAlarmsManager
    {
        private static ILog Logger = LogManager.GetLog(typeof(AlarmsManager));

        IUnityContainer Container;
        IDynamicEventAggregator EventAggregator;
        private bool disposing = false;

        public List<AlarmUnitModel> AvailableUnits { get; set; }

        public List<AlarmDto> Alarms { get; set; }

        public List<DisableAlarmUnitDto> DisableAlarmUnits { get; set; }

        private DateTime LastPing = DateTime.Now;

        private DateTime? MonitoringDevice_LastData = null;
        private int? MonitoringDevice_DeviceID = null;
        private Guid MonitoringDevice_ViewModelGuid;

        private int PingCount = 0;

        private Storyboard ping_story = null;

        private Storyboard ping_sound_story = null;

        private UsersServiceClient LoadServiceUsers
        {
            get
            {
                UsersServiceClient proxy = ServiceFactory.GetService<UsersServiceClient>(ServicesUri.UsersService);
                proxy.PingUserSessionCompleted += new EventHandler<PingUserSessionCompletedEventArgs>(proxy_PingUserSessionCompleted);
                return proxy;
            }
        }

        private UnitsServiceClient LoadServiceUnits
        {
            get
            {
                UnitsServiceClient proxy = ServiceFactory.GetService<UnitsServiceClient>(ServicesUri.UnitsService);
                proxy.GetRootUnitCompleted += new EventHandler<GetRootUnitCompletedEventArgs>(proxy_GetRootUnitCompleted);
                return proxy;
            }
        }

        public AlarmsManager(IUnityContainer container)
        {
            Container = container;
            EventAggregator = container.Resolve<IDynamicEventAggregator>();

            //EventAggregator.GetEvent<AlarmsNewReceived>().Subscribe(NewAlarms_Event);
            EventAggregator.GetEvent<AlarmsCloseAlarm>().Subscribe(CloseAlarm_Event);
            EventAggregator.GetEvent<UnitChangedEvent>().Subscribe(UnitChangedEvent_Event);

            LoadServiceUnits.GetRootUnitAsync();
        }

        public void UnitChangedEvent_Event(IUnit u)
        {
            MonitoringDevice_DeviceID = null;
            MonitoringDevice_LastData = null;
        }

        void ping_story_Completed(object sender, EventArgs e)
        {
            if (ping_story != null)
            {
                ping_story.Stop();
            }
            if (!disposing)
            {
                PingCount++;
                LoadServiceUsers.PingUserSessionAsync(new PingRequestDto()
                {
                    AlarmDateStart = LastPing,
                    MonitoringLastDataDate = MonitoringDevice_LastData,
                    MonitoringDeviceID = MonitoringDevice_DeviceID
                });
                ping_story.Begin();
            }
        }

        void proxy_PingUserSessionCompleted(object sender, PingUserSessionCompletedEventArgs e)
        {
            if (e.Result != null && !disposing)
            {
                LastPing = DateTime.Now;

                if (e.Result.Alarms != null)
                {
                    Alarms = new List<AlarmDto>(e.Result.Alarms);
                }
                else
                {
                    Alarms = new List<AlarmDto>();
                }

                if (e.Result.DisableAlarmUnits != null)
                {
                    if (DisableAlarmUnits != null)
                    {
                        DisableAlarmUnits.Clear();
                    }
                    DisableAlarmUnits = new List<DisableAlarmUnitDto>(e.Result.DisableAlarmUnits);
                }

                if (Alarms != null)
                {
                    Logger.Debug(string.Format("Aktualna lista alarmów: {0}", string.Join(",", (from a in Alarms select a.DeviceUnitID).ToArray())));

                    for (int i = 0; i < AvailableUnits.Count; i++)
                    {
                        AvailableUnits[i].SetAlarm((from a in Alarms select a.DeviceUnitID).ToArray());
                    }
                }

                if (DisableAlarmUnits != null)
                {
                    for (int i = 0; i < AvailableUnits.Count; i++)
                    {
                        AvailableUnits[i].SetDisableAlarm((from a in DisableAlarmUnits select a.UnitID).ToArray());
                    }
                }

                EventAggregator.GetEvent<AlarmsNewReceived>().Publish(Alarms);

                if (e.Result.MonitoringDeviceRefreshData && MonitoringDevice_DeviceID.HasValue)
                {
                    EventAggregator.GetEvent<RefreshDeviceDataEvent>().Publish(MonitoringDevice_DeviceID.Value);
                    EventAggregator.GetEvent<RefreshDeviceExtDataEvent>().Publish(new object[2] { MonitoringDevice_DeviceID.Value, MonitoringDevice_ViewModelGuid });
                }
            }

            // 8 minut
            if ((PingCount % 24) == 0)
            {
                if (MonitoringDevice_DeviceID.HasValue)
                {
                    EventAggregator.GetEvent<RefreshDeviceHistoryEvent>().Publish(MonitoringDevice_DeviceID.Value);
                }
            }
        }

        public void CloseAlarm_Event(AlarmDto o)
        {
            ping_story_Completed(null, null);
        }

        void proxy_GetRootUnitCompleted(object sender, GetRootUnitCompletedEventArgs e)
        {
            if (!disposing)
            {
                AvailableUnits = new List<AlarmUnitModel>();
                AvailableUnits.Add(ConvertUnit(e.Result));

                BloodyUser bu = User.Current as BloodyUser;
                ping_story = new Storyboard()
                {
                    Duration = new System.Windows.Duration(new TimeSpan(0, 0, 20))
                };
                ping_story.Completed += new EventHandler(ping_story_Completed);
                ping_story.Begin();

                ping_story_Completed(null, null);

                if (Bootstrapper.UserMonitoringState)
                {
                    ping_sound_story = new Storyboard()
                    {
                        Duration = new System.Windows.Duration(new TimeSpan(0, 0, 30))
                    };
                    ping_sound_story.Completed += new EventHandler(ping_sound_story_Completed);
                    ping_sound_story.Begin();
                }
            }
        }

        void ping_sound_story_Completed(object sender, EventArgs e)
        {
            if (!disposing)
            {
                if (Alarms != null && Alarms.Count > 0 && Bootstrapper.UserMonitoringState)
                {
                    EventAggregator.GetEvent<PlayAlarmSound>().Publish(Alarms.Count);
                }
                ping_sound_story.Begin();
            }
        }

        private AlarmUnitModel ConvertUnit(UnitDTO unit)
        {
            if (!disposing)
            {
                AlarmUnitModel vm = this.Container.Resolve<AlarmUnitModel>();
                vm.SetUnit(unit);
                return vm;
            }

            return null;
        }

        public void NewAlarms_Event(List<AlarmDto> a)
        {
            if (AvailableUnits != null && a != null)
            {
                for (int i = 0; i < AvailableUnits.Count; i++)
                {
                    AvailableUnits[i].SetAlarm((from u in a select u.DeviceUnitID).ToArray());
                }
            }
        }

        public bool IsUnitAlarm(int unit)
        {
            if (AvailableUnits != null)
            {
                for (int i = 0; i < AvailableUnits.Count; i++)
                {
                    if (AvailableUnits[i].IsUnitAlarm(unit))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public void Dispose()
        {
            disposing = true;
            if (ping_story != null)
            {
                ping_story.Stop();
            }
            ping_story = null;
            Container = null;
            EventAggregator = null;
            if (AvailableUnits != null)
            {
                AvailableUnits.Clear();
            }
            AvailableUnits = null;
            if (Alarms != null)
            {
                Alarms.Clear();
            }
            Alarms = null;
        }

        public void SetMonitoringDevice(int device, DateTime? last_data)
        {
            MonitoringDevice_DeviceID = device;
            MonitoringDevice_LastData = last_data;
        }

        public void SetMonitoringDevice(int device, DateTime? last_data, Guid g)
        {
            MonitoringDevice_DeviceID = device;
            MonitoringDevice_LastData = last_data;
            MonitoringDevice_ViewModelGuid = g;
        }

        public void UpdateMonitoringDevice(int device, DateTime? last_data, Guid g)
        {
            if (MonitoringDevice_ViewModelGuid == g && MonitoringDevice_DeviceID == device)
            {
                MonitoringDevice_LastData = last_data;
            }
        }
    }
}
