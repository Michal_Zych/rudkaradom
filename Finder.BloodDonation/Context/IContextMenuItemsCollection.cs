﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace Finder.BloodDonation.Context
{
	public interface IContextMenuItemsCollection
	{
		IList<IContextMenuItem> Items { get; set; }
		void Refresh();
	}

	public class GenericContextMenuItemsCollection<T> : IContextMenuItemsCollection
	{
		public T AssociatedObject { get; set; }

		public GenericContextMenuItemsCollection(T associatedObject)
		{
			this.AssociatedObject = associatedObject;
		}


		protected IList<IContextMenuItem> registeredItems;
		public void RegisterMenuItem(IContextMenuItem item)
		{
			registeredItems.Add(item);
		}


		public IList<IContextMenuItem> Items { get; set; }


		public event EventHandler Opened = delegate { };

		public virtual void RegisterItems(IList<IContextMenuItem> items)
		{
			registeredItems = items;
		}


		public virtual void Refresh()
		{
			if (registeredItems == null)
			{
				registeredItems = new List<IContextMenuItem>();
				RegisterItems(registeredItems);
			}

			if (Items == null)
			{
				Items = new List<IContextMenuItem>();
			}

			Items.Clear();
			foreach (IContextMenuItem i in registeredItems)
			{
				if (i.IsAvailable())
					Items.Add(i);
			}
		}
	}
}
