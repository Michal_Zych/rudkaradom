﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Context
{
	public interface IContextMenuItem
	{
		object Header { get; set; }
		object Icon { get; set; }
		ICommand Command { get; set; }
		object CommandParameter { get; set; }

		bool IsAvailable();
	}

	public class GenericMenuItem : MenuItem, IContextMenuItem
	{

		#region IContextMenuItem Members


		public virtual bool IsAvailable()
		{
			return true;
		}

		#endregion
	}
}
