﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Interactivity;

namespace Finder.BloodDonation.Context
{

	public class ContextMenuBehavior : Behavior<System.Windows.Controls.ContextMenu>
	{
		private IContextMenuItemsCollection contextCollection;


		public ContextMenuBehavior(IContextMenuItemsCollection contextCollection)
		{
			this.contextCollection = contextCollection;
		}


		protected override void OnAttached()
		{
			base.OnAttached();


			this.AssociatedObject.Opened += new RoutedEventHandler(AssociatedObject_Opened);
		}

		void AssociatedObject_Opened(object sender, RoutedEventArgs e)
		{
			contextCollection.Refresh();
			if (contextCollection.Items.Count > 0)
			{
				this.AssociatedObject.ItemsSource = contextCollection.Items;
			}
			else
			{
				this.AssociatedObject.ItemsSource = null;
				this.AssociatedObject.IsOpen = false;
				//todo: choiwac menu jezeli nie ma itemow
			}
		}
	}
}
