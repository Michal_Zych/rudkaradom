﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Interactivity;

namespace Finder.BloodDonation.Context
{
	public static class ContextMenuProperties
	{
		public static IContextMenuItemsCollection GetItems(DependencyObject obj)
		{
			return (IContextMenuItemsCollection)obj.GetValue(FooProperty);
		}

		public static void SetItems(DependencyObject obj, IContextMenuItemsCollection value)
		{
			obj.SetValue(FooProperty, value);
		}

		public static readonly DependencyProperty FooProperty =
			DependencyProperty.RegisterAttached(
				"Items"
				, typeof(IContextMenuItemsCollection)
				, typeof(ContextMenuProperties)
				, new PropertyMetadata(OnSetCommandCallbackSelected));


		private static void OnSetCommandCallbackSelected(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
		{
			System.Windows.Controls.ContextMenu element = dependencyObject as System.Windows.Controls.ContextMenu;
			if (element == null)
				throw new ArgumentException("ContextMenuProperties.Items property can be applied only for ContextMenu control");

			BehaviorCollection behaviors = Interaction.GetBehaviors(element);
			behaviors.Add(new ContextMenuBehavior(e.NewValue as IContextMenuItemsCollection));

			element.SetValue(Interaction.BehaviorsProperty, behaviors);

		}
	}
}
