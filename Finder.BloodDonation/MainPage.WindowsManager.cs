﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;

namespace Finder.BloodDonation
{
    public partial class MainPage : IWindowsManager
    {
        private Dictionary<string, DialogWindow> _windows;


        public void RegisterWindow(string key, string name, UserControl form, Size size, bool isModal, bool canResize = true)
        {
            if (_windows == null)
                this._windows = new Dictionary<string, DialogWindow>();

            //if (_windows.ContainsKey(key))
            //    throw new ArgumentException("Key must be unique");

            var dialog = new DialogWindow(name, form, size)
                                      {
                                          isModal = isModal
                                      };
            dialog.isModal = isModal;
            if (canResize) dialog.ResizeMode = Telerik.Windows.Controls.ResizeMode.CanResize;
            else dialog.ResizeMode = Telerik.Windows.Controls.ResizeMode.NoResize;

            _windows[key] = dialog;
        }

        public void RegisterWindowIfNotRegister(string key, string name, UserControl form, Size size, bool isModal, bool canResize = true)
        {
            if (_windows == null)
                this._windows = new Dictionary<string, DialogWindow>();

            if (_windows.ContainsKey(key))
                return;

            RegisterWindow(key, name, form, size, isModal, canResize);

        }

        public void CloseWindow(string key)
        {
            if (this._windows == null) throw new NullReferenceException("windows");
            var win = _windows[key];
            if (win != null)
                win.Close();
            this.ReportBackground.Visibility = Visibility.Collapsed;
            EventAggregator.GetEvent<CloseWindowEvent>().Publish(key);
        }

        public void ShowWindow(string key)
        {
            var win = GetWindow(key);
            win.ShowDialog();
            EventAggregator.GetEvent<ShowWindowEvent>().Publish(key);
        }

        public void ShowWindow(string key, object data)
        {
            this.ShowWindow(key, data, null);
        }

        public bool CheckIfWindowIsShowed(string key)
        {
            var win = GetWindow(key);

            bool isOpen = win.IsOpen;

            return isOpen;
        }

        public void ShowWindow(string key, object data, bool? isEditMode)
        {
            var win = GetWindow(key);
            win.ShowDialog(data, isEditMode);
            EventAggregator.GetEvent<ShowWindowEvent>().Publish(key);
        }

        private DialogWindow GetWindow(string key)
        {
            if (this._windows == null)
                throw new NullReferenceException("windows");
            var win = _windows[key];
            if (win != null)
            {
                if (win.isModal)
                    this.ReportBackground.Visibility = Visibility.Visible;
            }
            return win;
        }

	    private void ButtonEventsHandler_OnMouseMove(object sender, MouseEventArgs e)
	    {
			Debug.WriteLine("Event: ButtonEventsHandler_OnMouseMove");
	    }
    }
}
