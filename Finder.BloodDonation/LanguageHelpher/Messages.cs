﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.LanguageHelpher
{
    public class Messages
    {
        public Language CurrentLanguage { get; set; }

        public IDictionary<MessageCodes, LanguageMessage> MessageDictionary { get; private set; }

        public Messages(Language applicationLanguage = Language.PL)
        {
            CurrentLanguage = applicationLanguage;
            MessageDictionary = new Dictionary<MessageCodes, LanguageMessage>();

            LanguageMessage e = new LanguageMessage();
            e.Add(Language.PL, "Urządzenie jest już dodane. Czy chcesz je usunąć i dodać ponownie?");
            e.Add(Language.EN, "The patient is already registered in the hospital. \n Do you want to unsubscribe and accept it again?");
            MessageDictionary.Add(MessageCodes.UNREGISTER_AND_REGISTER_PATIENT_QUESTION, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Pomyślnie dodano urządzenie!");
            e.Add(Language.EN, "The patient has been successfully added!");
            MessageDictionary.Add(MessageCodes.SUCCESSFULL_REGISTRATION_PATIENT, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Przypisz lokalizator urządzeniu");
            e.Add(Language.EN, "Assign band to patient");
            MessageDictionary.Add(MessageCodes.ASSIGN_BAND_TO_PATIENT,e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Pomyślnie przypisano lokalizator!");
            e.Add(Language.EN, "Succesfull assigned band to patient");
            MessageDictionary.Add(MessageCodes.SUCCESFULL_ASSIGNED_BAND_TO_PATIENT,e);
        }

        public string Message(MessageCodes messageCodes)
        {
            if (MessageDictionary == null)
                throw new NullReferenceException("ControlsTextDictionary is not initialized. Please first use InitializeControls!");

            if (MessageDictionary[messageCodes] == null)
                throw new NullReferenceException("ErrorCode is not found in ErrorsDictionary.");

            return MessageDictionary[messageCodes].Get();
        }

        public string Message(MessageCodes messageCodes, Language language)
        {
            if (MessageDictionary == null)
                throw new NullReferenceException("ErrorsDictionary is not initialized. Please first use InitializeControls!");

            if (MessageDictionary[messageCodes] == null)
                throw new NullReferenceException("ErrorCode is not found in ErrorsDictionary.");

            return MessageDictionary[messageCodes].Get(language);
        }
    }

    public enum MessageCodes
    {
        UNREGISTER_AND_REGISTER_PATIENT_QUESTION,
        SUCCESSFULL_REGISTRATION_PATIENT,
        ASSIGN_BAND_TO_PATIENT,
        SUCCESFULL_ASSIGNED_BAND_TO_PATIENT

    }
}
