﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.LanguageHelpher
{
    public class LanguageMessage
    {
        private Dictionary<Language, string> Message { get; set; }

        public LanguageMessage()
        {
            Message = new Dictionary<Language, string>();
        }

        public void Add(Language language, string message)
        {
            if (message == String.Empty)
                throw new ArgumentException("Message argument can't by empty.");

            if (Message == null)
                throw new NullReferenceException("Message dictionary is not initialize");

            if (Message.ContainsKey(language))
                throw new Exception("This language is exists in Message. Please use Edit method to change value.");

            Message.Add(language, message);
        }

        public void Edit(Language language, string newMessage)
        {
            if (newMessage == String.Empty)
                throw new ArgumentException("Message argument can't by empty.");

            if (Message == null)
                throw new NullReferenceException("Message dictionary is not initialize");

            if (!Message.ContainsKey(language))
                throw new Exception("This language is not exists in Message. Please use Add method to add value.");

            Message[language] = newMessage;
        }

        public void Remove(Language language)
        {
            if (Message == null)
                throw new NullReferenceException("Message dictionary is not initialize");

            if (!Message.ContainsKey(language))
                throw new Exception("This language is not exists in Message. Please use Edit method to change value.");

            Message.Remove(language);
        }

        public string Get()
        {
            if (Message == null)
                throw new NullReferenceException("Message dictionary is not initialize");

            if (!Message.ContainsKey(LanguageManager.CurrentLanguage))
                throw new Exception("This language is not exists in Message. Please use Edit method to change value.");

            return Message[LanguageManager.CurrentLanguage];
        }

        public string Get(Language language)
        {
            if (Message == null)
                throw new NullReferenceException("Message dictionary is not initialize");

            if (!Message.ContainsKey(language))
                throw new Exception("This language is not exists in Message. Please use Edit method to change value.");

            return Message[language];
        }
    }

    public enum Language
    {
        PL = 0,
        EN = 1,
    }
}
