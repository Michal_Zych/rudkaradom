﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.LanguageHelpher
{
    public class ControlsText
    {
        public Language CurrentLanguage { get; set; }

        public IDictionary<ControlsTextCodes, LanguageMessage> ControlsTextDictionary { get; private set; }

        public ControlsText(Language applicationLanguage=Language.PL)
        {
            CurrentLanguage = applicationLanguage;
            ControlsTextDictionary = new Dictionary<ControlsTextCodes, LanguageMessage>();

            LanguageMessage e = new LanguageMessage();

            e.Add(Language.PL, "Urządzenia medyczne");
            e.Add(Language.EN, "Medical devices");
            ControlsTextDictionary.Add(ControlsTextCodes.TABS_PATIENTS, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Monitorowanie");
            e.Add(Language.EN, "Monitoring");
            ControlsTextDictionary.Add(ControlsTextCodes.TABS_MONITORABLE, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Lokalizacja");
            e.Add(Language.EN, "Localization");
            ControlsTextDictionary.Add(ControlsTextCodes.TABS_LOCALIZATION, e);


            e = new LanguageMessage();
            e.Add(Language.PL, "Zdarzenia");
            e.Add(Language.EN, "Events");
            ControlsTextDictionary.Add(ControlsTextCodes.TABS_EVENTS, e);


            e = new LanguageMessage();
            e.Add(Language.PL, "Raporty");
            e.Add(Language.EN, "Raports");
            ControlsTextDictionary.Add(ControlsTextCodes.TABS_RAPORTS, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Konfiguracja");
            e.Add(Language.EN, "Configuration");
            ControlsTextDictionary.Add(ControlsTextCodes.TABS_CONFIGURATION, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Planowanie");
            e.Add(Language.EN, "Planning");
            ControlsTextDictionary.Add(ControlsTextCodes.TABS_PLANNING, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Lokalizatory");
            e.Add(Language.EN, "Localizators");
            ControlsTextDictionary.Add(ControlsTextCodes.TABS_BANDS, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Transmitery");
            e.Add(Language.EN, "Transmitters");
            ControlsTextDictionary.Add(ControlsTextCodes.TABS_TRANSMITTERS, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "OK");
            e.Add(Language.EN, "Save");
            ControlsTextDictionary.Add(ControlsTextCodes.ADDING_BUTTON, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Anuluj");
            e.Add(Language.EN, "Cancel");
            ControlsTextDictionary.Add(ControlsTextCodes.CANCEL_BUTTON, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Nie wybrano oddziału!");
            e.Add(Language.EN, "Non selected department");
            ControlsTextDictionary.Add(ControlsTextCodes.EMPTY_DEPARTMENT_COMBOBOX_INFO, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Nie wybrano sali!");
            e.Add(Language.EN, "Non selected room");
            ControlsTextDictionary.Add(ControlsTextCodes.EMPTY_ROOM_COMBOBOX_INFO, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Nazwa:");
            e.Add(Language.EN, "First Name:");
            ControlsTextDictionary.Add(ControlsTextCodes.FIRSTNAME_LABEL, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Nazwisko:");
            e.Add(Language.EN, "Last Name:");
            ControlsTextDictionary.Add(ControlsTextCodes.LASTNAME_LABEL, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Kod:");
            e.Add(Language.EN, "PESEL:");
            ControlsTextDictionary.Add(ControlsTextCodes.PESEL_LABEL, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Oddział:");
            e.Add(Language.EN, "Department:");
            ControlsTextDictionary.Add(ControlsTextCodes.DEPARTMENT_LABEL, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Pokój:");
            e.Add(Language.EN, "Room:");
            ControlsTextDictionary.Add(ControlsTextCodes.ROOM_LABEL, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Kod kreskowy lokalizatora:");
            e.Add(Language.EN, "Band Barcode:");
            ControlsTextDictionary.Add(ControlsTextCodes.BAND_BARCODE_LABEL, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Dodaj urządzenie");
            e.Add(Language.EN, "Add device");
            ControlsTextDictionary.Add(ControlsTextCodes.REGISTER_PATIENT_LABEL, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Edycja danych urządzenia");
            e.Add(Language.EN, "Patient Edit");
            ControlsTextDictionary.Add(ControlsTextCodes.EDIT_PATIENT_LABEL, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Brak");
            e.Add(Language.EN, "Empty");
            ControlsTextDictionary.Add(ControlsTextCodes.DEFAULT_EMPTY_VALUE, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Przyczyna zmiany:");
            e.Add(Language.EN, "Reason:");
            ControlsTextDictionary.Add(ControlsTextCodes.REASON, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Dodaj urządzenie");
            e.Add(Language.EN, "Add device");
            ControlsTextDictionary.Add(ControlsTextCodes.REGISTRATION_BUTTON, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Zapisz");
            e.Add(Language.EN, "Save");
            ControlsTextDictionary.Add(ControlsTextCodes.SAVE_BUTTON, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Odśwież");
            e.Add(Language.EN, "Refresh");
            ControlsTextDictionary.Add(ControlsTextCodes.REFRESH_BUTTON, e);
        }

        public string Text(ControlsTextCodes controlsTextCodes)
        {
            if (ControlsTextDictionary == null)
                throw new NullReferenceException("ControlsTextDictionary is not initialized. Please first use InitializeControls!");

            if (ControlsTextDictionary[controlsTextCodes] == null)
                throw new NullReferenceException("ErrorCode is not found in ErrorsDictionary.");

            return ControlsTextDictionary[controlsTextCodes].Get();
        }

        public string Text(ControlsTextCodes controlsTextCodes, Language language)
        {
            if (ControlsTextDictionary == null)
                throw new NullReferenceException("ErrorsDictionary is not initialized. Please first use InitializeControls!");

            if (ControlsTextDictionary[controlsTextCodes] == null)
                throw new NullReferenceException("ErrorCode is not found in ErrorsDictionary.");

            return ControlsTextDictionary[controlsTextCodes].Get(language);
        }
    }

    public enum ControlsTextCodes
    {
        TABS_PATIENTS = 1,
        TABS_MONITORABLE = 2,
        TABS_LOCALIZATION = 3,
        TABS_EVENTS = 4,
        TABS_RAPORTS = 5,
        TABS_CONFIGURATION = 6,
        TABS_PLANNING = 7,
        TABS_BANDS,
        TABS_TRANSMITTERS,
        ADDING_BUTTON,
        CANCEL_BUTTON,
        EMPTY_DEPARTMENT_COMBOBOX_INFO,
        EMPTY_ROOM_COMBOBOX_INFO,
        FIRSTNAME_LABEL,
        LASTNAME_LABEL,
        PESEL_LABEL,
        DEPARTMENT_LABEL,
        ROOM_LABEL,
        BAND_BARCODE_LABEL,
        REGISTER_PATIENT_LABEL,
        EDIT_PATIENT_LABEL,
        DEFAULT_EMPTY_VALUE,
        REASON,
        REGISTRATION_BUTTON,
        SAVE_BUTTON,
        REFRESH_BUTTON
    }
}
