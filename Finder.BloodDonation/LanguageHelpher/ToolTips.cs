﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.LanguageHelpher
{
    public class ToolTips
    {
        public Language CurrentLanguage { get; set; }

        public IDictionary<ToolTipCodes, LanguageMessage> ToolTipsDictionary { get; private set; }
        
        public ToolTips(Language applicationLanguage=Language.PL)
        {
            CurrentLanguage = applicationLanguage;
            ToolTipsDictionary = new Dictionary<ToolTipCodes, LanguageMessage>();

            LanguageMessage e = new LanguageMessage();

            e.Add(Language.PL, "Dodaj urządzenie");
            e.Add(Language.EN, "Add device");
            ToolTipsDictionary.Add(ToolTipCodes.REGISTRATION, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Lokalizacja");
            e.Add(Language.EN, "Location");
            ToolTipsDictionary.Add(ToolTipCodes.LOCATION, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Konfiguracja");
            e.Add(Language.EN, "Configuration");
            ToolTipsDictionary.Add(ToolTipCodes.CONFIGURATION, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Szczegóły");
            e.Add(Language.EN, "Details");
            ToolTipsDictionary.Add(ToolTipCodes.DETAILS, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Przypisz lokalizator do urządzenia");
            e.Add(Language.EN, "Przypisz lokalizator do urządzenia");
            ToolTipsDictionary.Add(ToolTipCodes.ADMINISTRATION, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Edycja");
            e.Add(Language.EN, "Edit");
            ToolTipsDictionary.Add(ToolTipCodes.EDIT, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Przyczyna zmiany");
            e.Add(Language.EN, "Reason");
            ToolTipsDictionary.Add(ToolTipCodes.REASON_TOOLTIP, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Wybór oddziału do którego ma zostać przypisane urządzenie");
            e.Add(Language.EN, "Selection of the department to which the patient is to be assigned");
            ToolTipsDictionary.Add(ToolTipCodes.DEPARTMENT_COMBOBOX_TOOLTIP, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Wybór pokoju do którego ma zostać przypisane urządzenie");
            e.Add(Language.EN, "Selection of the room to which the patient is to be assigned");
            ToolTipsDictionary.Add(ToolTipCodes.ROOM_COMBOBOX_TOOLTIP, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Kod kreskowy lokalizatora");
            e.Add(Language.EN, "Localizator Barcode");
            ToolTipsDictionary.Add(ToolTipCodes.BAND_BARCODE_TOOLTIP, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Nazwa");
            e.Add(Language.EN, "Name");
            ToolTipsDictionary.Add(ToolTipCodes.FIRSTNAME_TOOLTIP, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Nazwisko");
            e.Add(Language.EN, "Last Name");
            ToolTipsDictionary.Add(ToolTipCodes.LASTNAME_TOOLTIP, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Kod");
            e.Add(Language.EN, "Kod");
            ToolTipsDictionary.Add(ToolTipCodes.PESEL_TOOLTIP, e);
        }

        public string ToolTip(ToolTipCodes toolTipCode)
        {
            if (ToolTipsDictionary == null)
                throw new NullReferenceException("ErrorsDictionary is not initialized. Please first use InitializeErrors!");

            if (ToolTipsDictionary[toolTipCode] == null)
                throw new NullReferenceException("ErrorCode is not found in ErrorsDictionary.");

            return ToolTipsDictionary[toolTipCode].Get();
        }

        public string ToolTip(ToolTipCodes toolTipCode, Language language)
        {
            if (ToolTipsDictionary == null)
                throw new NullReferenceException("ErrorsDictionary is not initialized. Please first use InitializeErrors!");

            if (ToolTipsDictionary[toolTipCode] == null)
                throw new NullReferenceException("ErrorCode is not found in ErrorsDictionary.");

            return ToolTipsDictionary[toolTipCode].Get(language);
        }
    }

    public enum ToolTipCodes
    {
        REGISTRATION = 1,
        CONFIGURATION = 2,
        DETAILS = 3,
        ADMINISTRATION,
        LOCATION,
        EDIT,
        REASON_TOOLTIP,
        DEPARTMENT_COMBOBOX_TOOLTIP,
        ROOM_COMBOBOX_TOOLTIP,
        BAND_BARCODE_TOOLTIP,
        FIRSTNAME_TOOLTIP,
        LASTNAME_TOOLTIP,
        PESEL_TOOLTIP
    }
}
