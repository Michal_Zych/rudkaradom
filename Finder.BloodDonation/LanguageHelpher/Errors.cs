﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.LanguageHelpher
{
    public class Errors
    {
        public Language CurrentLanguage { get; set; }

        public IDictionary<ErrorCodes, LanguageMessage> ErrorsDictionary { get; private set; }

        public Errors(Language applicationLanguage=Language.PL)
        {
            CurrentLanguage = applicationLanguage;
            ErrorsDictionary = new Dictionary<ErrorCodes, LanguageMessage>();

            LanguageMessage e = new LanguageMessage();

            e.Add(Language.PL, "Nie wybrano numeru telefonu");
            e.Add(Language.EN, "Non selection phone number");
            ErrorsDictionary.Add(ErrorCodes.NON_SELLECTION_PHONE_NUMBER, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Brak numerów telefonów.");
            e.Add(Language.EN, "No phone numbers.");
            ErrorsDictionary.Add(ErrorCodes.PHONE_NUMBER_LIST_IS_EMPTY, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Nie wybrano urządzenia.");
            e.Add(Language.EN, "Non selection device!");
            ErrorsDictionary.Add(ErrorCodes.NON_SELLECTION_PATIENT, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Niepoprawny format numeru PESEL!");
            e.Add(Language.EN, "Bad pesel format!");
            ErrorsDictionary.Add(ErrorCodes.BAD_PESEL_FORMAT, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Nazwisko pacjeta nie może być puste!");
            e.Add(Language.EN, "The patient's last name can not be empty");
            ErrorsDictionary.Add(ErrorCodes.BAD_LASTNAME_FORMAT, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Nazwa urządzenia nie może być pusta!");
            e.Add(Language.EN, "The patient's first name can not be empty");
            ErrorsDictionary.Add(ErrorCodes.BAD_FIRSTNAME_FORMAT, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Nie wybrano oddziału do którego urządzenie ma być przypisane!");
            e.Add(Language.EN, "Non selected assignment department");
            ErrorsDictionary.Add(ErrorCodes.NON_SELLECTION_DEPARTMENT, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Nie wprowadzono kodu kreskowego lokalizatora!");
            e.Add(Language.EN, "The bar code of the armband has not been entered");
            ErrorsDictionary.Add(ErrorCodes.NON_ENTERED_BAND_BARCODE, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Lokalizator o podanym kodzie nie istnieje!");
            e.Add(Language.EN, "No localizer with given code!");
            ErrorsDictionary.Add(ErrorCodes.WRONG_BARCODE, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Coś poszło nie tak!");
            e.Add(Language.EN, "Something went wrong");
            ErrorsDictionary.Add(ErrorCodes.OTHER_ERROR, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Kod kreskowy lokalizatora nie może być pusty!");
            e.Add(Language.EN, "The bar code of the armband can not be empty!");
            ErrorsDictionary.Add(ErrorCodes.EMPTY_BAND_BARCODE, e);

            e = new LanguageMessage();
            e.Add(Language.PL, "Nie wybrano żadnego rekordu!");
            e.Add(Language.EN, "Non selected record!");
            ErrorsDictionary.Add(ErrorCodes.NON_SELLECTION_RECORD, e);
        }

        public string Error(ErrorCodes errorCode)
        {
            if (ErrorsDictionary == null)
                throw new NullReferenceException("ErrorsDictionary is not initialized. Please first use InitializeErrors!");

            if (ErrorsDictionary[errorCode] == null)
                throw new NullReferenceException("ErrorCode is not found in ErrorsDictionary.");

            return ErrorsDictionary[errorCode].Get();
        }

        public string Error(ErrorCodes errorCode, Language language)
        {
            if (ErrorsDictionary == null)
                throw new NullReferenceException("ErrorsDictionary is not initialized. Please first use InitializeErrors!");

            if (ErrorsDictionary[errorCode] == null)
                throw new NullReferenceException("ErrorCode is not found in ErrorsDictionary.");

            return ErrorsDictionary[errorCode].Get(language);
        }
    }

    public enum ErrorCodes
    {
        NON_SELLECTION_PHONE_NUMBER = 1,
        PHONE_NUMBER_LIST_IS_EMPTY = 2,
        NON_SELLECTION_PATIENT,
        NON_SELLECTION_DEPARTMENT,
        NON_ENTERED_BAND_BARCODE,
        WRONG_BARCODE,
        NON_SELLECTION_ALARM,
        BAD_PESEL_FORMAT,
        BAD_LASTNAME_FORMAT,
        BAD_FIRSTNAME_FORMAT,
        OTHER_ERROR,
        EMPTY_BAND_BARCODE,
        NON_SELLECTION_RECORD
    }
}
