﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.LanguageHelpher
{
    public static class LanguageManager
    {
        public static Language CurrentLanguage { get; set; }
        public static Errors Errors { get; set; }
        public static ToolTips ToolTips { get; set; }
        public static ControlsText ControlsText { get; set; }
        public static Messages Messages { get; set; }

        public static void LoadLanguage(Language language)
        {
            CurrentLanguage = language;
            Errors = new Errors(CurrentLanguage);
            ControlsText = new ControlsText(CurrentLanguage);
            ToolTips = new ToolTips(CurrentLanguage);
            Messages = new Messages(CurrentLanguage);
        }

        public static string GetError(ErrorCodes errorCode)
        {
            return Errors.Error(errorCode);
        }

        public static string GetError(ErrorCodes errorCode, Language language)
        {
            return Errors.Error(errorCode, language);
        }

        public static string GetControlsText(ControlsTextCodes controlsTextCodes)
        {
            return ControlsText.Text(controlsTextCodes);
        }

        public static string GetControlsText(ControlsTextCodes controlsTextCodes, Language language)
        {
            return ControlsText.Text(controlsTextCodes, language);
        }

        public static string GetToolTip(ToolTipCodes toolTipCodes)
        {
            return ToolTips.ToolTip(toolTipCodes);
        }

        public static string GetToolTip(ToolTipCodes toolTipCodes, Language language)
        {
            return ToolTips.ToolTip(toolTipCodes, language);
        }
    }
}
