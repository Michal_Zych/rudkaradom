﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.TreeMenu
{
    public struct TreeMenuDescription
    {
        public int Id;
        public int ParentId;
        public string Name;
    }
}
