﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Model.Enum;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.TreeMenu
{
    public class TreeMenuItem : IUnit
    {
        public ObservableCollection<TreeMenuItem> SubItems { get; set; }

        public System.Collections.Generic.IList<ITab> AvailableTabs
        {
            get
            {
                return UnitTypeHelper.GetAllTabsForTreeMenuItem(UnitDescription);
            }
        }

        public int Identity {get;set;}
    
        public UnitType UnitType
        {
            get { return (Model.Enum.UnitType)Identity; }
        }

        public int SelectionSource {get;set;}

        public string UnitDescription {get; set;}

        public bool IsMonitored {get;set;}
    }
}
