﻿using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Model.Authentication;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.TreeMenu
{
    public static class MenuTree
    {
       /* private static TreeMenuDescription[] toolDefs = new TreeMenuDescription[] 
        {
              new TreeMenuDescription() {Id = 1, ParentId = 0, Name = TabNames.ToolTransmitters}
            , new TreeMenuDescription() {Id = 2, ParentId = 0, Name = TabNames.ToolBands}
            , new TreeMenuDescription() {Id = 3, ParentId = 0, Name = TabNames.ExternalSystems}
            , new TreeMenuDescription() {Id = 4, ParentId = 3, Name = TabNames.Asseco}
            , new TreeMenuDescription() {Id = 5, ParentId = 4, Name = TabNames.AssecoOut}
            , new TreeMenuDescription() {Id = 6, ParentId = 4, Name = TabNames.AssecoIn}
            , new TreeMenuDescription() {Id = 7, ParentId = 4, Name = TabNames.AssecoMapping}
            //, new TreeMenuDescription() {Id = 8, ParentId = 0, Name = TabNames.Backup}
        };

        private static TreeMenuDescription[] administrationDefs = new TreeMenuDescription[] 
        {
              new TreeMenuDescription() {Id = 1, ParentId = 0, Name = TabNames.Permissions}
            , new TreeMenuDescription() {Id = 2, ParentId = 0, Name = TabNames.Messages}
            , new TreeMenuDescription() {Id = 3, ParentId = 0, Name = TabNames.Settings}
            , new TreeMenuDescription() {Id = 4, ParentId = 0, Name = TabNames.LoggedUsers}
            , new TreeMenuDescription() {Id = 5, ParentId = 0, Name = TabNames.Logs}
            , new TreeMenuDescription() {Id = 6, ParentId = 0, Name = TabNames.ServiceStatus}
            , new TreeMenuDescription() {Id = 7, ParentId = 0, Name = TabNames.Versions}
        };

        private static TreeMenuDescription[] archiveDefs = new TreeMenuDescription[] 
        {
              new TreeMenuDescription() {Id = 1, ParentId = 0, Name = TabNames.ArchivePatients}
        };*/

        public static ObservableCollection<TreeMenuItem> GetToolsMenu()
        {
            List<TreeMenuDescription> toolDefs = new List<TreeMenuDescription>();

            if (BloodyUser.Current.IsInRole(PermissionNames.ToolsTransmitters))
                toolDefs.Add(new TreeMenuDescription() { Id = 1, ParentId = 0, Name = TabNames.ToolTransmitters });
            if (BloodyUser.Current.IsInRole(PermissionNames.ToolsBands))
                toolDefs.Add(new TreeMenuDescription() { Id = 2, ParentId = 0, Name = TabNames.ToolBands });
            if (BloodyUser.Current.IsInRole(PermissionNames.SuperUser))
                toolDefs.Add(new TreeMenuDescription() { Id = 3, ParentId = 0, Name = TabNames.ExternalSystems });
            if (BloodyUser.Current.IsInRole(PermissionNames.SuperUser))
                toolDefs.Add(new TreeMenuDescription() { Id = 4, ParentId = 3, Name = TabNames.Asseco });
            if (BloodyUser.Current.IsInRole(PermissionNames.SuperUser))
                toolDefs.Add(new TreeMenuDescription() { Id = 5, ParentId = 4, Name = TabNames.AssecoOut });
            if (BloodyUser.Current.IsInRole(PermissionNames.SuperUser))
                toolDefs.Add(new TreeMenuDescription() { Id = 6, ParentId = 4, Name = TabNames.AssecoIn });
            if (BloodyUser.Current.IsInRole(PermissionNames.SuperUser))
                toolDefs.Add(new TreeMenuDescription() { Id = 6, ParentId = 4, Name = TabNames.AssecoMapping });

            return GetItems(toolDefs);
        }

        public static ObservableCollection<TreeMenuItem> GetAdministrationMenu()
        {
            List<TreeMenuDescription> administrationDefs = new List<TreeMenuDescription>();

            if (BloodyUser.Current.IsInRole(PermissionNames.AdministrationPersonell))
                administrationDefs.Add(new TreeMenuDescription() { Id = 1, ParentId = 0, Name = TabNames.Permissions });
            //if (BloodyUser.Current.IsInRole(PermissionNames.AdministrationMessages))
            //    administrationDefs.Add(new TreeMenuDescription() { Id = 2, ParentId = 0, Name = TabNames.Messages });
            if (BloodyUser.Current.IsInRole(PermissionNames.AdministrationSystemSettings))
                administrationDefs.Add(new TreeMenuDescription() { Id = 3, ParentId = 0, Name = TabNames.Settings });
            if (BloodyUser.Current.IsInRole(PermissionNames.AdministrationLoggedUsers))
                administrationDefs.Add(new TreeMenuDescription() { Id = 4, ParentId = 0, Name = TabNames.LoggedUsers });
            if (BloodyUser.Current.IsInRole(PermissionNames.AdministrationSystemEvents))
                administrationDefs.Add(new TreeMenuDescription() { Id = 5, ParentId = 0, Name = TabNames.Logs });
            //if (BloodyUser.Current.IsInRole(PermissionNames.AdministrationServiceStatuses))
            //    administrationDefs.Add(new TreeMenuDescription() { Id = 6, ParentId = 0, Name = TabNames.ServiceStatus });
            //if (BloodyUser.Current.IsInRole(PermissionNames.AdministrationVersions))
            //    administrationDefs.Add(new TreeMenuDescription() { Id = 7, ParentId = 0, Name = TabNames.Versions });

            return GetItems(administrationDefs);
        }

        public static ObservableCollection<TreeMenuItem> GetArchiveMenu()
        {
            List<TreeMenuDescription> archiveDefs = new List<TreeMenuDescription>();

            if (BloodyUser.Current.IsInRole(PermissionNames.Archive))
                archiveDefs.Add(new TreeMenuDescription() { Id = 1, ParentId = 0, Name = TabNames.ArchivePatients });

            return GetItems(archiveDefs);
        }

        public static ObservableCollection<TreeMenuItem> GetItems(IList<TreeMenuDescription> items)
        {
            var menu = new ObservableCollection<TreeMenuItem>();

            foreach (var item in items)
            {
                var menuItem = new TreeMenuItem()
                {
                    Identity = item.Id,
                    UnitDescription = item.Name,
                    SubItems = new ObservableCollection<TreeMenuItem>()
                };


                var parent = FindItemByIdentity(menu, item.ParentId);
                if (parent == null)
                {
                    menu.Add(menuItem);
                }
                else
                {
                    parent.SubItems.Add(menuItem);
                }
            }

            return menu;
        }

        private static TreeMenuItem FindItemByIdentity(ObservableCollection<TreeMenuItem> items, int id)
        {
            foreach (var item in items)
            {
                if (item.Identity == id)
                {
                    return item;
                }
                else
                {
                    var child = FindItemByIdentity(item.SubItems, id);
                    if (child != null) return child;
                }
            }
            return null;
        }
    }
}
