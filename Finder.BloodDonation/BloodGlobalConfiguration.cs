﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.Configuration;
using Finder.BloodDonation.Common.Authentication;
using Microsoft.Practices.Unity;

namespace Finder.BloodDonation
{
	public class BloodGlobalConfiguration : GlobalConfiguration
	{
		public BloodGlobalConfiguration(IUnityContainer container)
			: base(container)
		{

		}

		public override FinderFX.SL.Core.Authentication.IAuthenticationManager GetAuthenticationManager()
		{
			return new BloodAuthenticationManager();
		}
	}
}
