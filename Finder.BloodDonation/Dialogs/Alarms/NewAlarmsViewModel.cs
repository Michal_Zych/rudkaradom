﻿using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Controls;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.DynamicData.DBs;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Model.Dto.Enums;
using Finder.BloodDonation.Tabs.AlarmDetails;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Dialogs.Alarms
{
    public class NewAlarmsViewModel : ViewModelBase, IDialogWindow
    {
        [RaisePropertyChanged]
        public List<Alarm> AlarmsList { get; set; }

        public RelayCommand OkClick { get; set; }
        public RelayCommand CancelClick { get; set; }

        private ITabViewModel _alarms;
        [RaisePropertyChanged]
        public ITabViewModel Alarms
        {
            get
            {
                if (_alarms == null)
                {
                    //patientInfoMessage = new PatientInfoMessage() { Id = PatientData.PatientId.ToString() };

                    _alarms = Container.Resolve<AlarmDetailsViewModel>();

                   //Messenger.Default.Send<PatientInfoMessage>(patientInfoMessage);
                }
                return _alarms;
            }
            set
            {
                _alarms = value;
            }
        }
        

        public string WindowKey
        {
            get { return WindowsKeys.NewAlarmsView; }
        }

        public NewAlarmsViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            OkClick = new RelayCommand(OnOkClick);
            CancelClick = new RelayCommand(OnCancelClick);

            EventAggregator.GetEvent<BandStatusesRefreshedEvent>().Subscribe(BandStatusesRefreshed);
        }

        private IList<BandStatusDto> BandStatusDto { get; set; }

        public void BandStatusesRefreshed(object obj)
        {
            BandStatusDto = DbBandStatus.GetItems();

            if (BandStatusDto == null)
                return;
           
            List<Alarm> monitorableObjects = new List<Alarm>();

            for (int i = 0; i < BandStatusDto.Count; i++)
            {
                if (BandStatusDto[i] == null)
                    continue;

                if (BandStatusDto[i].TopAlarmSeverity == (byte)EventTypes.Event)
                    continue;

                monitorableObjects.Add(CreateAlarmFromBandStatuses(BandStatusDto[i]));
            }

            AlarmsList = monitorableObjects;
        }

        private Alarm CreateAlarmFromBandStatuses(BandStatusDto bandStatus)
        {
            return new Alarm()
            {
                Id = bandStatus.BandId,
                DisplayColor = bandStatus.TopAlarmSeverity == (byte)EventTypes.Alarm 
                               ? DBBuffers.BitmapMode.Red 
                               : DBBuffers.BitmapMode.Yellow,
                ObjectId = bandStatus.ObjectId,
                ObjectType = (ObjectType)bandStatus.ObjectType,
                EventTypes = (EventTypes)bandStatus.TopAlarmSeverity,
                Type = (Finder.BloodDonation.Tabs.UnitsList.AlarmType)bandStatus.TopAlarmType,
                Info = bandStatus.ObjectDisplayName,
                StartTime = bandStatus != null ? (DateTime)bandStatus.TopAlarmDate : new DateTime()
            };
        }

        private void OnCancelClick()
        {
            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        private void OnOkClick()
        {
            throw new NotImplementedException();
        }

        public object Data
        {
            set
            {
                
            }
        }
    }
}
