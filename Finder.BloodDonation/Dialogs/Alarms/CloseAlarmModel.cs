﻿using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Dialogs.Alarms
{
    [OnCompleted]
    [UIException]
    public class CloseAlarmModel : ViewModelBase, IDialogWindow
    {
        private CloseAlarmData _data;
        public UnitsServiceClient Proxy { get; set; } 

        [RaisePropertyChanged]
        public string Reason { get; set; }

        [RaisePropertyChanged]
        public string CloseAlarmInfo { get; set; }

        [RaisePropertyChanged]
        public string Title { get; set; }

        public RelayCommand SaveActionCommand { get; set; }
        public RelayCommand CancelActionCommand { get; set; }

        public CloseAlarmModel(IUnityContainer container)
            : base(container)
        {
            SaveActionCommand = new RelayCommand(OnSaveActionCommand);
            CancelActionCommand = new RelayCommand(OnCancelActionComman);

            Proxy = ServiceFactory.GetService<UnitsServiceClient>();
            Proxy.CloseAlarmsCompleted += Proxy_CloseAlarmsCompleted;

            Title = "Zamknij alarm";
        }

        private void OnCancelActionComman()
        {
            Close(CloseButton.Cancel);
        }

        private void Proxy_CloseAlarmsCompleted(object sender, CloseAlarmsCompletedEventArgs e)
        {
            if (e.Result.Equals(String.Empty))
            {
                MsgBox.Warning("Alarm zamknięty pomyślnie!");
                EventAggregator.GetEvent<RefreshCloseAlarmsListeners>().Publish(null);
                Close(CloseButton.Cancel);
            }
            else
            {
                MsgBox.Warning(e.Result);
            }
        }

        public void OnSaveActionCommand()
        {
            Close(CloseButton.OK);
        }

        public string WindowKey
        {
            get { return WindowsKeys.CloseAlarm; }
        }

        public object Data
        {
            set 
            {
                _data = (CloseAlarmData)value;

                if(_data != null)
                {
                    CloseAlarmInfo = "Czy na pewno chcesz zamknąć alarm?";
                    //CloseAlarmInfo = _data.AlarmInfo.
                }
            }
        }

        private void Close(CloseButton closedType)
        {
            switch (closedType)
            {
                case CloseButton.OK:
                    SaveData();
                    break;

                case CloseButton.Cancel:
                    Container.Resolve<IWindowsManager>().CloseWindow(this.WindowKey);
                    break;
            }
        }

        private void SaveData()
        {
            if(_data == null)
                return;

            if(_data.AlarmInfo == null)
                return;

            _data.AlarmInfo.Message = Reason;

            Proxy.CloseAlarmsAsync(_data.AlarmInfo);
        }
    }
}
