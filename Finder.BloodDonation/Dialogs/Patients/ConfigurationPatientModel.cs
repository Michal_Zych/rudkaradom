﻿using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Bands;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Tools;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.UnitsService;
using GalaSoft.MvvmLight.Messaging;
using Finder.BloodDonation.Filters;

namespace Finder.BloodDonation.Dialogs.Patients
{
    [OnCompleted]
    [UIException]
    public class ConfigurationPatientModel: ViewModelBase, IDialogWindow, ITabViewModel, IUnitFiltered
    {
        private PatientConfigurationData _data;
        private const int MINIMUM_VALUE_OFFSET = 1;

        public int Id = -1;

        private int _sendWarningsAfter_MoveDay;
        [RaisePropertyChanged]
        public int SendWarningsAfter_MoveDay
        {
            get
            {
                return _sendWarningsAfter_MoveDay;
            }
            set
            {
                if (SendAlarmAfter_MoveDay <= value && IsSendAlarm_MoveDay)
                {
                    SendAlarmAfter_MoveDay = value + MINIMUM_VALUE_OFFSET;
                }

                _sendWarningsAfter_MoveDay = value;
            }
        }

        [RaisePropertyChanged]
        public int SendAlarmAfter_MoveDay { get; set; }

        [RaisePropertyChanged]
        public int SendWarningsAfter_NotMoveDay
        {
            get
            {
                return _sendWarningsAfter_NotMoveDay;
            }
            set
            {
                if (SendAlarmAfter_NotMoveDay <= value && IsSendAlarm_NotMoveDay)
                {
                    SendAlarmAfter_NotMoveDay = value + MINIMUM_VALUE_OFFSET;
                }

                _sendWarningsAfter_NotMoveDay = value;
            }
        }
        [RaisePropertyChanged]
        public int SendAlarmAfter_NotMoveDay { get; set; }

        private int _sendWarningsAfter_NotMoveDay;


        private int _sendWarningsAfter_MoveNight;
        [RaisePropertyChanged]
        public int SendWarningsAfter_MoveNight
        {
            get
            {
                return _sendWarningsAfter_MoveNight;
            }
            set
            {
                if (SendAlarmAfter_MoveNight <= value && IsSendAlarm_MoveNight)
                {
                    SendAlarmAfter_MoveNight = value + MINIMUM_VALUE_OFFSET;
                }

                _sendWarningsAfter_MoveNight = value;
            }
        }

        [RaisePropertyChanged]
        public int SendAlarmAfter_MoveNight { get; set; }

        private int _sendWarningsAfter_NotMoveNight;
        [RaisePropertyChanged]
        public int SendWarningsAfter_NotMoveNight
        {
            get
            {
                return _sendWarningsAfter_NotMoveNight;
            }
            set
            {
                if (SendAlarmAfter_NotMoveNight <= value && IsSendAlarm_NotMoveNight)
                {
                    SendAlarmAfter_NotMoveNight = value + MINIMUM_VALUE_OFFSET;
                }

                _sendWarningsAfter_NotMoveNight = value;
            }
        }

        private int CheckAlarmNumericUpDownAndGetSetterValue(int value, ref int fieldToCheck, bool isFieldToCheckActive)
        {
            if (fieldToCheck <= value && isFieldToCheckActive)
            {
                fieldToCheck = value + MINIMUM_VALUE_OFFSET;
            }

            return value;
        }

        [RaisePropertyChanged]
        public int SendAlarmAfter_NotMoveNight { get; set; }

        private int _sendWarning_PatientOutRoomValue;
        [RaisePropertyChanged]
        public int SendWarning_PatientOutRoomValue
        {
            get
            {
                return _sendWarning_PatientOutRoomValue;
            }
            set
            {
                if (SendAlarm_PatientOutRoomValue <= value && IsSendAllarm_PatientOutRoom)
                {
                    SendAlarm_PatientOutRoomValue = value + MINIMUM_VALUE_OFFSET;
                }

                _sendWarning_PatientOutRoomValue = value;
            }
        }
        [RaisePropertyChanged]
        public int SendAlarm_PatientOutRoomValue { get; set; }
        [RaisePropertyChanged]
        public int SendAlarm_PatientInProhibitedZone { get; set; }

        private int _connectionLost_SendWarning;
        [RaisePropertyChanged]
        public int ConnectionLost_SendWarning
        {
            get
            {
                return _connectionLost_SendWarning;
            }
            set
            {
                if (ConnectionLost_SendAlarm <= value && IsConnectionLost_SendAlarm)
                {
                    ConnectionLost_SendAlarm = value + MINIMUM_VALUE_OFFSET;
                }

                _connectionLost_SendWarning = value;
            }
        }

        [RaisePropertyChanged]
        public string Reason { get; set; }

        public RelayCommand AddNumber { get; set; }
        public RelayCommand EditNumber { get; set; }
        public RelayCommand DeleteNumber { get; set; }

        public RelayCommand UpNumber { get; set; }
        public RelayCommand DownNumber { get; set; } 

        public RelayCommand AddProhibitedZone { get; set; }
        public RelayCommand EditProhibitedZone { get; set; }
        public RelayCommand DeleteProhibitedZone { get; set; }

        public RelayCommand SaveActionCommand { get; set; }
        public RelayCommand CancelActionCommand { get; set; }

        [RaisePropertyChanged]
        public int SelectedPhoneNumberIndex { get; set; }
        [RaisePropertyChanged]
        public int SelectedProhibitedZonesIndex { get; set; }

        [RaisePropertyChanged]
        public bool IsAlarmSOS { get; set; }
        [RaisePropertyChanged]
        public bool IsAlarmFromAccelerometer { get; set; }
        [RaisePropertyChanged]
        public bool IsSendWarnings_MoveDay { get; set; }
        [RaisePropertyChanged]
        public bool IsSendAlarm_MoveDay { get; set; }
        [RaisePropertyChanged]
        public bool IsSendWarnings_NotMoveDay { get; set; }
        [RaisePropertyChanged]
        public bool IsSendAlarm_NotMoveDay { get; set; }
        [RaisePropertyChanged]
        public bool IsSendWarnings_MoveNight { get; set; } 
        [RaisePropertyChanged]
        public bool IsSendAlarm_MoveNight { get; set; }
        [RaisePropertyChanged]
        public bool IsSendWarnings_NotMoveNight { get; set; }
        [RaisePropertyChanged]
        public bool IsSendAlarm_NotMoveNight { get; set; }
        [RaisePropertyChanged]
        public bool IsSendWarning_PatientOutRoom { get; set; }
        [RaisePropertyChanged]
        public bool IsSendAllarm_PatientOutRoom { get; set; }
        [RaisePropertyChanged]
        public bool IsConnectionLost_SendWarning { get; set; }
        [RaisePropertyChanged]
        public bool IsConnectionLost_SendAlarm { get; set; }


        //[RaisePropertyChanged]
        //public int SendWarningsAfter_MoveDay { get; set; }
        //[RaisePropertyChanged]
        //public int SendAlarmAfter_MoveDay { get; set; }
        //[RaisePropertyChanged]
        //public int SendWarningsAfter_NotMoveDay { get; set; }
        //[RaisePropertyChanged]
        //public int SendAlarmAfter_NotMoveDay { get; set; }
        //[RaisePropertyChanged]
        //public int SendWarningsAfter_MoveNight { get; set; }
        //[RaisePropertyChanged]
        //public int SendAlarmAfter_MoveNight { get; set; }
        //[RaisePropertyChanged]
        //public int SendWarningsAfter_NotMoveNight { get; set; }
        //[RaisePropertyChanged]
        //public int SendAlarmAfter_NotMoveNight { get; set; }
        //[RaisePropertyChanged]
        //public int SendWarning_PatientOutRoomValue { get; set; }
        //[RaisePropertyChanged]
        //public int SendAlarm_PatientOutRoomValue { get; set; }
        //[RaisePropertyChanged]
        //public int SendAlarm_PatientInProhibitedZone { get; set; }
        //[RaisePropertyChanged]
        //public int ConnectionLost_SendWarning { get; set; }
        [RaisePropertyChanged]
        public int ConnectionLost_SendAlarm { get; set; }


        [RaisePropertyChanged]
        public ObservableCollection<Contact> CallNumbersList { get; set; }
        [RaisePropertyChanged]
        public ObservableCollection<UnitDTO> ProhibitedZonesList { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<GridViewUnitModel> ProhibitedZonesListToView { get; set; }

        private IWindowsManager _windowsManager;
        private PhoneNumberData _phoneData;
        private ProhibitedZoneData _prohibitedZoneData;

        private UnitsServiceClient Proxy { get; set; }



        public string WindowKey
        {
            get { return WindowsKeys.ConfigurationPatient; }
        }

        public object Data
        {
            set 
            {
                _data = (PatientConfigurationData)value;

                //SetDataToField();
            }
        }

        public ConfigurationPatientModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            AddNumber = new RelayCommand(OnAddNumber);
            EditNumber = new RelayCommand(OnEditNumber);
            DeleteNumber = new RelayCommand(OnDeleteNumbe);
            UpNumber = new RelayCommand(OnUpNumber);
            DownNumber = new RelayCommand(OnDownNumber);

            AddProhibitedZone = new RelayCommand(OnAddProhibitedZone);
            EditProhibitedZone = new RelayCommand(OnEditProhibitedZone);
            DeleteProhibitedZone = new RelayCommand(OnDeleteProhibitedZone);

            SaveActionCommand = new RelayCommand(OnSaveActionCommand);
            CancelActionCommand = new RelayCommand(OnCancelActionCommand);

            CallNumbersList = new ObservableCollection<Contact>();
            ProhibitedZonesList = new ObservableCollection<UnitDTO>();

            var viewFactory = unityContainer.Resolve<ViewFactory>();
            _windowsManager = this.Container.Resolve<IWindowsManager>();
            _windowsManager.RegisterWindow(WindowsKeys.EditNumber, "Edytowanie", viewFactory.CreateView<EditNumber>(), new System.Windows.Size(400, 230), true);
            _windowsManager.RegisterWindow(WindowsKeys.EditProhibitedZoneModel, "Edytowanie", viewFactory.CreateView<EditProhibitedZone>(), new System.Windows.Size(400, 450), true);
            _windowsManager.RegisterWindow(WindowsKeys.SuccessDialog, "Powodzenie", viewFactory.CreateView<Finder.BloodDonation.Dialogs.Success.Success>(), new System.Windows.Size(440, 170), false);

            Messenger.Default.Register<PatientConfigurationData>(this, OnPatientConfigurationDataChanged);
            EventAggregator.GetEvent<OnSaveAllPatientDetailsTabs>().Subscribe(OnClosedHandler);

            Proxy = ServiceFactory.GetService<UnitsServiceClient>();
            Proxy.SetPatientConfigurationCompleted += Proxy_SetPatientConfigurationCompleted;
            Proxy.GetPatientConfigurationCompleted += Proxy_GetPatientConfigurationCompleted;
            Proxy.GetLocalizationConfigForUnitCompleted += Proxy_GetLocalizationConfigForUnitCompleted;

            EventAggregator.GetEvent<PasteFromClipboardEvent>().Subscribe(OnPasteFromClipboard);
            EventAggregator.GetEvent<CopyToClipboardEvent>().Subscribe(OnCopyToClipboard);
            EventAggregator.GetEvent<PasteDefaultDataEvent>().Subscribe(OnPasteDefaultData);

            EventAggregator.GetEvent<SendAfterRegisterResolveEvent>().Publish(null);
        }

        public void Proxy_GetLocalizationConfigForUnitCompleted(object sender, GetLocalizationConfigForUnitCompletedEventArgs e)
        {
            if(e.Result != null)
            {
                ConnectionLost_SendAlarm = e.Result.ConnectionLost_SendAlarm;
                ConnectionLost_SendWarning = e.Result.ConnectionLost_SendWarning;
                IsAlarmFromAccelerometer = e.Result.IsAlarmFromAccelerometer;
                IsAlarmSOS = e.Result.IsAlarmSOS;
                IsConnectionLost_SendAlarm = e.Result.IsConnectionLost_SendAlarm;
                IsConnectionLost_SendWarning = e.Result.IsConnectionLost_SendWarning;
                IsSendWarnings_MoveDay = e.Result.IsSendWarnings_MoveDay;
                IsSendWarnings_MoveNight = e.Result.IsSendWarnings_MoveNight;
                IsSendWarnings_NotMoveDay = e.Result.IsSendWarnings_NotMoveDay;
                IsSendWarnings_NotMoveNight = e.Result.IsSendWarnings_NotMoveNight;
                IsSendAlarm_MoveDay = e.Result.IsSendAlarm_MoveDay;
                IsSendAlarm_MoveNight = e.Result.IsSendAlarm_MoveNight;
                IsSendAlarm_NotMoveDay = e.Result.IsSendAlarm_NotMoveDay;
                IsSendAlarm_NotMoveNight = e.Result.IsSendAlarm_NotMoveNight;
                IsSendAllarm_PatientOutRoom = e.Result.IsSendAllarm_PatientOutRoom;
                IsSendWarning_PatientOutRoom = e.Result.IsSendWarning_PatientOutRoom;
                
                CallNumbersList = new ObservableCollection<Contact>(e.Result.PhoneNumbers);
                SendAlarm_PatientInProhibitedZone = e.Result.SendAlarm_PatientInProhibitedZone;
                SendAlarm_PatientOutRoomValue = e.Result.SendAlarm_PatientOutRoomValue;
                SendAlarmAfter_MoveDay = e.Result.SendAlarmAfter_MoveDay;
                SendAlarmAfter_MoveNight = e.Result.SendAlarmAfter_MoveNight;
                SendAlarmAfter_NotMoveDay = e.Result.SendAlarmAfter_NotMoveDay;
                SendAlarmAfter_NotMoveNight = e.Result.SendAlarmAfter_NotMoveNight;
                SendWarning_PatientOutRoomValue = e.Result.SendWarning_PatientOutRoomValue;
                SendWarningsAfter_MoveDay = e.Result.SendWarningsAfter_MoveDay;
                SendWarningsAfter_MoveNight = e.Result.SendWarningsAfter_MoveNight;
                SendWarningsAfter_NotMoveDay = e.Result.SendWarningsAfter_NotMoveDay;
                SendWarningsAfter_NotMoveNight = e.Result.SendWarningsAfter_NotMoveNight;

                ProhibitedZonesList = new ObservableCollection<UnitDTO>(e.Result.ProhibitedZonesList);

                List<GridViewUnitModel> _dynamicList = new List<GridViewUnitModel>();

                for (int i = 0; i < ProhibitedZonesList.Count; i++)
                {
                    UnitDTO _temp = UnitManager.Instance.Units.Where(f => f.ID == ProhibitedZonesList[i].ID).FirstOrDefault();

                    ProhibitedZonesList[i] = _temp;

                    _dynamicList.Add(new GridViewUnitModel()
                    {
                        DepartmentName = UnitManager.Instance.SearchDepartment(_temp) != null ? UnitManager.Instance.SearchDepartment(_temp).Name : String.Empty,
                        RoomName = _temp != null ? _temp.Name : String.Empty
                    });
                }

                ProhibitedZonesListToView = new ObservableCollection<GridViewUnitModel>(_dynamicList);
            }
        }

        public void OnPasteDefaultData(object obj)
        {
            OnPasteDefaultData();
        }

        public void OnCopyToClipboard(object obj)
        {
            OnCopyToClipboard();
        }

        public void OnPasteFromClipboard(object obj)
        {
            OnPasteFromClipboard();
        }

        public void OnClosedHandler(object obj)
        {
            SaveData();
        }

        void Proxy_SetPatientConfigurationCompleted(object sender, SetPatientConfigurationCompletedEventArgs e)
        {
            _windowsManager.ShowWindow(WindowsKeys.SuccessDialog, "Konfiguracja zakończona sukcesem!");
        }

        private void OnPatientConfigurationDataChanged(PatientConfigurationData obj)
        {
            if (obj != null)
            {
                _data = obj;
                SetDataToField();
            }
        }

        private void SendDataToDataBase()
        {

        }

        void Proxy_GetPatientConfigurationCompleted(object sender, GetPatientConfigurationCompletedEventArgs e)
        {
            if(e.Result != null)
            {
                ConnectionLost_SendAlarm = e.Result.ConnectionLost_SendAlarm;
                ConnectionLost_SendWarning = e.Result.ConnectionLost_SendWarning;
                IsAlarmFromAccelerometer = e.Result.IsAlarmFromAccelerometer;
                IsAlarmSOS = e.Result.IsAlarmSOS;
                IsConnectionLost_SendAlarm = e.Result.IsConnectionLost_SendAlarm;
                IsConnectionLost_SendWarning = e.Result.IsConnectionLost_SendWarning;
                IsSendAlarm_MoveDay = e.Result.IsSendAlarm_MoveDay;
                IsSendAlarm_MoveNight = e.Result.IsSendAlarm_MoveNight;
                IsSendAlarm_NotMoveDay = e.Result.IsSendAlarm_NotMoveDay;
                IsSendAlarm_NotMoveNight = e.Result.IsSendAlarm_NotMoveNight;
                IsSendAllarm_PatientOutRoom = e.Result.IsSendAllarm_PatientOutRoom;
                IsSendWarning_PatientOutRoom = e.Result.IsSendWarning_PatientOutRoom;
                IsSendWarnings_MoveDay = e.Result.IsSendWarnings_MoveDay;
                IsSendWarnings_MoveNight = e.Result.IsSendWarnings_MoveNight;
                IsSendWarnings_NotMoveDay = e.Result.IsSendWarnings_NotMoveDay;
                IsSendWarnings_NotMoveNight = e.Result.IsSendWarnings_NotMoveNight;
                CallNumbersList = new ObservableCollection<Contact>(e.Result.PhoneNumbers);
                ProhibitedZonesList = new ObservableCollection<UnitDTO>(e.Result.ProhibitedZonesList);
                SendAlarm_PatientInProhibitedZone = e.Result.SendAlarm_PatientInProhibitedZone;
                SendAlarm_PatientOutRoomValue = e.Result.SendAlarm_PatientOutRoomValue;
                SendAlarmAfter_MoveDay = e.Result.SendAlarmAfter_MoveDay;
                SendAlarmAfter_MoveNight = e.Result.SendAlarmAfter_MoveNight;
                SendAlarmAfter_NotMoveDay = e.Result.SendAlarmAfter_NotMoveDay;
                SendAlarmAfter_NotMoveNight = e.Result.SendAlarmAfter_NotMoveNight;
                SendWarning_PatientOutRoomValue = e.Result.SendWarning_PatientOutRoomValue;
                SendWarningsAfter_MoveDay = e.Result.SendWarningsAfter_MoveDay;
                SendWarningsAfter_MoveNight = e.Result.SendWarningsAfter_MoveNight;
                SendWarningsAfter_NotMoveDay = e.Result.SendWarningsAfter_NotMoveDay;
                SendWarningsAfter_NotMoveNight = e.Result.SendWarningsAfter_NotMoveNight;
                Reason = String.Empty;
            }
        }

        public void OnDeleteProhibitedZone()
        {
            if (SelectedProhibitedZonesIndex < 0 || SelectedProhibitedZonesIndex >= ProhibitedZonesList.Count)
                return;
            else
            {
                ProhibitedZonesList.RemoveAt(SelectedProhibitedZonesIndex);
                ProhibitedZonesListToView.RemoveAt(SelectedProhibitedZonesIndex);
            }
        }

        private void SetDataToField()
        {
            if (_data != null)
            {
                ConnectionLost_SendAlarm = _data.PatientConfigData.ConnectionLost_SendAlarm;
                ConnectionLost_SendWarning = _data.PatientConfigData.ConnectionLost_SendWarning;
                IsAlarmFromAccelerometer = _data.PatientConfigData.IsAlarmFromAccelerometer;
                IsAlarmSOS = _data.PatientConfigData.IsAlarmSOS;
                IsConnectionLost_SendAlarm = _data.PatientConfigData.IsConnectionLost_SendAlarm;
                IsConnectionLost_SendWarning = _data.PatientConfigData.IsConnectionLost_SendWarning;
                IsSendAlarm_MoveDay = _data.PatientConfigData.IsSendAlarm_MoveDay;
                IsSendAlarm_MoveNight = _data.PatientConfigData.IsSendAlarm_MoveNight;
                IsSendAlarm_NotMoveDay = _data.PatientConfigData.IsSendAlarm_NotMoveDay;
                IsSendAlarm_NotMoveNight = _data.PatientConfigData.IsSendAlarm_NotMoveNight;
                IsSendAllarm_PatientOutRoom = _data.PatientConfigData.IsSendAllarm_PatientOutRoom;
                IsSendWarning_PatientOutRoom = _data.PatientConfigData.IsSendWarning_PatientOutRoom;
                IsSendWarnings_MoveDay = _data.PatientConfigData.IsSendWarnings_MoveDay;
                IsSendWarnings_MoveNight = _data.PatientConfigData.IsSendWarnings_MoveNight;
                IsSendWarnings_NotMoveDay = _data.PatientConfigData.IsSendWarnings_NotMoveDay;
                IsSendWarnings_NotMoveNight = _data.PatientConfigData.IsSendWarnings_NotMoveNight;
                CallNumbersList = new ObservableCollection<Contact>(_data.PatientConfigData.PhoneNumbers);
                ProhibitedZonesList = new ObservableCollection<UnitDTO>(_data.PatientConfigData.ProhibitedZonesList);
                SendAlarm_PatientInProhibitedZone = _data.PatientConfigData.SendAlarm_PatientInProhibitedZone;
                SendWarning_PatientOutRoomValue = _data.PatientConfigData.SendWarning_PatientOutRoomValue;
                SendAlarm_PatientOutRoomValue = _data.PatientConfigData.SendAlarm_PatientOutRoomValue;
                SendAlarmAfter_MoveDay = _data.PatientConfigData.SendAlarmAfter_MoveDay;
                SendAlarmAfter_MoveNight = _data.PatientConfigData.SendAlarmAfter_MoveNight;
                SendAlarmAfter_NotMoveDay = _data.PatientConfigData.SendAlarmAfter_NotMoveDay;
                SendAlarmAfter_NotMoveNight = _data.PatientConfigData.SendAlarmAfter_NotMoveNight;
                SendWarningsAfter_MoveDay = _data.PatientConfigData.SendWarningsAfter_MoveDay;
                SendWarningsAfter_MoveNight = _data.PatientConfigData.SendWarningsAfter_MoveNight;
                SendWarningsAfter_NotMoveDay = _data.PatientConfigData.SendWarningsAfter_NotMoveDay;
                SendWarningsAfter_NotMoveNight = _data.PatientConfigData.SendWarningsAfter_NotMoveNight;

                List<GridViewUnitModel> _dynamicList = new List<GridViewUnitModel>();

                for (int i = 0; i < ProhibitedZonesList.Count; i++)
                {
                    UnitDTO _temp = UnitManager.Instance.Units.Where(f => f.ID == ProhibitedZonesList[i].ID).FirstOrDefault();

                    ProhibitedZonesList[i] = _temp;

                    _dynamicList.Add(new GridViewUnitModel()
                    {
                        DepartmentName = UnitManager.Instance.SearchDepartment(_temp) != null ? UnitManager.Instance.SearchDepartment(_temp).Name : String.Empty,
                        RoomName = _temp != null ? _temp.Name : String.Empty
                    });
                }

                ProhibitedZonesListToView = new ObservableCollection<GridViewUnitModel>(_dynamicList);
            }

            Reason = String.Empty;
        }

        private void SaveData()
        {
            if (_data == null)
            {
                _data = new PatientConfigurationData();
                _data.PatientConfigData = new PatientConfigurationDTO();
            }


            _data.PatientConfigData.IsAlarmFromAccelerometer = IsAlarmFromAccelerometer;
            _data.PatientConfigData.IsAlarmSOS = IsAlarmSOS;
            _data.PatientConfigData.IsConnectionLost_SendAlarm = IsConnectionLost_SendAlarm;
            _data.PatientConfigData.IsConnectionLost_SendWarning = IsConnectionLost_SendWarning;
            _data.PatientConfigData.ConnectionLost_SendAlarm = ConnectionLost_SendAlarm;
            _data.PatientConfigData.ConnectionLost_SendWarning = ConnectionLost_SendWarning;
            _data.PatientConfigData.IsSendAlarm_MoveDay = IsSendAlarm_MoveDay;
            _data.PatientConfigData.IsSendAlarm_MoveNight = IsSendAlarm_MoveNight;
            _data.PatientConfigData.IsSendAlarm_NotMoveDay = IsSendAlarm_NotMoveDay;
            _data.PatientConfigData.IsSendAlarm_NotMoveNight = IsSendAlarm_NotMoveNight;
            _data.PatientConfigData.IsSendAllarm_PatientOutRoom = IsSendAllarm_PatientOutRoom;
            _data.PatientConfigData.IsSendWarning_PatientOutRoom = IsSendWarning_PatientOutRoom;
            _data.PatientConfigData.IsSendWarnings_MoveDay = IsSendWarnings_MoveDay;
            _data.PatientConfigData.IsSendWarnings_MoveNight = IsSendWarnings_MoveNight;
            _data.PatientConfigData.IsSendWarnings_NotMoveDay = IsSendWarnings_NotMoveDay;
            _data.PatientConfigData.IsSendWarnings_NotMoveNight = IsSendWarnings_NotMoveNight;
            _data.PatientConfigData.PhoneNumbers = ConvertObservableCollectionToList(CallNumbersList);
            _data.PatientConfigData.ProhibitedZonesList = new List<UnitDTO>(ProhibitedZonesList);
            _data.PatientConfigData.SendAlarm_PatientInProhibitedZone = SendAlarm_PatientInProhibitedZone;
            _data.PatientConfigData.SendAlarm_PatientOutRoomValue = SendAlarm_PatientOutRoomValue;
            _data.PatientConfigData.SendAlarmAfter_MoveDay = SendAlarmAfter_MoveDay;
            _data.PatientConfigData.SendAlarmAfter_MoveNight = SendAlarmAfter_MoveNight;
            _data.PatientConfigData.SendAlarmAfter_NotMoveDay = SendAlarmAfter_NotMoveDay;
            _data.PatientConfigData.SendAlarmAfter_NotMoveNight = SendAlarmAfter_NotMoveNight;
            _data.PatientConfigData.SendWarning_PatientOutRoomValue = SendWarning_PatientOutRoomValue;
            _data.PatientConfigData.SendWarningsAfter_MoveDay = SendWarningsAfter_MoveDay;
            _data.PatientConfigData.SendWarningsAfter_MoveNight = SendWarningsAfter_MoveNight;
            _data.PatientConfigData.SendWarningsAfter_NotMoveDay = SendWarningsAfter_NotMoveDay;
            _data.PatientConfigData.SendWarningsAfter_NotMoveNight = SendWarningsAfter_NotMoveNight;
            _data.PatientConfigData.Reason = Reason;

            //Messenger.Default.Send<PatientConfigurationData>(_data);
            ///TODO: Przesłanie danych do okna 'PatientAlarmingView'.
            ///Z okna 'PatientAlarmingViewModel wywołać zdarzenie zamykające okno.
            ///

            string msg = String.Empty;
            int testNumber;

            for (int i = 0; i < _data.PatientConfigData.PhoneNumbers.Count; i++)
            { 
                if (!int.TryParse(_data.PatientConfigData.PhoneNumbers[i].Phone, out testNumber))
                {
                    msg = "Numer telefonu zawiera niedozowolone znaki!";
                }
                if(_data.PatientConfigData.PhoneNumbers[i].Phone.Length <= 1)
                {
                    msg = "Numer telefonu musi zawierać przynajmniej dwie cyfry!";
                }

            }

            if (!string.IsNullOrEmpty(msg))
            {
                MsgBox.Warning(msg);
                return;
            }

                    Proxy.SetPatientConfigurationAsync(_data.PatientConfigData);
        }

        public List<Contact> ConvertObservableCollectionToList(ObservableCollection<Contact> input)
        {
            List<Contact> output = new List<Contact>();

            for(int i = 0; i < input.Count; i++)
            {
                output.Add(input[i]);
            }

            return output;
        }

        public void OnEditProhibitedZone()
        {
            
        }

        public void OnCancelActionCommand()
        {
            Close(CloseButton.Cancel);
        }

        public void OnSaveActionCommand()
        {
            Close(CloseButton.OK);
        }

        public void OnDownNumber()
        {
            try
            {
                ChangePhoneNumberPosition(Direction.Down);
            }
            catch { }
        }

        public void OnUpNumber()
        {
            try
            {
                ChangePhoneNumberPosition(Direction.Up);
            }
            catch { }
        }

        private void ChangePhoneNumberPosition(Direction directionToChange)
        {
            Contact _temp = CallNumbersList[SelectedPhoneNumberIndex];

            if (directionToChange == Direction.Up && SelectedPhoneNumberIndex == CallNumbersList.Count - 1)
            {
                CallNumbersList.RemoveAt(SelectedPhoneNumberIndex);
                CallNumbersList.Insert(SelectedPhoneNumberIndex, _temp);
                CallNumbersList[SelectedPhoneNumberIndex].SequenceNr = Convert.ToByte(SelectedPhoneNumberIndex);
                SelectedPhoneNumberIndex = SelectedPhoneNumberIndex - 1;
            }
            else if (directionToChange == Direction.Up && SelectedPhoneNumberIndex != CallNumbersList.Count - 1)
            {
                if (SelectedPhoneNumberIndex > 0)
                {
                    CallNumbersList.RemoveAt(SelectedPhoneNumberIndex);
                    CallNumbersList.Insert(SelectedPhoneNumberIndex - 1, _temp);
                    CallNumbersList[SelectedPhoneNumberIndex - 1].SequenceNr = Convert.ToByte(SelectedPhoneNumberIndex - 1);
                    SelectedPhoneNumberIndex = SelectedPhoneNumberIndex - 2;
                }
            }
            else
            {
                CallNumbersList.RemoveAt(SelectedPhoneNumberIndex);
                CallNumbersList.Insert(SelectedPhoneNumberIndex + 1, _temp);
                CallNumbersList[SelectedPhoneNumberIndex + 1].SequenceNr = Convert.ToByte(SelectedPhoneNumberIndex + 1);
                SelectedPhoneNumberIndex = SelectedPhoneNumberIndex + 1;
            }
        }

        public void OnAddNumber()
        {
            _phoneData = new PhoneNumberData()
            {
                PhoneNumber = new Contact(),
                IsEdited = false
            };

            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(ClosePhoneWindowEventHandling);
            _windowsManager.ShowWindow(WindowsKeys.EditNumber, _phoneData);
        }

        public void OnAddProhibitedZone()
        {
            if (_prohibitedZoneData == null)
            {
                _prohibitedZoneData = new ProhibitedZoneData()
                {
                    ProhibitedZone = new ObservableCollection<UnitDTO>()
                };
            }

            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseProhibitedWindowEventHandling);
            _windowsManager.ShowWindow(WindowsKeys.EditProhibitedZoneModel, _prohibitedZoneData);
        }

        public void CloseProhibitedWindowEventHandling(string obj)
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(CloseProhibitedWindowEventHandling);
            ObservableCollection<Contact> _temp = CallNumbersList;

            if (obj.Equals(WindowsKeys.EditProhibitedZoneModel))
            {
                ProhibitedZonesList = _prohibitedZoneData.ProhibitedZone;
                ProhibitedZonesListToView = new ObservableCollection<GridViewUnitModel>();

                for(int i = 0; i < ProhibitedZonesList.Count; i++)
                {
                    GridViewUnitModel _t = new GridViewUnitModel()
                    {
                        RoomName = ProhibitedZonesList[i].Name,
                        DepartmentName = UnitManager.Instance.SearchDepartment(ProhibitedZonesList[i]).Name
                    };

                    ProhibitedZonesListToView.Add(_t);
                }
            }
        }

        private void Close(CloseButton option)
        {
            string msg = String.Empty;

            if (option == CloseButton.OK)
            {
                
                if (msg != String.Empty)
                {
                    MsgBox.Warning(msg);
                    return;
                }

                SaveData();
            }

            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        public void OnDeleteNumbe()
        {
            if (SelectedPhoneNumberIndex < 0 && SelectedPhoneNumberIndex >= CallNumbersList.Count)
            {
                return;
            }

            CallNumbersList.RemoveAt(SelectedPhoneNumberIndex);
            
            for(int i = 0; i < CallNumbersList.Count; i++)
            {
                CallNumbersList[i].SequenceNr = Convert.ToByte(i);
            }
        }

        public void OnEditNumber()
        {
            _phoneData = new PhoneNumberData()
            {
                PhoneNumber = new Contact()
                {
                    SequenceNr = CallNumbersList[SelectedPhoneNumberIndex].SequenceNr,
                    Phone = CallNumbersList[SelectedPhoneNumberIndex].Phone,
                    Description = CallNumbersList[SelectedPhoneNumberIndex].Description
                },
                IsEdited = true
            };

            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(ClosePhoneWindowEventHandling);
            _windowsManager.ShowWindow(WindowsKeys.EditNumber, _phoneData);
        }

        public void ClosePhoneWindowEventHandling(string obj)
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(ClosePhoneWindowEventHandling);
            ObservableCollection<Contact> _temp = CallNumbersList;

            if (obj.Equals(WindowsKeys.EditNumber))
            {
                if (_phoneData.IsCanceled)
                {

                }
                else
                {
                    if (!_phoneData.IsEdited)
                    {
                        _phoneData.PhoneNumber.SequenceNr = Convert.ToByte(CallNumbersList.Count);
                        _temp.Add(_phoneData.PhoneNumber);
                        CallNumbersList = _temp;
                    }
                    else
                    {
                        CallNumbersList[SelectedPhoneNumberIndex] = _phoneData.PhoneNumber;
                    }
                }
            }
        }

        private void OnPasteDefaultData()
        {
            if (_data != null && _data.PatientUnitId != null)
                Proxy.GetLocalizationConfigForUnitAsync((int)_data.PatientUnitId);
            else
                MsgBox.Warning("Pacjent nie jest przypisany do żadnej jednostki organizacyjnej!");
        }

        public void OnPasteFromClipboard()
        {
            if(Clipboard.PatientConfigurationClipboard == null)
            {
                return;
            }

            ConnectionLost_SendAlarm = Clipboard.PatientConfigurationClipboard.ConnectionLost_SendAlarm;
            ConnectionLost_SendAlarm = Clipboard.PatientConfigurationClipboard.ConnectionLost_SendWarning;
            IsAlarmFromAccelerometer = Clipboard.PatientConfigurationClipboard.IsAlarmFromAccelerometer;
            IsAlarmSOS = Clipboard.PatientConfigurationClipboard.IsAlarmSOS;
            IsConnectionLost_SendAlarm = Clipboard.PatientConfigurationClipboard.IsConnectionLost_SendAlarm;
            IsConnectionLost_SendWarning = Clipboard.PatientConfigurationClipboard.IsConnectionLost_SendWarning;
            ConnectionLost_SendWarning = Clipboard.PatientConfigurationClipboard.ConnectionLost_SendWarning;
            ConnectionLost_SendAlarm = Clipboard.PatientConfigurationClipboard.ConnectionLost_SendAlarm;
            IsSendAlarm_MoveDay = Clipboard.PatientConfigurationClipboard.IsSendAlarm_MoveDay;
            IsSendAlarm_MoveNight = Clipboard.PatientConfigurationClipboard.IsSendAlarm_MoveNight;
            IsSendAlarm_NotMoveDay = Clipboard.PatientConfigurationClipboard.IsSendAlarm_NotMoveDay;
            IsSendAlarm_NotMoveNight = Clipboard.PatientConfigurationClipboard.IsSendAlarm_NotMoveNight;
            IsSendAllarm_PatientOutRoom =  Clipboard.PatientConfigurationClipboard.IsSendAllarm_PatientOutRoom;
            IsSendWarning_PatientOutRoom = Clipboard.PatientConfigurationClipboard.IsSendWarning_PatientOutRoom;
            IsSendWarnings_MoveDay = Clipboard.PatientConfigurationClipboard.IsSendWarnings_MoveDay;
            IsSendWarnings_MoveNight = Clipboard.PatientConfigurationClipboard.IsSendWarnings_MoveNight;
            IsSendWarnings_NotMoveDay = Clipboard.PatientConfigurationClipboard.IsSendWarnings_NotMoveDay;
            IsSendWarnings_NotMoveNight = Clipboard.PatientConfigurationClipboard.IsSendWarnings_NotMoveNight;
            CallNumbersList = new ObservableCollection<Contact>(Clipboard.PatientConfigurationClipboard.PhoneNumbers);
            ProhibitedZonesList = new ObservableCollection<UnitDTO>(Clipboard.PatientConfigurationClipboard.ProhibitedZonesList);
            SendWarning_PatientOutRoomValue = Clipboard.PatientConfigurationClipboard.SendWarning_PatientOutRoomValue;
            SendWarningsAfter_MoveDay = Clipboard.PatientConfigurationClipboard.SendWarningsAfter_MoveDay;
            SendWarningsAfter_MoveNight = Clipboard.PatientConfigurationClipboard.SendWarningsAfter_MoveNight;
            SendWarningsAfter_NotMoveDay = Clipboard.PatientConfigurationClipboard.SendWarningsAfter_NotMoveDay;
            SendWarningsAfter_NotMoveNight = Clipboard.PatientConfigurationClipboard.SendWarningsAfter_NotMoveNight;
            SendAlarm_PatientInProhibitedZone = Clipboard.PatientConfigurationClipboard.SendAlarm_PatientInProhibitedZone;
            SendAlarm_PatientOutRoomValue = Clipboard.PatientConfigurationClipboard.SendAlarm_PatientOutRoomValue;
            SendAlarmAfter_MoveDay = Clipboard.PatientConfigurationClipboard.SendAlarmAfter_MoveDay;
            SendAlarmAfter_MoveNight = Clipboard.PatientConfigurationClipboard.SendAlarmAfter_MoveNight;
            SendAlarmAfter_NotMoveDay = Clipboard.PatientConfigurationClipboard.SendAlarmAfter_NotMoveDay;
            SendAlarmAfter_NotMoveNight = Clipboard.PatientConfigurationClipboard.SendAlarmAfter_NotMoveNight;
            ProhibitedZonesListToView = new ObservableCollection<GridViewUnitModel>(GetGridViewUnitModel(Clipboard.PatientConfigurationClipboard.ProhibitedZonesList));
        }

        public List<GridViewUnitModel> GetGridViewUnitModel(List<UnitDTO> input)
        {
            List<GridViewUnitModel> output = new List<GridViewUnitModel>();

            for(int i = 0; i < input.Count; i++)
            {
                output.Add(new GridViewUnitModel(){
                    RoomName = input[i].Name,
                    DepartmentName = UnitManager.Instance.SearchDepartment(input[i]).Name
                });
            }

            return output;
        }

        public void OnCopyToClipboard()
        {
            Clipboard.PatientConfigurationClipboard = new PatientConfigurationDTO();
            Clipboard.PatientConfigurationClipboard.ConnectionLost_SendAlarm = ConnectionLost_SendAlarm;
            Clipboard.PatientConfigurationClipboard.ConnectionLost_SendWarning = ConnectionLost_SendWarning;
            Clipboard.PatientConfigurationClipboard.IsAlarmFromAccelerometer = IsAlarmFromAccelerometer;
            Clipboard.PatientConfigurationClipboard.IsAlarmSOS = IsAlarmSOS;
            Clipboard.PatientConfigurationClipboard.IsConnectionLost_SendAlarm = IsConnectionLost_SendAlarm;
            Clipboard.PatientConfigurationClipboard.IsConnectionLost_SendWarning = IsConnectionLost_SendWarning;
            Clipboard.PatientConfigurationClipboard.ConnectionLost_SendWarning = ConnectionLost_SendWarning;
            Clipboard.PatientConfigurationClipboard.ConnectionLost_SendAlarm = ConnectionLost_SendAlarm;
            Clipboard.PatientConfigurationClipboard.IsSendAlarm_MoveDay = IsSendAlarm_MoveDay;
            Clipboard.PatientConfigurationClipboard.IsSendAlarm_MoveNight = IsSendAlarm_MoveNight;
            Clipboard.PatientConfigurationClipboard.IsSendAlarm_NotMoveDay = IsSendAlarm_NotMoveDay;
            Clipboard.PatientConfigurationClipboard.IsSendAlarm_NotMoveNight = IsSendAlarm_NotMoveNight;
            Clipboard.PatientConfigurationClipboard.IsSendAllarm_PatientOutRoom = IsSendAllarm_PatientOutRoom;
            Clipboard.PatientConfigurationClipboard.IsSendWarning_PatientOutRoom = IsSendWarning_PatientOutRoom;
            Clipboard.PatientConfigurationClipboard.IsSendWarnings_MoveDay = IsSendWarnings_MoveDay;
            Clipboard.PatientConfigurationClipboard.IsSendWarnings_MoveNight = IsSendWarnings_MoveNight;
            Clipboard.PatientConfigurationClipboard.IsSendWarnings_NotMoveDay = IsSendWarnings_NotMoveDay;
            Clipboard.PatientConfigurationClipboard.IsSendWarnings_NotMoveNight = IsSendWarnings_NotMoveNight;
            Clipboard.PatientConfigurationClipboard.PhoneNumbers = new List<Contact>(CallNumbersList);
            Clipboard.PatientConfigurationClipboard.ProhibitedZonesList = new List<UnitDTO>(ProhibitedZonesList);
            Clipboard.PatientConfigurationClipboard.SendWarning_PatientOutRoomValue = SendWarning_PatientOutRoomValue;
            Clipboard.PatientConfigurationClipboard.SendWarningsAfter_MoveDay = SendWarningsAfter_MoveDay;
            Clipboard.PatientConfigurationClipboard.SendWarningsAfter_MoveNight = SendWarningsAfter_MoveNight;
            Clipboard.PatientConfigurationClipboard.SendWarningsAfter_NotMoveDay = SendWarningsAfter_NotMoveDay;
            Clipboard.PatientConfigurationClipboard.SendWarningsAfter_NotMoveNight = SendWarningsAfter_NotMoveNight;
            Clipboard.PatientConfigurationClipboard.SendAlarm_PatientInProhibitedZone = SendAlarm_PatientInProhibitedZone;
            Clipboard.PatientConfigurationClipboard.SendAlarm_PatientOutRoomValue = SendAlarm_PatientOutRoomValue;
            Clipboard.PatientConfigurationClipboard.SendAlarmAfter_MoveDay = SendAlarmAfter_MoveDay;
            Clipboard.PatientConfigurationClipboard.SendAlarmAfter_MoveNight = SendAlarmAfter_MoveNight;
            Clipboard.PatientConfigurationClipboard.SendAlarmAfter_NotMoveDay = SendAlarmAfter_NotMoveDay;
            Clipboard.PatientConfigurationClipboard.SendAlarmAfter_NotMoveNight = SendAlarmAfter_NotMoveNight;
        }

        public void Refresh(IUnit unit)
        {
            Proxy.GetPatientConfigurationAsync(_data.PatientConfigData.PatientId);
        }

        public event EventHandler LoadingCompleted;

        public ViewModelState State
        {
            get { throw new NotImplementedException(); }
        }

        public string Title
        {
            get { throw new NotImplementedException(); }
        }

        public bool ExitButtonVisible
        {
            get { throw new NotImplementedException(); }
        }
    }


    public enum Direction
    {
        Up = -1,
        Down = 1
    }

    public struct GridViewUnitModel
    {
        public string RoomName { get; set; }
        public string DepartmentName { get; set; }
    }
}
