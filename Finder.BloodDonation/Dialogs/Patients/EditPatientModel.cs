﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Tools;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Converters;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Settings;
using System.Collections.Generic;
using System.Linq;
using Telerik.Windows.Controls;
using ViewModelBase = FinderFX.SL.Core.MVVM.ViewModelBase;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.DynamicData.DBs;
using Finder.BloodDonation.DynamicData.Memory;
using Finder.BloodDonation.DynamicData.Paging;
using ZonesInfoDto = Finder.BloodDonation.Model.Dto.ZonesInfoDto;
using Finder.BloodDonation.LanguageHelpher;
using Finder.BloodDonation.Tabs.PatientDetails;

namespace Finder.BloodDonation.Dialogs.Patients
{
    public partial class EditPatientModel : ViewModelBase, IDialogWindow
    {
        #region Const
        public const int DEPARTMENT_ID = 8;
        public const int ROOM_ID = 13;
        #endregion

        #region Variable
        private PatientEditData _data;
        private bool _isEdited;
        #endregion

        #region Property
        [RaisePropertyChanged]
        public string Title { get; set; }

        [RaisePropertyChanged]
        public int PatientID { get; set; }

        [RaisePropertyChanged]
        public string PatientFirstName { get; set; }

        [RaisePropertyChanged]
        public string PatientLastName { get; set; }

        [RaisePropertyChanged]
        public string PatientPesel { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<UnitDTO> DepartmentList { get; set; }

        [RaisePropertyChanged]
        public int SelectedDepartment { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<UnitDTO> RoomList { get; set; }

        [RaisePropertyChanged]
        public int SelectedRoom { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<BandDTO> BandList { get; set; }

        [RaisePropertyChanged]
        public int SelectedBand { get; set; }

        [RaisePropertyChanged]
        public string BandKey { get; set; }

        [RaisePropertyChanged]
        public string Reason { get; set; }

        IList<BandDTO> testBandsDb;

        [RaisePropertyChanged]
        public Visibility EditMode { get; set; }

        [RaisePropertyChanged]
        public string BandBarCode { get; set; }

        [RaisePropertyChanged]
        public double DesignHeightSize { get; set; }

        [RaisePropertyChanged]
        public Thickness MarginToEdit { get; set; }
        
        private UnitsServiceClient Proxy { get; set; }

        private IWindowsManager _windowsManager;

        public string WindowKey
        {
            get { return WindowsKeys.EditPatient; }
        }

        //public void CreateTestBandList()
        //{
        //    testBandsDb = BandDb.GetItems();
        //    BandList = new ObservableCollection<BandDTO>();

        //    for (int i = 0; i < testBandsDb.Count; i++)
        //    {
        //        BandList.Add(testBandsDb[i]);
        //    }
        //}

        object IDialogWindow.Data
        {
            set
            {
                _data = (PatientEditData)value;
                _isEdited = !string.IsNullOrEmpty(_data.Patient.FirstName);
                EditMode = _isEdited ? Visibility.Collapsed : Visibility.Visible;
                MarginToEdit = _isEdited ? new Thickness(0d, 20d, 0d, 20d) : new Thickness(0d);

                var title = (_isEdited) ? LanguageManager.ControlsText.Text(ControlsTextCodes.EDIT_PATIENT_LABEL) 
                    : LanguageManager.ControlsText.Text(ControlsTextCodes.REGISTER_PATIENT_LABEL);

                Title = title;

                var _temp = UnitManager.Instance.Units;

                if (DepartmentList == null)
                {
                    DepartmentList = new ObservableCollection<UnitDTO>();
                    DepartmentList.Add(new UnitDTO() { Name =LanguageManager.ControlsText.Text(ControlsTextCodes.DEFAULT_EMPTY_VALUE) });

                    foreach (UnitDTO unit in _temp)                             
                    {
                        if (UnitTypeOrganizer.IsHospitalDepartment(unit.UnitTypeIDv2))
                        {
                            DepartmentList.Add(unit);
                        }
                    }

                    SelectedDepartment = DepartmentList.IndexOf(DepartmentList.FirstOrDefault(x=>x.ID == UnitTypeOrganizer.GetRegistartionUnitId()));
                }
            
                if (_isEdited)
                {
                    PatientID = _data.Patient.ID;

                    PatientFirstName = _data.Patient.FirstName;

                    PatientLastName = _data.Patient.LastName;

                    PatientPesel = _data.Patient.Pesel;

                    SelectedDepartment = GetSelectedDepartmentIndex();

                    SelectedRoom = GetSelectedRoomIndex();

                    BandBarCode = _data.Patient.BandBarCode;

                    Reason = String.Empty;
                }
                else
                {
                    ClearFields();
                }
            }
        }
        #endregion

        #region Command
        public RelayCommand OkClick { get; set; }
        public RelayCommand CancelClick { get; set; }
        public RelayCommand DepartmentSelectionChanged { get; set; }
        public RelayCommand WindowLoaded { get; set; }
        #endregion

        public EditPatientModel(IUnityContainer unityContainer) : base(unityContainer)
        {
            OkClick = new RelayCommand(OnOkClick);
            CancelClick = new RelayCommand(OnCancelClick);
            DepartmentSelectionChanged = new RelayCommand(OnDepartmentChanged);
            WindowLoaded = new RelayCommand(OnWindowLoaded);

            Proxy = ServiceFactory.GetService<UnitsServiceClient>();
            Proxy.AssignBandUsingBarCodeCompleted += Proxy_AssignBandUsingBarCodeCompleted;
            Proxy.PatientToHospitalCompleted += Proxy_PatientToHospitalCompleted;
            Proxy.ZonesGetCompleted += Proxy_ZonesGetCompleted;
            Proxy.AssignBandToObjectCompleted += Proxy_AssignBandToObjectCompleted;
            Proxy.EditPatientCompleted += Proxy_EditPatientCompleted;
            Proxy.GetBandConfigurationCompleted += Proxy_GetBandConfigurationCompleted;

            var viewFactory = unityContainer.Resolve<ViewFactory>();
            _windowsManager = this.Container.Resolve<IWindowsManager>();
            _windowsManager.RegisterWindowIfNotRegister(WindowsKeys.SuccessDialog, "Powodzenie", viewFactory.CreateView<Finder.BloodDonation.Dialogs.Success.Success>(), new Size(440, 170), true);
        
        }

        void Proxy_GetBandConfigurationCompleted(object sender, GetBandConfigurationCompletedEventArgs e)
        {
            BandList = new ObservableCollection<BandDTO>(e.Result);
        }

        public void OnWindowLoaded()
        {
            Proxy.GetBandConfigurationAsync(null);
        }

        public void Proxy_EditPatientCompleted(object sender, EditPatientCompletedEventArgs e)
        {
            int tmp;

            if (e.Result != null && !int.TryParse(e.Result.ToString(), out tmp))
            {
                MsgBox.Error(e.Result.ToString());
            }
            else
            {
                _data.Canceled = false;

                Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
            }
        }

        private void Proxy_AssignBandToObjectCompleted(object sender, AssignBandToObjectCompletedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void OnRegisterPatientWithoutBand(object sender, WindowClosedEventArgs e)
        {
            bool? confirmed = (bool?) e.DialogResult;

            if (confirmed.HasValue && confirmed.Value)
            {
                Proxy.PatientToHospitalAsync(_data.Patient);
            }
            else
            {
                return;
            }
        }

        void Proxy_ZonesGetCompleted(object sender, ZonesGetCompletedEventArgs e)
        {
            if (e.Result == null)
             return;
            
            GetRoomList(e.Result);
        }

        private void Proxy_GetUnitsWithTransmitterCompleted(object sender, GetUnitsWithTransmitterCompletedEventArgs e)
        {
            
        }

        private void Proxy_PatientToHospitalCompleted(object sender, PatientToHospitalCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                const string NO_FORCE_TO_ASSIGN_CODE = "-1";

                if (e.Result.ErrorCode.Equals(NO_FORCE_TO_ASSIGN_CODE))
                {
                    MsgBox.Confirm(
                       LanguageManager.Messages.Message(MessageCodes.UNREGISTER_AND_REGISTER_PATIENT_QUESTION),
                        OnAddPowerToCreatePatiente, MsgBox.Buttons.YesNo);
                }
                else if (!String.IsNullOrEmpty(e.Result.ErrorCode) || !String.IsNullOrEmpty(e.Result.ErrorMessage))
                {
                    MsgBox.Warning(e.Result.ErrorMessage);
                }
                else
                {
                    _windowsManager.ShowWindow(WindowsKeys.SuccessDialog, LanguageManager.Messages.Message(MessageCodes.SUCCESSFULL_REGISTRATION_PATIENT));
                    _data.Canceled = false;

                    Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
                }
            }
            else
            {
                MsgBox.Warning(LanguageManager.Errors.Error(ErrorCodes.OTHER_ERROR));
            }
        }

        void OnAddPowerToCreatePatiente(object sender, WindowClosedEventArgs e)
        {
            bool? confirmed = (bool?) e.DialogResult;
            _data.Patient.ForceAction = 1;

            if(confirmed.HasValue && confirmed.Value)
                Proxy.PatientToHospitalAsync(_data.Patient);
        }

        void Proxy_AssignBandUsingBarCodeCompleted(object sender, AssignBandUsingBarCodeCompletedEventArgs e)
        {
            const string NO_FORCE_TO_ASSIGN_CODE = "-1";
            short _temp;

            Proxy.PatientToHospitalAsync(_data.Patient);
        }

        public void OnConfirmUnlockAllReceivers(object sender, WindowClosedEventArgs e)
        {
            bool? confirmed = (bool?)e.DialogResult;

            //if (confirmed.HasValue && confirmed.Value)
                //Proxy.AssignBandUsingBarCodeAsync(BandBarCode, 1);
        }

        #region Events
        private void OnOkClick()
        {
            Close(CloseButton.OK);
        }

        private void OnCancelClick()
        {
            Close(CloseButton.Cancel);
        }

        private void OnDepartmentChanged()
        {
            Proxy.ZonesGetAsync();
        }
        #endregion

        #region Helphers method
        private void ClearFields()
        {
            PatientFirstName = string.Empty;

            PatientLastName = string.Empty;

            PatientPesel = string.Empty;

            BandBarCode = string.Empty;

            Reason = string.Empty;

            SelectedDepartment = DepartmentList.IndexOf(DepartmentList.FirstOrDefault(x => x.ID == UnitTypeOrganizer.GetRegistartionUnitId()));
        }

        private void GetRoomList(List<ZonesInfoDto> zonesInfo)
        {
            List<UnitDTO> unitsWithTransmitters = new List<UnitDTO>();
            List<UnitDTO> allUnits = UnitManager.Instance.Units.ToList();

            foreach (var zoneInfo in zonesInfo)
            {
                 unitsWithTransmitters.Add(allUnits.Where(e=>e.ID == zoneInfo.UnitId).FirstOrDefault());
            }

            RoomList = new ObservableCollection<UnitDTO>(SelectedDepartment != -1 ? unitsWithTransmitters.GetRoomsForSelectedDepartment(DepartmentList[SelectedDepartment].ID) : unitsWithTransmitters);
        }

        private int GetSelectedDepartmentIndex()
        {
            int _selectedIndex = 0;

            for (int i = 0; i < DepartmentList.Count; i++)
            {
                    if (_data.Patient.DepartmentId == DepartmentList[i].ID)
                    {
                        _selectedIndex = i;
                    }
            }

            return _selectedIndex;
        }

        private int GetSelectedRoomIndex()
        {
            int _selectedIndex = 0;

            if (RoomList != null)
            {
                for (int i = 0; i < RoomList.Count; i++)
                {
                    if (_data.Patient.RoomId == RoomList[i].ID)
                    {
                        _selectedIndex = i;
                    }
                }

                return _selectedIndex;
            }
            return 0;
        }

        private void Close(CloseButton button)
        {
            if (button == CloseButton.OK)
            {
                string errorMsg = "";

                if (string.IsNullOrWhiteSpace(PatientFirstName))
                {
                    errorMsg = LanguageManager.Errors.Error(ErrorCodes.BAD_FIRSTNAME_FORMAT);
                }
                // RADOM
                //if(string.IsNullOrWhiteSpace(PatientLastName))
                //{
                //    errorMsg = LanguageManager.Errors.Error(ErrorCodes.BAD_LASTNAME_FORMAT);
                //}
                //if(!PeselValidator.ValidatePesel(PatientPesel))
                //{
                //    errorMsg = LanguageManager.Errors.Error(ErrorCodes.BAD_PESEL_FORMAT);
                //}
                //if(string.IsNullOrEmpty(BandBarCode))
                //{
                //    errorMsg = LanguageManager.Errors.Error(ErrorCodes.NON_ENTERED_BAND_BARCODE);
                //}
                //else if (!BandList.Any(x => x.BarCode == BandBarCode))
                //{
                //    errorMsg = LanguageManager.Errors.Error(ErrorCodes.WRONG_BARCODE);
                //}

                if (errorMsg != "")
                {
                    MsgBox.Error(errorMsg);
                    return;
                }

                SaveData();
            }
            else if (button.Equals(CloseButton.Cancel))
            {
                Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
            }
        }

        private void SaveData()
        {   
            _data.Patient.FirstName = PatientFirstName;
            _data.Patient.LastName = PatientLastName;
            _data.Patient.Pesel = PatientPesel;

            if (SelectedDepartment > 0 && SelectedDepartment < DepartmentList.Count)
            {
                _data.Patient.DepartmentName = DepartmentList[SelectedDepartment].Name;
                _data.Patient.DepartmentId = DepartmentList[SelectedDepartment].ID;
            }
            if(
                RoomList != null  &&
                SelectedRoom > -1 && 
                SelectedRoom < RoomList.Count
                )
            {
                _data.Patient.RoomName = RoomList[SelectedRoom].Name;
                _data.Patient.RoomId = RoomList[SelectedRoom].ID;
            }

            _data.Patient.BandBarCode = BandBarCode;
            _data.Patient.Reason = Reason;

            if (!_isEdited)
            {
                if (SelectedDepartment <= 0)
                {
                    MsgBox.Error(LanguageManager.Errors.Error(ErrorCodes.NON_SELLECTION_DEPARTMENT));
                    return;
                }
                
                Proxy.PatientToHospitalAsync(_data.Patient);
            }
            else
            {

                Proxy.EditPatientAsync(_data.Patient);
            }
        }
        #endregion
    }
}
