﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Dialogs.Patients
{
    public class PatientAssignData
    {
        public PatientDTO Patient { get; set; }
        public IUnit currentUnitId { get; set; }
        public string Code { get; set; }
        public string CodeType { get; set; }

        public AssignWindowType AssignWindowType { get; set; }
        public bool IsCanceled = true;
    }

    public enum AssignWindowType
    {
        AssignToRoom = 0,
        AssignToDepartment = 1,
        UnsubscribingFromRoom,
        UnsubscribingFromDepartment
    }
}
