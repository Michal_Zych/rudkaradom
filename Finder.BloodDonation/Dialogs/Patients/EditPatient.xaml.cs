﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Telerik.Windows.Controls.Data.DataForm;

namespace Finder.BloodDonation.Dialogs.Patients
{
    [ViewModel(typeof(EditPatientModel))]
    public partial class EditPatient : UserControl
    {
        public EditPatient()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //okButton.IsFocused = true;
            //reasonTextBox.Focu
            okButton.Focus();
        }
    }
}
