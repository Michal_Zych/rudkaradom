﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Dialogs.Patients
{
    public class ProhibitedZone
    {
        public int UnitId { get; set; }
        public string UnitName { get; set; }
        public string UnitLocation { get; set; }
    }
}
