﻿using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Model.Dto;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.Model.Enum;
using System.Collections.ObjectModel;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Tabs.MenuAdministration.SettingsSystem;
using Finder.BloodDonation.LanguageHelpher;

namespace Finder.BloodDonation.Dialogs.Patients
{
    public class PatientAssignModel : ViewModelBase, IDialogWindow
    {
        private const string SPACE = " ";

        private PatientAssignData _data;

        #region RelayCommands Property
        public RelayCommand OkClick { get; set; }
        public RelayCommand CancelClick { get; set; }
        #endregion


        #region View Fields Property 
        [RaisePropertyChanged]
        public string Title { get; set; }

        [RaisePropertyChanged]
        public string PatientDisplayName { get; set; }

        public string CodeType { get; set; }

        [RaisePropertyChanged]
        public string Code { get; set; }

        [RaisePropertyChanged]
        public string EmptyDisplayText { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<UnitDTO> Collection { get; set; }

        [RaisePropertyChanged]
        public int Selected { get; set; }

        [RaisePropertyChanged]
        public string ComboboxAssignData { get; set; }

        [RaisePropertyChanged]
        public Visibility DepartmentVisibility { get; set; }

        [RaisePropertyChanged]
        public Visibility RoomVisibility { get; set; }

        [RaisePropertyChanged]
        public string PatientFirstName { get; set; }

        [RaisePropertyChanged]
        public string PatientLastName { get; set; }

        [RaisePropertyChanged]
        public string SelectedDepartmentName { get; set; }

        [RaisePropertyChanged]
        public Visibility CodeVisibility { get; set; }

        [RaisePropertyChanged]
        public Visibility ComboBoxVisibility { get; set; }

        [RaisePropertyChanged]
        public Visibility PatientInfoVisibility { get; set; }

        [RaisePropertyChanged]
        public string PatientDisplayLocation { get; set; }

        [RaisePropertyChanged]
        public string Reason { get; set; }

        private IWindowsManager _windowsManager;
        #endregion
        
        public string WindowKey
        {
            get { return WindowsKeys.PatientAssign; }
        }

        #region Service helpher properties, methods and events
        private UnitsServiceClient Proxy { get; set; }

        private void ExecuteServiceAsyncMethods()
        {
            if (Proxy != null)
                Proxy.ZonesGetAsync();
        }

        private void InitializeUnitsService()
        {
            Proxy = ServiceFactory.GetService<UnitsServiceClient>();
            Proxy.ZonesGetCompleted += Proxy_ZonesGetCompleted;
            Proxy.AssignPatientToWardRoomCompleted += Proxy_AssignPatientToWardRoomCompleted;
        }

        void Proxy_AssignPatientToWardRoomCompleted(object sender, AssignPatientToWardRoomCompletedEventArgs e)
        {
            if (!e.Result.Equals(String.Empty) && !e.Result.Equals(_data.Patient.ID))
            {
                MsgBox.Warning(e.Result);
                return;
            }
            else
            {
                switch (_data.AssignWindowType)
                {
                    case AssignWindowType.AssignToDepartment:
                        _windowsManager.ShowWindow(WindowsKeys.SuccessDialog,"Pomyślnie przypisano urządzenie na oddział");
                        break;

                    case AssignWindowType.AssignToRoom:
                        _windowsManager.ShowWindow(WindowsKeys.SuccessDialog, "Pomyślnie przypisano urządzenie do sali");
                        break;

                    case AssignWindowType.UnsubscribingFromDepartment:
                        _windowsManager.ShowWindow(WindowsKeys.SuccessDialog, "Pomyślnie wypisano urządzenie z oddziału");
                        break;

                    case AssignWindowType.UnsubscribingFromRoom:
                        _windowsManager.ShowWindow(WindowsKeys.SuccessDialog, "Pomyślnie wypisano urządzenie z sali");
                        break;
                }

                Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
            }
        }

        private void Proxy_ZonesGetCompleted(object sender, ZonesGetCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                Collection = _data.AssignWindowType.Equals(AssignWindowType.AssignToDepartment) ? GetDepartmentUnits(UnitManager.Instance.Units) :
                    GetZonesForDepartment(e.Result);

                //if (Finder.BloodDonation.Dialogs.Patients.AssignWindowType.AssignToDepartment == _data.AssignWindowType)
                //{
                if (_data.Patient != null && _data.Patient.DepartmentId != 0)
                    Selected = Collection.IndexOf(Collection.FirstOrDefault(x => x.ID == _data.Patient.DepartmentId));
                else
                    Selected = Collection.IndexOf(Collection.FirstOrDefault(x => x.ID == UnitTypeOrganizer.GetRegistartionUnitId()));

            }
        }
        #endregion

        public PatientAssignModel(IUnityContainer unityContainer) : base(unityContainer)
        {
            InitializeCommand();
            InitializeUnitsService();

            var viewFactory = unityContainer.Resolve<ViewFactory>();
            _windowsManager = this.Container.Resolve<IWindowsManager>();
            _windowsManager.RegisterWindowIfNotRegister(WindowsKeys.SuccessDialog, "Powodzenie", viewFactory.CreateView<Finder.BloodDonation.Dialogs.Success.Success>(), new Size(440, 170), true);
        }
        private void InitializeCommand()
        {
            OkClick = new RelayCommand(OnOkClick);
            CancelClick = new RelayCommand(OnCancelClick);
        }

        public object Data
        {
            set 
            {
                _data = (PatientAssignData)value;

                if (_data != null)
                {
                    ClearFields();
                    
                    ExecuteServiceAsyncMethods();

                    SetWindowDataToFields();
                    SetAllDataToWindowFields(PermissionManager.CheckIfCurrentUserHasAuthentication(PermissionSectionType.Patient));
                }
            }
        }

        private void ClearFields()
        {
            Code = String.Empty;
            Reason = String.Empty;
        }

        #region Window fieds helphers method
        private void SetAllDataToWindowFields(bool isAuthenticatedState)
        {
            SetWindowDataToFields();
            SetPatientDataToFields(isAuthenticatedState, String.Empty, String.Empty);
        }

        private void SetWindowDataToFields()
        {
            Title = _data.AssignWindowType == AssignWindowType.AssignToDepartment ? "Dodaj urządzenie " + (_data.Patient != null ? (_data.Patient.FirstName + " na oddział") : string.Empty + "na oddział.")
                : _data.AssignWindowType == AssignWindowType.AssignToRoom ? "Dodaj urządzenie " + (_data.Patient != null ? (_data.Patient.FirstName + " na salę") : string.Empty + "na salę.")
                :   _data.AssignWindowType == AssignWindowType.UnsubscribingFromDepartment ? "Usuń urządzenie " + " " + _data.Patient.FirstName + " z oddziału "
                : "Usuń urządzenie  " + " " + _data.Patient.FirstName + " z sali";

            CodeVisibility = _data.AssignWindowType == AssignWindowType.AssignToDepartment ? Visibility.Visible
                : _data.AssignWindowType == AssignWindowType.AssignToRoom ? Visibility.Collapsed
                : _data.AssignWindowType == AssignWindowType.UnsubscribingFromDepartment ? Visibility.Collapsed
                : _data.AssignWindowType == AssignWindowType.UnsubscribingFromRoom ? Visibility.Collapsed : Visibility.Collapsed;

            ComboBoxVisibility = _data.AssignWindowType == AssignWindowType.AssignToDepartment ||
                _data.AssignWindowType == AssignWindowType.AssignToRoom 
                ? Visibility.Visible : Visibility.Collapsed;

            if (CodeVisibility == Visibility.Collapsed)
            {
                Code = _data.Code;
            }
            else if (CodeVisibility == Visibility.Visible && _data.Patient != null)
            {
                Code = _data.Patient.Pesel;


                PatientDisplayName = _data.AssignWindowType == AssignWindowType.UnsubscribingFromDepartment || _data.AssignWindowType == AssignWindowType.UnsubscribingFromRoom
                    ? "Nazwa wyświetlana: " + _data.Patient.FirstName + " " + _data.Patient.LastName.FirstOrDefault() + "."
                    : String.Empty;
            }

            ComboboxAssignData = GetComboxAssignData();
            DepartmentVisibility = GetDepartmentVisibility();
            SelectedDepartmentName = GetSelectedDepartmentName();
            EmptyDisplayText = GetEmptyDisplayText();
            PatientInfoVisibility = _data.AssignWindowType == AssignWindowType.UnsubscribingFromDepartment || _data.AssignWindowType == AssignWindowType.UnsubscribingFromRoom 
                ? Visibility.Visible 
                : Visibility.Collapsed;

            RoomVisibility = _data.AssignWindowType == AssignWindowType.AssignToRoom ? Visibility.Visible : Visibility.Collapsed;

            if (_data.Patient != null && _data.AssignWindowType == AssignWindowType.UnsubscribingFromDepartment)
                PatientDisplayLocation = "Aktualny oddział: " + _data.Patient.DepartmentName + Container.Resolve<ISettingsRespository>().Settings.UnitsLocationSeparator + _data.Patient.RoomName;

            if (_data.Patient != null && _data.AssignWindowType == AssignWindowType.UnsubscribingFromRoom)
                PatientDisplayLocation = "Aktualna sala: " + _data.Patient.DepartmentName + Container.Resolve<ISettingsRespository>().Settings.UnitsLocationSeparator + _data.Patient.RoomName;
        }
        private void SetPatientDataToFields(bool isAuthenticatedState, string patientDisplayNameIfNotAuthenticated, string displayPesselIfNotAuthenticated)
        {
            //PatientDisplayName = isAuthenticatedState ? GetPatientDisplayName() : patientDisplayNameIfNotAuthenticated;
            
            //Key = isAuthenticatedState ? _data.Patient.Pesel : displayPesselIfNotAuthenticated;
        }

        private string GetEmptyDisplayText()
        {
            return _data.AssignWindowType.Equals(AssignWindowType.AssignToDepartment)
                        ? "Nie wybrano oddziału"
                        : "Nie wybrano sali";
        }

        private string GetSelectedDepartmentName()
        {
            return _data.AssignWindowType.Equals(AssignWindowType.AssignToRoom) ? GetAssignedDepartmentName() : _data.currentUnitId != null ? GetDepartmentName() : String.Empty;
        }

        private string GetDepartmentName()
        {
            string name = String.Empty;

            if(_data.currentUnitId != null)
            {
                if(UnitManager.Instance.Units.FirstOrDefault( x=> x.ID == _data.currentUnitId.Identity).UnitTypeIDv2 == 29)
                {
                    name = UnitManager.Instance.Units.FirstOrDefault(x => x.ID == _data.currentUnitId.Identity).Name;
                }
            }

            return name;
        }
        private string GetComboxAssignData()
        {
            return _data.AssignWindowType == AssignWindowType.AssignToDepartment
                        ? "Oddział:"
                        : "Pokój:";
        }

        private string GetAssignedDepartmentName()
        {
            string name = String.Empty;

            try
            {
                name = UnitManager.Instance.Units.Where(e => e.ID == _data.Patient.DepartmentId)
                    .FirstOrDefault().Name;
            }
            catch(NullReferenceException e)
            {
                name = "Nie wybrano!";
            }

            return name;
        }

        private Visibility GetDepartmentVisibility()
        {
            return _data.AssignWindowType == AssignWindowType.AssignToDepartment || _data.AssignWindowType == AssignWindowType.AssignToRoom
                        ? Visibility.Visible
                        : Visibility.Collapsed;
        }

        private ObservableCollection<UnitDTO> GetDepartmentUnits(IEnumerable<UnitDTO> allUnits)
        {
            return new ObservableCollection<UnitDTO>(allUnits.Where(e => UnitTypeOrganizer.IsHospitalDepartment(e.UnitTypeIDv2)).ToList());
        }

        /// <summary>
        /// This method search all unit with transmitter for assigned departments identity.
        /// </summary>
        /// <param name="zonesInfo"></param>
        /// <returns>All units with transmitter for assigned department.</returns>
        private ObservableCollection<UnitDTO> GetZonesForDepartment(List<ZonesInfoDto> zonesInfo)
        {
            return GetZonesUnitForCurrentDepartment(GetAllZonesUnit(zonesInfo));
        }

        private ObservableCollection<UnitDTO> GetZonesUnitForCurrentDepartment(IEnumerable<UnitDTO> allZonesList)
        {
               return new ObservableCollection<UnitDTO>(allZonesList.GetRoomsForSelectedDepartment(_data.Patient.DepartmentId));
        }
        /// <summary>
        /// This method search all zones unit with transmitter.
        /// </summary>
        /// <param name="zonesInfo"></param>
        /// <returns>All units with transmitter.</returns>
        private IEnumerable<UnitDTO> GetAllZonesUnit(List<ZonesInfoDto> zonesInfo)
        {
            return
                UnitManager.Instance.Units.Where(e => zonesInfo.Select(z => z.UnitId).Contains(e.ID));
        }
        #endregion

        #region consistency of data helphers method
        private void SaveData()
        {
            _data.IsCanceled = false;

            if (_data.AssignWindowType.Equals(AssignWindowType.AssignToDepartment))
            {
                if (Collection[Selected].ID == _data.Patient.DepartmentId)
                {
                    MsgBox.Warning("Pacjent jest już przypisany do tego oddziału!");
                    return;
                }

                if (_data.Patient == null)
                    _data.Patient = new PatientDTO();

                _data.Patient.DepartmentId = Collection[Selected].ID;
                _data.Patient.DepartmentName = Collection[Selected].Name;
                _data.Patient.FirstName = PatientFirstName;
                _data.Patient.LastName = PatientLastName;
                _data.Patient.RoomId = -1;
                _data.Patient.RoomName = null;
            }
            else if(_data.AssignWindowType.Equals(AssignWindowType.UnsubscribingFromDepartment))
            {
                if (_data.Patient.DepartmentId == -1 || _data.Patient.DepartmentId == null)
                {
                    MsgBox.Warning("Pacjent nie posiada przypisanego oddziału!");
                    return;
                }

                _data.Patient.DepartmentId = -1;
                _data.Patient.RoomId = -1;
            }
            else if(_data.AssignWindowType.Equals(AssignWindowType.UnsubscribingFromRoom))
            {
                if (_data.Patient.RoomId == -1 || _data.Patient.RoomId == null)
                {
                    MsgBox.Warning("Pacjent nie posiada przypisanej sali!");
                    return;
                }

                _data.Patient.RoomId = -1;
            }
            else
            {
                if (Collection[Selected].ID == _data.Patient.RoomId)
                {
                    MsgBox.Warning("Pacjent jest już przypisany do tej sali!");
                    return;
                }

                _data.Patient.RoomId = Collection[Selected].ID;
                _data.Patient.RoomName = Collection[Selected].Name;
            }

            _data.Patient.Reason = Reason;
            Proxy.AssignPatientToWardRoomAsync(_data.Patient, Code, CodeType);
        }
        
        /// <summary>
        /// Checking if consistency of data in window fields is correct.
        /// </summary>
        /// <returns>True, if data in all fields is correct.</returns>
        private bool CheckIfWindowFieldDataIsCorrect()
        {
            if (Selected < 0 || Selected > Collection.Count - 1)
                return false;
            else
                return true;
        }

        /// <summary>
        /// This method show error if message is not empty.
        /// </summary>
        /// <param name="msg">Error message.</param>
        /// <returns>True, if error message is showed.</returns>
        private bool CheckAndExecutedMessageIfNotEmpty(string msg)
        {
            //if (String.IsNullOrEmpty(msg))
            //{
                return false;
            //}
            //else
            //{
            //    MsgBox.Error(msg);
            //    return true;
            //}
            
        }
        private string GetErrorMessage()
        {
            return CheckIfWindowFieldDataIsCorrect()
                ? String.Empty
                : "Nie wybrano " + (_data.AssignWindowType == AssignWindowType.AssignToDepartment ? "oddziału" : "sali");
        }

        #endregion
        #region Command methods
        private void OnCancelClick()
        {
            Cancel(CloseButton.Cancel);
        }

        private void OnOkClick()
        {
            Cancel(CloseButton.OK);
        }

        private void Cancel(CloseButton action)
        {


            if (action.Equals(CloseButton.OK))
                if (CheckAndExecutedMessageIfNotEmpty(GetErrorMessage()))
                    return;
                else
                    SaveData();
            else
                Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }
        #endregion
    }
}
