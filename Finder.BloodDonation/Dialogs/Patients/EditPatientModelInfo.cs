﻿using Finder.BloodDonation.LanguageHelpher;
using FinderFX.SL.Core.MVVM;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Dialogs.Patients
{
    public partial class EditPatientModel
    {
        [RaisePropertyChanged]
        public string AddTextButton
        {
            get
            {
                return LanguageManager.ControlsText.Text(ControlsTextCodes.ADDING_BUTTON);
            }
        }

        [RaisePropertyChanged]
        public string CancelTextButton
        {
            get
            {
                return LanguageManager.ControlsText.Text(ControlsTextCodes.CANCEL_BUTTON);
            }
        }

        [RaisePropertyChanged]
        public string EmptyDepartmentComboboxText
        {
            get
            {
                return LanguageManager.ControlsText.Text(ControlsTextCodes.EMPTY_DEPARTMENT_COMBOBOX_INFO);
            }
        }

        [RaisePropertyChanged]
        public string EmptyRoomComboboxText
        {
            get
            {
                return LanguageManager.ControlsText.Text(ControlsTextCodes.EMPTY_ROOM_COMBOBOX_INFO);
            }
        }

        [RaisePropertyChanged]
        public string FirstNameLabel
        {
            get
            {
                return LanguageManager.ControlsText.Text(ControlsTextCodes.FIRSTNAME_LABEL);
            }
        }

        [RaisePropertyChanged]
        public string FirstNameToolTip
        {
            get
            {
                return LanguageManager.ToolTips.ToolTip(ToolTipCodes.FIRSTNAME_TOOLTIP);
            }
        }

        [RaisePropertyChanged]
        public string LastNameLabel
        {
            get
            {
                return LanguageManager.ControlsText.Text(ControlsTextCodes.LASTNAME_LABEL);
            }
        }

        [RaisePropertyChanged]
        public string LastNameToolTip
        {
            get
            {
                return LanguageManager.ToolTips.ToolTip(ToolTipCodes.LASTNAME_TOOLTIP);
            }
        }

        [RaisePropertyChanged]
        public string PeselLabel
        {
            get
            {
                return LanguageManager.ControlsText.Text(ControlsTextCodes.PESEL_LABEL);
            }
        }

        [RaisePropertyChanged]
        public string PeselToolTip
        {
            get
            {
                return LanguageManager.ToolTips.ToolTip(ToolTipCodes.PESEL_TOOLTIP);
            }
        }

        [RaisePropertyChanged]
        public string DepartmetLabel
        {
            get
            {
                return LanguageManager.ControlsText.Text(ControlsTextCodes.DEPARTMENT_LABEL);
            }
        }

        [RaisePropertyChanged]
        public string RoomLabel
        {
            get
            {
                return LanguageManager.ControlsText.Text(ControlsTextCodes.ROOM_LABEL);
            }
        }

        [RaisePropertyChanged]
        public string BandBarCodeLabel
        {
            get
            {
                return LanguageManager.ControlsText.Text(ControlsTextCodes.BAND_BARCODE_LABEL);
            }
        }

        [RaisePropertyChanged]
        public string DepartmentComboboxToolTip
        {
            get
            {
                return LanguageManager.ToolTips.ToolTip(ToolTipCodes.DEPARTMENT_COMBOBOX_TOOLTIP);
            }
        }

        [RaisePropertyChanged]
        public string RoomComboboxToolTip
        {
            get
            {
                return LanguageManager.ToolTips.ToolTip(ToolTipCodes.ROOM_COMBOBOX_TOOLTIP);
            }
        }

        [RaisePropertyChanged]
        public string BandBarCodeToolTip
        {
            get
            {
                return LanguageManager.ToolTips.ToolTip(ToolTipCodes.BAND_BARCODE_TOOLTIP);
            }
        }

        [RaisePropertyChanged]
        public string ReasonLabel
        {
            get
            {
                return LanguageManager.ControlsText.Text(ControlsTextCodes.REASON);
            }
        }

        [RaisePropertyChanged]
        public string ReasonToolTip
        {
            get
            {
                return LanguageManager.ToolTips.ToolTip(ToolTipCodes.REASON_TOOLTIP);
            }
        }
    }
}
