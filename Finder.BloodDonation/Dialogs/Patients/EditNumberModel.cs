﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Tools;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Dialogs.Patients
{
    public class EditNumberModel : FinderFX.SL.Core.MVVM.ViewModelBase, IDialogWindow
    {
        public RelayCommand OKClick { get; set; }
        public RelayCommand CancelClick { get; set; }

        [RaisePropertyChanged]
        public string Title { get; set; }

        [RaisePropertyChanged]
        public string Number { get; set; }
        
        [RaisePropertyChanged]
        public string Description { get; set; }

        private PhoneNumberData _data;

        public string WindowKey
        {
            get { return WindowsKeys.EditNumber; }
        }

        public object Data
        {
            set
            {
                _data = (PhoneNumberData)value;

                if(_data.IsEdited)
                {
                    Title = "Edycja ";
                    Number = _data.PhoneNumber.Phone.ToString();
                    Description = _data.PhoneNumber.Description;
                }
                else
                {
                    Title = "Dodawanie ";
                    Number = String.Empty;
                    Description = String.Empty;
                }

                Title = Title + "numeru";
            }
        }

        public EditNumberModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            OKClick = new RelayCommand(OnOkClick);
            CancelClick = new RelayCommand(OnCancelClick);
        }

        public void OnCancelClick()
        {
            Close(CloseButton.Cancel);
        }

        public void OnOkClick()
        {
            Close(CloseButton.OK);
        }

        private void Close(CloseButton closeButton)
        {
            string msg = String.Empty;
            int phone;

            if (closeButton == CloseButton.OK)
            {
                if (Number.Equals(String.Empty))
                {
                    msg = "Nie podano numeru";
                }
                if (Description.Equals(String.Empty))
                {
                    msg = "Nie podano opisu";
                }
                if (!int.TryParse(Number, out phone))
                {
                    msg = "Numer telefonu zawiera niedozwolone znaki";
                }
                if(Number.Length <= 1)
                {
                    msg = "Numer musi zawierać przynajmniej dwie cyfry!";
                }

                if (!msg.Equals(String.Empty))
                {
                    MsgBox.Error(msg);
                    return;
                }

                SaveData();
            }

            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        private void SaveData()
        {
            _data.IsCanceled = false;

            _data.PhoneNumber.Phone = Number;
            _data.PhoneNumber.Description = Description;
        }
    }
}
