﻿using Finder.BloodDonation.Model.Dto;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataManagers;
using System.Text;
using System.Globalization;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Dialogs.Patients
{
    public static class Extension
    {
        /// <summary>
        /// Allows phone number of the format: NPA = [2-9][0-8][0-9] Nxx = [2-9]      [0-9][0-9] Station = [0-9][0-9][0-9][0-9]
        /// </summary>
        /// <param name="strPhone"></param>
        /// <returns></returns>
        public static bool IsValidUSPhoneNumber(string strPhone)
        {
            string regExPattern = @"(^[\+]){1}([0-9]{2})\-?[-]?([0-9]{3})\-?[-]?([0-9]{3})[-]\-?([0-9]{3})$";
            return MatchStringFromRegex(strPhone, regExPattern);
        }
        // Function which is used in IsValidUSPhoneNumber function
        public static bool MatchStringFromRegex(string str, string regexstr)
        {
            str = str.Trim();
            System.Text.RegularExpressions.Regex pattern = new System.Text.RegularExpressions.Regex(regexstr);
            return pattern.IsMatch(str);
        }

        public static IEnumerable<PatientDTO> GetPatientsWithPhrase(this IEnumerable<PatientDTO> patientsToCheck, string phrase, CultureInfo culture)
        {
            int numeric;

            foreach(PatientDTO patient in patientsToCheck)
            {
                    if (int.TryParse(phrase, out numeric))
                    {
                        if (patient.Pesel != null)
                        {
                            if (patient.Pesel.ToString().ToLower().Contains(phrase.ToLower()))
                                yield return patient;
                        }
                        
                        if (patient.ID != null)
                        {
                            if (patient.ID.ToString().ToLower().Contains(phrase.ToLower()))
                                yield return patient;
                        }
                        if (patient.DepartmentId != null)
                        {
                            if (patient.DepartmentId.ToString().ToLower().Contains(phrase.ToLower()))
                                yield return patient;
                        }
                        if (patient.RoomId != null)
                        {
                            if (patient.RoomId.ToString().ToLower().Contains(phrase.ToLower()))
                                yield return patient;
                        }
                        if (patient.BandId != null)
                        {
                            if (patient.BandId.ToString().ToLower().Contains(phrase.ToLower()))
                                yield return patient;
                        }
                    }
                    else
                    {
                        if (patient.FirstName != null)
                        {
                            if (patient.FirstName.ToLower().Contains(phrase.ToLower()))
                                yield return patient;
                        }
                        if (patient.LastName != null)
                        {
                            if (patient.LastName.ToLower().Contains(phrase.ToLower()))
                                yield return patient;
                        }
                        if (patient.DepartmentName != null)
                        {
                            if (patient.DepartmentName.ToLower().Contains(phrase.ToLower()))
                                yield return patient;
                        }
                        if (patient.RoomName != null)
                        {
                            if (patient.RoomName.ToLower().Contains(phrase.ToLower()))
                                yield return patient;
                        }
                    }
                }
        }

        public static IEnumerable<UnitDTO> GetRoomsForSelectedDepartment(this IEnumerable<UnitDTO> unitsToCheck, int departmentId)
        {
            UnitDTO _temp;

            foreach (UnitDTO unit in unitsToCheck)
            {
                if (unit != null)
                {
                    _temp = unit.Parent;

                    while (_temp != null && !UnitTypeOrganizer.IsHospitalDepartment(_temp.UnitTypeIDv2))
                    {
                        _temp = _temp.Parent;
                    }

                    if (_temp != null && _temp.ID == departmentId)
                    {
                        yield return unit;
                    }
                }
            }
        }

        //public static IEnumerable<UnitDTO> GetRoomsForSelectedDepartmentV2(this IEnumerable<UnitDTO> unitsToCheck, int departmentId)
        //{
        //    UnitDTO _temp;

        //    foreach(UnitDTO unit in unitsToCheck)
        //    {
        //        _temp = unit.Parent;

        //        while(_temp != null && !UnitTypeOrganizer.IsHospitalDepartment(_temp.UnitTypeIDv2))
        //        {

        //        }
        //    }
        //}

        public static string ToVarchar(this string value)
        {
            StringBuilder stringBuilder = new StringBuilder()
                .Append("'")
                .Append(value).Replace(" ", String.Empty)
                .Append("'");

            return stringBuilder.ToString();
        }

        public static string ToVarchar(this int value)
        {
            return GetStringForNotCommaValues(value.ToString());
        }

        public static string ToVarchar(this long value)
        {
            return GetStringForNotCommaValues(value.ToString());
        }

        public static string ToVarchar(this short value)
        {
            return GetStringForNotCommaValues(value.ToString());
        }

        public static string ToVarchar(this bool value)
        {
            if (value)
                return GetStringForNotCommaValues("1");
            else
                return GetStringForNotCommaValues("0");
        }

        public static string ToVarchar(this DateTime value, bool? isJustHour, bool? isConvertToUtc)
        {
            StringBuilder stringBuilder = new StringBuilder();

            if(isJustHour.Equals(false) || isJustHour.Equals(null))
            {
                if(isConvertToUtc.Equals(true))
                {
                    return stringBuilder.Append("'").Append(value.ToUniversalTime().ToString().Replace(".", "-")).Append("'").ToString();
                }
                else
                {
                    return stringBuilder.Append("'").Append(value.ToString().Replace(".", "-")).Append("'").ToString();
                }
            }
            else
            {
                int index = -1;

                if(isConvertToUtc.Equals(true))
                {
                    index = value.ToUniversalTime().ToString().IndexOf(' ');
                    return stringBuilder.Append("'").Append(value.ToUniversalTime().ToString().Remove(0, index + 1)).Append("'").ToString();
                }
                else
                {
                    index = value.ToString().IndexOf(' ');

                    return stringBuilder.Append("'").Append(value.ToString().Remove(0, index + 1)).Append("'").ToString();
                }
            }
        }

        public static string ToVarchar(this double value)
        {
            return GetStringForCommaValues(value.ToString());
        }

        public static string ToVarchar(this float value)
        {
            return GetStringForCommaValues(value.ToString());
        }

        private static string GetStringForCommaValues(string value)
        {
            StringBuilder outputBuilder = new StringBuilder();

            return outputBuilder.Append(value.ToString().Replace(",", ".")).ToString();
        }

        private static string GetStringForNotCommaValues(string value)
        {
            StringBuilder stringBuilder = new StringBuilder()
                .Append(value).Replace(" ", String.Empty);

            return stringBuilder.ToString();
        }
    }
}
