﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.UnitsService;
using Finder.Log.Collections;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;

namespace Finder.BloodDonation.Dialogs.Patients
{
    public class EditProhibitedZoneModel : FinderFX.SL.Core.MVVM.ViewModelBase, IDialogWindow
    {
        ProhibitedZoneData _data;

        [RaisePropertyChanged]
        public int SelectedDepartmentIndex { get; set; }
        [RaisePropertyChanged]
        public int SelectedRoomIndex { get; set; }

        [RaisePropertyChanged]
        public string Title { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<UnitDTO> DepartmentList { get; set; }
        [RaisePropertyChanged]
        public ObservableCollection<UnitDTO> RoomList { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<UnitDTO> AddingProhibitedZone { get; set; }

        [RaisePropertyChanged]
        public bool IsEnabled { get; set; }

        public RelayCommand OKClick { get; set; }
        public RelayCommand CancelClick { get; set; }
        public RelayCommand DepartmentSelectionChanged { get; set; }
        public RelayCommand RoomSelectionChanged { get; set; }

        private UnitsServiceClient Proxy { get; set; }

        public string WindowKey
        {
            get { return WindowsKeys.EditProhibitedZoneModel; }
        }

        public object Data
        {
            set 
            {
                _data = (ProhibitedZoneData)value;
                GetDepartmentData();
                AddingProhibitedZone = new ObservableCollection<UnitDTO>();
                IsEnabled = true;

                Title = "Wyznaczanie stref zakazanych";
            }
        }

        public EditProhibitedZoneModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            OKClick = new RelayCommand(OnOkClick);
            CancelClick = new RelayCommand(OnCancelClick);
            
            DepartmentSelectionChanged = new RelayCommand(OnDepartmentSelectionChanged);
            RoomSelectionChanged = new RelayCommand(OnRoomSelectionChanged);

            Proxy = ServiceFactory.GetService<UnitsServiceClient>();
            Proxy.GetUnitsWithTransmitterCompleted += Proxy_GetUnitsWithTransmitterCompleted;

            GetDepartmentData();

            if (AddingProhibitedZone != null)
                AddingProhibitedZone = new ObservableCollection<UnitDTO>();
        }

        private List<UnitDTO> unitsWithTransmiter;

        public void Proxy_GetUnitsWithTransmitterCompleted(object sender, GetUnitsWithTransmitterCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                unitsWithTransmiter = e.Result;
                GetRoomList();
            }
        }

        public void OnRoomSelectionChanged()
        {
            AddProhibitedZone();
        }

        public void GetDepartmentData()
        {
            var _temp = UnitManager.Instance.Units;

            if (DepartmentList == null)
            {
                DepartmentList = new ObservableCollection<UnitDTO>();
                DepartmentList.Add(new UnitDTO() { Name = "Brak" });

                foreach (UnitDTO unit in _temp)
                {
                    if (unit.UnitTypeIDv2 == HOSPITAL_DEPARTMENT)
                    {
                        DepartmentList.Add(unit);
                    }
                }
            }
        }

        public const int HOSPITAL_DEPARTMENT = 29;

        private void GetRoomList()
        {
            if (unitsWithTransmiter != null)
            {
                var _temp = UnitManager.Instance.Units;
                // Określa czy pomieszczenie zostało już dodane.
                bool isUsed = false;

                RoomList = new ObservableCollection<UnitDTO>();
                RoomList.Add(new UnitDTO() { Name = "Brak" });

                foreach (UnitDTO item in unitsWithTransmiter)
                {
                    if (item.UnitTypeIDv2 == 3)
                    {
                        UnitDTO _tempUnit = _temp.ToList().FirstOrDefault(x => x.ID == item.ID);
                        var _tmpItem = _temp.ToList().FirstOrDefault(x => x.ID == item.ID);
                        //UnitDTO _tempUnit = item;

                        
                            while (_tempUnit != null && _tempUnit.UnitTypeIDv2 != HOSPITAL_DEPARTMENT)
                            {
                                _tempUnit = _tempUnit.Parent;
                            }
                            if (_tempUnit != null)
                            {
                                if (_tempUnit.Name.Equals(DepartmentList[SelectedDepartmentIndex].Name))
                                {
                                    // Sprawdzenie czy pomieszczenie nie jest już obecne w liście.
                                    for (int i = 0; i < AddingProhibitedZone.Count; i++)
                                    {
                                        if (_tmpItem.ID == AddingProhibitedZone[i].ID)
                                        {
                                            isUsed = true;
                                        }
                                    }
                                    if (!isUsed)
                                        RoomList.Add(_tmpItem);

                                    isUsed = false;
                                }
                            }
                    }
                }
            }
        }

        public void AddProhibitedZone()
        {
            //if (AddingProhibitedZone == null)
                AddingProhibitedZone = new ObservableCollection<UnitDTO>();

            if (AddingProhibitedZone.Count <= 0)
            {
                if (SelectedRoomIndex < 1 || SelectedRoomIndex >= RoomList.Count)
                    return;

                for (int i = 0; i < _data.ProhibitedZone.Count; i++)
                {
                    if (_data.ProhibitedZone[i].Equals(RoomList[SelectedRoomIndex]))
                        return;
                }

                Messenger msg = new Messenger();
                msg.Send<ObservableCollection<UnitDTO>>(RoomList);
                AddingProhibitedZone.Add(RoomList[SelectedRoomIndex]);
                IsEnabled = false;
                //GetRoomList();
            }
        }

        public void OnDepartmentSelectionChanged()
        {
            Proxy.GetUnitsWithTransmitterAsync();
            //GetRoomList();
        }

        public void OnCancelClick()
        {
            Close(CloseButton.Cancel);
        }

        public void OnOkClick()
        {
            Close(CloseButton.OK);
        }

        private void Close(CloseButton close)
        {
            if(close.Equals(CloseButton.OK))
            {
                SaveData();
            }

            GetDepartmentData();
            GetRoomList();
            SelectedDepartmentIndex = 0;
            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        private void SaveData()
        {
            _data.IsCanceled = false;

            if(_data.ProhibitedZone == null)
            {
                _data.ProhibitedZone = new ObservableCollection<UnitDTO>();
            }
            else
            {
                for(int i = 0; i < _data.ProhibitedZone.Count; i++)
                {
                    AddingProhibitedZone.Add(_data.ProhibitedZone[i]);
                }
            }
            _data.ProhibitedZone = AddingProhibitedZone;
        }
    }
}
