﻿using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Tools;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.UnitsService;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.LanguageHelpher;
using Finder.BloodDonation.Common.Authentication;

namespace Finder.BloodDonation.Dialogs.Patients
{
    [OnCompleted]
    [UIException]
    public class AssignBandModel : ViewModelBase, IDialogWindow
    {
        public RelayCommand SaveActionCommand { get; set; }
        public RelayCommand CancelActionCommand { get; set; }

        private IUnit SelectedUnit = null;

        private AssignBandData _data;

        private UnitsServiceClient Proxy { get; set; }
        
        private IWindowsManager _windowsManager;


        [RaisePropertyChanged]
        public string Code { get; set; }


        [RaisePropertyChanged]
        public string Reason { get; set; }

        [RaisePropertyChanged]
        public string Title { get; set; }

        [RaisePropertyChanged]
        public string BandBarCodeLabel 
        {
            get
            {
                return LanguageManager.ControlsText.Text(ControlsTextCodes.BAND_BARCODE_LABEL);
            }
        }

        [RaisePropertyChanged]
        public string BandBarCodeToolTip
        {
            get
            {
                return LanguageManager.ToolTips.ToolTip(ToolTipCodes.BAND_BARCODE_TOOLTIP);
            }
        }

        [RaisePropertyChanged]
        public string AddTextButton
        {
            get
            {
                return LanguageManager.ControlsText.Text(ControlsTextCodes.ADDING_BUTTON);
            }
        }

        [RaisePropertyChanged]
        public string CancelTextButton
        {
            get
            {
                return LanguageManager.ControlsText.Text(ControlsTextCodes.CANCEL_BUTTON);
            }
        }

        [RaisePropertyChanged]
        public string ReasonLabel
        {
            get
            {
                return LanguageManager.ControlsText.Text(ControlsTextCodes.REASON);
            }
        }

        public void Refresh(IUnit unit)
        {
            SelectedUnit = unit;
        }

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }


        public bool ExitButtonVisible
        {
            get { return false; }
        }

        public string WindowKey
        {
            get { return WindowsKeys.AssignBand; }
        }

        public AssignBandModel(IUnityContainer container)
            : base(container)
        {
            SaveActionCommand = new RelayCommand(OnSaveCommand);
            CancelActionCommand = new RelayCommand(OnCancelCommand);

            Proxy = ServiceFactory.GetService<UnitsServiceClient>();
            Proxy.AssignBandToObjectCompleted += Proxy_AssignBandToObjectCompleted;

            var viewFactory = container.Resolve<ViewFactory>();
            _windowsManager = this.Container.Resolve<IWindowsManager>();
            _windowsManager.RegisterWindowIfNotRegister(WindowsKeys.SuccessDialog, "Powodzenie", viewFactory.CreateView<Finder.BloodDonation.Dialogs.Success.Success>(), new Size(440, 170), true);
        }

        void Proxy_AssignBandToObjectCompleted(object sender, AssignBandToObjectCompletedEventArgs e)
        {
            // TODO: Obsługa przypisania
            if (e.Result.Equals(_data.AssignedBandInfo.ObjectId.ToString()))
            {
                _windowsManager.ShowWindow(WindowsKeys.SuccessDialog,LanguageManager.Messages.Message(MessageCodes.SUCCESFULL_ASSIGNED_BAND_TO_PATIENT));
                //MsgBox.Warning(LanguageManager.Messages.Message(MessageCodes.SUCCESFULL_ASSIGNED_BAND_TO_PATIENT));
                Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
            }
            else
            {
                MsgBox.Confirm(e.Result + Environment.NewLine + Environment.NewLine + "Czy chcesz wymusić przypisanie mimo wszystko?", AssignBandToObjectPrompt, MsgBox.Buttons.YesNo);
                //MsgBox.Warning(e.Result);
            }
        }

        void AssignBandToObjectPrompt(object sender, Telerik.Windows.Controls.WindowClosedEventArgs e)
        {
            if (e.DialogResult == true)
                SendValueFromModelToData(true);
        }

        public void OnCancelCommand()
        {
            Close(CloseButton.Cancel);
        }

        public void OnSaveCommand()
        {
            Close(CloseButton.OK);
        }

        /// <summary>
        /// This method show error if message is not empty.
        /// </summary>
        /// <param name="msg">Error message.</param>
        /// <returns>True, if error message is showed.</returns>
        private bool CheckAndExecutedMessageIfNotEmpty(string msg)
        {
            if (!String.IsNullOrEmpty(msg))
            {
                return false;
            }
            else
            {
                MessageBox.Show(msg);
                return true;
            }
        }

        private string CheckIfBarCodeAndReasonIsCorrectAndGetMessage()
        {
            return String.IsNullOrEmpty(Code) ? LanguageManager.Errors.Error(ErrorCodes.EMPTY_BAND_BARCODE) : String.Empty;
        }

        public object Data
        {
            set
            {
                _data = (AssignBandData)value;

                Title = LanguageManager.Messages.Message(MessageCodes.ASSIGN_BAND_TO_PATIENT) + " " + _data.PatientName;
                Code = String.Empty;
                Reason = String.Empty;
            }
        }

        #region Closed Helphers Method
        private void Close(CloseButton closeAction)
        {
            if (closeAction.Equals(CloseButton.OK))
            {
                if (Code.Equals(String.Empty))
                    MsgBox.Warning(LanguageManager.Errors.Error(ErrorCodes.NON_ENTERED_BAND_BARCODE));
                else
                    SendValueFromModelToData();
            }
            else
            {
                Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
            }
        }
        private void SendValueFromModelToData(bool force = false)
        {
            _data.IsClosed = false;
            int barCode;

            _data.AssignedBandInfo.BarCode = Code;
            _data.AssignedBandInfo.Reason = Reason;

            Proxy.AssignBandToObjectAsync(new Model.Dto.AssignedBandInfoDto() { BandId = null, BarCode = Code, ObjectId = _data.AssignedBandInfo.ObjectId, ObjectType = 4, OperatorId = BloodyUser.Current.ID, Reason = Reason }, force);
        }

        private bool ExecuteErrorMessageIfNotEmpty(string message)
        {
            if (!String.IsNullOrEmpty(message))
            {
                MsgBox.Error(message);
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}
