﻿using Finder.BloodDonation.Model.Dto;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Dialogs.Patients
{
    public class ProhibitedZoneData
    {
        public ObservableCollection<UnitDTO> ProhibitedZone { get; set; }

        public bool IsCanceled { get; set; }

        public ProhibitedZoneData()
        {
            IsCanceled = true;
        }
    }
}
