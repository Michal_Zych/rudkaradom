﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;

namespace Finder.BloodDonation.Dialogs.Patients
{
    [ViewModel(typeof(AssignBandModel))]
    public partial class AssignBand : UserControl
    {
        public AssignBand()
        {
            InitializeComponent();
        }
    }
}
