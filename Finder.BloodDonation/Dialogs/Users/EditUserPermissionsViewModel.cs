﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.PermissionsProxy;
using FinderFX.SL.Core.Extensions;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using Telerik.Windows.Controls;
using ViewModelBase = FinderFX.SL.Core.MVVM.ViewModelBase;
using System.Windows.Threading;
using Finder.BloodDonation.Helpers;
using System.Windows;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;

namespace Finder.BloodDonation.Dialogs.Users
{
    public class EditUserPermissionsViewModel : ViewModelBase, IDialogWindow//IDialogWindow<EditUserPermissionsInitializationData>
    {
        private EditUserPermissionsInitializationData _data;
        private List<int> _inputPermissionsUnitID;

        public EditUserPermissionsViewModel(IUnityContainer unityContainer) : base(unityContainer)
        {
            SelectedUnits = new List<UnitDTO>();
            SaveClick = new RelayCommand(SaveClickEvent);
            CancelClick = new RelayCommand(CancelClickEvent);
            Unit = new ObservableCollection<UnitDTO>();
            Unit.Add(Container.Resolve<UnitViewModel>("Root").Unit);

            this.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(EditUserPermissionsViewModel_PropertyChanged);
        }

        void EditUserPermissionsViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "UserCurrentRole":
                    break;
                default:
                    break;
            }
        }
        
        [RaisePropertyChanged]
        public ObservableCollection<DBRoleDTO> AvailableRoles { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<UnitDTO> Unit { get; set; }

        [RaisePropertyChanged]
        public string UserName { get; set; }

        [RaisePropertyChanged]
        public bool StatusPanelEnabled { get; set; }
            
        [RaisePropertyChanged]
        public DBRoleDTO UserCurrentRole { get; set; }

        public RelayCommand SaveClick { get; set; }

        public RelayCommand CancelClick { get; set; }


        private List<UnitDTO> SelectedUnits { get; set; }

        [RaisePropertyChanged]
        public string Reason { get; set; }

        [RaisePropertyChanged]
        public Visibility IsReasonVisible { get; set; }


        private PermissionsServiceClient PermissionsService
        {
            get
            {
                var proxy =
                    GetService<PermissionsServiceClient>(ServicesUri.PermissionsService);
                return proxy;
            }
        }

        #region IDialogWindow<EditUserPermissionsInitializationData> Members

        public string WindowKey
        {
            get { return WindowsKeys.EditUserPermissions; }
        }

        object IDialogWindow.Data
        {
            set
            {
                _data = (EditUserPermissionsInitializationData)value;
                AvailableRoles = _data.AvailableRoles.ToObservableCollection();
                //
                Unit = new ObservableCollection<UnitDTO>() {_data.Unit};
                Unit[0].Parent = null; //najwzszy unit nie powinien miec parrenta
                Unit[0].SetAllIsSelectedProperty(false);
                //
                UserName = string.Format("{0} {1} {2}", _data.Account.Name, _data.Account.LastName, _data.Account.Login);

                //
                UserCurrentRole = AvailableRoles.FirstOrDefault(r => r.ID == _data.CurrentRole.ID);

                //
                _inputPermissionsUnitID = _data.CurrentPermissionsUnitIDs;
                //Unit[0].Children[0].Children[0].IsSelected = true;
                

                //
                Reason = "";
                foreach (int id in _inputPermissionsUnitID)
                {
                    UnitDTO u = Unit[0].FindUnit(id);
                    if (u != null)
                        u.IsSelected = true;
                }

                if (Global.Settings.Current.GetInt(ConfigurationKeys.ShowReasonField) == ConfigurationKeys.ENABLED)
                {
                    IsReasonVisible = Visibility.Visible;
                }
                else
                {
                    IsReasonVisible = Visibility.Collapsed;
                }


                //UnitDTO unitToExpand = Unit[0].FindUnit(_data.SelectedUnit.ID);
                //EventAggregator.GetEvent<ExpandTreeInEditPermissionWindow>().Publish(unitToExpand.GetPath("|"));
            }
        }

        #endregion


        

        private void SaveClickEvent()
        {
            SelectedUnits = new List<UnitDTO>();
            GetSelectedUnits(Unit[0]);

            var unitsToRequest = new List<UnitDTO>();
            foreach (UnitDTO selectedUnit in SelectedUnits)
            {
                if (!selectedUnit.AnyParrentSelected())
                {
                    unitsToRequest.Add(selectedUnit);
                }
            }


            string logInfo = "".LogEdit("Rola", _data.CurrentRole.Name, _data.CurrentRole.ID, UserCurrentRole.Name, UserCurrentRole.ID)
                                .LogEdit("Dostępne JO", GetListForLog(_data.CurrentPermissionsUnitIDs), GetListForLog(unitsToRequest.Select(u => u.ID).ToList()));

                 



            if (unitsToRequest.Any())
            {
                PermissionsService.EditUserPermissionsAsync(new RequestSetUserPermissions
                {
                    RoleId = UserCurrentRole.ID,
                    Units = unitsToRequest.Select(u => u.ID).ToList(),
                    UserId = _data.Account.ID,
                }, Reason, logInfo);
                Close();
            }
            else
            {
                MsgBox.Warning("Użytkownk musi posiadać uprawnienia");
            }
        }

        private string GetListForLog(List<int> list)
        {
            string result = "";
            foreach (var i in list)
            {
                result = result +
                    ((result == "") ? GetUnitLog(i) : ", " + GetUnitLog(i));
            }
            return "{" + result + "}";
        }

        private string GetUnitLog(int i)
        {
            var u = Unit[0].FindUnit(i);
            return u.Name + "<ID: " + i.ToString() + ">";
        }

        private void GetSelectedUnits(UnitDTO unit)
        {
            if (unit.IsSelected.HasValue && unit.IsSelected.Value)
                SelectedUnits.Add(unit);

            if (unit.Children != null)
            {
                foreach (UnitDTO unitDto in unit.Children)
                {
                    GetSelectedUnits(unitDto);
                }
            }
        }

        private void CancelClickEvent()
        {
            Close();
        }

        private void Close()
        {
            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }
    }

    public class EditUserPermissionsInitializationData
    {
        public List<DBRoleDTO> AvailableRoles { get; set; }
        public DBRoleDTO CurrentRole { get; set; }

        public AccountDto Account { get; set; }
        public UnitDTO Unit { get; set; }
        public List<int> CurrentPermissionsUnitIDs { get; set; }
    }
}