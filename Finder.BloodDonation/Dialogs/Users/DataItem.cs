﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Data;

namespace Finder.BloodDonation.Dialogs.Users
{
    [ContentProperty("Children")]
    public class DataItem
    {
        public DataItem()
        {
            Children = new DataItemCollection();
        }

        public string Text { get; set; }

        public DataItemCollection Children { get; private set; }
    }
}
