﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Model.Authentication;
using FinderFX.SL.Core.Authentication;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Telerik.Windows.Controls;

namespace Finder.BloodDonation.Dialogs.Users
{
    [ViewModel(typeof(EditUserPermissionsViewModel))]
    public partial class EditUserPermissionsView : UserControl, IReuseWindow
    {
        private IDynamicEventAggregator eventAggregator;

        public EditUserPermissionsView(IUnityContainer container)
        {
            InitializeComponent();
            var c = User.Current.RfidStatuses;
        }

        private void RadComboBox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            RadTreeView tree = e.OriginalSource as RadTreeView;
            RadComboBox combo = sender as RadComboBox;

            if ((tree == null) || (tree.SelectedItem == null) || (combo == null))
            {
                return;
            }

            combo.SelectedItem = (tree.SelectedItem as DataItem).Text;
            combo.IsDropDownOpen = false;
        }
        private bool _isEditMode;
        public bool isEditMode
        {
            get
            {
                return this._isEditMode;
            }
            set
            {
                this._isEditMode = false;
            }
        }
    }
}
