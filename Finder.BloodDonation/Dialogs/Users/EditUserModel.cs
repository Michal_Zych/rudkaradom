﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Converters;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Tools;
using FinderFX.SL.Core.Authentication;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using ViewModelBase = FinderFX.SL.Core.MVVM.ViewModelBase;
using Global = Finder.BloodDonation.Settings;

namespace Finder.BloodDonation.Dialogs.Users
{
    public class EditUserModel : ViewModelBase, IDialogWindow
    {
        private UserEditData _data;

        [RaisePropertyChanged]
        public string Title { get; set; }

        [RaisePropertyChanged]
        public string Login { get; set; }

        [RaisePropertyChanged]
        public string Password { get; set; }

        [RaisePropertyChanged]
        public string Name { get; set; }

        [RaisePropertyChanged]
        public string LastName { get; set; }

        [RaisePropertyChanged]
        public string Phone { get; set; }

        [RaisePropertyChanged]
        public string Email { get; set; }

        [RaisePropertyChanged]
        public string Reason { get; set; }

        [RaisePropertyChanged]
        public Visibility IsReasonVisible { get; set; }

        [RaisePropertyChanged]
        public string ImageSource { get; set; }

        public RelayCommand OKClick { get; set; }

        public RelayCommand CancelClick { get; set; }


        public EditUserModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            OKClick = new RelayCommand(OnOKClick);
            CancelClick = new RelayCommand(OnCancelClick);

        }

        public string WindowKey
        {
            get { return WindowsKeys.EditUser; }
        }


        private bool _isEdited;

        private string _password;
        private static string PASSWORD_MASK = "●●●●●";
        object IDialogWindow.Data
        {
            set
            {
                _data = (UserEditData)value;
                _isEdited = !string.IsNullOrEmpty(_data.Account.Login);

                string title;
                if (_isEdited)
                {
                    title = "Edycja";
                    ImageSource = "/Finder.BloodDonation;component/Themes/Default/user_edit.png";
                }
                else
                {
                    title = "Tworzenie";
                    ImageSource = "/Finder.BloodDonation;component/Themes/Default/user_add.png";
                }

                Title = title + " użytkownika";

                Login = _data.Account.Login;
                _password = _data.Account.Password;
                Password = PASSWORD_MASK;
                Name = _data.Account.Name;
                LastName = _data.Account.LastName;
                Phone = _data.Account.Phone;
                Email = _data.Account.EMail;
                Reason = "";

                if (Global.Settings.Current.GetInt(ConfigurationKeys.ShowReasonField) == ConfigurationKeys.ENABLED)
                {
                    IsReasonVisible = Visibility.Visible;
                }
                else
                {
                    IsReasonVisible = Visibility.Collapsed;
                }
            }
        }


        private void OnOKClick()
        {
            Close(CloseButton.OK);
        }
        private void OnCancelClick()
        {
            Close(CloseButton.Cancel);
        }

        private void Close(CloseButton button)
        {
            if (button == CloseButton.OK)
            {
                string errorMsg = "";
                if (String.IsNullOrWhiteSpace(LastName))
                {
                    errorMsg = "Pole nazwisko nie może być puste.";
                }
                if (string.IsNullOrWhiteSpace(Name))
                {
                    errorMsg = "Pole imię nie może być puste.";
                }
                if (string.IsNullOrWhiteSpace(Password) || (Password == PASSWORD_MASK && string.IsNullOrEmpty(_password)))
                {
                    errorMsg = "Pole hasło nie może być puste.";
                }
                if (string.IsNullOrWhiteSpace(Login))
                {
                    errorMsg = "Pole login nie może być puste.";
                }
                if (errorMsg != "")
                {
                    MsgBox.Error(errorMsg);
                    return;
                }

                SaveData();
            }

            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        private void SaveData()
        {
            _data.Canceled = false;

            if (_isEdited)
            {
                _data.LogInfo = "".LogEdit("Login", _data.Account.Login, Login)
                    .LogEdit("Imię", _data.Account.Name, Name)
                    .LogEdit("Nazwisko", _data.Account.LastName, LastName)
                    .LogEdit("Telefon", _data.Account.Phone, Phone)
                    .LogEdit("Email", _data.Account.EMail, Email);

                if (Password != PASSWORD_MASK)
                    _data.LogInfo = _data.LogInfo.LogPasswordChanged();
            }
            else
            {
                _data.LogInfo = "".LogCreate("Login", Login)
                    .LogCreate("Imię", Name)
                    .LogCreate("Nazwisko", LastName)
                    .LogCreate("Telefon", Phone)
                    .LogCreate("Email", Email);
            }
            _data.Account.Login = Login;
            if (Password != PASSWORD_MASK)
            {
                _data.Account.Password = HashHelper.CalculateHash(Password);
            }
            _data.Account.Name = Name;
            _data.Account.LastName = LastName;
            _data.Account.Phone = Phone;
            _data.Account.EMail = Email;
            _data.Reason = Reason;
        }
    }
}
