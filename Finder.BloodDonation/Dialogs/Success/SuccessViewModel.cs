﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Dialogs.Success
{
    public class SuccessViewModel : FinderFX.SL.Core.MVVM.ViewModelBase, IDialogWindow
    {
        private const string DEFAULT_TEXT = "Wykonano pomyślnie!";

        [RaisePropertyChanged]
        public string SuccessTextToDisplay { get; set; }

        public RelayCommand CloseWindowCommand { get; set; }

        public string WindowKey
        {
            get { return WindowsKeys.SuccessDialog; }
        }

        public object Data
        {
            set { 
                if(value != null)
                {
                    SuccessTextToDisplay = (string)value;
                }
            }
        }

        public SuccessViewModel(IUnityContainer container)
            : base(container)
        {
            SuccessTextToDisplay = DEFAULT_TEXT;
            CloseWindowCommand = new RelayCommand(OnCloseWindow);
        }

        public void OnCloseWindow()
        {
            SuccessTextToDisplay = DEFAULT_TEXT;
            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }
    }
}
