﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Dialogs.Bands
{
    public class BarCodeViewModel : ViewModelBase, IDialogWindow
    {
        BarCodeEditData data;

        [RaisePropertyChanged]
        public int BandId { get; set; }

        [RaisePropertyChanged]
        public string BandMac { get; set; }

        [RaisePropertyChanged]
        public string BarCode { get; set; }

        public RelayCommand OkClick { get; set; }
        public RelayCommand CancelClick { get; set; }

        public BarCodeViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            OkClick = new RelayCommand(OnOkClick);
            CancelClick = new RelayCommand(OnCancelClick);
        }

        private void OnOkClick()
        {
            throw new NotImplementedException();
        }

        private void OnCancelClick()
        {
            Container.Resolve<IWindowsManager>().CloseWindow(WindowsKeys.BarCodeView);
        }

        public string WindowKey
        {
            get { return WindowsKeys.BarCodeView; }
        }

        public object Data
        {
            set 
            {
                data = (BarCodeEditData)value;

                ClearFields();

                if (data == null)
                    SetDataToField();
            }
        }

        private void SetDataToField()
        {
            BandId = data.BandId;
            BandMac = data.BandMac;
            BarCode = data.BarCode;
        }

        private void ClearFields()
        {
            BandId = -1;
            BandMac = String.Empty;
            BarCode = String.Empty;
        }
    }
}
