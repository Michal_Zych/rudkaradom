﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;

namespace Finder.BloodDonation.Dialogs.Bands
{
    public class BatteryExchangeViewModel : ViewModelBase, IDialogWindow
    {
        private BatteryExchangeData data;

        public RelayCommand OkClick { get; set; }
        public RelayCommand CancelClick { get; set; }

        [RaisePropertyChanged]
        public string BatteryVlotage { get; set; }

        [RaisePropertyChanged]
        public string BatteryLifeMounts { get; set; }

        [RaisePropertyChanged]
        public string BatteryCapacity { get; set; }

        [RaisePropertyChanged]
        public string BatteryId { get; set; }

        [RaisePropertyChanged]
        public string BatteryName { get; set; }

        private int batteryTypesSelectedIndex;

        [RaisePropertyChanged]
        public int BatteryTypesSelectedIndex
        {
            get
            {
                return batteryTypesSelectedIndex;
            }
            set
            {
                const string SPACE = " ";
                const string EMPTY = "-";

                if (BatteryType != null)
                {
                    if (value >= 0 && value < BatteryType.Count)
                    {
                        BatteryVlotage = BatteryType[value].Voltage.ToString() + SPACE + "V";
                        BatteryId = BatteryType[value].Id.ToString();
                        BatteryLifeMounts = BatteryType[value].LifeMonts + SPACE + "miesięcy";
                        BatteryCapacity = BatteryType[value].Capacity + SPACE + "mAh";
                        BatteryName = BatteryType[value].Name;
                    }
                }
                else
                {
                    BatteryVlotage = EMPTY;
                    BatteryId = EMPTY;
                    BatteryLifeMounts = EMPTY;
                    BatteryCapacity = EMPTY;
                    BatteryName = EMPTY;
                }

                batteryTypesSelectedIndex = value;
            }
        }

        public List<BatteryTypeDTO> BatteryType { get; set; }

        private List<int> batteryIndex;

        private UnitsServiceClient Proxy { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<string> BatteryTypes { get; set; }

        public BatteryExchangeViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            OkClick = new RelayCommand(OnOkClick);
            CancelClick = new RelayCommand(OnCancelClick);

            Proxy = ServiceFactory.GetService<UnitsServiceClient>();

            Proxy.GetBatteryTypesCompleted += proxy_GetBatteryTypesCompleted;
            Proxy.GetBatteryTypesAsync();
        }

        void proxy_GetBatteryTypesCompleted(object sender, GetBatteryTypesCompletedEventArgs e)
        {
            BatteryType = new List<BatteryTypeDTO>();
            BatteryTypeDTO dto;

            BatteryTypes = new ObservableCollection<string>();
            batteryIndex = new List<int>();

            if (e.Result != null)
            {
                for (int i = 0; i < e.Result.Count; i++)
                {
                    BatteryTypes.Add(e.Result[i].Name);
                    batteryIndex.Add(e.Result[i].Id);

                    dto = new BatteryTypeDTO()
                    {
                        Id = e.Result[i].Id,
                        Name = e.Result[i].Name,
                        Voltage = e.Result[i].Voltage,
                        Description = e.Result[i].Description,
                        LifeMonts = e.Result[i].LifeMonts,
                        Capacity = e.Result[i].Capacity
                    };

                    BatteryType.Add(dto);
                }
            }
        }

        private void OnCancelClick()
        {
            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        private void OnOkClick()
        {
            throw new NotImplementedException();
        }

        public string WindowKey
        {
            get { return WindowsKeys.BatteryExchange; }
        }

        public object Data
        {
            set 
            {
                data = (BatteryExchangeData)value;

                ClearFields();

                if (data == null)
                    SetDataToFields();
            }
        }

        private void SetDataToFields()
        {
            BatteryTypesSelectedIndex = BatteryType.IndexOf(BatteryType.FirstOrDefault(e => e.Id == data.ActualBatteryId));
        }

        private void ClearFields()
        {
            BatteryTypesSelectedIndex = -1;
        }

        private void SaveData()
        {
            if(data != null)
            {

            }
        }
    }
}
