﻿using Finder.BloodDonation.Dialogs.Patients;
using Finder.BloodDonation.Model.Dto;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Dialogs.Bands
{
    public static class Clipboard
    {
        public static int BateryTypeSelectedIndex { get; set; }
        public static BandDTO BandClipboard { get; set; }

        public static UnitDTO Unit { get; set; }

        public static PatientConfigurationDTO PatientConfigurationClipboard {get; set;}
        public static LocalizationConfigDTO LocalizationConfigurationClipboard { get; set; }
    }
}
