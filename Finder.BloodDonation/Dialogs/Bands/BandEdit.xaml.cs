﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;


namespace Finder.BloodDonation.Dialogs.Bands
{
    [ViewModel(typeof(BandEditModel))]
    public partial class BandEdit : UserControl
    {

        public BandEdit()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SaveBtn.Focus();
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            scrollView.ScrollToTop();
        }
    }
}
