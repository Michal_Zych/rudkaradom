﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Dialogs.Bands
{
    public class ParameterEditData
    {
        public BandParameter BandParameter  { get; set; }

        public bool isEdited = false;
        public bool Canceled = true;
    }
}
