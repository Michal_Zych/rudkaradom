﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Events;
using System.Collections.Generic;
using System.Collections;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Dialogs.Base;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.Dialogs.Helpers;
using Microsoft.Practices.Composite.Presentation.Events;
using Finder.BloodDonation.UnitsService;
using Telerik.Windows.Controls;
using Finder.BloodDonation.Dialogs.Tree;
using Finder.BloodDonation.Debugs;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.Tabs.MenuAdministration.SettingsSystem;
using Finder.BloodDonation.Helpers;

namespace Finder.BloodDonation.Dialogs.Bands
{
    [OnCompleted]
    [UIException]
    public class BandEditModel : FinderFX.SL.Core.MVVM.ViewModelBase, IDialogWindow
    {
        public event EventHandler LoadingCompleted = delegate { };

        private string[] VIEW_TITLE = { "Dodawanie lokalizatora", "Edycja lokalizatora", "Szczegóły lokalizatora" };

        private List<string> SliderTextInformation;

        private BandParameter _parameterData;

        private BandEditData _data;

        private TreeUnitViewData treeData;

        private ParameterEditData _parameterEditData;

        public bool isDebugReasonVisibility;
        [RaisePropertyChanged]
        public bool IsDebugReasonVisibility
        {
            get
            {
                return isDebugReasonVisibility;
            }
            set
            {
                #if DEBUG
                    if(value){
                        IsReasonVisibility = Visibility.Visible;
                        MinWidthDescription = 740d;
                    }
                    else {
                        IsReasonVisibility = Visibility.Collapsed;
                        MinWidthDescription = 1570d;
                    }
                #endif
                    isDebugReasonVisibility = value;
            }
        }

        [RaisePropertyChanged]
        public Visibility IsDebug { get; set; }

        private int batteryTypesSelectedIndex;

        [RaisePropertyChanged]
        public string BandLocation { get; set; }

        [RaisePropertyChanged]
        public int BatteryTypesSelectedIndex
        {
            get
            {
                return batteryTypesSelectedIndex;
            }
            set
            {
                const string SPACE = " ";
                const string EMPTY = "-";

                if (BatteryType != null)
                {
                    if (value >= 0 && value < BatteryType.Count)
                    {
                        BatteryVlotage = BatteryType[value].Voltage.ToString() + SPACE + "V";
                        BatteryId = BatteryType[value].Id.ToString();
                        BatteryLifeMounts = BatteryType[value].LifeMonts + SPACE + "miesięcy";
                        BatteryCapacity = BatteryType[value].Capacity + SPACE + "mAh";
                        BatteryName = BatteryType[value].Name;
                    }
                }
                else
                {
                    BatteryVlotage = EMPTY;
                    BatteryId = EMPTY;
                    BatteryLifeMounts = EMPTY;
                    BatteryCapacity = EMPTY;
                    BatteryName = EMPTY;
                }

                batteryTypesSelectedIndex = value;
                NotifyPropertyChange("BatteryTypesSelectedIndex");
            }
        }

        [RaisePropertyChanged]
        public string BatteryVlotage { get; set; }

        [RaisePropertyChanged]
        public string BatteryLifeMounts { get; set; }

        [RaisePropertyChanged]
        public string BatteryCapacity { get; set; }

        [RaisePropertyChanged]
        public string BatteryId { get; set; }

        [RaisePropertyChanged]
        public string BatteryName { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<string> BatteryTypes { get; set; }

        [RaisePropertyChanged]
        public int DescriptionColumnSpan { get; set; }

        private void ViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        public string Title
        {
            get { return ""; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }

        [RaisePropertyChanged]
        public int SelectedPropertyIndex { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<BandParameter> ParametersList { get; set; }

        [RaisePropertyChanged]
        public BandViewType ViewType { get; set; }

        [RaisePropertyChanged]
        public string BandTitle { get; set; }

        [RaisePropertyChanged]
        public string AssignedTo { get; set; }

        [RaisePropertyChanged]
        public bool IdBandVisible { get; set; }

        [RaisePropertyChanged]
        public bool IsMacEnabled { get; set; }

        [RaisePropertyChanged]
        public string IdBand { get; set; }

        [RaisePropertyChanged]
        public string MAC { get; set; }

        [RaisePropertyChanged]
        public string BarCode { get; set; }

        [RaisePropertyChanged]
        public string SerialNumber { get; set; }

        [RaisePropertyChanged]
        public string UidNfc { get; set; }

        [RaisePropertyChanged]
        public bool EnableState { get; set; }

        [RaisePropertyChanged]
        public SliderConfigurator TransmissionConfiguration { get; set; }

        [RaisePropertyChanged]
        public SliderConfigurator BroadcastingIntervaleConfiguration { get; set; }

        [RaisePropertyChanged]
        public SliderConfigurator SensitivityConfiguration { get; set; }

        [RaisePropertyChanged]
        public SliderConfigurator AccConfiguration { get; set; }

        [RaisePropertyChanged]
        public string OnSensitivityRangeControl { get; set; }

        [RaisePropertyChanged]
        public double VisableState { get; set; }

        [RaisePropertyChanged]
        public string ParameterKey { get; set; }

        [RaisePropertyChanged]
        public string ParameterValue { get; set; }

        [RaisePropertyChanged]
        public string Reason { get; set; }

        [RaisePropertyChanged]
        public Thickness DefaultMarginOffset { get; set; }

        [RaisePropertyChanged]
        public Visibility IsReasonVisibility { get; set; }

        [RaisePropertyChanged]
        public string Description { get; set; }

        [RaisePropertyChanged]
        public double MinWidthDescription { get; set; }

        public RelayCommand TransmissionPowerChangeCommand { get; set; }
        public RelayCommand ACCRangeChangeCommand { get; set; }
        public RelayCommand SensitivityRangeChangeCommand { get; set; }
        public RelayCommand BroadcastingIntervalRangeTextChangedCommand { get; set; }

        public RelayCommand CopyToClipboard { get; set; }
        public RelayCommand PasteFromClipboard { get; set; }
        public RelayCommand PasteDefaultData { get; set; }

        public RelayCommand CreateParameterCommand { get; set; }
        public RelayCommand EditSelectedParameterCommand { get; set; }
        public RelayCommand DeleteParameterCommand { get; set; }

        public RelayCommand CancelActionCommand { get; set; }
        public RelayCommand SaveActionCommand { get; set; }

        public RelayCommand AssignedToClickCommand { get; set; }

        public List<BatteryTypeDTO> BatteryType { get; set; }

        private UnitsServiceClient Proxy { get; set; }

        private IUnityContainer _container;

        private List<int> batteryIndex;

        private readonly IWindowsManager _windowsManager;

        /// <summary>
        /// Typ akcji wykonywanej na opasce ( Dodawanie / Edycja / Szczegóły ).
        /// </summary>
        [RaisePropertyChanged]
        public string ActionType { get; set; }

        private void InitializeProperties()
        {   
            EnableState = GetEnableState();

            SliderTextInformation = new List<string>();

            CreateSttingsList();
        }

        private string GetActionType()
        {
            if (_data.IsEdited)
                return "Edytuj";
            else
                return "Utwórz";
        }

        private bool GetEnableState()
        {
            if (ViewType == BandViewType.Create)
                return true;
            else
                return false;
        }

        private void ClearField()
        {
            Reason = String.Empty;
            IdBand = String.Empty;
            MAC = String.Empty;
            BarCode = String.Empty;
            UidNfc = String.Empty;
            CreateSttingsList();
            Description = String.Empty;
            SerialNumber = String.Empty;
            IsMacEnabled = true;
            BatteryCapacity = String.Empty;
            BatteryLifeMounts = String.Empty;
            BatteryName = String.Empty;
            BatteryVlotage = String.Empty;
            BatteryId = String.Empty;
            AssignedTo = String.Empty;
            BandLocation = String.Empty;
        }

        private void SetToFieldFrom_data()
        {
            if (_data.band == null)
            {
                ClearField();
            }
            else
            {
                SetDataToFields();
            }

            if(_data.SelectedUnitId != null)
            {
                if (_data.band == null)
                {
                    _data.band = new BandDTO();
                }

                treeData = new TreeUnitViewData()
                {
                    ViewMode = TreeViewMode.OnlyOneSelect,
                    Unit = UnitManager.Instance.Units.FirstOrDefault(x => x.ID == _data.SelectedUnitId)
                };

                CloseAssignToWindow(null);
            }
        }

        private void SetDataToFields()
        {
            if(_data.IsEdited)
            {
                BarCode = _data.band.BarCode;
                UidNfc = _data.band.Uid.ToString();
                SerialNumber = _data.band.SerialNumber;
            }

            IdBand = _data.band.Id.ToString();
            IsMacEnabled = _data.IsEdited ? false : true;
            MAC = _data.band.MacString;
            UidNfc = _data.band.Uid.ToString();
            SerialNumber = _data.band.SerialNumber;
            TransmissionConfiguration.SetSelectedIndexWithValue(_data.band.TransmissionPower.ToString());
            SensitivityConfiguration.SetSelectedIndexWithValue(_data.band.SensitivityPercent.ToString());
            AccConfiguration.SetSelectedIndexWithValue(_data.band.AccPercent.ToString());
            BroadcastingIntervaleConfiguration.SetSelectedIndexWithValue(_data.band.BroadcastingIntervalMs.ToString());
            ParametersList = new ObservableCollection<BandParameter>(_data.band.Parameters);
            BatteryTypesSelectedIndex = _data.band.BatteryTypeId - 1;
            Description = _data.band.Description;
            BandTitle += " o id: " + _data.band.Id;

            if(treeData != null && treeData.Unit != null)
            {
                CloseAssignToWindow(null);
            }
        }

        object IDialogWindow.Data
        {
            set
            {
                ClearField();

                _data = (BandEditData)value;

                IsReasonVisibility = Visibility.Visible;

                ViewType = _data.IsEdited ? BandViewType.Edit : BandViewType.Create;

                ActionType = GetActionType();

                BandTitle = GetTabsTitle();
                
                IdBandVisible = GetEditableComponentVisiblity();

                DefaultMarginOffset = GetMargin();

                SetToFieldFrom_data();

                Proxy.GetBatteryTypesAsync();

                if (IsReasonVisibility == Visibility.Collapsed)
                {
                    MinWidthDescription = 1570d;
                }
                else
                {
                    MinWidthDescription = 740d;
                }

                DescriptionColumnSpan = 1;

                #if DEBUG
                    IsDebug = Visibility.Visible;
                #endif
            }
        }

        public BandEditModel(IUnityContainer container)
            : base(container)
        {
            BatteryTypes = new ObservableCollection<string>();
            BatteryTypesSelectedIndex = 0;

            _container = container;
            TransmissionPowerChangeCommand = new RelayCommand(OnTransmissionPowerChange);
            ACCRangeChangeCommand = new RelayCommand(OnACCRangeChange);
            SensitivityRangeChangeCommand = new RelayCommand(OnSensitivityRangeChange);
            BroadcastingIntervalRangeTextChangedCommand = new RelayCommand(OnBroadcastingIntervalRangeTextChanged);

            CopyToClipboard = new RelayCommand(OnCopyToClipboard);
            PasteFromClipboard = new RelayCommand(OnPasteFromClipboard);
            PasteDefaultData = new RelayCommand(OnPasteDefaultData);

            CreateParameterCommand = new RelayCommand(OnCreateParameter);
            EditSelectedParameterCommand = new RelayCommand(OnEditSelectedParameter);
            DeleteParameterCommand = new RelayCommand(OnDeleteParameter);

            CancelActionCommand = new RelayCommand(OnCancelWindow);
            SaveActionCommand = new RelayCommand(OnSaveWindow);

            AssignedToClickCommand = new RelayCommand(OnAssignedToClickCommand);

            var viewFactory = container.Resolve<ViewFactory>();
            _windowsManager = this.Container.Resolve<IWindowsManager>();
            _windowsManager.RegisterWindow(WindowsKeys.CreateParameter, "Edytowanie", viewFactory.CreateView<ParameterUniwersal>(), new Size(400, 430), true);
            _windowsManager.RegisterWindow(WindowsKeys.TreeUnitHelpher, "Edytuj", viewFactory.CreateView<TreeUnitHelpher>(), new Size(550, 650), false, false);
            
            InitializeProperties();

            Proxy = ServiceFactory.GetService<UnitsServiceClient>();

            Proxy.GetBatteryTypesCompleted += proxy_GetBatteryTypesCompleted;
            Proxy.GetBandConfigurationCompleted += Proxy_GetBandConfigurationCompleted;
            Proxy.GetBatteryTypesAsync();
        }

        public Thickness GetMargin()
        {
            return new Thickness(2d);
        }

        private void OnAssignedToClickCommand()
        {
            if (_data != null)
            {
                treeData = new TreeUnitViewData()
                {
                    ViewMode = TreeViewMode.OnlyOneSelect,
                    Unit = UnitManager.Instance.Units.FirstOrDefault(x => x.ID == _data.SelectedUnitId)
                };
            }
            else
            {
                treeData = new TreeUnitViewData()
                {
                    ViewMode = TreeViewMode.OnlyOneSelect,
                };
            }

            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseAssignToWindow);
            _windowsManager.ShowWindow(WindowsKeys.TreeUnitHelpher, treeData);
        }

        public void CloseAssignToWindow(string obj)
        {
            _data.band.UnitId = treeData.Unit.ID;
            AssignedTo = treeData.Unit.Name + " ( ID: " + treeData.Unit.ID + " )";
            BandLocation = treeData.Unit.GetPath("/");
        }

        public void OnPasteDefaultData()
        {
            pasteDefaultData = true;
            treeData.Unit = UnitManager.Instance.Units.FirstOrDefault(x=>x.ID == UnitTypeOrganizer.GetRegistartionUnitId()); //UnitManager.Instance.Units.Where(e=>e.ID == Container.Resolve<SystemSettingDto>().)
            AssignedTo = treeData.Unit.Name + " ( ID: " + treeData.Unit.ID + " )";
            BandLocation = treeData.Unit.GetPath("/");
            Proxy.GetBatteryTypesAsync();
        }

        public void OnPasteFromClipboard()
        {
            if(Clipboard.BandClipboard != null)
            {
                TransmissionConfiguration.SetSelectedIndexWithValue(Clipboard.BandClipboard.TransmissionPower.ToString());
                SensitivityConfiguration.SetSelectedIndexWithValue(Clipboard.BandClipboard.SensitivityPercent.ToString());
                AccConfiguration.SetSelectedIndexWithValue(Clipboard.BandClipboard.AccPercent.ToString());
                BroadcastingIntervaleConfiguration.SetSelectedIndexWithValue(Clipboard.BandClipboard.BroadcastingIntervalMs.ToString());
                ParametersList = new ObservableCollection<BandParameter>(Clipboard.BandClipboard.Parameters);
                BatteryTypesSelectedIndex = Clipboard.BateryTypeSelectedIndex;
                Description = Clipboard.BandClipboard.Description;

                if (Clipboard.BateryTypeSelectedIndex > 0)
                {
                    BatteryId = BatteryType[BatteryTypesSelectedIndex].Id.ToString();
                    BatteryVlotage = BatteryType[BatteryTypesSelectedIndex].Voltage.ToString();
                    BatteryName = BatteryType[batteryTypesSelectedIndex].Name;
                    BatteryLifeMounts = BatteryType[batteryTypesSelectedIndex].LifeMonts.ToString();
                    BatteryCapacity = BatteryType[batteryTypesSelectedIndex].Capacity.ToString();
                }
            }

            if(Clipboard.Unit != null)
            {
                treeData.Unit = Clipboard.Unit;
                AssignedTo = treeData.Unit.Name + " ( ID: " + treeData.Unit.ID + " )";
                BandLocation = treeData.Unit.GetPath("/");
            }
        }

        public void OnCopyToClipboard()
        {
            BandDTO bandDataToClipboard = new BandDTO();

            Clipboard.Unit = treeData.Unit;

            bandDataToClipboard.AccPercent = Convert.ToByte(AccConfiguration.ValueIndex);
            bandDataToClipboard.SensitivityPercent = Convert.ToByte(SensitivityConfiguration.ValueIndex);
            bandDataToClipboard.BroadcastingIntervalMs = Convert.ToInt32(BroadcastingIntervaleConfiguration.Value.ToString().Replace(" " + AccConfiguration.leftUnit, String.Empty));
            bandDataToClipboard.TransmissionPower = Convert.ToInt16(Convert.ToInt16(TransmissionConfiguration.Value.ToString().Replace(" " + AccConfiguration.leftUnit, String.Empty)));
            bandDataToClipboard.Description = Description;
            bandDataToClipboard.Parameters = ParametersList.ToList<BandParameter>();
            

            Clipboard.BateryTypeSelectedIndex = BatteryTypesSelectedIndex;
            Clipboard.BandClipboard = bandDataToClipboard;
        }

        private void CreateSttingsList()
        {
            TransmissionConfiguration = new SliderConfigurator(_container,ParameterType.TransmissionPower, true);
            BroadcastingIntervaleConfiguration = new SliderConfigurator(_container, ParameterType.BroadcastingIntervale, true);
            SensitivityConfiguration = new SliderConfigurator(_container, "%", true);
            AccConfiguration = new SliderConfigurator(_container, "%", true);
        }

        private void GetSliderTextInformation(ParameterType parameterType)
        {
            
        }

        /// <summary>
        /// This method check out what type of view this is.
        /// </summary>
        /// <returns>Tabs Title.</returns>
        private string GetTabsTitle()
        {
            CheckIfViewTypeIsNotNull();

            return VIEW_TITLE[(int)ViewType];
        }

        private void CheckIfViewTypeIsNotNull()
        {
            if (ViewType.Equals(null))
                throw new NullReferenceException("ViewType variable is not initialized!");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Visiblity state for editable fields.</returns>
        private bool GetEditableComponentVisiblity()
        {
            CheckIfViewTypeIsNotNull();

            if (ViewType.Equals(BandViewType.Create))
                return false;
            else
                return true;
        }

        private void OnTransmissionPowerChange()
        {

        }

        private void OnACCRangeChange()
        {

        }

        private void OnSensitivityRangeChange()
        {

        }

        private void OnBroadcastingIntervalRangeTextChanged()
        {

        }

        private void OnCreateParameter()
        {
           _parameterEditData = new ParameterEditData()
           {
                BandParameter = new BandParameter()
           };

           EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling);
           this._windowsManager.ShowWindow(WindowsKeys.CreateParameter, _parameterEditData);
        }

        public void CloseWindowEventHandling(string key)
        {
            ObservableCollection<BandParameter> _temp = ParametersList;
            EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(CloseWindowEventHandling);

            if (key.Equals(WindowsKeys.CreateParameter))
            {
                if(_parameterEditData.Canceled)
                {
                                     
                }
                else
                {
                    if (!_parameterEditData.isEdited)
                    {
                        _temp.Add(_parameterEditData.BandParameter);
                        ParametersList = _temp;  
                    }
                    else
                    {
                        List<BandParameter> bp = new List<BandParameter>();
                        
                        for (int i = 0; i < ParametersList.Count; i++)
                        {
                            if (i != SelectedPropertyIndex)
                            {
                                bp.Add(ParametersList[i]);
                            }
                            else
                            {
                                bp.Add(_parameterEditData.BandParameter);
                            }
                        }
                        
                        ParametersList = new ObservableCollection<BandParameter>(bp);
                    }
                }
            }
        }

        private void OnEditSelectedParameter()
        {
            if (SelectedPropertyIndex >= 0)
            {
                _parameterEditData = new ParameterEditData()
                {
                    BandParameter = ParametersList[SelectedPropertyIndex],
                    isEdited = true
                };

                EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling);
                this._windowsManager.ShowWindow(WindowsKeys.CreateParameter, _parameterEditData);
            }
            else
            {
                MsgBox.Warning("Nie wybrano parametru do edycji.");
            }
        }

        private void OnDeleteParameter()
        {
            if(
                SelectedPropertyIndex != null &&
                SelectedPropertyIndex >= 0   &&
                SelectedPropertyIndex < ParametersList.Count
                )
            {
                MsgBox.Confirm("Czy jesteś pewien że chcesz usunąć parametr \nz kluczem: " + ParametersList[SelectedPropertyIndex].Key + "\no wartości: " +
                    ParametersList[SelectedPropertyIndex].Value + "?", new EventHandler<WindowClosedEventArgs>(OnConfirmUnlockAllReceivers), MsgBox.Buttons.YesNo);
            }
            else
            {
                MsgBox.Warning("Nie wybrano parametru do usunięcia.");
            }
        }

        public void OnConfirmUnlockAllReceivers(object sender, WindowClosedEventArgs e)
        {
            bool? confirmed = (bool?)e.DialogResult;

            if (confirmed.HasValue && confirmed.Value)
                ParametersList.Remove(ParametersList[SelectedPropertyIndex]);
        }

        private void OnSaveWindow()
        {
            Close(CloseButton.OK);
        }

        private void Close(CloseButton closeButton)
        {
            string message = "";

            if(closeButton == CloseButton.OK)
            {
                if (MAC == String.Empty)
                    message = "Nie podano adresu MAC!";

                if (BatteryTypesSelectedIndex == -1)
                    message = "Nie wybrano baterii!";

                if (_data.band.UnitId == null || AssignedTo.Equals(String.Empty))
                    message = "Nie przypisano do rzednej jednostki organizacyjnej!";

                if(!message.Equals(String.Empty))
                {
                    MsgBox.Warning(message);
                    return;
                }


                SaveData();
            }

            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        private void SaveData()
        {
            unchecked
            {
                UInt64 macAsNumber = GetMacAsNumber(MAC);

                SetDataFromFields();
            }

            UnitsServiceClient proxy = ServiceFactory.GetService<UnitsServiceClient>();
            proxy.SetBandConfigurationCompleted += proxy_SetBandConfigurationCompleted;
            proxy.SetBandConfigurationAsync(_data.band);
        }

        private UInt64 GetMacAsNumber(string mac)
        {
            UInt64 macAsNumber = 0;

            string hex = mac.Replace(":", String.Empty);
            if (hex.Length == 12)
            {
                macAsNumber = Convert.ToUInt64(hex, 16);
            }
            else
            {
                macAsNumber = Convert.ToUInt64(hex);
            }

            return macAsNumber;
        }

        private void SetDataFromFields()
        {
            _data.band.Description = Description;
            _data.band.Mac = Convert.ToInt64(GetMacAsNumber(MAC));
            _data.band.BarCode = string.IsNullOrEmpty(BarCode) ? null : BarCode;
            _data.band.SerialNumber = string.IsNullOrEmpty(SerialNumber) ? null : SerialNumber;
            _data.band.Uid = string.IsNullOrEmpty(BarCode) ? (long?)null : Convert.ToInt64(BarCode);
            _data.band.Reason = Convert.ToString(Reason);
            _data.band.AccPercent = Convert.ToByte(AccConfiguration.Value.ToString().Replace(" " + AccConfiguration.leftUnit, String.Empty));
            _data.band.SensitivityPercent = Convert.ToByte(SensitivityConfiguration.Value.ToString().Replace(" " + AccConfiguration.leftUnit, String.Empty));
            _data.band.TransmissionPower = Convert.ToInt16(TransmissionConfiguration.Value.ToString().Replace(" " + AccConfiguration.leftUnit, String.Empty));
            _data.band.BatteryInstallationDateUtc = DateTime.Now.ToUniversalTime();
            _data.band.BatteryTypeId = Convert.ToByte(batteryIndex[BatteryTypesSelectedIndex]);
            _data.band.BroadcastingIntervalMs = Convert.ToInt32(BroadcastingIntervaleConfiguration.Value.ToString().Replace(" " + AccConfiguration.leftUnit, String.Empty));
            _data.band.AccPercent = Convert.ToByte(AccConfiguration.Value.ToString().Replace(" " + AccConfiguration.leftUnit, String.Empty));
            _data.band.Parameters = ParametersList.ToList<BandParameter>();
        }

        void proxy_SetBandConfigurationCompleted(object sender, SetBandConfigurationCompletedEventArgs e)
        {
            int a;

            if (e.Result != null && int.TryParse(e.Result, out a))
            {
                _data.Canceled = true;  
                Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
            }
            else if (!string.IsNullOrEmpty(e.Result))
            {
                MsgBox.Error(e.Result);
            }
        }

        void proxy_GetBatteryTypesCompleted(object sender, GetBatteryTypesCompletedEventArgs e)
        {
            BatteryType = new List<BatteryTypeDTO>();
            BatteryTypeDTO dto;

            int index = -1;
            if (BatteryTypesSelectedIndex != -1)
                index = BatteryTypesSelectedIndex;

            BatteryTypes = new ObservableCollection<string>();
            batteryIndex = new List<int>();

            if(e.Result != null)
            {
                for (int i = 0; i < e.Result.Count; i++)
                {
                    BatteryTypes.Add(e.Result[i].Name);
                    batteryIndex.Add(e.Result[i].Id);

                    dto = new BatteryTypeDTO()
                    {
                        Id = e.Result[i].Id,
                        Name = e.Result[i].Name,
                        Voltage = e.Result[i].Voltage,
                        Description = e.Result[i].Description,
                        LifeMonts = e.Result[i].LifeMonts,
                        Capacity = e.Result[i].Capacity
                    };

                    BatteryType.Add(dto);
                }
            }

            BatteryTypesSelectedIndex = index;

            //if (ViewType.Equals(BandViewType.Create))
            Proxy.GetBandConfigurationAsync((short?)(null));
        }

        bool pasteDefaultData = false;

        void Proxy_GetBandConfigurationCompleted(object sender, GetBandConfigurationCompletedEventArgs e)
        {
            if(e.Result != null)
            {
                var defaultBand = e.Result.Where(i => i.Id == 0).FirstOrDefault();

                if (ViewType.Equals(BandViewType.Create) || pasteDefaultData)
                {
                    TransmissionConfiguration.SetSelectedIndexWithValue(defaultBand.TransmissionPower.ToString());
                    BroadcastingIntervaleConfiguration.SetSelectedIndexWithValue(defaultBand.BroadcastingIntervalMs.ToString());
                    SensitivityConfiguration.SetSelectedIndexWithValue(defaultBand.SensitivityPercent.ToString());
                    AccConfiguration.SetSelectedIndexWithValue(defaultBand.AccPercent.ToString());
                    BroadcastingIntervaleConfiguration.SetSelectedIndexWithValue(defaultBand.BroadcastingIntervalMs.ToString());
                    ParametersList = new ObservableCollection<BandParameter>(defaultBand.Parameters);

                    if (defaultBand.BatteryTypeId >= 0)
                    {
                        BatteryTypesSelectedIndex = BatteryType.IndexOf(BatteryType.FirstOrDefault(x => x.Id == defaultBand.BatteryTypeId));
                        BatteryId = BatteryType.FirstOrDefault(x => x.Id == defaultBand.BatteryTypeId).Id.ToString();
                        BatteryVlotage = BatteryType.FirstOrDefault(x => x.Id == defaultBand.BatteryTypeId).Voltage.ToString();
                        BatteryName = BatteryType.FirstOrDefault(x => x.Id == defaultBand.BatteryTypeId).Name;
                        BatteryLifeMounts = BatteryType.FirstOrDefault(x => x.Id == defaultBand.BatteryTypeId).LifeMonts.ToString();
                        BatteryCapacity = BatteryType.FirstOrDefault(x => x.Id == defaultBand.BatteryTypeId).Capacity.ToString();
                    }

                    pasteDefaultData = false;
                }
            }
        }

        private void OnCancelWindow()
        {
            Close(CloseButton.Cancel);
        }

        public string WindowKey
        {
            get { return WindowsKeys.CreateBand; }
        }

        public object Data
        {
            set { throw new NotImplementedException(); }
        }
    }
}
