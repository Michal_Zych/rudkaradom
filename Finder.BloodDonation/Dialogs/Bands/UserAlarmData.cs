﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Dialogs.Bands
{
    public class UserAlarmData
    {
        public string BandsName { get; set; }
        public int BandId { get; set; }
        public string WarningInfo { get; set; }

        public bool IsClosed = true;
    }
}
