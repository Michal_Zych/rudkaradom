﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Events;
using System.Collections.Generic;
using System.Collections;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Dialogs.Base;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.Dialogs.Helpers;

namespace Finder.BloodDonation.Dialogs.Bands
{
    public class ParameterUniwersalModel : ViewModelBase, IDialogWindow
    {
        private ParameterEditData dat;
        public ParameterEditData _data {
            get{
                return dat;
            }
            set
            {
                dat = value;
                ParameterKey = value.BandParameter.Key;
                ParameterValue = value.BandParameter.Value;
                ParameterDescription = value.BandParameter.Description;
            }
        }

        [RaisePropertyChanged]
        public string ParameterKey { get; set; }

        [RaisePropertyChanged]
        public string ParameterValue { get; set; }

        [RaisePropertyChanged]
        public string ParameterDescription { get; set; }

        [RaisePropertyChanged]
        public string Title { get; set; }

        public RelayCommand OKClick { get; set; }
        public RelayCommand CancelClick { get; set; }

        public ParameterUniwersalModel(IUnityContainer container)
            : base(container)
        {
            OKClick = new RelayCommand(OnOKClick);
            CancelClick = new RelayCommand(OnCancelClick);
        }

        private void OnCancelClick()
        {
            Close(CloseButton.Cancel);
        }

        private void OnOKClick()
        {
            Close(CloseButton.OK);
        }

        private void Close(CloseButton closeButton)
        {
            string msg = String.Empty;

            if(closeButton == CloseButton.OK)
            {
                if(ParameterKey.Equals(String.Empty))
                {
                    msg = "Nie podano klucza wartości";
                }
                if(ParameterDescription.Equals(String.Empty))
                {
                    msg = "Nie podano wartości";
                }

                if(!msg.Equals(String.Empty))
                {
                    MsgBox.Error(msg);
                    return;
                }

                SaveData();
            }

            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        private void SaveData()
        {
            _data.Canceled = false;

            _data.BandParameter.Key = ParameterKey;
            _data.BandParameter.Value = ParameterValue;
            _data.BandParameter.Description = ParameterDescription;
        }

        public string WindowKey
        {
            get { return WindowsKeys.CreateParameter; }
        }

        public object Data
        {
            set 
            {
                _data = (ParameterEditData)value;


               // var title = (_data.isEdited) ? "Edycja " : "Tworzenie ";
                //Title = title + "Parametru";
            }
        }
    }
}
