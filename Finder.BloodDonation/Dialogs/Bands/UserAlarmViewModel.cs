﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Dialogs.Bands
{
    public class UserAlarmViewModel : FinderFX.SL.Core.MVVM.ViewModelBase, IDialogWindow
    {
        internal UserAlarmData data;

        [RaisePropertyChanged]
        public string BandName { get; set; }

        [RaisePropertyChanged]
        public string BandWarning { get; set; }

        public RelayCommand OkClick { get; set; }
        public RelayCommand CancelClick { get; set; }

        public UserAlarmViewModel(IUnityContainer container)
            : base(container)
        {
            OkClick = new RelayCommand(OnOkClick_Command);
            CancelClick = new RelayCommand(OnCancelClick_Command);
        }

        private void OnCancelClick_Command()
        {
            Container.Resolve<IWindowsManager>().CloseWindow(this.WindowKey);
        }

        private void OnOkClick_Command()
        {
            throw new NotImplementedException();
        }

        public string WindowKey
        {
            get { return WindowsKeys.UserAlarm; }
        }

        public object Data
        {
            set 
            {
                data = (UserAlarmData)value;

                if (data != null)
                    SetDataToField();
            
            }
        }

        private void SetDataToField()
        {
            BandName = data.BandsName;
            BandWarning = String.Empty;
        }
    }
}
