﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Dialogs.Helpers
{
    internal static class WindowsKeys
    {
        //internal static string CreateUser = "CreateUser";
        //internal static string RemoveUser = "RemoveUser"; 
        internal static string EditUserPermissions = "EditUserPermissiones";

        internal static string AddAttribute = "AddAttribute";
        internal static string EditAttribute = "EditAttribute";
        internal static string AddAttributeMap = "AddAttributeMap";
        internal static string AddOrder = "AddOrder";
        internal static string ChangeOrderStatus = "ChangeOrderStatus";
        internal static string DeleteOrder = "DeleteOrder";
        internal static string EditOrder = "EditOrder";

        internal static string DeleteUnit = "DeleteUnit";
        internal static string EditUnit = "EditUnit";
        internal static string ConfirmMoveUnit = "ConfirmMoveUnit";
        internal static string EditUser = "EditUser";
        internal static string DeleteUser = "DeleteUser";

        internal static string ToolsWindow = "ToolsWindow";
        internal static string ConfirmDialog = "ConfirmDialog";
        internal static string MessageDialog = "Nowy komunikat";
        internal static string SettingDialog = "Konfiguracja";

        internal static string EditPatient = "EditPatient";

        internal static string CreateBand = "CreateBand";
        internal static string EditBand = "EditBand";

        internal static string CreateParameter = "CreateParameter";
        internal static string EditParameter = "EditParameter";

        internal static string ConfigurationPatient = "ConfigurationPatient";

        internal static string EditNumber = "EditNumber";

        internal static string EditProhibitedZoneModel = "EditProhibitedZoneModel";

        internal static string EditTransmitter = "EditTransmitter";

        internal static string PatientAssign = "PatientAssign";

        internal static string SettingsView = "SettingsView";

        internal static string EditSettingsParameter = "EditSettingsParameter";

        internal static string TreeUnitHelpher = "TreeUnitHelpher";

        internal static string LocalizationConfig = "LocalizationConfig";

        internal static string AssignBand = "AssignBand";

        internal static string EditBackup = "EditBackup";

        internal static string ColumnEditorDialog = "ColumnEditorDialog";

        internal static string DetailsMonitorableObject = "DetailsMonitorableObject";

        internal static string UserAlarm = "UserAlarm";

        internal static string BarCodeView = "BarCodeView";

        internal static string BatteryExchange = "BatteryExchange";

        internal static string NewAlarmsView = "NewAlarmsView";

        internal static string PatientAlarmingView = "PatientAlarmingView";

        internal static string PatientOutOfZone = "PatientOutOfZone";

        internal static string CreateTransmitter = "CreateTransmitter";

        internal static string AssignTransmitterModel = "AssignTransmitter";

        internal static string CloseAlarm = "CloseAlarm";

        internal static string SystemEventDialog = "SystemEventDialog";

        internal static string BatteryReplace = "BatteryReplace";

        internal static string CloseAlarmDialog = "CloseAlarmDialog";

        internal static string MapUnit = "MapUnit";
		 internal static string Group = "Group";

        internal static string TransmitterEventsDialog = "TransmitterEventsDialog";
        internal static string LocalizationAlarmsPopup = "LocalizationAlarmsPopup";

        internal static string SuccessDialog = "SuccessDialog";
    }
}
