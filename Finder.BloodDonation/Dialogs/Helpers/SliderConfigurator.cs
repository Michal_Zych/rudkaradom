﻿using Finder.BloodDonation.Settings;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Dialogs.Helpers
{
    public class SliderConfigurator : ViewModelBase
    {
        public static System.Collections.Generic.List<string[]> BandValues = new List<string[]>()
        {
            new string[]{ "-40", "-20", "-16", "-12", "-8", "-4", "0", "3", "4" },
            new string[]{ "160", "320", "640", "1280", "2560", "5120", "10240" },
        };

        public static List<string[]> BandParametersUnits = new List<string[]>()
        {
            new string[] { "dBm" },
            new string[] { "ms" }
        };

        private const int ARRAY_LENGTH_OFFSET = 1;
        private const int FIRST_ELEMENT = 0;

        public string leftUnit;
        public string rightUnit;

        private bool isPercentage;

        [RaisePropertyChanged]
        public string PercentageUnit { get { return "%"; } }

        public bool IsItemsWithUnit { get; set; }

        [RaisePropertyChanged]
        public string MinimumValueWithUnit { get; set; }
        [RaisePropertyChanged]
        public string MaximumValueWithUnit { get; set; }

        ParameterType parameterType;

        private string valueText;

        [RaisePropertyChanged]
        public string ValueText
        {
            get
            {
                return valueText;
            }

            set
            {
                valueText = value;
                string _temp = string.Empty;

                if(isPercentage)
                {

                }
            }
        }

        [RaisePropertyChanged]
        public string[] Items
        {
            get
            {
                if (!IsItemsWithUnit)
                    return BandValues[(int)parameterType];
                else
                {
                    string[] _temp;

                    _temp = new string[BandValues[(int)parameterType].Length];

                    for (int i = 0; i < BandValues[(int)parameterType].Length; i++)
                    {
                        _temp[i] = BandValues[(int)parameterType][i] + " " + leftUnit;
                    }

                    return _temp;
                }
            }
        }

        public void SetSelectedIndexWithValue(string value)
        {
            if (!isPercentage)
            {
                for (int i = 0; i < Items.Length; i++)
                {
                    if (Items[i].Replace(" "+ leftUnit, String.Empty).Equals(value))
                        ValueIndex = i;
                }
            }
            else
                ValueIndex = int.Parse(value);
        }

        [RaisePropertyChanged]
        public object Value
        {
            get
            {
                if (!isPercentage)
                {
                    if (parameterType.Equals(null))
                    {
                        throw new NullReferenceException();
                    }
                    if (BandValues[(int)parameterType].Equals(null))
                    {
                        throw new NullReferenceException();
                    }
                    if (
                        ValueIndex < 0 ||
                        ValueIndex > BandValues[(int)parameterType].Length - ARRAY_LENGTH_OFFSET
                        )
                    {
                        throw new ArgumentOutOfRangeException();
                    }

                    return BandValues[(int)parameterType][ValueIndex];
                }
                else
                {
                    if (!IsItemsWithUnit)
                        return ValueIndex;
                    else
                    {
                        return ValueIndex + " %";
                    }
                }
            }
        }

        /// <summary>
        /// Slider minimum value text.
        /// </summary>
        [RaisePropertyChanged]
        public string MinimumValueText { get; set; }
        /// <summary>
        /// Slider maximum value text.
        /// </summary>
        [RaisePropertyChanged]
        public string MaximumValueText { get; set; }

        [RaisePropertyChanged]
        public int MinimumValue { get; set; }
        [RaisePropertyChanged]
        public int MaximumValue { get; set; }

        int valueIndex;
        /// <summary>
        /// Return index of the "BandValues" array or Percentage Value.
        /// </summary>
        [RaisePropertyChanged]
        public int ValueIndex 
        {
            get 
            {
                return valueIndex;
            } 
            set 
            {
                ValueText = value + " %";
                valueIndex = value;
            }
        }

        /// <summary>
        /// This constructor create and configurate slider for nonspecific array.
        /// </summary>
        /// <param name="container">UnitContainer with ViewModel class.</param>
        /// <param name="parameterType">An index that returns an array of values ​​for an irregular slider.</param>
        /// <param name="isItemsWithUnit">Whether the unit is to be returned.</param>
        public SliderConfigurator(IUnityContainer container, ParameterType parameterType, bool isItemsWithUnit) : base(container)
        {
            if (container.Equals(null))
                throw new ArgumentNullException("Parameter container is not defined.");

            this.parameterType = parameterType;
            isPercentage = false;
            this.IsItemsWithUnit = isItemsWithUnit;

            if (
                BandParametersUnits[(int)parameterType].Equals(null) ||
                BandValues[(int)parameterType].Equals(null)
                )
            {
                throw new NullReferenceException("Bad parameterType.");
            }

            if (
                BandValues[(int)parameterType].Length == 0 ||
                BandParametersUnits[(int)parameterType].Length == 0
                )
            {
                throw new NullReferenceException();
            }

            var temp = BandValues[(int)parameterType];

            this.leftUnit = BandParametersUnits[(int)parameterType][FIRST_ELEMENT];
            this.rightUnit = BandParametersUnits[(int)parameterType][BandParametersUnits[(int)parameterType].Length - ARRAY_LENGTH_OFFSET];

            this.MinimumValueText = BandValues[(int)parameterType][FIRST_ELEMENT];
            this.MaximumValueText = BandValues[(int)parameterType][BandValues[(int)parameterType].Length - ARRAY_LENGTH_OFFSET].ToString();

            this.MinimumValueWithUnit = this.MinimumValueText + leftUnit;
            this.MaximumValueWithUnit = this.MaximumValueText + rightUnit;

            SetStartDefaultData();
        }

        /// <summary>
        /// This constructor create and configurate slider for percentage value.
        /// </summary>
        /// <param name="container">UnitContainer with ViewModel class.</param>
        /// <param name="unit">Value unit - If not given, default ( auto ) "%".</param>
        /// <param name="isItemsWithUnit">Whether the unit is to be returned.</param>
        public SliderConfigurator(IUnityContainer container, string unit, bool isItemsWithUnit)
            : base(container)
        {
            if (container.Equals(null))
                throw new ArgumentNullException("Parameter container is not defined.");
            if (unit.Equals(String.Empty))
                unit = "%";

            isPercentage = true;
            this.IsItemsWithUnit = isItemsWithUnit;
            SetStartDefaultData();

            this.MinimumValueText = "0";
            this.MaximumValueText = "100";

            this.leftUnit = unit;
            this.rightUnit = unit;

            MinimumValueWithUnit = this.MinimumValueText + unit;
            MaximumValueWithUnit = this.MaximumValueText + unit;
        }

        public SliderConfigurator(IUnityContainer container, string leftUnit, string rightUnit)
            : base(container)
        {
            isPercentage = true;
            SetStartDefaultData();

            this.leftUnit = leftUnit;
            this.rightUnit = rightUnit;

            this.MinimumValueText = "0";
            this.MaximumValueText = "100";

            this.MinimumValueWithUnit  = MinimumValueText + this.leftUnit;
            this.MaximumValueWithUnit = MaximumValueText + this.rightUnit;
        }

        public SliderConfigurator(IUnityContainer container, string leftUnit, string rightUnit, int minValue, int maxValue)
            : base(container)
        {
            isPercentage = false;
            this.leftUnit = leftUnit;
            this.rightUnit = rightUnit;

            this.MinimumValueText = "-20";
            this.MaximumValueText = "20";

            this.MinimumValueWithUnit = MinimumValueText + this.leftUnit;
            this.MaximumValueWithUnit = MaximumValueText + this.rightUnit;

            MinimumValue = minValue;
            MaximumValue = maxValue;

            ValueIndex = 0;
        }

        private void SetStartDefaultData()
        {
            if (!isPercentage)
            {
                MinimumValue = FIRST_ELEMENT;
                MaximumValue = BandValues[(int)parameterType].Length - ARRAY_LENGTH_OFFSET;

                ValueIndex = (int)Math.Round((decimal)BandValues[(int)parameterType].Length / 2, 0);
            }
            else
            {
                MinimumValue = 0;
                MaximumValue = 100;
                ValueIndex = 50;
            }
        }
    }

    public enum ParameterType
    {
        TransmissionPower = 0,
        BroadcastingIntervale
    }
}
