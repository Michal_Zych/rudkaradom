﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Settings;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using ViewModelBase = FinderFX.SL.Core.MVVM.ViewModelBase;
using Global = Finder.BloodDonation.Settings;

namespace Finder.BloodDonation.Dialogs.Units
{
    public class DeleteUnitModel : ViewModelBase, IDialogWindow
    {
        private UnitEditData _data;

        [RaisePropertyChanged]
        public string Title { get; set; }

        [RaisePropertyChanged]
        public string UnitName { get; set; }

        [RaisePropertyChanged]
        public string UnitLocation { get; set; }

        [RaisePropertyChanged]
        public string Reason { get; set; }

        [RaisePropertyChanged]
        public Visibility IsReasonVisible { get; set; }


        public RelayCommand OKClick { get; set; }

        public RelayCommand CancelClick { get; set; }




        public DeleteUnitModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            OKClick = new RelayCommand(OnOKClick);
            CancelClick = new RelayCommand(OnCancelClick);

        }

        public string WindowKey
        {
            get { return WindowsKeys.DeleteUnit; }
        }

        object IDialogWindow.Data
        {
            set
            {
                _data = (UnitEditData)value;

                if(UnitTypeOrganizer.IsSensorUnit(_data.Unit.UnitTypeIDv2))
                {
                    Title = "Kasowanie czujnika";
                }
                else
                {
                    Title = "Kasowanie jednostki organizacyjnej";
                }

                UnitName = _data.Unit.Name;
                UnitLocation = _data.Unit.Path;
                Reason = "";
                if (Global.Settings.Current.GetInt(ConfigurationKeys.ShowReasonField) == ConfigurationKeys.ENABLED)
                {
                    IsReasonVisible = Visibility.Visible;
                }
                else
                {
                    IsReasonVisible = Visibility.Collapsed;
                }
            }
        }


        private void OnOKClick()
        {
            Close(CloseButton.OK);
        }
        private void OnCancelClick()
        {
            Close(CloseButton.Cancel);
        }

        private void Close(CloseButton button)
        {
            if (button == CloseButton.OK)
            {
                SaveData();
            }

            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        private void SaveData()
        {
            _data.Canceled = false;
            _data.LogInfo = "";
            _data.Reason = Reason;
        }
    }
}
