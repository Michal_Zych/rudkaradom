﻿using Finder.BloodDonation.Model.Dto;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Dialogs.Units
{
    public class UnitEditData
    {
        public UnitDTO Unit { get; set; }
        public UnitDTO ParentUnit { get; set; }

        public string Reason { get; set; }
        public bool Canceled = true;

        public string LogInfo { get; set; }
    }
}
