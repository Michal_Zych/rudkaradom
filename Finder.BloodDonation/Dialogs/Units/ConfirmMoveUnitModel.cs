﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Settings;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using ViewModelBase = FinderFX.SL.Core.MVVM.ViewModelBase;
using Global = Finder.BloodDonation.Settings;

namespace Finder.BloodDonation.Dialogs.Units
{
    public class ConfirmMoveUnitModel : ViewModelBase, IDialogWindow
    {
        private UnitEditData _data;

        [RaisePropertyChanged]
        public string Title { get; set; }

        [RaisePropertyChanged]
        public string UnitName { get; set; }

        [RaisePropertyChanged]
        public string UnitLocation { get; set; }

        [RaisePropertyChanged]
        public string Reason { get; set; }

        [RaisePropertyChanged]
        public Visibility IsReasonVisible { get; set; }

        [RaisePropertyChanged]
        public string ParentName { get; set; }

        [RaisePropertyChanged]
        public string NewParentName { get; set; }

        [RaisePropertyChanged]
        public string UnitNewLocation { get; set; }

        public RelayCommand OKClick { get; set; }

        public RelayCommand CancelClick { get; set; }




        public ConfirmMoveUnitModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            OKClick = new RelayCommand(OnOKClick);
            CancelClick = new RelayCommand(OnCancelClick);

        }

        public string WindowKey
        {
            get { return WindowsKeys.ConfirmMoveUnit; }
        }

        object IDialogWindow.Data
        {
            set
            {
                _data = (UnitEditData)value;

                if (UnitTypeOrganizer.IsSensorUnit(_data.Unit.UnitTypeIDv2))
                {
                    Title = "Przenoszenie czujnika";
                }
                else
                {
                    Title = "Przenoszenie jednostki organizacyjnej";
                }

                UnitName = _data.Unit.Name;
                UnitLocation = _data.Unit.Path;

                ParentName = "z: " + _data.Unit.Parent.Name;
                NewParentName = "do: " + _data.ParentUnit.Name;
                UnitNewLocation = _data.ParentUnit.Path;

                Reason = "";
                if (Global.Settings.Current.GetInt(ConfigurationKeys.ShowReasonField) == ConfigurationKeys.ENABLED)
                {
                    IsReasonVisible = Visibility.Visible;
                }
                else
                {
                    IsReasonVisible = Visibility.Collapsed;
                }
            }
        }


        private void OnOKClick()
        {
            Close(CloseButton.OK);
        }
        private void OnCancelClick()
        {
            Close(CloseButton.Cancel);
        }

        private void Close(CloseButton button)
        {
            if (button == CloseButton.OK)
            {
                SaveData();
            }

            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        private void SaveData()
        {
            _data.Canceled = false;

            _data.LogInfo = ""
                    .LogEdit("Lokalizacja", _data.Unit.Parent.Name, _data.Unit.ParentUnitID.Value, _data.ParentUnit.Name, _data.ParentUnit.ID);
            _data.Unit.Parent = _data.ParentUnit;
            _data.Unit.ParentUnitID = _data.Unit.Parent.ID;
            _data.Reason = Reason;
        }
    }
}
