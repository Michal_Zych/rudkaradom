﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Converters;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Settings;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using ViewModelBase = FinderFX.SL.Core.MVVM.ViewModelBase;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.DataManagers;
using System.Linq;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.UnitsService;

namespace Finder.BloodDonation.Dialogs.Units
{
    public class EditUnitModel : ViewModelBase, IDialogWindow
    {
        private UnitEditData _data;

        [RaisePropertyChanged]
        public string Title { get; set; }

        [RaisePropertyChanged]
        public string UnitName { get; set; }

        [RaisePropertyChanged]
        public string UnitLocation { get; set; }

        [RaisePropertyChanged]
        public string UnitDescription { get; set; }

        [RaisePropertyChanged]
        public bool IsDepartment { get; set; }

        [RaisePropertyChanged]
        public bool IsRegistration { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<int> PictureList { get; set; }

        [RaisePropertyChanged]
        public int? SelectedPicture { get; set; }

        [RaisePropertyChanged]
        public Visibility IsSensor { get; set; }

        [RaisePropertyChanged]
        public string Reason { get; set; }

        [RaisePropertyChanged]
        public Visibility IsReasonVisible { get; set; }

        [RaisePropertyChanged]
        public bool IsUnassignedPatients { get; set; }

        public RelayCommand OKClick { get; set; }

        public RelayCommand CancelClick { get; set; }

        [RaisePropertyChanged]
        public bool IsDepartmentEnable { get; set; }

        [RaisePropertyChanged]
        public Visibility IsCreatetVisiblity { get; set; }

        [RaisePropertyChanged]
        public Visibility IsDepartmentVisiblity { get; set; }

        [RaisePropertyChanged]
        public Visibility IsRegistrationVisiblity { get; set; }

        [RaisePropertyChanged]
        public Visibility IsUnassignedPatientsVisiblity { get; set; }

        private UnitType unitTypes;

        private int patientInUnit = 0;

        private UnitsServiceClient Proxy;

        public EditUnitModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            OKClick = new RelayCommand(OnOKClick);
            CancelClick = new RelayCommand(OnCancelClick);

            Proxy = ServiceFactory.GetService<UnitsServiceClient>("Finder-BloodDonation-Model-Services-UnitsService.svc");
            Proxy.GetAssignedPatientsCountCompleted += proxy_GetAssignedPatientsCountCompleted;
        }

        public string WindowKey
        {
            get { return WindowsKeys.EditUnit; }
        }

        private bool _isEdited;
        private bool _isSensor;
        private bool _isBitSensor;


        public void proxy_GetAssignedPatientsCountCompleted(object sender, GetAssignedPatientsCountCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                patientInUnit = e.Result;
            }
        }

        private void SetData()
        {
            _isEdited = !string.IsNullOrEmpty(_data.Unit.Name);
            _isSensor = UnitTypeOrganizer.IsSensorUnit(_data.Unit.UnitTypeIDv2);
            _isBitSensor = UnitTypeOrganizer.IsBitSensorUnit(_data.Unit.UnitTypeIDv2);

            IsRegistration = false;
            IsDepartment = false;
            IsUnassignedPatients = false;

            if (_isSensor)
            {
                IsSensor = Visibility.Visible;
            }
            else
            {
                IsSensor = Visibility.Collapsed;
            }

            var title = (_isEdited) ? "Edycja " : "Tworzenie ";
            if (_isSensor)
            {
                title = title + "czujnika";
            }
            else
            {
                title = title + "jednostki organizacyjnej";
            }

            Title = title;

            if (!_isEdited)
            {
                if ((_data.Unit.Parent.UnitTypeIDv2 != (int)UnitTypeV2.HospitalDepartment && _data.Unit.Parent.UnitTypeIDv2 != (int)UnitType.DEPARTMENT) && !_data.Unit.Parent.IsMonitored)
                {
                    IsDepartmentEnable = true;
                }
                else
                {
                    IsDepartmentEnable = false;
                }
            }
            else
            {
                if ((_data.Unit.UnitTypeIDv2 != (int)UnitTypeV2.HospitalDepartment && _data.Unit.UnitTypeIDv2 != (int)UnitType.DEPARTMENT) && !_data.Unit.IsMonitored)
                {
                    IsDepartmentEnable = true;
                }
                else
                {
                    IsDepartmentEnable = false;
                }
            }

            UnitLocation = _data.Unit.Path;

            UnitName = _data.Unit.Name;

            UnitDescription = _data.Unit.Description;

            var list = UnitTypeOrganizer.GetUnitTypes();
            List<int> filteredList = new List<int>();

            foreach (var e in list)
            {
                if (UnitTypeOrganizer.IsSensorUnit(e) == _isSensor
                    && ((!_isEdited) || (UnitTypeOrganizer.IsBitSensorUnit(e) == _isBitSensor)))
                {
                    filteredList.Add(e);
                }
            }

            if (_isEdited)
            {
                IsRegistrationVisiblity = Visibility.Collapsed;

                if (GetDepartmentsForCreatedUnits(_data.Unit) == null)
                {
                    IsDepartmentVisiblity = Visibility.Visible;
                    //IsDepartment = true;
                    IsDepartmentEnable = true;

                    if(_data.Unit.UnitTypeIDv2 == (int)UnitTypeV2.HospitalDepartment)
                    {
                        IsDepartment = true;
                    }
                    else
                    {
                        IsDepartment = false;
                    }
                }
                else
                {
                    IsDepartmentEnable = false;
                    IsDepartmentVisiblity = Visibility.Collapsed;
                }
                if(_data != null && UnitTypeOrganizer.GetRegistartionUnitId() == _data.Unit.ID)
                {
                    Title += " dla oddziału: \"" + _data.Unit.Name + "\"";
                    IsDepartmentVisiblity = Visibility.Visible;
                    IsDepartmentEnable = false;
                }
                if(_data != null && UnitTypeOrganizer.GetUnassignedPatientsUnitId() == _data.Unit.ID)
                {
                    IsDepartmentVisiblity = Visibility.Visible;
                    IsDepartmentEnable = false;
                    Title += " dla oddziału: \"Pacjenci nieprzypisani\"";
                }

                IsUnassignedPatientsVisiblity = Visibility.Collapsed;
            }
            else
            {
                var _temp = UnitManager.Instance.Units.Where(e => e.ID == UnitTypeOrganizer.GetRegistartionUnitId()).ToList();

                if (GetDepartmentsForCreatedUnits(_data.Unit) == null)
                {
                    IsDepartmentVisiblity = Visibility.Visible;
                }
                else
                {
                    IsDepartmentVisiblity = Visibility.Collapsed;
                }
            }

            PictureList = new ObservableCollection<int>(UnitTypeOrganizer.GetPictureIDs());
            SelectedPicture = _data.Unit.PictureID;

            Reason = "";
            if (Global.Settings.Current.GetInt(ConfigurationKeys.ShowReasonField) == ConfigurationKeys.ENABLED)
            {
                IsReasonVisible = Visibility.Visible;
            }
            else
            {
                IsReasonVisible = Visibility.Collapsed;
            }
        }

        object IDialogWindow.Data
        {
            set
            {
                _data = (UnitEditData)value;
                Proxy.GetAssignedPatientsCountAsync(_data.Unit.ID);

                SetData();
            }
        }

        private void OnOKClick()
        {
            Close(CloseButton.OK);
        }
        private void OnCancelClick()
        {
            Close(CloseButton.Cancel);
        }

        private void Close(CloseButton button)
        {
            UnitManager.Instance.ReloadRoot(action);

            if (button == CloseButton.OK)
            {
                string errorMsg = "";

                if (IsDepartment && IsRegistration)
                {
                    errorMsg = "Nie można utworzyć rejestracji będącej oddziałem!";
                }
                if (IsUnassignedPatients)
                {

                }

                if (IsRegistration)
                {

                }
                if (IsDepartment)
                {
                    if (GetDepartmentsForCreatedUnits(_data.Unit) != null)
                    {
                        errorMsg = "Nie można tworzyć oddziału w oddziale!";
                    }
                    else
                    {
                        unitTypes = UnitType.HOSPITAL_DEPARTMENT;
                    }
                }
                else
                {
                    unitTypes = UnitType.DEFAULT;
                }

                if (string.IsNullOrWhiteSpace(UnitName))
                {
                    errorMsg = "Nazwa nie może być pusta.";
                }
                if(patientInUnit > 0)
                {
                    errorMsg = "Nie można edytować jednostki organizacyjnej do której przypisani są pacjenci!";
                }

                if (errorMsg != "")
                {
                    MsgBox.Error(errorMsg);
                    return;
                }

                SaveData();
            }

            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        private void action()
        {
            
        }

        public UnitDTO GetDepartmentsForCreatedUnits(UnitDTO unitToCheck)
        {
            UnitDTO _temp = null;

            if (unitToCheck.Parent != null)
            {
                _temp = unitToCheck.Parent;
            }

            while (_temp == null || (!UnitTypeOrganizer.IsHospitalDepartment(_temp.UnitTypeIDv2) && _temp.ID != UnitTypeOrganizer.GetRegistartionUnitId()))
            {
                if (_temp.Parent != null)
                    _temp = _temp.Parent;
                else
                    return null;
            }

            return _temp;
        }

        private void SaveData()
        {
            _data.Canceled = false;

            if (_isEdited)
            {
                _data.LogInfo = "".LogEdit("Nazwa", _data.Unit.Name, UnitName)
                    .LogEdit("Opis", _data.Unit.Description, UnitDescription)
                    .LogEdit("IdObrazka", _data.Unit.PictureID.ToString(), SelectedPicture.ToString());
            }
            else
            {
                _data.LogInfo = "".LogCreate("Nazwa", UnitName)
                    .LogCreate("NadrzędnaJO", _data.ParentUnit.Name, _data.ParentUnit.ID)
                    .LogCreate("Opis", UnitDescription)
                    .LogCreate("IdObrazka", SelectedPicture.ToString());
            }

            _data.Unit.UnitTypeIDv2 = (int)unitTypes;
            _data.Unit.Name = UnitName;
            _data.Unit.Description = UnitDescription;
            _data.Unit.PictureID = SelectedPicture;
            _data.Reason = Reason;
        }
    }
}
