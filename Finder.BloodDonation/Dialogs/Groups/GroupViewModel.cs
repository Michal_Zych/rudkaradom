﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Dialogs.Patients;
using Finder.BloodDonation.LanguageHelpher;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Dialogs.Groups
{
    public class GroupViewModel : ViewModelBase, IDialogWindow
    {
        private GroupData _data;

        [RaisePropertyChanged]
        public string Title { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<GroupDto> GroupsList { get; set; }

        [RaisePropertyChanged]
        public int SelectedGroupIndex { get; set; }

        [RaisePropertyChanged]
        public string Reason { get; set; }

        [RaisePropertyChanged]
        public string ReasonLabel { get { return LanguageManager.GetControlsText(ControlsTextCodes.REASON); } }

        public RelayCommand CreateGroupCommand { get; set; }
        public RelayCommand EditGroupCommand { get; set; }
        public RelayCommand RemoveGroupCommand { get; set; }

        public RelayCommand OkClick { get; set; }
        public RelayCommand CancelClick { get; set; }

        public UnitsServiceClient Proxy;

        public string WindowKey
        {
            get { return WindowsKeys.Group; }
        }

        public object Data
        {
            set 
            {
                _data = (GroupData)value;

                ClearFields();

                Proxy.TransmitterGroupsGetAsync();
            }
        }

        public void ClearFields()
        {
            GroupsList = null;
            Reason = String.Empty;
        }

        public GroupViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            Proxy = ServiceFactory.GetService<UnitsServiceClient>();
            Proxy.TransmitterGroupsGetCompleted += Proxy_TransmitterGroupsGetCompleted;
            Proxy.TransmitterGroupSetCompleted += Proxy_TransmitterGroupSetCompleted;

            CreateGroupCommand = new RelayCommand(OnCreateGroupCommand);
            EditGroupCommand = new RelayCommand(OnEditGroupCommand);

            CancelClick = new RelayCommand(OnCancelClick);
        }

        private void OnCancelClick()
        {
            Container.Resolve<IWindowsManager>().CloseWindow(WindowsKeys.Group);
        }

        public void Proxy_TransmitterGroupSetCompleted(object sender, TransmitterGroupSetCompletedEventArgs e)
        {
            int t = -1;

            if(int.TryParse(e.Result.ToString(),out t))
            {
                MsgBox.Succes("Operacja zakończona powodzeniem."); 
            }
            else
            {
                MsgBox.Warning("Operacja nie powiodła się.");
            }
        }

        private void OnEditGroupCommand()
        {
            if (SelectedGroupIndex >= 0)
            {
                MsgBox.Prompt("Edycja grupy " + GroupsList[SelectedGroupIndex].Name, "Nazwa:", OnEditGroup);
            }
            else
            {
                MsgBox.Warning("Nie wybrano elementu do edycji.");
            }
        }

        private void OnEditGroup(object sender, Telerik.Windows.Controls.WindowClosedEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.PromptResult))
            {
                GroupsList[SelectedGroupIndex].Name = e.PromptResult;

                Proxy.TransmitterGroupSetAsync(GroupsList[SelectedGroupIndex]);
            }
        }

        private void OnCreateGroupCommand()
        {
            MsgBox.Prompt("Tworzenie grupy", "Nazwa:", OnCreateGroup);
        }

        private void OnCreateGroup(object sender, Telerik.Windows.Controls.WindowClosedEventArgs e)
        {
            if(!String.IsNullOrEmpty(e.PromptResult))
            {
                GroupDto dto = new GroupDto() { Id = null, Name = e.PromptResult, Reason = String.Empty };

                Proxy.TransmitterGroupSetAsync(dto);
            }
        }

        public void Proxy_TransmitterGroupsGetCompleted(object sender, TransmitterGroupsGetCompletedEventArgs e)
        {
            if(e.Result != null)
            {
                GroupsList = new ObservableCollection<GroupDto>(e.Result);
            }
        }
    }
}
