﻿using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Tabs.TransmitterEvents;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Dialogs.Transmitters
{
    [OnCompleted]
    [UIException]
    public class TransmitterEventsModel : ViewModelBase, IDialogWindow
    {
        private TransmitterEventsData _data;

        [RaisePropertyChanged]
        public string Title { get; set; }

        private ITabViewModel _transmitterEventsDetails;
        [RaisePropertyChanged]
        public ITabViewModel TransmitterEventsDetails
        {
            get
            {
                if (_transmitterEventsDetails == null)
                {
                    _transmitterEventsDetails = Container.Resolve<TransmitterEventsViewModel>();
                }
                return _transmitterEventsDetails;
            }
            set
            {
                _transmitterEventsDetails = value;
            }
        }

        public TransmitterEventsModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            this.ViewAttached += new EventHandler(ViewModel_ViewAttached);
            EventAggregator.GetEvent<RequestToRepeatMessageEvent>().Subscribe(OnMessageListener);
        }

        public void OnMessageListener(object obj)
        {
            if(_data != null)
            {
                EventAggregator.GetEvent<TransferTransmitterIdEvent>().Publish(_data.SelectedTransmitterId);
            }
        }

        public object Data
        {
            set
            {
                _data = (TransmitterEventsData)value;

                if (_data != null)
                {
                    Title = "Zdarzenia transmittera";

                    EventAggregator.GetEvent<TransferTransmitterIdEvent>().Publish(_data.SelectedTransmitterId);
                }
            }
        }

        private void ViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public event EventHandler LoadingCompleted = delegate { };


        public string WindowKey
        {
            get { return WindowsKeys.TransmitterEventsDialog; }
        }
    }
}
