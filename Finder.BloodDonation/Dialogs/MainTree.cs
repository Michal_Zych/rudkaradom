﻿using Finder.BloodDonation.Model.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Dialogs.MainTree
{
    public class MainTree
    {
        public IEnumerable<int> FilterByMainTree(UnitDTO unit)
        {
            UnitDTO parent = unit;
            Queue<UnitDTO> queue = new Queue<UnitDTO>();

            queue.Enqueue(parent);

            while (queue.Count > 0)
            {
                parent = queue.Dequeue();
                yield return parent.ID;

                for (int i = 0; i < parent.Children.Count; i++)
                {
                    if (parent.Children[i] != null)
                    {
                        queue.Enqueue(parent.Children[i]);
                    }
                }
            }
        }


    }
}

