﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation;
using Telerik.Windows.Controls;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;

namespace Finder.BloodDonation.Dialogs.Backup
{
    public class BackupEditModel : ViewModelBase , IDialogWindow
    {
        BackupEditData _data;

        public string WindowKey
        {
            get { return WindowsKeys.EditBackup; }
        }

        public object Data
        {
            set 
            {
                _data = (BackupEditData)value;
            }
        }


    }
}
