﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Converters;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Settings;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using ViewModelBase = FinderFX.SL.Core.MVVM.ViewModelBase;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;

namespace Finder.BloodDonation.Dialogs.Messages
{
    public class MessageDialogModel : ViewModelBase, IDialogWindow
    {
        private MessageDialogData _data;

        [RaisePropertyChanged]
        public string Title { get; set; }

        [RaisePropertyChanged]
        public string Message { get; set; }

        private DateTime _activeFrom;

        [RaisePropertyChanged]
        public DateTime ActiveFromDate { get; set; }

        [RaisePropertyChanged]
        public DateTime ActiveFromTime { get; set; }

        private DateTime _activeTo;

        [RaisePropertyChanged]
        public DateTime ActiveToDate { get; set; }

        [RaisePropertyChanged]
        public DateTime ActiveToTime { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<string> TypeList { get; set; }

        [RaisePropertyChanged]
        public int SelectedType { get; set; }

        public RelayCommand OKClick { get; set; }

        public RelayCommand CancelClick { get; set; }

        public MessageDialogModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            OKClick = new RelayCommand(OnOKClick);
            CancelClick = new RelayCommand(OnCancelClick);

            var list = new ObservableCollection<string>();
            for (int i = 0; i < MessageType.Name.GetLength(0); i++)
                list.Add(MessageType.Name[i]);
            TypeList = list;

        }

        public string WindowKey
        {
            get { return WindowsKeys.MessageDialog; }
        }

        object IDialogWindow.Data
        {
            set
            {
                Title = "Nowy komunikat";

                _data = (MessageDialogData)value;
                if (_data.ID.HasValue)
                {
                    Message = _data.Message;
                    SelectedType = _data.Mode;
                    _activeFrom = _data.ActiveFrom;
                    _activeTo = _data.ActiveTo;
                }
                else
                {
                    Message = "";
                    SelectedType = MessageType.Instant;
                    _activeFrom = DateTime.Now;
                    int mins = Global.Settings.Current.GetInt(ConfigurationKeys.DefMessageActivityMinutes);
                    _activeTo = _activeFrom.AddMinutes(mins);
                }

                ActiveFromDate = _activeFrom.Date;
                ActiveFromTime = _activeFrom;
                ActiveToDate = _activeTo.Date;
                ActiveToTime = _activeTo;

            }
        }

        private void OnOKClick()
        {
            Close(CloseButton.OK);
        }
        private void OnCancelClick()
        {
            Close(CloseButton.Cancel);
        }

        private void Close(CloseButton button)
        {
            if (button == CloseButton.OK)
            {
                string errorMsg = "";
                if (string.IsNullOrWhiteSpace(Message))
                {
                    errorMsg = "Treść komunikatu nie może być pusta.";
                }

                _activeFrom = GetActiveFrom();
                _activeTo = GetActiveTo();
                if (_activeFrom >= _activeTo)
                {
                    errorMsg = "Data końcowa musi być większa od początkowej.";
                    int mins = Global.Settings.Current.GetInt(ConfigurationKeys.DefMessageActivityMinutes);
                    _activeTo = _activeFrom.AddMinutes(mins);
                }

                if (errorMsg != "")
                {
                    MsgBox.Error(errorMsg);
                    return;
                }

                SaveData();
            }

            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        private DateTime GetActiveFrom()
        {
            return new DateTime(
        ActiveFromDate.Year,
        ActiveFromDate.Month,
        ActiveFromDate.Day,
        ActiveFromTime.Hour,
        ActiveFromTime.Minute,
        ActiveFromTime.Second);
        }
        private DateTime GetActiveTo()
        {
            return new DateTime(
                        ActiveToDate.Year,
                        ActiveToDate.Month,
                        ActiveToDate.Day,
                        ActiveToTime.Hour,
                        ActiveToTime.Minute,
                        ActiveToTime.Second
                        );
        }

        private void SaveData()
        {
            _data.Canceled = false;

            _data.Message = Message;
            _data.ActiveFrom = GetActiveFrom();
            _data.ActiveTo = GetActiveTo();
            _data.Mode = (byte)SelectedType;
        }

    }
}
