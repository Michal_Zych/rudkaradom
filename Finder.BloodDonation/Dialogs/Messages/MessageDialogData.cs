﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Dialogs.Messages
{
    public class MessageDialogData
    {
        public int? ID { get; set; }
        public int ClientID { get; set; }
        public string Message { get; set; }
        public DateTime ActiveFrom { get; set; }
        public DateTime ActiveTo { get; set; }
        public byte Mode { get; set; }

        public bool Canceled = true;
    }
}
