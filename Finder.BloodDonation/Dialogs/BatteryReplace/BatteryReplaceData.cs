﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Dialogs.BatteryReplace
{
    public class BatteryReplaceData
    {
        public int BandId { get; set; }
        public string BandDisplayName { get; set; }
        public int BatteryId { get; set; }
        public int OperatorId { get; set; }
        public string Reason { get; set; }
    }
}
