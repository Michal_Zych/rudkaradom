﻿using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Dialogs.BatteryReplace
{
    [OnCompleted]
    [UIException]
    public class BatteryReplaceViewModel : FinderFX.SL.Core.MVVM.ViewModelBase, IDialogWindow
    {
        public RelayCommand SaveActionCommand { get; set; }
        public RelayCommand CancelActionCommand { get; set; }

        [RaisePropertyChanged]
        public string Band { get; set; }

        private BatteryReplaceData _data;
        private UnitsServiceClient UnitsProxy
        {
            get
            {
                UnitsServiceClient _proxy = ServiceFactory.GetService<UnitsServiceClient>();
                _proxy.GetBatteryTypesCompleted += _proxy_GetBatteryTypesCompleted;
                _proxy.BandBatteryChangeCompleted += _proxy_BandBatteryChangeCompleted;
                return _proxy;
            }
        }

        public void _proxy_BandBatteryChangeCompleted(object sender, BandBatteryChangeCompletedEventArgs e)
        {
            if(e.Result != null && e.Result.ErrorMessage == String.Empty)
            {
                MsgBox.Succes("Wymiana baterii zakończona powodzeniem!");
            }
            else
            {
                MsgBox.Succes(e.Result.ErrorMessage);
            }
        }

        public List<BatteryTypeDTO> BatteryType { get; set; }
        private List<int> batteryIndex;

        public void _proxy_GetBatteryTypesCompleted(object sender, GetBatteryTypesCompletedEventArgs e)
        {
            BatteryType = new List<BatteryTypeDTO>();
            BatteryTypeDTO dto;

            BatteryTypes = new ObservableCollection<string>();
            batteryIndex = new List<int>();

            if (e.Result != null)
            {
                for (int i = 0; i < e.Result.Count; i++)
                {
                    BatteryTypes.Add(e.Result[i].Name);
                    batteryIndex.Add(e.Result[i].Id);

                    dto = new BatteryTypeDTO()
                    {
                        Id = e.Result[i].Id,
                        Name = e.Result[i].Name,
                        Voltage = e.Result[i].Voltage,
                        Description = e.Result[i].Description,
                        LifeMonts = e.Result[i].LifeMonts,
                        Capacity = e.Result[i].Capacity
                    };

                    BatteryType.Add(dto);
                }
            }
        }

        [RaisePropertyChanged]
        public ObservableCollection<string> BatteryTypes { get; set; }

        [RaisePropertyChanged]
        public string BatteryVlotage { get; set; }

        private int batteryTypesSelectedIndex;
        [RaisePropertyChanged]
        public int BatteryTypesSelectedIndex
        {
            get
            {
                return batteryTypesSelectedIndex;
            }
            set
            {
                const string SPACE = " ";
                const string EMPTY = "-";

                if (BatteryType != null)
                {
                    if (value >= 0 && value < BatteryType.Count)
                    {
                        BatteryVlotage = BatteryType[value].Voltage.ToString() + SPACE + "V";
                        BatteryId = BatteryType[value].Id.ToString();
                        BatteryLifeMounts = BatteryType[value].LifeMonts + SPACE + "miesięcy";
                        BatteryCapacity = BatteryType[value].Capacity + SPACE + "mAh";
                        BatteryName = BatteryType[value].Name;
                    }
                }
                else
                {
                    BatteryVlotage = "-";
                    BatteryId = "-";
                    BatteryLifeMounts = "-";
                    BatteryCapacity = "-";
                    BatteryName = "-";
                }

                batteryTypesSelectedIndex = value;
            }
        }

        [RaisePropertyChanged]
        public string Title { get; set; }

        [RaisePropertyChanged]
        public string Reason { get; set; }

        [RaisePropertyChanged]
        public string BatteryId { get; set; }

        [RaisePropertyChanged]
        public string BatteryName { get; set; }

        [RaisePropertyChanged]
        public string BatteryCapacity { get; set; }

        [RaisePropertyChanged]
        public string BatteryLifeMounts { get; set; }

        public BatteryReplaceViewModel(IUnityContainer container)
            : base(container)
        {
            BatteryTypes = new ObservableCollection<string>();
            BatteryTypesSelectedIndex = -1;

            SaveActionCommand = new RelayCommand(OnSaveActionCommand);
            CancelActionCommand = new RelayCommand(OnCloseActionCommand);
        }

        public void OnCloseActionCommand()
        {
            Close(CloseButton.Cancel);
        }

        public void OnSaveActionCommand()
        {
            Close(CloseButton.OK);
        }

        public void Close(CloseButton closeType)
        {
            if (closeType == CloseButton.OK)
            {
                SaveData();
            }

            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        public void SaveData()
        {
            string msg = String.Empty;

            if(BatteryTypesSelectedIndex < 0)
            {
                msg = "Nie wybrano nowej baterii!";
            }
            if(BatteryTypes == null)
            {
                msg = "Nie odnaleziono żadnych nowych baterii!";
            }

            if(!msg.Equals(String.Empty))
            {
                MsgBox.Warning(msg);
                return;
            }

            BatteryTypeDTO dto = new BatteryTypeDTO()
            {
                Id = BatteryType[BatteryTypesSelectedIndex].Id,
                LifeMonts = BatteryType[BatteryTypesSelectedIndex].LifeMonts,
                Name = BatteryType[BatteryTypesSelectedIndex].Name,
                Voltage = BatteryType[BatteryTypesSelectedIndex].Voltage,
                Capacity = BatteryType[BatteryTypesSelectedIndex].Capacity,
                Description = BatteryType[BatteryTypesSelectedIndex].Description
            };

            UnitsProxy.BandBatteryChangeAsync(_data.BandId, dto, Reason);
        }

        private void ClearFields()
        {
            BatteryTypesSelectedIndex = -1;
            BatteryVlotage = "-";
            BatteryId = "-";
            BatteryLifeMounts = "-";
            BatteryCapacity = "-";
            BatteryName = "-";
        }

        public object Data
        {
            set
            {
                ClearFields();
                _data = (BatteryReplaceData)value;

                if (_data != null)
                {
                    Band = _data.BandDisplayName;
                    Title = "Wymiana baterii w lokalizatorze: " + _data.BandDisplayName;
                    UnitsProxy.GetBatteryTypesAsync();
                }
            }
        }

        public string WindowKey
        {
            get
            {
                return WindowsKeys.BatteryReplace;
            }
        }
    }
}