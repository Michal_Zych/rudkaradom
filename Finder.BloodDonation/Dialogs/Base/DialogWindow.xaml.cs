﻿using System;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;
using ResizeMode = Telerik.Windows.Controls.ResizeMode;
using WindowStartupLocation = Telerik.Windows.Controls.WindowStartupLocation;

namespace Finder.BloodDonation.Dialogs.Base
{
    public partial class DialogWindow : RadWindow
    {
        public bool isModal = true;
        UserControl _form;

        public object DataContext
        {
            get
            {
                return this._form.DataContext;
            }
        }

        public DialogWindow(string title, UserControl form, Size windowSize)
        {
            // You need this line to tell the DialogWindow to use your theme in Generic.xaml as a default one.
            InitializeComponent();

            Content = this._form = form;
            //DataContext = parent.DataContext;
            Height = windowSize.Height;
            Header = title;
            Width = windowSize.Width;
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            ResizeMode = ResizeMode.CanResize;
            this.SetSize(form);
        }

        private const double Per = 1.5;
        private void SetSize(UserControl form)
        {
            
            //H
            this.Height = double.IsNaN(form.Height) ? this.Height : Math.Round(form.Height * Per);
            this.MinHeight = Math.Round(form.MinHeight * Per);
            this.MaxHeight = Math.Round(form.MaxHeight * Per);

            //W
            this.Width = double.IsNaN(form.Width) ? this.Width : Math.Round(form.Width * Per);
            this.MinWidth = Math.Round(form.MinWidth * Per);
            this.MaxWidth = Math.Round(form.MaxWidth * Per);
        }

        private void FormOnSizeChanged(object sender, SizeChangedEventArgs sizeChangedEventArgs)
        {
        }

        private void SetData(object data)
        {
            var dCtx = (this.DataContext as IDialogWindow);
            dCtx.Data = data;
        }

        private void SetIsEditMode(bool IsEditMode)
        {
            var dCtx = (this.DataContext as IReuseWindow);
            dCtx.isEditMode = IsEditMode;
        }
        public void ShowDialog()
        {
            if (isAvailableSetEditMode())
                SetIsEditMode(false);
            base.ShowDialog();
        }


        public void ShowDialog(object data)
        {

            this.ShowDialog(data, null);
        }

        public void ShowDialog(object data, bool? IsEditMode)
        {
            if (IsEditMode.HasValue)
                SetIsEditMode(IsEditMode.Value);
            SetData(data);
            if (isModal)
                base.ShowDialog();
            else base.Show();
        }

        private bool isAvailableSetEditMode()
        {
            return (this.DataContext is IReuseWindow);
        }

        /// <summary>
        /// Optional
        /// </summary>
        /// <param name="data"></param>
        public void SetDataContext(object data)
        {
            if (data == null) throw new ArgumentNullException("data");

            base.DataContext = data;
        }
    }
}
