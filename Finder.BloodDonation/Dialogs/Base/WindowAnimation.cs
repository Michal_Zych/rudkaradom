﻿using System;
using System.Windows;
using System.Windows.Media.Animation;
using Telerik.Windows.Controls;

namespace Finder.BloodDonation.Dialogs.Base
{
    public static class WindowAnimation
    {
        public static readonly DependencyProperty OpenAnimationProperty =
            DependencyProperty.RegisterAttached("OpenAnimation", typeof (Storyboard), typeof (WindowAnimation),
                                                new PropertyMetadata(OnOpenAnimationChanged));

        public static readonly DependencyProperty CloseAnimationProperty =
            DependencyProperty.RegisterAttached("CloseAnimation", typeof (Storyboard), typeof (WindowAnimation),
                                                new PropertyMetadata(OnCloseAnimationChanged));

        private static readonly DependencyProperty EventWrapperProperty =
            DependencyProperty.RegisterAttached("EventWrapper", typeof (CloseEventsWrapper), typeof (WindowAnimation),
                                                null);

        public static Storyboard GetOpenAnimation(DependencyObject obj)
        {
            return (Storyboard) obj.GetValue(OpenAnimationProperty);
        }

        public static void SetOpenAnimation(DependencyObject obj, Storyboard value)
        {
            obj.SetValue(OpenAnimationProperty, value);
        }

        private static void OnOpenAnimationChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var window = d as RadWindow;
            var newValue = e.NewValue as Storyboard;
            var oldValue = e.OldValue as Storyboard;

            if (window != null)
            {
                if (oldValue == null)
                {
                    AttachOpenEvents(window);
                }
                else if (newValue == null)
                {
                    DetachOpenEvents(window);
                }
            }
        }

        private static void AttachOpenEvents(RadWindow window)
        {
            window.Opened += OnWindowOpened;
        }

        private static void DetachOpenEvents(RadWindow window)
        {
            window.Opened -= OnWindowOpened;
        }

        private static void OnWindowOpened(object sender, RoutedEventArgs e)
        {
            var window = sender as RadWindow;
            if (window != null)
            {
                Storyboard openAnimation = GetOpenAnimation(window);
                if (openAnimation != null)
                {
                    openAnimation.Completed += OnOpenAnimationCompleted;
                    openAnimation.Begin();
                }
            }
        }

        private static void OnOpenAnimationCompleted(object sender, EventArgs e)
        {
            var openAnimation = sender as Storyboard;
            if (openAnimation != null)
            {
                openAnimation.Completed -= OnOpenAnimationCompleted;
            }
        }

        public static Storyboard GetCloseAnimation(DependencyObject obj)
        {
            return (Storyboard) obj.GetValue(CloseAnimationProperty);
        }

        public static void SetCloseAnimation(DependencyObject obj, Storyboard value)
        {
            obj.SetValue(CloseAnimationProperty, value);
        }

        private static void OnCloseAnimationChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var window = d as RadWindow;
            var newValue = e.NewValue as Storyboard;
            var oldValue = e.OldValue as Storyboard;

            if (window != null)
            {
                if (oldValue == null)
                {
                    SetEventWrapper(window, new CloseEventsWrapper(window));
                }
                else if (newValue == null)
                {
                    CloseEventsWrapper wrapper = GetEventWrapper(window);
                    wrapper.DetachCloseEvents();
                    window.ClearValue(EventWrapperProperty);
                }
            }
        }

        private static CloseEventsWrapper GetEventWrapper(DependencyObject obj)
        {
            return (CloseEventsWrapper) obj.GetValue(EventWrapperProperty);
        }

        private static void SetEventWrapper(DependencyObject obj, CloseEventsWrapper value)
        {
            obj.SetValue(EventWrapperProperty, value);
        }

        #region Nested type: CloseEventsWrapper

        private class CloseEventsWrapper
        {
            private bool closing;
            private RadWindow window;

            public CloseEventsWrapper(RadWindow window)
            {
                this.window = window;
                AttachCloseEvents();
            }

            public void AttachCloseEvents()
            {
                window.PreviewClosed += OnWindowPreviewClosed;
            }

            public void DetachCloseEvents()
            {
                window.PreviewClosed -= OnWindowPreviewClosed;
                window = null;
            }

            private void OnWindowPreviewClosed(object sender, WindowPreviewClosedEventArgs e)
            {
                if (closing)
                {
                    closing = false;
                }
                else
                {
                    Storyboard closeAnimation = GetCloseAnimation(window);
                    if (closeAnimation != null)
                    {
                        e.Cancel = true;
                        closing = true;

                        closeAnimation.Completed += OnCloseAnimationCompleted;
                        closeAnimation.Begin();
                    }
                }
            }

            private void OnCloseAnimationCompleted(object sender, EventArgs e)
            {
                var openAnimation = sender as Storyboard;
                if (openAnimation != null)
                {
                    openAnimation.Completed -= OnOpenAnimationCompleted;
                    window.Close();
                }
            }
        }

        #endregion
    }
}