﻿using Finder.BloodDonation.Common;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.Common.Events;

namespace Finder.BloodDonation.Dialogs.MapUnit
{
    [OnCompleted]
    [UIException]
    public class MapUnitViewModel : FinderFX.SL.Core.MVVM.ViewModelBase, IDialogWindow
    {
        private MapUnitData _data;

        private UnitsServiceClient Proxy
        {
            get
            {
                UnitsServiceClient _tmp = ServiceFactory.GetService<UnitsServiceClient>();
                _tmp.MapExternalUnitCompleted += _tmp_MapExternalUnitCompleted;
                return _tmp;
            }
        }

        public void _tmp_MapExternalUnitCompleted(object sender, MapExternalUnitCompletedEventArgs e)
        {
            int s;

            if(e.Result != null)
            {
                if(int.TryParse(e.Result, out s))
                {
                    MsgBox.Succes("Mapowanie zakończone pomyślnie!");
                }
                else
                {
                    MsgBox.Warning(e.Result);
                }
            }

            EventAggregator.GetEvent<CloseWindowEvent>().Publish(null);
        }

        public RelayCommand SaveActionCommand { get; set; }
        public RelayCommand CancelActionCommand { get; set; }
        public RelayCommand ClearButton { get; set; }


        [RaisePropertyChanged]
        public ObservableCollection<UnitDTO> Unit { get; set; }

        [RaisePropertyChanged]
        public string SelectedUnitId
        {
            get;
            set;
        }

        [RaisePropertyChanged]
        public string UnitIdToMap { get; set; }

        [RaisePropertyChanged]
        public string Description { get; set; }

        [RaisePropertyChanged]
        public string LocalUnitId { get; set; }


        private UnitDTO _selectedUnit;
        [RaisePropertyChanged]
        public UnitDTO SelectedUnit
        {
            get
            {
                return _selectedUnit;
            }
            set
            {
                if(value != null)
                {
                    SelectedUnitId = value.ID.ToString();

                    _selectedUnit = value;
                }
            }
        }

        [RaisePropertyChanged]
        public string Reason { get; set; }

        public string WindowKey
        {
            get { return WindowsKeys.MapUnit; }
        }

        public object Data
        {
            set 
            {
                ClearFields();
                _data = (MapUnitData)value;
                SetDataToFields();
            }
        }

        private void SetDataToFields()
        {
            if(_data != null)
            {
                UnitIdToMap = _data.dtoToMap.ExtId;
                Description = _data.dtoToMap.ExtDescription;
                LocalUnitId = _data.dtoToMap.M2mId.ToString();

                Unit = new ObservableCollection<UnitDTO>() { UnitManager.Instance.RootUnit };
                Unit[0].Parent = null;
                Unit[0].SetAllIsSelectedProperty(false);     
           
                if(!String.IsNullOrEmpty(LocalUnitId))
                {

                    SelectedUnit = UnitManager.Instance.Units.FirstOrDefault(x => x.ID == int.Parse(LocalUnitId));
                }
            }
        }

        private void ClearFields()
        {
            SelectedUnit = null;
            Reason = String.Empty;
            UnitIdToMap = String.Empty;
            Description = String.Empty;
            LocalUnitId = String.Empty;
        }

        public MapUnitViewModel(IUnityContainer container)
            : base(container)
        {
            SaveActionCommand = new RelayCommand(OnSaveCommand);
            CancelActionCommand = new RelayCommand(OnCancelCommand);
            ClearButton = new RelayCommand(OnClearCommand);
        }

        private void OnClearCommand()
        {
            SelectedUnit = null;
            SelectedUnitId = null;
        }

        private void OnCancelCommand()
        {
            Close(CloseButton.Cancel);
        }

        private void OnSaveCommand()
        {
            Close(CloseButton.OK);
        }

        private void Close(CloseButton closeButton)
        {
            switch(closeButton)
            {
                case CloseButton.OK:
                    SaveData();
                    break;

                case CloseButton.Cancel:
                    break;
            }

            Container.Resolve<IWindowsManager>().CloseWindow(this.WindowKey);
        }

        private void SaveData()
        {
            if(_data != null)
            {
                int _t;
                int? m2mId = int.TryParse(SelectedUnitId, out _t) ? (int?)_t : null;

                Proxy.MapExternalUnitAsync(_data.dtoToMap.ExtId, m2mId, Finder.BloodDonation.Common.Authentication.BloodyUser.Current.ID, Reason);
            }
        }
    }
}
