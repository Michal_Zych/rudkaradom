﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;

namespace Finder.BloodDonation.Dialogs.MapUnit
{
    [ViewModel(typeof(MapUnitViewModel))]
    public partial class MapUnitView : UserControl
    {
        public MapUnitView()
        {
            InitializeComponent();
        }

        private void Button_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
        }

        private void Button_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
        }
    }
}
