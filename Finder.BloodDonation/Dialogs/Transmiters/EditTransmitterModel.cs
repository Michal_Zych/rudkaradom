﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Tools;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.Dialogs.Tree;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using Microsoft.Practices.Composite.Presentation.Regions;
using System.Collections.ObjectModel;
using Finder.BloodDonation.Dialogs.Groups;

namespace Finder.BloodDonation.Dialogs.Transmiters
{
    public class EditTransmitterModel : ViewModelBase, IDialogWindow
    {
        private TransmitterEditData _data;

        public RelayCommand OkClick { get; set; }
        public RelayCommand CancelClick { get; set; }
        public RelayCommand AssignedToClickCommand { get; set; }
        public RelayCommand TypeSelectionChangedCommand { get; set; }

        public RelayCommand addClick { get; set; }
        public RelayCommand removeClick { get; set; }
        public RelayCommand manageClick { get; set; }

        public RelayCommand SaveActionCommand { get; set; }

        [RaisePropertyChanged]
        public SliderConfigurator SensitivityConfiguration { get; set; }

        [RaisePropertyChanged]
        public bool? IsTransmitterBandUpdater { get; set; }

        [RaisePropertyChanged]
        public bool IsMSISDNEnableState { get; set; }

        private int transmitterSelectedTypeIndex;
        [RaisePropertyChanged]
        public int TransmitterSelectedTypeIndex {
            get
            {
                return transmitterSelectedTypeIndex;
            }
            set
            {
                if(transmitterSelectedTypeIndex != value)
                {
                    if(value == 0)
                    {
                        IsMSISDNEnableState = false;
                    }
                    else
                    {
                        IsMSISDNEnableState = true;
                    }
                }
                transmitterSelectedTypeIndex = value;
            }
        }

        [RaisePropertyChanged]
        public string IpAddress { get; set; }

        [RaisePropertyChanged]
        public short RssiTreshold { get; set; }

        [RaisePropertyChanged]
        public string Title { get; set; }
        [RaisePropertyChanged]
        public string TransmitterId { get; set; }
        [RaisePropertyChanged]
        public string TransmitterMacString { get; set; }
        [RaisePropertyChanged]
        public string TransmitterIp { get; set; }
        [RaisePropertyChanged]
        public string TransmitterNote { get; set; }
        [RaisePropertyChanged]
        public string TransmitterMSISDN { get; set; }
        [RaisePropertyChanged]
        public string TransmitterInstallationName { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<GroupDto> GroupsList { get; set; }

        [RaisePropertyChanged]
        public int SelectedGroupIndex { get; set; }

        [RaisePropertyChanged]
        public Visibility AddGroupVisibility { get; set; }

        [RaisePropertyChanged]
        public int AddGroupSelectedIndex { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<GroupDto> AddedGroupsList { get; set; }

        [RaisePropertyChanged]
        public int AddedSelectedGroupIndex { get; set; }

        private void SetDataToField()
        {
            IsTransmitterBandUpdater = _data.Transmitter.IsBandUpdater; 
            TransmitterId = _data.Transmitter.Id.ToString();
            TransmitterMacString = _data.Transmitter.MacString;
            TransmitterMSISDN = _data.Transmitter.MSISDN;
            TransmitterSelectedTypeIndex = _data.Transmitter.Type;

            if (TransmitterSelectedTypeIndex == (byte)TransmitterType.GSM)
            {
                IsMSISDNEnableState = true;
            }
            else
            {
                IsMSISDNEnableState = false;
            }

            RssiTreshold = _data.Transmitter.RssiTreshold == null ? (short)0 : (short)_data.Transmitter.RssiTreshold;
            IpAddress = _data.Transmitter.IpAddress;
            treeData = new TreeUnitViewData();
            treeData.Unit = UnitManager.Instance.Units.FirstOrDefault(x => x.ID == _data.Transmitter.UnitId);
            TransmitterNote = _data.Transmitter.Description;
            SensitivityConfiguration = new SliderConfigurator(this.Container, "%", true);
            SensitivityConfiguration.SetSelectedIndexWithValue(_data.Transmitter.Sensitivity.ToString());
        }

        private ObservableCollection<GroupDto> GetAddedGroups(string groups)
        {
            ObservableCollection<GroupDto> temp = new ObservableCollection<GroupDto>();

            if (!String.IsNullOrEmpty(groups))
            {
                string[] groupsArray = groups.Split(',');

                for (int j = 0; j < groupsArray.Length; j++)
                {
                    for (int i = 0; i < GroupsList.Count; i++)
                    {
                        if (!String.IsNullOrEmpty(groupsArray[j]) && GroupsList[i].Id == int.Parse(groupsArray[j]))
                        {
                            temp.Add(GroupsList[i]);
                        }
                    }
                }
            }

            return temp;
        }

        private void ClearFields()
        {
            IsTransmitterBandUpdater = false;
            TransmitterId = String.Empty;
            TransmitterMacString = String.Empty;
            TransmitterMSISDN = String.Empty ;
            IsMSISDNEnableState = false;
            TransmitterSelectedTypeIndex = (byte)TransmitterType.LAN;
            TransmitterNote = String.Empty;
            SensitivityConfiguration.ValueIndex = 50;
            IpAddress = String.Empty;
            SelectedGroupIndex = -1;
            RssiTreshold = 0;
        }

        [RaisePropertyChanged]
        public string AssignedTo { get; set; }

        [RaisePropertyChanged]
        public string BandLocation { get; set; }

        private TreeUnitViewData treeData;
        private readonly IWindowsManager _windowsManager;
        private UnitsServiceClient Proxy { get; set; }

        public string WindowKey
        {
            get { return WindowsKeys.EditTransmitter; }
        }

        public object Data
        {
            set {
                _data = (TransmitterEditData)value;

                if(_data != null)
                {
                    if (_data.Transmitter.Id != null)
                    {
                        short s = _data.Transmitter.Id == null ? (short)-1 : (short)_data.Transmitter.Id;
                        Proxy.TransmitterConfigurationGetAsync(s);
                    }

                    if(_data.Transmitter.Id == null)
                        Proxy.TransmitterGroupsGetAsync();

                    if (_data.Transmitter.Id != null)
                    {
                        Title = "Edycja transmitera";

                        SetDataToField();
                    }
                    else
                    {
                        Title = "Dodawanie transmitera";

                        ClearFields();
                    }
                }
            }
        }

        public void OnAssignedToClickCommand()
        {
            treeData = new TreeUnitViewData()
            {
                ViewMode = TreeViewMode.OnlyOneSelect
            };

            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseAssignToWindow);
            _windowsManager.ShowWindow(WindowsKeys.TreeUnitHelpher, treeData);
        }

        public void CloseAssignToWindow(string obj)
        {
            _data.Transmitter.AssignUnitId = treeData.Unit.ID;
            AssignedTo = treeData.Unit.Name;
        }

        public EditTransmitterModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            OkClick = new RelayCommand(OnOkClick);
            CancelClick = new RelayCommand(OnCancelClick);
            AssignedToClickCommand = new RelayCommand(OnAssignedToClickCommand);
            SaveActionCommand = new RelayCommand(OnSaveActionCommand);
            TypeSelectionChangedCommand = new RelayCommand(OnTypeSelectionChanged);

            Proxy = ServiceFactory.GetService<UnitsServiceClient>();
            Proxy.TransmitterConfigurationSetCompleted += Proxy_TransmitterConfigurationSetCompleted;
            Proxy.TransmitterConfigurationGetCompleted += Proxy_TransmitterConfigurationGetCompleted;
            Proxy.TransmitterGroupsGetCompleted += Proxy_TransmitterGroupsGetCompleted;

            var viewFactory = unityContainer.Resolve<ViewFactory>();
            _windowsManager = unityContainer.Resolve<IWindowsManager>();
            _windowsManager.RegisterWindowIfNotRegister(WindowsKeys.TreeUnitHelpher,  "Edytuj", viewFactory.CreateView<TreeUnitHelpher>(), new Size(550, 650), false, false);
            _windowsManager.RegisterWindow(WindowsKeys.Group, "", viewFactory.CreateView<GroupView>(), new Size(500, 290), false, false);

            SensitivityConfiguration = new SliderConfigurator(unityContainer, String.Empty, true);

            addClick = new RelayCommand(OnAddClick);
            removeClick = new RelayCommand(OnRemoveClick);
            manageClick = new RelayCommand(OnManageClick);

            if(_data != null)
            {
               if(_data.Transmitter.Id != null)
               {
                   Title = "Edycja transmitera";
                   TransmitterId = _data.Transmitter.Id.ToString();
                   TransmitterMacString = _data.Transmitter.MacString;
                   TransmitterSelectedTypeIndex = _data.Transmitter.Type;

                   if (TransmitterSelectedTypeIndex == (int)TransmitterType.GSM)
                   {
                       IsMSISDNEnableState = true;
                       TransmitterMSISDN = _data.Transmitter.MSISDN;
                   }
                   else
                   {
                       IsMSISDNEnableState = false;
                   }

                   SensitivityConfiguration.ValueIndex = _data.Transmitter.Sensitivity;

                   TransmitterInstallationName = UnitManager.Instance.Units
                                                   .Where(e => e.ID == _data.Transmitter.Id)
                                                   .FirstOrDefault().Name;

                   TransmitterNote = _data.Transmitter.Description;
               }
               else
               {
                   Title = "Dodawanie transmitera";
                   TransmitterSelectedTypeIndex = (byte)TransmitterType.LAN;
               }
            }
        }

        void Proxy_TransmitterConfigurationGetCompleted(object sender, TransmitterConfigurationGetCompletedEventArgs e)
        {
            if(e.Result != null)
            {
                var t = e.Result.FirstOrDefault(x => x.Id == _data.Transmitter.Id);
                _data.Transmitter.Name = t.Name;
                _data.Transmitter.Mac = t.Mac;
                _data.Transmitter.MacString = t.MacString;
                _data.Transmitter.MSISDN = t.MSISDN;
                _data.Transmitter.ReportIntervalSec = t.ReportIntervalSec;
                _data.Transmitter.RssiTreshold = t.RssiTreshold;
                _data.Transmitter.Sensitivity = t.Sensitivity;
                _data.Transmitter.SwitchDelayS = t.SwitchDelayS;
                _data.Transmitter.Transitions = t.Transitions;
                _data.Transmitter.TransmitterInstallationUnit = t.TransmitterInstallationUnit;
                _data.Transmitter.Type = t.Type;
                _data.Transmitter.UnitId = t.UnitId;
            }

            Proxy.TransmitterGroupsGetAsync();
        }

        void Proxy_TransmitterGroupsGetCompleted(object sender, TransmitterGroupsGetCompletedEventArgs e)
        {
            if(e.Result != null)
            {
                GroupsList = new ObservableCollection<GroupDto>(e.Result);

                var t = GroupsList.FirstOrDefault(x => x.Id == _data.Transmitter.Group);

                if (t != null)
                {
                    SelectedGroupIndex = int.Parse(t.Id.ToString());
                }

                AddedGroupsList = GetAddedGroups(_data.Transmitter.Transitions);
            }
        }

        public void OnManageClick()
        {
            _windowsManager.ShowWindow(WindowsKeys.Group, new GroupData());
        }

        public void OnRemoveClick()
        {
            List<GroupDto> _temp = CreateTemporyList();

            if(AddedSelectedGroupIndex >= 0)
            {
                _temp.RemoveAt(AddedSelectedGroupIndex);

                AddedGroupsList = new ObservableCollection<GroupDto>(_temp);
            }
            else
            {
                MsgBox.Warning("Nie wybrano grupy do usunięcia.");
            }
        }

        public void OnAddClick()
        {
            List<GroupDto> _temp = CreateTemporyList();

            if(AddGroupSelectedIndex >= 0)
            {
                _temp.Add(GroupsList[AddGroupSelectedIndex]);
            }

            AddedGroupsList = new ObservableCollection<GroupDto>(_temp);
        }

        public List<GroupDto> CreateTemporyList()
        {
            List<GroupDto> _temp = new List<GroupDto>();

            if (AddedGroupsList != null)
            {
                _temp = new List<GroupDto>(AddedGroupsList);
            }

            return _temp;
        }

        private void OnTypeSelectionChanged()
        {
            if(TransmitterSelectedTypeIndex == (int)TransmitterType.GSM)
            {
                IsMSISDNEnableState = true;
            }
            else
            {
                IsMSISDNEnableState = false;
            }
        }

        void Proxy_TransmitterConfigurationSetCompleted(object sender, TransmitterConfigurationSetCompletedEventArgs e)
        {
            if (e.Result != string.Empty)
            {
                MsgBox.Warning(e.Result);
            }
            else
            {
                MsgBox.Succes("Sukces!");
                Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
            }
        }

        public void OnSaveActionCommand()
        {
            Cancel(CloseButton.OK);
        }

        private void Cancel(CloseButton action)
        {
            long _temp;

            if (action == CloseButton.OK)
            {
                string errorMsg = "";

                if(TransmitterSelectedTypeIndex < 0)
                {
                    errorMsg = "Nie wybrano typu transmittera.";
                }
                if(string.IsNullOrEmpty(TransmitterMacString) && !long.TryParse(TransmitterMacString, out _temp))
                {
                    errorMsg = "Podany Mac jest nieprawidłowy.";
                }
                if(TransmitterSelectedTypeIndex.Equals((int)TransmitterType.GSM))
                {
                    if(string.IsNullOrEmpty(TransmitterMSISDN))
                    {
                        errorMsg = "Nie podano MSISDN";
                    }
                }
                if(!errorMsg.Equals(String.Empty))
                {
                    MsgBox.Warning(errorMsg);
                    return;
                }

                SaveData();
            }

            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        public void OnCancelClick()
        {
            Cancel(CloseButton.Cancel);
        }

        public void OnOkClick()
        {
            Cancel(CloseButton.OK);
        }

        public Boolean CheckIPValid(String strIP)
        {
            if (!String.IsNullOrEmpty(strIP))
            {
                char chrFullStop = '.';
                string[] arrOctets = strIP.Split(chrFullStop);
                if (arrOctets.Length != 4)
                {
                    return false;
                }

                Int16 MAXVALUE = 255;
                Int32 temp; // Parse returns Int32
                foreach (String strOctet in arrOctets)
                {
                    if (strOctet.Length > 3)
                    {
                        return false;
                    }

                    temp = int.Parse(strOctet);
                    if (temp > MAXVALUE)
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        private void SaveData()
        {
            long t;

            int separatorCount = 0;

            for (int i = 0; i < TransmitterMacString.Length; i++)
            {
                if (TransmitterMacString[i].Equals(":"))
                {
                    separatorCount++;
                }
            }

            if ((separatorCount < 5 && TransmitterMacString.Length != 12) || (separatorCount == 5 && TransmitterMacString.Length != 17))
            {
                MsgBox.Warning("Podany adres MAC jest nieprawidłowy!");
                return;
            }

            string hex = TransmitterMacString.Replace(":", String.Empty);

            UInt64 macAsNumber = Convert.ToUInt64(hex, 16);
            long mac = long.Parse(macAsNumber.ToString());
            _data.Transmitter.Mac = mac;
            _data.Transmitter.MacString = TransmitterMacString;

            if (TransmitterSelectedTypeIndex == (int)TransmitterType.GSM)
            {
                _data.Transmitter.MSISDN = TransmitterMSISDN;
            }

            _data.Transmitter.Sensitivity = Convert.ToByte(SensitivityConfiguration.ValueIndex);
            _data.Transmitter.IsBandUpdater = IsTransmitterBandUpdater;
            _data.Transmitter.Description = TransmitterNote;
            _data.Transmitter.Type = TransmitterSelectedTypeIndex;

            if (SelectedGroupIndex >= 0 && SelectedGroupIndex < GroupsList.Count)
            {
                _data.Transmitter.Group = (byte)GroupsList[SelectedGroupIndex].Id;
            }

            if (CheckIPValid(IpAddress))
            {
                _data.Transmitter.IpAddress = IpAddress;
            }
            else
            {
                MsgBox.Warning("Adres Ip jest nieprawidłowy.");
                return;
            }

            _data.Transmitter.RssiTreshold = RssiTreshold;
            _data.Transmitter.Transitions = String.Empty;
            for (int i = 0; i < AddedGroupsList.Count; i++)
            {
                if (i > 0)
                {
                    _data.Transmitter.Transitions += ",";
                }

                _data.Transmitter.Transitions += AddedGroupsList[i].Id.ToString();
            }

            Proxy.TransmitterConfigurationSetAsync(_data.Transmitter);
        }
    }
}
