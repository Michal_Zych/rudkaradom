﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;

namespace Finder.BloodDonation.Dialogs.Transmiters
{
    [ViewModel(typeof(EditTransmitterModel))]
    public partial class EditTransmiter : UserControl
    {
        public EditTransmiter()
        {
            InitializeComponent();
        }

        private void type_ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            SaveBtn.Focus();
        }
    }
}
