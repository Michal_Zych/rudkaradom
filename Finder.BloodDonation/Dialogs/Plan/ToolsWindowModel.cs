﻿using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Converters;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Tabs.UnitsList;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using ViewModelBase = FinderFX.SL.Core.MVVM.ViewModelBase;

namespace Finder.BloodDonation.Dialogs.Plan
{
    public class ToolsWindowModel : ViewModelBase, IDialogWindow
    {
        private object _data;

        private RoomPlanViewModel item;

        [RaisePropertyChanged]
        public RoomPlanViewModel _item 
        { 
            get 
            { 
                return item;
            } 
            
            set 
            { 
                if(value != null)
                {
                    // Poprawić ( Brak poprawnej synchronizacji ).

                    //_data = value;
                    item = value;

                    /*
                     * Name = value.Unit.Name;
                    Location = value.Unit.Location;

                    Left = item.Unit.TLY * SCALE;
                    Top = item.Unit.TLX * SCALE;
                    Width = (item.BR.Y - item.TL.Y) * SCALE;
                    Height = (item.BR.X - item.TL.X) * SCALE;
                     */
                }
            } 
        }
        public const int SCALE = 100;
        private double DEFAULT_WIDTH = 30;
        private double DEFAULT_HEIGHT = 30;

        [RaisePropertyChanged]
        public string Name { get; set; }

        [RaisePropertyChanged]
        public string Location { get; set; }

        [RaisePropertyChanged]
        public double Left { get; set; }

        [RaisePropertyChanged]
        public double Top { get; set; }

        [RaisePropertyChanged]
        public double Height { get; set; }

        [RaisePropertyChanged]
        public double Width { get; set; }

        [RaisePropertyChanged]
        public double MemoryLeft { get; set; }

        [RaisePropertyChanged]
        public double MemoryTop { get; set; }

        [RaisePropertyChanged]
        public double MemoryHeight { get; set; }

        [RaisePropertyChanged]
        public double MemoryWidth { get; set; }

        [RaisePropertyChanged]
        public RelayCommand ToMemory { get; set; }

        [RaisePropertyChanged]
        public RelayCommand LeftToMemory { get; set; }

        [RaisePropertyChanged]
        public RelayCommand TopToMemory { get; set; }

        [RaisePropertyChanged]
        public RelayCommand WidthToMemory { get; set; }

        [RaisePropertyChanged]
        public RelayCommand HeightToMemory { get; set; }

        [RaisePropertyChanged]
        public RelayCommand FromMemory { get; set; }

        [RaisePropertyChanged]
        public RelayCommand LeftFromMemory { get; set; }

        [RaisePropertyChanged]
        public RelayCommand TopFromMemory { get; set; }

        [RaisePropertyChanged]
        public RelayCommand WidthFromMemory { get; set; }

        [RaisePropertyChanged]
        public RelayCommand HeightFromMemory { get; set; }

        [RaisePropertyChanged]
        public RelayCommand<RoutedPropertyChangedEventArgs<double>> LeftChanged { get; set; }

        [RaisePropertyChanged]
        public RelayCommand<RoutedPropertyChangedEventArgs<double>> TopChanged { get; set; }

        [RaisePropertyChanged]
        public RelayCommand<RoutedPropertyChangedEventArgs<double>> WidthChanged { get; set; }

        [RaisePropertyChanged]
        public RelayCommand<RoutedPropertyChangedEventArgs<double>> HeightChanged { get; set; }


        public ToolsWindowModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            EventAggregator.GetEvent<SizeLocationChangedEvent>().Subscribe(SizeLocationChangedEventHandler);
            ToMemory = new RelayCommand(OnToMemory);
            LeftToMemory = new RelayCommand(OnLeftToMemory);
            TopToMemory = new RelayCommand(OnTopToMemory);
            WidthToMemory = new RelayCommand(OnWidthToMemory);
            HeightToMemory = new RelayCommand(OnHeightToMemory);

            FromMemory = new RelayCommand(OnFromMemory);
            LeftFromMemory = new RelayCommand(OnLeftFromMemory);
            TopFromMemory = new RelayCommand(OnTopFromMemory);
            WidthFromMemory = new RelayCommand(OnWidthFromMemory);
            HeightFromMemory = new RelayCommand(OnHeightFromMemory);

            LeftChanged = new RelayCommand<RoutedPropertyChangedEventArgs<double>>(OnLeftChanged);
            TopChanged = new RelayCommand<RoutedPropertyChangedEventArgs<double>>(OnTopChanged);
            WidthChanged = new RelayCommand<RoutedPropertyChangedEventArgs<double>>(OnWidthChanged);
            HeightChanged = new RelayCommand<RoutedPropertyChangedEventArgs<double>>(OnHeightChanged);

            MemoryHeight = DEFAULT_HEIGHT;
            MemoryWidth = DEFAULT_WIDTH;
        }

        #region FromMemory
        public void OnHeightFromMemory()
        {
            Height = MemoryHeight;
        }

        public void OnWidthFromMemory()
        {
            Width = MemoryWidth;
        }

        public void OnTopFromMemory()
        {
            Top = MemoryTop;
        }

        public void OnLeftFromMemory()
        {
            Left = MemoryLeft;
        }

        public void OnFromMemory()
        {
            _MoveFromPlanToWindow = true; 
            Height = MemoryHeight;
            Width = MemoryWidth;
            Top = MemoryTop;
            Left = MemoryLeft;
            _MoveFromPlanToWindow = false;
            LocationSizeChanged();
        }
        #endregion


        #region ToMemory
        public void OnHeightToMemory()
        {
            MemoryHeight = Height;
        }

        public void OnWidthToMemory()
        {
            MemoryWidth = Width;
        }

        public void OnTopToMemory()
        {
            MemoryTop = Top;
        }

        public void OnLeftToMemory()
        {
            MemoryLeft = Left;
        }

        public void OnToMemory()
        {
            OnHeightToMemory();
            OnWidthToMemory();
            OnLeftToMemory();
            OnTopToMemory();
        }
        #endregion

        public void SizeLocationChangedEventHandler(object e)
        {
            _MoveFromPlanToWindow = true;
            _item = e as RoomPlanViewModel;
            if (_item == null)
            {
                Left = 0;
                Top = 0;
                Width = 0;
                Height = 0;
                Name = "nie wybrano";
                Location = "";
            }
            else
            {
                Left = _item.TL.Y * SCALE;
                Top = _item.TL.X * SCALE;
                Width = (_item.BR.Y - _item.TL.Y) * SCALE;
                Height = (_item.BR.X - _item.TL.X) * SCALE;
                _MoveFromPlanToWindow = false;

                Name = _item.Unit.Name;
                Location = _item.Unit.Location;
            }
        }

        private bool _MoveFromPlanToWindow = false;
        
        public void OnLeftChanged(RoutedPropertyChangedEventArgs<double> arg)
        {
            Left = arg.NewValue;
            LocationSizeChanged();
        }
        public void OnTopChanged(RoutedPropertyChangedEventArgs<double> arg)
        {
            Top = arg.NewValue;
            LocationSizeChanged();
        }
        public void OnWidthChanged(RoutedPropertyChangedEventArgs<double> arg)
        {
            Width = arg.NewValue;
            LocationSizeChanged();
        }
        public void OnHeightChanged(RoutedPropertyChangedEventArgs<double> arg)
        {
            Height = arg.NewValue;
            LocationSizeChanged();
        }
        


        public void LocationSizeChanged()
        {
            if (_MoveFromPlanToWindow || _item == null) return;
            
            _item.Unit.TLY = Left / SCALE;
            _item.Unit.TLX = Top / SCALE;
            _item.Unit.BRY = (Width + Left) / SCALE;
            _item.Unit.BRX = (Height + Top) / SCALE;

            _item.SetSizeLocation();
        }

        public string WindowKey
        {
            get { return WindowsKeys.ToolsWindow; }
        }

        object IDialogWindow.Data
        {
            set
            {
                _data = value;

                SizeLocationChangedEventHandler(_data);
            }
        }
    }
}
