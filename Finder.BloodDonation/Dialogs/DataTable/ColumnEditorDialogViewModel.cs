﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Converters;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Settings;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using ViewModelBase = FinderFX.SL.Core.MVVM.ViewModelBase;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.DataTable;
using System.Linq;
using Finder.BloodDonation.Common.Filters;


namespace Finder.BloodDonation.Dialogs.DataTable
{
    public class ColumnEditorDialogViewModel : ViewModelBase, IDialogWindow
    {
        public IDataTable dataTable { get; set; }

        [RaisePropertyChanged]
        public string WindowTitle { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<ColumnItem> Columns { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<string> SomeList { get { return new ObservableCollection<string>() { "Lewo", "Środek", "Prawo" }; } }

        [RaisePropertyChanged]
        public int SelectedColumnIndex { get; set; }

        public RelayCommand CloseCommand { get; set; }
        public RelayCommand ColumnToLeft { get; set; }
        public RelayCommand ColumnToRight { get; set; }
        public RelayCommand SaveSettings { get; set; }

        public ColumnEditorDialogViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            CloseCommand = new RelayCommand(OnCloseCommand);
            ColumnToLeft = new RelayCommand(OnColumnToLeft);
            ColumnToRight = new RelayCommand(OnColumnToRight);
            SaveSettings = new RelayCommand(OnSaveSettings);

            WindowTitle = "Kolejność kolumn";
        }

        private void OnSaveSettings()
        {
            dataTable.SaveColumns();
        }

        private void OnColumnToRight()
        {
            if (SelectedColumnIndex < Columns.Count - 1 && SelectedColumnIndex >= 0)
            {
                dataTable.MoveColumn(Columns[SelectedColumnIndex].Name, 1);
                ArrangeRows();
            }
        }

        private void OnColumnToLeft()
        {
            if (SelectedColumnIndex > 0)
            {
                dataTable.MoveColumn(Columns[SelectedColumnIndex].Name, -1);
                ArrangeRows();
            }
        }
        private void OnCloseCommand()
        {
            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        public string WindowKey
        {
            get { return WindowsKeys.ColumnEditorDialog; }
        }


        private void ArrangeRows()
        {
            string selectedName = Columns[SelectedColumnIndex].Name;
            foreach (var column in Columns)
            {
                column.ColumnNumber = dataTable[column.Name].ColumnNumber;
            }
            var list = Columns.OrderBy(x => x.ColumnNumber);
            Columns = new ObservableCollection<ColumnItem>(list);

            int newSelected = 0;
            while (!Columns[newSelected].Name.Equals(selectedName, StringComparison.InvariantCulture))
            {
                newSelected++;
            }
            SelectedColumnIndex = newSelected;
        }


        public object Data
        {
            set { Initialize((IDataTable)value); }
        }

        private void Initialize(IDataTable dataTable)
        {
            this.dataTable = dataTable;
            var i = dataTable.Columns.Where(x => !x.IsHidden && !x.Name.Equals(String.Empty)).OrderBy(x => x.ColumnNumber);

            var cols = new ObservableCollection<ColumnItem>();
            foreach (var column in i)
            {
                var c = new ColumnItem
                {
                    dataTable = this.dataTable,
                    Name = column.Name,
                    ColumnNumber = column.ColumnNumber,
                    Label = column.Label,
                    IsVisible = column.IsVisible,
                    Alignment = column.Alignment,
                };
                cols.Add(c);
            }
            Columns = cols;
        }
    }
}
