﻿using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataTable;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Dialogs.DataTable
{
    public class ColumnItem
    {
        public string Name { get; set; }
        public int ColumnNumber { get; set; }
        public string Label { get; set; }
        public List<string> SomeList { get { return new List<string>() { "Lewo", "Środek", "Prawo" }; } }

        private TextAlignment alignment;
        public TextAlignment Alignment
        {
            get
            {
                return alignment;
            }
            set
            {
                alignment = value;
                dataTable.SetColumnAlignment(Name, value);
            }
        }

        private bool isVisible;
        public bool IsVisible
        {
            get
            {
                return isVisible;
            }
            set
            {
                isVisible = value;
                dataTable.SetColumnVisibility(Name, value);
            }
        }
        public IDataTable dataTable { get; set; }
    }
}
