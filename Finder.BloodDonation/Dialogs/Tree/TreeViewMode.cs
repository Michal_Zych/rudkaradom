﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Dialogs.Tree
{
    public enum TreeViewMode
    {
        Multiselect = 0,
        OnlyZones = 1,
        OnlyDepartments = 2,
        AssignedOfTheBand = 3,
        OnlyOneSelect
    }
}
