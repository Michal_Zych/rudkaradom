﻿using Finder.BloodDonation.Model.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Dialogs.Tree
{
    public class TreeUnitViewData
    {
        public UnitDTO Unit { get; set; }
        public TreeViewMode ViewMode { get; set; }

        public bool Closed = true;
    }
}
