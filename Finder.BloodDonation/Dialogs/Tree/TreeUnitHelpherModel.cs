﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Tools;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace Finder.BloodDonation.Dialogs.Tree
{
    public class TreeUnitHelpherModel :FinderFX.SL.Core.MVVM.ViewModelBase, IDialogWindow
    {
        private TreeUnitViewData _data;

        public RelayCommand SaveClick { get; set; }
        public RelayCommand CancelClick { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<UnitDTO> Unit { get; set; }

        [RaisePropertyChanged]
        public UnitDTO SelectedUnit { get; set; }

        [RaisePropertyChanged]
        public string Reason { get; set; }

        public string WindowKey
        {
            get { return WindowsKeys.TreeUnitHelpher; }
        }

        public object Data
        {
            set 
            {
                _data = (TreeUnitViewData)value;

                if (_data.ViewMode.Equals(TreeViewMode.OnlyOneSelect))
                {
                    Unit = new ObservableCollection<UnitDTO>() { UnitManager.Instance.RootUnit };

                    if(_data.Unit != null)
                    {
                        SelectedUnit = _data.Unit;
                    }

                    Unit[0].Parent = null; 
                    Unit[0].SetAllIsSelectedProperty(false);
                }

                Reason = "";
            }
        }

        public void SaveData()
        {
            _data.Closed = false;

            if (_data.ViewMode.Equals(TreeViewMode.OnlyOneSelect))
                _data.Unit = SelectedUnit;
        }

        private void SaveClickEvent()
        {
            Close(CloseButton.OK);
        }

        public void GetSelectedUnits(ObservableCollection<UnitDTO> Unit)
        {

        }

        public TreeUnitHelpherModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            SaveClick = new RelayCommand(SaveClickEvent);
            CancelClick = new RelayCommand(CancelClickEvent);
            Unit = new ObservableCollection<UnitDTO>();
            Unit.Add(Container.Resolve<UnitViewModel>("Root").Unit);

            this.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(EditUserPermissionsViewModel_PropertyChanged);
        }

        private void EditUserPermissionsViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            
        }

        private void Close(CloseButton closeButton)
        {
            string message = "";

            if (closeButton == CloseButton.OK)
            {
                if(SelectedUnit == null)
                {
                    message = "Nie wybrano unita!";
                }

                if (!message.Equals(String.Empty))
                {
                    MsgBox.Warning(message);
                    return;
                }

                SaveData();
            }

            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        public void CancelClickEvent()
        {
            Close(CloseButton.Cancel);
        }
    }
}
