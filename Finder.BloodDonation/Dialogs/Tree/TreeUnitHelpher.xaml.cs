﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Telerik.Windows.Controls;
using Finder.BloodDonation.Dialogs.Users;
using Finder.BloodDonation.Dialogs.Base;
using FinderFX.SL.Core.Authentication;
using Finder.BloodDonation.Model.Dto;
using System.Diagnostics;
using Finder.BloodDonation.Layout;

namespace Finder.BloodDonation.Dialogs.Tree
{
    [ViewModel(typeof(TreeUnitHelpherModel))]
    public partial class TreeUnitHelpher : UserControl, IReuseWindow
    {
        private IDynamicEventAggregator eventAggregator;

        public TreeUnitHelpher()
        {
            InitializeComponent();
            
        }

        public TreeUnitHelpher(IUnityContainer container)
        {
            InitializeComponent();
            var c = User.Current.RfidStatuses;
            //string g = ((UnitDTO)trees.Items[0]).Path;
            //trees.ExpandItemByPath(trees.Items);

            /*if (trees.Items != null)
            {
                List<UnitViewModel> OpenPath = new List<UnitViewModel>();

                UnitViewModel r = null;

                for (int i = 0; i < trees.Items.Count; i++)
                {
                    //r = trees
                }
            }*/
        }

        private void RadComboBox_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangedEventArgs e)
        {
            RadTreeView tree = e.OriginalSource as RadTreeView;
            RadComboBox combo = sender as RadComboBox;

            if ((tree == null) || (tree.SelectedItem == null) || (combo == null))
            {
                return;
            }

            combo.SelectedItem = (tree.SelectedItem as DataItem).Text;
            combo.IsDropDownOpen = false;
        }
        private bool _isEditMode;
        public bool isEditMode
        {
            get
            {
                return this._isEditMode;
            }
            set
            {
                this._isEditMode = false;
            }
        }
    }
}
