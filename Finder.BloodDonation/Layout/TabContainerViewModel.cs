﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System.Collections.ObjectModel;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Common.Events;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model;
using FinderFX.SL.Core.Authentication;
using System.Diagnostics;
using Finder.BloodDonation.Tabs;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.Model.Authentication;

namespace Finder.BloodDonation.Layout
{
    public class TabContainerViewModel : ViewModelBase, ITabManager
    {
        private IDictionary<string, ITabViewModel> _reusedTabs;

        [RaisePropertyChanged]
        public ObservableCollection<TabItem> TabViewItems { get; set; }

        [RaisePropertyChanged]
        public ITab SelectedTab { get; set; }

        [RaisePropertyChanged]
        public object SelectedItem { get; set; }

        [RaisePropertyChanged]
        public IList<ITab> Tabs { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<TabItem> SelectedTabChanged { get; set; }

        [RaisePropertyChanged]
        public IUnit SelectedUnit { get; set; }

        [RaisePropertyChanged]
        public string SelectedUnitName { get; set; }

        [RaisePropertyChanged]
        public string SelectedUnitDescription { get; set; }

        [RaisePropertyChanged]
        public string Title { get; set; }
        [RaisePropertyChanged]
        public bool IsTitleVisible { get; set; }

        [RaisePropertyChanged]
        public bool IsSubtitleVisible { get; set; }
        [RaisePropertyChanged]
        public bool ExitButtonVisible { get; set; }

        public DelegateCommand<object> ExitCommand { get; set; }

        public TabContainerViewModel(IUnityContainer container)
            : base(container)
        {
            _reusedTabs = new Dictionary<string, ITabViewModel>();

            ITabManager iam = this.Container.Resolve<ITabManager>();
            if (iam != null)
            {
                iam.Dispose();
            }
            this.Container.RegisterInstance<ITabManager>(this);

            Tabs = new List<ITab>();
            TabViewItems = new ObservableCollection<TabItem>();

            this.SelectedTabChanged = new DelegateCommand<TabItem>(OnSelectedTabChanged);

            this.EventAggregator.GetEvent<UnitChangedEvent>().Subscribe(OnUnitChangedEvent);
            this.EventAggregator.GetEvent<SelectExtendedUnitEvent>().Subscribe(SelectExtendedUnitEvent_Event);
            this.EventAggregator.GetEvent<ChangeVisibilityUserInterfaceEvent>().Subscribe(OnVisibilityChangeUserInterface);
            this.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(TabContainerViewModel_PropertyChanged);

            ExitCommand = new DelegateCommand<object>(Handle_ExitCommand);
            IsTitleVisible = true;
            IsSubtitleVisible = false;
        }

        public void OnVisibilityChangeUserInterface(bool obj)
        {
            //IsTitleVisible = obj;
        }

        private void Handle_ExitCommand(object obj)
        {
            MsgBox.Warning("Zamykam " + SelectedTab.Name);
        }

        public string SelectedTabName()
        {
            if (this.SelectedTab != null)
            {
                return this.SelectedTab.Name;
            }
            return null;
        }

        void TabContainerViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
        }

        public void SelectExtendedUnitEvent_Event(SelectedUnit unit)
        {
            var s = string.IsNullOrEmpty(unit.Unit.UnitDescription) ? unit.UnitName : unit.UnitName + " [" + unit.Unit.UnitDescription + "]";
            if (Global.Settings.Current.GetInt(Global.ConfigurationKeys.ShowUnitId) == Global.ConfigurationKeys.ENABLED)
            {
                s += BloodyUser.Current.IsInRole(PermissionNames.HyperAdmin) ? " id: " + unit.Unit.Identity
                    + "(" + unit.Unit.SelectionSource + ")" : String.Empty;
            }
            Title = s;


            SelectedUnitName = unit.UnitName;
            SelectedUnitDescription = unit.Unit.UnitDescription;
            if (Global.Settings.Current.GetInt(Global.ConfigurationKeys.ShowUnitId) == Global.ConfigurationKeys.ENABLED)
            {
                SelectedUnitDescription += BloodyUser.Current.IsInRole(PermissionNames.HyperAdmin) ? "   id: " + unit.Unit.Identity
                    + "(" + unit.Unit.SelectionSource + ")" : String.Empty;
            }
        }

        public void OnUnitChangedEvent(IUnit unit)
        {
            Debug.WriteLine("selected unit: " + unit.ToString() + "(" + unit.Identity + ")  selection source:" + unit.SelectionSource);

            this.SelectedUnit = unit;

            var i = unit.AvailableTabs;

            if (unit != null)
                RefreshTabs(unit.AvailableTabs);
            else
                RefreshTabs(new List<ITab>());
        }

        bool preventSelectionChangedEvent = false;
        private void RefreshTabs(IList<ITab> tabs)
        {
            ITab selectedTab = this.SelectedTab;

            preventSelectionChangedEvent = true;
            //todo: zastopowac wywalane taby (moze dodac nowy interfejs?)
            this.Tabs = RefreshITabs(tabs);

            this.FilterTabs();

            this.AddMissingTabs();

            TabItem item = this.TabViewItems.FirstOrDefault(x => (ITab)x.GetValue(ITabValueConverter.ITabProperty) == selectedTab);
            if (item == null)
            {
                if (this.TabViewItems.Count > 0)
                    item = this.TabViewItems[0];
            }

            if (item != null)
            {
                item.IsSelected = true;
            }

            preventSelectionChangedEvent = false;

            this.SelectedTabChanged.Execute(item);
        }

        private IList<ITab> RefreshITabs(IList<ITab> oldTabs)
        {
            IList<ITab> tempTabs = new List<ITab>();

            foreach (ITab tab in oldTabs)
            {
                ITab item = Tabs.FirstOrDefault(x => x.Name == tab.Name);
                //jezeli tab juz jest zalaodwany 
                //to go przenies
                if (item != null)
                {
                    tempTabs.Add(item);
                }
                //jezeli to nowy tab to go dodaj
                else
                {
                    tempTabs.Add(tab);
                }
            }

            return tempTabs;
        }

        private void FilterTabs()
        {
            for (int i = this.TabViewItems.Count - 1; i >= 0; i--)
            {
                TabItem item = this.TabViewItems[i];
                ITab itab = item.GetValue(ITabValueConverter.ITabProperty) as ITab;

                if (itab == null || !this.Tabs.Contains(itab))
                {
                    // tabs are not removed, are moved to reusable list
                    //if (itab != null)
                    //{
                    //    ViewModelBase vm = itab.TabViewModel as ViewModelBase;
                    //    if (vm != null)
                    //        vm.Dispose();
                    //    itab.ContentLoaded -= tab_ContentLoaded;
                    //}



                    if (!_reusedTabs.ContainsKey(itab.Name) && itab.TabViewModel != null)
                        _reusedTabs.Add(itab.Name, itab.TabViewModel);

                    item.Content = null;
                    this.TabViewItems.Remove(item);
                }
            }
        }

        private void AddMissingTabs()
        {
            for (int i = 0; i < this.Tabs.Count; i++)
            //foreach (ITab tab in this.Tabs)
            {
                ITab itab = this.Tabs[i];

                TabItem item = this.TabViewItems.FirstOrDefault(x => (ITab)x.GetValue(ITabValueConverter.ITabProperty) == itab);

                if (item == null)
                {
                    item = new TabItem();
                    item.Header = itab.Header;
                    item.SetValue(ITabValueConverter.ITabProperty, itab);

                    if (itab.TabViewModel == null)
                    {
                        itab.ContentLoaded += tab_ContentLoaded;
                    }
                    else
                    {
                        item.Content = itab.TabViewModel.View;
                    }

                    TabViewItems.Insert(i, item);

                }

            }
        }

        void tab_ContentLoaded(object sender, EventArgs e)
        {
            ITab tab = sender as ITab;

            TabItem item = this.TabViewItems.FirstOrDefault(x => (ITab)x.GetValue(ITabValueConverter.ITabProperty) == tab);
            item.Content = tab.TabViewModel.View;

            tab.TabViewModel.Refresh(this.SelectedUnit);
        }

        public void OnSelectedTabChanged(TabItem obj)
        {
            if (preventSelectionChangedEvent)
                return;

            //metoda jest wywolywana dwa razy: raz gdy selected item 
            //jest null, dwa gdy jest ustawiane
            if (obj == null)
                return;

            //R.S. Jeśli zakładka lista traci fokus zastopować odświeżanie
            //docelowo dorobić powiadomianie dowolnej zakładki o tym że traci fokus
            /*
            if (this.SelectedTab != null)
            {
                var up = this.SelectedTab.TabViewModel as UnitPreviewModel;
                if (up != null)
                {
                    var lv = up.UnitList as UnitsListView;
                    if (lv != null)
                    {
                        var lvm = null; lv.DataContext as UnitsListViewModel;
                        if (lvm != null)
                        {
                            lvm.StopRefreshing();
                        }
                    }
                }
            }
            */
            this.SelectedTab = (ITab)obj.GetValue(ITabValueConverter.ITabProperty);

            ((BloodyUser)User.Current).SetCustomProperty(ConfigurationsKeys.SELECTED_TAB, this.SelectedTab.Name);

            if (this.SelectedTab.Name == TabNames.PreviewEdit)
            {
                Bootstrapper.PlanningPreview = true;
            }
            else
            {
                Bootstrapper.PlanningPreview = false;
            }

            if (this.SelectedTab.TabViewModel == null)
            {
                //todo: sprawdz czy dany tab nie jest zapisany gdizes na pomocnicznej liscie
                if (_reusedTabs.ContainsKey(this.SelectedTab.Name))
                {
                    if (_reusedTabs[this.SelectedTab.Name] != null)
                    {
                        this.SelectedTab.TabViewModel = _reusedTabs[this.SelectedTab.Name];
                        this.SelectedTab.TabViewModel.Refresh(this.SelectedUnit);
                    }
                }
                else
                {
                    this.EventAggregator.GetEvent<LoadTabContentRequestEvent>()
                        .Publish(this.SelectedTab);
                }
            }
            else
            {
                this.SelectedTab.TabViewModel.Refresh(this.SelectedUnit);
            }
            if (!String.IsNullOrEmpty(this.SelectedTab.TabViewModel.Title))
            {
                Title = this.SelectedTab.TabViewModel.Title;

            }
            ExitButtonVisible =  (Tabs.Count == 1) &  this.SelectedTab.TabViewModel.ExitButtonVisible;
        }

        public void SelectTab(string name, Action<string> loaded)
        {
            if (TabViewItems != null)
            {
                for (int i = 0; i < TabViewItems.Count; i++)
                {
                    ITab it = (ITab)TabViewItems[i].GetValue(ITabValueConverter.ITabProperty);
                    if (it.Name == name)
                    {
                        SelectedItem = TabViewItems[i];
                        if (loaded != null)
                        {
                            if (it.TabViewModel == null || it.TabViewModel.State != ViewModelState.Loaded)
                            {
                                it.ContentLoaded +=
                                    delegate
                                    {
                                        if (loaded != null)
                                        {
                                            loaded(name);
                                        }
                                    };
                            }
                            else
                            {
                                if (loaded != null)
                                {
                                    loaded(name);
                                }
                            }
                        }
                    }
                }
            }
        }

        void Dispose()
        {

        }
    }
}
