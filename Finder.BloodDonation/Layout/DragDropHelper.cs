﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Finder.BloodDonation.Layout
{
	public interface IDraggableObject
	{
		bool Cancel { get; set; }
	}

	public static class DragDropHelper
	{
		public static T GetDropDestination<T>(Microsoft.Windows.DragEventArgs e) where T : class
		{
			return ((FrameworkElement)e.OriginalSource).DataContext as T;
		}

		public static List<T> GetDragedItems<T>(Microsoft.Windows.DragEventArgs e) where T : class
		{
			ItemDragEventArgs item = e.Data.GetData(typeof(ItemDragEventArgs)) as ItemDragEventArgs;

			return GetDragedItems<T>(item);
		}

		//bez sensu :)
		//public static T GetDragSource<T>(Microsoft.Windows.DragEventArgs e) where T : class
		//{
		//    FrameworkElement element = e.OriginalSource as FrameworkElement;
		//    if (element == null)
		//        return null;

		//    return element.DataContext as T;
		//}

	    public static bool IsDragable = false;

		public static List<T> GetDragedItems<T>(ItemDragEventArgs item) where T : class
		{
			Collection<Selection> items = item.Data as Collection<Selection>;
		    List<T> dragedItems = new List<T>();

		    if (IsDragable)
		    {
		        foreach (Selection sel in items)
		        {
		            T unit = sel.Item as T;
		            if (unit == null)
		                continue;

		            dragedItems.Add(unit);
		        }
		    }

		    return dragedItems;
		}
	}
}
