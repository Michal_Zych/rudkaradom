﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Context;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Common.Authentication;
using FinderFX.SL.Core.Authentication;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Model.Authentication;
using Microsoft.Practices.Composite.Presentation.Commands;
using System.Collections.Generic;
using Finder.BloodDonation.DataManagers;

namespace Finder.BloodDonation.Layout
{

	public class UnitContextMenuItem : GenericMenuItem
	{
		public UnitDTO Unit { get; set; }
		public string Permission { get; set; }
		public IList<int> UnitTypesV2 { get; set; }

		public UnitContextMenuItem(UnitDTO unit, string permission)
		{
			this.Unit = unit;
			this.Permission = permission;
		}

		public UnitContextMenuItem(UnitDTO unit, string permission, int requiredTypeV2)
			:this(unit,permission, new List<int> { requiredTypeV2 })
		{
			
		}

		public UnitContextMenuItem(UnitDTO unit, string permission, IList<int> requiredTypesV2)
			: this(unit, permission)
		{
			UnitTypesV2 = requiredTypesV2;
		}


		public override bool IsAvailable()
		{
			BloodyUser user = User.Current as BloodyUser;

			if (this.Unit == null)
				return false;

			bool hasPermission = user.HasPermission(
				this.Unit.ID,
				this.Permission);

			bool isProperType = 
				UnitTypesV2 == null ? 
				true : UnitTypesV2.Contains(this.Unit.UnitTypeIDv2);

			return hasPermission && isProperType;
		}
	}

	public class UnitContextCollection : GenericContextMenuItemsCollection<UnitViewModel>
	{
		public UnitContextCollection(UnitViewModel vm)
			: base(vm)
		{

		}

		public override void RegisterItems(System.Collections.Generic.IList<IContextMenuItem> items)
		{
            /*
            items.Add(new UnitContextMenuItem(AssociatedObject.Unit, PermissionNames.UnitsListing, UnitTypeOrganizer.CanBeInFavoritesList())
            {
                Header = "Dodaj do ulubionych",
                Command = new DelegateCommand<object>(AssociatedObject.OnAddToFavoritesCommand)
            });
            */
            

            //items.Add(new UnitContextMenuItem(AssociatedObject.Unit, PermissionNames.UnitsListing)
            //{
            //    Header = "Ustaw jako domyślny",
            //    Command = new DelegateCommand<object>(AssociatedObject.OnSetUnitAsDefaultCommand)
            //});

            //if (!UnitManager.Instance.CheckIfUnitIsMonitored(AssociatedObject.Unit.ID) && UnitTypeOrganizer.GetUnassignedPatientsUnitId() != AssociatedObject.Unit.ID)
            //{
            //    items.Add(new UnitContextMenuItem(AssociatedObject.Unit, PermissionNames.AddUnit)
            //    {
            //        Header = "Dodaj jednostkę organizacyjną",
            //        Command = new DelegateCommand<object>(AssociatedObject.OnAddUnitCommand)
            //    });
            //}
            /*
            items.Add(new UnitContextMenuItem(AssociatedObject.Unit, PermissionNames.AddUnit, UnitTypeOrganizer.CanBeParent())
            {
                Header = "Dodaj czujnik",
                Command = new DelegateCommand<object>(AssociatedObject.OnAddSensorCommand)
            });
             */

            //items.Add(new UnitContextMenuItem(AssociatedObject.Unit, PermissionNames.RemoveUnit)
            //{
            //    Header = "Edytuj",
            //    Command = new DelegateCommand<object>(AssociatedObject.OnEditUnitCommand)
            //});

            //items.Add(new UnitContextMenuItem(AssociatedObject.Unit, PermissionNames.RemoveUnit)
            //{
            //    Header = "Usuń",
            //    Command = new DelegateCommand<object>(AssociatedObject.OnDeleteUnitCommand)
            //});
		}
	}
}
