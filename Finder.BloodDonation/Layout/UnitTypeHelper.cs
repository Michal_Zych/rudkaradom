﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.LanguageHelpher;
using Finder.BloodDonation.Common.Authentication;

namespace Finder.BloodDonation.Layout
{
    public static class UnitTypeHelper
    {
        public static Dictionary<string, SimpleTab> AllTabs { get; private set; }

        static UnitTypeHelper()
        {
            AllTabs = new Dictionary<string, SimpleTab>();
            AllTabs.Add(TabNames.UnitPlan, new SimpleTab("Widok", TabNames.UnitPlan, PermissionNames.UnitsListing));
            AllTabs.Add(TabNames.AlarmsList, new SimpleTab("Alarmy", TabNames.AlarmsList, PermissionNames.AlarmListing));
            AllTabs.Add(TabNames.Reports, new SimpleTab("Raporty", TabNames.Reports, PermissionNames.Reports));

            AllTabs.Add(TabNames.Configuration, new SimpleTab("Konfiguracja", TabNames.Configuration, PermissionNames.UnitConfiguration));
            AllTabs.Add(TabNames.HubDetails, new SimpleTab("Szczegóły", TabNames.HubDetails, PermissionNames.HubsListing));
            AllTabs.Add(TabNames.HubsList, new SimpleTab("Lista", TabNames.HubsList, PermissionNames.HubsListing));
            AllTabs.Add(TabNames.DiagnosticsData, new SimpleTab("Surowe dane", TabNames.DiagnosticsData, PermissionNames.Diagnostics));
            AllTabs.Add(TabNames.HubsCommunicationLogs, new SimpleTab("Logi", TabNames.HubsCommunicationLogs, PermissionNames.HubsLogs));
            AllTabs.Add(TabNames.PreviewEdit, new SimpleTab("Planowanie", TabNames.PreviewEdit, PermissionNames.UnitsPlanning));

            //Units Localization
            AllTabs.Add(TabNames.Patients, new SimpleTab(LanguageManager.GetControlsText(ControlsTextCodes.TABS_PATIENTS), TabNames.Patients, PermissionNames.UnitPatients));
            AllTabs.Add(TabNames.Monitoring, new SimpleTab(LanguageManager.GetControlsText(ControlsTextCodes.TABS_MONITORABLE), TabNames.Monitoring, PermissionNames.UnitMonitoring));
            AllTabs.Add(TabNames.Localization, new SimpleTab(LanguageManager.GetControlsText(ControlsTextCodes.TABS_LOCALIZATION), TabNames.Localization, PermissionNames.UnitLocalization));
            AllTabs.Add(TabNames.Events, new SimpleTab(LanguageManager.GetControlsText(ControlsTextCodes.TABS_EVENTS), TabNames.Events, PermissionNames.UnitEvents));
            AllTabs.Add(TabNames.LocalizationReports, new SimpleTab(LanguageManager.GetControlsText(ControlsTextCodes.TABS_RAPORTS), TabNames.LocalizationReports, PermissionNames.UnitsListing));
            AllTabs.Add(TabNames.LocalizationConfig, new SimpleTab(LanguageManager.GetControlsText(ControlsTextCodes.TABS_CONFIGURATION), TabNames.LocalizationConfig, PermissionNames.UnitConfiguration));
            AllTabs.Add(TabNames.LocalizationPlan, new SimpleTab(LanguageManager.GetControlsText(ControlsTextCodes.TABS_PLANNING), TabNames.UnitPlan, PermissionNames.UnitPlanning));
            
            ///////////////////////////////////////////////////
            AllTabs.Add(TabNames.Transmitters, new SimpleTab(LanguageManager.GetControlsText(ControlsTextCodes.TABS_TRANSMITTERS), TabNames.Transmitters, PermissionNames.UnitTransmitters));

            AllTabs.Add(TabNames.Bands, new SimpleTab(LanguageManager.GetControlsText(ControlsTextCodes.TABS_BANDS), TabNames.Bands, PermissionNames.UnitBands));
            //Administration
            AllTabs.Add(TabNames.Permissions, new SimpleTab("Użytkownicy", TabNames.Permissions, PermissionNames.AdministrationPersonell));
            AllTabs.Add(TabNames.Messages, new SimpleTab("Komunikaty", TabNames.Messages, PermissionNames.AdministrationMessages));
            AllTabs.Add(TabNames.Settings, new SimpleTab("Konfiguracja systemu", TabNames.Settings, PermissionNames.AdministrationSystemSettings));
            AllTabs.Add(TabNames.LoggedUsers, new SimpleTab("Zalogowani użytkownicy", TabNames.LoggedUsers, PermissionNames.AdministrationLoggedUsers));
            AllTabs.Add(TabNames.Logs, new SimpleTab("Logi", TabNames.Logs, PermissionNames.AdministrationSystemEvents));
            AllTabs.Add(TabNames.ServiceStatus, new SimpleTab("Podsystemy", TabNames.ServiceStatus, PermissionNames.AdministrationServiceStatuses));
            AllTabs.Add(TabNames.Versions, new SimpleTab("Wersje", TabNames.Versions, PermissionNames.AdministrationVersions));
            
            //Tools
            var i = BloodyUser.Current.IsInRole(PermissionNames.ToolsTransmitters);
            AllTabs.Add(TabNames.ToolTransmitters, new SimpleTab("Transmitery", TabNames.ToolTransmitters, PermissionNames.ToolsTransmitters));
            AllTabs.Add(TabNames.ToolBands, new SimpleTab("Opaski", TabNames.ToolBands, PermissionNames.ToolsBands));
            AllTabs.Add(TabNames.ExternalSystems, new SimpleTab("Systemy zewnętrzne", TabNames.ExternalSystems, PermissionNames.SuperUser));
            AllTabs.Add(TabNames.Asseco, new SimpleTab("Konfiguracja", TabNames.Asseco, PermissionNames.SuperUser));
            AllTabs.Add(TabNames.AssecoOut, new SimpleTab("Wysyłanie", TabNames.AssecoOut, PermissionNames.SuperUser));
            AllTabs.Add(TabNames.AssecoIn, new SimpleTab("Odbiór", TabNames.AssecoIn, PermissionNames.SuperUser));
            AllTabs.Add(TabNames.ExternalEvents, new SimpleTab("Zdarzenia zewnętrzne", TabNames.ExternalEvents, PermissionNames.SuperUser));
            AllTabs.Add(TabNames.AssecoMapping, new SimpleTab("Mapowanie", TabNames.AssecoMapping, PermissionNames.SuperUser));
            //AllTabs.Add(TabNames.Backup, new SimpleTab("Kopie bezpieczeństwa", TabNames.Backup, PermissionNames.SuperUser));
            AllTabs.Add(TabNames.ArchivePatients, new SimpleTab("Dane Archiwalne", TabNames.ArchivePatients, PermissionNames.Archive));
        }

        public static List<ITab> GetAllTabsForType(int unitTypeIDv2)
        {
            List<ITab> tabs = new List<ITab>();

            tabs.Add(AllTabs[TabNames.Patients].Clone());
            //RADOM tabs.Add(AllTabs[TabNames.Monitoring].Clone());
            //RADOM tabs.Add(AllTabs[TabNames.Localization].Clone());
            tabs.Add(AllTabs[TabNames.Events].Clone());
            tabs.Add(AllTabs[TabNames.LocalizationReports].Clone());
            tabs.Add(AllTabs[TabNames.LocalizationConfig].Clone());
            //RADOM tabs.Add(AllTabs[TabNames.LocalizationPlan].Clone());
            tabs.Add(AllTabs[TabNames.Transmitters].Clone());
            tabs.Add(AllTabs[TabNames.Bands].Clone());

            return tabs;
        }


        public static List<ITab> GetAllTabsForTreeMenuItem(string name)
        {
            List<ITab> tabs = new List<ITab>();

            if (AllTabs.ContainsKey(name))
                tabs.Add(AllTabs[name].Clone());

            if (name == TabNames.Asseco)
            {
                tabs.Add(AllTabs[TabNames.AssecoIn].Clone());
                tabs.Add(AllTabs[TabNames.AssecoOut].Clone());
                tabs.Add(AllTabs[TabNames.AssecoMapping].Clone());
            }

            return tabs;
        }

        public static List<ITab> GetTabs(params string[] TabNames)
        {
            List<ITab> toReturn = new List<ITab>();
            foreach (var tabName in TabNames)
            {
                if (AllTabs.ContainsKey(tabName))
                {
                    toReturn.Add(AllTabs[tabName].Clone());
                }
                else
                {
                    toReturn.Add(AllTabs[Finder.BloodDonation.Common.Layout.TabNames.NotImplemented].Clone());
                }
            }
            return toReturn;
        }

        public static UnitType GetChildUnitType(UnitType parentType)
        {
            return UnitType.CENTRE;
        }


    }
}
