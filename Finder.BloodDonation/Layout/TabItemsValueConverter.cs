﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Collections.Generic;
using Finder.BloodDonation.Common.Layout;

namespace Finder.BloodDonation.Layout
{
	public class TabItemsValueConverter : IValueConverter
	{

		#region IValueConverter Members

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			IEnumerable<ITab> source = value as IEnumerable<ITab>;
			if (source == null)
				return null;

			List<TabItem> tabs = new List<TabItem>();
			foreach (ITab t in source)
			{
				TabItem item = new TabItem();
				item.Header = t.Header;
				item.Content = t.TabViewModel;
				
				item.SetValue(ITabValueConverter.ITabProperty, t);

				tabs.Add(item);
			}

			return tabs;

		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion
	}

	public class ITabValueConverter : IValueConverter
	{
		public static ITab GetITab(DependencyObject obj)
		{
			return (ITab)obj.GetValue(ITabProperty);
		}

		public static void SetITab(DependencyObject obj, ITab value)
		{
			obj.SetValue(ITabProperty, value);
		}

		public static readonly DependencyProperty ITabProperty =
			DependencyProperty.RegisterAttached(
				"ITab"
				, typeof(ITab)
				, typeof(TabItem)
				, null);


		#region IValueConverter Members

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			ITab t = value as ITab;
			if (t == null)
				return null;

			TabItem item = new TabItem();
			item.Header = t.Header;
			item.Content = t.TabViewModel;

			item.SetValue(ITabValueConverter.ITabProperty, t);

			return item;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			TabItem tabItem = value as TabItem;
			if (tabItem == null)
				return null;

			ITab tab = tabItem.GetValue(ITabProperty) as ITab;

			return tab;
		}

		#endregion
	}
}
