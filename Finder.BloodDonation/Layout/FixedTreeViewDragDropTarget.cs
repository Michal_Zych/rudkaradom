﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Layout
{
	public class FixedTreeViewDragDropTarget : TreeViewDragDropTarget
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="T:FixedTreeViewDragDropTarget"/> class.
		/// </summary>
		public FixedTreeViewDragDropTarget()
		{
			AddHandler(MouseLeftButtonDownEvent, new MouseButtonEventHandler(HandleMouseLeftButtonDown), true);
			AddHandler(MouseLeftButtonUpEvent, new MouseButtonEventHandler(HandleMouseLeftButtonUp), true);
		}

		#endregion

		#region Properties

		/// <summary>
		/// Whether or not the mouse is currently down on the element beneath it.
		/// </summary>
		public static bool IsMouseDown
		{
			get;
			private set;
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// Handles the MouseLeftButtonDown event of the control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="T:MouseButtonEventArgs"/> instance containing the event data.</param>
		private void HandleMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			IsMouseDown = true;
		}

		/// <summary>
		/// Handles the MouseLeftButtonUp event of the control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="T:MouseButtonEventArgs"/> instance containing the event data.</param>
		private void HandleMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			IsMouseDown = false;
		}

	    private bool IsDragable = false;

		/// <summary>
		/// Adds all selected items when drag operation begins.
		/// </summary>
		/// <param name="eventArgs">Information about the event.</param>
		protected override void OnItemDragStarting(ItemDragEventArgs eventArgs)
		{
		    if (IsDragable)
		    {
		        if (IsMouseDown)
		        {
		            base.OnItemDragStarting(eventArgs);
		        } // if
		        else
		        {
		            eventArgs.Cancel = true;
		            eventArgs.Handled = true;
		        } // else
		    }
		}

		#endregion
	}
}
