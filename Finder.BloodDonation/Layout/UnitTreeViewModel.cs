﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Events;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Model;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Authentication;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Tabs.RefreshTools;
using Finder.BloodDonation.Tabs.Contents;
using GalaSoft.MvvmLight.Command;
using System.Text;
using System.Windows.Input;
using Telerik.Windows;
using Finder.BloodDonation.Behaviors;
using Telerik.Windows.Controls;
using ViewModelBase = FinderFX.SL.Core.MVVM.ViewModelBase;
using Finder.BloodDonation.Tabs;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Dialogs.Units;
using Finder.BloodDonation.Dialogs.Helpers;
using Microsoft.Practices.Composite.Presentation.Events;
using Finder.BloodDonation.Tools;
using System.Windows.Media;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.TreeMenu;
using Finder.BloodDonation.UsersService;
using Finder.BloodDonation.Tabs.MenuAdministration.SettingsSystem;
using Finder.BloodDonation.Controls;

namespace Finder.BloodDonation.Layout
{
    [UIException]
    public class UnitTreeViewModel : ViewModelBase, IUnitsManager, IAccordionManager
    {

        private ISettingsRespository _systemSettingsManager;
        public ISettingsRespository SystemSettingsManager
        {
            get
            {
                if(_systemSettingsManager == null)
                {
                    _systemSettingsManager = new SettingsManager();
                    _systemSettingsManager.Load();
                    Container.RegisterInstance<ISettingsRespository>(_systemSettingsManager);
                }
                
                return _systemSettingsManager;
            }
        }

        private ObservableCollection<UnitViewModel> _availableUnits;
        [RaisePropertyChanged]
        public ObservableCollection<UnitViewModel> AvailableUnits
        { 
            get
            {
                return _availableUnits;
            }
            set
            {
                _availableUnits = value;
            } 
        }

        [RaisePropertyChanged]
        public double IconSize { get; set; }

        public ObservableCollection<int> UnitIdWithDisconnectedTransmitter { get; set; }


        [RaisePropertyChanged]
        public DelegateCommand<object> ReloadTreeCommand { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<UnitDTO> AvailableUnitsList { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<IUnit> SelectedUnitChanged { get; set; }

        [RaisePropertyChanged]
        public UnitDTO SelectedUnitSearchBox { get; set; }

         [RaisePropertyChanged]
        public bool HubsSelected { get; set; }

        public UnitViewModel CurrentUnit { get; set; }

        [RaisePropertyChanged]
        public UnitViewModel SelectedUnit { get; set; }

        [RaisePropertyChanged]
        public UnitViewModel SelectedFavorite { get; set; }

        [RaisePropertyChanged]
        public bool IsDropDownOpen { get; set; }

        public UnitViewModel Root { get; set; }

        [RaisePropertyChanged]
        public int SelectedAccordionItemIndex { get; set; }

        [RaisePropertyChanged]
        public object SelectedAccordionItem { get; set; }

        [RaisePropertyChanged]
        public Boolean RootUnitLoaded { get; set; }
        
        [RaisePropertyChanged]
        public SolidColorBrush OldDataBrush { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<TreeMenuItem> AdministrationTreeData { get; set; }

        [RaisePropertyChanged]
        public TreeMenuItem SelectedAdministrationItem { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<TreeMenuItem> ArchiveTreeData { get; set; }

        [RaisePropertyChanged]
        public TreeMenuItem SelectedArchiveItem { get; set; }
        
        [RaisePropertyChanged]
        public ObservableCollection<TreeMenuItem> ToolsTreeData { get; set; }

        [RaisePropertyChanged]
        public TreeMenuItem SelectedToolsItem { get; set; }

        [RaisePropertyChanged]
        public Image BaseUnitImage { get; set; }

        [RaisePropertyChanged]
        public bool IsUnitMonitoredVisiblity { get; set; }

        [RaisePropertyChanged]
        public bool IsUnitProhibitedVisiblity { get; set; }

        [RaisePropertyChanged]
        public Visibility IsTouchedScreenVisibility { get; set; }

        public RelayCommand<KeyEventArgs> cbSelectionChanged { get; set; }

        public RelayCommand searchBoxLostFocus { get; set; }

        public RelayCommand CreateUnitCommand { get; set; }
        public RelayCommand EditUnitCommand { get; set; }
        public RelayCommand AssignTransmitterCommand { get; set; }
        public RelayCommand RemoveUnitCommand { get; set; }

        public RelayCommand<RadRoutedEventArgs> AccordionItemSelected { get; set; }

        public RelayCommand<RadTreeViewDragEventArgs> PreviewDragStarted { get; set; }
        public RelayCommand<RadTreeViewDragEndedEventArgs> PreviewDragEnded { get; set; }

        private int patientInUnit = 0;

        private UnitsServiceClient DataService
        {
            get
            {
                var proxy = ServiceFactory.GetService<UnitsServiceClient>("Finder-BloodDonation-Model-Services-UnitsService.svc");
                proxy.GetAssignedPatientsCountCompleted += proxy_GetAssignedPatientsCountCompleted;
                return proxy;
            }
        }

        public void proxy_GetAssignedPatientsCountCompleted(object sender, GetAssignedPatientsCountCompletedEventArgs e)
        {
            if(e.Result != null)
            {
                patientInUnit = e.Result;
            }
        }


        private IDictionary<string, string> _filters = null;

        IRefreshManager timer = null;

        private readonly IWindowsManager _windowsManager;

        public UnitTreeViewModel(IUnityContainer container)
            : base(container)
        {
            RegisterPropertyChanged();
            LanguageHelpher.LanguageManager.LoadLanguage(LanguageHelpher.Language.PL);
            IsTouchedScreenVisibility = LocalSettings.IsTouchedScreen ? Visibility.Visible : Visibility.Collapsed;

            RootUnitLoaded = false;

            SelectedUnitChanged = new DelegateCommand<IUnit>(OnSelectedUnitChanged);
            ReloadTreeCommand = new DelegateCommand<object>(OnReloadTree);
            
            //TODO: Utworzyc komendy.

            EventAggregator.GetEvent<UnitOpenEvent>().Subscribe(UnitOpenEvent_Event);
            EventAggregator.GetEvent<UnitSelectExpandEvent>().Subscribe(UnitSelectExpandEvent_Handler);
            EventAggregator.GetEvent<AlarmsUnitAlarmSet>().Subscribe(AlarmsUnitAlarmSet_Event);
            EventAggregator.GetEvent<AlarmsUnitAlarmDispose>().Subscribe(AlarmsUnitAlarmDispose_Event);
            EventAggregator.GetEvent<AlarmsUnitAlarmDisableSet>().Subscribe(AlarmsUnitAlarmDisableSet_Event);
            EventAggregator.GetEvent<AlarmsUnitAlarmDisableDispose>().Subscribe(AlarmsUnitAlarmDisableDispose_Event);
            EventAggregator.GetEvent<AlarmsUnitOldDataSet>().Subscribe(AlarmsUnitOldDataSet_Event);
            EventAggregator.GetEvent<AlarmsUnitOldDataDispose>().Subscribe(AlarmsUnitOldDataDispose_Event);
            EventAggregator.GetEvent<AlarmsTransmitterDisconnectedSet>().Subscribe(AlarmsTransmitterDisconnectedSet_Event);
            EventAggregator.GetEvent<AlarmsTransmitterDisconnectedDispose>().Subscribe(AlarmsTransmitterDisconnectedDispose_Event);

            EventAggregator.GetEvent<SetUnitAsDefaultEvent>().Subscribe(OnSetUnitAsDefaultEvent);
            EventAggregator.GetEvent<SetUserDefaultUnitEvent>().Subscribe(OnSetUserDefaultUnitEvent);


            EventAggregator.GetEvent<SelectStructureUnitEvent>().Subscribe(SelectStructureUnitEvent_Event);

            EventAggregator.GetEvent<ReloadRootEvent>().Subscribe(ReloadRoot);

            // edycja unitów
            EventAggregator.GetEvent<TestOkienkaEvent>().Subscribe(OnTestOkienkaEvent);

            EventAggregator.GetEvent<AddUnitEvent>().Subscribe(OnAddUnitEvent);
            EventAggregator.GetEvent<AddSensorEvent>().Subscribe(OnAddSensorEvent);

            EventAggregator.GetEvent<EditUnitEvent>().Subscribe(OnEditUnitEvent);

            EventAggregator.GetEvent<DeleteUnitEvent>().Subscribe(OnDeleteUnitEvent);
            //
            EventAggregator.GetEvent<UnitIdWithDisconnectedTransmitterReceived>().Subscribe(OnTransmitterStatusChanged);

            EventAggregator.GetEvent<UnitIdWithDisconnectedTransmitterReceived>().Subscribe(OnUnitIdWithDisconnectedTransmitterReceive);

            EventAggregator.GetEvent<ChangeSelectedUnitEvent>().Subscribe(OnSelectUnit);

            CreateUnitCommand = new RelayCommand(OnCreateCommand);
            EditUnitCommand = new RelayCommand(OnEditUnit);
            RemoveUnitCommand = new RelayCommand(OnRemoveUnit);
            AssignTransmitterCommand = new RelayCommand(OnAssignTransimtter);
            
            this.Container.RegisterInstance<UnitTreeViewModel>(this);
            this.Container.RegisterInstance<IUnitsManager>(this);
            this.Container.RegisterInstance<IAccordionManager>(this);

            _windowsManager = this.Container.Resolve<IWindowsManager>();

            this.cbSelectionChanged = new RelayCommand<KeyEventArgs>(cbSelectionChangedEvent);
            this.searchBoxLostFocus = new RelayCommand(searchBoxLostFocusEvent);
            this.AccordionItemSelected = new RelayCommand<RadRoutedEventArgs>(OnAccordionItemSelected);

            PreviewDragStarted = new RelayCommand<RadTreeViewDragEventArgs>(OnPreviewDragStarted);
            PreviewDragEnded = new RelayCommand<RadTreeViewDragEndedEventArgs>(OnPreviewDragEnded);

            timer = new RefreshManager();

            var viewFactory = container.Resolve<ViewFactory>();
            _windowsManager.RegisterWindow(WindowsKeys.DeleteUnit, "Kasowanie", viewFactory.CreateView<DeleteUnit>(), new Size(400, 250), true);
            _windowsManager.RegisterWindow(WindowsKeys.EditUnit, "Edycja", viewFactory.CreateView<EditUnit>(), new Size(600, 400), true, false);
            _windowsManager.RegisterWindow(WindowsKeys.ConfirmMoveUnit, "Potwierdź", viewFactory.CreateView<ConfirmMoveUnit>(), new Size(400, 330), true);

            OldDataBrush = new SolidColorBrush(GetOldDataColor());

            ToolsTreeData = MenuTree.GetToolsMenu();
            AdministrationTreeData = MenuTree.GetAdministrationMenu();
            ArchiveTreeData = MenuTree.GetArchiveMenu();

            var test = SystemSettingsManager;
            FinderImage.LoadBasicImages();
        }

        public void OnTransmitterStatusChanged(IList<int> obj)
        {
            try
            {
                Queue<UnitViewModel> q = new Queue<UnitViewModel>();
                List<UnitViewModel> vis = new List<UnitViewModel>();

                while (q.Count != 0)
                {
                    UnitViewModel v = q.Dequeue();

                    foreach (var neighbor in v.Children)
                    {
                        if (!vis.Contains(neighbor))
                        {
                            vis.Add(v);

                            neighbor.IsAlarm = false;

                            q.Enqueue(neighbor);
                        }
                    }
                }

                for (int i = 0; i < obj.Count; i++)
                {
                    bool isEnd = false;
                    UnitViewModel uvm = AvailableUnits.FirstOrDefault();
                    List<UnitViewModel> visited = new List<UnitViewModel>();
                    Queue<UnitViewModel> queue = new Queue<UnitViewModel>();
                    queue.Enqueue(uvm);

                    while (queue.Count != 0 && !isEnd)
                    {
                        UnitViewModel v = queue.Dequeue();

                        foreach (var neighbor in v.Children)
                        {
                            if (!visited.Contains(neighbor))
                            {
                                visited.Add(v);

                                if (neighbor.Unit.ID == obj[i])
                                {
                                    if (!neighbor.IsAlarm)
                                    {
                                        neighbor.IsAlarm = true;
                                        isEnd = true;
                                    }
                                }

                                queue.Enqueue(neighbor);
                            }
                        }
                    }

                }
            }
            catch (Exception e)
            {

            }
        }

        public void OnSelectUnit(int unitId)
        {
            SelectUnit(unitId);
        }

        public void OnUnitIdWithDisconnectedTransmitterReceive(IList<int> obj)
        {
            UnitIdWithDisconnectedTransmitter = new ObservableCollection<int>(obj);
        }

        private void OnAssignTransimtter()
        {
            MsgBox.Warning("Dodawanie transmitera");
        }

        private void OnRemoveUnit()
        {
            string message = String.Empty;

            if (SelectedUnit != null)
            {
                if (SelectedUnit.Unit.ID == UnitTypeOrganizer.GetUnassignedPatientsUnitId())
                {
                    message = "Nie można usunąć jednostki organizacyjnej będącej \n pacjentami nieprzypisanymi!";
                }

                if (SelectedUnit.Unit.ID == UnitTypeOrganizer.GetRegistartionUnitId())
                {
                    message = "Nie można usunąć jednostki organizacyjnej będącej rejestracją!";
                }

                if (SelectedUnit.Unit.Children != null && SelectedUnit.Unit.Children.Count > 0)
                {
                    message = "Nie można usunąć niepustej jednostki organizacyjnej.";
                }

                if(patientInUnit > 0)
                {
                    message = "Nie można usunąć jednostki organizacyjnej, do której przypisani są pacjenci!";
                }

                if (message != String.Empty)
                {
                    MsgBox.Error(message);
                    return;
                }

                _unitEditData = new UnitEditData
                {
                    Unit = SelectedUnit.Unit
                };

                EventAggregator.GetEvent<CloseWindowEvent>()
                    .Subscribe(CloseWindowEventHandling, ThreadOption.UIThread, true);

                this._windowsManager.ShowWindow(WindowsKeys.DeleteUnit, _unitEditData);
            }
        }

        private void OnEditUnit()
        {
            if (SelectedUnit != null && SelectedUnit.Unit != null)
                OnEditUnitEvent(SelectedUnit.Unit);
        }

        private void OnCreateCommand()
        {
            var nu = new UnitDTO();
            nu.UnitTypeIDv2 = UnitTypeOrganizer.DEFAULT_UNIT_TYPE;

            if (SelectedUnit != null)
                CreateUnit(nu, SelectedUnit.Unit);
        }

        private Color GetOldDataColor()
        {
            return Settings.Settings.Current.GetColor(ConfigurationKeys.OldDataColor);
        }

        private UnitEditData _unitEditData;
        
        public void OnAddUnitEvent(UnitDTO unit)
        {
            var nu = new UnitDTO();
            nu.UnitTypeIDv2 = UnitTypeOrganizer.DEFAULT_UNIT_TYPE;

            CreateUnit(nu, unit);
        }
        public void OnAddSensorEvent(UnitDTO unit)
        {
            var nu = new UnitDTO();

            UnitEditData _u = new UnitEditData()
            {
                Unit = unit,
                ParentUnit = unit.Parent
            };

            //_u.Unit.UnitTypeIDv2 = (int)UnitType.ZONE;
            _u.Unit.IsMonitored = true;

            EditUnit(_u);
        }

        private void CreateUnit(UnitDTO unit, UnitDTO parent)
        {
            unit.Parent = parent;

            _unitEditData = new UnitEditData
            {
                Unit = unit,
                ParentUnit = parent
            };
            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling, ThreadOption.UIThread, true);
            this._windowsManager.ShowWindow(WindowsKeys.EditUnit, _unitEditData);
        }

        public void OnDeleteUnitEvent(UnitDTO unit)
        {
            string message = String.Empty;

            if(unit.ID == UnitTypeOrganizer.GetUnassignedPatientsUnitId())
            {
                message = "Nie można usunąć jednostki organizacyjnej będącej \n pacjentami nieprzypisanymi!";
            }
            if(unit.ID == UnitTypeOrganizer.GetRegistartionUnitId())
            {
                message = "Nie można usunąć jednostki organizacyjnej będącej rejestracją!";
            }
            if (unit.Children != null && unit.Children.Count > 0)
            {
                message = "Nie można usunąć niepustej jednostki organizacyjnej.";
            }
            if (patientInUnit > 0)
            {
                message = "Nie można usunąć jednostki organizacyjnej, do której \n przypisani są pacjenci!";
            }

            if(message != String.Empty)
            {
                MsgBox.Error(message);
                return;
            }

            _unitEditData = new UnitEditData
            {
                Unit = unit
            };
            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling, ThreadOption.UIThread, true);
            this._windowsManager.ShowWindow(WindowsKeys.DeleteUnit, _unitEditData);
        }

        private void DeleteUnit(UnitEditData data)
        {
            UnitsServiceClient proxy = ServiceFactory.GetService<UnitsServiceClient>();
            proxy.DeleteUnitCompleted += new EventHandler<DeleteUnitCompletedEventArgs>(proxy_DeleteUnitCompleted);
            proxy.DeleteUnitAsync(data.Unit.ID, data.Reason, data.Unit);
        }

        private void proxy_DeleteUnitCompleted(object sender, DeleteUnitCompletedEventArgs e)
        {
            if (e.Result < 0)
            {
                MsgBox.Error("Kasowanie nie powiodło się.");
            }
            else
            {
                BloodyUser.Current.ReloadPermissions();


                UnitDTO deletedUnit = e.UserState as UnitDTO;
                EventAggregator.GetEvent<RemoveFromFavoriteEvent>().Publish(new List<UnitDTO> { deletedUnit});
                IUnit edited = SearchUnit(deletedUnit.ID);
                var editedView = edited as UnitViewModel;

                IUnit parent = SearchUnit(deletedUnit.ParentUnitID.Value);
                    var parentView = parent as UnitViewModel;
                
                parentView.vm_UnitRemoved(editedView, EventArgs.Empty);

                SelectedUnit = parentView;
            }
        }
        
        public void OnEditUnitEvent(UnitDTO unit)
        {
            _unitEditData = new UnitEditData
            {
                Unit = unit.Clone()
            };

            if (unit.Parent != null)
            {
                _unitEditData.Unit.Parent = unit.Parent;
                _unitEditData.Unit.ParentUnitID = unit.ParentUnitID;
                _unitEditData.Unit.Description = unit.Description;
                _unitEditData.Unit.UnitTypeIDv2 = unit.UnitTypeIDv2;
                _unitEditData.Unit.UnitType = unit.UnitType;
                _unitEditData.Unit.PictureID = unit.PictureID;

                EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling, ThreadOption.UIThread, true);
                this._windowsManager.ShowWindow(WindowsKeys.EditUnit, _unitEditData);
            }
            else
            {
                MsgBox.Warning("Nie można edytować głównego akordionu!");
            }
        }

        public void OnTestOkienkaEvent(UnitDTO unit)
        {
            _unitEditData = new UnitEditData
            {
                Unit = unit,
                ParentUnit = unit.Parent
            };

            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling, ThreadOption.UIThread,
                                           true);
            this._windowsManager.ShowWindow(WindowsKeys.EditUnit, _unitEditData);
        }

        private void CloseWindowEventHandling(string key)
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(CloseWindowEventHandling);

            if (key == WindowsKeys.DeleteUnit)
            {
                if (_unitEditData.Canceled)
                    ;                                         
                else
                    DeleteUnit(_unitEditData);
            }

            if (key == WindowsKeys.EditUnit)
            {
                if (_unitEditData.Canceled)
                   ;                                       
                else
                {
                    if (_unitEditData.Unit.ID == 0)
                        CreateUnit(_unitEditData);
                    else EditUnit(_unitEditData);
                }
            }

            if (key == WindowsKeys.ConfirmMoveUnit)
            {
                if (_unitEditData.Canceled)
                    ReloadRoot(true);
                else
                    MoveUnit(_unitEditData);
            }
        }

        private void MoveUnit(UnitEditData data)
        {
            var proxy = ServiceFactory.GetService<UnitsServiceClient>("Finder-BloodDonation-Model-Services-UnitsService.svc");
            proxy.MoveUnitCompleted += ProxyOnMoveUnitCompleted;
            proxy.MoveUnitAsync(data.Unit.ID, data.ParentUnit.ID, data.Reason, data.LogInfo, data.Unit);
        }

        private void ProxyOnMoveUnitCompleted(object sender, MoveUnitCompletedEventArgs e)
        {
            if (e.Result < 0)
            {
                MsgBox.Error("Przeniesienie jednostki organizacyjnej nie powiodło się.");
                System.Diagnostics.Debug.WriteLine("unit move completed");
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("unit move completed");
            }
            ReloadRoot(true);
            
        }

        public IUnit SearchOldParent(int unit)
        {
            for (int i = 0; i < AvailableUnits.Count; i++)
            {
                IUnit u = AvailableUnits[i].SearchUnit(unit);
                if (u != null)
                {
                    return u;
                }
            }

            return null;
        }

        private void EditUnit(UnitEditData data)
        {
            UnitsServiceClient proxy = ServiceFactory.GetService<UnitsServiceClient>();
            proxy.UpdateUnitCompleted += new EventHandler<UpdateUnitCompletedEventArgs>(proxy_UpdateUnitCompleted);
            proxy.UpdateUnitAsync(data.Unit, data.Reason, data.LogInfo, data.Unit);
        }

        private void proxy_UpdateUnitCompleted(object sender, UpdateUnitCompletedEventArgs e)
        {
            if (e.Result < 0)
            {
                MsgBox.Error("Edycja się nie powiodła.");
            }
            else
            {
                UnitDTO editedUnit = e.UserState as UnitDTO;

                IUnit edited = SearchUnit(editedUnit.ID);
                var editedView = edited as UnitViewModel;
                
                editedView.UnitDescription = editedUnit.Description;
                editedView.Unit.Name = editedUnit.Name;
                editedView.Unit.Description = editedUnit.Description;
                editedView.Unit.UnitTypeIDv2 = editedUnit.UnitTypeIDv2;
                editedView.Unit.PictureID = editedUnit.PictureID;
                editedView.Unit.IsMonitored = editedUnit.IsMonitored;

                BloodyUser.Current.ReloadPermissions();

                SelectedUnit = editedView;
            }
        }

        private void CreateUnit(UnitEditData data)
        {
            data.Unit.ParentUnitID = data.Unit.Parent.ID;
            data.Unit.ClientID = data.Unit.Parent.ClientID;
            data.Unit.Children = new List<UnitDTO>();

            UnitsServiceClient proxy = ServiceFactory.GetService<UnitsServiceClient>();
            proxy.CreateUnitCompleted += new EventHandler<CreateUnitCompletedEventArgs>(proxy_CreateUnitCompleted);
            proxy.CreateUnitAsync(data.Unit, data.Reason, data.LogInfo, data.Unit);
        }

        private void proxy_CreateUnitCompleted(object sender, CreateUnitCompletedEventArgs e)
        {
            int unitId;
            
            if (!int.TryParse(e.Result, out unitId))
            {
                MsgBox.Error(e.Result);
            }
            else
            {
                UnitDTO addedUnit = e.UserState as UnitDTO;
                addedUnit.ID = unitId;

                IUnit addedTo = SearchUnit(addedUnit.ParentUnitID.Value);
                var addedToView = addedTo as UnitViewModel;
                
                if (UnitTypeOrganizer.IsSensorUnit(addedUnit.UnitTypeIDv2))
                {
                    addedUnit.IsMonitored = true;
                    addedUnit.Parent.IsMonitored = true;
                    addedToView.IsMonitored = true;
                }

                if (addedToView.Unit.Children == null)
                    addedToView.Unit.Children = new List<UnitDTO>();

                addedToView.Unit.Children.Add(addedUnit);
                //TODO: odswiezyc uprawnienia uzytkownika
                //pobrac na nowo? 
                //czy recznie przebudowac?

                UnitViewModel vm = Container.Resolve<UnitViewModel>();
                vm.SetUnit(addedUnit, Root);
                
                addedToView.Children.Add(vm);

                BloodyUser.Current.ReloadPermissions();

                //do dodanego unita nie ma jeszcze uprawnień(nie pokarzą się zakładki) wracam więc fokusem do poprzednio wybranego unita
                var old = SelectedUnit;
                SelectedUnit = vm;
                SelectedUnit = old;
            }
        }

        public void searchBoxLostFocusEvent()
        {
            this.SelectedUnitSearchBox = null;
        }

        public void cbSelectionChangedEvent(KeyEventArgs eventArgs)
        {
            this.IsDropDownOpen = true;
            if (eventArgs.Key == Key.Enter)
            {
                if (SelectedUnitSearchBox != null)
                {
                    Debug.WriteLine("Search combobox, unit:" + SelectedUnitSearchBox);

                    EventAggregator.GetEvent<ExpandAndSelect>().Publish(new UnitSelectExpandEventInfo()
                        {
                            UnitId = this.SelectedUnitSearchBox.ID,
                            SelectedInfo = new SelectedUnitInfo()
                            {
                                Tree = AccordionItemsNames.UNITS
                            }
                        });
                }
                else
                {
                    Debug.WriteLine("Search comobox, NULL");
                }
                IsDropDownOpen = false;
            }
        }

        public void OnAccordionItemSelected(RadRoutedEventArgs e)
        {
            string s = (e.Source as FrameworkElement).Name;
            switch ((e.Source as FrameworkElement).Name)
            {
                case AccordionItemsNames.UNITS:
                    SelectedArchiveItem = null;
                    OnSelectedUnitChanged(CurrentUnit);
                    ((BloodyUser)User.Current).SetCustomProperty(ConfigurationsKeys.SELECTED_OPTION, AccordionItemsNames.UNITS);
                    break;

                case AccordionItemsNames.ADMINISTRATION:
                    SelectedArchiveItem = null;
                    ((BloodyUser)User.Current).SetCustomProperty(ConfigurationsKeys.SELECTED_OPTION, AccordionItemsNames.ADMINISTRATION);
                    break;
                case AccordionItemsNames.ARCHIVE:                  
                     ((BloodyUser)User.Current).SetCustomProperty(ConfigurationsKeys.SELECTED_OPTION, AccordionItemsNames.ARCHIVE);
                    break;
                case AccordionItemsNames.TOOLS:
                    SelectedArchiveItem = null;
                    ((BloodyUser)User.Current).SetCustomProperty(ConfigurationsKeys.SELECTED_OPTION, AccordionItemsNames.TOOLS);
                    break;
            }
        }

        protected override void ViewLoaded()
        {
            UnitManager.Instance.ReloadRoot(RestoreLayoutAfterRootReloaded);
            //ReloadRoot(true);
        }

        private void RestoreLayoutAfterRootReloaded()
        {
            RootReloaded();
            StartRestoreLayout();
        }

        private void RootReloaded()
        {
            AvailableUnits = new ObservableCollection<UnitViewModel>();

            this.Root = ConvertUnit(UnitManager.Instance.RootUnit);
            this.Container.RegisterInstance<UnitViewModel>("Root", Root);

            RootUnitLoaded = true;

            AvailableUnits.Add(Root);

            this.AvailableUnitsList = new ObservableCollection<UnitDTO>(UnitManager.Instance.Units);
        }

        private void proxy_GetRootUnitCompleted(object sender, GetRootUnitCompletedEventArgs e)
        {
            AvailableUnits = new ObservableCollection<UnitViewModel>();

            var res = e.Result;
            res.SetParrentsForChildren();
            this.Root = ConvertUnit(e.Result);

            this.Container.RegisterInstance<UnitViewModel>("Root", Root);

            RootUnitLoaded = true;

            AvailableUnits.Add(Root);

            this.AvailableUnitsList = new ObservableCollection<UnitDTO>();
            var unit = AvailableUnits[0].Unit;
            unit.ForEachDescendant(desc => AvailableUnitsList.Add(desc));
            if ((bool)e.UserState)
                StartRestoreLayout();
        }

        public void ReloadRoot(bool RestoreLayout)
        {
            //MsgBox.Warning("Stare ReloadRoot");
            UnitsServiceClient proxy = ServiceFactory.GetService<UnitsServiceClient>("Finder-BloodDonation-Model-Services-UnitsService.svc");
            proxy.GetRootUnitCompleted += new EventHandler<GetRootUnitCompletedEventArgs>(proxy_GetRootUnitCompleted);
            proxy.GetRootUnitAsync(RestoreLayout);
        }

        public void SelectStructureUnitEvent_Event(int o)
        {
            System.Diagnostics.Debug.WriteLine("tree selecting unit: " + o);

            var pathLength = SelectUnit(o);

            if (pathLength == 0)
                MsgBox.Warning("Nie masz uprawnień do tej jednostki");
        }

        public void UnitSelectExpandEvent_Handler(UnitSelectExpandEventInfo param)
        {
            SelectUnit(param.UnitId, param.SelectedInfo);
        }

        protected override void ViewUnloaded()
        {
            AvailableUnits = null;
        }

        private void RestoreLayout(object sender, EventArgs e)
        {
            BloodyUser bu = User.Current as BloodyUser;
            if (bu != null)
            {
                var option = bu.GetCustomProperty(ConfigurationsKeys.SELECTED_OPTION);

                // set default accordion
                if (String.IsNullOrEmpty(option))
                {
                    ((BloodyUser)User.Current).SetCustomProperty(ConfigurationsKeys.SELECTED_OPTION, AccordionItemsNames.UNITS);
                    option = AccordionItemsNames.UNITS;
                }

                EventAggregator.GetEvent<ExpandAccordionEvent>().Publish(option);

                var selectedUnitId = Convert.ToInt32(bu.GetCustomProperty(ConfigurationsKeys.SELECTED_UNIT));
                var lastTabName = bu.GetCustomProperty(ConfigurationsKeys.SELECTED_TAB);
                var lastSubview = bu.GetCustomProperty(ConfigurationsKeys.SELECTED_SUBVIEW);

                System.Diagnostics.Debug.WriteLine("expanding tree for option:{0} unit:{1} tab:{2} sub:{3}", option, selectedUnitId, lastTabName, lastSubview);

                if (Convert.ToInt32(selectedUnitId) > 0)
                {
                    switch (option)
                    {
                        case AccordionItemsNames.UNITS:
                            //SelectUnit(selectedUnitId);

                            SelectUnit(selectedUnitId, new SelectedUnitInfo()
                            {
                                Tree = AccordionItemsNames.UNITS,
                                TabName = lastTabName,
                                SubviewName = lastSubview
                            });

                            break;
                        default:

                            break;
                    }
                }

                if(option == AccordionItemsNames.ADMINISTRATION)
                    EventAggregator.GetEvent<ExpandAdminSubviewEvent>().Publish(lastTabName);

                if (option == AccordionItemsNames.ARCHIVE)
                    EventAggregator.GetEvent<ExpandArchiveSubviewEvent>().Publish(lastTabName);

                if (option == AccordionItemsNames.TOOLS)
                    EventAggregator.GetEvent<ExpandToolSubviewEvent>().Publish(lastTabName);



                ITabManager TabManager = Container.Resolve<ITabManager>();
                TabManager.SelectTab(lastTabName, null);

                //EventAggregator.GetEvent<ContentSubviewRestoredEvent>().Publish(lastSubview);
            }
        }

        private void RegisterPropertyChanged()
        {
            this.PropertyChanged += UnitTreeViewModel_PropertyChanged;
        }

        private void UnRegisterPropertyChanged()
        {
            this.PropertyChanged -= UnitTreeViewModel_PropertyChanged;
        }

        void UnitTreeViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedAccordionItemIndex") OnSelectedAccordionItemChanged();

            if (e.PropertyName == "SelectedAdministrationItem") OnAdministrationItemChanged();

            if (e.PropertyName == "SelectedArchiveItem") OnArchiveItemChanged();
            
            if (e.PropertyName == "SelectedToolsItem") OnToolsItemChanged();

            if (e.PropertyName == "SelectedUnit")
            {
                //Debug.WriteLine("selected unit: " + ((SelectedUnit != null) ? SelectedUnit.Identity.ToString() : String.Empty));

                OnSelectedUnitChanged(SelectedUnit);
            }
        }

        private void OnToolsItemChanged()
        {
            if(SelectedToolsItem != null)
            {
                SelectedArchiveItem = null;
                SelectedAdministrationItem = null;
                this.EventAggregator.GetEvent<UnitChangedEvent>().Publish(SelectedToolsItem);
            }
        }

        private void OnArchiveItemChanged()
        {
            if (SelectedArchiveItem != null)
            {
                SelectedToolsItem = null;
                SelectedAdministrationItem = null;
                this.EventAggregator.GetEvent<UnitChangedEvent>().Publish(SelectedArchiveItem);
            }
        }

        private void OnAdministrationItemChanged()
        {
            if (SelectedAdministrationItem != null)
            {
                SelectedToolsItem = null;
                SelectedArchiveItem = null;
                this.EventAggregator.GetEvent<UnitChangedEvent>().Publish(SelectedAdministrationItem);
            }
        }

        private void OnSelectedAccordionItemChanged()
        {

            switch ((AccordionMode)SelectedAccordionItemIndex)
            {
                    /*
                case AccordionMode.Favorites:
                    ((BloodyUser)User.Current).SetCustomProperty(ConfigurationsKeys.SELECTED_OPTION, AccordionItemsNames.FAVORITES);
                    break;
                     */
                case AccordionMode.List:
                default:
                    ((BloodyUser)User.Current).SetCustomProperty(ConfigurationsKeys.SELECTED_OPTION, AccordionItemsNames.UNITS);
                    break;
            }

            //Bootstrapper.FavoritesSelected = false;
            /*
            if (SelectedAccordionItemIndex == (int)AccordionMode.Favorites)
            {
                //  Bootstrapper.FavoritesSelected = true;
                if (Favorites == null)
                {
                    Favorites = this.Container.Resolve<FavoriteViewModel>();
                }
            }
             */
        }


        public int SelectUnit(int unitID)
        {
            if (AvailableUnits != null)
            {
                List<UnitViewModel> OpenPath = new List<UnitViewModel>();
                UnitViewModel r = null;
                for (int i = 0; i < AvailableUnits.Count; i++)
                {
                    r = AvailableUnits[i].FillUnitPath(OpenPath, unitID);
                    if (r != null)
                    {
                        break;
                    }
                }

                if (OpenPath.Count > 0)
                {
                    OpenPath.Reverse();
                    EventAggregator.GetEvent<ExpandUnitEvent>().Publish(
                        new SelectedUnitInfo()
                        {
                            Path = OpenPath.ToArray(),
                            Tree = AccordionItemsNames.UNITS
                        }
                        );
                    UnitOpenEvent_Event(unitID);
                }

                return OpenPath.Count;
            }

            return 0;
        }

        public void SelectUnit(int unitID, SelectedUnitInfo info)
        {
            if (AvailableUnits != null)
            {
                List<UnitViewModel> OpenPath = new List<UnitViewModel>();
                UnitViewModel r = null;
                for (int i = 0; i < AvailableUnits.Count; i++)
                {
                    r = AvailableUnits[i].FillUnitPath(OpenPath, unitID);
                    if (r != null)
                    {
                        break;
                    }
                }

                if (OpenPath.Count > 0)
                {
                    OpenPath.Reverse();

                    info.Path = OpenPath.ToArray();
                    EventAggregator.GetEvent<ExpandUnitEvent>().Publish(info);

                    UnitOpenEvent_Event(unitID);
                }
            }
        }

        public void UnitOpenEvent_Event(int unit)
        {
            if (AvailableUnits != null)
            {
                List<UnitViewModel> OpenPath = new List<UnitViewModel>();
                UnitViewModel r = null;
                for (int i = 0; i < AvailableUnits.Count; i++)
                {
                    r = AvailableUnits[i].FillUnitPath(OpenPath, unit);
                    if (r != null)
                    {
                        break;
                    }
                }

                if (OpenPath.Count > 0)
                {
                    //OpenPath.Insert(0, r);
                    OpenPath.Reverse();
                    EventAggregator.GetEvent<SelectUnitEvent>().Publish(
                        new SelectedUnitInfo()
                        {
                            Path = OpenPath.ToArray(),
                            Tree = AccordionItemsNames.UNITS
                        }
                    );
                    IUnit u = SearchUnit(unit);
                    if (u != null)
                    {
                        u.SelectionSource = SelectionSources.MainTree;
                        this.EventAggregator.GetEvent<UnitChangedEvent>().Publish(u);
                        this.EventAggregator.GetEvent<SelectExtendedUnitEvent>().Publish(
                            new SelectedUnit()
                            {
                                Unit = u,
                                UnitName = ((Finder.BloodDonation.Layout.UnitViewModel)(u)).Unit.Name,
                            }
                        );
                    }
                }
            }
        }

        public IUnit SearchUnit(int unit)
        {
            for (int i = 0; i < AvailableUnits.Count; i++)
            {
                IUnit u = AvailableUnits[i].SearchUnit(unit);
                if (u != null)
                {
                    return u;
                }
            }

            return null;
        }

        public void SelectDepartments()
        {
            if (AvailableUnits != null)
            {
                List<UnitViewModel> OpenPath = new List<UnitViewModel>();
                UnitViewModel r = null;
                for (int i = 0; i < AvailableUnits.Count; i++)
                {
                    r = AvailableUnits[i].FillUnitPathByType(OpenPath, UnitType.DEPARTMENT);
                    if (r != null)
                    {
                        break;
                    }
                }

                if (OpenPath.Count > 0)
                {
                    OpenPath.Reverse();
                    EventAggregator.GetEvent<ExpandUnitEvent>().Publish(
                        new SelectedUnitInfo()
                        {
                            Path = OpenPath.ToArray(),
                            Tree = AccordionItemsNames.UNITS
                        }
                    );
                }
            }
        }

        public IUnit GetUnitDepartment(int unitID)
        {
            if (AvailableUnits != null)
            {
                for (int i = 0; i < AvailableUnits.Count; i++)
                {
                    IUnit u = AvailableUnits[i].UnitDepartment(unitID);
                    if (u != null)
                    {
                        return u;
                    }
                }
            }

            return null;
        }

        public void AlarmsUnitAlarmSet_Event(int unit)
        {
            if (AvailableUnits != null)
            {
                for (int i = 0; i < AvailableUnits.Count; i++)
                {
                    AvailableUnits[i].SetAlarm(unit);
                }
            }
        }

        public void AlarmsUnitAlarmDispose_Event(int unit)
        {
            if (AvailableUnits != null)
            {
                for (int i = 0; i < AvailableUnits.Count; i++)
                {
                    AvailableUnits[i].UnsetAlarm(unit);
                }
            }
        }

        public void AlarmsUnitAlarmDisableSet_Event(int unit)
        {
            if (AvailableUnits != null)
            {
                for (int i = 0; i < AvailableUnits.Count; i++)
                {
                    AvailableUnits[i].SetDisableAlarm(unit);
                }
            }
        }

        public void AlarmsUnitAlarmDisableDispose_Event(int unit)
        {
            if (AvailableUnits != null)
            {
                for (int i = 0; i < AvailableUnits.Count; i++)
                {
                    AvailableUnits[i].UnsetDisableAlarm(unit);
                }
            }
        }

        public void AlarmsTransmitterDisconnectedDispose_Event(int unit)
        {
            if(AvailableUnits != null)
            {
                for(int i = 0; i < AvailableUnits.Count; i++)
                {
                    AvailableUnits[i].UnsetTransmitterDisconnected(unit);
                }
            }
        }

        public void AlarmsTransmitterDisconnectedSet_Event(int unit)
        {
             if(AvailableUnits != null)
            {
                for(int i = 0; i < AvailableUnits.Count; i++)
                {
                    AvailableUnits[i].SetTransmitterDisconnected(unit);
                }
            }
        }

        public void AlarmsUnitOldDataSet_Event(int unit)
        {
            if (AvailableUnits != null)
            {
                for (int i = 0; i < AvailableUnits.Count; i++)
                {
                    AvailableUnits[i].SetOldData(unit);
                }
            }
        }

        public void AlarmsUnitOldDataDispose_Event(int unit)
        {
            if (AvailableUnits != null)
            {
                for (int i = 0; i < AvailableUnits.Count; i++)
                {
                    AvailableUnits[i].UnsetOldData(unit);
                }
            }
        }

        private void StartRestoreLayout()
        {
            Storyboard ping_expand = new Storyboard()
            {
                Duration = new Duration(new TimeSpan(0, 0, 1))
            };
            ping_expand.Completed += new EventHandler(RestoreLayout);
            ping_expand.Begin();
        }

        public void OnReloadTree(object o)
        {

            if (null != CurrentUnit)
                User.Current.SetProperty(ConfigurationsKeys.SELECTED_UNIT, CurrentUnit.Unit.ID.ToString());
            ReloadRoot(true);

        }

        public void OnSelectedUnitChanged(IUnit unit)
        {
            this.SelectedUnitSearchBox = null;

            if (unit != null)
            {
                unit.SelectionSource = SelectionSources.MainTree;
                ((BloodyUser)User.Current).SetCustomProperty(ConfigurationsKeys.SELECTED_UNIT, unit.Identity.ToString());
            }

            if (this.CurrentUnit != null)
            {
                this.CurrentUnit.DeselectUnit();
            }
            this.CurrentUnit = unit as UnitViewModel;

            if (this.CurrentUnit == null)
                return;

            this.EventAggregator.GetEvent<UnitChangedEvent>().Publish(unit); // second refresh
            this.EventAggregator.GetEvent<SelectExtendedUnitEvent>().Publish(
                new SelectedUnit()
                {
                    Unit = unit,
                    UnitName = this.CurrentUnit.Unit.Name,
                }
            );

            DataService.GetAssignedPatientsCountAsync(this.CurrentUnit.Unit.ID);
        }

        private UnitViewModel ConvertUnit(UnitDTO unit)
        {
            UnitViewModel vm = this.Container.Resolve<UnitViewModel>();
            vm.SetUnit(unit, vm);
            return vm;
        }


        public void OnSetUnitAsDefaultEvent(UnitDTO unit)
        {
            //R.Sz.Add
            BloodyUser.Current.SetCustomProperty(ConfigurationsKeys.DEFAULT_UNIT, unit.ID.ToString());
            
            OnSetTabAsDefault();
            //var model = Container.Resolve<UnitPreviewModel>();
            //double height = model.SplitterRowHeight;
            //BloodyUser.Current.SetCustomProperty(ConfigurationsKeys.SPLITTER_HEIGHT, unit.ID.ToString());
        }

        public void OnSetTabAsDefault()
        {
            ITabManager TabManager = Container.Resolve<ITabManager>();
            string name = TabManager.SelectedTabName();
            BloodyUser.Current.SetCustomProperty(ConfigurationsKeys.DEFAULT_TAB, name);
        }

        public void OnSetUserDefaultUnitEvent(bool b)
        {
            var defaultUnitId = Convert.ToInt32(BloodyUser.Current.GetCustomProperty(ConfigurationsKeys.DEFAULT_UNIT));

            if (Convert.ToInt32(defaultUnitId) > 0)
            {
                EventAggregator.GetEvent<ExpandAccordionEvent>().Publish(AccordionItemsNames.UNITS);
                SelectUnit(defaultUnitId);
                OnSetUserDefaultTab();
            }
        }

        public void OnSetUserDefaultTab()
        {
            string defaultUnitName = BloodyUser.Current.GetCustomProperty(ConfigurationsKeys.DEFAULT_TAB);

            if(!string.IsNullOrEmpty(defaultUnitName))
            {
                ITabManager TabManager = Container.Resolve<ITabManager>();
                TabManager.SelectTab(defaultUnitName, null);
            }
        }
        

        public List<UnitDTO> GetFlatChilds(int parentUnitID)
        {
            List<UnitDTO> result = new List<UnitDTO>();
            for (int i = 0; i < AvailableUnits.Count; i++)
            {
                result = AvailableUnits[i].GetFlatChilds(parentUnitID, false);
            }
            return result;
        }

        public int GetSelectedTreeIndex()
        {
            return this.SelectedAccordionItemIndex;
        }

        private void OnPreviewDragStarted(RadTreeViewDragEventArgs e)
        {
            if (IsDragables)
            {
                System.Diagnostics.Debug.WriteLine("UTV preview drag started");

                var unitVm = e.DraggedItems.FirstOrDefault() as UnitViewModel;
                bool draggable;


                if (!BloodyUser.Current.HasPermission(unitVm.Identity, PermissionNames.AddUnit))
                {
                    draggable = false;
                }
                else
                {
                    if (unitVm == Root)
                    {
                        draggable = false;
                    }
                    else
                    {
                        draggable = UnitTypeOrganizer.IsMovableUnit(unitVm.Unit.UnitTypeIDv2);
                    }
                }

                if (!draggable)
                {

                    e.Handled = true;
                }
            }
        }

        private void OnPreviewDragEnded(RadTreeViewDragEndedEventArgs e)
        {
            if (IsDragables)
            {
                var unit = SelectedUnit;


                System.Diagnostics.Debug.WriteLine("UTV preview drag ended");
                var payload = (UnitViewModel) e.DraggedItems.FirstOrDefault(); // viewmodelbase
                var target = e.TargetDropItem.Item as UnitViewModel; //target.Item - viewmodelbase

                e.TargetDropItem.DropPosition = DropPosition.Inside;


                if ((payload.Unit.ParentUnitID == target.Unit.ID) | !CanBeDropped(payload, target))
                {
                    e.Handled = true;
                    return;
                }


                _unitEditData = new UnitEditData
                {
                    Unit = payload.Unit,
                    ParentUnit = target.Unit
                };
                EventAggregator.GetEvent<CloseWindowEvent>()
                    .Subscribe(CloseWindowEventHandling, ThreadOption.UIThread, true);
                this._windowsManager.ShowWindow(WindowsKeys.ConfirmMoveUnit, _unitEditData);

                /*
                var confirmed = MessageBox.Show(
                    String.Format("Potwierdzasz przeniesienie [{0}] z [{1}] do [{2}]?", payload.Unit.Name, payload.Unit.Parent.Name, target.Unit.Name),
                    String.Format("Potwierdzenie przeniesienia jednostki [{0}]", payload.Unit.Name),
                    MessageBoxButton.OKCancel);
    
    
    
                if (confirmed == MessageBoxResult.OK)
                {
                    User.Current.SetProperty(ConfigurationsKeys.SELECTED_UNIT, payload.Unit.ID.ToString());
                    var proxy = ServiceFactory.GetService<UnitsServiceClient>("Finder-BloodDonation-Model-Services-UnitsService.svc");
                    proxy.MoveUnitCompleted += ProxyOnMoveUnitCompleted;
                    proxy.MoveUnitAsync(payload.Unit.ID, target.Unit.ID);
                }
                else
                {
                    e.Handled = true;
                }
    
                */

                //  e.Handled = true;
            }
        }

        private static bool IsDragables = false;

        private bool IsDraggable(UnitViewModel item)
        {
            if (item == null)
                return false;

            if (item == Root)
                return false;


            if (((BloodyUser)User.Current).HasPermission(item.Unit.ID, PermissionNames.UnitsRearranging)
                && UnitTypeOrganizer.IsMovableUnit(item.Unit.UnitTypeIDv2)
                )
                return IsDragables;

            return false;
        }

        private static bool CanBeDropped(UnitViewModel dragged, UnitViewModel target)
        {

            return UnitTypeOrganizer.CanUnitContainOther(target.Unit.UnitTypeIDv2, dragged.Unit.UnitTypeIDv2);
            /*
            if (dragged.UnitType.IsMovable())
                return target.UnitType.IsStore();
            else
                return target.UnitType.CanBeDroppedOn();
            */
        }

        private static bool IsDroppableOn__(UnitViewModel item)
        {
            if (item == null)
                return false;

            if (!((BloodyUser)User.Current).HasPermission(item.Unit.ID, PermissionNames.UnitsRearranging))
                return false;

            if (!item.UnitType.CanBeDroppedOn())
                return false;

            return IsDragables;
        }
    }
}
