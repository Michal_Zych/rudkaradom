﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common;
using System.Text;
using Finder.BloodDonation.Tabs.Contents;
using System.Windows.Data;
using Telerik.Windows.Controls;

namespace Finder.BloodDonation.Layout
{
    [ViewModel(typeof(UnitTreeViewModel))]
    public partial class UnitTreeView : UserControl
    {
        private IDynamicEventAggregator EventAggregator;

        [Dependency]
        private IUnityContainer _container { get; set; }


        public UnitTreeView(IUnityContainer container)
        {
            InitializeComponent();
            EventAggregator = container.Resolve<IDynamicEventAggregator>();
            EventAggregator.GetEvent<SelectUnitEvent>().Subscribe(SelectUnitEvent_Event);
            EventAggregator.GetEvent<ExpandUnitEvent>().Subscribe(ExpandUnitEvent_Event);
            EventAggregator.GetEvent<ExpandAccordionEvent>().Subscribe(ExpandAccordionEvent_Event);
            EventAggregator.GetEvent<ExpandAndSelect>().Subscribe(ExpandAndSelect_Event);

            _container = container;
        }


        public void ExpandAccordionEvent_Event(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                for (int i = 0; i < accordion.Items.Count; i++)
                {
                    //((System.Windows.FrameworkElement)(accordion.Items[0])).Name
                    FrameworkElement fe = accordion.Items[i] as FrameworkElement;
                    if (fe != null)
                    {
                        if (fe.Name == name)
                        {
                            accordion.SelectedItem = fe;
                            (accordion.SelectedItem as Telerik.Windows.Controls.RadPanelBarItem).IsExpanded = true;
                            //accordion.SelectedIndex = i;
                        }
                    }
                }
            }
        }

        public void SelectUnitEvent_Event(SelectedUnitInfo unit)
        {
            if (unit.Path != null && unit.Path.Length > 0)
            {
                ExpandUnitEvent_Event(unit);
                TreeView target = null;

                //switch (unit.Tree)
                //{
                //    case AccordionItemsNames.FAVORITES:
                //        //target = TreeViewFavorites;
                //        break;
                //    case AccordionItemsNames.HUBS:
                //        target = hubsTree;
                //        break;
                //    case AccordionItemsNames.UNITS:
                //        //target = TreeView1;
                //        break;
                //}
                Storyboard sb = new Storyboard()
                {
                    Duration = new Duration(new TimeSpan(0, 0, 0, 0, 100))
                };
                sb.Completed += delegate(object s, EventArgs er)
                {
                    if (target != null)
                    {
                        target.SelectItem(unit.Path[unit.Path.Length - 1]);
                    }
                };
                sb.Begin();
            }
        }

        public void ExpandUnitEvent_Event(SelectedUnitInfo unit)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                if (unit.Path != null && unit.Path.Length > 0)
                {
                    RadTreeView tree;
                    var nodes = (UnitViewModel[])unit.Path;
                    var names = nodes.Select(n => n.Unit.Name);
                    var s = string.Join("|", names);

                    tree = listTree;

                    tree.ExpandItemByPath(s, "|");
                    tree.SelectItemByPath(s, "|");

                }
            });
        }

        public void ExpandAndSelect_Event(UnitSelectExpandEventInfo data)
        {
            EventAggregator.GetEvent<ExpandAccordionEvent>().Publish(data.SelectedInfo.Tree);
            EventAggregator.GetEvent<UnitSelectExpandEvent>().Publish(data);
            ITabManager TabManager = _container.Resolve<ITabManager>();
            TabManager.SelectTab(data.SelectedInfo.TabName, null);
            EventAggregator.GetEvent<ContentSubviewRestoredEvent>().Publish(data.SelectedInfo.SubviewName);
        }

        private void cbSearch_LostFocus(object sender, RoutedEventArgs e)
        {

        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            BindingExpression binding = (sender as TextBox).GetBindingExpression(TextBox.TextProperty);
            binding.UpdateSource();
            base.OnKeyUp(e);
        }

        private void accordion_Selected(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {

        }
    }
}
