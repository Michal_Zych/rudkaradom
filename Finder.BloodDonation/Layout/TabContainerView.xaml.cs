﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Composite.Events;
using Finder.BloodDonation.Common.Events;
using System.Windows.Data;

namespace Finder.BloodDonation.Layout
{
	[ViewModel(typeof(TabContainerViewModel))]
	public partial class TabContainerView : UserControl
	{
        IDynamicEventAggregator EventAggregator { get; set; }

		public TabContainerView()
		{
			InitializeComponent();
            Style s = new Style();

            this.Loaded += new RoutedEventHandler(TabContainerView_Loaded);
		}

        void TabContainerView_Loaded(object sender, RoutedEventArgs e)
        {
            EventAggregator = Bootstrapper.StaticContainer.Resolve<IDynamicEventAggregator>();
            EventAggregator.GetEvent<ChangeVisibilityUserInterfaceEvent>().Subscribe(ChangeTabsVisibility);
        }

        public void ChangeTabsVisibility(bool val)
        {
            foreach (var tab in tabControl1.Items)
            {
                TabItem tabObj = (TabItem)tab;
                tabObj.Visibility = val ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
            }
        }

	}
}
