﻿using System;
using System.Linq;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Layout;
using System.Collections.Generic;
using Finder.BloodDonation.Common.Authentication;
using FinderFX.SL.Core.Authentication;
using Finder.BloodDonation.Common.Events;
using Microsoft.Practices.Composite.Presentation.Commands;
using System.Collections.ObjectModel;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Events;
using Finder.BloodDonation.Context;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using System.Windows;
using GalaSoft.MvvmLight.Command;
using System.Windows.Input;
using Telerik.Windows.Controls;
using PostSharp.Aspects;

namespace Finder.BloodDonation.Layout
{
    using Finder.BloodDonation.Settings;
    using Finder.BloodDonation.Filters;
    using System.Windows.Media;
    using Finder.BloodDonation.Model.Enum;
    using mpost.SilverlightFramework;

    public class SimpleTab : ITab
    {
        public SimpleTab(object header, string name, string requiredPermission)
        {
            _header = header;
            _name = name;
            _requiredPermission = requiredPermission;
        }

        #region ITab Members

        private object _header;
        public object Header
        {
            get { return _header; }
        }

        private string _name;
        public string Name { get { return _name; } }

        private ITabViewModel _tabViewModel;
        public ITabViewModel TabViewModel
        {
            get
            {
                return _tabViewModel;
            }
            set
            {
                //if (_tabViewModel != null)
                //    _tabViewModel.LoadingCompleted -= _tabViewModel_LoadingCompleted;
                _tabViewModel = value;
                if (_tabViewModel.State == ViewModelState.Loaded)
                    ContentLoaded(this, EventArgs.Empty);
                else
                    _tabViewModel.LoadingCompleted += _tabViewModel_LoadingCompleted;

            }

        }

        void _tabViewModel_LoadingCompleted(object sender, EventArgs e)
        {
            ContentLoaded(this, EventArgs.Empty);
        }

        private string _requiredPermission;
        public string RequiredPermission
        {
            get
            {
                return _requiredPermission;
            }
        }

        public event EventHandler ContentLoaded = delegate { };

        #endregion

        #region IDisposable Members

        public void Dispose()
        {

        }

        #endregion

        public SimpleTab Clone()
        {
            return new SimpleTab(
                this.Header,
                this.Name,
                this.RequiredPermission);
        }
    }

    [FinderFX.SL.Core.Communication.OnCompleted(AttributeTargetMembers = "regex:Completed")]
    public class UnitViewModel : FinderFX.SL.Core.MVVM.ViewModelBase, IUnit, IDraggableObject
    {
        private IUserSessionManager _userSessionManager = null;

        private bool _isAlarm;
        [RaisePropertyChanged]
        public bool IsAlarm
        {
            get
            {
                return _isAlarm;
            }
            set
            {
                _isAlarm = value;
                SetColorState();
            }
        }

        private bool _isDisableAlarm;
        [RaisePropertyChanged]
        public bool IsDisableAlarm
        {
            get
            {
                return _isDisableAlarm;
            }
            set
            {
                _isDisableAlarm = value;
                SetColorState();
            }
        }

        private bool _isTransmitterDisconnected;
        [RaisePropertyChanged]
        public bool IsTransmitterDisconnected
        {
            get { return _isTransmitterDisconnected; }
            set
            {
                _isTransmitterDisconnected = value;
                SetColorState();
            }
        }



        private bool _isOldData;
        [RaisePropertyChanged]
        public bool IsOldData
        {
            get
            {
                return _isOldData;
            }
            set
            {
                _isOldData = value;
                SetColorState();
            }
        }

        private void SetColorState()
        {
            var state = UnitColorState.Normal;
            if (IsAlarm)
            {
                state = UnitColorState.IsAlarm;
            }
            else if (IsDisableAlarm)
            {
                state = UnitColorState.Normal;
            }
            else if (IsOldData)
            {
                state = UnitColorState.IsOld;
            }
            else if (IsTransmitterDisconnected)
            {
                state = UnitColorState.IsTransmitterDisconnected;
            }
            ColorState = state;
        }

        [RaisePropertyChanged]
        public UnitColorState ColorState { get; set; }

        [RaisePropertyChanged]
        public UnitDTO Unit { get; private set; }

        [RaisePropertyChanged]
        public ObservableCollection<int> UnitIdWithDisconnectedTransmitter { get; set; }

        [RaisePropertyChanged]
        public Brush OldDataBrush1 { get { return Settings.Current.OldDataBrush; } }

        public IList<UnitViewModel> Children { get; private set; }
        public int SelectionSource { get; set; }
        public string UnitDescription { get; set; }
        public bool IsMonitored { get; set; }

        [RaisePropertyChanged]
        public int PrepCount { get; set; }

        [RaisePropertyChanged]
        public string CurrentState { get; set; }

        public UnitViewModel Root { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> RemoveFromFavoritesCommand { get; set; }

        public RelayCommand<KeyEventArgs> EditKeyUpCommand { get; set; }
        public RelayCommand EditLostFocusCommand { get; set; }
        public DelegateCommand<object> RollbackUnitCommand { get; set; }
        public DelegateCommand<object> ChangeName { get; set; }

        public RelayCommand<UnitDTO> cbSelectionChanged { get; set; }

        public RelayCommand SaveUnitCommand { get; set; }
        public RelayCommand CancelUnitCommand { get; set; }
        public event EventHandler UnitRemoved = delegate { };

        public UnitViewModel(IUnityContainer container)
            : base(container)
        {
            this.Children = new ObservableCollection<UnitViewModel>();
            this.RemoveFromFavoritesCommand = new DelegateCommand<object>(OnRemoveFromFavoriteCommand);

            this.SaveUnitCommand = new RelayCommand(OnSaveUnitCommand);
            this.CancelUnitCommand = new RelayCommand(OnCanelEditUnitCommand);
            this.ChangeName = new DelegateCommand<object>(OnEditUnitCommand);
            this.EditKeyUpCommand = new RelayCommand<KeyEventArgs>(OnKeyUpCommand);
            this.EditLostFocusCommand = new RelayCommand(OnEditLostFocusCommand);
            _userSessionManager = container.Resolve<IUserSessionManager>();

            EventAggregator.GetEvent<UnitIdWithDisconnectedTransmitterReceived>().Subscribe(OnUnitIdWithDisconnectedTransmitter);
            
        }

        public void OnUnitIdWithDisconnectedTransmitter(IList<int> obj)
        {
            UnitIdWithDisconnectedTransmitter = new ObservableCollection<int>(obj);
        }

        public void SetUnit(UnitDTO unit, UnitViewModel root)
        {
            Unit = unit;
            UnitDescription = unit.Description;
            IsMonitored = unit.IsMonitored;
            IsAlarm = _userSessionManager.IsUnitAlarm(Unit.ID);
            IsDisableAlarm = _userSessionManager.IsUnitAlarmingDisabled(Unit.ID);
            IsOldData = _userSessionManager.IsOldDataUnit(Unit.ID);
            UnitRemoved += new EventHandler(vm_UnitRemoved);
            SetContextMenu();

            if (this != root)
                Root = root;

            if (unit.Children != null)
            {
                foreach (UnitDTO u in unit.Children)
                {
                    UnitViewModel vm = Container.Resolve<UnitViewModel>();
                    vm.SetUnit(u, root);
                    vm.UnitRemoved += new EventHandler(vm_UnitRemoved);
                    Children.Add(vm);
                }
            }

            SetContextMenu();
        }

        public void SetFavoriteUnit(UnitDTO unit)
        {
            Unit = unit;
            IsAlarm = _userSessionManager.IsUnitAlarm(Unit.ID);
            IsDisableAlarm = _userSessionManager.IsUnitAlarmingDisabled(Unit.ID);
            IsMonitored = unit.IsMonitored;
            if (unit.Children != null)
            {
                foreach (UnitDTO u in unit.Children)
                {
                    UnitViewModel vm = Container.Resolve<UnitViewModel>();
                    vm.SetFavoriteUnit(u);
                    vm.UnitRemoved += new EventHandler(vm_UnitRemoved);
                    vm.SetFavoriteContextMenu();
                    Children.Add(vm);
                }
            }

            //SetContextMenu();
        }

        public IUnit SearchUnit(int unit)
        {
            if (Unit != null)
            {
                if (Unit.ID == unit)
                {
                    return this;
                }
            }

            if (Children != null)
            {
                for (int i = 0; i < Children.Count; i++)
                {
                    IUnit u = Children[i].SearchUnit(unit);
                    if (u != null)
                    {
                        return u;
                    }
                }
            }

            return null;
        }

        public IUnit UnitDepartment(int unitID)
        {
            if (Unit.UnitType == Model.Enum.UnitType.DEPARTMENT)
            {
                if (Children != null)
                {
                    for (int i = 0; i < Children.Count; i++)
                    {
                        IUnit u = Children[i].SearchUnit(unitID);
                        if (u != null)
                        {
                            return this;
                        }
                    }
                }
            }
            else
            {
                if (Children != null)
                {
                    for (int i = 0; i < Children.Count; i++)
                    {
                        IUnit u = Children[i].UnitDepartment(unitID);
                        if (u != null)
                        {
                            return u;
                        }
                    }
                }
            }

            return null;
        }

        #region Alarms

        public void SetAlarm(int units)
        {
            if (units == Unit.ID)
            {
                IsAlarm = true;
                NotifyPropertyChange("IsAlarm");
            }
            else
            {
                if (Children != null)
                {
                    for (int i = 0; i < Children.Count; i++)
                    {
                        Children[i].SetAlarm(units);
                    }
                }
            }
        }

        public void UnsetAlarm(int unit)
        {
            if (Unit.ID == unit)
            {
                if (IsAlarm)
                {
                    IsAlarm = false;
                    NotifyPropertyChange("IsAlarm");
                }
            }
            else
            {
                if (Children != null)
                {
                    for (int i = 0; i < Children.Count; i++)
                    {
                        Children[i].UnsetAlarm(unit);
                    }
                }
            }
        }

        public void SetDisableAlarm(int units)
        {
            if (units == Unit.ID)
            {
                IsDisableAlarm = true;
                NotifyPropertyChange("IsDisableAlarm");
            }
            else
            {
                if (Children != null)
                {
                    for (int i = 0; i < Children.Count; i++)
                    {
                        Children[i].SetDisableAlarm(units);
                    }
                }
            }
        }

        public void UnsetDisableAlarm(int unit)
        {
            if (Unit.ID == unit)
            {
                if (IsDisableAlarm)
                {
                    IsDisableAlarm = false;
                    NotifyPropertyChange("IsDisableAlarm");
                }
            }
            else
            {
                if (Children != null)
                {
                    for (int i = 0; i < Children.Count; i++)
                    {
                        Children[i].UnsetDisableAlarm(unit);
                    }
                }
            }
        }


        public void SetTransmitterDisconnected(int unit)
        {
            if (unit == Unit.ID)
            {
                IsDisableAlarm = true;
                NotifyPropertyChange("IsTransmitterDisconnected");
            }
            else
            {
                if (Children != null)
                {
                    for (int i = 0; i < Children.Count; i++)
                    {
                        Children[i].SetTransmitterDisconnected(unit);
                    }
                }
            }
        }

        public void UnsetTransmitterDisconnected(int unit)
        {
            if (Unit.ID == unit)
            {
                if (IsDisableAlarm)
                {
                    IsDisableAlarm = false;
                    NotifyPropertyChange("IsTransmitterDisconnected");
                }
            }
            else
            {
                if (Children != null)
                {
                    for (int i = 0; i < Children.Count; i++)
                    {
                        Children[i].UnsetTransmitterDisconnected(unit);
                    }
                }
            }
        }

        public void SetOldData(int units)
        {
            if (units == Unit.ID)
            {
                IsOldData = true;
                NotifyPropertyChange("IsOldData");
            }
            else
            {
                if (Children != null)
                {
                    for (int i = 0; i < Children.Count; i++)
                    {
                        Children[i].SetOldData(units);
                    }
                }
            }
        }

        public void UnsetOldData(int unit)
        {
            if (Unit.ID == unit)
            {
                if (IsOldData)
                {
                    IsOldData = false;
                    NotifyPropertyChange("IsOldData");
                }
            }
            else
            {
                if (Children != null)
                {
                    for (int i = 0; i < Children.Count; i++)
                    {
                        Children[i].UnsetOldData(unit);
                    }
                }
            }
        }

        public UnitViewModel FillUnitPath(List<UnitViewModel> path, int unit)
        {
            if (Unit.ID == unit)
            {
                path.Add(this);
                return this;
            }
            else
            {
                for (int i = 0; i < Children.Count; i++)
                {
                    UnitViewModel r = Children[i].FillUnitPath(path, unit);
                    if (r != null)
                    {
                        path.Add(this);
                        return r;
                    }
                }
            }

            return null;
        }

        public UnitViewModel FillUnitPathByType(List<UnitViewModel> path, UnitType uType)
        {
            if (Unit.UnitType == uType)
            {
                path.Add(this);
                return this;
            }
            else
            {
                for (int i = 0; i < Children.Count; i++)
                {
                    UnitViewModel r = Children[i].FillUnitPathByType(path, uType);
                    if (r != null)
                    {
                        path.Add(this);
                        return r;
                    }
                }
            }

            return null;
        }

        #endregion Alarms

        #region IUnit Members

        public System.Collections.Generic.IList<ITab> AvailableTabs
        {
            get
            {
                List<ITab> allTabs = UnitTypeHelper.GetAllTabsForType(this.Unit.UnitTypeIDv2);

                //TODO: uwzglednic uprawnienia uzytkownika (role i dostep do obiektow)
                List<ITab> availableTabs = new List<ITab>();
                foreach (ITab t in allTabs)
                {
                    if (UserHasPermission(this.Unit.ID, t.RequiredPermission))
                    {
                        
                        // TYMCZASOWO USUNIĘTA ZAKŁADKA LOKALIZACJA I PLANOWANIE DLA SZPITALA
                        if (this.Unit.ID == 1 && (t.Name == TabNames.Localization || t.Name == TabNames.UnitPlan))
                            continue;
               
                        // RADOM
                        if (this.Unit.ID != int.Parse(App.Current.Resources["ConfigurableUnitId"].ToString()) && t.Name == TabNames.LocalizationConfig)
                            continue;

                        //nie dodawaj zakładki Planowanie dla unitu z ulubionych
                        if (!((SelectionSource == SelectionSources.Favorites) && (t.Name == TabNames.PreviewEdit)))
                        {
                            availableTabs.Add(t);
                        }
                    }
                }

                return availableTabs;
            }
        }

        private bool UserHasPermission(int unitID, string permission)
        {
            BloodyUser user = (BloodyUser)User.Current;

            return user.HasPermission(unitID, permission);
        }

        private bool UserHasPermission(string requiredPermission)
        {
            return true;
        }

        public int Identity
        {
            get
            {
                return this.Unit != null ? this.Unit.ID : 0;
            }
        }

        public UnitType UnitType
        {
            get
            {
                return 0;
                //return UnitType;
            }
        }
        #endregion

        public void OnRemoveFromFavoriteCommand(object obj)
        {
            /*IList<UnitDTO> list = new List<UnitDTO>();

            if (this.Unit.UnitType == UnitType.FAVORITE &&
                this.Children.Count > 0)
            {
                foreach (UnitViewModel vm in this.Children)
                {
                    list.Add(vm.Unit);
                }
            }
            else
            {
                list.Add(this.Unit);
            }


            this.EventAggregator.GetEvent<RemoveFromFavoriteEvent>().Publish(list);*/
        }

        public void OnAddToFavoritesCommand(object obj)
        {
            this.EventAggregator.GetEvent<AddToFavoriteEvent>().Publish(new List<UnitDTO> { this.Unit });
        }


        public void OnSetUnitAsDefaultCommand(object obj)
        {
            this.EventAggregator.GetEvent<SetUnitAsDefaultEvent>().Publish(this.Unit);
        }

        public override string ToString()
        {
            return (Unit != null) ? Unit.Name : "undef";
        }

        #region Context menu

        [RaisePropertyChanged]
        public GenericContextMenuItemsCollection<UnitViewModel> Context { get; set; }

        [RaisePropertyChanged]
        public GenericContextMenuItemsCollection<UnitViewModel> FavoriteContext { get; set; }

        private void SetContextMenu()
        {
            Context = new UnitContextCollection(this);
        }

        public class FavoriteContextItem : GenericMenuItem
        {
            private UnitViewModel ViewModel { get; set; }

            public FavoriteContextItem(UnitViewModel vm)
            {
                ViewModel = vm;
                Header = "Usuń z ulubionych";
                Command = ViewModel.RemoveFromFavoritesCommand;
            }

            public override bool IsAvailable()
            {
                //available for everything
                return true;
                //return (ViewModel.Unit.IsMonitoredDevice());
            }
        }


        public void SetFavoriteContextMenu()
        {
            FavoriteContext = new GenericContextMenuItemsCollection<UnitViewModel>(this);
            FavoriteContext.RegisterItems(new List<IContextMenuItem> { new FavoriteContextItem(this) });
            Context = FavoriteContext;
        }

        #endregion Context menu

        #region Add unit


        /*
        private void AddRegularUnit(UnitDTO parentUnit, UnitType unitType)
        {
            UnitDTO newUnit = new UnitDTO();
            newUnit.Name = Settings.Current["UnitNewlyCreatedName"];//"<Zmień nazwę>";
            newUnit.ParentUnitID = parentUnit.ID;
            newUnit.UnitType = unitType;
            newUnit.ClientID = parentUnit.ClientID;
            newUnit.Children = new List<UnitDTO>();

            UnitsServiceClient proxy = ServiceFactory.GetService<UnitsServiceClient>();
            proxy.InsertUnitCompleted += new EventHandler<InsertUnitCompletedEventArgs>(proxy_InsertUnitCompleted);
            proxy.InsertUnitAsync(newUnit, newUnit);
        }*/
        /*
        void proxy_InsertUnitCompleted(object sender, InsertUnitCompletedEventArgs e)
        {
            if (e.Result != 0)
            {
                UnitDTO addedUnit = e.UserState as UnitDTO;
                addedUnit.ID = e.Result;

                if (this.Unit.Children == null)
                    this.Unit.Children = new List<UnitDTO>();

                this.Unit.Children.Add(addedUnit);
                //TODO: odswiezyc uprawnienia uzytkownika
                //pobrac na nowo? 
                //czy recznie przebudowac?

                UnitViewModel vm = this.Container.Resolve<UnitViewModel>();
                vm.SetUnit(addedUnit, Root);
                vm.SetContextMenu();
                vm.UnitRemoved += new EventHandler(vm_UnitRemoved);
                this.Children.Add(vm);

                BloodyUser.Current.ReloadPermissions();

            }
        }
         * */

        #endregion Add unit

        #region Remove unit


        public void vm_UnitRemoved(object sender, EventArgs e)
        {
            this.Children.Remove(sender as UnitViewModel);
            if (Children.Count == 0) IsMonitored = false;
        }

        #endregion Remove unit

        #region Edit unit

        public void OnEditUnitCommand_(object obj)
        {

        }

        public void DeselectUnit()
        {
            this.Unit.CancelEdit();
            this.CurrentState = "Normal";
        }

        public void OnSaveUnitCommand()
        {

        }

        public void OnCanelEditUnitCommand()
        {
            this.DeselectUnit();
        }

        public void OnKeyUpCommand(KeyEventArgs args)
        {
            switch (args.Key)
            {
                case Key.Escape:
                    this.DeselectUnit();
                    break;
                case Key.Enter:
                    this.SaveUnitCommand.Execute(null);
                    this.CurrentState = "Normal";
                    break;
            }
        }

        public void OnEditLostFocusCommand()
        {
            //this.DeselectUnit();
        }



        #endregion Edit unit

        #region IDraggableObject Members

        public bool Cancel { get; set; }

        #endregion


        internal List<UnitDTO> GetFlatChilds(int parentUnitID, bool f)
        {
            List<UnitDTO> result = new List<UnitDTO>();
            if (Unit.ID == parentUnitID)
            {
                f = true;
            }

            for (int i = 0; i < Children.Count; i++)
            {
                if (f)
                {
                    result.Add(Children[i].Unit);
                }
                result.AddRange(Children[i].GetFlatChilds(parentUnitID, f));
            }

            return result;
        }

        public void OnTestOkienkaCommand(object obj)
        {
            this.EventAggregator.GetEvent<TestOkienkaEvent>().Publish(this.Unit);
        }


        public void OnAddUnitCommand(object obj)
        {
            this.EventAggregator.GetEvent<AddUnitEvent>().Publish(this.Unit);
        }

        public void OnAddSensorCommand(object obj)
        {
            this.EventAggregator.GetEvent<AddSensorEvent>().Publish(this.Unit);
        }

        public void OnEditUnitCommand(object obj)
        {
            this.EventAggregator.GetEvent<EditUnitEvent>().Publish(this.Unit);
        }

        public void OnDeleteUnitCommand(object obj)
        {
            this.EventAggregator.GetEvent<DeleteUnitEvent>().Publish(this.Unit);
        }

    }
}
