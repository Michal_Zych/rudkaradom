﻿using System;
namespace Finder.BloodDonation.JsInterop
{
    public interface IJsComBridge
    {
        //void EnableTransmission();
        //void DisableTransmission();
        void AcquireTagInfo(string result);
		void AcquireErrorInfo(string result);
        void SetTagInfo(string info);
		void SetUserTag(int userId, string userName);
		void SetIsbtTag(string tagParams);
	}
}
