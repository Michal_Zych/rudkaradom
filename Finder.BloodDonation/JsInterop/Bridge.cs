﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using System.Windows.Browser;
using Finder.BloodDonation.Common.Events;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Model.Dto;
using System.Globalization;

namespace Finder.BloodDonation.JsInterop
{
    public class Bridge : Finder.BloodDonation.JsInterop.IJsComBridge
    {
        public const string TagTypeLogin = "taglogin";
        public const string TagTypeStarted = "started";
        public const string TagTypeRead = "tagread";
        public const string TagTypeWritten = "tagwritten";

        public const int TagCommandClear = 0;
        public const int TagCommandReload = 1;

        private IDynamicEventAggregator EventAggregator;
        private string _activeTarget;

        public Bridge(IUnityContainer container, IDynamicEventAggregator aggregator)
        {
            EventAggregator = aggregator;

            HtmlPage.RegisterScriptableObject("Bridge", this);

            EventAggregator.GetEvent<RfidTransmissionStateChangedEvent>().Subscribe(HandleRfidTransmissionStateChangedEvent);
            EventAggregator.GetEvent<RfidSetUserTagEvent>().Subscribe(HandleRfidSetUserTagEvent);
            EventAggregator.GetEvent<RfidSetPrepTagEvent>().Subscribe(HandleRfidSetPrepTagEvent);
            EventAggregator.GetEvent<RfidSetCommandEvent>().Subscribe(HandleRfidSetCommandEvent);

            EventAggregator.GetEvent<ActiveRfidInfoTargetSetEvent>().Subscribe(HandleActiveRfidInfoTargetSetEvent);
        }

        [ScriptableMember]
        public void AcquireTagInfo(string result)
        {
            var info = ConvertToRfidInfo(result);
            info.ActiveTarget = _activeTarget;

            System.Diagnostics.Debug.WriteLine("Bridge; Publishing tag info: {0}", info);

            EventAggregator.GetEvent<TagInfoReceivedEvent>().Publish(info);

            //if ((result = ConvertTagUrl(result)) != null)
            //    EventAggregator.GetEvent<TagDataReceivedEvent>().Publish(result);
        }

        [ScriptableMember]
        public void AcquireErrorInfo(string result)
        {

        }

        public void HandleRfidTransmissionStateChangedEvent(bool enable)
        {
            System.Diagnostics.Debug.WriteLine("handling rfid up in bridge");

            /* To wywala także na prodzie, tylko nie wyświetla błędów - co to jest w ogóle?
             * if (enable)
                HtmlPage.Window.Invoke("enableRfidTransmission", "");
            else
                HtmlPage.Window.Invoke("disableRfidTransmission", "");*/
        }

        public void HandleRfidSetUserTagEvent(AccountDto account)
        {
            SetUserTag(account.ID, account.Login);
        }

        public void HandleRfidSetPrepTagEvent(PrepDto prep)
        {
            var info = String.Format("{0}|{1}|{2}|{3:yyyy-MM-dd HH:mm:ss}|{4}", prep.DonationNumber, prep.Group, prep.Type, prep.ValidUntil, prep.Capacity);
            SetIsbtTag(info);
        }

        public void HandleActiveRfidInfoTargetSetEvent(string activeTarget)
        {
            //System.Diagnostics.Debug.WriteLine("Bridge; Setting active target to: {0}", activeTarget);
            _activeTarget = activeTarget;
        }

        public void HandleRfidSetCommandEvent(int command)
        {
            switch (command)
            {
                case TagCommandClear:
                    ClearTag();
                    break;
                case TagCommandReload:
                    ReloadTags();
                    break;
            }
        }

        public void SetTagInfo(string info)
        {
            HtmlPage.Window.Invoke("setTagData", info);
        }

        public void SetUserTag(int userId, string userName)
        {
            HtmlPage.Window.Invoke("setUserTag", userId, userName);
        }

        public void SetIsbtTag(string tagParams)
        {
            HtmlPage.Window.Invoke("setIsbtTag", tagParams);
        }

        public void ClearTag()
        {
            HtmlPage.Window.Invoke("clearTag");
        }

        public void ReloadTags()
        {
            HtmlPage.Window.Invoke("reloadTags");
        }

        public static string ConvertTagUrl(string url)
        {
            if (url.Contains("&"))
            {
                var parts = url.Split("&".ToCharArray());

                var type = parts[0].Substring(parts[0].IndexOf("=") + 1);
                var uid = parts[1].Substring(parts[1].IndexOf("=") + 1);
                var content = parts[2].Substring(parts[2].IndexOf("=") + 1);

                return String.Format("{0};{1};{2}", type, uid, content);
            }

            return null;
        }

        private static RfidInfo ConvertToRfidInfo(string url)
        {
            var info = new RfidInfo();

            if (url.Contains("&"))
            {
                var parts = url.Split("&".ToCharArray());

                var type = parts[0].Substring(parts[0].IndexOf("=") + 1);
                var uid = parts[1].Substring(parts[1].IndexOf("=") + 1);
                var content = parts[2].Substring(parts[2].IndexOf("=") + 1);

                info.Type = type.ToLowerInvariant();
                //info.Uid = Convert.ToUInt64(uid);
                info.Uid = unchecked((long)Convert.ToUInt64(uid));

                if (content.Equals("00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00"))
                    info.Content = String.Empty;
                else
                    info.Content = content;

                switch (info.Type)
                {
                    case TagTypeLogin:

                        var loginDetails = info.Content.Split("|".ToCharArray());
                        var userId = Convert.ToInt32(loginDetails[0], CultureInfo.InvariantCulture);
                        var login = loginDetails[1];

                        info.UserId = userId;
                        info.Content = login;

                        break;
                    default:
                        break;
                }
            }
            else
            {
                info.Type = TagTypeStarted;
            }

            return info;
        }
    }
}
