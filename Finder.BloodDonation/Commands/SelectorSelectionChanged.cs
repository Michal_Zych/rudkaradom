﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core;
using System.Windows.Controls.Primitives;

namespace Finder.BloodDonation.Commands
{
    public class SelectorSelectionChangedBehavior : CommandBehaviorBase<Selector>
    {
        public SelectorSelectionChangedBehavior(Selector element)
            : base(element)
        {
            element.SelectionChanged += new SelectionChangedEventHandler(element_SelectionChanged);
        }

        void element_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            base.CommandParameter = ((Selector)sender).SelectedItem;
            base.ExecuteCommand();
        }
    }

    public static partial class SelectorCommands
    {
        public static string GetSelectorSelectionChanged(DependencyObject obj)
        {
            return (string)obj.GetValue(SelectorSelectionChangedProperty);
        }

        public static void SetSelectorSelectionChanged(DependencyObject obj, string value)
        {
            obj.SetValue(SelectorSelectionChangedProperty, value);
        }

        public static readonly DependencyProperty SelectorSelectionChangedProperty =
            DependencyProperty.RegisterAttached(
                "SelectorSelectionChanged"
                , typeof(ICommand)
                , typeof(SelectorCommands)
                , new PropertyMetadata(OnSetCommandCallbackSelectionChanged));

        private static void OnSetCommandCallbackSelectionChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            Selector element = dependencyObject as Selector;
            if (element == null)
                throw new ArgumentException("SelectorSelectionChanged property can be applied only for Selector control");

            SelectorSelectionChangedBehavior behavior = element.GetValue(SelectorSelectionChangedCommandBehaviorProperty) as SelectorSelectionChangedBehavior;
            if (behavior == null)
            {
                behavior = new SelectorSelectionChangedBehavior(element);
                behavior.Command = e.NewValue as ICommand;
            }

        }

        public static SelectorSelectionChangedBehavior GetSelectorSelectionChangedCommandBehavior(DependencyObject obj)
        {
            return (SelectorSelectionChangedBehavior)obj.GetValue(SelectorSelectionChangedCommandBehaviorProperty);
        }

        public static void SetSelectorSelectionChangedCommandBehavior(DependencyObject obj, SelectorSelectionChangedBehavior value)
        {
            obj.SetValue(SelectorSelectionChangedCommandBehaviorProperty, value);
        }

        public static readonly DependencyProperty SelectorSelectionChangedCommandBehaviorProperty =
            DependencyProperty.RegisterAttached("SelectorSelectionChangedCommandBehavior",
            typeof(SelectorSelectionChangedBehavior), typeof(SelectorCommands), null);
    }
}
