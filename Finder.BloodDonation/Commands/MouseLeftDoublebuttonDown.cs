﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.Behaviors;
using FinderFX.SL.Core;

namespace Finder.BloodDonation.Commands
{
    public class DMouseLeftButtonDownCommand
    {
        private static readonly DependencyProperty DMouseLeftButtonDownCommandProperty = DependencyProperty.RegisterAttached(
            "DMouseLeftButtonDownCommand",
            typeof(DMouseLeftButtonDownCommandBehavior),
            typeof(DMouseLeftButtonDownCommand),
            null);

        public static readonly DependencyProperty CommandProperty = DependencyProperty.RegisterAttached(
            "Command",
            typeof(ICommand),
            typeof(DMouseLeftButtonDownCommand),
            new PropertyMetadata(OnSetCommandCallback));

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "Only works for selector")]
        public static void SetCommand(FrameworkElement selector, ICommand command)
        {
            selector.SetValue(CommandProperty, command);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "Only works for selector")]
        public static ICommand GetCommand(FrameworkElement selector)
        {
            return selector.GetValue(CommandProperty) as ICommand;
        }

        private static void OnSetCommandCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement dataForm = dependencyObject as FrameworkElement;
            if (dataForm == null)
            {
                throw new ArgumentException("MouseLeftButtonDownCommand property can be applied only for FrameworkElement");
            }

            DMouseLeftButtonDownCommandBehavior behavior = dataForm.GetValue(DMouseLeftButtonDownCommandProperty) as DMouseLeftButtonDownCommandBehavior;
            if (behavior == null)
            {
                behavior = new DMouseLeftButtonDownCommandBehavior(dataForm);
                behavior.Command = e.NewValue as ICommand;
            }
        }

        private static DMouseLeftButtonDownCommandBehavior GetOrCreateBehavior(FrameworkElement selector)
        {
            var behavior = selector.GetValue(DMouseLeftButtonDownCommandProperty) as DMouseLeftButtonDownCommandBehavior;
            if (behavior == null)
            {
                behavior = new DMouseLeftButtonDownCommandBehavior(selector);
                selector.SetValue(DMouseLeftButtonDownCommandProperty, behavior);
            }

            return behavior;
        }
    }

    public class DMouseLeftButtonDownCommandBehavior : CommandBehaviorBase<FrameworkElement>
    {
        long last_tick = 0L;

        public DMouseLeftButtonDownCommandBehavior(FrameworkElement element)
            : base(element)
        {
            element.MouseLeftButtonDown += new MouseButtonEventHandler(element_MouseLeftButtonDown);
        }

        void element_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TimeSpan ts = new TimeSpan(DateTime.Now.Ticks - last_tick);
            if (last_tick > 0 && ts.TotalMilliseconds > 500 && ts.TotalMilliseconds < 1500)
            {
                base.CommandParameter = sender;
                base.ExecuteCommand();
            }
            last_tick = DateTime.Now.Ticks;
        }
    }
}
