﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core;

namespace Finder.BloodDonation.Commands
{
	public class TreeViewSelectedItemChangedBehavior : CommandBehaviorBase<TreeView>
	{
		public TreeViewSelectedItemChangedBehavior(TreeView element)
			: base(element)
		{
			element.SelectedItemChanged += new RoutedPropertyChangedEventHandler<object>(element_SelectedItemChanged);
		}

		void element_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			base.CommandParameter = ((TreeView)sender).SelectedItem;
			base.ExecuteCommand();
		}


	}

	public static partial class TreeViewCommands
	{
		public static string GetTreeViewSelectedItemChanged(DependencyObject obj)
		{
			return (string)obj.GetValue(TreeViewSelectedItemChangedProperty);
		}

		public static void SetTreeViewSelectedItemChanged(DependencyObject obj, string value)
		{
			obj.SetValue(TreeViewSelectedItemChangedProperty, value);
		}

		public static readonly DependencyProperty TreeViewSelectedItemChangedProperty =
			DependencyProperty.RegisterAttached(
				"TreeViewSelectedItemChanged"
				, typeof(ICommand)
				, typeof(TreeViewCommands)
				, new PropertyMetadata(OnSetCommandCallbackSelectedItemChanged));


		private static void OnSetCommandCallbackSelectedItemChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
		{
			TreeView element = dependencyObject as TreeView;
			if (element == null)
				throw new ArgumentException("TreeViewSelectedItemChanged property can be applied only for TreeView control");

			TreeViewSelectedItemChangedBehavior behavior = element.GetValue(TreeViewSelectedItemChangedCommandBehaviorProperty) as TreeViewSelectedItemChangedBehavior;
			if (behavior == null)
			{
				behavior = new TreeViewSelectedItemChangedBehavior(element);
				behavior.Command = e.NewValue as ICommand;
			}

		}

		public static TreeViewSelectedItemChangedBehavior GetTreeViewSelectedItemChangedCommandBehavior(DependencyObject obj)
		{
			return (TreeViewSelectedItemChangedBehavior)obj.GetValue(TreeViewSelectedItemChangedCommandBehaviorProperty);
		}

		public static void SetTreeViewSelectedItemChangedCommandBehavior(DependencyObject obj, TreeViewSelectedItemChangedBehavior value)
		{
			obj.SetValue(TreeViewSelectedItemChangedCommandBehaviorProperty, value);
		}

		public static readonly DependencyProperty TreeViewSelectedItemChangedCommandBehaviorProperty =
			DependencyProperty.RegisterAttached("TreeViewSelectedItemChangedCommandBehavior",
			typeof(TreeViewSelectedItemChangedBehavior), typeof(TreeViewCommands), null);



	}
}
