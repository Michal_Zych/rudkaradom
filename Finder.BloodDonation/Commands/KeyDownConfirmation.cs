﻿using System;
using FinderFX.SL.Core;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows;

namespace Finder.BloodDonation.Commands
{
    public class KeyDownConfirmationBehavior : System.Windows.Interactivity.Behavior<TextBox>
    {
        public static ICommand GetConfirmation(DependencyObject obj)
        {
            return obj.GetValue(ConfirmationProperty) as ICommand;
        }

        public static void SetConfirmation(DependencyObject obj, ICommand value)
        {
            obj.SetValue(ConfirmationProperty, value);
        }

        public static readonly DependencyProperty ConfirmationProperty =
                            DependencyProperty.RegisterAttached("Confirmation", typeof(ICommand),
                            typeof(KeyDownConfirmationBehavior), new PropertyMetadata(null));


        public static ICommand GetClear(DependencyObject obj)
        {
            return obj.GetValue(ClearProperty) as ICommand;
        }

        public static void SetClear(DependencyObject obj, ICommand value)
        {
            obj.SetValue(ClearProperty, value);
        }

        public static readonly DependencyProperty ClearProperty =
                            DependencyProperty.RegisterAttached("Clear", typeof(ICommand),
                            typeof(KeyDownConfirmationBehavior), new PropertyMetadata(null));




        protected override void OnAttached()
        {
            this.AssociatedObject.KeyDown += new KeyEventHandler(AssociatedObject_KeyDown);
        }

        void AssociatedObject_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Confirmation();
                return;
            }

            if (e.Key == Key.Escape)
            {
                Clear();
                return;
            }


        }

        private void Confirmation()
        {
            //force re-binding
            this.AssociatedObject
                .GetBindingExpression(TextBox.TextProperty).UpdateSource();

            ICommand confirmationCommand = GetConfirmation(this.AssociatedObject);
            if (confirmationCommand != null)
            {
                confirmationCommand.Execute(null);
            }
        }

        private void Clear()
        {
            ICommand confirmationCommand = GetClear(this.AssociatedObject);
            if (confirmationCommand != null)
            {
                confirmationCommand.Execute(null);
            }
            this.AssociatedObject.Text = string.Empty;
        }
    }
}
