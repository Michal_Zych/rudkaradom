﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.Behaviors;
using FinderFX.SL.Core;

namespace Finder.BloodDonation.Commands
{
    public class DataGridSelectionItemsChanged
    {
        private static readonly DependencyProperty DataGridSelectionItemsChangedProperty = DependencyProperty.RegisterAttached(
            "DataGridSelectionItemsChanged",
            typeof(DataGridSelectionItemsChangedCommandBehavior),
            typeof(DataGridSelectionItemsChanged),
            null);

        public static readonly DependencyProperty CommandProperty = DependencyProperty.RegisterAttached(
            "Command",
            typeof(ICommand),
            typeof(DataGridSelectionItemsChanged),
            new PropertyMetadata(OnSetCommandCallback));

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "Only works for selector")]
        public static void SetCommand(DataGrid selector, ICommand command)
        {
            selector.SetValue(CommandProperty, command);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "Only works for selector")]
        public static ICommand GetCommand(DataGrid selector)
        {
            return selector.GetValue(CommandProperty) as ICommand;
        }

        private static void OnSetCommandCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            DataGrid dataForm = dependencyObject as DataGrid;
            if (dataForm == null)
            {
                throw new ArgumentException("SelectedIndexChanged property can be applied only for DataGrid control");
            }

            DataGridSelectionItemsChangedCommandBehavior behavior = dataForm.GetValue(DataGridSelectionItemsChangedProperty) as DataGridSelectionItemsChangedCommandBehavior;
            if (behavior == null)
            {
                behavior = new DataGridSelectionItemsChangedCommandBehavior(dataForm);
                behavior.Command = e.NewValue as ICommand;
            }
        }

        private static DataGridSelectionItemsChangedCommandBehavior GetOrCreateBehavior(DataGrid selector)
        {
            var behavior = selector.GetValue(DataGridSelectionItemsChangedProperty) as DataGridSelectionItemsChangedCommandBehavior;
            if (behavior == null)
            {
                behavior = new DataGridSelectionItemsChangedCommandBehavior(selector);
                selector.SetValue(DataGridSelectionItemsChangedProperty, behavior);
            }

            return behavior;
        }
    }

    public class DataGridSelectionItemsChangedCommandBehavior : CommandBehaviorBase<DataGrid>
    {
        public DataGridSelectionItemsChangedCommandBehavior(DataGrid element)
            : base(element)
        {
            element.SelectionChanged += new SelectionChangedEventHandler(element_SelectionChanged);
        }

        void element_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            base.CommandParameter = ((DataGrid)sender).SelectedItems;
            base.ExecuteCommand();
        }
    }
}
