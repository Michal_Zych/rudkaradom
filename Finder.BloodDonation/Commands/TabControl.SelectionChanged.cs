﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core;

namespace Finder.BloodDonation.Commands
{
	public class TabControlSelectionChangedBehavior : CommandBehaviorBase<TabControl>
	{
		public TabControlSelectionChangedBehavior(TabControl element)
			: base(element)
		{
			element.SelectionChanged += new SelectionChangedEventHandler(element_SelectionChanged);
		}

		void element_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			base.CommandParameter = ((TabControl)sender).SelectedItem;
			base.ExecuteCommand();
		}

	}

	public static partial class TabControlCommands
	{
		public static string GetTabControlSelectionChanged(DependencyObject obj)
		{
			return (string)obj.GetValue(TabControlSelectionChangedProperty);
		}

		public static void SetTabControlSelectionChanged(DependencyObject obj, string value)
		{
			obj.SetValue(TabControlSelectionChangedProperty, value);
		}

		public static readonly DependencyProperty TabControlSelectionChangedProperty =
			DependencyProperty.RegisterAttached(
				"TabControlSelectionChanged"
				, typeof(ICommand)
				, typeof(TabControlCommands)
				, new PropertyMetadata(OnSetCommandCallbackSelectionChanged));


		private static void OnSetCommandCallbackSelectionChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
		{
			TabControl element = dependencyObject as TabControl;
			if (element == null)
				throw new ArgumentException("TabControlSelectionChanged property can be applied only for TabControl control");

			TabControlSelectionChangedBehavior behavior = element.GetValue(TabControlSelectionChangedCommandBehaviorProperty) as TabControlSelectionChangedBehavior;
			if (behavior == null)
			{
				behavior = new TabControlSelectionChangedBehavior(element);
				behavior.Command = e.NewValue as ICommand;
			}

		}

		public static TabControlSelectionChangedBehavior GetTabControlSelectionChangedCommandBehavior(DependencyObject obj)
		{
			return (TabControlSelectionChangedBehavior)obj.GetValue(TabControlSelectionChangedCommandBehaviorProperty);
		}

		public static void SetTabControlSelectionChangedCommandBehavior(DependencyObject obj, TabControlSelectionChangedBehavior value)
		{
			obj.SetValue(TabControlSelectionChangedCommandBehaviorProperty, value);
		}

		public static readonly DependencyProperty TabControlSelectionChangedCommandBehaviorProperty =
			DependencyProperty.RegisterAttached("TabControlSelectionChangedCommandBehavior",
			typeof(TabControlSelectionChangedBehavior), typeof(TabControlCommands), null);



	}
}
