﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core;

namespace Finder.BloodDonation.Commands
{
	public class CheckBoxUncheckedBehavior : CommandBehaviorBase<CheckBox>
	{
		public CheckBoxUncheckedBehavior(CheckBox element)
			: base(element)
		{
			element.Unchecked += new RoutedEventHandler(element_Unchecked);
		}

		void element_Unchecked(object sender, RoutedEventArgs e)
		{
			base.CommandParameter = ((CheckBox)sender).IsChecked;
			base.ExecuteCommand();
		}

	

	}

	public static partial class CheckBoxCommands
	{
		public static string GetCheckBoxUnchecked(DependencyObject obj)
		{
			return (string)obj.GetValue(CheckBoxUncheckedProperty);
		}

		public static void SetCheckBoxUnchecked(DependencyObject obj, string value)
		{
			obj.SetValue(CheckBoxUncheckedProperty, value);
		}

		public static readonly DependencyProperty CheckBoxUncheckedProperty =
			DependencyProperty.RegisterAttached(
				"CheckBoxUnchecked"
				, typeof(ICommand)
				, typeof(CheckBoxCommands)
				, new PropertyMetadata(OnSetCommandCallbackUnchecked));


		private static void OnSetCommandCallbackUnchecked(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
		{
			CheckBox element = dependencyObject as CheckBox;
			if (element == null)
				throw new ArgumentException("CheckBoxUnchecked property can be applied only for CheckBox control");

			CheckBoxUncheckedBehavior behavior = element.GetValue(CheckBoxUncheckedCommandBehaviorProperty) as CheckBoxUncheckedBehavior;
			if (behavior == null)
			{
				behavior = new CheckBoxUncheckedBehavior(element);
				behavior.Command = e.NewValue as ICommand;
			}

		}

		public static CheckBoxUncheckedBehavior GetCheckBoxUncheckedCommandBehavior(DependencyObject obj)
		{
			return (CheckBoxUncheckedBehavior)obj.GetValue(CheckBoxUncheckedCommandBehaviorProperty);
		}

		public static void SetCheckBoxUncheckedCommandBehavior(DependencyObject obj, CheckBoxUncheckedBehavior value)
		{
			obj.SetValue(CheckBoxUncheckedCommandBehaviorProperty, value);
		}

		public static readonly DependencyProperty CheckBoxUncheckedCommandBehaviorProperty =
			DependencyProperty.RegisterAttached("CheckBoxUncheckedCommandBehavior",
			typeof(CheckBoxUncheckedBehavior), typeof(CheckBoxCommands), null);



	}
}
