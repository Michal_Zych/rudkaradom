﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.Behaviors;
using FinderFX.SL.Core;

namespace Finder.BloodDonation.Commands
{
    public class MouseLeftButtonDownCommand
    {
        private static readonly DependencyProperty MouseLeftButtonDownCommandProperty = DependencyProperty.RegisterAttached(
            "MouseLeftButtonDownCommand",
            typeof(MouseLeftButtonDownCommandBehavior),
            typeof(MouseLeftButtonDownCommand),
            null);

        public static readonly DependencyProperty CommandProperty = DependencyProperty.RegisterAttached(
            "Command",
            typeof(ICommand),
            typeof(MouseLeftButtonDownCommand),
            new PropertyMetadata(OnSetCommandCallback));

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "Only works for selector")]
        public static void SetCommand(FrameworkElement selector, ICommand command)
        {
            selector.SetValue(CommandProperty, command);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Justification = "Only works for selector")]
        public static ICommand GetCommand(FrameworkElement selector)
        {
            return selector.GetValue(CommandProperty) as ICommand;
        }

        private static void OnSetCommandCallback(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement dataForm = dependencyObject as FrameworkElement;
            if (dataForm == null)
            {
                throw new ArgumentException("MouseLeftButtonDownCommand property can be applied only for FrameworkElement");
            }

            MouseLeftButtonDownCommandBehavior behavior = dataForm.GetValue(MouseLeftButtonDownCommandProperty) as MouseLeftButtonDownCommandBehavior;
            if (behavior == null)
            {
                behavior = new MouseLeftButtonDownCommandBehavior(dataForm);
                behavior.Command = e.NewValue as ICommand;
            }
        }

        private static MouseLeftButtonDownCommandBehavior GetOrCreateBehavior(FrameworkElement selector)
        {
            var behavior = selector.GetValue(MouseLeftButtonDownCommandProperty) as MouseLeftButtonDownCommandBehavior;
            if (behavior == null)
            {
                behavior = new MouseLeftButtonDownCommandBehavior(selector);
                selector.SetValue(MouseLeftButtonDownCommandProperty, behavior);
            }

            return behavior;
        }
    }

    public class MouseLeftButtonDownCommandBehavior : CommandBehaviorBase<FrameworkElement>
    {
        public MouseLeftButtonDownCommandBehavior(FrameworkElement element)
            : base(element)
        {
            element.MouseLeftButtonDown += new MouseButtonEventHandler(element_MouseLeftButtonDown);
        }

        void element_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            base.CommandParameter = sender;
            base.ExecuteCommand();
        }
    }
}
