﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core;

namespace Finder.BloodDonation.Commands
{
	public class CheckBoxCheckedBehavior : CommandBehaviorBase<CheckBox>
	{
		public CheckBoxCheckedBehavior(CheckBox element)
			: base(element)
		{
			element.Checked += new RoutedEventHandler(element_Checked);
		}

		void element_Checked(object sender, RoutedEventArgs e)
		{
			base.CommandParameter = ((CheckBox)sender).IsChecked;
			base.ExecuteCommand();
		}

	

	}

	public static partial class CheckBoxCommands
	{
		public static string GetCheckBoxChecked(DependencyObject obj)
		{
			return (string)obj.GetValue(CheckBoxCheckedProperty);
		}

		public static void SetCheckBoxChecked(DependencyObject obj, string value)
		{
			obj.SetValue(CheckBoxCheckedProperty, value);
		}

		public static readonly DependencyProperty CheckBoxCheckedProperty =
			DependencyProperty.RegisterAttached(
				"CheckBoxChecked"
				, typeof(ICommand)
				, typeof(CheckBoxCommands)
				, new PropertyMetadata(OnSetCommandCallbackChecked));


		private static void OnSetCommandCallbackChecked(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
		{
			CheckBox element = dependencyObject as CheckBox;
			if (element == null)
				throw new ArgumentException("CheckBoxChecked property can be applied only for CheckBox control");

			CheckBoxCheckedBehavior behavior = element.GetValue(CheckBoxCheckedCommandBehaviorProperty) as CheckBoxCheckedBehavior;
			if (behavior == null)
			{
				behavior = new CheckBoxCheckedBehavior(element);
				behavior.Command = e.NewValue as ICommand;
			}

		}

		public static CheckBoxCheckedBehavior GetCheckBoxCheckedCommandBehavior(DependencyObject obj)
		{
			return (CheckBoxCheckedBehavior)obj.GetValue(CheckBoxCheckedCommandBehaviorProperty);
		}

		public static void SetCheckBoxCheckedCommandBehavior(DependencyObject obj, CheckBoxCheckedBehavior value)
		{
			obj.SetValue(CheckBoxCheckedCommandBehaviorProperty, value);
		}

		public static readonly DependencyProperty CheckBoxCheckedCommandBehaviorProperty =
			DependencyProperty.RegisterAttached("CheckBoxCheckedCommandBehavior",
			typeof(CheckBoxCheckedBehavior), typeof(CheckBoxCommands), null);



	}
}
