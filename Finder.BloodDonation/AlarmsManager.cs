﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Model.Dto;
using System.Collections.Generic;
using Finder.BloodDonation.ModelServices;
using FinderFX.SL.Core.Communication;
using System.Windows.Media.Animation;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Authentication;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Authentication;

namespace Finder.BloodDonation
{
    public class AlarmsManager : IAlarmsManager
    {
        IUnityContainer Container;
        IDynamicEventAggregator EventAggregator;

        public List<AlarmUnitModel> AvailableUnits { get; set; }

        public List<AlarmDto> Alarms { get; set; }

        private DateTime LastPing = DateTime.Now;

        private Storyboard ping_story = null;

        private AlarmsServiceClient LoadService
        {
            get
            {
                AlarmsServiceClient proxy = ServiceFactory.GetService<AlarmsServiceClient>(ServicesUri.AlarmsService);
                proxy.PingAlarmsCompleted += new EventHandler<PingAlarmsCompletedEventArgs>(proxy_PingAlarmsCompleted);
                return proxy;
            }
        }

        public AlarmsManager(IUnityContainer container)
        {
            Container = container;
            EventAggregator = container.Resolve<IDynamicEventAggregator>();

            EventAggregator.GetEvent<AlarmsNewReceived>().Subscribe(NewAlarms_Event);
            EventAggregator.GetEvent<AlarmsCloseAlarm>().Subscribe(CloseAlarm_Event);

            UnitsServiceClient proxy = ServiceFactory.GetService<UnitsServiceClient>("Finder-BloodDonation-Model-Services-UnitsService.svc");
            proxy.GetRootUnitCompleted += new EventHandler<GetRootUnitCompletedEventArgs>(proxy_GetRootUnitCompleted);
            proxy.GetRootUnitAsync();
        }

        void ping_story_Completed(object sender, EventArgs e)
        {
            LoadService.PingAlarmsAsync(LastPing);
            ping_story.Begin();
        }

        void proxy_PingAlarmsCompleted(object sender, PingAlarmsCompletedEventArgs e)
        {
            LastPing = DateTime.Now;
            Alarms = e.Result;
            if (e.Result != null)
            {
                for (int i = 0; i < AvailableUnits.Count; i++)
                {
                    AvailableUnits[i].SetAlarm((from a in Alarms select a.DeviceUnitID).ToArray());
                }
            }

            EventAggregator.GetEvent<AlarmsNewReceived>().Publish(e.Result);
        }

        public void CloseAlarm_Event(AlarmDto o)
        {
            LoadService.PingAlarmsAsync(LastPing);
            ping_story.Begin();
        }

        void proxy_GetRootUnitCompleted(object sender, GetRootUnitCompletedEventArgs e)
        {
            AvailableUnits = new List<AlarmUnitModel>();
            AvailableUnits.Add(ConvertUnit(e.Result));

            BloodyUser bu = User.Current as BloodyUser;
            if (bu.HasPermission(1, PermissionNames.AlarmListing))
            {
                ping_story = new Storyboard()
                {
                    Duration = new System.Windows.Duration(new TimeSpan(0, 0, 30))
                };
                ping_story.Completed += new EventHandler(ping_story_Completed);
                ping_story.Begin();
            }
        }

        private AlarmUnitModel ConvertUnit(UnitDTO unit)
        {
            AlarmUnitModel vm = this.Container.Resolve<AlarmUnitModel>();
            vm.SetUnit(unit);
            return vm;
        }

        public void NewAlarms_Event(List<AlarmDto> a)
        {
            for (int i = 0; i < AvailableUnits.Count; i++)
            {
                AvailableUnits[i].SetAlarm((from u in a select u.DeviceUnitID).ToArray());
            }
        }

        public bool IsUnitAlarm(int unit)
        {
            for (int i = 0; i < AvailableUnits.Count; i++)
            {
                if (AvailableUnits[i].IsUnitAlarm(unit))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
