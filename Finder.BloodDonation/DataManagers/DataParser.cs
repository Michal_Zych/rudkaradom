﻿using Finder.BloodDonation.UsersService;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.Dialogs.Patients;


namespace Finder.BloodDonation.DataManagers
{
    public class DataParser
    {
        public static string ConvetDataToString(List<SettingItem> settingItems)
        {
            List<SettingItem> items = settingItems;
            StringBuilder stringBuilder = new StringBuilder();


            for (int i = 0; i < items.Count(); i++)
            {
                if (items[i].Type.Equals("System.String"))
                    stringBuilder.Append(items[i].Name).Append("=").Append(Convert.ToString(items[i].Value).ToVarchar()).Append(",");
                else if (items[i].Type.Equals("System.Int32") && !items[i].Name.Equals("Id") && !items[i].Name.Equals("UserId"))
                    stringBuilder.Append(items[i].Name).Append("=").Append(Convert.ToInt32(items[i].Value).ToVarchar()).Append(",");
                else if (items[i].Type.Equals("System.Int64"))
                    stringBuilder.Append(items[i].Name).Append("=").Append(Convert.ToInt64(items[i].Value).ToVarchar()).Append(",");
                else if (items[i].Type.Equals("System.Int16"))
                    stringBuilder.Append(items[i].Name).Append("=").Append(Convert.ToInt16(items[i].Value).ToVarchar()).Append(",");
                else if (items[i].Type.Equals("System.Double"))
                    stringBuilder.Append(items[i].Name).Append("=").Append(Convert.ToDouble(items[i].Value).ToVarchar()).Append(",");
                else if (items[i].Type.Equals("System.Boolean"))
                    stringBuilder.Append(items[i].Name).Append("=").Append(Convert.ToBoolean(items[i].Value).ToVarchar()).Append(",");
                else if (items[i].Type.Equals("System.Single"))
                    stringBuilder.Append(items[i].Name).Append("=").Append(Convert.ToSingle(items[i].Value).ToVarchar()).Append(",");
                else if (items[i].Type.Equals("System.DateTime") && !items[i].Name.Equals("StartDateUtc"))
                    stringBuilder.Append(items[i].Name).Append("=").Append(Convert.ToDateTime(items[i].Value).ToVarchar(true, false)).Append(",");
            }

           stringBuilder.Remove(stringBuilder.Length - 1, 1);

            string output = stringBuilder.ToString();

            return output;
        }
    }
}
