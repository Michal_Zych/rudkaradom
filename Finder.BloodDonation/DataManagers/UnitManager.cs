﻿using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.DataManagers
{
    public class UnitManager
    {
        public static UnitManager Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new UnitManager();
                }
                return instance;
            }
        }

        public UnitDTO RootUnit
        {
            get { return rootUnit; }
        }

        public IEnumerable<UnitDTO> Units
        {
            get
            {
                return units.Values;
            }
        }

        private static UnitManager instance;
        private UnitsServiceClient proxy;
        private UnitDTO rootUnit;
        private IDictionary<int, UnitDTO> units;
        private IDictionary<int, int> unitTransmitter;
        private IList<ZonesInfoDto> zonesInfo;

        private UnitManager()
        {
            proxy = ServiceFactory.GetService<UnitsServiceClient>("Finder-BloodDonation-Model-Services-UnitsService.svc");
            proxy.GetRootUnitCompleted += new EventHandler<GetRootUnitCompletedEventArgs>(proxy_GetRootUnitCompleted);
            proxy.GetUnitTransmittersCompleted += new EventHandler<GetUnitTransmittersCompletedEventArgs>(proxy_GetUnitTransmittersCompleted);
            //proxy.GetUnitsWithTransmitterCompleted += new EventHandler<GetUnitsWithTransmitterCompletedEventArgs>(proxy_GetUnitsWithTransmitterCompleted);
            proxy.ZonesGetCompleted += proxy_ZonesGetCompleted;  
        }

        void proxy_ZonesGetCompleted(object sender, ZonesGetCompletedEventArgs e)
        {
            if (e.Result != null)
                zonesInfo = e.Result;
        }

        /*private void proxy_GetUnitsWithTransmitterCompleted(object sender, GetUnitsWithTransmitterCompletedEventArgs e)
        {
            if (e.Result != null)
                unitTransmitters = e.Result;
        }*/

        public void ReloadRoot()
        {
            ReloadRoot(null);
        }

        public void ReloadRoot(Action action)
        {
            proxy.GetUnitTransmittersAsync(action);
        }

        private void proxy_GetUnitTransmittersCompleted(object sender, GetUnitTransmittersCompletedEventArgs e)
        {
            unitTransmitter = e.Result;
            proxy.GetRootUnitAsync(e.UserState);
        }       

        private void proxy_GetRootUnitCompleted(object sender, GetRootUnitCompletedEventArgs e)
        {
            var r = e.Result;
            r.SetParrentsForChildren();
            rootUnit = r;
            
            units = new Dictionary<int, UnitDTO>();
            units[rootUnit.ID] = rootUnit;
            rootUnit.ForEachDescendant(unit => units[unit.ID] = unit);

            
            if ((Action)e.UserState != null)
            {
                Action callback = (Action)e.UserState;
                callback();
            }

            proxy.ZonesGetAsync();
        }

        public bool CheckIfUnitIsMonitored(int unitId)
        {
            if (zonesInfo != null)
            {
                for (int i = 0; i < zonesInfo.Count; i++)
                {
                    if (unitId == zonesInfo[i].UnitId)
                        return true;
                }

                return false;
            }

            return false;
        }

        private const int HOSPITAL_DEPARTMENT = 29;

        public UnitDTO SearchDepartment(UnitDTO unit)
        {
            UnitDTO output = null;

            UnitDTO _temp = unit.Parent;

            while (_temp != null && _temp.UnitTypeIDv2 != HOSPITAL_DEPARTMENT)
            {
                _temp = _temp.Parent;
            }

            output = _temp;

            return output;
        }
        
    }
}
