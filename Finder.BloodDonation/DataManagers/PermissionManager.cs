﻿
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.DataManagers
{
    public static class PermissionManager
    {
        /// <summary>
        /// This method checked if current user has authenticate to section.
        /// </summary>
        /// <param name="section">Section to full access.</param>
        /// <returns>True if user has access.</returns>
        public static bool CheckIfCurrentUserHasAuthentication(PermissionSectionType section)
        {
            return true;
        }
    }

    public enum PermissionSectionType
    {
        Patient = 0
    }
}
