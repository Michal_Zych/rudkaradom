﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.IO;
using System.Windows.Media.Imaging;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Tools;
using Microsoft.Practices.Composite.Modularity;
using Telerik.Windows.DragDrop;
using Telerik.Windows.Media.Imaging;
using Path = System.IO.Path;

namespace Finder.BloodDonation.DBBuffers
{
    public enum BitmapMode { Normal=8, Red=1, Gray=3, White=7, Yellow=2, Cyan=4, Green=5, LightGreen=6}

    public class BitmapBuffer
    {
        public static IDictionary<BitmapMode, double[]> Intense = new Dictionary<BitmapMode, double[]>()
        {
            {BitmapMode.Normal, new double[]{1, 1, 1}},
            {BitmapMode.Red, new double[]{1, .2, .2}},
            {BitmapMode.Gray, new double[]{0,0,0}},
            {BitmapMode.White, new double[]{.2, .2, 1}},
            {BitmapMode.Yellow, new double[]{1, 1, .2}},
            {BitmapMode.Cyan, new double[]{.2, 1, 1}},
            {BitmapMode.Green, new double[]{.5, 1, .5}},
            {BitmapMode.LightGreen, new double[]{.6, .8, .6}}
        };

        private static IDictionary<string, Image> imagesBuffer;
        private static IDictionary<string, ImageBrush> buffer;
        private static string[] ImagesFolders = { "Objects/", "UnitIcons/StatusIcon/", "UnitIcons/", "OthersIcon/" };
        private static string ImageExtension = ".png";
        
        public static Image GetIcon(string name, ImagesFolder ImagesFloder)
        {
            if (String.IsNullOrEmpty(name))
                return null;

            return GetImage(ImagesFolders[(int) ImagesFloder] + name);
        }

        public static Image GetImage(string imagesSource)
        {
            Image _temp = new Image();
            _temp.Source = new BitmapImage(new Uri(GetFileLocation(imagesSource), UriKind.RelativeOrAbsolute));

            return _temp;
        }

        /*
        public static Image GetImageFromResorces(string name)
        {
            if (imagesBuffer == null || imagesBuffer.Count <= 0)
                return new Image();

            Image img = new Image();
            img.Source = imagesBuffer[name + ImageExtension].Source;
            return img;
        }
        */

        public static ImageBrush Get(string name, BitmapMode mode = BitmapMode.Normal)
        {
            if (String.IsNullOrEmpty(name))
            {
                return null;
            }

            return GetImageBrush(ImagesFolders[0] + name, mode);
        }

        private static ImageBrush GetImageBrush(string name, BitmapMode mode)
        {
            if (buffer == null)
            {
                buffer = new Dictionary<string, ImageBrush>();
            }

            var key = GetKey(name, mode);

            if (buffer.ContainsKey(key))
            {
                return buffer[key];
            }
            else
            {
                var brush = new ImageBrush()
                {
                    ImageSource = new BitmapImage(new Uri(GetFileLocation(name), UriKind.RelativeOrAbsolute)),
                    Stretch = Stretch.Uniform
                };
                brush.ImageOpened += ImageOpened;
                buffer[key] = brush;
            }
            return buffer[key];
        }

        public static string GetFileLocation(string name, ImagesFolder imagesFolder)
        {
            return GetFileLocation(ImagesFolders[(int) imagesFolder] + name);
        }

        private static string GetFileLocation(string name)
        {
            if (!name.Contains("."))
            {
                name = name + ImageExtension;
            }
            return "../../Images/" + name;
        }

        private static Image ConvertImageBrushToImage(ImageBrush brushToConvert)
        {
            BitmapSource src = (BitmapSource)brushToConvert.ImageSource;
            string path = "../../Images/" + ImagesFolders[(int) ImagesFolder.Objects];

            Image img = new Image();
            img.Source = src;

            if (img.Source != null)
            {
                return img;
            }
            else
            {
                return null;
            }
        }

        
        private static string GetKey(string name, BitmapMode bitmapMode)
        {
            return name.ToUpper() + ":" + ((int)bitmapMode).ToString();
        }

        private static string[] bitmapNames = { "ludzik", "kula" };

        public static void Initialize()
        {
            if (buffer != null) return;
            buffer = new Dictionary<string, ImageBrush>();

            foreach (var n in bitmapNames)
            {
                var name = ImagesFolders[0] + n;
                var key = GetKey(name, BitmapMode.Normal);
                var brush = new ImageBrush()
                {
                    ImageSource = new BitmapImage(new Uri(GetFileLocation(name), UriKind.RelativeOrAbsolute)),
                    Stretch = Stretch.Uniform
                };
                brush.ImageOpened += ImageOpened;
                buffer[key] = brush;
            }
        }

        private static void ImageOpened(object sender, RoutedEventArgs e)
        {
            ImageBrush sourceImage = sender as ImageBrush;
            var name = (sourceImage.ImageSource as BitmapImage).UriSource.ToString();
            name = name.Substring(name.IndexOf(ImagesFolders[(int)ImagesFolder.Objects]), name.IndexOf(ImageExtension) - name.IndexOf(ImagesFolders[(int)ImagesFolder.Objects]));

            sourceImage.ImageOpened -= ImageOpened;
            BitmapImage bi = sourceImage.ImageSource as BitmapImage;

            foreach (BitmapMode mode in Enum.GetValues(typeof(BitmapMode)))
            {
                if (mode == BitmapMode.Normal) continue;
                var key = GetKey(name, mode);

                var brush = new ImageBrush();
                if (mode == BitmapMode.Gray)
                {
                    brush.ImageSource = bi.MakeGrayScale();
                }
                else
                {
                    brush.ImageSource = bi.MakeColorsIntense(Intense[mode]);
                }

                buffer[key] = brush;
            }
        }
    }

    public enum ImagesFolder
    {
        Objects = 0,
        UnitIconsStatus = 1,
        UnitIconsUnit = 2,
        OthersIcon = 3
    }
}

