﻿using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.UsersService;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.DBBuffers
{
    public static class UsersContactInforBuffer
    {
        private static IUnityContainer _container = null;
        private static IDictionary<int, UserContactInfoDto> _data;

        public static void SetContainer(IUnityContainer container)
        {
            _container = container;
        }

        private const string NO_DATA = "brak danych";
        public static UserContactInfoDto GetItem(int userId)
        {
            UserContactInfoDto result = null;
            if(_data != null)
            {
                if(_data.ContainsKey(userId))
                    result = _data[userId];
            }

            if (result == null)
            {
                result = new UserContactInfoDto{Id = userId, LastName=NO_DATA, Name=NO_DATA, Email=NO_DATA, Phone=NO_DATA};

            }
            return result;
        }

        public static IList<UserContactInfoDto> GetAllItems()
        {
            if (_data == null || _data.Count == 0)
                return null;


            var list = new List<UserContactInfoDto>();
            foreach (var item in _data.Values)
            {
                list.Add(item);
            }

            return list;
        }


        private const int CLIENTID = -1;

        public static void Reload(int unitId = CLIENTID)
        {
            var proxy = ServiceFactory.GetService<UsersServiceClient>(ServicesUri.UsersService);
            proxy.GetUsersContactsForUnitCompleted += new EventHandler<GetUsersContactsForUnitCompletedEventArgs>(proxy_GetUsersContactsForUnitCompleted);
            proxy.GetUsersContactsForUnitAsync(unitId);
        }

        static void proxy_GetUsersContactsForUnitCompleted(object sender, GetUsersContactsForUnitCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                var dic = new Dictionary<int, UserContactInfoDto>();
                foreach (var item in e.Result)
                {
                    dic.Add(item.Id, item);
                }

                _data = dic;
                if (_container != null)
                {
                    _container.Resolve<IDynamicEventAggregator>().GetEvent<UsersContactInforBufferRefreshedEvent>().Publish(e.Result.Count);

                }
            }
        }


    }
}
