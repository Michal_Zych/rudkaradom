﻿using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.PermissionsProxy;
using Finder.BloodDonation.Tools;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Debugs
{
    public class DebugHelpher
    {
        [RaisePropertyChanged]
        public bool IsProjectInDebugMode
        {
            get
            {
                #if(DEBUG)
                    return true;
                #else
                    return false;
                #endif
            }
        }
        [RaisePropertyChanged]
        public List<string> DebugModes { get; set; }
        [RaisePropertyChanged]
        public int SelectedMode { get; set; }

        public RelayCommand SelectedModeChanged { get; set; }

        public DebugHelpher(List<string> debugModes)
        {
            #if (DEBUG)
                DebugModes = debugModes;

                if (DebugModes != null)
                {
                    if (DebugModes.Count > 0)
                    {
                        SelectedMode = 0;
                        SelectedModeChanged = new RelayCommand(OnSelectedModeChanged);
                    }
                }
                else
                {
                    MsgBox.Warning("Tryby pracy okienka nie mogą być puste.");
                }
            #endif
        }

        public void OnSelectedModeChanged()
        {
            /// TODO: Implementacja dla zmiany trybu pracy okienka.
        }
    }
}
