﻿using Finder.BloodDonation.DBBuffers;
using Finder.BloodDonation.Model.Dto.Enums;
using Finder.BloodDonation.Tabs.UnitsList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Finder.BloodDonation.Controls
{
    [DataContract]
    public class Alarm
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public AlarmType Type { get; set; }

        [DataMember]
        public int ObjectId { get; set; }

        [DataMember]
        public ObjectType ObjectType { get; set; }

        [DataMember]
        public string Info { get; set; }

        [DataMember]
        public DateTime StartTime { get; set; }

        [DataMember]
        public BitmapMode DisplayColor { get; set; }

        [DataMember]
        public EventTypes EventTypes { get; set; }
    }
}
