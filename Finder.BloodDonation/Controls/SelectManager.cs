﻿using Finder.BloodDonation.Tabs.UnitsList;
using Finder.BloodDonation.Tools;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Linq;

namespace Finder.BloodDonation.Controls
{
    public class SelectManager
    {
        Image image; 
        public List<IObjectable> SelectedObject { get; set; }
        public List<IObjectable> MonitorableObject { get; set; }

        public SelectManager(List<IObjectable> monitorableObject, Image image)
        {
            this.MonitorableObject = monitorableObject;
            this.image = image;
            this.SelectedObject = new List<IObjectable>();
        }

        public void Click(Point clickedPosition, bool clearSelected=true)
        {
            if (MonitorableObject == null)
                return;
            if (MonitorableObject.Count < 1)
                return;

            if(clearSelected)
                SelectedObject = new List<IObjectable>();

            for(int i = 0; i < MonitorableObject.Count; i++)
            {
                if ((MonitorableObject[i].MonitorableObject.PositionInViewportX < clickedPosition.X &&
                    MonitorableObject[i].MonitorableObject.PositionInViewportX + MonitorableObject[i].MonitorableObject.Width > clickedPosition.X) &&
                    (MonitorableObject[i].MonitorableObject.PositionInViewportY < clickedPosition.Y &&
                    MonitorableObject[i].MonitorableObject.PositionInViewportY + MonitorableObject[i].MonitorableObject.Height > clickedPosition.Y)
                    )
                {
                    image.Width = MonitorableObject[i].MonitorableObject.Width;
                    image.Height = MonitorableObject[i].MonitorableObject.Height;

                    if (!HitTestIfTransparency(new Point() { X = clickedPosition.X - MonitorableObject[i].MonitorableObject.PositionInViewportX, Y = clickedPosition.Y - MonitorableObject[i].MonitorableObject.PositionInViewportY }))
                    {
                        SelectedObject.Add(MonitorableObject[i]);
                    }
                }
            }
        }

        private bool HitTestIfTransparency(Point ClickedPositionOnImage)
        {
            var w = new WriteableBitmap(image, null);
            
            try
            {
                if (w.Pixels[w.PixelWidth * (int)ClickedPositionOnImage.Y + (int)ClickedPositionOnImage.X] != 0)
                {
                    return false; // nie transparent.
                }
                else
                {
                    return true; // transparent.
                }
            }
            catch { return true; }
        }
    }
}
