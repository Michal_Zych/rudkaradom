﻿using Finder.BloodDonation.DBBuffers;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Finder.BloodDonation.Controls
{
    public class StatusImage
    {
        public Image Image { get; set; }
        public string FileName { get; set; }
        public BitmapMode DisplayColor { get; set; }

        public ActionType ActionType { get; set; }

        private bool isActiveAction = false;

        public bool IsActiveAction
        {
            get { return isActiveAction; }
            set
            {
                if (value && Image.Visibility != Visibility.Collapsed)
                {
                    InitializeAction();
                }
                else
                {
                    try
                    {
                        dispatcherTimer.Stop();
                        dispatcherTimer = null;
                    }
                    catch (Exception e)
                    {
                        
                    }
                    
                }

                isActiveAction = value;
            }
        }

        private DispatcherTimer dispatcherTimer;

        public StatusImage()
        {
            
        }


        private void InitializeAction()
        {
            dispatcherTimer = new DispatcherTimer();

            if(ActionType.Equals(ActionType.Flashes))
                dispatcherTimer.Interval = new TimeSpan(0,0,0,1,0);
            else if(ActionType.Equals(ActionType.Spines))
                dispatcherTimer.Interval = new TimeSpan(0,0,0,0,50);

            dispatcherTimer.Tick += dispatcherTimer_Tick;
            dispatcherTimer.Start();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (ActionType.Equals(ActionType.Flashes))
            {
                Image.Visibility = Image.Visibility.Equals(Visibility.Collapsed) ? Visibility.Visible : Visibility.Collapsed;
            }
        }
    }
}
