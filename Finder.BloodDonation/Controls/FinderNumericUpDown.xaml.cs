﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Finder.BloodDonation.Controls
{
    public partial class FinderNumericUpDown : UserControl
    {
        public FinderNumericUpDown()
        {
            InitializeComponent();
            //RightButton.GotFocus += RightButton_GotFocus;
            //LeftButton.GotFocus += LeftButton_GotFocus;
        }

        /*void LeftButton_GotFocus(object sender, RoutedEventArgs e)
        {
            Value = CheckValueChangedDirection(isUp) ? Increment() : Decrement();
        }

        void RightButton_GotFocus(object sender, RoutedEventArgs e)
        {
            Value = CheckValueChangedDirection(isUp) ? Increment() : Decrement();
        }*/

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(Int32), typeof(FinderNumericUpDown), new PropertyMetadata(OnValueChanged));

        private static void OnValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetValue = (FinderNumericUpDown) d;
            var oldValue = (int) e.OldValue;

            var newValue = targetValue.Value;
            targetValue.OnValueChanged(oldValue, newValue);
        }
        
        protected virtual void OnValueChanged(int oldValue, int newValue)
        {
            if (IsSendWarningIfControlIsAlarming)
            {
                if (newValue <= MaxValue && newValue > MinValue)
                    NumericTextBox.Text = newValue.ToString();
                else if (newValue > MaxValue && newValue > MinValue)
                    Value = oldValue;
                else
                    Value = MinValue + 1;
            }
            else
            {
                if (newValue <= MaxValue && newValue >= 0)
                    NumericTextBox.Text = newValue.ToString();
                else
                {
                    Value = oldValue;
                }
            }
        } 

        public Int32 Value
        {
            get { return Convert.ToInt32(GetValue(ValueProperty)); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty UnitProperty =
            DependencyProperty.Register("UnitToDisplay", typeof(String), typeof(FinderNumericUpDown), new PropertyMetadata(String.Empty, OnUnitChanged));

        private static void OnUnitChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetUnit = (FinderNumericUpDown)d;
            var oldUnit = e.OldValue.ToString();

            var newUnit = targetUnit.UnitToDisplay;
            targetUnit.OnUnitChanged(oldUnit, newUnit);
        }

        protected virtual void OnUnitChanged(string oldUnit, string newUnit)
        {
            UnitTextBlock.Text = newUnit;
        } 

        public String UnitToDisplay
        {
            get { return GetValue(UnitProperty).ToString(); }
            set { SetValue(UnitProperty, value); }
        }
        
        public static readonly DependencyProperty ChangeValueSizeProperty =
            DependencyProperty.Register("ChangeValueSize", typeof(Int32), typeof(FinderNumericUpDown), new PropertyMetadata(1));

        public Int32 ChangeValueSize
        {
            get { return Convert.ToInt32(GetValue(ChangeValueSizeProperty)); }
            set { SetValue(ChangeValueSizeProperty, value); }
        }

        public static readonly DependencyProperty MinValueProperty =
            DependencyProperty.Register("MinValue", typeof(Int32), typeof(FinderNumericUpDown), new PropertyMetadata(null));

        public Int32 MinValue
        {
            get { return Convert.ToInt32(GetValue(MinValueProperty)); }
            set { SetValue(MinValueProperty, value); }
        }

        public static readonly DependencyProperty MaxValueProperty =
            DependencyProperty.Register("MaxValue", typeof(Int32), typeof(FinderNumericUpDown), new PropertyMetadata(null));

        public Int32 MaxValue
        {
            get { return Convert.ToInt32(GetValue(MaxValueProperty)); }
            set { SetValue(MaxValueProperty, value); }
        }

        public static readonly DependencyProperty SelectedAllTextIfGotFocusProperty =
            DependencyProperty.Register("SelectedAllTextIfGotFocus", typeof(Boolean), typeof(FinderNumericUpDown), new PropertyMetadata(true));

        public Boolean SelectedAllTextIfGotFocus
        {
            get { return Convert.ToBoolean(GetValue(SelectedAllTextIfGotFocusProperty)); }
            set { SetValue(SelectedAllTextIfGotFocusProperty, value); }
        }

        public static readonly DependencyProperty IsSendWarningIfControlIsAlarmingProperty =
            DependencyProperty.Register("IsSendWarningIfControlIsAlarming", typeof(Boolean),
                typeof(FinderNumericUpDown), new PropertyMetadata(false, OnIsSendWarningIfControlIsAlarmingChanged));

        private static void OnIsSendWarningIfControlIsAlarmingChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetUnit = (FinderNumericUpDown)d;
            var oldUnit = (Boolean)e.OldValue;

            var newUnit = targetUnit.IsSendWarningIfControlIsAlarming;
            targetUnit.OnIsSendWarningIfControlIsAlarmingChanged(oldUnit, newUnit);
        }

        protected virtual void OnIsSendWarningIfControlIsAlarmingChanged(bool oldValue, bool newValue)
        {
            if (Value < MinValue && IsSendWarningIfControlIsAlarming)
            {
                Value = MinValue + 1;
            }
        } 

        public Boolean IsSendWarningIfControlIsAlarming
        {
            get { return Convert.ToBoolean(GetValue(IsSendWarningIfControlIsAlarmingProperty)); }
            set { SetValue(IsSendWarningIfControlIsAlarmingProperty, value);}
        }

        private DispatcherTimer dt;
        public TextBox controlTextBox;
        public bool isUp = false;

        private void InitializeDispatcherTimer()
        {
            dt = new DispatcherTimer();
            dt.Interval = new TimeSpan(0, 0, 0, 0, 100);
            dt.Tick += dt_Tick;
            dt.Start();
        }

        private int Decrement()
        {
            return Value - ChangeValueSize;
        }

        private int Increment()
        {
            return Value + ChangeValueSize;
        }

        private bool CheckValueChangedDirection(bool isUped)
        {
            if (!isUped)
                return true;
            else
                return false;
        }

        int timeSpan = 0;

        private void dt_Tick(object sender, EventArgs e)
        {
            timeSpan++;

            if (timeSpan > 3)
            {
                Value = CheckValueChangedDirection(isUp)
                    ? Decrement()
                    : Increment();
            }
        }

        private void LeftButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (dt != null)
            {
                dt.Stop();
                dt = null;
            }

            isUp = false;
            InitializeDispatcherTimer();
        }

        private void LeftButton_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //Value = CheckValueChangedDirection(isUp) ? Decrement() : Increment();

            try
            {
                dt.Stop();
            }
            catch (Exception exception)
            {

            }

            timeSpan = 0;
            dt = null;
        }

        private void LeftButton_Loaded(object sender, RoutedEventArgs e)
        {
            LeftButton.AddHandler(FrameworkElement.MouseLeftButtonDownEvent, new MouseButtonEventHandler(LeftButton_MouseLeftButtonDown), true);
            LeftButton.AddHandler(FrameworkElement.MouseLeftButtonUpEvent, new MouseButtonEventHandler(LeftButton_MouseLeftButtonUp), true);

        }

        private void NumericTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            const string expression = "[^0-9]";

            if (System.Text.RegularExpressions.Regex.IsMatch(NumericTextBox.Text, expression))
                NumericTextBox.Text = Value.ToString();
            else
                Value = int.Parse(NumericTextBox.Text);

        }

        private void RightButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (dt != null)
            {
                dt.Stop();
                dt = null;
            }

            isUp = true;
            InitializeDispatcherTimer();
        }

        private void RightButton_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                dt.Stop();
            }
            catch (Exception exception)
            {
                
            }

            timeSpan = 0;
            dt = null;
        }

        private void RightButton_Loaded(object sender, RoutedEventArgs e)
        {
            RightButton.AddHandler(FrameworkElement.MouseLeftButtonDownEvent, new MouseButtonEventHandler(RightButton_MouseLeftButtonDown), true);
            RightButton.AddHandler(FrameworkElement.MouseLeftButtonUpEvent, new MouseButtonEventHandler(RightButton_MouseLeftButtonUp), true);
        }

        private void NumericTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (SelectedAllTextIfGotFocus)
            {
                NumericTextBox.SelectAll();
            }
        }

        private void LeftButton_Click(object sender, RoutedEventArgs e)
        {
            Value = Decrement();
        }

        private void RightButton_Click(object sender, RoutedEventArgs e)
        {
            Value = Increment();
        }

        private void NumericTextBox_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (Value <= MinValue && IsSendWarningIfControlIsAlarming)
                Value = MinValue + 1;
        }
    }
}
