﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Controls
{
    public enum ControlStyle
    {
        Normal = 0,
        Dark = 1,
        Blood = 2
    }
}
