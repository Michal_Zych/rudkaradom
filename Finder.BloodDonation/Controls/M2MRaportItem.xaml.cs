﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Controls
{
    public partial class M2MRaportItem : UserControl
    {
        public static readonly DependencyProperty TitleProperty =
    DependencyProperty.Register("Title", typeof(string), typeof(M2MRaportItem),
        new PropertyMetadata(OnTitleChanged));

        private static void OnTitleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetTitle = (M2MRaportItem)d;
            var oldTitle = (string)e.OldValue;

            var newTitle = targetTitle.Title;
            targetTitle.OnTitleChanged(oldTitle, newTitle);
        }

        [System.ComponentModel.Category("Item Settings")]
        public string Title
        {
            get { return (string)(GetValue(TitleProperty)); }
            set { SetValue(TitleProperty, value); }
        }

        protected virtual void OnTitleChanged(string oldTitle, string newTitle)
        {
            //OnStatusImagesChanged(StatusImages, StatusImages);
        }

        public static readonly DependencyProperty DescriptionProperty =
    DependencyProperty.Register("Description", typeof(string), typeof(M2MRaportItem),
        new PropertyMetadata(OnDescriptionChanged));

        private static void OnDescriptionChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetDescription = (M2MRaportItem)d;
            var oldTitle = (string)e.OldValue;

            var newTitle = targetDescription.Description;
            targetDescription.OnDescriptionChanged(oldTitle, newTitle);
        }

        [System.ComponentModel.Category("Item Settings")]
        public string Description
        {
            get { return (string)(GetValue(DescriptionProperty)); }
            set { SetValue(DescriptionProperty, value); }
        }

        protected virtual void OnDescriptionChanged(string oldDescription, string newDescription)
        {
            //OnStatusImagesChanged(StatusImages, StatusImages);
        }

        public static readonly DependencyProperty StateProperty =
    DependencyProperty.Register("State", typeof(State), typeof(M2MRaportItem),
        new PropertyMetadata(OnStateChanged));

        private static void OnStateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetState = (M2MRaportItem)d;
            var oldState = (State)e.OldValue;

            var newState = targetState.State;
            targetState.OnStateChanged(oldState, newState);
        }

        [System.ComponentModel.Category("Item Settings")]
        public State State
        {
            get { return (State)(GetValue(StateProperty)); }
            set { SetValue(DescriptionProperty, value); }
        }

        protected virtual void OnStateChanged(State oldState, State newState)
        {
            SetSelectedColor();
        }

        public static readonly DependencyProperty SelectedColorProperty =
DependencyProperty.Register("State", typeof(SelectedColor), typeof(M2MRaportItem),
new PropertyMetadata(OnSelectedColorChanged));

        private static void OnSelectedColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetSelectedColor = (M2MRaportItem)d;
            var oldSelectedColor = (SelectedColor)e.OldValue;

            var newSelectedColor = targetSelectedColor.SelectedColor;
            targetSelectedColor.OnSelectedColorChanged(oldSelectedColor, newSelectedColor);
        }

        [System.ComponentModel.Category("Item Settings")]
        public SelectedColor SelectedColor
        {
            get { return (SelectedColor)(GetValue(SelectedColorProperty)); }
            set { SetValue(DescriptionProperty, value); }
        }

        protected virtual void OnSelectedColorChanged(SelectedColor oldSelectedColor, SelectedColor newSelectedColor)
        {
            SetSelectedColor();
        }

        private void SetSelectedColor()
        {
            if (State == Controls.State.Selected)
            {
                switch (SelectedColor)
                {
                    case Controls.SelectedColor.LightBlue:
                        LayoutRoot.Background = new SolidColorBrush(Color.FromArgb(100, 196, 220, 255));
                        break;

                    case Controls.SelectedColor.LightGray:
                        LayoutRoot.Background = new SolidColorBrush(Colors.LightGray);
                        break;

                    case Controls.SelectedColor.LightRed:
                        LayoutRoot.Background = new SolidColorBrush(Color.FromArgb(100, 255, 214, 224));
                        break;

                    case Controls.SelectedColor.LightGreen:
                        LayoutRoot.Background = new SolidColorBrush(Color.FromArgb(100, 167, 255, 165));
                        break;
                }
            }
            else
            {
                LayoutRoot.Background = new SolidColorBrush(Colors.Transparent);
            }
        }

        public M2MRaportItem()
        {
            InitializeComponent();
            LayoutRoot.Background = new SolidColorBrush(Colors.Transparent);
        }
    }

    public enum State
    {
        Selected,
        Unselected
    }

    public enum SelectedColor
    {
        LightBlue,
        LightRed,
        LightGreen,
        LightGray
    }
}
