﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Controls
{
	public class ExtendedListBox : ListBox
	{
		public ClickAction ClickOnSelectedItemAction
		{
			get { return (ClickAction)GetValue(ClickOnSelectedItemActionProperty); }
			set { SetValue(ClickOnSelectedItemActionProperty, value); }
		}

		public static readonly DependencyProperty ClickOnSelectedItemActionProperty =
			DependencyProperty.Register("ClickOnSelectedItemAction", typeof(ClickAction), typeof(ExtendedListBox), new PropertyMetadata(ClickAction.OverrideSelection));

		protected override DependencyObject GetContainerForItemOverride()
		{
			//return base.GetContainerForItemOverride();
			ExtendedListBoxItem item = new ExtendedListBoxItem();
			item.ParentListBox = this;
			if (this.ItemContainerStyle != null)
			{
				item.Style = this.ItemContainerStyle;
			}
			return item;
		}
	}

	public class ExtendedListBoxItem : ListBoxItem
	{
		public ExtendedListBox ParentListBox { get; set; }
		protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
		{
			if (this.ParentListBox != null &&
				this.ParentListBox.ClickOnSelectedItemAction == ClickAction.KeepSelectionIfItemSelected &&
				this.IsSelected)
			{
				return;
			}
			base.OnMouseLeftButtonDown(e);
		}
	}

	public enum ClickAction
	{
		KeepSelectionIfItemSelected,
		OverrideSelection
	}
}
