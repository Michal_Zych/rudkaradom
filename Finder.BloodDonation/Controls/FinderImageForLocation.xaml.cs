﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Messaging;
using System.Windows.Shapes;
using Finder.BloodDonation.DBBuffers;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Tabs;
using Finder.BloodDonation.Tabs.UnitsList;

namespace Finder.BloodDonation.Controls
{
    //TODO: Dodać właściwość odpowiedzialną za zmianę stylu.
    public partial class FinderImageForLocation : UserControl
    {
        private const int X = 0;
        private const int Y = 1;

        private const int ON_YOUR_SELF_XY = 0;
        private const int COLUMN_COUNT = 6;
        private const int ROW_COUNT = 6;
        private int[,] CornersPosition = {{0, 0}, {0, 6}, {6, 0}, {6, 6}};
        private int[,] NextToEachOrderPosition = {{6, 6}};

        private static List<ImageInfo> _memory;
        private static List<ImageInfo> _Memory
        {
            get
            {
                if(_memory == null)
                {
                    _memory = new List<ImageInfo>();
                }
                return _memory;
            }
            set
            {
                if(_memory == null)
                {
                    _memory = new List<ImageInfo>();
                }
                _memory = value;
            }
        }

        public static readonly DependencyProperty BaseImageProperty =
            DependencyProperty.Register("BaseImage", typeof(Image), typeof(FinderImageForLocation),
                new PropertyMetadata(OnBaseImageChanged));

        private static void OnBaseImageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetImage = (FinderImageForLocation)d;
            var oldImage = (Image)e.OldValue;

            var newImage = targetImage.BaseImage;
            targetImage.OnBaseImageChanged(oldImage, newImage);
        }

        [System.ComponentModel.Category("Base Image Settings")]
        public Image BaseImage
        {
            get { return (Image)(GetValue(BaseImageProperty)); }
            set { SetValue(BaseImageProperty, value); }
        }

        protected virtual void OnBaseImageChanged(Image oldImage, Image newImage)
        {
            OnStatusImagesChanged(StatusImages, StatusImages);
        }

        public static readonly DependencyProperty IsBaseImagesFromResourcesProperty =
            DependencyProperty.Register("IsBaseImageFromResources", typeof(Boolean), typeof(FinderImageForLocation), 
                new PropertyMetadata(false));

        [System.ComponentModel.Category("Base Image Settings")]
        public bool IsBaseImageFromResources
        {
            get { return (bool) GetValue(IsBaseImagesFromResourcesProperty); }
            set { SetValue(IsBaseImagesFromResourcesProperty, value); }
        }

        public static readonly DependencyProperty BaseImageNameInResourcesProperty =
            DependencyProperty.Register("BaseImageNameInResources", typeof(String), typeof(FinderImageForLocation), 
            new PropertyMetadata(String.Empty, OnBaseImageNameInResourcesChenged));

        private static void OnBaseImageNameInResourcesChenged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetImages = (FinderImageForLocation)d;
            var oldImages = (String)e.OldValue;

            var newImages = targetImages.BaseImageNameInResources;
            targetImages.OnBaseImageNameInResourcesChenged(oldImages, newImages);
        }


        private void LoadImage(string name, BitmapMode displayColor)
        {
            if (IsBaseImageFromResources)
            {
                if (CheckIfImageIsOutOfMemory(name, displayColor))
                {

                    var sourceImage = new ImageBrush()
                    {
                        ImageSource =
                            new BitmapImage(new Uri(BitmapBuffer.GetFileLocation(name, BaseImageFolderInResources),
                                UriKind.RelativeOrAbsolute)),
                        Stretch = Stretch.Uniform
                    };

                    BitmapImage bi = sourceImage.ImageSource as BitmapImage;

                    BaseImage = new Image();
                    BaseImage.Source = bi;
                    BaseImage.Stretch = Stretch.Uniform;

                    bi.ImageOpened += (sender, e) =>
                    {
                        var brush = new ImageBrush();

                        if (displayColor.Equals(BitmapMode.Gray))
                        {
                            brush.ImageSource = bi.MakeGrayScale();
                        }
                        else if (displayColor.Equals(BitmapMode.Normal))
                        {
                            brush.ImageSource = sourceImage.ImageSource;
                        }
                        else
                        {
                            if (displayColor == 0)
                                displayColor = BitmapMode.Normal;

                            brush.ImageSource = bi.MakeColorsIntense(BitmapBuffer.Intense[displayColor]);
                        }

                        BaseImage.Source = brush.ImageSource;
                    };

                    _Memory.Add(new ImageInfo() { Color = DisplayBaseImageColor, Name = name, Image = BaseImage });
                    
                    //if (tempStatus != null)
                    //    OnStatusImagesChanged(tempStatus, tempStatus);
                }
                else
                {
                    BaseImage = new Image();
                    BaseImage.Source = _Memory.FirstOrDefault(x => x.Name != null && x.Name.Equals(name) && x.Color == displayColor).Image.Source;
                    BaseImage.Stretch = Stretch.Uniform;

                    //if (tempStatus != null)
                    //    OnStatusImagesChanged(tempStatus, tempStatus);
                }
            }
        }

        protected void OnBaseImageNameInResourcesChenged(String oldName, String newName)
        {
            LoadImage(newName, DisplayBaseImageColor);                                                      // TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
            OnStatusImagesChanged(StatusImages, StatusImages);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name">Image name from resources.</param>
        /// <param name="color">Image color.</param>
        /// <returns>True if image is out of memory.</returns>
        private bool CheckIfImageIsOutOfMemory(String name, BitmapMode color)
        {
            var res = _Memory.FirstOrDefault(x => x.Name != null && x.Name.Equals(name) && x.Color == color);

            if (res != null)
                return false;
            else
                return true;
        }

        
        private bool HitTestIfTransparency(HitPoint hitPosition)
        {
            var w = new WriteableBitmap(BaseImage, null);

            try
            {
                if (w.Pixels[w.PixelWidth * (int)hitPosition.Y + (int)hitPosition.X] != 0)
                {
                    return false; // nie transparent.
                }
                else
                {
                    return true; // transparent.
                }
            }
            catch { return true; }
        }

        [Description("Image name with extension ( example: 'test.png'")]
        [System.ComponentModel.Category("Base Image Settings")]
        public string BaseImageNameInResources
        {
            get { return (String) GetValue(BaseImageNameInResourcesProperty); }
            set { SetValue(BaseImageNameInResourcesProperty, value);}
        }

        public static readonly DependencyProperty BaseImageFolderInResourcesProperty =
            DependencyProperty.Register("BaseImageFolderInResources", typeof(ImagesFolder), typeof(FinderImageForLocation),
            new PropertyMetadata(ImagesFolder.OthersIcon));

        [Description("The folder in which the image is located.")]
        [System.ComponentModel.Category("Base Image Settings")]
        public ImagesFolder BaseImageFolderInResources
        {
            get { return (ImagesFolder) GetValue(BaseImageFolderInResourcesProperty); }
            set { SetValue(BaseImageFolderInResourcesProperty, value);}
        }

        public static readonly DependencyProperty StatusImagesProperty =
            DependencyProperty.Register("StatusImages", typeof(IList<StatusImage>), typeof(FinderImageForLocation),
                new PropertyMetadata(OnStatusImagesChanged));

        [System.ComponentModel.Category("Status Images")]
        public IList<StatusImage> StatusImages
        {
            get { return (IList<StatusImage>)(GetValue(StatusImagesProperty)); }
            set { SetValue(StatusImagesProperty, value); }
        }

        private static void OnStatusImagesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetImages = (FinderImageForLocation)d;
            var oldImages = (IList<StatusImage>)e.OldValue;

            var newImages = targetImages.StatusImages;
            targetImages.OnStatusImagesChanged(oldImages, newImages);
        }

        public static readonly DependencyProperty StatusImagesSizeProperty =
            DependencyProperty.Register("StatusImagesSize", typeof(StatusImagesSize), typeof(FinderImageForLocation),
                new PropertyMetadata(StatusImagesSize.Middle));

        [System.ComponentModel.Category("Status Images")]
        public StatusImagesSize StatusImagesSize
        {
            get { return (StatusImagesSize) (GetValue(StatusImagesSizeProperty)); }
            set { SetValue(StatusImagesSizeProperty,value);}
        }

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(String),
            typeof(FinderImageForLocation),
            new PropertyMetadata(String.Empty, OnTextChanged));

        public String Text
        {
            get { return GetValue(TextProperty).ToString(); }
            set { SetValue(TextProperty, value);}
        }

        private static void OnTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var targetText = (FinderImageForLocation)d;
                var oldText = e.OldValue.ToString();

                var newImages = targetText.Text;
                targetText.OnTextChanged(oldText, newImages);
            }
            catch(Exception ex)
            {
              
            }
        }

        protected virtual void OnTextChanged(string oldText, string newText)
        {
            ImageTextBlock.Text = newText;
        }

        private int GetNodeOffset(int positionXorY)
        {
            return positionXorY + (int)StatusImagesSize > ROW_COUNT ? (int) StatusImagesSize : 0;
        }

        public static readonly DependencyProperty TextHorizontalAlignmentProperty =
            DependencyProperty.Register("TextHorizontalAlignment", typeof(HorizontalAlignment), typeof(FinderImageForLocation),
                new PropertyMetadata(System.Windows.HorizontalAlignment.Center));
        
        [System.ComponentModel.Category("Text Alignment")]
        public HorizontalAlignment TextHorizontalAlignment
        {
            get { return (HorizontalAlignment) GetValue(TextHorizontalAlignmentProperty); }
            set { SetValue(TextHorizontalAlignmentProperty, value);}
        }

        
        public static readonly DependencyProperty TextVerticalAlignmentProperty =
            DependencyProperty.Register("TextVerticalAlignment", typeof(VerticalAlignment), typeof(FinderImageForLocation),
                new PropertyMetadata(System.Windows.VerticalAlignment.Center));

        [System.ComponentModel.Category("Text Alignment")]
        public VerticalAlignment TextVerticalAlignment
        {
            get { return (VerticalAlignment)GetValue(TextVerticalAlignmentProperty); }
            set { SetValue(TextVerticalAlignmentProperty, value); }
        }

        public int TextRow { get; set; }
        public int TextColumn { get; set; }

        public static readonly DependencyProperty HorizontalTextPositionProperty =
            DependencyProperty.Register("HorizontalTextPosition", typeof(HorizontalTextPosition), typeof(FinderImageForLocation), new PropertyMetadata(HorizontalTextPosition.Right));

        [System.ComponentModel.Category("Text Position Relative To The BaseImage")]
        public HorizontalTextPosition HorizontalTextPosition
        {
            get { return (HorizontalTextPosition)GetValue(HorizontalTextPositionProperty); }
            set
            {
                TextColumn = (int)value;
                SetValue(HorizontalTextPositionProperty, value);
            }
        }

        public static readonly DependencyProperty VerticalTextPositionProperty =
            DependencyProperty.Register("VerticalTextPosition", typeof(VerticalTextPosition), typeof(FinderImageForLocation),
                new PropertyMetadata(VerticalTextPosition.Center));

        [System.ComponentModel.Category("Text Position Relative To The BaseImage")]
        public VerticalTextPosition VerticalTextPosition
        {
            get { return (VerticalTextPosition)GetValue(VerticalTextPositionProperty); }
            set
            {
                TextRow = (int)value;
                SetValue(VerticalTextPositionProperty, value);
            }
        }

        public static readonly DependencyProperty ControlStyleProperty = DependencyProperty.Register("ControlStyle",
            typeof(ControlStyle), typeof(FinderImageForLocation),
            new PropertyMetadata(ControlStyle.Normal, OnControlStyleChanged));

        public static readonly DependencyProperty DisplayBaseImageColorProperty = DependencyProperty.Register("DisplayBaseImageColor", typeof(BitmapMode), typeof(FinderImageForLocation),
            new PropertyMetadata(OnBaseImageColorChanged));

        private static void OnBaseImageColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetColor = (FinderImageForLocation)d;
            var oldColor = (ControlStyle)e.OldValue;

            var newColor = targetColor.DisplayBaseImageColor;
            targetColor.OnBaseImageColorChanged(oldColor, newColor);
        }

        private void OnBaseImageColorChanged(Controls.ControlStyle oldColor, BitmapMode newColor)
        {
            LoadImage(BaseImageNameInResources, newColor);
        }

        [System.ComponentModel.Category("Base Image Settings")]
        public BitmapMode DisplayBaseImageColor
        {
            get { return (BitmapMode) GetValue(DisplayBaseImageColorProperty); }
            set { SetValue(DisplayBaseImageColorProperty, value);}
        }

        private static void OnControlStyleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetStyle = (FinderImageForLocation)d;
            var oldStyle = (ControlStyle)e.OldValue;

            var newStyle = targetStyle.ControlStyle;
            targetStyle.OnControlStyleChanged(oldStyle, newStyle);
        }

        protected void OnControlStyleChanged(ControlStyle oldStyle, ControlStyle newStyle)
        {
            if (ControlStyle.Equals(ControlStyle.Normal))
            {
                LayoutRoot.Background = new SolidColorBrush(Colors.Transparent);
                ImageTextBlock.Foreground = new SolidColorBrush(Colors.Black);
            }
            else if (ControlStyle.Equals(ControlStyle.Blood))
            {
                LayoutRoot.Background = new SolidColorBrush(Colors.Red);
                LayoutRoot.Opacity = 0.1d;
                ImageTextBlock.Foreground =  new SolidColorBrush(Colors.Red);
            }
            else if (ControlStyle.Equals(ControlStyle.Dark))
            {
                LayoutRoot.Background = new SolidColorBrush(Colors.Black);
                LayoutRoot.Opacity = 0.4d;
                ImageTextBlock.Foreground = new SolidColorBrush(Colors.Cyan);
            }
        }

        public ControlStyle ControlStyle
        {
            get { return (ControlStyle) GetValue(ControlStyleProperty); }
            set { SetValue(ControlStyleProperty, value); }
        }

        protected virtual void OnStatusImagesChanged(IList<StatusImage> oldImages, IList<StatusImage> newImages)
        {
           
            if (BaseImage != null)
            {
                BaseImage.Stretch = Stretch.Uniform;
                LeftLayout.Children.Clear();
                Grid.SetRow(BaseImage, 0);
                Grid.SetColumn(BaseImage, 0);
                Grid.SetColumnSpan(BaseImage, 6);
                Grid.SetRowSpan(BaseImage, 6);
                LeftLayout.Children.Add(BaseImage);
                BaseImage.MouseLeftButtonDown += BaseImage_MouseLeftButtonDown;
                BaseImage.MouseLeftButtonUp += BaseImage_MouseLeftButtonUp;
            }

            if (newImages != null)
            {
                for (int i = 0; i < newImages.Count; i++)
                {
                    var sourceImage = new ImageBrush()
                    {
                        ImageSource =
                            new BitmapImage(new Uri(BitmapBuffer.GetFileLocation(newImages[i].FileName, ImagesFolder.UnitIconsStatus),
                                UriKind.RelativeOrAbsolute)),
                        Stretch = Stretch.Uniform
                    };

                    BitmapImage bi = sourceImage.ImageSource as BitmapImage;

                    newImages[i].Image = new Image();
                    newImages[i].Image.Source = bi;
                    newImages[i].Image.Stretch = Stretch.Uniform;
                    var reference = newImages[i].Image;
                    var color = newImages[i].DisplayColor;

                    bi.ImageOpened += (sender, e) =>
                    {
                        var brush = new ImageBrush();

                        /*if (DisplayBaseImageColor.Equals(BitmapMode.Gray))
                        {
                            brush.ImageSource = bi.MakeGrayScale();
                        }
                        else if (DisplayBaseImageColor.Equals(BitmapMode.Normal))
                        {
                            brush.ImageSource = sourceImage.ImageSource;
                        }
                        else
                        {
                            brush.ImageSource = bi.MakeColorsIntense(BitmapBuffer.Intense[DisplayBaseImageColor]);
                        }*/

                        brush.ImageSource = bi.MakeColorsIntense(BitmapBuffer.Intense[color]);

                        reference.Source = brush.ImageSource;
                    };

                    if (AlligmentType.Equals(AlligmentType.OnYourself))
                    {
                        Grid.SetRow(newImages[i].Image,ON_YOUR_SELF_XY);
                        Grid.SetColumn(newImages[i].Image, ON_YOUR_SELF_XY);
                        Grid.SetRowSpan(newImages[i].Image, ROW_COUNT);
                        Grid.SetColumnSpan(newImages[i].Image, COLUMN_COUNT);
                    }
                    else if (AlligmentType.Equals(AlligmentType.InTheCorners))
                    {
                        Grid.SetRow(newImages[i].Image, CornersPosition[i,Y] - GetNodeOffset(CornersPosition[i,Y]));
                        Grid.SetColumn(newImages[i].Image, CornersPosition[i,X] - GetNodeOffset(CornersPosition[i,X]));
                        Grid.SetRowSpan(newImages[i].Image, (int)StatusImagesSize);
                        Grid.SetColumnSpan(newImages[i].Image, (int)StatusImagesSize);
                    }
                    else if (AlligmentType.Equals(AlligmentType.NextToEachOther))
                    {
                        CheckAndSetStatusImagesSizeForNextToEachOther(oldImages, newImages);

                        Grid.SetRow(newImages[i].Image, NextToEachOrderPosition[0, Y] - ((i+1) * (int)StatusImagesSize));
                        Grid.SetColumn(newImages[i].Image, NextToEachOrderPosition[0,X] - (int)StatusImagesSize);
                        Grid.SetRowSpan(newImages[i].Image, (int)StatusImagesSize);
                        Grid.SetColumnSpan(newImages[i].Image, (int)StatusImagesSize);
                    }

                    LeftLayout.Children.Add(newImages[i].Image);
                }
            }
            else
            {
                if (newImages != null)
                    tempStatus = newImages;
            }
        }

        private IList<StatusImage> tempStatus = null;

        private void CheckAndSetStatusImagesSizeForNextToEachOther(IList<StatusImage> oldImages, IList<StatusImage> newImages)
        {
            if (StatusImagesSize.Equals(StatusImagesSize.Big) && newImages.Count > 2)
            {
                StatusImagesSize = StatusImagesSize.Middle;
            }
            if (StatusImagesSize.Equals(StatusImagesSize.Middle) && newImages.Count > 3)
            {
                StatusImagesSize = StatusImagesSize.Small;
            }
            if (StatusImagesSize.Equals(StatusImagesSize.Small) && newImages.Count > 6)
            {
                AlligmentType = AlligmentType.OnYourself;
                OnStatusImagesChanged(newImages, newImages);
            }
        }

        void BaseImage_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            IsHitTransparency = null;
            this.UpdateLayout();
        } 

        public static readonly DependencyProperty AlligmentTypeProperty =
            DependencyProperty.Register("AlligmentType", typeof(AlligmentType), typeof(FinderImageForLocation), new PropertyMetadata(null));

        public AlligmentType AlligmentType
        {
            get { return (AlligmentType)(GetValue(AlligmentTypeProperty)); }
            set { SetValue(AlligmentTypeProperty, value); }
        }

        public FinderImageForLocation()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty IsHitTransparencyProperty =
            DependencyProperty.Register("IsHitTransparency", typeof(bool?), typeof(FinderImageForLocation), new PropertyMetadata(null));

        public bool? IsHitTransparency
        {
            get { return (bool?)GetValue(IsHitTransparencyProperty); }
            set { SetValue(IsHitTransparencyProperty, value); }
        }
        
        void BaseImage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            
        }
    }
}
