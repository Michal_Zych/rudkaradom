﻿using Finder.BloodDonation.DBBuffers;
using Finder.BloodDonation.Model.Dto.Enums;
using Finder.BloodDonation.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Controls
{
    public partial class M2MAlarmDisplayer : UserControl
    {
        private const string SCROLL_VIEVER_NAME = "ScrollViewer";
        private const string BASE_GRID_NAME = "BaseGrid";

        private const string MONITORABLE_OBJECT_PREFIX = "MonitorableObject_";
        private const string ALARM_PREFIX = "Alarm_";
        private const string CHECKBOX_PREFIX = "CheckBox_";

        private ScrollViewer ScrollViewer = null;
        private Grid BaseGrid = null;

        private bool[] IsPatientExpand { get; set; }

        /// <summary>
        /// This list stored all alarms identificators to closed.
        /// </summary>
        private List<int> alarmsToClosedIds { get; set; }

        private List<int> MonitorableObjectIds { get; set; }
        private List<ObjectType> MonitorableObjectTypes { get; set; }

        private bool IsMonitorableObjectChanged { get; set; }

        private int ROW = 0;

        public static readonly DependencyProperty AlarmsListProperty =
            DependencyProperty.Register("AlarmsList", typeof(IList<Alarm>), typeof(M2MAlarmDisplayer),
                new PropertyMetadata(OnAlarmsListChenged));

        public IList<Alarm> AlarmsList
        {
            get { return (List<Alarm>)(GetValue(AlarmsListProperty)); }
            set { SetValue(AlarmsListProperty, value); }
        }

        private static void OnAlarmsListChenged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetAlarms = (M2MAlarmDisplayer)d;
            var oldAlarms = (IList<Alarm>)e.OldValue;

            var newAlarms = targetAlarms.AlarmsList;
            targetAlarms.OnAlarmsListChenged(oldAlarms, newAlarms);
        }

        protected void OnAlarmsListChenged(IList<Alarm> oldAlarms, IList<Alarm> newAlarms)
        {
            ReStart();
        }

        public void ReStart()
        {
            RemoveAll();

            InitializeMonitorablesObjectsLists();
            InitializeMonitorableObjectExpandArray(MonitorableObjectIds.Count);

            InitializeControls();

            InitializeMonitorableObjectControls();
            this.ScrollViewer.Content = BaseGrid;
            viewport.Children.Add(ScrollViewer);
            alarmsToClosedIds = new List<int>();
        }

        private void RemoveAll()
        {
            ROW = 0;
            ScrollViewer = null;
            BaseGrid = null;
            viewport.Children.Clear();
            MonitorableObjectIds = null;
            MonitorableObjectTypes = null;
            IsPatientExpand = null;
        }

        public void Update()
        {

        }

        private void InitializeControls()
        {
            ScrollViewer = InitializeScrollViewer();
            BaseGrid = InitializeBaseGrid();
        }

        private void InitializeMonitorableObjectControls()
        {
            for (int i = 0; i < MonitorableObjectIds.Count; i++)
                InitializeMonitorableObjectItem(MonitorableObjectIds[i]);
        }

        private void InitializeMonitorableObjectItem(int index)
        {
            Border b = new Border();
            b.BorderBrush = new SolidColorBrush(Color.FromArgb(50, 243, 100, 100));
            b.BorderThickness = new Thickness(2);
            b.Name = MONITORABLE_OBJECT_PREFIX + index;

            Grid g = new Grid();
            g.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });

            g.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto }); // CheckBox.
            g.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto }); // FinderImage.
            g.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto }); // Display Name.
            g.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto }); // Display Name.

            CheckBox checkBox = new CheckBox();
            checkBox.Name = CHECKBOX_PREFIX + MONITORABLE_OBJECT_PREFIX + index;
            Grid.SetRow(checkBox, 0);
            Grid.SetColumn(checkBox, 0);
            g.Children.Add(checkBox);
            checkBox.Checked += checkBox_Checked;
            checkBox.Unchecked += checkBox_Unchecked;

            FinderImage image = GetImage(index);
            image.TextHorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            image.HorizontalTextPosition = HorizontalTextPosition.Right;
            Alarm[] alarms = AlarmsList.Where(e => e.ObjectId == index).OrderByDescending(x=>x.EventTypes).ToArray();

            image.DisplayBaseImageColor = GetImageColor(alarms.FirstOrDefault());
            Grid.SetRow(image, 0);
            Grid.SetColumn(image, 1);
            g.Children.Add(image);
            g.Background = new SolidColorBrush(Color.FromArgb(50, 199, 199, 199));

            TextBlock tb = new TextBlock();
            tb.Text = AlarmsList.FirstOrDefault(e=>e.ObjectId == index).Info;
            Grid.SetRow(tb,0);
            Grid.SetColumn(tb,2);
            g.Children.Add(tb);

            b.Child = g;
            b.MouseEnter += b_MouseEnter;
            b.MouseLeave += b_MouseLeave;
            b.MouseLeftButtonDown += b_MouseLeftButtonDown;
            b.MouseLeftButtonUp += b_MouseLeftButtonUp;

            BaseGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            Grid.SetRow(b, ROW);
            BaseGrid.Children.Add(b);
            ROW++;
            InitializeAlarmsForMonitorableObject(index);
        }

        private void checkBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (sender == null)
                return;

            IsMonitorableObjectChanged = true;
            RemoveAlarmsIdsFromClosedAlarmsList(GetAlarmsIdsForMonitorableObject(ParseCheckBoxNameAndGetId((CheckBox)sender)));
            ChangeAllAlarmsCheckBoxInMonitorableObjectChanged((CheckBox)sender, false);
            IsMonitorableObjectChanged = false;
        }

        // For Monitorable object.
        private void checkBox_Checked(object sender, RoutedEventArgs e)
        {
            if (sender == null)
                return;

            IsMonitorableObjectChanged = true;
            AddAlarmsIdsToClosedAlarmsList(GetAlarmsIdsForMonitorableObject(ParseCheckBoxNameAndGetId((CheckBox)sender)));
            ChangeAllAlarmsCheckBoxInMonitorableObjectChanged((CheckBox)sender, true);

            MsgBox.Confirm("Czy jesteś pewien/na, że chcesz zamknąć alarm?", OnClose, MsgBox.Buttons.YesNo);

            IsMonitorableObjectChanged = false;
        }

        private void OnClose(object sender, Telerik.Windows.Controls.WindowClosedEventArgs e)
        {
            bool dr =
                e.DialogResult == null || e.DialogResult == false 
                ? false 
                : true;

            if(dr)
            {

            }
            else
            {

            }

        }

        private void ChangeAllAlarmsCheckBoxInMonitorableObjectChanged(CheckBox checkBox, bool checkedState)
        {
            const int CHECKBOX_POSIRION_IN_ITEM_GRID = 0;

            List<Border> t = GetAlarmsBordersForMonitorableObject(int.Parse(new string(checkBox.Name.SkipWhile(f => !Char.IsDigit(f)).ToArray()))).ToList(); // Poprawić nulla.

            GetAlarmsBordersForMonitorableObject(int.Parse(new string(checkBox.Name.SkipWhile(f => !Char.IsDigit(f)).ToArray())))
                .ToList()
                .ForEach(x => ((CheckBox)((Grid)x.Child).Children[CHECKBOX_POSIRION_IN_ITEM_GRID]).IsChecked = checkedState);
        }


        private IEnumerable<Border> GetAlarmsBordersForMonitorableObject(int monitorableObjectId)
        {
            List<Border> t = new List<Border>();

            for (int i = 0; i < BaseGrid.Children.Count; i++)
            {
                if (((Border)BaseGrid.Children[i]).Name.Contains(ALARM_PREFIX))
                {
                    for (int j = 0; j < AlarmsList.Count; j++)
                    {
                        if (EqualsIndex(AlarmsList[j].Id,
                            GetIndexFromName(((Border)BaseGrid.Children[i]).Name)))
                        {
                            if (EqualsIndex(AlarmsList[j].ObjectId,
                                monitorableObjectId))
                            {
                                t.Add((Border)BaseGrid.Children[i]);
                            }
                        }
                    }
                }
            }

            return t;
        }

        /// <summary>
        /// This method equals two numeric value.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="otherId"></param>
        /// <returns>true if equals, false if not.</returns>
        private bool EqualsIndex(int id, int otherId)
        {
            if (id == otherId)
                return true;
            else
                return false;
        }

        /// <summary>
        /// This method parsed text and get numeric index.
        /// </summary>
        /// <param name="name">Name to parse.</param>
        /// <returns>Numeric index.</returns>
        private int GetIndexFromName(string name)
        {
            return
                int.Parse(new string(name
                                .SkipWhile(f => !Char.IsDigit(f)).ToArray()));
        }

        /// <summary>
        /// This method checked if alarm identification is not contains in closedAlarmsIds list.
        /// </summary>
        /// <param name="alarmsIds">Alarms identificators to closed.</param>
        private void AddAlarmsIdsToClosedAlarmsList(IEnumerable<int> alarmsIds)
        {
            alarmsIds
                .Where(x => alarmsToClosedIds.Any(e=>e != x))
                .ToList()
                .ForEach(y => alarmsToClosedIds.Add(y));
        }

        /// <summary>
        /// This method checked if alarm identifcation is contains in closedAlarmsIds list.
        /// </summary>
        /// <param name="alarmsIds">Alarms identificators to closed.</param>
        private void RemoveAlarmsIdsFromClosedAlarmsList(IEnumerable<int> alarmsIds)
        {
            alarmsIds
                .Where(x => alarmsToClosedIds.Any(e => e == x))
                .ToList()
                .ForEach(y => alarmsToClosedIds.Remove(y));
        }

        /// <summary>
        /// This method parse checkbox name and return monitorable object identyficator.
        /// </summary>
        /// <param name="checkBoxToParse">CheckBox control to name parse.</param>
        /// <returns>Monitorable object identyficator</returns>
        private int ParseCheckBoxNameAndGetId(CheckBox checkBoxToParse)
        {
            return 
                int.Parse(
                    checkBoxToParse.Name
                    .Contains(CHECKBOX_PREFIX + MONITORABLE_OBJECT_PREFIX)
                    ? checkBoxToParse.Name.Replace(CHECKBOX_PREFIX + MONITORABLE_OBJECT_PREFIX, String.Empty)
                    : checkBoxToParse.Name.Replace(CHECKBOX_PREFIX + ALARM_PREFIX, String.Empty)
                );
        }

        /// <summary>
        /// This method returns alarms identyfication collection for monitorable object.
        /// </summary>
        /// <param name="id">Monitorable object identyficator.</param>
        /// <returns>Alarms identyfication collection</returns>
        private IEnumerable<int> GetAlarmsIdsForMonitorableObject(int id)
        {
            return AlarmsList
                .Where(x => x.ObjectId == id)
                .Select(x => x.Id);
        }

        void b_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Border b = (Border)sender;
            ((Grid)b.Child).Background = new SolidColorBrush(Color.FromArgb(50, 199, 199, 199));

            int index = int.Parse(b.Name.Replace(MONITORABLE_OBJECT_PREFIX, String.Empty));

            ExpandOrHoverItemsForAcordion(index);
        }

        /// <summary>
        /// This method is responsible for expands and hovered items for acordion.
        /// </summary>
        /// <param name="acordionId">Monitorable object idnetyficator.</param>
        private void ExpandOrHoverItemsForAcordion(int acordionId)
        {
            IEnumerable<int> itemsId = AlarmsList.Where(e => e.ObjectId == acordionId).Select(e=>e.Id);

            foreach(int i in itemsId)
                for (int j = 0; j < BaseGrid.Children.Count; j++)
                    CheckIfControlIsAlarmItemAndChangeVisibility(GetBorderIfUIElementIsBorder(j), i);
        }

        /// <summary>
        /// This method checked if UIElement from Children in 'BaseGrid' is Border type and return Border or null. 
        /// </summary>
        /// <param name="index">UIElemnet index in children array from BaseGrid.</param>
        /// <returns>Border if is exist, null if UIElement has a different type.</returns>
        private Border GetBorderIfUIElementIsBorder(int index)
        {
            return (BaseGrid.Children[index].GetType().Equals(typeof(Border))) ?
                (Border)BaseGrid.Children[index] : null;
        }

        /// <summary>
        /// This method checks if the element is an alarm and changed visibility state.
        /// </summary>
        /// <param name="b">Item for Checked.</param>
        /// <param name="itemId">Target alarm identyficator.</param>
        private void CheckIfControlIsAlarmItemAndChangeVisibility(Border b, int itemId)
        {
            if (b != null && b.Name.Contains(ALARM_PREFIX + itemId))
                ChangeVisibilityStateForItem(b);
        }

        /// <summary>
        /// This method changed item visibility state.
        /// </summary>
        /// <param name="b">Item to changed.</param>
        private void ChangeVisibilityStateForItem(Border b)
        {
            switch (b.Visibility)
            {
                case System.Windows.Visibility.Visible:
                    b.Visibility = System.Windows.Visibility.Collapsed;
                    break;

                case System.Windows.Visibility.Collapsed:
                    b.Visibility = System.Windows.Visibility.Visible;
                    break;
            }
        }

        void b_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Border b = (Border)sender;
            Grid g = (Grid)b.Child;
            g.Background = new SolidColorBrush(Color.FromArgb(50, 133, 133, 133));
        }

        void b_MouseLeave(object sender, MouseEventArgs e)
        {
            Border b = (Border)sender;
            Grid g = (Grid)b.Child;
            string id = String.Empty;

            SolidColorBrush scb = g.Background as SolidColorBrush;

            Color newColor = new Color();
            int A = scb.Color.A;
            int R = scb.Color.R + 50;
            int G = scb.Color.G + 50;
            int B = scb.Color.B + 50;

            g.Background = new SolidColorBrush(Color.FromArgb((byte)A, (byte)R, (byte)G, (byte)B));
        }

        void b_MouseEnter(object sender, MouseEventArgs e)
        {
            Border b = (Border)sender;
            Grid g = (Grid)b.Child;
            string id = String.Empty;

            SolidColorBrush scb = g.Background as SolidColorBrush;

            int A = scb.Color.A;
            int R = scb.Color.R - 50;
            int G = scb.Color.G - 50;
            int B = scb.Color.B - 50;

            g.Background = new SolidColorBrush(Color.FromArgb((byte)A, (byte)R, (byte)G, (byte)B));
        }

        private void InitializeAlarmsForMonitorableObject(int monitorableObjectId)
        {
            Alarm[] alarms = AlarmsList.Where(e => e.ObjectId == monitorableObjectId).ToArray();

            for(int i = 0; i < alarms.Length; i++)
            {
                Border b = new Border();
                b.BorderBrush = new SolidColorBrush(Color.FromArgb(50, 243, 100, 100));
                b.BorderThickness = new Thickness(2);

                b.Name = ALARM_PREFIX + alarms[i].Id;

                Grid g = new Grid();
                g.Background = new SolidColorBrush(Color.FromArgb(50, 222, 222, 222));
                g.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });

                g.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(35d)});
                g.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
                g.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
                g.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
                g.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });

                CheckBox checkBox = new CheckBox();
                checkBox.Name = CHECKBOX_PREFIX + ALARM_PREFIX + alarms[i].Id;
                checkBox.Checked +=alarmCheckBox_Checked;
                checkBox.Unchecked += alarmCheckBox_Unchecked;
                Grid.SetColumn(checkBox, 1);
                Grid.SetRow(checkBox, 0);
                g.Children.Add(checkBox);

                FinderImage fi = new FinderImage();
                fi.TextHorizontalAlignment = System.Windows.HorizontalAlignment.Right;
                fi.HorizontalTextPosition = HorizontalTextPosition.Right;
                fi.Width = 50d;
                fi.Height = 50d;
                fi.DisplayBaseImageColor = GetImageColor(alarms[i]);
                fi.IsBaseImageFromResources = true;
                fi.BaseImageFolderInResources = ImagesFolder.Objects;
                fi.BaseImageNameInResources = GetImageName(alarms[i]);
                Grid.SetRow(fi, 0);
                Grid.SetColumn(fi, 2);
                g.Children.Add(fi);

                TextBlock alarmDate = new TextBlock();
                alarmDate.Text = alarms[i].StartTime.ToString();
                Grid.SetRow(alarmDate, 0);
                Grid.SetColumn(alarmDate, 3);
                g.Children.Add(alarmDate);

                TextBlock alarmType = new TextBlock();
                alarmType.Text = alarms[i].Type.Equals(Tabs.UnitsList.AlarmType.SOS)
                    ? "SOS" : alarms[i].Type.Equals(Tabs.UnitsList.AlarmType.NoMoving) 
                    ? "Pacjent się nie rusza" : alarms[i].Type.Equals(Tabs.UnitsList.AlarmType.NoGoZoneIn) 
                    ? "Wejście do strefy zakazanej" : String.Empty;
                Grid.SetRow(alarmType, 0);
                Grid.SetColumn(alarmType, 4);
                g.Children.Add(alarmType);
                BaseGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
                b.Child = g;
                b.MouseEnter+=b_MouseEnter;
                b.MouseLeave+=b_MouseLeave;
                b.MouseLeftButtonDown += item_LeftButtonDown;
                b.MouseLeftButtonUp += item_LeftButtonUp;

                Grid.SetRow(b, ROW);
                ROW++;
                BaseGrid.Children.Add(b);
            }
        }

        private void alarmCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            AlarmsCheckedStateChange(sender, false);
        }

        private void alarmCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            AlarmsCheckedStateChange(sender, true);
        }

        private void AlarmsCheckedStateChange(object sender, bool newCheckedState)
        {
            if (!IsMonitorableObjectChanged)
            {
                if (alarmsToClosedIds.Contains(GetIndexFromName(((CheckBox)sender).Name)))
                {
                    alarmsToClosedIds.Remove(GetIndexFromName(((CheckBox)sender).Name));
                    ((CheckBox)sender).IsChecked = false;
                }
            }
        }

        private void item_LeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Border b = (Border)sender;
            Grid g = (Grid)b.Child;
            g.Background = new SolidColorBrush(Color.FromArgb(50, 199, 199, 199));
        }

        private void item_LeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Border b = (Border)sender;
            Grid g = (Grid)b.Child;
            g.Background = new SolidColorBrush(Color.FromArgb(50, 190, 184, 222));
        }

        private FinderImage GetImage(int monitorableObjectId)
        {
            FinderImage fi = new FinderImage();
            fi.Width = 50d;
            fi.Height = 50d;
            fi.IsBaseImageFromResources = true;

            Alarm[] alarms = AlarmsList.Where(e => e.ObjectId == monitorableObjectId).ToArray();

            string name = string.Empty;
            BitmapMode? bm = null;

            for(int i = 0; i < alarms.Length; i++)
            {
                if (name.Equals(String.Empty))
                    name = GetImageName(alarms[i]);

                if (bm != null && bm.Equals(BitmapMode.Red))
                    break;
                
                if (bm == null)
                    bm = GetImageColor(alarms[i]);
            }

            
            fi.BaseImageFolderInResources = ImagesFolder.Objects;
            fi.BaseImageNameInResources = name;

            return fi;
        }

        private string GetImageName(Alarm alarm)
        {
            switch(alarm.ObjectType)
            {
                case ObjectType.Patient:
                    return "holter.png";

                case ObjectType.Band:
                    return "lokalizator.png";

                default:
                    return String.Empty;
            }
        }

        private BitmapMode GetImageColor(Alarm alarm)
        {
            switch(alarm.EventTypes)
            {
                case EventTypes.Alarm:
                    return BitmapMode.Red;
                
                case EventTypes.Warning:
                    return BitmapMode.Yellow;

                default:
                    return BitmapMode.Green;
            }
        }

        private ScrollViewer InitializeScrollViewer()
        {
            ScrollViewer g = new ScrollViewer();
            g.Width = 550d;
            g.Height = 200d;
            g.Name = SCROLL_VIEVER_NAME;
            g.HorizontalScrollBarVisibility = ScrollBarVisibility.Visible;
            g.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
            return g;
        }

        private Grid InitializeBaseGrid()
        {
            Grid g = new Grid();
            g.Name = BASE_GRID_NAME;

            return g;
        }

        private void InitializeMonitorableObjectExpandArray(int count)
        {
            IsPatientExpand = new bool[count];
        }
        
        private void CloseAlarm(IEnumerable<int> alarmIds)
        {
            // ToDo: Implementation...
        }

        private void CloseAlarm(int alarmId)
        {
            // ToDo: Implementation...
        }

        /// <summary>
        /// This method initialization Monitorable Objects lists ( Ids and Types ).
        /// </summary>
        private void InitializeMonitorablesObjectsLists()
        {
            InitializationMonitorableObjectIdsAndTypesIfNotInitialized();

            for (int i = 0; AlarmsList != null && i < AlarmsList.Count; i++)
                if (!CheckIfMonitorableObjectIsExistsInMonitorableObjectIds(AlarmsList[i].ObjectId))
                    AddMonitorablesObjectsToLists(AlarmsList[i].ObjectId, AlarmsList[i].ObjectType);
        }

        /// <summary>
        /// This method checked if MonitorableObjectIds And Types arrays is initialized 
        /// and initialize if null. 
        /// </summary>
        private void InitializationMonitorableObjectIdsAndTypesIfNotInitialized()
        {
            if (!CheckIfMonitorableObjectIdsIsInitialized())
                InitializeMonitorableObjectIds();
            if (!CheckIfMonitorableObjectTypesIsInitialized())
                InitializeMonitorableObjectTypes();
        }

        /// <summary>
        /// This method added Monitorable object info to MonitorableObjectIds and MonitorableObjectTypes.
        /// </summary>
        /// <param name="monitorableObjectId">Monitorable object id.</param>
        /// <param name="monitorableObjectType">Monitorable object type.</param>
        private void AddMonitorablesObjectsToLists(int monitorableObjectId, ObjectType monitorableObjectType)
        {
            MonitorableObjectIds.Add(monitorableObjectId);
            MonitorableObjectTypes.Add(monitorableObjectType);
        }

        /// <summary>
        /// This method checked if monitorable object is exists in 'MonitorableObjectIds' list.
        /// </summary>
        /// <param name="monitorableObjectId">Monitorable object index.</param>
        /// <returns>true, if monitorable object is exist. false, if not exists.</returns>
        private bool CheckIfMonitorableObjectIsExistsInMonitorableObjectIds(int monitorableObjectId)
        {
            if (MonitorableObjectIds.Contains(monitorableObjectId))
                return true;
            else
                return false;
        }

        /// <summary>
        /// This method checked if Monitorable object indexes is initialized.
        /// </summary>
        /// <returns>True, if Monitorable object indexes is initialized, false if is not initialized.</returns>
        private bool CheckIfMonitorableObjectIdsIsInitialized()
        {
            if (MonitorableObjectIds == null)
                return false;
            else
                return true;
        }

        /// <summary>
        /// This method initialize monitorable object indexes.
        /// </summary>
        private void InitializeMonitorableObjectIds()
        {
            MonitorableObjectIds = new List<int>();
        }

        /// <summary>
        /// This method checked if Monitorable object types is initialized.
        /// </summary>
        /// <returns>True, if Monitorable object types is initialized, false if is not initialized.</returns>
        private bool CheckIfMonitorableObjectTypesIsInitialized()
        {
            if (MonitorableObjectTypes == null)
                return false;
            else
                return true;
        }

        /// <summary>
        /// This method initialize monitorable object types.
        /// </summary>
        private void InitializeMonitorableObjectTypes()
        {
            MonitorableObjectTypes = new List<ObjectType>();
        }

        private static void OnAlarmsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {

        }

        public M2MAlarmDisplayer()
        {
            InitializeComponent();
        }
    }
}
