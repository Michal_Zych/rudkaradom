﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Globalization;

namespace Finder.BloodDonation.Converters
{
    public class DonationNumberConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string partStr = string.Empty;
            try
            {
                var part = System.Convert.ToInt32(parameter);
                var donationNr = (string)value;
                switch (part)
                {
                    case 1: partStr = donationNr.Substring(0, 5); break;
                    case 2: partStr = donationNr.Substring(5, 2); break;
                    case 3: partStr = donationNr.Substring(7, 6); break;
                    case 4: partStr = donationNr.Substring(13, 2); break;
                }
            }
            catch (Exception ex)
            {

            }

            return partStr;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
