﻿using System.Windows.Media.Imaging;
using Finder.BloodDonation.Controls;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.DBBuffers;

namespace Finder.BloodDonation.Converters
{
    using System;
    using System.Net;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Ink;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Animation;
    using System.Windows.Shapes;
    using Finder.BloodDonation.Model.Enum;
    using Finder.BloodDonation.Helpers;
    using Finder.BloodDonation.Settings;
    using Finder.BloodDonation.Model.Dto;
    using System.Collections.Generic;
    using Finder.BloodDonation.Layout;
    using System.Linq;


    /// <summary>
    /// Konwerter typu jednostki na odpowadająca mu grafike.
    /// </summary>
    /// 
    public class UnitImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string pictureName;
            const int WRONG_DATA = -1;

            if (parameter.ToString() == "AAA")
            {
                pictureName = "aa";
            }

            pictureName = UnitTypeOrganizer.GetPictureName(WRONG_DATA);

            try
            {
                UnitDTO unit = value as UnitDTO;
                if (unit != null)
                {
                    if (unit.PictureID.HasValue)
                        pictureName = unit.PictureID.Value.ToString();
                    else
                        pictureName = unit.UnitTypeIDv2.ToString();
                };

                if (value is UnitType)
                {
                    pictureName = value.ToString();
                };

                UnitPreviewDto unitPreview = value as UnitPreviewDto;
                if (unitPreview != null)
                {
                    if (unitPreview.PictureID.HasValue)
                        pictureName = unitPreview.PictureID.Value.ToString();
                    else
                        pictureName = unitPreview.UnitTypeV2.ToString();
                };

                if (value is int)
                {
                    pictureName = value.ToString();
                }
            }
            catch (Exception e)
            {

            }

            if(String.Equals(parameter.ToString(), "normal", StringComparison.OrdinalIgnoreCase))
            {
                return pictureName + ".png";
            }

            if (!parameter.Equals("prohibited") && !parameter.Equals("monitored") && !parameter.Equals("statusimages"))
            {
                Image baseImage = BitmapBuffer.GetIcon(pictureName + GetSufix(parameter.ToString()), ImagesFolder.UnitIconsUnit);
                return baseImage;
            }
            else if(parameter.Equals("statusimages"))
            {
                IList<StatusImage> statusImages = new List<StatusImage>();
                UnitDTO unit = (value as UnitViewModel).Unit as UnitDTO;
                bool isMonitored = unit != null && UnitManager.Instance.CheckIfUnitIsMonitored(unit.ID);
                
                StatusImage si = new StatusImage();
                
                if (isMonitored)
                {
                    si = new StatusImage();
                    si.FileName = "monitoredImage.png";

                    bool isConnect = false;
                    
                    
                    try
                    {
                        isConnect = !(value as UnitViewModel).IsAlarm;//.UnitIdWithDisconnectedTransmitter.FirstOrDefault(x => x == unit.ID);
                     }
                    catch(Exception ex)
                    {
                        isConnect = false;
                    }

                    if (isConnect)
                        si.DisplayColor = BitmapMode.Green;
                    else
                        si.DisplayColor = BitmapMode.Red;
                    
                    si.Image = BitmapBuffer.GetIcon("monitoredImage", ImagesFolder.UnitIconsStatus);
                    si.ActionType = ActionType.Flashes;

                    statusImages.Add(si);
                }

                return statusImages;
            }

            else if (parameter.Equals("controlstyle"))
            {
                ControlStyle style = ControlStyle.Normal;

                return style;
            }
            else
            {
                return null;
            }
        }

        private string GetSufix(string param)
        {
            string sufix = Settings.Current["ImagesSuffixNormal"];
            switch (param)
            {
                case "hover":
                    sufix = Settings.Current["ImagesSuffixHover"];
                    break;
                case "alarm":
                    sufix = Settings.Current["ImagesSuffixAlarm"];
                    break;
                case "normal":
                    break;
                case "monitored":
                    sufix = Settings.Current["ImagesSuffixMonitored"];
                    break;
                case "prohibited":
                    sufix = Settings.Current["ImagesSuffixProhibited"];
                    break;
                default:
                    break;
            }
            return sufix;
        }

        //zwraca id obrazka  skojarzonego z danym typem
        int GetPictureId(int unitTypeV2)
        {
            return unitTypeV2;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
