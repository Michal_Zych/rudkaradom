﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Model.Dto;

namespace Finder.BloodDonation.Converters
{
    public class AlarmSensorConverter : System.Windows.Data.IValueConverter
    {
        private static Brush brush_red = new SolidColorBrush(Colors.Red);
        private static Brush brush_black = new SolidColorBrush(Colors.Black);

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            return brush_red;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
