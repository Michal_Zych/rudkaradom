﻿using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Tabs.UnitsList;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation;

namespace Finder.BloodDonation.Converters
{
    public class BackgroundConverter : System.Windows.Data.IValueConverter
    {
        private static Brush brushRed = new SolidColorBrush(Colors.Red);
        private static Brush brushGrey = new SolidColorBrush(Colors.LightGray);

        public static Brush brushUser;


        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (brushUser == null)
            {
                brushUser = new SolidColorBrush(Colors.Yellow);
                brushUser.Opacity = 0.5;
            }
               // var brushUser = new SolidColorBrush(Settings.Settings.Current.GetColor(Settings.Settings.Current[Settings.SettingConsts.OldDataColor]));

            brushRed.Opacity = 0.5;
            brushGrey.Opacity = 0.5;
            try
            {
                var room = value as RoomViewModel;
                if (room != null)
                {
                    if (room.IsAlarm)
                    {
                        return brushRed;
                    }
                    else if (room.IsAlarmDisabled)
                    {
                        return brushGrey;
                    }
                    //else if(!room.Unit.Value.HasValue)
                    //{
                    //    return Settings.Settings.Current.OldDataBrush;
                    //}
                    //else if (room.Unit.MeasureTime.HasValue)
                    // {
                    //   var mins = Settings.Settings.Current.GetInt(Settings.ConfigurationKeys.OldDataTimeMinutes);
                    //  if(DateTime.Now.Subtract(room.Unit.MeasureTime.Value).TotalMinutes > mins)
                    //  {
                    //      return Settings.Settings.Current.OldDataBrush;
                    //  }
                    //}
                    //}
                    //else
                    //{
                    if (value != null)
                    {
                        bool b = System.Convert.ToBoolean(value);
                        if (b)
                        {
                            return brushRed;
                        }
                    }

                }
            }
            catch (Exception)
            {

            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
