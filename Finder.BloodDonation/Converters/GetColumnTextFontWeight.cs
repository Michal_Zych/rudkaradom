﻿using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
namespace Finder.BloodDonation.Converters
{
    public class GetColumnTextFontWeight : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var colName = (string)parameter;
            if (colName == null) return FontWeights.Normal;

            var item = value as PatientDTO;
            if (item != null)
            {
                if (colName.Equals("FirstName", StringComparison.InvariantCultureIgnoreCase)) return FontWeights.ExtraLight;
                if (colName.Equals("LastName", StringComparison.InvariantCultureIgnoreCase)) return FontWeights.ExtraBold;
            }

            return FontWeights.Normal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
