﻿using Finder.BloodDonation.DynamicData.DBs;
using Finder.BloodDonation.Model.Dto;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DBBuffers;
using Finder.BloodDonation.Controls;

namespace Finder.BloodDonation.Converters
{
    public class DeviceStatusConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            BandStatusDto status = value as BandStatusDto;

            if (status != null)
            {
                if (status.TopAlarmSeverity == (byte)EventTypes.Alarm)
                {
                    return BitmapMode.Red;
                }
                else if (status.TopAlarmSeverity == (byte)EventTypes.Warning)
                {
                    return BitmapMode.Yellow;
                }
                else
                {
                    if (status.ObjectUnitId != null && status.ObjectUnitId != 0)
                    {
                        if (status.CurrentUnitId == null || status.ObjectUnitId == 0)
                            return BitmapMode.Gray;

                        if (status.CurrentUnitId == status.ObjectUnitId)
                            return BitmapMode.Green;
                        else
                            return BitmapMode.Green;
                            //return BitmapMode.Green;
                    }
                    else
                    {
                        return BitmapMode.Cyan;
                    }
                }
            }

            return BitmapMode.White;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
