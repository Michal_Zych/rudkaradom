﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Helpers;

namespace Finder.BloodDonation.Converters
{
    public class AlarmListTempConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                AlarmDto ad = value as AlarmDto;


                if(ad.EventType < AlarmEventDescriptor.NO_SENSOR)
                {
                    if(ad.IsBitSensor)
                    {
                        if(ad.Value == 0)
                        {
                            return ad.LoStateDesc;
                        }
                        else
                        {
                            return ad.HiStateDesc;
                        }
                    }
                    return ad.Value.ToDisplayString() + ad.MU;
                }
                else
                {
                    return ad.EventType.ToDisplayString();
                }
            }
            catch (Exception)
            {

            }

            return "@#$%";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
