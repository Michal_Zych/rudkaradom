﻿using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.Model.Dto;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;

namespace Finder.BloodDonation.Converters
{
    public class DisplaySearchUnitConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            UnitDTO unit = UnitManager.Instance.Units.FirstOrDefault(x => x.ID == (int)value);
            
            if (unit == null)
                return string.Empty;

            string output = unit.GetPath("/") + " " + unit.Name;
            return output;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
