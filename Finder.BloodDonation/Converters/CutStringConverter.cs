﻿using System; 
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace Finder.BloodDonation.Converters
{
    public class CutStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //check parameter
            if (parameter == null)
                throw new NotImplementedException("Paramater can't be null");

            int paramValue = System.Convert.ToInt32(parameter);
            if (paramValue < 3){
                throw new ArgumentException("Parameter is not valid. Value must be greater than 3");
            }
            paramValue = paramValue - 3;

            //check value
            if (String.IsNullOrEmpty((string)value))
                return value;
            else
            {
                //cut value
                string tmpVal = (string)value;
                if (tmpVal.Length > paramValue)
                    return tmpVal.Substring(0, paramValue) + "...";
                else
                    return tmpVal;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
