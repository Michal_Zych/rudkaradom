﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace Finder.BloodDonation.Converters
{
	public class BloodGroupSymbolConverter : IValueConverter
	{

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			string symbol = String.Empty;

			if (value == null)
				return symbol;

			var prefix = ((string)value).Substring(0, 2);


			/*
			mapping.Add(6, 12); // a-
			mapping.Add(62, 13); // a+
			mapping.Add(17, 14); // b-
			mapping.Add(73, 15); // b+

			mapping.Add(28, 16); // ab-
			mapping.Add(84, 17); // ab+
			mapping.Add(95, 18); // 0-
			mapping.Add(51, 19); // 0+
			// plasma
			mapping.Add(66, 22); // a
			mapping.Add(77, 23); // b
			mapping.Add(88, 24); // ab
			mapping.Add(55, 25); // 0
			 */

			switch (prefix)
			{
				case "06":
					return symbol = "A-";
				case "62":
					return symbol = "A+";
				case "17":
					return symbol = "B-";
				case "73":
					return symbol = "B+";
				
				case "28":
					return symbol = "AB-";
				case "84":
					return symbol = "AB+";
				case "95":
					return symbol = "0-";
				case "51":
					return symbol = "0+";

				case "66":
					return symbol = "A";
				case "77":
					return symbol = "B";
				case "88":
					return symbol = "AB";
				case "55":
					return symbol = "0";
			}


			return symbol;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
