﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Converters
{
    public class SensorTypeConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                int ut = (int)value;
                switch (parameter.ToString())
                {
                    case "T1":
                        if (ut == 1)
                        {
                            return "Glicerol";
                        }
                        else
                        {
                            return "Wilgotność";
                        }
                    case "T2":
                        return "Powietrze";
                }
            }
            catch (Exception)
            {
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
