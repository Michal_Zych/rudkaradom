﻿using Finder.BloodDonation.Layout;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Converters
{
    public class UnitTreeFontColorConverter : System.Windows.Data.IValueConverter
    {
        private static Brush AlarmBrush = new SolidColorBrush(Colors.Red);
        private static Brush NormalBrush = new SolidColorBrush(Colors.Black);

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            var nullableState = value as UnitColorState?;
            UnitColorState state;
            if(nullableState == null || !nullableState.HasValue)
            {
                state = UnitColorState.Normal;
            }
            else
            {
                state = nullableState.Value;
            }
            
            var brush = NormalBrush;
            switch(state)
            {
                case UnitColorState.IsAlarm : brush = AlarmBrush; 
                    break;
                case UnitColorState.IsOld: brush = Settings.Settings.Current.OldDataBrush;
                    break;

            }

            if (targetType == typeof(Color))
            {
                return ((SolidColorBrush)brush).Color;
            }
            return brush;

            /*
            var unit = value as UnitViewModel;
            if(unit == null)
            {
                ;
            }

            var brush = NormalBrush;

            if(unit != null)
            {
                if(unit.IsAlarm)
                {
                    brush = AlarmBrush;
                }
                else if(unit.IsDisableAlarm)
                {
                    brush = NormalBrush;
                }
                else if(unit.IsOldData)
                {
                    brush = Settings.Settings.Current.OldDataBrush;
                }
            }

            if (targetType == typeof(Color))
            {
                return ((SolidColorBrush)brush).Color;
            }
            return brush;
             */
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
