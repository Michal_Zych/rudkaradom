﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Converters
{
    public class ColumnEditorDataNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                string textToConvert = value.ToString();

                if (parameter.ToString().Equals("normal"))
                {
                    switch (textToConvert)
                    {
                        case "True":
                            return "Tak";

                        case "False":
                            return "Nie";

                        case "Left":
                            return "Lewo";

                        case "Center":
                            return "Środek";

                        case "Right":
                            return "Prawo";
                    }
                }
                else if(parameter.ToString().Equals("back"))
                {
                    switch (textToConvert)
                    {
                        case "Tak":
                            return "True";

                        case "Nie":
                            return "False";

                        case "Lewo":
                            return "Left";

                        case "Środek":
                            return "Center";

                        case "Prawo":
                            return "Right";
                    }
                }

                return textToConvert;
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                string textToConvert = value.ToString();

                switch (textToConvert)
                {
                    case "Tak":
                        return "True";

                    case "Nie":
                        return "False";

                    case "Lewo":
                        return "Left";

                    case "Środek":
                        return "Center";

                    case "Prawo":
                        return "Right";
                }

                return textToConvert;
            }
            return string.Empty;
        }
    }
}
