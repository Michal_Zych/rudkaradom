﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Converters
{
    public class VPercentConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                DeviceType dt = (DeviceType)value;
                if (dt == DeviceType.THERMOHYGROMETER)
                {
                    return Visibility.Visible;
                }
            }
            catch (Exception)
            {
            }

            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
