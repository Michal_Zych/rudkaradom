﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Converters
{
    public class NullableDateConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DateTime? date = value as DateTime?;
            if (date == null) return "";

            
            string param = parameter == null ? "" : (string)parameter;
            string mask = "yyyy-MM-dd";

            if (date.HasValue && date.Value.Year > 1)
            {
                if (param.Equals("tooltip", StringComparison.InvariantCultureIgnoreCase)
                    || param.Equals("dateTime", StringComparison.InvariantCultureIgnoreCase)
                    )
                {
                    mask = "yyyy-MM-dd HH:mm:ss";
                }
                else if (param.Equals("time", StringComparison.InvariantCultureIgnoreCase))
                {
                    mask = "HH:mm:ss";
                }

                return date.Value.ToString(mask);
            }
            else
            {
                return "";
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null)
                return null;

            var dateTime = value as DateTime?;

            if (dateTime == null)
                return null;

            var convertedDate = dateTime.Value.ToUniversalTime();

            return convertedDate;
        }
    }
}
