﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Globalization;

namespace Finder.BloodDonation.Converters
{
    using Finder.BloodDonation.Settings;

    public class DescriptionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string text = string.Empty;
            try
            {
                var part = System.Convert.ToInt32(parameter);
                text = (string)value;
                var pos = text.IndexOf(Settings.BLOOD_LABEL_LINE_SEPARATOR);

                if (part == 1)
                {
                    text = (pos == -1) ? text : text.Substring(0, pos);
                }
                else
                {
                    text = (pos == -1) ? string.Empty : text.Substring(pos + 1).Replace(Settings.BLOOD_LABEL_LINE_SEPARATOR, Environment.NewLine);
                }
            }
            catch (Exception ex)
            {

            }

            return text;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
