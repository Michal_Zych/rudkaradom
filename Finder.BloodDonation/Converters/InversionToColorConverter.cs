﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Media;

namespace Finder.BloodDonation.Converters
{
    public class InversionToColorConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int inversion;
            int negative;
            var brush = new SolidColorBrush(Colors.White);  
            try
            {
                inversion = System.Convert.ToInt32(value);
                negative = System.Convert.ToInt32(parameter);

                if (inversion == negative)
                    brush = new SolidColorBrush(Colors.White);
                else
                    brush = new SolidColorBrush(Colors.Black);
            }
            catch (Exception ex)
            {
            }

            return brush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
