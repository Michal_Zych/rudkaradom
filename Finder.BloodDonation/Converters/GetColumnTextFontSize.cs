﻿using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Converters
{
    public class GetColumnTextFontSize : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var colName = (string)parameter;
            if (colName == null) return 14;

            var item = value as PatientDTO;
            if (item != null)
            {
                if (colName.Equals("RoomName", StringComparison.InvariantCultureIgnoreCase)) return 10;
            }

            return 14;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
