﻿using Finder.BloodDonation.Layout;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;


namespace Finder.BloodDonation.Converters
{
    public class OldDataVisibilityConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var nullableState = value as UnitColorState?;
            UnitColorState state;
            if (nullableState == null || !nullableState.HasValue)
            {
                state = UnitColorState.Normal;
            }
            else
            {
                state = nullableState.Value;
            }

            if(state == UnitColorState.IsOld)
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
