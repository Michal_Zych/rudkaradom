﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Model.Dto;
using System.Windows.Data;
namespace Finder.BloodDonation.Converters
{
    public static class  MessageType 
    {
        public const byte Instant = 0;
        public const byte AfterLogon = 1;
        public static string[] Name = { "Natychmiastowy", "Po zalogowaniu" };

    }

    public class MessageTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var v = value as byte?;

            if (v.HasValue)
                return MessageType.Name[v.Value];
        
            return "???";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
