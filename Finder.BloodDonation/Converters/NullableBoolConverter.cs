﻿using Finder.BloodDonation.Filters;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;


namespace Finder.BloodDonation.Converters
{
    public class NullableBoolConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var b = (bool?)value;

            if (!b.HasValue) return 0;
            if (b.Value) return 2;
            else return 1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            switch((int)value)
            {
                case 1: return false;
                case 2: return true;
            }
            return null;
        }
    }
}
