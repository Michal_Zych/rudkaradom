﻿using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Filters;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;


namespace Finder.BloodDonation.Converters
{
    public class GetFilterDescription : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var dataTable = value as IDataTable;
            if (dataTable != null)
            {
                string propertyName = (string)parameter;

                var column = dataTable[propertyName];

                return column.Label
                        + Finder.BloodDonation.Settings.Settings.BLOOD_LABEL_LINE_SEPARATOR
                        + column.DataType.GetText();
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
