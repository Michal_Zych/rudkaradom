﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Model.Dto;

namespace Finder.BloodDonation.Converters
{
    public class LocalizationAlarmColorConverter : System.Windows.Data.IValueConverter
    {
        public static Brush AlarmBrush = new SolidColorBrush(Colors.Red);
        public static Brush WarningBrush = new SolidColorBrush(Colors.Orange);
        public static Brush NormalBrush = new SolidColorBrush(Colors.Black);

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                int severity = 0;
                try
                {
                    severity = (int)value;
                }                    
                catch{}

                if (severity == 1) return WarningBrush;
                if (severity == 2) return AlarmBrush;
            }

            return NormalBrush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
