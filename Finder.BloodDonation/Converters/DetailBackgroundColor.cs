﻿using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Tabs.PatientTests;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Converters
{
    public class DetailBackgroundColor : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {

            var patient = value as PatientTestItemViewModel;
            if (patient != null)
            {
                if (patient.Item.ID % 10 == 0)
                {
                    return new SolidColorBrush(Color.FromArgb(50, 255, 0, 0));
                }
                if (patient.Item.ID % 5 == 0)
                {
                    return new SolidColorBrush(Color.FromArgb(50, 255, 255, 0));
                }
            }


            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
