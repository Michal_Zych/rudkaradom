﻿using Finder.BloodDonation.Controls;
using Finder.BloodDonation.DBBuffers;
using Finder.BloodDonation.Tabs.UnitsList;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Converters
{
    public class MonitorableObjectConnectionStatusConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Finder.BloodDonation.Tabs.UnitsList.Object mo = value as Finder.BloodDonation.Tabs.UnitsList.Object;
            IList<StatusImage> statusImages = new List<StatusImage>();

            if(mo != null)
            {
                if(mo.CurrentUnitId != null)
                {
                    return statusImages;
                }
                else
                {
                    if(mo.PreviousUnitId != null)
                    {
                        StatusImage si = new StatusImage();
                        si.FileName = "disconectedObject.png";
                        si.DisplayColor = BitmapMode.Red;
                        si.Image = BitmapBuffer.GetIcon("disconectedObject", ImagesFolder.UnitIconsStatus);
                       
                        statusImages.Add(si);
                    }
                }
            }

            return statusImages;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
