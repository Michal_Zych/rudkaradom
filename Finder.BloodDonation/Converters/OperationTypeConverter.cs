﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Model.Dto;
using System.Windows.Data;
namespace Finder.BloodDonation.Converters
{
    public class OperationTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var v = value as byte?;

            if(v.HasValue)
            switch (v.Value)
            {
                case 0:
                    return "Usunięcie";
                case 1:
                    return "Utworzenie";
                case 2:
                    return "Edycja";
            }
            return "???";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
