﻿using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Filters;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;


namespace Finder.BloodDonation.Converters
{
    public class GetColumnAlignment : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is ObservableCollection<IDataTableColumn>)
            {
                var columns = value as ObservableCollection<IDataTableColumn>;

                string propertyName = (string)parameter;
                foreach (var column in columns)
                {
                    if (column.Name.Equals(propertyName, StringComparison.InvariantCultureIgnoreCase))
                        return column.Alignment;
                }
            }


            var dataTable = value as IDataTable;
            if (dataTable != null)
            {
                string propertyName = (string)parameter;
                var i = dataTable[propertyName].Alignment;
                return dataTable[propertyName].Alignment;
            }

            return 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
