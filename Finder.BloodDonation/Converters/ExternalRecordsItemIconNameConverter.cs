﻿using Finder.BloodDonation.Model.Dto;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Converters
{
    public class ExternalRecordsItemIconNameConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ExternalRecordsInDto item = value as ExternalRecordsInDto;

            if (item != null)
            {
                if(item.Type.Equals("Z"))
                {
                    return "eventIcon.png";
                }
                else if(item.Type.Equals("O"))
                {
                    return "receptionIcon.png";
                }
            }

            ExternalRecordsOutDto itemOut = value as ExternalRecordsOutDto;

            if(itemOut != null)
            {
                if(itemOut.EventId == 1)
                {
                    return "requestIcon.png";
                }
                else
                {
                    return "requestIconForEvents.png";
                }
            }

            return "emptyImage.png";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    
}
