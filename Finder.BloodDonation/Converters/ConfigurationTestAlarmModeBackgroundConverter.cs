﻿using System;
using System.Globalization;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Converters
{
    public class ConfigurationTestAlarmModeBackgroundConverter : IValueConverter
    {
        private static Brush brushAlarm = new SolidColorBrush(Colors.Yellow);
        private static Brush brushGrey = new SolidColorBrush(Colors.LightGray);

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var testAlarmMode = (bool)value;
            return testAlarmMode ? brushAlarm : brushGrey;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return brushGrey;
        }
    }
}
