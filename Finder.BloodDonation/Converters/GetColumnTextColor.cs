﻿using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;


namespace Finder.BloodDonation.Converters
{
    public class GetColumnTextColor : IValueConverter
    {
        private static SolidColorBrush black = new SolidColorBrush(Colors.Black);
        private static SolidColorBrush red = new SolidColorBrush(Colors.Red);
        private static SolidColorBrush blue = new SolidColorBrush(Colors.Blue);

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var colName = (string)parameter;
            if (colName == null) return black;

            var item = value as PatientDTO;
            if (item != null)
            {
                if (colName.Equals("ID", StringComparison.InvariantCultureIgnoreCase)) return red;
                if (colName.Equals("LastName", StringComparison.InvariantCultureIgnoreCase)) return blue;
            }

            return black;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
