﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Helpers;

namespace Finder.BloodDonation.Converters
{
    public class AlarmRangeValueConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                AlarmDto ad = value as AlarmDto;
                if(ad.IsBitSensor)
                {
                    return ad.HiStateDesc + Environment.NewLine + ad.LoStateDesc;
                    return "< " + ad.LoStateDesc + " , " + ad.HiStateDesc + " >";
                }
                return ad.LoRange.ToDisplayString() + ad.MU + " ÷ " + ad.UpRange.ToDisplayString() + ad.MU;
            }
            catch (Exception)
            {

            }

            return "@#$%";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
