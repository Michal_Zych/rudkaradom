﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Converters
{
    /// <summary>
    /// Konwertuje obecnośc alarmu na widocznośc ikony.
    /// </summary>
    public class VisibilityConverter : System.Windows.Data.IValueConverter
    {       
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
                if (value != null)
                {
                    bool b = System.Convert.ToBoolean(value);
                    if (b)
                    {
                        return Visibility.Visible;
                    }
                    else
                    {
                        return Visibility.Collapsed;
                    }
                }

                return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
