﻿
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Globalization;

namespace Finder.BloodDonation.Converters
{
    using Finder.BloodDonation.Settings;

    public class LineSeparatedTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string text = string.Empty;
            try
            {
                text = value.ToString();
                text = text.Replace(Settings.BLOOD_LABEL_LINE_SEPARATOR, Environment.NewLine);
            }
            catch (Exception e)
            {
            }
            return text;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
