﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Model.Dto;

namespace Finder.BloodDonation.Converters
{
    public class AlarmT1FontColorConverter : System.Windows.Data.IValueConverter
    {
        private static Brush brush = new SolidColorBrush(Colors.Red);
        private static Brush brush2 = new SolidColorBrush(Color.FromArgb(255, 0, 255, 0));

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value != null)
                {
                    CurrentDataDto b = value as CurrentDataDto;
                    if (b != null && (b.Temp1 < b.A1L || b.Temp1 > b.A1H))
                    {
                        return brush;
                    }
                    else
                    {
                        return brush2;
                    }
                }
            }
            catch (Exception)
            {
            }

            return brush2;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
