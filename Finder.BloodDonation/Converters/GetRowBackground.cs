﻿using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Converters
{
    public class GetRowBackground : IValueConverter
    {
        private static SolidColorBrush none = new SolidColorBrush(Colors.Black);

        private static SolidColorBrush black = new SolidColorBrush(Colors.Black);
        private static SolidColorBrush red = new SolidColorBrush(Colors.Red);
        private static SolidColorBrush blue = new SolidColorBrush(Colors.Blue);

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var item = value as PatientDTO;
            if (item != null)
            {
                if (item.RoomId == null)
                {
                    return new SolidColorBrush(Color.FromArgb(20, 220, 0, 0));
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
