﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Converters
{
    public class Background2Converter : System.Windows.Data.IValueConverter
    {
        private static Brush brush = new SolidColorBrush(Colors.Red);
        private static Brush brush_normal = new SolidColorBrush(Color.FromArgb(151, 8, 70, 118));

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value != null)
                {
                    bool b = System.Convert.ToBoolean(value);
                    if (b)
                    {
                        return brush;
                    }
                }
            }
            catch (Exception)
            {
            }

            return brush_normal;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
