﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace Finder.BloodDonation.Converters
{
    [ContentProperty("TemplateMap")]
    public class TemplateSelectorConverter : IValueConverter
    {
        public TemplateMap TemplateMap { get; set; }

        public TemplateSelectorConverter()
        {
            TemplateMap = new TemplateMap();
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                DataTemplate dt = TemplateMap.Where(t => t.DataItemIdentityString == value.ToString()).Select(t => t.DataTemplate).FirstOrDefault();
                if (dt != null)
                {
                    return dt;
                }
                else
                {
                    return TemplateMap.Where(t => t.DataItemIdentityString == "PART_DEFAULT").Select(t => t.DataTemplate).FirstOrDefault();
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class TemplateMap : List<TemplateMapEntry>
    {
    }

    public class TemplateMapEntry
    {
        public string DataItemIdentityString { get; set; }
        public DataTemplate DataTemplate { get; set; }
    }
}