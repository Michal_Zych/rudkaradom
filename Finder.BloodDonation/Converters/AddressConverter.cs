﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Converters
{
    public class AddressConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                int adres = 0;
                if (int.TryParse(value.ToString(), out adres))
                {
                    try
                    {
                        return string.Format("0x{0}", string.Format("{0:x2}", adres).ToUpper());
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                try
                {
                    return System.Convert.ToInt32(value.ToString(), 16);
                }
                catch (Exception)
                {
                }
            }

            return null;
        }
    }
}
