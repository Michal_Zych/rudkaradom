﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Reflection;

namespace Finder.BloodDonation.Converters
{
	public class LocalizedConverter : IValueConverter
	{
		private ResourceDictionary _resourceDictionary;
		public ResourceDictionary ResourceDictionary
		{
			get { return _resourceDictionary; }
			set
			{
				_resourceDictionary = value;
			}
		}

		public LocalizedConverter()
		{
			//_resourceDictionary = new ResourceDictionary("Finder.BloodDonation.Resources.RfidViews", Assembly.GetExecutingAssembly());
		}

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			//do your own thing using the _dict
			//var person = value as Person
			//if (person.Status == "Awesome")
			//    return _resourceDictionary["AwesomeBrush"]
			//else
			//    return _resourceDictionary["NotAwesomeBrush"];
			var localized = value as string;

			if (!String.IsNullOrEmpty(localized))
			{
				return _resourceDictionary[localized];
			}
			else
			{
				return "niezdefiniowany";
			}

		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
