﻿using Finder.BloodDonation.DBBuffers;
using Finder.BloodDonation.Model.Dto;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Converters
{
    public class TransmitterStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            TransmitterDetailsDto transmitterDetailsDto = value as TransmitterDetailsDto;

            if(transmitterDetailsDto != null)
            {
                if (transmitterDetailsDto.IsConnected)
                    return BitmapMode.Green;
                else
                    return BitmapMode.Red;
            }
            else
            {
                return BitmapMode.Normal;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
