﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Common.Authentication;

namespace Finder.BloodDonation.Converters
{
    public static class AlarmEventDescriptor
    {
        public const byte VALUE_TO_LOW = 1;
        public const byte VALUE_TO_HIGH = 2;
        public const byte NO_SENSOR = 3;
        public const byte NO_DEVICE = 4;
        public const byte NO_HUB = 5;

        public static string GetDescription(AlarmDto ad)
        {
            if (ad == null) return "null alarm";

            string desc = "niezidentyfikowany";

            byte eventType = ad.EventType;

            if (!BloodyUser.Current.CanSeeAllAlarms)
            {
                if (eventType > VALUE_TO_HIGH) eventType = NO_SENSOR;
            }

            switch (eventType)
            {
                case VALUE_TO_LOW:
                    if (ad.IsBitSensor)
                    {
                        desc = "Stan alarmowy";// +ad.LoStateDesc;
                    }
                    else
                    {
                        desc = "Przekroczony dolny zakres";
                    }
                    break;
                case VALUE_TO_HIGH:
                    if (ad.IsBitSensor)
                    {
                        desc = "Stan alarmowy";// +ad.HiStateDesc;
                    }
                    else
                    {
                        desc = "Przekroczony górny zakres";
                    }
                    break;
                case NO_SENSOR:
                    desc = "Brak łączności z czujnikiem";
                    break;
                case NO_DEVICE:
                    desc = "Brak łączności z urządzeniem";
                    break;
                case NO_HUB:
                    desc = "Brak łączności z koncentratorem";
                    break;

            }

            return desc;
        }

        internal static string GetShortDescription(byte eventType)
        {

            if (!BloodyUser.Current.CanSeeAllAlarms)
            {
                return "Err";
            }

            string desc = "OK";
            switch (eventType)
            {
                case NO_SENSOR:
                    desc = "Err S";
                    break;
                case NO_DEVICE:
                    desc = "Err D";
                    break;
                case NO_HUB:
                    desc = "Err H";
                    break;

            }

            return desc;
        }
    }
}
