﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Helpers;
namespace Finder.BloodDonation.Converters
{
    public class BitSensorAlarmingConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return 0.0;
            double result = 0d;
            try
            {
                var sensor = value as UnitPreviewDto;
                if (sensor != null)
                {
                    if (sensor.MeasureUnit == "0")
                    {
                        //z obrotem 
                        result = 180;
                    }
                }
            }

            catch
            { }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
