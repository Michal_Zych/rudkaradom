﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace Finder.BloodDonation.Converters
{
	public class PrepTypeNameConverter : IValueConverter
	{

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{

			if (value == null)
				return "-";

			switch (value as string)
			{
				case "blood": return Resources.TypeNames.blood;
				case "plasma": return Resources.TypeNames.plasma;
				case "platelets": return Resources.TypeNames.platelets;
				case "blood_derivs": return Resources.TypeNames.blood_derivs;
				case "reagents": return Resources.TypeNames.reagents;
                case "catalyzer": return Resources.TypeNames.catalyzer;
                case "inhibitor": return Resources.TypeNames.inhibitor;
				case "_0_plus": return Resources.TypeNames._0_plus;
				case "_0_minus": return Resources.TypeNames._0_minus;
				case "_a_minus": return Resources.TypeNames._a_minus;
				case "_a_plus": return Resources.TypeNames._a_plus;
				case "_b_minus": return Resources.TypeNames._b_minus;
				case "_b_plus": return Resources.TypeNames._b_plus;
				case "_ab_minus": return Resources.TypeNames._ab_minus;
				case "_ab_plus": return Resources.TypeNames._ab_plus;
				case "_plasma_a": return Resources.TypeNames._plasma_a;
				case "_plasma_b": return Resources.TypeNames._plasma_b;
				case "_plasma_ab": return Resources.TypeNames._plasma_ab;
				case "_plasma_0": return Resources.TypeNames._plasma_0;
				default: return "-";
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
