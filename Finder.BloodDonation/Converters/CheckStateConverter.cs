﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Converters
{
    public class CheckStateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var result = (bool?)value;

            if (!result.HasValue)
                return ToggleState.Indeterminate;
            else if (result.Value)
                return ToggleState.On;
            else
                return ToggleState.Off;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ToggleState state = (ToggleState)value;
            var toReturn = state == ToggleState.Off ? false : state == ToggleState.On ? true : default(bool?);
            return toReturn;
        }
    }
}
