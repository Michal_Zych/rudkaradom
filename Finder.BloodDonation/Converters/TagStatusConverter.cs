﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Globalization;

namespace Finder.BloodDonation.Converters
{
    public class TagStatusConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var imagePath = "../../Images/Pojemnik_null.png";
            try
            {
                var statusId = (int)value;
                switch (statusId)
                {
                    case 1: imagePath = "../../Images/Pojemnik_green.png"; break;
                    case 2: imagePath = "../../Images/Pojemnik_red.png"; break;
                    case 3: imagePath = "../../Images/Pojemnik_tag.png"; break;
                }
            }
            catch (Exception ex)
            {

            }

            return imagePath;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
