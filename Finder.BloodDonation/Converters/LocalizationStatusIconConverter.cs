﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.DBBuffers;
using System.Linq;

namespace Finder.BloodDonation.Converters
{
    public class LocalizationStatusIconConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ImageBrush result = null;
            if(value is PatientTestDto)
            {
                var patient = value as PatientTestDto;

                string name;
                if(patient.Id > 10)
                {
                    name = "kula";
                }
                else
                {
                    name = "ludzik";
                }

                result = BitmapBuffer.Get(name, (BitmapMode)(patient.Id % (1 + (int)Enum.GetValues(typeof(BitmapMode)).Cast<BitmapMode>().Last())));

                if (result != null)
                {
                    result.Stretch = Stretch.Uniform;
                }
                return result;
            }
            return null;
       }


        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
