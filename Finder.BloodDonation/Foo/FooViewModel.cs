﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Layout;

namespace Finder.BloodDonation.Foo
{
	public class FooViewModel : ViewModelBase, ITabViewModel
	{
		[RaisePropertyChanged]
		public string BiText { get; set; }

		public FooViewModel(IUnityContainer container)
			: base(container)
		{
			this.ViewAttached += new EventHandler(FooViewModel_ViewAttached);
		}

		void FooViewModel_ViewAttached(object sender, EventArgs e)
		{
			_state = ViewModelState.Loaded;
			LoadingCompleted(this, EventArgs.Empty);
		}

		#region ITabViewModel Members

		public void Refresh(IUnit unit)
		{
			BiText = unit.Identity.ToString();
		}

		#endregion

		#region ITabViewModel Members


		public event EventHandler LoadingCompleted = delegate { };

		#endregion

		#region ITabViewModel Members


		public object TabView
		{
			get { return this.View; }
		}

		#endregion

		#region ITabViewModel Members

		private ViewModelState _state = ViewModelState.Normal;
		public ViewModelState State
		{
			get { return _state; }
		}

		#endregion


        public string Title
        {
            get { return "Foo"; }
        }

        public bool ExitButtonVisible
        {
            get { return true; }
        }
    }
}
