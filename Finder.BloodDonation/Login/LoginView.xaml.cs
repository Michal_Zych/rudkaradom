﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Composite.Presentation.Commands;
using FinderFX.SL.Core.Authentication;
using System.Windows.Data;
using System.Windows.Browser;
using Finder.BloodDonation.Common.Events;
using System.Globalization;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.UsersService;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Model;
using System.Reflection;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.DBBuffers;
using Finder.BloodDonation.Tabs.MenuAdministration.SettingsSystem;

namespace Finder.BloodDonation.Login
{

    public partial class LoginView : UserControl, ILoggingControl
    {
        private const bool DEFAULT_MONITORING_STATE = true;


        private IDynamicEventAggregator _eventAggregator;

        private bool _loggedIn = false;

        public string LoginName { get; set; }

        public string Password { get; set; }

        public bool Monitoring { get; set; }

        public bool IsBusy { get; set; }

        public DelegateCommand<object> LoginCommand { get; set; }

        public event EventHandler<CredentialsEventArgs> TryToAuthenticate;

        //R.Sz. Nowe właściwości które moga być różne dla różnychh klientów
        public string Client { get; private set; }
        public string BackgroundImage { get; set; }
        public string LogoImage { get; set; }

        public LoginView()
        {
            //BitmapBuffer.LoadAllIcons();
            IsBusy = false;
            InitializeComponent();
            LoginCommand = new DelegateCommand<object>(LoginCommand_Command);
            //User.CurrentUserChanged += new EventHandler(User_CurrentUserChanged);

            this.DataContext = this;

            _eventAggregator = Bootstrapper.StaticContainer.Resolve<IDynamicEventAggregator>();
			_eventAggregator.GetEvent<TagInfoReceivedEvent>().Subscribe(HandleStartAndTagLoginInfo);

            // for +autologin
            this.Loaded += new RoutedEventHandler(LoginView_Loaded);

            //R.Sz. Pobranie numeru klienta z jakim była uruchomiona strona
            Client = App.Current.Resources[ConfigurationsKeys.CLIENT_PARAM_NAME].ToString();
            
            //BackgroundImage = string.Format(@"/Finder.BloodDonation;component/Images/bkg{0}.jpg", Client);
            BackgroundImage = @"/Finder.BloodDonation;component/Images/bkg.jpg";
            if(Client == "4")//m2mTeam
                BackgroundImage = string.Format(@"/Finder.BloodDonation;component/Images/3a{0}.jpg", Client);
            LogoImage = string.Format(@"/Finder.BloodDonation;component/Images/Logo{0}.png", Client);
            //R.Sz. end

            //this.DataContext = this;

            lVersion.Text = "Wersja " + ConfigurationsKeys.APP_VERSION;

            
                //ping_story_Completed(null, null);
                Monitoring = DEFAULT_MONITORING_STATE;         
        }

        void User_CurrentUserChanged(object sender, EventArgs e)
        {
            IsBusy = false;
            loginTextBox.Text = GetCookie(ConfigurationsKeys.COOKIE_NAME);
        }

        public void AuthenticationFailed()
        {
            IsBusy = false;
            bLoginFailed.Visibility = System.Windows.Visibility.Visible;
        }

        public void ClearCredentials()
        {
            passwordLoginBox.Password = string.Empty;
            loginTextBox.Text = string.Empty;
            string last_login = GetCookie(ConfigurationsKeys.COOKIE_NAME);
            if (last_login != null)
            {
                loginTextBox.Text = last_login;
                LoginName = last_login;
            }
            monitoringCheckBox.IsChecked = DEFAULT_MONITORING_STATE;
        }

        public void SetCookie(string key, string value)
        {
            DateTime expireDate = DateTime.Now.Add(TimeSpan.FromMinutes(600));
            string newCookie = String.Format("{0}={1};expires={2}", key, value, expireDate.ToString("R"));

            HtmlPage.Document.SetProperty("cookie", newCookie);
        }

        private string GetCookie(string key)
        {
            string[] cookies = HtmlPage.Document.Cookies.Split(';');

            foreach (string cookie in cookies)
            {
                string[] keyValue = cookie.Split('=');
                if (keyValue.Length == 2)
                {
                    if (keyValue[0].ToString() == key)
                        return keyValue[1];
                }
            }
            return null;
        }

        private string GetCodedLogin(string login)
        {
            return LoginClientCoder.EncodeLoginClient(login, Client);
        }

        public void LoginCommand_Command(object o)
        {
            SetCookie(ConfigurationsKeys.COOKIE_NAME, LoginName);

            Password = passwordLoginBox.Password;

            IsBusy = true;
            Bootstrapper.monitoring = Monitoring;
            //Bootstrapper.phone = Phone;
            bLoginFailed.Visibility = System.Windows.Visibility.Collapsed;
            string g = HashHelper.CalculateHash(Password);
            TryToAuthenticate(this, new CredentialsEventArgs()
            {
                Login = GetCodedLogin(LoginName),
                PasswordHash = HashHelper.CalculateHash(Password)
            });
        }

        private void passwordLoginBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                LoginCommand_Command(null);
                e.Handled = true;
            }
        }

        private void LoginView_Loaded(object sender, RoutedEventArgs e)
        {
            /* // autologin
            TryToAuthenticate(this, new CredentialsEventArgs()
            {
                Login = "ktolkacz",
                PasswordHash = HashHelper.CalculateHash("q1")
            });
            // */

            this.rfidReaderStatus.Text = String.Empty;

            // try to enable rfid reader
            _eventAggregator.GetEvent<RfidTransmissionStateChangedEvent>().Publish(true);
        }

        public void HandleStartAndTagLogin(string result)
        {
            System.Diagnostics.Debug.WriteLine("handling tag login info: " + result);

            if (result.StartsWith("Started"))
                this.rfidReaderStatus.Text = "aktywny";

            if (result.StartsWith("TagLogin"))
            {
                var parts = result.Split(";".ToCharArray());

                var uid = unchecked((long)Convert.ToUInt64(parts[1], CultureInfo.InvariantCulture));

                var loginDetails = parts[2].Split("|".ToCharArray());
                var userId = Convert.ToInt32(loginDetails[0], CultureInfo.InvariantCulture);
                var login = loginDetails[1];

                this.rfidReaderStatus.Text = "logowanie (" + login + ")";

                TryToAuthenticate(this, new CredentialsEventArgs()
                {
                    Login = GetCodedLogin(login),
                    PasswordHash = HashHelper.CalculateHash(uid.ToString())
                });
            }
        }

        public void HandleStartAndTagLoginInfo(RfidInfo info)
        {
            if (User.Current.Identity.IsAuthenticated)
            {
                return;
            }

            this.rfidReaderStatus.Text = "Oczekiwanie na kartę lub hasło";

            if (info.Type.Equals(Finder.BloodDonation.JsInterop.Bridge.TagTypeLogin))
            {
                this.rfidReaderStatus.Text = "Weryfikacja użytkownika (" + info.Content + ")...";
                System.Diagnostics.Debug.WriteLine("handling tag login info: {0}, state: {1}", info.Content, User.Current.Identity.IsAuthenticated);

                var uid = info.Uid;

                TryToAuthenticate(this, new CredentialsEventArgs()
                {
                    Login = GetCodedLogin(info.Content),
                    PasswordHash = HashHelper.CalculateHash(uid.ToString())
                });

                //this.rfidReaderStatus.Text = "Logowanie (" + info.Content + ")";

            }
        }

        public void AuthenticationSuccess()
        {
            System.Diagnostics.Debug.WriteLine("log in successful: {0}", User.Current.Identity.IsAuthenticated);
            _loggedIn = true;
            this.rfidReaderStatus.Text = "Weryfikacja zakończona pomyślnie";

        }




    }
}
