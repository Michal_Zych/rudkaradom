﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Input;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.UsersService;
using FinderFX.SL.Core.Authentication;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using System.Windows.Threading;
using Telerik.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Tabs.MenuAdministration.SettingsSystem;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Success;


namespace Finder.BloodDonation.Login
{
    public partial class LoginStatus : UserControl
    {
        enum EditedField { Phone, EMail };
        private EditedField _edited;

        public DispatcherTimer SMNetworkTimer = null;
        public Nullable<DateTime> AppTimer;
        public TimeSpan SessionDuration;
        public Style DefaultProgressbarStyle = null;

        private IWindowsManager _windowsManager;

        [RaisePropertyChanged]
        public string MonitoringStatusImage { get; set; }

        public LoginStatus()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(LoginStatus_Loaded);

            IDynamicEventAggregator EventAggregator = Bootstrapper.StaticContainer.Resolve<IDynamicEventAggregator>();
            EventAggregator.GetEvent<PasswordChanged>().Subscribe(PasswordChanged_Event);
        }

        public void PasswordChanged_Event(bool success)
        {
            //MessageBox.Show("Event password changed");
            Logout(true);
        }

        void SMNetworkTimer_Tick(object sender, EventArgs e)
        {
            var time = System.DateTime.Now - AppTimer.Value;
            if (time > SessionDuration)
            {
                this.Dispatcher.BeginInvoke(() =>
                {
                    this.SMNetworkTimer.Stop();
                    Application.Current.RootVisual.MouseMove += RootVisual_MouseMove;
                });
            }
            else
            {
                SetTimeOfEndSession(SessionDuration - time);
            }
        }

        void RootVisual_MouseMove(object sender, MouseEventArgs mouseEventArgs)
        {
            RadWindow.Alert("Koniec sesji", (s, ea) =>
            {
                Logout(false);
            });
            Application.Current.RootVisual.MouseMove -= RootVisual_MouseMove;
        }

        private void SetTimeOfEndSession(TimeSpan timeSpan)
        {
            ProgressBar.Value = ProgressBar.Maximum - timeSpan.Ticks;
            //this.TimeToEndSession.Text = timeSpan.ToString(@"mm\:ss");
        }

        private IDynamicEventAggregator EventAggregator;

        private void LoginStatus_Loaded(object sender, RoutedEventArgs e)
        {
            SetDisplayCurrentCredentials();
            User.CurrentUserChanged += new EventHandler(User_CurrentUserChanged);
            VisualStateManager.GoToState(this, "Read", false);
            VisualStateManager.GoToState(this, "Read1", false);

            txtListAlarms.Click += new RoutedEventHandler(txtListAlarms_Click);
            txtDisplayName.Click += new RoutedEventHandler(txtDisplayName_Click);
            txtPhone.Click += new RoutedEventHandler(txtPhone_Click);
            txtEMail.Click += new RoutedEventHandler(txtEMail_Click);
            EventAggregator = Bootstrapper.StaticContainer.Resolve<IDynamicEventAggregator>();
            EventAggregator.GetEvent<AlarmsNewReceived>().Subscribe(AlarmsNewReceived_Event);
            EventAggregator.GetEvent<ExternalLogoutEvent>().Subscribe(HandleExternalLogout);
            EventAggregator.GetEvent<RestartLogoutTimer>().Subscribe(SetAppTimer);
            EventAggregator.GetEvent<LocalizationAlarmsReceived>().Subscribe(LocalizationAlarmsReceived_Event);
            Bootstrapper.StaticContainer.Resolve<ISettingsRespository>().Load();
            
            var viewFactory = Bootstrapper.StaticContainer.Resolve<ViewFactory>();
            _windowsManager = Bootstrapper.StaticContainer.Resolve<IWindowsManager>();
            _windowsManager.RegisterWindowIfNotRegister(WindowsKeys.SuccessDialog, "Powodzenie"
                , viewFactory.CreateView<Success>(), new Size(440, 170), true);

            var config = new SessionDurationConfigurator();
            SessionDuration = config.GetSessionDuration();

            this.ProgressBar.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void txtDisplayName_Click(object sender, RoutedEventArgs e)
        {
            if (BloodyUser.Current.ID != ConfigurationKeys.SYSTEM_ADMIN_ID)
                EventAggregator.GetEvent<ShowWindow>().Publish("passwd");
        }

        public void SetAppTimer(int i)
        {
            this.AppTimer = DateTime.Now;
        }

        private void btnZapiszHaslo_Click(object sender, RoutedEventArgs e)
        {

        }

        private void proxy_ChangePasswordCompleted(object sender, ChangePasswordCompletedEventArgs e)
        {
            if (e.Result)
            {
                VisualStateManager.GoToState(this, "Read1", false);
            }
        }

        private void btnAnulujHaslo_Click(object sender, RoutedEventArgs e)
        {
            VisualStateManager.GoToState(this, "Read1", false);
        }


        private void txtListAlarms_Click(object sender, RoutedEventArgs e)
        {
            //EventAggregator.GetEvent<ShowWindow>().Publish("alarnew");
            EventAggregator.GetEvent<ShowWindow>().Publish(WindowsKeys.LocalizationAlarmsPopup);
        }

        public void AlarmsNewReceived_Event(List<AlarmDto> a)
        {
            return;
            txtListAlarms.Content = 0;
            if (a != null)
            {
                txtListAlarms.Content = a.Count.ToString();
            }
        }


        public void LocalizationAlarmsReceived_Event(List<LocalizationAlarmDto> a)
        {
            txtListAlarms.Content = 0;
            if (a != null)
            {
                txtListAlarms.Content = a.Count.ToString();
            }
        }

        public void HandleExternalLogout(int result)
        {
            Logout(true);
        }

        private void SetDisplayCurrentCredentials()
        {
            txtPhone.Content = "[Brak telefonu]";
            txtDisplayName.Content = "[Niezalogowany]";

            if (User.Current.Identity.IsAuthenticated)
            {
                Configuration.Current.Reload_(GetAccount);
            }
        }

        private void GetAccount()
        {
            UsersServiceClient proxy = ServiceFactory.GetService<UsersServiceClient>(ServicesUri.UsersService);
            proxy.GetAccountCompleted += new EventHandler<GetAccountCompletedEventArgs>(proxy_GetAccountCompleted);
            proxy.GetAccountAsync(User.Current.ID);
        }

        private void User_CurrentUserChanged(object sender, EventArgs e)
        {
            SetDisplayCurrentCredentials();
        }

        private void SetMonitoringStatus()
        {
            if (Bootstrapper.UserMonitoringState)
            {
                //imgStatus.Source = new BitmapImage(new Uri("/Finder.BloodDonation;component/Images/Ico-monstatON-mono.png", UriKind.Relative));
            }
            else
            {
                //imgStatus.Source = new BitmapImage(new Uri("/Finder.BloodDonation;component/Images/Ico-monstatOFF-mono.png", UriKind.Relative));
            }

        }

        private void proxy_GetAccountCompleted(object sender, GetAccountCompletedEventArgs e)
        {
            try
            {
                if (e.Result != null)
                {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        txtDisplayName.Content = e.Result.Name + " " + e.Result.LastName;
                        txtPhone.Content = e.Result.Phone;
                        txtEMail.Content = e.Result.EMail;
                        if (BloodyUser.Current.IsInRole(PermissionNames.HyperAdmin))
                        {
                            txtDisplayName.Foreground = new SolidColorBrush(Colors.Red);
                        }
                        SetMonitoringStatus();
                    });

                    var days = Configuration.Current.GetInt(ConfigurationKeys.MaxPasswordAgeDays);

                    if ((e.Result.ID != ConfigurationKeys.SYSTEM_ADMIN_ID) &&
                        (e.Result.PasswordChangeDate.AddDays(Bootstrapper.StaticContainer.Resolve<ISettingsRespository>().Settings.MaxPasswordAgeDays) < DateTime.Now))
                    {
                        MsgBox.Warning("Upłynął termin ważności hasła. Proszę zmienić hasło.");
                    }

                    if (Configuration.Current.GetInt(ConfigurationKeys.EnableMonitoringStatusButton) == ConfigurationKeys.ENABLED)
                    {
                        //icoMonitoringStatus.Cursor = Cursors.Hand;
                    }
                    else
                    {
                        //icoMonitoringStatus.Cursor = Cursors.Arrow;
                    }

                    UsersServiceClient proxy = ServiceFactory.GetService<UsersServiceClient>(ServicesUri.UsersService);
                    proxy.GetAfterLoginMessagesCompleted += new EventHandler<GetAfterLoginMessagesCompletedEventArgs>(proxy_GetAfterLoginMessagesCompleted);
                    proxy.GetAfterLoginMessagesAsync();
                }
            }
            catch (Exception er)
            {

            }
        }

        private void proxy_GetAfterLoginMessagesCompleted(object sender, GetAfterLoginMessagesCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                foreach (var s in e.Result)
                {
                    MsgBox.Warning(s);
                }
            }
        }

        private void txtPhone_Click(object sender, RoutedEventArgs e)
        {
            string phoneNr = "";
            if (txtPhone.Content != null)
            {
                phoneNr = txtPhone.Content.ToString();
            }

            _edited = EditedField.Phone;

            VisualStateManager.GoToState(this, "Edit", false);
            editText.Text = phoneNr;
            editText.SelectAll();
        }

        private void txtEMail_Click(object sender, RoutedEventArgs e)
        {
            string eMail = "";
            if (txtPhone.Content != null)
            {
                eMail = txtEMail.Content.ToString();
            }

            _edited = EditedField.EMail;

            VisualStateManager.GoToState(this, "Edit", false);
            editText.Text = eMail;
            editText.SelectAll();
        }

        private void MonitoringStatus_Click(object sender, RoutedEventArgs e)
        {
            if (Settings.Settings.Current.GetInt(ConfigurationKeys.EnableMonitoringStatusButton) == ConfigurationKeys.ENABLED)
            {
                Bootstrapper.monitoring = !Bootstrapper.monitoring;
                SetMonitoringStatus();
            }
        }

        private void btnZapisz_Click(object sender, RoutedEventArgs e)
        {
            UsersServiceClient proxy = ServiceFactory.GetService<UsersServiceClient>(ServicesUri.UsersService);

            switch (_edited)
            {
                case EditedField.Phone:
                    proxy.ChangePhoneCompleted += new EventHandler<ChangePhoneCompletedEventArgs>(proxy_ChangePhoneCompleted);
                    proxy.ChangePhoneAsync(editText.Text);
                    break;
                case EditedField.EMail:
                    proxy.ChangeEMailCompleted += new EventHandler<ChangeEMailCompletedEventArgs>(proxy_ChangeEMailCompleted);
                    proxy.ChangeEMailAsync(editText.Text);
                    break;
            }
        }

        private void EditCompleted()
        {
            switch (_edited)
            {
                case EditedField.Phone: txtPhone.Content = editText.Text; break;
                case EditedField.EMail: txtEMail.Content = editText.Text; break;
            }
            VisualStateManager.GoToState(this, "Read", false);
        }

        private void proxy_ChangePhoneCompleted(object sender, ChangePhoneCompletedEventArgs e)
        {
            if (e.Result)
            {
                EditCompleted();
            }
        }

        private void proxy_ChangeEMailCompleted(object sender, ChangeEMailCompletedEventArgs e)
        {
            if (e.Result)
            {
                EditCompleted();
            }
        }

        private void btnAnuluj_Click(object sender, RoutedEventArgs e)
        {
            VisualStateManager.GoToState(this, "Read", false);
        }

        private void txtSave_Click(object sender, RoutedEventArgs e)
        {
            EventAggregator.GetEvent<SaveSettingsEvent>().Publish(true);

            if (!String.IsNullOrEmpty(LocalStorageSettings.MonitorableObjectScale))
                LocalStorageManager.SaveSettings("MonitorableObjectScale", LocalStorageSettings.MonitorableObjectScale);

            if (!String.IsNullOrEmpty(LocalStorageSettings.LocalizationPlanViewScale))
                LocalStorageManager.SaveSettings("LocalizationPlanViewScale", LocalStorageSettings.LocalizationPlanViewScale);

            var settings = ((BloodyUser)User.Current).SerializedSettings;
            EventAggregator.GetEvent<RfidTransmissionStateChangedEvent>().Publish(false);

            UsersServiceClient proxy = ServiceFactory.GetService<UsersServiceClient>(ServicesUri.UsersService);
            proxy.SaveSettingsAsync(settings);
            proxy.SaveSettingsCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(HandleSaveCompleted);
        }

        private void HandleSaveCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            _windowsManager.ShowWindow(WindowsKeys.SuccessDialog, "Ustawienia zostały zapisane.");
        }

        private void txtLogout_Click(object sender, RoutedEventArgs e)
        {
            Logout(false);
        }

        private void Logout(bool immediate)
        {
            if (SMNetworkTimer != null)
            {
                SMNetworkTimer.Stop();
            }
            AppTimer = null;
            if (immediate)
            {
                EventAggregator.GetEvent<RfidTransmissionStateChangedEvent>().Publish(false);

                HandleLogoutCompletion(this, null);
            }
            else
            {
                // save selected layout items
                var settings = ((BloodyUser)User.Current).SerializedSettings;

                EventAggregator.GetEvent<RfidTransmissionStateChangedEvent>().Publish(false);

                UsersServiceClient proxy = ServiceFactory.GetService<UsersServiceClient>(ServicesUri.UsersService);
                proxy.SaveSettingsAsync(settings);
                proxy.SaveSettingsCompleted += new EventHandler<System.ComponentModel.AsyncCompletedEventArgs>(HandleLogoutCompletion);
            }

        }

        private void HandleLogoutCompletion(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            User.Current.Logout();
            SetDisplayCurrentCredentials();

            HtmlPage.Window.Eval("location.reload(true);");
        }
    }
}
