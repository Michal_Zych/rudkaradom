﻿using System;
using Finder.BloodDonation.Common.Authentication;
using Microsoft.Practices.Unity;


namespace Finder.BloodDonation.Login
{
    using Finder.BloodDonation.Settings;

    class SessionDurationConfigurator : ISesionDurationConfigurator
    {
       public TimeSpan GetSessionDuration()
        {
            TimeSpan result;
            if (BloodyUser.Current.IsInRoles(Settings.Current.GetList("PermissionsForAdminSessionDuration")))
            {
                result = Settings.Current.GetBool("AdminAutoLogoff")
                         ? new TimeSpan(Settings.Current.GetInt("AdminSessionDurationHours"),
                                        Settings.Current.GetInt("AdminSessionDurationMinutes"),
                                        Settings.Current.GetInt("AdminSessionDurationSeconds"))
                         : TimeSpan.Zero;
            }
            else
            {
                result = Settings.Current.GetBool("UserAutoLogoff")
                    ? new TimeSpan(Settings.Current.GetInt("UserSessionDurationHours"),
                        Settings.Current.GetInt("UserSessionDurationMinutes"),
                        Settings.Current.GetInt("UserSessionDurationSeconds"))
                    : TimeSpan.Zero;
            }

            return result;
        }
    }
}
