﻿using System;


namespace Finder.BloodDonation.Login
{
    interface ISesionDurationConfigurator
    {
        //zwraca TimeSpan jesli nie ma być autowylogowania to TimeSpan.Zero
        TimeSpan GetSessionDuration();
    }
}
