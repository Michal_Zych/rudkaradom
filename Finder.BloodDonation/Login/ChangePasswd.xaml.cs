﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.UsersService;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.Authentication;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Tools;

namespace Finder.BloodDonation.Login
{
    [ViewModel(typeof(ChangePasswdViewModel))]
    [UIException]
    public partial class ChangePasswd : UserControl
    {
        public ChangePasswd()
        {
            InitializeComponent();

            IDynamicEventAggregator EventAggregator = Bootstrapper.StaticContainer.Resolve<IDynamicEventAggregator>();
            EventAggregator.GetEvent<ShowWindow>().Subscribe(ShowWindow_Event);
        }

        public void ShowWindow_Event(string win)
        {
            if (win == "passwd")
            {
                new_pwd_1.Password = string.Empty;
                new_pwd_2.Password = string.Empty;
                old_pwd.Password = string.Empty;
            }
        }

        private void btnZapiszHaslo_Click(object sender, RoutedEventArgs e)
        {
            if (new_pwd_1.Password.Trim().Length == 0)
            {
                MsgBox.Error("Proszę uzupełnić nowe hasło.");
            }

            if (new_pwd_1.Password == new_pwd_2.Password)
            {
                UsersServiceClient proxy = ServiceFactory.GetService<UsersServiceClient>(ServicesUri.UsersService);
                proxy.ChangePasswordCompleted += new EventHandler<ChangePasswordCompletedEventArgs>(proxy_ChangePasswordCompleted);
                proxy.ChangePasswordAsync(
                    HashHelper.CalculateHash(new_pwd_1.Password),
                    HashHelper.CalculateHash(
                        HashHelper.CalculateHash(new_pwd_1.Password),
                        HashHelper.CalculateHash(old_pwd.Password))
                );
            }
            else
            {
                MsgBox.Error("Wpisane nowe hasła nie zgadzają się ze sobą");
            }
        }

        void proxy_ChangePasswordCompleted(object sender, ChangePasswordCompletedEventArgs e)
        {
            if (e.Result)
            {
                MsgBox.Warning("Hasło zostało zmienione.");

                IDynamicEventAggregator EventAggregator = Bootstrapper.StaticContainer.Resolve<IDynamicEventAggregator>();
                EventAggregator.GetEvent<PasswordChanged>().Publish(true);
            }
            else
            {
                MsgBox.Warning("Nie udało się zmienić hasła. Najprawdopodobniej aktualne hasło jest nieprawidłowe.");
            }
        }
    }
}
