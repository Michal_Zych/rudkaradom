﻿using System;
using System.Net;
using System.Linq;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Common.Events;
using System.Collections.ObjectModel;
using System.Windows.Media.Animation;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using System.Collections.Generic;
using System.Collections;

namespace Finder.BloodDonation.Login
{
    [OnCompleted]
    public class ChangePasswdViewModel : ViewModelBase
    {
        public ChangePasswdViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {

        }
    }
}