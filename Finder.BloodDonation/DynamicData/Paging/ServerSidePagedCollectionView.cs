﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using FinderFX.SL.Core.MVVM;

namespace Finder.BloodDonation.DynamicData.Paging
{
    /// <summary>
    /// A paged collection view that uses a IPagedDataSource instance to asynchronously
    /// retrieve paged data from a server
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ServerSidePagedCollectionView<T> : ObservableCollection<T>,
      IPagedCollectionView
    {
        protected IPagedDataSource<T> _pagedDataSource;

        private int _pageSize;

        private bool _canChangedPage = true;

        private bool _isPageChanging;

        private int _itemCount;

        private int _totalItemCount;

        private int _pageIndex = 0;

        public bool CanChangePage
        {
            private set
            {
                SetField(ref _canChangedPage, value, "CanChangePage");
            }
            get
            {
                return _canChangedPage;
            }
        }

        public bool IsPageChanging
        {
            private set
            {
                SetField(ref _isPageChanging, value, "IsPageChanging");
            }
            get
            {
                return _isPageChanging;
            }
        }

        public int ItemCount
        {
            private set
            {
                SetField(ref _itemCount, value, "ItemCount");
            }
            get
            {
                return _itemCount;
            }
        }

        private int _totalPages;
        public int TotalPages
        {
            set
            {
                SetField(ref _totalPages, value, "TotalPages");
            }
            get
            {
                return _totalPages;
            }
        }

        public int PageIndex
        {
            private set
            {
                SetField(ref _pageIndex, value, "PageIndex");
            }
            get
            {
                return _pageIndex;
            }
        }

        public int PageSize
        {
            set { }
            //{
            //    System.Diagnostics.Debug.WriteLine("SSPCV setting pagesize to: {0}", value);
            //    //SetField(ref _pageSize, value, "PageSize");

            //    _pageSize = value;
            //}
            get
            {
                return _pageSize;
            }
        }

        public bool DataAvailable { get; set; }

        public int TotalItemCount
        {
            private set
            {
                SetField(ref _totalItemCount, value, "TotalItemCount");
            }
            get
            {
                return _totalItemCount;
            }
        }

        public event EventHandler<EventArgs> PageChanged;

        public event EventHandler<PageChangingEventArgs> PageChanging;

        public ServerSidePagedCollectionView(IPagedDataSource<T> pagedDataSource)
        {
            _pagedDataSource = pagedDataSource;
            RefreshData(0);
        }

        public ServerSidePagedCollectionView(IPagedDataSource<T> pagedDataSource, int pageSize)
        {
            _pageSize = pageSize;
            _pagedDataSource = pagedDataSource;
            RefreshData(0);
        }

        public bool MoveToFirstPage()
        {
            RefreshData(0);
            return true;
        }

        public bool MoveToLastPage()
        {
            RefreshData(TotalPages - 1);
            return true;
        }

        public bool MoveToNextPage()
        {
            if (PageIndex + 1 < TotalPages)
                RefreshData(PageIndex + 1);
            return true;
        }

        public bool MoveToPage(int pageIndex)
        {
            RefreshData(pageIndex);
            return true;
        }

        public bool MoveToPreviousPage()
        {
            RefreshData(PageIndex - 1);
            return true;
        }

        protected void OnPageChanged()
        {
            if (PageChanged != null)
            {
                PageChanged(this, EventArgs.Empty);
            }
        }

        protected void OnPageChanging(int newPageIndex)
        {
            if (PageChanging != null)
            {
                PageChanging(this, new PageChangingEventArgs(newPageIndex));
            }
        }

        /// <summary>
        /// Fetches the data for the given page
        /// </summary>
        public void RefreshData(int newPageIndex)
        {
            // set the pre-fetch state
            CanChangePage = false;
            OnPageChanging(newPageIndex);

            _pagedDataSource.FetchData(newPageIndex, response =>
              {
                  // process the received data
                  DataReceived(response);

                  // set the post-fetch state
                  PageIndex = newPageIndex;
                  OnPageChanged();
                  CanChangePage = true;
              });
        }

        /// <summary>
        /// Updates the items exposed by this view with the given data
        /// </summary>
        private void DataReceived(PagedDataResponse<T> response)
        {
            TotalItemCount = ItemCount = response.TotalItemCount;
            TotalPages = (int)Math.Ceiling((double)TotalItemCount / (double)PageSize);

            //System.Diagnostics.Debug.WriteLine("SSPCV total items: {0} pagesize: {1} pages: {2} from height: {3}", TotalItemCount, PageSize, TotalPages, System.Windows.Application.Current.Host.Content.ActualHeight);

            this.ClearItems();
            GC.Collect();

            foreach (var item in response.Items)
                this.Add(item);
   
        }

        /// <summary>
        /// Sets the given field, raising a PropertyChanged event
        /// </summary>
        private void SetField<T>(ref T field, T value, string propertyName)
        {
            if (object.Equals(field, value))
                return;

            field = value;
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

    }
}
