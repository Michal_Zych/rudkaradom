﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace Finder.BloodDonation.DynamicData.Paging
{
  /// <summary>
  /// Defines a source of data that can be paged.
  /// </summary>
  public interface IPagedDataSource<TDataType>
  {
    /// <summary>
    /// Asynchronously returns the data for the given page
    /// </summary>
    void FetchData(int pageNumber, Action<PagedDataResponse<TDataType>> responseCallback);
  }

  /// <summary>
  /// The items returned as a result of a paged data request.
  /// </summary>
  public class PagedDataResponse<TDataType>
  {
    /// <summary>
    /// The items contained within the requested page
    /// </summary>
    public List<TDataType> Items { get; set; }

    /// <summary>
    /// The total count of all available items
    /// </summary>
    public int TotalItemCount { get; set; }
  }
}
