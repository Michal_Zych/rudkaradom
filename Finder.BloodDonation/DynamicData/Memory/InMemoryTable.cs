﻿using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.Filters;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Helpers;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Model.Dto;

namespace Finder.BloodDonation.DynamicData.Memory
{
    public class InMemoryTable<DTO> : IAscyncDataSource<DTO>
    {
        private int count;
        private IList<DTO> items;

        private IUnitsManager unitsManager = null;

        public InMemoryTable(IUnitsManager unitsManager = null, IList<DTO> items = null)
        {
            SetData(items);

            if (unitsManager != null)
                this.unitsManager = unitsManager;
        }
        public InMemoryTable(IUnitsManager unitsManager)
            : this(unitsManager, new List<DTO>())
        {
        }

        public void SetData(IList<DTO> items)
        {
            this.items = items;
            count = items.Count;
        }

        public void GetData(int pageSize, int pageNr, IFilter filter, Action<IList<DTO>> callBack)
        {
            var dict = filter.GetFilterDictionary();
            string sortOrder = PopValue(dict, FilterConstant.SortOrderKey);

            var result = WhereResult(items, dict);

            if (!String.IsNullOrEmpty(sortOrder))
            {
                string columnName = sortOrder.Substring(1);
                var propertyInfo = typeof(DTO).GetProperty(columnName);
                if (sortOrder.StartsWith(FilterConstant.AscendingSign.ToString()))
                {
                    result = result.OrderBy(x => propertyInfo.GetValue(x, null));
                }
                else
                {
                    result = result.OrderByDescending(x => propertyInfo.GetValue(x, null));
                }
            }

            count = result.Count();

            if (pageSize > 0)
            {
                result = result
                        .Skip(pageSize * pageNr)
                        .Take(pageSize);
            }

            callBack(result.ToList());
        }


        public void GetTotalCount(IFilter filter, Action<int> callBack)
        {
            callBack(count);
        }


        public IEnumerable<DTO> WhereResult(IList<DTO> items, IDictionary<string, string> dict)
        {
            if (dict.Count == 0)
            {
                return items;
            }
            var units = GetUnitList(dict);

            var result = new List<DTO>();
            foreach (var i in items)
            {
                if (ItemMeetsConditions(i, dict, units))
                {
                    result.Add(i);
                }
            }
            return result;
        }

        private IDictionary<int, int> GetUnitList(IDictionary<string, string> dict)
        {
            IDictionary<int, int> units = new Dictionary<int, int>();
            string unitStr = PopValue(dict, FilterConstant.UnitFromTreeKey);
            int unitId;
            if (Int32.TryParse(unitStr, out unitId))
            {
                var childs = unitsManager.GetFlatChilds(unitId).Select(x => x.ID).ToList();
                childs.Add(unitId);
                units = childs.ToDictionary(x => x);
            }
            return units;
        }

        private static string PopValue(IDictionary<string, string> dict, string key)
        {
            string result = String.Empty;

            if (dict.Keys.Contains(key))
            {
                result = dict[key];
                dict.Remove(key);
            }

            return result;
        }

        private bool ItemMeetsConditions(DTO item, IDictionary<string, string> dict, IDictionary<int, int> units)
        {
            if (unitsManager != null)
            {
                var unitProperty = typeof(DTO).GetProperty("ShowInUnits");
                if (unitProperty != null)
                {
                    var showInUnits = (IEnumerable<int>)unitProperty.GetValue(item, null);
                    if (showInUnits != null && !units.ContainsElements(showInUnits))
                    {
                        return false;
                    }
                }
            }
            

            var fileds = dict.Keys.Select(k => FilterParser.GetPropertyName(k)).Distinct();

            foreach (var propertyName in fileds)
            {
                var property = typeof(DTO).GetProperty(propertyName);

                var value = property.GetValue(item, null);
                if (property.PropertyType == typeof(string))
                {
                    if (!PropertyMeetCondition((string)value, dict[propertyName])) return false;
                }

                if (property.PropertyType == typeof(bool?) || property.PropertyType == typeof(bool))
                {
                    if (!PropertyMeetCondition((bool?)value, dict[propertyName])) return false;
                }

                if (property.PropertyType == typeof(DateTime) || property.PropertyType == typeof(DateTime?))
                {
                    if (!PropertyMeetCondition(
                            (value == null) ? null : (DateTime?)value
                            , GetDictionaryValue(dict, FilterParser.GetFromPropertyName(propertyName))
                            , GetDictionaryValue(dict, FilterParser.GetToPropertyName(propertyName))
                        )) return false;
                }

                if (FilterParser.IsNumericType(property.PropertyType))
                {
                    if (!PropertyMeetCondition(
                            (value == null) ? (double?)null : Convert.ToDouble(value.ToString())
                            , GetDictionaryValue(dict, FilterParser.GetFromPropertyName(propertyName))
                            , GetDictionaryValue(dict, FilterParser.GetToPropertyName(propertyName))
                        )) return false;
                }

                if (property.PropertyType == typeof(int) || property.PropertyType == typeof(int?))
                {
                    if (!PropertyMeetCondition(
                         (property == null) ? null : (int?)property.GetValue(item, null)
                            , GetDictionaryValue(dict, FilterParser.GetFromPropertyName(propertyName))
                            , GetDictionaryValue(dict, FilterParser.GetToPropertyName(propertyName))
                        )) return false;
                }

            }
            return true;
        }





        private bool PropertyMeetCondition(double? value, string from, string to)
        {
            double? filterFrom = (from == null) ? (double?)null : Double.Parse(from);
            double? filterTo = (to == null) ? (double?)null : Double.Parse(to);

            if (filterFrom == null && filterTo.HasValue)
            {
                return !value.HasValue;
            }

            if (value == null) return false;

            if (filterFrom.HasValue && filterTo == null)
            {
                return value.Value == filterFrom.Value;
            }

            return value.Value >= filterFrom.Value && value.Value <= filterTo.Value;
        }




        private bool PropertyMeetCondition(DateTime? value, string from, string to)
        {
            if (value == null) return false;

            DateTime? filterFrom = DbDataExtension.FromSQLstring(from);
            DateTime? filterTo = DbDataExtension.FromSQLstring(to);

            bool fromOk = true;
            bool toOk = true;

            if (filterFrom.HasValue) fromOk = (value.Value >= filterFrom.Value);
            if (filterTo.HasValue) toOk = (value.Value <= filterTo.Value);
            return fromOk && toOk;
        }

        private string GetDictionaryValue(IDictionary<string, string> dict, string key)
        {
            if (dict.ContainsKey(key)) return dict[key];
            return null;
        }



        private bool PropertyMeetCondition(string value, string filterValue)
        {
            foreach (var condition in FilterParser.SplitByOrConditions(filterValue))
            {
                if (((string)value).Like(condition)) return true;
            }
            return false;
        }

        private bool PropertyMeetCondition(bool? value, string filterValue)
        {
            bool filterBool = (filterValue == "0") ? false : true;
            if (value == null || (bool)value != filterBool) return false;

            return true;
        }


        public void UpdateRecord(DTO newItemData, int itemToEditIndex)
        {
            if (itemToEditIndex < 0)
                return;
            if (itemToEditIndex >= items.Count)
                return;

            items[itemToEditIndex] = newItemData;
        }

        public void UpdateRecord(DTO newItemData, DTO itemToEdit)
        {
            DTO _temp = items.Where(e => e.Equals(itemToEdit)).FirstOrDefault();
            _temp = newItemData;
        }

        public DTO GetItem(int index)
        {
            return items[index];
        }

        public void AddNewRecord(DTO newItem)
        {
            items.Add(newItem);
        }

        public int GetCount()
        {
            return items.Count;
        }
    }
}
