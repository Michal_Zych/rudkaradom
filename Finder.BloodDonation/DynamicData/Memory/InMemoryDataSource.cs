﻿using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.DynamicData.Memory
{
    public class InMemoryDataSource<DTO, ViewModel> : IPagedDataSource<ViewModel>
        where ViewModel : IItemViewModel<DTO>
        where DTO : IDataTableDto
    {
        protected int _pageSize;
        protected IUnityContainer _container;
        private IDataTable _dataTable;
        private IAscyncDataSource<DTO> _dataSource;

        public InMemoryDataSource(IUnityContainer container, int pageSize, IDataTable dataTable, IAscyncDataSource<DTO> dataSource)
        {
            _container = container;
            _pageSize = pageSize;
            _dataTable = dataTable;
            _dataSource = dataSource;
        }

        public void FetchData(int pageNumber, Action<PagedDataResponse<ViewModel>> responseCallback)
        {
            var items = new List<ViewModel>();

            _dataSource.GetData(_pageSize, pageNumber, _dataTable, (list) =>
            {
                if (list != null)
                    foreach (var item in list)
                    {
                        var vm = _container.Resolve<ViewModel>();
                        vm.Item = item;
                        vm.DataTable = _dataTable;
                        items.Add(vm);
                    }

                _dataSource.GetTotalCount(_dataTable, (count) =>
                {
                    responseCallback(new PagedDataResponse<ViewModel>()
                    {
                        Items = items,
                        TotalItemCount = count
                    });
                });
            });
        }

    }
}
