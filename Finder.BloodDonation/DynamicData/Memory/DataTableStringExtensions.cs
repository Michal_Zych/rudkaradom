﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.DynamicData.Memory
{
    public static class DataTableStringExtensions
    {
        public static bool Like(this string toSearch, string toFind)
        {
            if ((toSearch == null || toSearch == "") && toFind.Trim() == "?") return true;
            if (toSearch == null) return false;
            return new Regex(@"\A" + new Regex(@"\.|\$|\^|\{|\[|\(|\||\)|\*|\+|\?|\\").Replace(toFind, ch => @"\" + ch).Replace('_', '.').Replace("%", ".*") + @"\z", RegexOptions.IgnoreCase).IsMatch(toSearch);
        }


        public static bool ContainsElements(this IDictionary<int, int> keysDictionary, IEnumerable<int> keysToSearch)
        {
            foreach (var key in keysToSearch)
            {
                if (keysDictionary.ContainsKey(key)) return true;
            }

            return false;
        }
    }
}
