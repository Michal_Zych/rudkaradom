using Finder.BloodDonation.Model.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.DynamicData.DBs
{
    public static class BandDb
    {
        public static IList<BandDTO> GetItems()
        {
            var items = new List<BandDTO>();


            for (int i = 0; i < 1000; i++)
            {
                items.Add(
                    new BandDTO()
                    {
                        Id = Convert.ToInt16(4),
                        Mac = Convert.ToInt64(30),
                        MacString = RandomString(random.Next(3, 15)),
                        Uid = Convert.ToInt64(50),
                        BarCode = RandomString(random.Next(3, 15)),
                        TransmissionPower = Convert.ToInt16(-8),
                        AccPercent = Convert.ToByte(30),
                        SensitivityPercent = Convert.ToByte(30),
                        BroadcastingIntervalMs = random.Next(5, 20),
                        Description = RandomString(random.Next(3, 15)),
                        ObjectType = Convert.ToByte(30),
                        ObjectId = random.Next(5, 20),
                        ObjectDisplayName = RandomString(random.Next(3, 15)),
                        UnitId = random.Next(5, 20),
                        CurrentUnitId = random.Next(5, 20),
                        CurrentUnitName = RandomString(random.Next(3, 15)),
                        CurrentUnitLocation = RandomString(random.Next(3, 15)),
                        BatteryTypeId = Convert.ToByte(0),
                        IsBatteryAlarm = random.Next(10) > 5,
                        BatteryInstallationDateUtc = DateTime.Now.AddDays(random.Next(-100, -2)),
                        Reason = RandomString(random.Next(3, 15)),
                    }
                    );
            };

            return items;
        }
        private static Random random = new Random();
        private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
