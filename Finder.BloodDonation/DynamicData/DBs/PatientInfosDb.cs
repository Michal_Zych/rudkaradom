using Finder.BloodDonation.Model.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.DynamicData.DBs
{
    public static class PatientInfoDb
    {
        public static IList<PatientInfoDto> GetItems()
        {
            var items = new List<PatientInfoDto>();

            items.Add(
                new PatientInfoDto()
                {
                    Id = 0,
                    FirstName = RandomString(random.Next(3, 15)),
                    LastName = RandomString(random.Next(3, 15)),
                    DisplayName = RandomString(random.Next(3, 15)),
                    Pesel = random.Next(5, 20),
                    HospitalAdmissionDate = DateTime.Now.AddDays(random.Next(-100, -2)),
                    Department = RandomString(random.Next(3, 15)),
                    DepartmentPath = RandomString(random.Next(3, 15)),
                    DepartmentAdmissionDate = DateTime.Now.AddDays(random.Next(-100, -2)),
                    Room = RandomString(random.Next(3, 15)),
                    RoomPath = RandomString(random.Next(3, 15)),
                    RoomAdmissionDate = DateTime.Now.AddDays(random.Next(-100, -2)),
                    CurrentlyDepartment = RandomString(random.Next(3, 15)),
                    CurrentlyDepartmentPath = RandomString(random.Next(3, 15)),
                    CurrentlyRoom = RandomString(random.Next(3, 15)),
                    CurrentlyRoomPath = RandomString(random.Next(3, 15)),
                }
                );
            items.Add(
                new PatientInfoDto()
                {
                    Id = 1,
                    FirstName = RandomString(random.Next(3, 15)),
                    LastName = RandomString(random.Next(3, 15)),
                    DisplayName = RandomString(random.Next(3, 15)),
                    Pesel = random.Next(5, 20),
                    HospitalAdmissionDate = DateTime.Now.AddDays(random.Next(-100, -2)),
                    Department = RandomString(random.Next(3, 15)),
                    DepartmentPath = RandomString(random.Next(3, 15)),
                    DepartmentAdmissionDate = DateTime.Now.AddDays(random.Next(-100, -2)),
                    Room = RandomString(random.Next(3, 15)),
                    RoomPath = RandomString(random.Next(3, 15)),
                    RoomAdmissionDate = DateTime.Now.AddDays(random.Next(-100, -2)),
                    CurrentlyDepartment = RandomString(random.Next(3, 15)),
                    CurrentlyDepartmentPath = RandomString(random.Next(3, 15)),
                    CurrentlyRoom = RandomString(random.Next(3, 15)),
                    CurrentlyRoomPath = RandomString(random.Next(3, 15)),
                }
                );


            return items;
        }
        private static Random random = new Random();
        private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
