using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DynamicData.Memory;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Tabs.Bands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.DynamicData.DBs
{
    public class BandsDataSource : IPagedDataSource<BandItemViewModel>
    {
        protected int _pageSize;
        protected IUnityContainer _container;
        private IFilter _filter;
        private IAscyncDataSource<BandDto> _dataSource;

        public BandsDataSource(IUnityContainer container, int pageSize, IFilter filter, IAscyncDataSource<BandDto> dataSource)
        {
            _container = container;
            _pageSize = pageSize;
            _filter = filter;
            _dataSource = dataSource;
        }

        public void FetchData(int pageNumber, Action<PagedDataResponse<BandItemViewModel>> responseCallback)
        {
            var items = new List<BandItemViewModel>();
            
            _dataSource.GetData(_pageSize, pageNumber, _filter, (list) =>
                {
                    if(list != null)
                        foreach(var item in list)
                        {
                            var vm = _container.Resolve<BandItemViewModel>();
                            vm.Item = item;
                            items.Add(vm);
                        }

                    _dataSource.GetTotalCount(_filter, (count) =>
                        {
                            responseCallback(new PagedDataResponse<BandItemViewModel>()
                            {
                                Items = items,
                                TotalItemCount = count
                            });
                        });
                });
        }


    }
}
