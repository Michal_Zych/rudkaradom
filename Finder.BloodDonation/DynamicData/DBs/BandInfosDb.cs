using Finder.BloodDonation.Model.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.DynamicData.DBs
{
    public static class BandInfoDb
    {
        public static IList<BandInfoDto> GetItems()
        {
            var items = new List<BandInfoDto>();

            /*items.Add(
                new BandInfoDto()
                {
				_Id = random.Next(5, 20),
BarCode = RandomString(random.Next(3, 15)),
_UnitIds = RandomString(random.Next(3, 15)),
MacString = RandomString(random.Next(3, 15)),
Uid = random.Next(5, 20),
BatteryTypeStr = RandomString(random.Next(3, 15)),
BatteryAlarmSeverity = RandomString(random.Next(3, 15)),
_BatteryAlarmSeverityIds = RandomString(random.Next(3, 15)),
BatteryInstallationDate = DateTime.Now.AddDays(random.Next(-100, -2)),
BatteryDaysToExchange = random.Next(5, 20),
Description = RandomString(random.Next(3, 15)),
ObjectType = RandomString(random.Next(3, 15)),
_ObjectTypeIds = RandomString(random.Next(3, 15)),
ObjectDisplayName = RandomString(random.Next(3, 15)),
ObjectUnitName = RandomString(random.Next(3, 15)),
ObjectUnitLocation = RandomString(random.Next(3, 15)),
CurrentUnitName = RandomString(random.Next(3, 15)),
_CurrentUnit = RandomString(random.Next(3, 15)),
_CurrentUnitLocation = RandomString(random.Next(3, 15)),
_CurrentUnitIds = RandomString(random.Next(3, 15)),
_BandUnitName = RandomString(random.Next(3, 15)),
_BandUnitLocation = RandomString(random.Next(3, 15)),
                }
                );
            items.Add(
                new BandInfoDto()
                {
				_Id = random.Next(5, 20),
BarCode = RandomString(random.Next(3, 15)),
_UnitIds = RandomString(random.Next(3, 15)),
MacString = RandomString(random.Next(3, 15)),
Uid = random.Next(5, 20),
BatteryTypeStr = RandomString(random.Next(3, 15)),
BatteryAlarmSeverity = RandomString(random.Next(3, 15)),
_BatteryAlarmSeverityIds = RandomString(random.Next(3, 15)),
BatteryInstallationDate = DateTime.Now.AddDays(random.Next(-100, -2)),
BatteryDaysToExchange = random.Next(5, 20),
Description = RandomString(random.Next(3, 15)),
ObjectType = RandomString(random.Next(3, 15)),
_ObjectTypeIds = RandomString(random.Next(3, 15)),
ObjectDisplayName = RandomString(random.Next(3, 15)),
ObjectUnitName = RandomString(random.Next(3, 15)),
ObjectUnitLocation = RandomString(random.Next(3, 15)),
CurrentUnitName = RandomString(random.Next(3, 15)),
_CurrentUnit = RandomString(random.Next(3, 15)),
_CurrentUnitLocation = RandomString(random.Next(3, 15)),
_CurrentUnitIds = RandomString(random.Next(3, 15)),
_BandUnitName = RandomString(random.Next(3, 15)),
_BandUnitLocation = RandomString(random.Next(3, 15)),
                }
                );

            for ($IdType$ i = 0; i < 1000; i++)
            {
                items.Add(
                    new BandInfoDto()
                    {
						_Id = random.Next(5, 20),
BarCode = RandomString(random.Next(3, 15)),
_UnitIds = RandomString(random.Next(3, 15)),
MacString = RandomString(random.Next(3, 15)),
Uid = random.Next(5, 20),
BatteryTypeStr = RandomString(random.Next(3, 15)),
BatteryAlarmSeverity = RandomString(random.Next(3, 15)),
_BatteryAlarmSeverityIds = RandomString(random.Next(3, 15)),
BatteryInstallationDate = DateTime.Now.AddDays(random.Next(-100, -2)),
BatteryDaysToExchange = random.Next(5, 20),
Description = RandomString(random.Next(3, 15)),
ObjectType = RandomString(random.Next(3, 15)),
_ObjectTypeIds = RandomString(random.Next(3, 15)),
ObjectDisplayName = RandomString(random.Next(3, 15)),
ObjectUnitName = RandomString(random.Next(3, 15)),
ObjectUnitLocation = RandomString(random.Next(3, 15)),
CurrentUnitName = RandomString(random.Next(3, 15)),
_CurrentUnit = RandomString(random.Next(3, 15)),
_CurrentUnitLocation = RandomString(random.Next(3, 15)),
_CurrentUnitIds = RandomString(random.Next(3, 15)),
_BandUnitName = RandomString(random.Next(3, 15)),
_BandUnitLocation = RandomString(random.Next(3, 15)),
                    }
                    );
            };*/

            return items;
        }
        private static Random random = new Random();
        private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
