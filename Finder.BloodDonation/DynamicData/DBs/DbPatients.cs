﻿using Finder.BloodDonation.Model.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.DynamicData.DBs
{
    public static class DbPatients
    {
        public static IList<PatientDTO> GetItems()
        {
            var items = new List<PatientDTO>();

            items.Add(
                new PatientDTO()
                {
                    ID = 0,
                    LastName = "Kowalski",
                    FirstName = "Jan",
                    DepartmentId = 2,
                    DepartmentName = "Onkologia",
                    RoomName = "Sala I",
                    BandId = 1,
                    Pesel = "84012505412",
                    RoomId = null,
                    InHospitalDate = new DateTime(2017, 1, 1, 11, 11, 11),
                    IsActive = true
                }
                );
            items.Add(
                new PatientDTO()
                {
                    ID = 1,
                    FirstName = "Janina",
                    LastName = "Nowak",
                    DepartmentId = 3,
                    BandId = 2,
                    RoomId = 4,
                    Pesel = "84862226011",
                    DepartmentName = "Onkologia",
                    RoomName = "Sala I",
                    InHospitalDate = new DateTime(2017, 2, 2, 12, 12, 12),
                    IsActive = false
                }
                );

            items.Add(
                new PatientDTO()
                {
                    ID = 2,
                    LastName = "Sierpień",
                    FirstName = "Krystyna",
                    DepartmentId = 3,
                    RoomId = 5,
                    Pesel = "94832126011",
                    DepartmentName = "Kardiologia",
                    RoomName = "Sala II",
                    IsActive = true,
                });

            items.Add(
                new PatientDTO()
                {
                    ID = 3,
                    LastName = "Nowak",
                    FirstName = "Stefan",
                    DepartmentId = 3,
                    RoomId = 6,
                    Pesel = "94321126011",
                    DepartmentName = "Pediatria",
                    RoomName = "Sala III",
                    IsActive = true,
                }
                );
            items.Add(
            new PatientDTO()
            {
                ID = 3,
                LastName = "Nowak",
                FirstName = "Kazimiera",
                DepartmentId = 3,
                RoomId = 4,
                Pesel = "14321126011",
                DepartmentName = "Pediatria",
                RoomName = "Sala III",
                IsActive = true,
            }
            );

            items.Add(
            new PatientDTO()
            {
                ID = 5,
                LastName = "Nowak",
                FirstName = "Stefan",
                DepartmentId = 3,
                RoomId = 5,
                Pesel = "99321126011",
                DepartmentName = "Onkologia",
                RoomName = "Sala III",
                IsActive = true,
            }
            );

            items.Add(
new PatientDTO()
{
    ID = 6,
    LastName = "Kazimierczak",
    FirstName = "Włodzimierz",
    DepartmentId = 3,
    RoomId = 6,
    Pesel = "99321126011",
    DepartmentName = "Neurologia",
    RoomName = "Sala III",
}
);

            items.Add(
new PatientDTO()
{
    ID = 7,
    LastName = "Kowalski",
    FirstName = "Sebastian",
    DepartmentId = 3,
    RoomId = null,
    Pesel = "99321126011",
    DepartmentName = "Kardiologia",
    RoomName = "Bardzo długa nazwa" + Environment.NewLine + "w dwóch liniach" + Environment.NewLine + "albo w trzech" + Environment.NewLine + "a może nawet w czterech",
}
);

            items.Add(
new PatientDTO()
{
    ID = 8,
    LastName = "Cygan",
    FirstName = "Anna",
    DepartmentId = 3,
    RoomId = 4,
    Pesel = "77321126011",
    DepartmentName = "Kardiologia",
    RoomName = "Sala V",
}
);

            items.Add(
new PatientDTO()
{
    ID = 9,
    LastName = "Jan",
    FirstName = "Kowalski",
    DepartmentId = 3,
    RoomId = null,
    Pesel = "923126011",
    DepartmentName = "Neurologia",
    RoomName = null,
    IsActive = true,
}
);

            items.Add(
new PatientDTO()
{
    ID = 10,
    LastName = "Kowalska",
    FirstName = "Maria",
    DepartmentId = 3,
    RoomId = 5,
    Pesel = "89321126011",
    DepartmentName = "Pediatria",
    RoomName = "Sala X",
}
);

            items.Add(
new PatientDTO()
{
    ID = 11,
    LastName = "Kowalska",
    FirstName = "Zofia",
    DepartmentId = 3,
    RoomId = 6,
    Pesel = "57321126011",
    DepartmentName = "Onkologia",
    RoomName = "Sala V",
}
);



            for (int i = 0; i < 1000; i++)
            {
                items.Add(
                    new PatientDTO()
                    {
                        ID = 12 + i,
                        LastName = RandomString(random.Next(3, 15)),
                        FirstName = RandomString(random.Next(3, 15)),
                        DepartmentId = i % 5,
                        RoomId = null,
                        Pesel = RandomString(random.Next(3, 15)),
                        DepartmentName = RandomString(random.Next(3, 15)),

                    }
                    );
            };



            return items;
        }


        private static Random random = new Random();
        private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
