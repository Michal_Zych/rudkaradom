﻿using Finder.BloodDonation.Model.Dto;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using Microsoft.Practices.Composite.Events;
using Finder.BloodDonation.Common.Events;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System.Reflection;

namespace Finder.BloodDonation.DynamicData.DBs
{
    public class DbBandStatus
    {
        private static DbBandStatus instance;
        private IDictionary<int, BandStatusDto> items;

        IDynamicEventAggregator EventAggregator;
        IUnityContainer Container;
       
        public static void Initialize(IUnityContainer container)
        {
            if(instance == null)
            {
                instance = new DbBandStatus(container);
            }
        }

        private DbBandStatus(IUnityContainer container)
        {
            Container = container;
            EventAggregator = container.Resolve<IDynamicEventAggregator>();
           
            items = new Dictionary<int, BandStatusDto>();

            EventAggregator.GetEvent<AutoBandStatusesRefreshEvent>().Subscribe(BandStatusesRefresh_Event);
            Proxy.GetBandsStatusesAsync(null);
        }

        public static IList<BandStatusDto> GetItems()
        {
           return instance.GetItemsList();
        }

        private IList<BandStatusDto> GetItemsList()
        {
            return items.Values.ToList();
        }

        private UnitsServiceClient Proxy
        {
            get{
                var proxy = ServiceFactory.GetService<UnitsServiceClient>(ServicesUri.UnitsService);
               proxy.GetBandsStatusesCompleted += GetBandsStatusesCompletedEventHandler;
               return proxy;
            }
        }

        public void GetBandsStatusesCompletedEventHandler(object sender, GetBandsStatusesCompletedEventArgs e)
        {
            if(e.Result != null)
            {
                foreach(var item in e.Result)
                {
                    if(items.ContainsKey(item.BandId))
                    {
                        foreach (var property in item.GetType().GetProperties()) 
                        {
                            var target = items[item.BandId];

                            var prop = target.GetType().GetProperty(property.Name);

                            if (prop.GetSetMethod() != null)
                                prop.SetValue(target, property.GetValue(item, null), null);
                        }

                        //if(items[item.BandId].SynchroToken != item.SynchroToken)
                        //{
                        //    items[item.BandId] = item;
                        //}
                    }
                    else
                    {
                        items[item.BandId] = item;
                    }
                }
            }
            EventAggregator.GetEvent<BandStatusesRefreshedEvent>().Publish(null);
        }

        public void BandStatusesRefresh_Event(int obj)
        {
            Proxy.GetBandsStatusesAsync(null);
        }
    }
}
