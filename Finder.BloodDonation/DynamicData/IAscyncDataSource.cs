﻿using Finder.BloodDonation.Common.Filters;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.DynamicData
{
    public interface IAscyncDataSource<T>
    {
        void GetData(int pageSize, int pageNr, IFilter filter, Action<IList<T>> callBack);
        void GetTotalCount(IFilter filter, Action<int> callBack);
    }
}
