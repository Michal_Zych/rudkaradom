using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataTable;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Filters;

namespace Finder.BloodDonation.Tabs.ExternalRecordsIn
{
    public class ExternalRecordsInDataTable : BaseDataTable
    {

        public ExternalRecordsInDataTable(IUnityContainer container)
            : base(container)
        {
        }

        public override ObservableCollection<IDataTableColumn> GetColumns()
        {
            return new ObservableCollection<IDataTableColumn>()
             .Add(50)
			 .Add(100, "Type", "Typ", FilterDataType.String)
             .Add(100, "Id", "Identyfikator", FilterDataType.Int)
             .Add(100, "RequestId", "Id zapytania", FilterDataType.Int)
             .Add(100, "StoreDateUtc", "Data zapisu", FilterDataType.DateTime)
             .Add(100, "RecordType", "Typ rekordu", FilterDataType.String)
             .Add(100, "RecordId", "Id obiektu", FilterDataType.Int)
             .Add(100, "Record", "Rekord", FilterDataType.String)
             .Add(100, "Processed", "Przetworzono", FilterDataType.Int)
             .Add(100, "Try", "Pr�ba wysy�ki", FilterDataType.Int)
             .Add(100, "ProcessedDateUtc", "Data przetworzenia", FilterDataType.DateTime)
             .Add(100, "Error", "Opis b��du", FilterDataType.String)
                          ;
        }


        public override ISortedFilter GetFilter(IUnityContainer container)
        {
            return new ExternalRecordsInFilter(container);
        }

		public override bool StartWithDefaultColumns
        {
            get
            {
                return false;
            }
        }
    }
}
