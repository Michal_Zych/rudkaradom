using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.ExternalRecordsIn
{
    public class ExternalRecordsInFilter : SortedFilter
    {
       [RaisePropertyChanged]
public string Type { get; set; }

[RaisePropertyChanged]
public string Id { get; set; }

[RaisePropertyChanged]
public string RequestId { get; set; }

[RaisePropertyChanged]
public DateTime? StoreDateUtcFrom { get; set; }
[RaisePropertyChanged]
public DateTime? StoreDateUtcTo { get; set; }

[RaisePropertyChanged]
public string RecordType { get; set; }

[RaisePropertyChanged]
public string RecordId { get; set; }

[RaisePropertyChanged]
public string Record { get; set; }

[RaisePropertyChanged]
public string Processed { get; set; }

[RaisePropertyChanged]
public string Try { get; set; }

[RaisePropertyChanged]
public DateTime? ProcessedDateUtcFrom { get; set; }
[RaisePropertyChanged]
public DateTime? ProcessedDateUtcTo { get; set; }

[RaisePropertyChanged]
public string Error { get; set; }



        public ExternalRecordsInFilter(IUnityContainer container)
            : base(container)
        {

        }
    }
}
