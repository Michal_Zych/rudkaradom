﻿using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Common.Messages;
using Finder.BloodDonation.DBBuffers;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.DynamicData.DBs;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Tabs.EventsDetailsMonitorableObject;
using Finder.BloodDonation.Tabs.UnitsList;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.Model.Dto.Enums;
using Finder.BloodDonation.Controls;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Tabs.MenuAdministration.SettingsSystem;

namespace Finder.BloodDonation.Tabs.Monitorable
{
    public class DetailsMonitorableObjectViewModel : ViewModelBase, IDialogWindow, ITabViewModel, IUnitFiltered
    {
        private const int PATIENT_MOVED = 16;
        private const int PATIENT_NO_MOVED = 17;

        private MonitorableObjectStatusDto MonitorableObjectStatus { get; set; }

        internal DetailsMonitorableObjectData data = null;
        internal PatientInfoMessage patientData = null;
        IList<BandStatusDto> BandStatusDto { get; set; }

        public RelayCommand CancelClick { get; set; }
        public RelayCommand LocalizeClickCommand { get; set; }

        [RaisePropertyChanged]
        public string DisplayName { get; private set; }

        [RaisePropertyChanged]
        public string DisplayId { get; private set; }

        [RaisePropertyChanged]
        public string AssigmnetDepartment { get; private set; }

        [RaisePropertyChanged]
        public string AssigmentRoom { get; private set; }

        [RaisePropertyChanged]
        public string ImageFileName { get; private set; }

        [RaisePropertyChanged]
        public BitmapMode? DisplayImageColor { get; private set; }

        [RaisePropertyChanged]
        public string AlarmType { get; private set; }

        [RaisePropertyChanged]
        public string AlarmDate { get; private set; }

        [RaisePropertyChanged]
        public string CurrentLocation { get; private set; }

        [RaisePropertyChanged]
        public string CurrentLocationText { get; private set; }

        [RaisePropertyChanged]
        public Visibility WaitIndicatorVisibility { get; private set; }

        [RaisePropertyChanged]
        public Visibility BandsDataVisibility { get; private set; }

        [RaisePropertyChanged]
        public Visibility LocalizationButtonVisibility { get; private set; }

        [RaisePropertyChanged]
        public IEnumerable EventsItem { get; private set; }

        [RaisePropertyChanged]
        public string EventDataFromText { get; private set; }

        [RaisePropertyChanged]
        public string EventDateFrom { get; private set; }

        [RaisePropertyChanged]
        public string EventDateToText { get; private set; }

        [RaisePropertyChanged]
        public string EventDateTo { get; private set; }

        [RaisePropertyChanged]
        public Visibility EventDataFromVisibility { get; private set; }

        [RaisePropertyChanged]
        public Visibility EventDateToVisibility { get; private set; }

        [RaisePropertyChanged]
        public Color AlarmTypeColor { get; set; }

        [RaisePropertyChanged]
        public int AlarmsCount { get; set; }

        [RaisePropertyChanged]
        public int WarningsCount { get; set; }

        [RaisePropertyChanged]
        public string MovedStatus { get; set; }

        [RaisePropertyChanged]
        public Visibility AlarmsCountVisibility { get; set; }

        [RaisePropertyChanged]
        public Visibility WarningsCountVisibility { get; set; }

        private ITabViewModel _detailsMonitorableObject;
        private Finder.BloodDonation.Tabs.UnitsList.Object objectToMonitorable = null;

        private UnitsServiceClient Proxy { get; set; }

        private int? CurrentUnitID = null;

        [RaisePropertyChanged]
        public ITabViewModel DetailsMonitorableObject
        {
            get
            {
                if(_detailsMonitorableObject == null)
                {
                    _detailsMonitorableObject = Container.Resolve<EventsDetailsMonitorableObjectViewModel>();
                }
                
                return _detailsMonitorableObject;
            }
            set
            {
                _detailsMonitorableObject = value;
            }
        }

        public DetailsMonitorableObjectViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            CancelClick = new RelayCommand(OnCancelClick);
            LocalizeClickCommand = new RelayCommand(OnLocalizeClick);

            Messenger.Default.Register<PatientInfoMessage>(this, MessageReceiver);

            Proxy = ServiceFactory.GetService<Finder.BloodDonation.UnitsService.UnitsServiceClient>(ServicesUri.UnitsService);
            Proxy.GetBandsStatusesCompleted += GetBandsStatusesCompletedEventHandler;
            Proxy.MonitoredObjectStatusGetCompleted += Proxy_MonitoredObjectStatusGetCompleted;
            Proxy.Object_GetLocationCompleted += Proxy_Object_GetLocationCompleted;

            Proxy.GetBandsStatusesAsync(null);
        }

        public void Proxy_Object_GetLocationCompleted(object sender, Object_GetLocationCompletedEventArgs e)
        {
            const string locationNotExistText = "brak danych";

            if (MonitorableObjectStatus == null || MonitorableObjectStatus.CurrentUnitId != null || MonitorableObjectStatus.PreviousUnitId != null || MonitorableObjectStatus.AlarmsCount > 0)
            {
                EventDataFromText = "Data:";
                EventDateFrom = e.Result != null
                    ? (
                         e.Result.InCurrent != new DateTime(1, 1, 1, 1, 0, 0)
                         ? e.Result.InCurrent.ToString()
                         :
                         (
                            e.Result.InPrevious != new DateTime(1, 1, 1, 1, 0, 0) ? e.Result.InPrevious.ToString() : locationNotExistText
                         )
                       )
                    : locationNotExistText;


                CurrentLocationText = "Ostatnio przebywał w:";
                CurrentLocation = e.Result != null
                    ? (
                         !String.IsNullOrEmpty(e.Result.CurrentUnitLocation)
                         ? e.Result.CurrentUnitLocation
                         :
                         (
                            !String.IsNullOrEmpty(e.Result.PreviousUnitLocation) ? e.Result.PreviousUnitLocation : locationNotExistText
                         )
                       )
                    : locationNotExistText;
            }
        }

        public void Proxy_MonitoredObjectStatusGetCompleted(object sender, MonitoredObjectStatusGetCompletedEventArgs e)
        {
            if(e.Result != null)
            {
                MonitorableObjectStatus = e.Result;

                if (MonitorableObjectStatus.CurrentUnitId != null || MonitorableObjectStatus.PreviousUnitId != null || MonitorableObjectStatus.AlarmsCount > 0)
                {
                    ClearField();
                    SetDataToField();
                }
                else
                {
                    SetNoBandStatus();
                }
            }
            else
            {
                MonitorableObjectStatus = new MonitorableObjectStatusDto();
            }
        }

        private void SetNoBandStatus()
        {
            DisplayImageColor = BitmapMode.White;
            ImageFileName = data.MonitorableObject.ImageName;
            
            WaitIndicatorVisibility = Visibility.Collapsed;
            AlarmType = "Pacjent nie ma przypisanej opaski!";
            BandsDataVisibility = Visibility.Visible;
            CurrentLocationText = "";
            MovedStatus = "";
            
            //throw new NotImplementedException();
        }

        private void GetBandsStatusesCompletedEventHandler(object sender, UnitsService.GetBandsStatusesCompletedEventArgs e)
        {
            if(e.Result != null)
            {
                BandStatusDto = e.Result;
                SetObjectToMonitorableFromDataBase();
            }
        }

        private void SetObjectToMonitorableFromDataBase()
        {
            if (patientData == null)
            {
                EventAggregator.GetEvent<SendAfterRegisterResolveEvent>().Publish(null);
                return;
            }

            if (BandStatusDto == null)
                return;

            var c = BandStatusDto.FirstOrDefault(x => x.ObjectId == int.Parse(patientData.Id) && x.ObjectType == 4);
            
            CurrentUnitID = c != null ? c.CurrentUnitId : null;
            
            if(CurrentUnitID == null)
            {
                CurrentUnitID = c != null ? c.PreviousUnitId : null;
            }

            objectToMonitorable = CreatePatient(BandStatusDto.FirstOrDefault(x => x.ObjectId == int.Parse(patientData.Id) && x.ObjectType == 4));

            if (objectToMonitorable != null)
            {
                data = new DetailsMonitorableObjectData(objectToMonitorable);
                Proxy.MonitoredObjectStatusGetAsync(4, int.Parse(patientData.Id));
            }
        }

        private BitmapMode GetColor(BandStatusDto dto)
        {
            if (dto.TopAlarmSeverity == (byte)EventTypes.Alarm)
            {
                return BitmapMode.Red;
            }
            else if (dto.TopAlarmSeverity == (byte)EventTypes.Warning)
            {
                return BitmapMode.Yellow;
            }
            else
            {
                if (dto.ObjectUnitId != null && dto.ObjectUnitId != 0)
                {
                    if (dto.CurrentUnitId == null && dto.PreviousUnitId == null)
                        return BitmapMode.Gray;

                    if (dto.AlarmDisconnectedOngoing == true)
                        return BitmapMode.Gray;

                    if (dto.CurrentUnitId == dto.ObjectUnitId)
                        return BitmapMode.Green;
                    if (dto.ObjectUnitId == null)
                        return BitmapMode.Cyan;
                    else
                        return BitmapMode.Green;
                }
                else
                {
                    return BitmapMode.Cyan;
                }
            }
        }

        private Patient CreatePatient(BandStatusDto dto)
        {
            Patient p = new Patient();
            try
            {
                p.Id = MonitorableObjectStatus.Id;
                p.ObjectType = ObjectType.Patient;
                p.CurrentUnitId = MonitorableObjectStatus.CurrentUnitId;
                p.PreviousUnitId = MonitorableObjectStatus.PreviousUnitId;
                p.Name = dto.ObjectDisplayName;
                p.DisplayColor = GetColor(dto);
                p.EventTypes = MonitorableObjectStatus.EventTypes != null ? (EventTypes)MonitorableObjectStatus.EventTypes : 0;
                p.AlarmDate = MonitorableObjectStatus.AlarmDate != null ? (DateTime)MonitorableObjectStatus.AlarmDate : new DateTime();

                p.AlarmType = MonitorableObjectStatus.AlarmType != null ? (AlarmType)MonitorableObjectStatus.AlarmType : 0;
                p.DisplayId = MonitorableObjectStatus.DisplayId;
                p.Location = String.IsNullOrEmpty(MonitorableObjectStatus.CurrentUnitName) ? "Brak danych"
                    : ReverseLocationPath(
                    UnitManager.Instance.Units.FirstOrDefault(x => x.ID == MonitorableObjectStatus.CurrentUnitId).GetPath("/") + "/" + MonitorableObjectStatus.CurrentUnitName, '/'
                    );

                p.AlarmInfo = "S";
                return p;
            }
            catch 
            {
                p.DisplayColor = DBBuffers.BitmapMode.White;
                return p;
            }
        }

        private string ReverseLocationPath(string stringPath, char separator)
        {
            List<string> wordList = new List<string>();
            StringBuilder sb = new StringBuilder(String.Empty);

            for (int i = 0; i < stringPath.Length; i++)
            {
                if (stringPath[i] != separator)
                    sb.Append(stringPath[i]);
                else
                {
                    wordList.Add(sb.ToString());
                    sb = new StringBuilder(String.Empty);
                }
            }

            wordList.Add(sb.ToString());
            sb = new StringBuilder(String.Empty);

            for (int i = wordList.Count - 1; i >= 0; i--)
            {
                sb.Append(wordList[i]);

                if (i > 0)
                {
                    sb.Append(" ").Append(separator).Append(" ");
                }
            }

            return sb.ToString();
        }

        public void MessageReceiver(PatientInfoMessage obj)
        {
            ClearField();

            if (obj != null)
            {
                patientData = obj;
            }

            SetBaseDataToField();
            Proxy.GetBandsStatusesAsync(null);
        }

        private void SetBaseDataToField()
        {
            WaitIndicatorVisibility = Visibility.Visible;
            Title = "Szczegóły pacjenta: " + patientData.FirstName + " " + patientData.LastName;
            DisplayName = patientData.FirstName + " " + patientData.LastName;
            DisplayId = patientData.Pesel;
            AssigmnetDepartment = (patientData.AssignmentDepartment != null ? patientData.AssignmentDepartment : "brak danych");
            AssigmentRoom = (patientData.AssignmentRoom != null ? patientData.AssignmentRoom : "brak danych");
            BandsDataVisibility = Visibility.Visible;
            byte s = 4;
            // TESTOWO ZAKOMENTOWANE! Jeśli nic nie wywali, to niech tak zostanie - występują konflikty w asynchronicznym pobieraniu statusu i lokalizacji osobno, prawdopodobnie lepiej zostawić status.
            //Proxy.Object_GetLocationAsync(int.Parse(patientData.Id), s);
            CurrentLocationText = "Miejsce pobytu:";
        }

        private void OnCancelClick()
        {
            Container.Resolve<IWindowsManager>().CloseWindow(this.WindowKey);
        }

        object IDialogWindow.Data
        {
            set
            {
                data = (DetailsMonitorableObjectData)value;

                if(data != null)
                    if (data.MonitorableObject != null)
                    {
                        Proxy.MonitoredObjectStatusGetAsync((byte)data.MonitorableObject.ObjectType, data.MonitorableObject.Id);
                    }
            }
        }

        private void ClearField()
        {
            DisplayName = String.Empty;
            DisplayId = "Brak danych";
            AssigmnetDepartment = String.Empty;
            AssigmentRoom = String.Empty;
            ImageFileName = String.Empty;
            AlarmType = null;
            AlarmTypeColor = Colors.Transparent;
            AlarmDate = String.Empty;
            EventDateFrom = String.Empty;
            EventDateToText = String.Empty;
            EventDateTo = String.Empty;
            CurrentLocation = String.Empty;
            LocalizationButtonVisibility = Visibility.Collapsed;
            DisplayImageColor = null;
            BandsDataVisibility = Visibility.Visible;
            AlarmsCount = 0;
            WarningsCount = 0;
            AlarmsCountVisibility = Visibility.Collapsed;
            WarningsCountVisibility = Visibility.Collapsed;
        }

        private void SetData()
        {
          
        }

        private void SetDataToField()
        {
            AlarmType = SetAlarmType();
            AlarmTypeColor = SetAlarmTypeColor();

            if (MonitorableObjectStatus.PreviousUnitId == null && MonitorableObjectStatus.CurrentUnitId == null)
                AlarmType = "Brak połączenia";

            if (MonitorableObjectStatus.RoomOutDateUtc.Value.Year == 1)
                MonitorableObjectStatus.RoomOutDateUtc = null;

            DisplayName = MonitorableObjectStatus.Name;
            DisplayId = MonitorableObjectStatus.DisplayId;
            AlarmDate = MonitorableObjectStatus.AlarmDate != null ? MonitorableObjectStatus.AlarmDate.Value.ToLocalTime().ToString() : "Brak danych";
            LocalizationButtonVisibility = MonitorableObjectStatus.CurrentUnitLocation != null ? Visibility.Visible : Visibility.Collapsed;
            CurrentLocationText = "Miejsce pobytu:";
            CurrentLocation = GetCurrentLocation();
            AssigmnetDepartment = MonitorableObjectStatus.AssigmentDepartmentName;
            AssigmentRoom = MonitorableObjectStatus.AssigmentRoomName;
            EventDateFrom = MonitorableObjectStatus.AlarmDate != null ?
                MonitorableObjectStatus.AlarmDate.Value.ToLocalTime().ToString() : MonitorableObjectStatus.RoomOutDateUtc != null && MonitorableObjectStatus.CurrentUnitId != null ? 
                MonitorableObjectStatus.RoomOutDateUtc.Value.ToLocalTime().ToString() : MonitorableObjectStatus.CurrentUnitId == null && MonitorableObjectStatus.InCurrentUnitUtc != null && MonitorableObjectStatus.InCurrentUnitUtc.Value.Year != 1 ?
                MonitorableObjectStatus.InCurrentUnitUtc.Value.ToLocalTime().ToString() : string.Empty;
            EventDataFromText = MonitorableObjectStatus.AlarmDate != null ?
                "Data alarmu: " : MonitorableObjectStatus.RoomOutDateUtc != null && MonitorableObjectStatus.CurrentUnitId != null ?
                "Data wyjścia: " : MonitorableObjectStatus.CurrentUnitId == null && MonitorableObjectStatus.InCurrentUnitUtc != null && MonitorableObjectStatus.InCurrentUnitUtc.Value.Year != 1 ?
                "Data zerwania połączenia: " : string.Empty;
            DisplayImageColor = GetPatientColor();
            WaitIndicatorVisibility = Visibility.Collapsed;
            BandsDataVisibility = Visibility.Visible;
            AlarmsCount = MonitorableObjectStatus.AlarmsCount;
            AlarmsCountVisibility = MonitorableObjectStatus.EventTypes == (int)EventTypes.Alarm ? Visibility.Visible : Visibility.Collapsed;
            WarningsCount = MonitorableObjectStatus.WarningsCount;
            WarningsCountVisibility = (MonitorableObjectStatus.EventTypes == (int)EventTypes.Alarm | MonitorableObjectStatus.EventTypes == (int)EventTypes.Warning) ? Visibility.Visible : Visibility.Collapsed;
            MovedStatus = MonitorableObjectStatus.CurrentUnitId != null ? MonitorableObjectStatus.MovedStatus : "";
            ImageFileName = data.MonitorableObject.ImageName;
        }

        private BitmapMode GetPatientColor()
        {
            if (MonitorableObjectStatus.EventTypes == (int)EventTypes.Alarm)
                return BitmapMode.Red;

            if (MonitorableObjectStatus.EventTypes == (int)EventTypes.Warning)
                return BitmapMode.Yellow;

            if (MonitorableObjectStatus.AssigmentRoomId == null)
                return BitmapMode.Cyan;

            if (MonitorableObjectStatus.CurrentUnitId == MonitorableObjectStatus.AssigmentRoomId)
                return BitmapMode.Green;

            if (MonitorableObjectStatus.CurrentUnitId == null)
                return BitmapMode.Gray;

            return BitmapMode.Green;
        }

        private Color SetAlarmTypeColor()
        {
            if(MonitorableObjectStatus.EventTypes == (int)EventTypes.Alarm)
            {
                return Colors.Red;
            }
            else if(MonitorableObjectStatus.EventTypes == (int)EventTypes.Warning)
            {
                return Colors.Orange;
            }

            if (MonitorableObjectStatus.BandId == null)
                return Colors.Blue;

            if (MonitorableObjectStatus.AssigmentRoomId == null)
                return Color.FromArgb(100, 189, 224, 255);

            if (MonitorableObjectStatus.CurrentUnitId != MonitorableObjectStatus.AssigmentRoomId)
                return Color.FromArgb(100, 25, 116, 26);

            if (MonitorableObjectStatus.CurrentUnitId == null && MonitorableObjectStatus.BandId != null)
                return Colors.LightGray;

            return Colors.Green;
        }

        private string GetCurrentLocation()
        {
            string _temp = "Brak danych";

            try
            {
                if (MonitorableObjectStatus.CurrentUnitId == null &&
                    MonitorableObjectStatus.PreviousUnitId == null)
                    return _temp;

                try
                {
                    // try, bo może nie być na liście przy braku uprawnień
                    if (MonitorableObjectStatus.CurrentUnitId != null)
                        _temp = UnitManager.Instance.Units.FirstOrDefault(x => x.ID == MonitorableObjectStatus.CurrentUnitId).GetPath(Container.Resolve<ISettingsRespository>().Settings.UnitsLocationSeparator);

                }
                catch
                {
                    _temp = MonitorableObjectStatus.CurrentUnitLocation + " / " + MonitorableObjectStatus.CurrentUnitName;
                }

                if (MonitorableObjectStatus.CurrentUnitId == null && MonitorableObjectStatus.PreviousUnitId != null)
                {
                    CurrentLocationText = "Ostatnio przebywał w:";
                    try
                    {
                        _temp = UnitManager.Instance.Units.FirstOrDefault(x => x.ID == MonitorableObjectStatus.PreviousUnitId).GetPath(Container.Resolve<ISettingsRespository>().Settings.UnitsLocationSeparator);
                    }
                    catch
                    {
                        _temp = MonitorableObjectStatus.PreviousUnitLocation + " / " + MonitorableObjectStatus.PreviousUnitName;
                    }
                }
                return _temp;
            }
            catch
            {
                return "Lokalizacja o ID: " + MonitorableObjectStatus.CurrentUnitId + " już nie istnieje.";
            }
        }

        private string SetAlarmType()
        {
            if (MonitorableObjectStatus.EventTypes == (int)EventTypes.Alarm)
            {
                switch (MonitorableObjectStatus.AlarmType)
                {
                    case (int)UnitsList.AlarmType.Connected:
                        return String.Empty;

                    case (int)UnitsList.AlarmType.SOS:
                        return "Alarm SOS";

                    case (int)UnitsList.AlarmType.ACC:
                        return "Alarm z akcelerometru";

                    case (int)UnitsList.AlarmType.ZoneOut:
                        return "Alarm pacjent poza salą";

                    case (int)UnitsList.AlarmType.Disconnected:
                        return "Alarm brak łączności";

                    case ((int)UnitsList.AlarmType.NoGoZoneIn | (int)UnitsList.AlarmType.NoGoZoneOut):
                        return "Wejście do strefy zakazanej:" + MonitorableObjectStatus.AlarmUnitName;

                    case (int)UnitsList.AlarmType.Moving:
                        return "Pacjent się porusza";

                    case (int)UnitsList.AlarmType.NoMoving:
                        return "Pacjent się nie porusza";
                }
            }
            else if(MonitorableObjectStatus.EventTypes == (int)EventTypes.Warning)
            {
                switch (MonitorableObjectStatus.AlarmType)
                {
                    case (int)UnitsList.AlarmType.ZoneOut:
                        return "Pacjent przebywa poza salą";

                    case (int)UnitsList.AlarmType.Disconnected:
                        return "Brak połączenia";

                    case ((int)UnitsList.AlarmType.NoGoZoneIn | (int)UnitsList.AlarmType.NoGoZoneOut):
                        return "Wejście do strefy zakazanej: " + MonitorableObjectStatus.AlarmUnitName;

                    case (int)UnitsList.AlarmType.Moving:
                        return "Pacjent się porusza";

                    case (int)UnitsList.AlarmType.NoMoving:
                        return "Pacjent się nie porusza";
                }
            }
            else
            {
                if (MonitorableObjectStatus.AssigmentRoomId != MonitorableObjectStatus.CurrentUnitId)
                    return "Pacjent poza salą";

                if (MonitorableObjectStatus.AlarmType == null)
                    return String.Empty;

                if (MonitorableObjectStatus.BandId == null)
                    return "Pacjent nie ma przypisanej opaski!";

                if (MonitorableObjectStatus.AssigmentRoomId == null)
                    return "Pacjent nie ma przypisanej sali";
            }

            return "Brak danych";
        }

        public string WindowKey
        {
            get { return WindowsKeys.DetailsMonitorableObject; }
        }

        public void Refresh(IUnit unit)
        {
            throw new NotImplementedException();
        }

        public event EventHandler LoadingCompleted;

        public ViewModelState State
        {
            get { throw new NotImplementedException(); }
        }

        [RaisePropertyChanged]
        public string Title { get; set; }

        public bool ExitButtonVisible
        {
            get { throw new NotImplementedException(); }
        }

        public void OnLocalizeClick()
        {
            if (MonitorableObjectStatus != null)
            {
                if (MonitorableObjectStatus.PreviousUnitId == null && MonitorableObjectStatus.CurrentUnitId == null)
                {
                    MsgBox.Warning("Pacjent nigdy nie był w żadnej lokalizacji!");
                    return;
                }

                int currentUnitId = MonitorableObjectStatus.CurrentUnitId != null ? (int)MonitorableObjectStatus.CurrentUnitId : 0;
                int currentSectionId = 0;
                int previousUnitId = MonitorableObjectStatus.PreviousUnitId != null ? (int)MonitorableObjectStatus.PreviousUnitId : 0;
                int previousSectionId = 0;

                if (currentUnitId != 0 && !UnitManager.Instance.Units.Where(x => x.ID == currentUnitId).FirstOrDefault().HasChildren()) // liść
                    currentSectionId = UnitManager.Instance.Units.Where(x => x.Children.Any(y => y.ID == currentUnitId)).FirstOrDefault().ID; // szukamy parenta

                if (currentUnitId == 0 && previousUnitId != 0 && !UnitManager.Instance.Units.Where(x => x.ID == previousUnitId).FirstOrDefault().HasChildren())
                    previousSectionId = UnitManager.Instance.Units.Where(x => x.Children.Any(y => y.ID == previousUnitId)).FirstOrDefault().ID;

                if (Convert.ToInt32(currentUnitId) > 0 && !String.IsNullOrEmpty(patientData.Id))
                {
                    EventAggregator.GetEvent<ExpandAccordionEvent>().Publish(AccordionItemsNames.UNITS);
                    EventAggregator.GetEvent<ChangeSelectedUnitEvent>().Publish(currentSectionId != 0 ? currentSectionId : currentUnitId);

                    ITabManager TabManager = Container.Resolve<ITabManager>();
                    TabManager.SelectTab(TabNames.Localization,
                        (e) =>
                        {
                            EventAggregator.GetEvent<ForwardingMessageEvent>().Publish(int.Parse(patientData.Id));
                        });

                    // zamykanie nie działa za pierwszym razem, dopiero za drugim i kolejnymi, nie wiadomo czemu tak się dzieje, więc zostawiamy
                    /*var windowsManager = this.Container.Resolve<IWindowsManager>();
                    windowsManager.CloseWindow(WindowsKeys.PatientAlarmingView);*/

                }
                else if (Convert.ToInt32(previousUnitId) > 0 && !String.IsNullOrEmpty(patientData.Id))
                {
                    EventAggregator.GetEvent<ExpandAccordionEvent>().Publish(AccordionItemsNames.UNITS);
                    EventAggregator.GetEvent<ChangeSelectedUnitEvent>().Publish(previousSectionId != 0 ? previousSectionId : previousUnitId);

                    ITabManager TabManager = Container.Resolve<ITabManager>();
                    TabManager.SelectTab(TabNames.Localization,
                        (e) =>
                        {
                            EventAggregator.GetEvent<ForwardingMessageEvent>().Publish(int.Parse(patientData.Id));
                        });
                }
                else if (currentUnitId == 0 && previousUnitId == 0)
                {
                    MsgBox.Warning("Nie posiadasz uprawnień do przeglądania tej sekcji! ( " + MonitorableObjectStatus.CurrentUnitLocation + " )");
                }
            }
            else
            {
                MsgBox.Warning("Nie wybrano pacjenta!");
            }
        }
    }
}
