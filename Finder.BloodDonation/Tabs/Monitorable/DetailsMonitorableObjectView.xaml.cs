﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Telerik.Windows.Controls;

namespace Finder.BloodDonation.Tabs.Monitorable
{
    [ViewModel(typeof(DetailsMonitorableObjectViewModel))]
    public partial class DetailsMonitorableObjectView : UserControl
    {
        /*
        public void DataContextChangedm(DetailsMonitorableObjectViewModel sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                var vm = e.NewValue as DetailsMonitorableObjectViewModel;

                CommandManager.SetCommandBindings(this, vm.);
            }
        }
        */

        public DetailsMonitorableObjectView()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            waitIndicator.Start();
        }

    }
}
