﻿
using Finder.BloodDonation.Tabs.UnitsList;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.Monitorable
{
    public class DetailsMonitorableObjectData
    {
        public Object MonitorableObject { get; private set; }

        public bool IsClosed = true;

        public DetailsMonitorableObjectData(Object monitorableObject)
        {
            this.MonitorableObject = monitorableObject;
        }
    }
}
