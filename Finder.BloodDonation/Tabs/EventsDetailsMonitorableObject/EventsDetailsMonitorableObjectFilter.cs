using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.EventsDetailsMonitorableObject
{
    public class EventsDetailsMonitorableObjectFilter : SortedFilter
    {
       [RaisePropertyChanged]
public DateTime? DateUtcFrom { get; set; }
[RaisePropertyChanged]
public DateTime? DateUtcTo { get; set; }

[RaisePropertyChanged]
public string TypeDescription { get; set; }

[RaisePropertyChanged]
public string UnitName { get; set; }

[RaisePropertyChanged]
public string UnitLocation { get; set; }

[RaisePropertyChanged]
public DateTime? EndDateUtcFrom { get; set; }
[RaisePropertyChanged]
public DateTime? EndDateUtcTo { get; set; }

[RaisePropertyChanged]
public string ClosingUserName { get; set; }

[RaisePropertyChanged]
public string ClosingUserLastName { get; set; }

[RaisePropertyChanged]
public string ClosingComment { get; set; }

[RaisePropertyChanged]
public string Message { get; set; }



        public EventsDetailsMonitorableObjectFilter(IUnityContainer container)
            : base(container)
        {

        }
    }
}
