using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.EventsDetailsMonitorableObject
{
    public class EventsDetailsMonitorableObjectRemotePagedDataSource: IPagedDataSource<EventsDetailsMonitorableObjectItemViewModel>
    {
        protected IUnityContainer _container;
        protected int _pageSize;
        protected IDataTable _dataTable;
        protected IUnitDataClient _dataClient;
        private string objectType;
        private string objectId;
        private const int PAGE_SIZE = 20;
        private int patientId;

        public EventsDetailsMonitorableObjectRemotePagedDataSource(IUnityContainer container, int pageSize, IDataTable dataTable, IUnitDataClient dataClient, string objectType, string objectId)
        {
            _container = container;
            _pageSize = pageSize;
            _dataTable = dataTable;
            _dataClient = dataClient;
            this.objectType = objectType;
            this.objectId = objectId;
        }

		 public void FetchData(int pageNumber, Action<PagedDataResponse<EventsDetailsMonitorableObjectItemViewModel>> responseCallback)
        {
            var proxy = _dataClient.GetProxy();
            var list = new List<EventsDetailsMonitorableObjectItemViewModel>();

            proxy.GetEventsDetailsCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<EventsDetailsMonitorableObjectItemViewModel>();
                    vm.Item = item;
                    vm.DataTable = _dataTable;
                    list.Add(vm);
                }
                Dictionary<string, string> fdictt = (Dictionary<string, string>)_dataTable.GetFilterDictionary();

                fdictt["TreeUnitId"] = null;
                fdictt["ObjectIdFrom"] = objectId.ToString();
                fdictt["ObjectIdTo"] = objectId.ToString();
                fdictt["ObjectTypeFrom"] = objectType;
                fdictt["ObjectTypeTo"] = objectType;
                fdictt["SortOrder"] = "-DateUtc";

                _container.Resolve<EventsDetailsMonitorableObjectItemViewModel>().SelectionChanged(-1);
                proxy.GetEventsDetailsFilteredTotalCountAsync(fdictt);
            };

            proxy.GetEventsDetailsFilteredTotalCountCompleted += (s, e) =>
            {
                responseCallback(new PagedDataResponse<EventsDetailsMonitorableObjectItemViewModel>()
                {
                    Items = list,
                    TotalItemCount = e.Result
                });
            };
            
            Dictionary<string, string> fdict = (Dictionary<string, string>)_dataTable.GetFilterDictionary();

            fdict["TreeUnitId"] = null;
            fdict["ObjectIdFrom"] = objectId.ToString();
            fdict["ObjectIdTo"] = objectId.ToString();
            fdict["ObjectTypeFrom"] = objectType;
            fdict["ObjectTypeTo"] = objectType;
            fdict["SortOrder"] = "-DateUtc";
            proxy.GetEventsDetailsAsync(fdict, pageNumber, PAGE_SIZE);
        }
    }
}
