using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataTable;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Authentication;

namespace Finder.BloodDonation.Tabs.EventsDetailsMonitorableObject
{
    public class EventsDetailsMonitorableObjectDataTable : BaseDataTable
    {

        public EventsDetailsMonitorableObjectDataTable(IUnityContainer container)
            : base(container)
        {
        }

        public override ObservableCollection<IDataTableColumn> GetColumns()
        {

            bool isHiddenForUser = !BloodyUser.Current.IsInRole(PermissionNames.HyperAdmin);
		
            return new ObservableCollection<IDataTableColumn>()
             .Add(50)
             .Add(100, "UnitLocation", "Lokalizacja strefy", FilterDataType.String, TextAlignment.Left)
             .Add(100, "TypeDescription", "Zdarzenie", FilterDataType.String, TextAlignment.Left)
             .Add(100, "DateUtc", "Data", FilterDataType.DateTime, TextAlignment.Left)
             .Add(100, "UnitName", "Strefa", FilterDataType.String, TextAlignment.Left)
             .Add(100, "EndDateUtc", "Zakończenie zdarzenia", FilterDataType.DateTime, TextAlignment.Left)
             .Add(100, "ClosingUserName", "Imię zamykającego", FilterDataType.String, TextAlignment.Left)
             .Add(100, "ClosingUserLastName", "Nazwisko zamykającego", FilterDataType.String, TextAlignment.Left)
             .Add(100, "ClosingComment", "Komentarz", FilterDataType.String, TextAlignment.Left)
             .Add(100, "Message", "Komunikat", FilterDataType.String, TextAlignment.Left, isHiddenForUser)
                             ;
        }


        public override ISortedFilter GetFilter(IUnityContainer container)
        {
            return new EventsDetailsMonitorableObjectFilter(container);
        }

		public override bool StartWithDefaultColumns
        {
            get
            {
                return false;
            }
        }
    }
}
