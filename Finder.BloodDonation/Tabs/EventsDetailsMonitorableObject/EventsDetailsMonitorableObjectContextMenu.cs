using Finder.BloodDonation.Context;
using Finder.BloodDonation.Tools;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.EventsDetailsMonitorableObject
{
    [FinderFX.SL.Core.Communication.OnCompleted(AttributeTargetMembers = "regex:Completed")]
    public class EventsDetailsMonitorableObjectContextMenu : GenericContextMenuItemsCollection<EventsDetailsMonitorableObjectItemViewModel>
    {
        private IUnityContainer container { get; set; }

        public EventsDetailsMonitorableObjectContextMenu(EventsDetailsMonitorableObjectItemViewModel vm, IUnityContainer container)
            : base(vm)
        {
            this.container = container;
        }

        public override void RegisterItems(System.Collections.Generic.IList<IContextMenuItem> items)
        {
            /*items.Add(new EventsDetailsMonitorableObjectContextItem(this.AssociatedObject)
            {
                Header = "Edytuj",
                Command = new DelegateCommand<object>(AssociatedObject.OnContextmenuClicked),
                CommandParameter = "Edytuj",
            });*/
        }
    }


    public class EventsDetailsMonitorableObjectContextItem : GenericMenuItem
    {
        private EventsDetailsMonitorableObjectItemViewModel Item { get; set; }
        public EventsDetailsMonitorableObjectContextItem(EventsDetailsMonitorableObjectItemViewModel vm)
        {
            Item = vm;
        }

        public override bool IsAvailable()
        {
            return Item.Item != null;
        }
    }

}
