using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataTable;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Authentication;

namespace Finder.BloodDonation.Tabs.ToolsBands
{
    public class ToolsBandsDataTable : BaseDataTable
    {

        public ToolsBandsDataTable(IUnityContainer container)
            : base(container)
        {
        }

        public override ObservableCollection<IDataTableColumn> GetColumns()
        {

            bool isHiddenForUser = !BloodyUser.Current.IsInRole(PermissionNames.HyperAdmin);

            return new ObservableCollection<IDataTableColumn>()
             .Add(50)
			 .Add(100, "Id", "ID", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "BarCode", "Kod kreskowy", FilterDataType.String, TextAlignment.Left) // v
             .Add(100, "MacString", "MAC", FilterDataType.String, TextAlignment.Left) // v
             .Add(100, "Uid", "UID", FilterDataType.Long, TextAlignment.Left, isHiddenForUser, false) 
             .Add(100, "SerialNumber", "Numer seryjny", FilterDataType.String, TextAlignment.Left, isHiddenForUser)
             .Add(100, "BatteryTypeName", "Typ baterii", FilterDataType.String, TextAlignment.Left) // v
             .Add(100, "BatteryAlarmSeverityName", "Zdarzenie", FilterDataType.String, TextAlignment.Left, isHiddenForUser)
             .Add(100, "BatteryInstallationDateUtc", "Instalacja baterii", FilterDataType.DateTime, TextAlignment.Left)
             .Add(100, "BatteryDaysToExchange", "Dni do wymiany baterii", FilterDataType.Int, TextAlignment.Left) // v
             .Add(100, "Description", "Opis", FilterDataType.String, TextAlignment.Left) // v
             .Add(100, "ObjectTypeName", "Typ obiektu", FilterDataType.String, TextAlignment.Left) // v
             .Add(100, "ObjectDisplayName", "Nazwa", FilterDataType.String, TextAlignment.Left) // v
             .Add(100, "UnitId", "ID jednostki organizacyjnej", FilterDataType.Int, TextAlignment.Left,isHiddenForUser)
             .Add(100, "UnitName", "Nazwa jednostki organizacyjnej", FilterDataType.String, TextAlignment.Left) // v
             .Add(100, "UnitLocation", "Lokalizacja jednostki organizacyjnej", FilterDataType.String, TextAlignment.Left) // v
             .Add(100, "CurrentUnitId", "ID aktualnej strefy", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "CurrentUnitName", "Aktualna strefa", FilterDataType.String, TextAlignment.Left)  // v
             .Add(100, "CurrentUnitLocation", "Lokalizacja aktualnej strefy", FilterDataType.String, TextAlignment.Left, true, false) //v
             ;
        }


        public override ISortedFilter GetFilter(IUnityContainer container)
        {
            return new ToolsBandsFilter(container);
        }

		public override bool StartWithDefaultColumns
        {
            get
            {
                return false;
            }
        }
    }
}
