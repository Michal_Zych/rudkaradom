using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.ToolsBands
{
    public class ToolsBandsRemotePagedDataSource: IPagedDataSource<ToolsBandsItemViewModel>
    {
        protected IUnityContainer _container;
        protected int _pageSize;
        protected IDataTable _dataTable;
        protected IUnitDataClient _dataClient;

        public ToolsBandsRemotePagedDataSource(IUnityContainer container, int pageSize, IDataTable dataTable, IUnitDataClient dataClient)
        {
            _container = container;
            _pageSize = pageSize;
            _dataTable = dataTable;
            _dataClient = dataClient;
        }


		 public void FetchData(int pageNumber, Action<PagedDataResponse<ToolsBandsItemViewModel>> responseCallback)
        {
            var proxy = _dataClient.GetProxy();
            var list = new List<ToolsBandsItemViewModel>();

            //var proxy = _dataClient.GetProxy();
            //var list = new List<BandInfoItemViewModel>();

            proxy.GetBandsInfoFilteredCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<ToolsBandsItemViewModel>();
                    vm.Item = item;
                    vm.DataTable = _dataTable;
                    list.Add(vm);
                }
                _container.Resolve<ToolsBandsItemViewModel>().SelectionChanged(-1);
                proxy.GetBandsInfoFilteredTotalCountAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary());
            };

            proxy.GetBandsInfoFilteredTotalCountCompleted += (s, e) =>
            {
                responseCallback(new PagedDataResponse<ToolsBandsItemViewModel>()
                {
                    Items = list,
                    TotalItemCount = e.Result
                });
            };

            proxy.GetBandsInfoFilteredAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary(), 0, _pageSize);

            /*proxy.GetToolsBandsFilteredCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<ToolsBandsItemViewModel>();
                    vm.Item = item;
                    vm.DataTable = _dataTable;
                    list.Add(vm);
                }
                _container.Resolve<ToolsBandsItemViewModel>().SelectionChanged(-1);
                proxy.GetToolsBandsFilteredTotalCountAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary());
            };

            proxy.GetToolsBandsFilteredTotalCountCompleted += (s, e) =>
            {
                responseCallback(new PagedDataResponse<ToolsBandsItemViewModel>()
                {
                    Items = list,
                    TotalItemCount = e.Result
                });
            };

            proxy.GetToolsBandsFilteredAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary(), pageNumber, _pageSize);
            */
        }
    }
}
