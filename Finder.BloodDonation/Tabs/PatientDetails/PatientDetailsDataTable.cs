using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataTable;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Authentication;

namespace Finder.BloodDonation.Tabs.PatientDetails
{
    public class PatientDetailsDataTable : BaseDataTable
    {

        public PatientDetailsDataTable(IUnityContainer container)
            : base(container)
        {
        }

        public override ObservableCollection<IDataTableColumn> GetColumns()
        {
            bool isHiddenForUser = !BloodyUser.Current.IsInRole(PermissionNames.HyperAdmin);

            return new ObservableCollection<IDataTableColumn>()
          .Add(50)
             .Add(90, "LastName", "Nazwisko", FilterDataType.String, TextAlignment.Left, true, false)
             .Add(75, "Name", "Imi�", FilterDataType.String, TextAlignment.Left, true, false)
             .Add(85, "DisplayName", "Nazwa", FilterDataType.String, TextAlignment.Left)
             .Add(80, "Pesel", "Kod", FilterDataType.String, TextAlignment.Left)
             .Add(125, "WardUnitName", "Oddzia�", FilterDataType.String, TextAlignment.Left)
             .Add(115, "WardAdmissionDate", "Dodano/usuni�to z oddzia�u", FilterDataType.DateTime, TextAlignment.Left)
             .Add(125, "WardUnitLocation", "Lokalizacja oddzia�u", FilterDataType.String, TextAlignment.Left, true, false)
             .Add(115, "RoomUnitName", "Sala", FilterDataType.String, TextAlignment.Left, true, false)
             .Add(105, "RoomAdmissionDate", "Przyj�to/wypisano z sali", FilterDataType.DateTime, TextAlignment.Left, true, false)
             .Add(125, "RoomUnitLocation", "Lokalizacja sali", FilterDataType.String, TextAlignment.Left, true, false)
             .Add(60, "BandId", "ID lokalizatora", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "BandBarCode", "Kod kreskowy lokalizatora", FilterDataType.String, TextAlignment.Left)
             .Add(100, "BandSerialNumber", "Numer seryjny lokalizatora", FilterDataType.String, TextAlignment.Left, false, false)
             .Add(115, "CurrentUnitName", "Aktualna strefa", FilterDataType.String, TextAlignment.Left)
             .Add(125, "CurrentUnitLocation", "Lokalizacja aktualnej strefy", FilterDataType.String, TextAlignment.Left, true, false)
             ;
        }

        public override ISortedFilter GetFilter(IUnityContainer container)
        {
            return new PatientDetailsFilter(container);
        }

		public override bool StartWithDefaultColumns
        {
            get
            {
                return false;
            }
        }
    }
}
