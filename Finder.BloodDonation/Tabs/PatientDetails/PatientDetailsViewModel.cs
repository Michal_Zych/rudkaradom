using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Events;
using System.Collections.Generic;
using System.Collections;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Dialogs.Patients;
using Finder.BloodDonation.Dialogs.Helpers;
using Microsoft.Practices.Composite.Presentation.Events;
using System.Diagnostics;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.Dialogs.Bands;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.UsersService;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.LanguageHelpher;
using Finder.BloodDonation.Tabs.PatientDetails.PatientAlarming;
using Finder.BloodDonation.DynamicData.DBs;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Dialogs.Success;
using Finder.BloodDonation.Tabs.MenuAdministration.SettingsSystem;

namespace Finder.BloodDonation.Tabs.PatientDetails
{
    [OnCompleted]
    [UIException]
    public class PatientDetailsViewModel : ViewModelBase, ITabViewModel
    {
        private readonly IWindowsManager _windowsManager;

        public event EventHandler LoadingCompleted = delegate { };
        private IUnit SelectedUnit = null;
        private IUnit _currentUnit;

        public RelayCommand ConfigurationCommand { get; set; }
        public RelayCommand CreatePatientInfoCommand { get; set; }
        public RelayCommand EditPatients { get; set; }
        public RelayCommand DetailsCommand { get; set; }
        public RelayCommand LocalizeClickCommand { get; set; }


        private PatientDTO selected;

        private PatientDetailsDto selectedItem;

        private UsersServiceClient Proxy { get; set; }
        private UnitsServiceClient UnitsProxy { get; set; }

        private PatientEditData _patientEditData;
        private PatientConfigurationData _patientConfigurationData;
        private PatientAlarmingData _patientAlarmingData;

        private List<BandStatusDto> _bandsStatusesList;

        [RaisePropertyChanged]
        public string Registration_ToolTip { get; set; }
        [RaisePropertyChanged]
        public string Edit_ToolTip { get; set; }
        [RaisePropertyChanged]
        public string Details_ToolTip { get; set; }
        [RaisePropertyChanged]
        public string Location_ToolTip { get; set; }
        [RaisePropertyChanged]
        public string Administration_ToolTip { get; set; }
        [RaisePropertyChanged]
        public string Configuration_ToolTip { get; set; }

        [RaisePropertyChanged]
        public string RegisterButton
        {
            get
            {
                return LanguageManager.ControlsText.Text(ControlsTextCodes.REGISTRATION_BUTTON);
            }
        }

        private void SetToolTips()
        {
            Registration_ToolTip = LanguageManager.GetToolTip(ToolTipCodes.REGISTRATION);
            Edit_ToolTip = LanguageManager.GetToolTip(ToolTipCodes.EDIT);
            Details_ToolTip = LanguageManager.GetToolTip(ToolTipCodes.DETAILS);
            Location_ToolTip = LanguageManager.GetToolTip(ToolTipCodes.LOCATION);
            Administration_ToolTip = LanguageManager.GetToolTip(ToolTipCodes.ADMINISTRATION);
            Configuration_ToolTip = LanguageManager.GetToolTip(ToolTipCodes.CONFIGURATION);
        }

        [RaisePropertyChanged]
        public bool RegistrationVisible { get; set; }

        [RaisePropertyChanged]
        public int AdministrationSelectedIndex { get; set; }

        private PatientAssignData _patientAssignData;
        private AssignBandData _assignBandData;

        private IUnitFiltered _details;
        [RaisePropertyChanged]
        public IUnitFiltered Details
        {
            get
            {
                if (_details == null)
                {
                    _details = Container.Resolve<PatientDetailsDetailsListViewModel>();
                }
                return _details;
            }
            set
            {
                _details = value;
            }
        }

        public void OnPingUserSession(string info)
        {
            //Refresh(_currentUnit);
        }

        private void Proxy_AssignPatientToWardRoomCompleted(object sender, AssignPatientToWardRoomCompletedEventArgs e)
        {
            //Refresh(_currentUnit);
        }

        private void Proxy_AssignBandToObjectCompleted(object sender, AssignBandToObjectCompletedEventArgs e)
        {
            //Refresh(_currentUnit);
        }

        public RelayCommand AdministrationSelectionChanged { get; set; }

        public void OnCreatePatientInfo()
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling);

            _patientEditData = new PatientEditData();
            _patientEditData.Patient = new PatientDTO();

            this._windowsManager.ShowWindow(WindowsKeys.EditPatient, _patientEditData);
        }
        
        public PatientDetailsViewModel(IUnityContainer container)
            : base(container)
        {
            //DbBandStatus.Initialize(container);

            AdministrationSelectedIndex = -1;

            this.ViewAttached += new EventHandler(ViewModel_ViewAttached);
            EventAggregator.GetEvent<SelectedItemChangedEvent<PatientDetailsDto>>().Subscribe(OnSelectedItemChangedEvent);
            EventAggregator.GetEvent<RefreshCloseAlarmsListeners>().Subscribe(OnRefreshFromClosedAllarms);
            EventAggregator.GetEvent<PingUserSessionEvent>().Subscribe(OnPingUserSession);

            var viewFactory = container.Resolve<ViewFactory>();
            _windowsManager = this.Container.Resolve<IWindowsManager>();

            CreatePatientInfoCommand = new RelayCommand(OnCreatePatientInfo);
            EditPatients = new RelayCommand(OnEditPatientDetails);
            AdministrationSelectionChanged = new RelayCommand(OnAdministrationSelectionChanged);
            DetailsCommand = new RelayCommand(OnDetailsWindowOpen);
            LocalizeClickCommand = new RelayCommand(OnLocalizeClick);

            if (_windowsManager != null)
            {
                _windowsManager.RegisterWindow(WindowsKeys.AssignBand, "",
                    viewFactory.CreateView<AssignBand>(), new Size(560, 320), true, true);
                _windowsManager.RegisterWindow(WindowsKeys.EditPatient, "",
                    viewFactory.CreateView<EditPatient>(), new Size(490, 520), true);
                _windowsManager.RegisterWindow(WindowsKeys.PatientAssign, "",
                    viewFactory.CreateView<PatientAssign>(), new Size(600, 400), true);
                _windowsManager.RegisterWindow(WindowsKeys.PatientAlarmingView, "",
                    viewFactory.CreateView<PatientAlarmingView>(), new Size(1280, 750), true,true);
                _windowsManager.RegisterWindowIfNotRegister(WindowsKeys.SuccessDialog, "Powodzenie",
                    viewFactory.CreateView<Success>(), new Size(440, 170), true);
            }

            Proxy = ServiceFactory.GetService<UsersServiceClient>(/*ServicesUri.UsersService*/);

            UnitsProxy = ServiceFactory.GetService<UnitsServiceClient>();
            UnitsProxy.AssignPatientToWardRoomCompleted += Proxy_AssignPatientToWardRoomCompleted;
            UnitsProxy.AssignBandToObjectCompleted += Proxy_AssignBandToObjectCompleted;
            UnitsProxy.PatientFromHospitalCompleted += UnitsProxy_PatientFromHospitalCompleted;
            UnitsProxy.GetBandsStatusesCompleted += UnitsProxy_GetBandsStatusesCompleted;

            SetToolTips();
        }

        public void UnitsProxy_GetBandsStatusesCompleted(object sender, GetBandsStatusesCompletedEventArgs e)
        {
            if(e.Result != null)
            {
                _bandsStatusesList = e.Result;
            }
        }
        /// <summary>
        /// Zdarzenie od�wie�enia tabeli po zamkni�ciu alarmu.
        /// </summary>
        /// <param name="obj"></param>
        public void OnRefreshFromClosedAllarms(object obj)
        {

            Refresh(_currentUnit);
        }

        public static int? selectedPatientId = null;

        public void OnLocalizeClick()
        {
            try
            {
                if (selectedItem != null)
                {
                    if (_bandsStatusesList != null)
                    {
                        var patientStatus = _bandsStatusesList.FirstOrDefault(x => x.ObjectId == selectedItem.Id && x.ObjectType == 4);

                        if (patientStatus != null)
                        {
                            if (patientStatus.CurrentUnitId != null || patientStatus.PreviousUnitId != null)
                            {
                                bool unitIsAvailable; // flaga czy mamy dost�p do unita gdzie pacjent si� znajduje

                                if (patientStatus.CurrentUnitId != null)
                                {
                                    selectedPatientId = selectedItem.Id;

                                    int currentUnitId = 0;
                                    int currentSectionId = 0;

                                    currentUnitId = (int)patientStatus.CurrentUnitId;

                                    // try dlatego, �e jak u�ytkownik nie ma uprawnie�, to nie ma wszystkich oddzia��w na li�cie - wi�c where si� generalnie sypnie
                                    // wtedy poka�emy po prostu jego obecn� lokalizacj�, bo pacjent i tak znajduje si� w strefie do kt�rej dany u�ytkownik nie ma uprawnie�
                                    try
                                    {
                                        if (currentUnitId != 0 && !UnitManager.Instance.Units.Where(x => x.ID == currentUnitId).FirstOrDefault().HasChildren()) // li��
                                            currentSectionId = UnitManager.Instance.Units.Where(x => x.Children.Any(y => y.ID == currentUnitId)).FirstOrDefault().ID; // szukamy parenta
                                    }
                                    catch(Exception ex)
                                    {
                                        currentSectionId = 0;
                                        currentUnitId = (int)patientStatus.CurrentUnitId;
                                    }

                                    unitIsAvailable = UnitManager.Instance.Units.Any(x => x.ID == currentUnitId); // sprawdzamy czy mamy takiego unita na li�cie

                                    if (Convert.ToInt32(currentUnitId) > 0 && selectedPatientId != null && unitIsAvailable)
                                    {
                                        EventAggregator.GetEvent<ExpandAccordionEvent>().Publish(AccordionItemsNames.UNITS);
                                        EventAggregator.GetEvent<ChangeSelectedUnitEvent>().Publish(currentSectionId != 0 ? currentSectionId : currentUnitId);

                                        ITabManager TabManager = Container.Resolve<ITabManager>();
                                        TabManager.SelectTab(TabNames.Localization,
                                            (e) =>
                                            {
                                                EventAggregator.GetEvent<ForwardingMessageEvent>().Publish((int)selectedPatientId);
                                            });

                                        selectedPatientId = null;
                                    }

                                    if (currentUnitId > 0 && !unitIsAvailable)
                                    {
                                        MsgBox.Warning("Pacjent znajduje si� w lokalizacji: " + patientStatus.CurrentUnitLocation + Container.Resolve<ISettingsRespository>().Settings.UnitsLocationSeparator + patientStatus.CurrentUnitName);
                                    }
                                }
                                else
                                {
                                    selectedPatientId = selectedItem.Id;

                                    int previousUnitId = 0;
                                    int previousSectionId = 0;

                                    try
                                    {
                                        previousUnitId = (int)patientStatus.PreviousUnitId;
                                        if (previousUnitId != 0 && !UnitManager.Instance.Units.Where(x => x.ID == previousUnitId).FirstOrDefault().HasChildren()) // li��
                                            previousSectionId = UnitManager.Instance.Units.Where(x => x.Children.Any(y => y.ID == previousUnitId)).FirstOrDefault().ID; // szukamy parenta 
                                    }
                                    catch (Exception ex)
                                    {
                                        previousSectionId = 0;
                                        previousUnitId = (int)patientStatus.PreviousUnitId;
                                    }

                                    unitIsAvailable = UnitManager.Instance.Units.Any(x => x.ID == previousUnitId);

                                    if (Convert.ToInt32(previousUnitId) > 0 && selectedPatientId != null)
                                    {
                                        EventAggregator.GetEvent<ExpandAccordionEvent>().Publish(AccordionItemsNames.UNITS);
                                        EventAggregator.GetEvent<ChangeSelectedUnitEvent>().Publish(previousSectionId != 0 ? previousSectionId : previousUnitId);

                                        ITabManager TabManager = Container.Resolve<ITabManager>();
                                        TabManager.SelectTab(TabNames.Localization,
                                            (e) =>
                                            {
                                                EventAggregator.GetEvent<ForwardingMessageEvent>().Publish((int)selectedPatientId);
                                            });

                                        selectedPatientId = null;
                                    }

                                    if (previousUnitId > 0 && !unitIsAvailable)
                                    {
                                        MsgBox.Warning("Pacjent ostatnio zajdowa� si� w nast�puj�cej lokalizacji: " + patientStatus.PreviousUnitLocation + Container.Resolve<ISettingsRespository>().Settings.UnitsLocationSeparator + patientStatus.PreviousUnitName);
                                    }
                                }
                            }
                            else
                            {
                                MsgBox.Warning("Pacjent nigdy nie znajdowa� si� w �adnej lokalizacji.");
                                return;
                            }
                        }
                        else
                        {
                            MsgBox.Warning("Nie mo�na zlokalizowa� pacjenta - pacjent nie ma przypisanej opaski.");
                            return;
                        }
                    }
                }
                else
                {
                    MsgBox.Warning("Nie wybrano pacjenta!");
                }
            }
            catch(Exception e)
            {
                MsgBox.Warning("Pacjent nigdy nie znajdowa� si� w �adnej lokalizacji.");
            }
        }

        void UnitsProxy_PatientFromHospitalCompleted(object sender, PatientFromHospitalCompletedEventArgs e)
        {
            if (e.Result.Equals(selectedItem.Id.ToString()))
            {
                _windowsManager.ShowWindow(WindowsKeys.SuccessDialog, "Pomy�lnie wypisano pacjenta ze szpitala!");
                MsgBox.Succes("Pomy�lnie wypisano pacjenta ze szpitala!");
                Refresh(_currentUnit);
            }
            else
            {
                MsgBox.Error(e.Result);
            }
        }

        public void OnDetailsWindowOpen()
        {
            if (selectedItem != null)
            {
                _patientAlarmingData = new PatientAlarmingData();
                _patientAlarmingData.PatientId = selectedItem.Id;
                _patientAlarmingData.PatientConfiguration = new PatientConfigurationData() { PatientConfigData = new PatientConfigurationDTO() };
                _patientAlarmingData.PatientRaport = new RaportsData();
                _patientAlarmingData.PatientAlarmsList = new List<Controls.Alarm>();
                _patientAlarmingData.Pesel = selectedItem.Pesel;
                _patientAlarmingData.FirstName = selectedItem.Name;
                _patientAlarmingData.LastName = selectedItem.LastName;
                _patientAlarmingData.AssignmentDepartment = selectedItem.WardUnitName;
                _patientAlarmingData.AssignmentRoom = selectedItem.RoomUnitName;

                if (selectedItem.RoomId != null)
                    _patientAlarmingData.AssignUnitId = (int)selectedItem.RoomId;
                else if (selectedItem.WardId != null)
                    _patientAlarmingData.AssignUnitId = (int)selectedItem.WardId;

                if (BloodyUser.Current.IsInRole(PermissionNames.PatientDetails))
                {
                    _windowsManager.ShowWindow(WindowsKeys.PatientAlarmingView, _patientAlarmingData);
                }
                else
                {
                    MsgBox.Warning("Nie masz uprawnie� do przegl�dania tego okna.");
                }
            }
            else
            {
                MsgBox.Error(LanguageManager.GetError(ErrorCodes.NON_SELLECTION_PATIENT));
            }
        }

        private void OnConfigurationPatient()
        {
            _patientConfigurationData = new PatientConfigurationData()
            {
                PatientConfigData = new PatientConfigurationDTO()
            };

            _windowsManager.ShowWindow(WindowsKeys.ConfigurationPatient, _patientConfigurationData);
        }

        private void OnEditPatientDetails()
        {
            if (selectedItem != null)
            {
                EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling);

                _patientEditData = new PatientEditData();
                _patientEditData.Patient = new PatientDTO()
                {
                    ID = selectedItem.Id,
                    DepartmentId = selectedItem.WardId,
                    DepartmentName = selectedItem.WardUnitName,
                    FirstName = selectedItem.Name,
                    LastName = selectedItem.LastName,
                    Pesel = selectedItem.Pesel,
                    RoomId = selectedItem.RoomId,
                    RoomName = selectedItem.RoomUnitName,
                    BandId = (short?)selectedItem.BandId,
                    BandBarCode = selectedItem.BandBarCode
                };

                this._windowsManager.ShowWindow(WindowsKeys.EditPatient, _patientEditData);
            }
            else
            {
                MsgBox.Error(LanguageManager.GetError(ErrorCodes.NON_SELLECTION_PATIENT));
            }
        }

        private void CloseAssignBandWindowEventHandling(string obj)
        {
            //Testowo zakomentowane automatyczne od�wie�anie
            if (obj == "AssignBand")
                Refresh(_currentUnit);
        }

        private void CloseAssignWindowEventHandling(string obj)
        {
            Refresh(_currentUnit);
        }

        public void OnSelectedItemChangedEvent(IItemViewModel<PatientDetailsDto> obj)
        {
            if (obj == null)
            {
                selectedItem = null;
            }
            else
            {
                selectedItem = obj.Item;
            }
            SelectedItemChanged();
        }

        private void SelectedItemChanged()
        {

        }

        private void ViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public void Refresh(IUnit unit)
        {
            AdministrationSelectedIndex = -1;
            SelectedUnit = unit;
            _currentUnit = unit;

            Details.Refresh(unit);
            UnitsProxy.GetBandsStatusesAsync(null);

            if (CheckIfSelectedUnitIsRegistration(SelectedUnit))
                RegistrationVisible = true;
            else
                RegistrationVisible = false;  // Zmieni� na false.
        }

        private bool CheckIfSelectedUnitIsRegistration(IUnit selectedUnit)
        {
            UnitDTO _temp = (UnitDTO)UnitManager.Instance.Units.Where(e => e.ID == selectedUnit.Identity).FirstOrDefault();

            if (_temp == null)
                return false;
            if (_temp.ID == UnitTypeOrganizer.GetRegistartionUnitId())
                return true;

            if (_temp.Parent == null)
                return false;

            UnitDTO _parent = _temp.Parent;

            while (_parent.ID != UnitTypeOrganizer.GetRegistartionUnitId())
            {
                _parent = _parent.Parent;

                if (_parent == null)
                {
                    return false;
                }
            }

            if (_parent == null)
            {
                return false;
            }
            if (_parent.ID == UnitTypeOrganizer.GetRegistartionUnitId())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        public string Title
        {
            get { return ""; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }

        public void CloseWindowEventHandling(string key)
        {
            Refresh(_currentUnit);
            EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(CloseWindowEventHandling);
        }

        public void OnAdministrationSelectionChanged()
        {
            
            if (selectedItem == null)
            {
                MsgBox.Error(LanguageManager.GetError(ErrorCodes.NON_SELLECTION_PATIENT));
                AdministrationSelectedIndex = -1;
                return;
            }

            const byte PATIENT_OBJECT_TYPE = 4;

            _assignBandData = new AssignBandData();
            _assignBandData.AssignedBandInfo = new AssignedBandInfoDto();
            _assignBandData.AssignedBandInfo.BandId = (short?)selectedItem.BandId;
            _assignBandData.AssignedBandInfo.ObjectId = selectedItem.Id;// (int)selected.ID;
            _assignBandData.AssignedBandInfo.ObjectType = PATIENT_OBJECT_TYPE;
            _assignBandData.AssignedBandInfo.OperatorId = BloodyUser.Current.ID;
            _assignBandData.PatientName = selectedItem.Name + " " + selectedItem.LastName;

            _windowsManager.ShowWindow(WindowsKeys.AssignBand, _assignBandData);
            EventAggregator.GetEvent<CloseWindowEvent>()
                .Subscribe(CloseAssignBandWindowEventHandling, ThreadOption.UIThread, true);

            AdministrationSelectedIndex = -1;
        }
    }
}
