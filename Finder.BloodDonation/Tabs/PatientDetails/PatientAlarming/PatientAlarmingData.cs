﻿using Finder.BloodDonation.Controls;
using Finder.BloodDonation.Dialogs.Patients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.PatientDetails.PatientAlarming
{
    public class PatientAlarmingData
    {
        public int PatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Pesel { get; set; }
        public string AssignmentDepartment { get; set; }
        public string AssignmentRoom { get; set; }
        public int? AssignUnitId { get; set; }
        public List<Alarm> PatientAlarmsList { get; set; }
        public PatientConfigurationData PatientConfiguration { get; set; }
        public RaportsData PatientRaport { get; set; }

        public bool IsClosed { get; set; }
    }
}
