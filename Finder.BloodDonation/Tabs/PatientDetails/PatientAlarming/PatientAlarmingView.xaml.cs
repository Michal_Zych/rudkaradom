﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;

namespace Finder.BloodDonation.Tabs.PatientDetails.PatientAlarming
{
    [ViewModel(typeof(PatientAlarmingViewModel))]
    public partial class PatientAlarmingView : UserControl
    {
        public PatientAlarmingView()
        {
            InitializeComponent();
        }

        private void StackPanel_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            onlyAlarm_Button.Style = Application.Current.Resources["TabButtonUnselected"] as Style;
            allEvents_Button.Style = Application.Current.Resources["TabButtonUnselected"] as Style;
            historyOnStay_Button.Style = Application.Current.Resources["TabButtonUnselected"] as Style;

            ((Button)sender).Style = Application.Current.Resources["TabButtonSelected"] as Style;
        }
    }
}
