﻿using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Common.Messages;
using Finder.BloodDonation.Dialogs;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Dialogs.Patients;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Tabs.EventsDetails;
using Finder.BloodDonation.Tabs.Monitorable;
using Finder.BloodDonation.Tabs.Reports.BandsTracking;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Practices.Unity;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Tabs.PatientDetails.PatientAlarming
{
    public class PatientAlarmingViewModel : ViewModelBase, IDialogWindow
    {
        [RaisePropertyChanged]
        public string Title { get; set; }

        [RaisePropertyChanged]
        public EventsDetailsDto Alarm { get; set; }

        [RaisePropertyChanged]
        public PatientAlarmingData PatientData { get; set; }

        [RaisePropertyChanged]
        public Visibility AlarmButtonVisibility { get; set; }

        [RaisePropertyChanged]
        public Visibility PatientConfigurationVisibility { get; set; }

        [RaisePropertyChanged]
        public Visibility PatientReportsVisibility { get; set; }

        [RaisePropertyChanged]
        public Visibility PatientEventsVisibility { get; set; }

        private PatientInfoMessage patientInfoMessage;

        public RelayCommand CloseAlarm { get; set; }

        public RelayCommand CancelClick { get; set; }
        public RelayCommand OkClick { get; set; }
        public RelayCommand EventsClick { get; set; }

        public RelayCommand CopyToClipboard { get; set; }
        public RelayCommand PasteFromClipboard { get; set; }
        public RelayCommand PasteDefaultData { get; set; }

        public RelayCommand OnlyAlarms { get; set; }
        public RelayCommand HistoryOfStay { get; set; }
        public RelayCommand AllEvents { get; set; }

        public ReportInfo ReportInfo { get; set; }
        private UnitsServiceClient Proxy { get; set; }

        private IUnitFiltered _details;
        [RaisePropertyChanged]
        public IUnitFiltered Details
        {
            get
            {
                if (_details == null)
                {
                    _details = Container.Resolve<ConfigurationPatientModel>();
                }
                return _details;
            }
            set
            {
                _details = value;
            }
        }

        private IUnitFiltered _monitorableDetails;
        [RaisePropertyChanged]
        public IUnitFiltered MonitorableDetails
        {
            get
            {
                if(_monitorableDetails == null)
                {
                    _monitorableDetails = Container.Resolve<DetailsMonitorableObjectViewModel>();
                }
                return _monitorableDetails;
            }
            set
            {
                _monitorableDetails = value;
            }
        }

        private ITabViewModel _bandsTrackingDetails;
        [RaisePropertyChanged]
        public ITabViewModel BandsTrackingDetails 
        {
            get
            {
                if(_bandsTrackingDetails == null)
                {
                    _bandsTrackingDetails = Container.Resolve<BandsTrackingReportsListViewModel>();
                }
                
                ReportInfo = new Common.ReportInfo() { ReportTypes = Enums.ReportTypes.Events, ObjectId = PatientData.PatientId };
                EventAggregator.GetEvent<SetBandsTrackingReportViewTypeEvent>().Publish(ReportInfo);
                
                return _bandsTrackingDetails;
            }
            set
            {
                _bandsTrackingDetails = value;
            }
        }

        string WindowTag { get { return "PatientsTab"; } }

        private ITabViewModel _events;
        [RaisePropertyChanged]
        public ITabViewModel Events
        {
            get
            {
                if(_events == null)
                {
                    patientInfoMessage = new PatientInfoMessage() { 
                        Id = PatientData.PatientId.ToString(), 
                        FirstName = PatientData.FirstName,
                        LastName = PatientData.LastName, 
                        Pesel = PatientData.Pesel, 
                        AssignmentDepartment = PatientData.AssignmentDepartment, 
                        AssignmentRoom = PatientData.AssignmentRoom 
                    };

                    _events = Container.Resolve<EventsDetailsViewModel>();
                    (_events as EventsDetailsViewModel).CloseAlarmTag = WindowTag;
                }
                return _events;
            }
            set
            {
                _events = value;
            }
        }
        
        public PatientAlarmingViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            CloseAlarm = new RelayCommand(OnCloseAlarm);

            OkClick = new RelayCommand(OnOkClick);
            CancelClick = new RelayCommand(OnCancelClick);
            EventsClick = new RelayCommand(OnEventsClick);

            CopyToClipboard = new RelayCommand(OnCopyToClipBoard);
            PasteFromClipboard = new RelayCommand(OnPasteFromClipBoard);
            PasteDefaultData = new RelayCommand(OnPasteDefaultData);

            OnlyAlarms = new RelayCommand(OnOnlyAlarms);
            HistoryOfStay = new RelayCommand(OnHistoryOfStay);
            AllEvents = new RelayCommand(OnAllEvents);

            EventAggregator.GetEvent<SendAfterRegisterResolveEvent>().Subscribe(OnSendAgain);
            EventAggregator.GetEvent<RequestToRepeatMessageEvent>().Subscribe(OnRepeatSend);

            Proxy = ServiceFactory.GetService<UnitsServiceClient>();
            Proxy.GetPatientConfigurationCompleted += Proxy_GetPatientConfigurationCompleted;
            Proxy.SetPatientConfigurationCompleted += Proxy_SetPatientConfigurationCompleted;

            AlarmButtonVisibility = Visibility.Collapsed;
        }

        public void OnRepeatSend(object obj)
        {
            EventAggregator.GetEvent<SetBandsTrackingReportViewTypeEvent>().Publish(ReportInfo);
        }

        public void OnCloseAlarm()
        {
            EventAggregator.GetEvent<CloseAlarmForPatientEvent>().Publish(WindowTag);
        }

        public void OnAllEvents()
        {
            AlarmButtonVisibility = Visibility.Collapsed;
            EventAggregator.GetEvent<GetAllEventsEvent>().Publish(null);
        }

        public void OnHistoryOfStay()
        {
            AlarmButtonVisibility = Visibility.Collapsed;
            EventAggregator.GetEvent<GetHistoryOfStayEvent>().Publish(null);
        }

        public void OnOnlyAlarms()
        {
            AlarmButtonVisibility = Visibility.Visible;
            EventAggregator.GetEvent<GetOnlyAlarmsEvent>().Publish(null);
        }

        public void OnSendAgain(object obj)
        {
            if (PatientData != null)
            {
                EventAggregator.GetEvent<ChangeEventButtonVisibilityForUnitEvent>().Publish(Visibility.Collapsed);
                Messenger.Default.Send<PatientConfigurationData>(PatientData.PatientConfiguration);
                Messenger.Default.Send<PatientInfoMessage>(
                    new PatientInfoMessage() { 
                        Id = PatientData.PatientId.ToString(),
                        FirstName = PatientData.FirstName,
                        LastName = PatientData.LastName,
                        Pesel = PatientData.Pesel,
                        AssignmentDepartment = PatientData.AssignmentDepartment,
                        AssignmentRoom = PatientData.AssignmentRoom 
                    });
            }
        }

        public void OnPasteDefaultData()
        {
            EventAggregator.GetEvent<PasteDefaultDataEvent>().Publish(null);
        }

        public void OnPasteFromClipBoard()
        {
            EventAggregator.GetEvent<PasteFromClipboardEvent>().Publish(null);
        }

        public void OnCopyToClipBoard()
        {
            EventAggregator.GetEvent<CopyToClipboardEvent>().Publish(null);
        }

        public void OnEventsClick()
        {
            Messenger.Default.Send<PatientInfoMessage>(
                new PatientInfoMessage()
                {
                    Id = PatientData.PatientId.ToString(),
                    FirstName = PatientData.FirstName,
                    LastName = PatientData.LastName,
                    Pesel = PatientData.Pesel,
                    AssignmentDepartment = PatientData.AssignmentDepartment,
                    AssignmentRoom = PatientData.AssignmentRoom
                });
        }

        private void OnPatientConfigutationDataChanged(PatientConfigurationData obj)
        {
            if (obj == null)
                return;

            if(obj != PatientData.PatientConfiguration)
            {
                PatientData.PatientConfiguration = obj;
            }
        }

        void Proxy_SetPatientConfigurationCompleted(object sender, SetPatientConfigurationCompletedEventArgs e)
        {
            if(e.Result != null)
            {
                MsgBox.Succes("Sukces!");
            }
        }

        public void Proxy_GetPatientConfigurationCompleted(object sender, GetPatientConfigurationCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                PatientData.PatientConfiguration = new PatientConfigurationData()
                {
                    PatientUnitId = PatientData.AssignUnitId,
                    PatientConfigData = e.Result
                };

                if (PatientData.PatientConfiguration != null)
                {
                    Messenger.Default.Send<PatientConfigurationData>(PatientData.PatientConfiguration);
                }
            }
        }

        public object Data
        {
            set
            {
                EventAggregator.GetEvent<EventsFilteredMethodChanger>().Publish(EventsFilteredType.MonitorableObject);
                EventAggregator.GetEvent<ChangeEventButtonVisibilityForUnitEvent>().Publish(Visibility.Collapsed);

                CheckCredential();

                EventsDetailsDetailsListViewModel.unitToRefreshed = null;
                PatientData = (PatientAlarmingData)value;

                if(PatientData != null)
                {
                    Title = "Szczegóły pacjenta: " + PatientData.FirstName + " " + PatientData.LastName;
                    Proxy.GetPatientConfigurationAsync(PatientData.PatientId);
                    OnEventsClick();

                    ReportInfo = new Common.ReportInfo() { ReportTypes = Enums.ReportTypes.Events, ObjectId = PatientData.PatientId };
                    EventAggregator.GetEvent<SetBandsTrackingReportViewTypeEvent>().Publish(ReportInfo);
                }
            }
        }

        private void CheckCredential()
        {
            if (BloodyUser.Current.IsInRole(PermissionNames.PatientConfiguration))
            {
                PatientConfigurationVisibility = Visibility.Visible;
            }
            else
            {
                PatientConfigurationVisibility = Visibility.Collapsed;
            }

            if(BloodyUser.Current.IsInRole(PermissionNames.PatientEvents))
            {
                PatientEventsVisibility = Visibility.Visible;
            }
            else
            {
                PatientEventsVisibility = Visibility.Collapsed;
            }

            if(BloodyUser.Current.IsInRole(PermissionNames.PatientReports))
            {
                PatientReportsVisibility = Visibility.Visible;
            }
            else
            {
                PatientReportsVisibility = Visibility.Collapsed;
            }
        }

        private void OnCancelClick()
        {
            
        }

        private void OnOkClick()
        {
            Close(CloseButton.OK);
        }

        private void Close(CloseButton closeButton)
        {
            if(closeButton == CloseButton.OK)
            {
                EventAggregator.GetEvent<OnSaveAllPatientDetailsTabs>().Publish(null);
                string errorMsg = "";

                SaveData();
            }
            else if (closeButton.Equals(CloseButton.Cancel))
            {
                Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
            }
        }

        private void SaveData()
        {
            PatientData.PatientConfiguration.PatientConfigData.PatientId = PatientData.PatientId;
        }

        public event EventHandler LoadingCompleted;

        
        public bool ExitButtonVisible
        {
            get { throw new NotImplementedException(); }
        }

        public string WindowKey
        {
            get { return WindowsKeys.PatientAlarmingView; }
        }
    }
}
