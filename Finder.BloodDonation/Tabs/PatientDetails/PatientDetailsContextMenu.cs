using Finder.BloodDonation.Context;
using Finder.BloodDonation.Tools;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.PatientDetails
{
    [FinderFX.SL.Core.Communication.OnCompleted(AttributeTargetMembers = "regex:Completed")]
    public class PatientDetailsContextMenu : GenericContextMenuItemsCollection<PatientDetailsItemViewModel>
    {
        private IUnityContainer container { get; set; }

        public PatientDetailsContextMenu(PatientDetailsItemViewModel vm, IUnityContainer container)
            : base(vm)
        {
            this.container = container;
        }

        public override void RegisterItems(System.Collections.Generic.IList<IContextMenuItem> items)
        {
            items.Add(new PatientDetailsContextItem(this.AssociatedObject)
            {
                Header = "Edytuj",
                Command = new DelegateCommand<object>(AssociatedObject.OnContextmenuClicked),
                CommandParameter = "Edytuj",
            });
        }
    }


    public class PatientDetailsContextItem : GenericMenuItem
    {
        private PatientDetailsItemViewModel Item { get; set; }
        public PatientDetailsContextItem(PatientDetailsItemViewModel vm)
        {
            Item = vm;
        }

        public override bool IsAvailable()
        {
            return Item.Item != null;
        }
    }

}
