using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.PatientDetails
{
    public class PatientDetailsRemotePagedDataSource: IPagedDataSource<PatientDetailsItemViewModel>
    {
        protected IUnityContainer _container;
        protected int _pageSize;
        protected IDataTable _dataTable;
        protected IUnitDataClient _dataClient;

        public PatientDetailsRemotePagedDataSource(IUnityContainer container, int pageSize, IDataTable dataTable, IUnitDataClient dataClient)
        {
            _container = container;
            _pageSize = pageSize;
            _dataTable = dataTable;
            _dataClient = dataClient;
        }

		public void FetchData(int pageNumber, Action<PagedDataResponse<PatientDetailsItemViewModel>> responseCallback)
        {
            var proxy = _dataClient.GetProxy();
            var list = new List<PatientDetailsItemViewModel>();
            
            proxy.GetPatientDetailsCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<PatientDetailsItemViewModel>();
                    vm.Item = item;
                    vm.DataTable = _dataTable;
                    list.Add(vm);
                }
                _container.Resolve<PatientDetailsItemViewModel>().SelectionChanged(-1);
                proxy.GetPatientDetailsFilteredTotalCountAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary());
            };

            proxy.GetPatientDetailsFilteredTotalCountCompleted += (s, e) =>
            {
                responseCallback(new PagedDataResponse<PatientDetailsItemViewModel>()
                {
                    Items = list,
                    TotalItemCount = e.Result
                });
            };

            proxy.GetPatientDetailsAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary(), pageNumber, _pageSize);
        }
    }
}
