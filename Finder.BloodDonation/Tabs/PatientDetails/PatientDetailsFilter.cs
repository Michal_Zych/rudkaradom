using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.PatientDetails
{
    public class PatientDetailsFilter : SortedFilter
    {
       [RaisePropertyChanged]
public string Name { get; set; }

[RaisePropertyChanged]
public string LastName { get; set; }

[RaisePropertyChanged]
public string DisplayName { get; set; }

[RaisePropertyChanged]
public string Pesel { get; set; }

[RaisePropertyChanged]
public string WardUnitName { get; set; }

[RaisePropertyChanged]
public string WardUnitLocation { get; set; }

[RaisePropertyChanged]
public DateTime? WardAdmissionDateFrom { get; set; }
[RaisePropertyChanged]
public DateTime? WardAdmissionDateTo { get; set; }

[RaisePropertyChanged]
public string RoomUnitName { get; set; }

[RaisePropertyChanged]
public string RoomUnitLocation { get; set; }

[RaisePropertyChanged]
public DateTime? RoomAdmissionDateFrom { get; set; }
[RaisePropertyChanged]
public DateTime? RoomAdmissionDateTo { get; set; }

[RaisePropertyChanged]
public string BandId { get; set; }

[RaisePropertyChanged]
public string BandBarCode { get; set; }

[RaisePropertyChanged]
public string BandSerialNumber { get; set; }

[RaisePropertyChanged]
public string CurrentUnitName { get; set; }

[RaisePropertyChanged]
public string CurrentUnitLocation { get; set; }



        public PatientDetailsFilter(IUnityContainer container)
            : base(container)
        {

        }
    }
}
