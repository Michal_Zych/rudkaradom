﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using System.Threading;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Threading;
using Telerik.Windows.Input;
using MouseButtonEventArgs = System.Windows.Input.MouseButtonEventArgs;

namespace Finder.BloodDonation.Tabs.LocalizationConfig
{
    [ViewModel(typeof(LocalizationConfigViewModel))]
    public partial class LocalizationConfigView : UserControl
    {
        private DateTime _pressed;

        public LocalizationConfigView()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void NumericUpDown_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void test_NumericUpDown_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if(DateTime.Now.Subtract(_pressed) > new TimeSpan(0,0,3))
            {

            }
        }

        private void ChangeSizeNumericUpDown(NumericUpDown numericUpDown)
        {

        }

        private void test_NumericUpDown_LostFocus(object sender, RoutedEventArgs e)
        {

        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void DataScrollViewer_Loaded(object sender, RoutedEventArgs e)
        {
            /*ScrollBar vertical = ((FrameworkElement)VisualTreeHelper.GetChild(DataScrollViewer, 0)).FindName("VerticalScrollBar") as ScrollBar;

            vertical.Width = 50;*/

        }
    }
}