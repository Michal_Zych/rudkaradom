﻿using System;
using System.Net;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Events;
using System.Collections.Generic;
using System.Collections;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Dialogs.Patients;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Dialogs;
using Finder.BloodDonation.DataManagers;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.Dialogs.Bands;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.LanguageHelpher;
using PostSharp.Reflection;
using Finder.BloodDonation.Dialogs.Success;

namespace Finder.BloodDonation.Tabs.LocalizationConfig
{
    [OnCompleted]
    [UIException]
    public class LocalizationConfigViewModel: ViewModelBase, ITabViewModel
    {
        LocalizationConfigData _data;

        private const int MINIMUM_VALUE_OFFSET = 1;

        public RelayCommand CopyToClipboard { get; set; }
        public RelayCommand PasteFromClipboard { get; set; }
        public RelayCommand PasteDefaultData { get; set; }

        public RelayCommand AddNumber { get; set; }
        public RelayCommand EditNumber { get; set; }
        public RelayCommand DeleteNumber { get; set; }

        public RelayCommand UpNumber { get; set; }
        public RelayCommand DownNumber { get; set; } 

        public RelayCommand AddProhibitedZone { get; set; }
        public RelayCommand EditProhibitedZone { get; set; }
        public RelayCommand DeleteProhibitedZone { get; set; }

        public RelayCommand SaveActionCommand { get; set; }
        public RelayCommand RefreshActionCommand { get; set; }

        public RelayCommand SensitivityChangeCommand { get; set; }

        public RelayCommand AssignTransmitterCommand { get; set; }
        public RelayCommand RemoveTransmitterCommand { get; set; }

        public Visibility RemoveTransmitterButtonVisibility { get; set; }

        [RaisePropertyChanged]
        public int SelectedPhoneNumberIndex { get; set; }
        [RaisePropertyChanged]
        public int SelectedProhibitedZonesIndex { get; set; }

        [RaisePropertyChanged]
        public bool IsAlarmSOS { get; set; }
        [RaisePropertyChanged]
        public bool IsAlarmFromAccelerometer { get; set; }
        [RaisePropertyChanged]
        public bool IsSendWarnings_MoveDay { get; set; }
        [RaisePropertyChanged]
        public bool IsSendAlarm_MoveDay { get; set; }
        [RaisePropertyChanged]
        public bool IsSendWarnings_NotMoveDay { get; set; }
        [RaisePropertyChanged]
        public bool IsSendAlarm_NotMoveDay { get; set; }
        [RaisePropertyChanged]
        public bool IsSendWarnings_MoveNight { get; set; }
        [RaisePropertyChanged]
        public bool IsSendAlarm_MoveNight { get; set; }
        [RaisePropertyChanged]
        public bool IsSendWarnings_NotMoveNight { get; set; }
        [RaisePropertyChanged]
        public bool IsSendAlarm_NotMoveNight { get; set; }
        [RaisePropertyChanged]
        public bool IsSendWarning_PatientOutRoom { get; set; }
        [RaisePropertyChanged]
        public bool IsSendAllarm_PatientOutRoom { get; set; }
        [RaisePropertyChanged]
        public bool IsConnectionLost_SendWarning { get; set; }
        [RaisePropertyChanged]
        public bool IsConnectionLost_SendAlarm { get; set; }
        [RaisePropertyChanged]
        public bool IsMostImportand { get; set; }
        [RaisePropertyChanged]
        public bool DetailsUpdateBandCheckedState { get; set; }
        [RaisePropertyChanged]
        public bool? BandUpdater { get; set; }
        [RaisePropertyChanged]
        public System.Windows.Visibility IsVisiblityTransmiterDetails { get; set; }
        [RaisePropertyChanged]
        public bool IsProhibitedZone { get; set; }

        [RaisePropertyChanged]
        public System.Windows.Visibility AssignTransmitterButtonVisibility { get; set; }

        [RaisePropertyChanged]
        public int DetailsId { get; set; }

        private int _sendWarningsAfter_MoveDay;
        [RaisePropertyChanged]
        public int SendWarningsAfter_MoveDay 
        { 
            get 
            { 
                return _sendWarningsAfter_MoveDay;
            }
            set 
            {
                if (SendAlarmAfter_MoveDay <= value && IsSendAlarm_MoveDay)
                {
                    SendAlarmAfter_MoveDay = value + MINIMUM_VALUE_OFFSET;
                }

                _sendWarningsAfter_MoveDay = value; 
            }
        }
        
        [RaisePropertyChanged]
        public int SendAlarmAfter_MoveDay { get; set; }

        private int _sendWarningsAfter_NotMoveDay;
        [RaisePropertyChanged]
        public int SendWarningsAfter_NotMoveDay 
        {
            get
            {
                return _sendWarningsAfter_NotMoveDay;
            }
            set
            {
                if(SendAlarmAfter_NotMoveDay <= value && IsSendAlarm_NotMoveDay)
                {
                    SendAlarmAfter_NotMoveDay = value + MINIMUM_VALUE_OFFSET;
                }

                _sendWarningsAfter_NotMoveDay = value;
            }
        }
        [RaisePropertyChanged]
        public int SendAlarmAfter_NotMoveDay { get; set; }

        private int _sendWarningsAfter_MoveNight;
        [RaisePropertyChanged]
        public int SendWarningsAfter_MoveNight
        {
            get
            {
                return _sendWarningsAfter_MoveNight;
            }
            set
            {
                if(SendAlarmAfter_MoveNight <= value && IsSendAlarm_MoveNight)
                {
                    SendAlarmAfter_MoveNight = value + MINIMUM_VALUE_OFFSET;
                }

                _sendWarningsAfter_MoveNight = value;
            }
        }

        [RaisePropertyChanged]
        public int SendAlarmAfter_MoveNight { get; set; }

        private int _sendWarningsAfter_NotMoveNight;
        [RaisePropertyChanged]
        public int SendWarningsAfter_NotMoveNight 
        {
            get
            {
                return _sendWarningsAfter_NotMoveNight;
            }
            set
            {
                if(SendAlarmAfter_NotMoveNight <= value && IsSendAlarm_NotMoveNight)
                {
                    SendAlarmAfter_NotMoveNight = value + MINIMUM_VALUE_OFFSET;
                }

                _sendWarningsAfter_NotMoveNight = value;
            }
        }

        private int CheckAlarmNumericUpDownAndGetSetterValue(int value, ref int fieldToCheck, bool isFieldToCheckActive)
        {
            if(fieldToCheck <= value && isFieldToCheckActive)
            {
                fieldToCheck = value + MINIMUM_VALUE_OFFSET;
            }

            return value;
        }

        [RaisePropertyChanged]
        public int SendAlarmAfter_NotMoveNight { get; set; }

        private int _sendWarning_PatientOutRoomValue;
        [RaisePropertyChanged]
        public int SendWarning_PatientOutRoomValue 
        {
            get
            {
                return _sendWarning_PatientOutRoomValue;
            }
            set
            {
                if(SendAlarm_PatientOutRoomValue <= value && IsSendAllarm_PatientOutRoom)
                {
                    SendAlarm_PatientOutRoomValue = value + MINIMUM_VALUE_OFFSET;
                }

                _sendWarning_PatientOutRoomValue = value;
            }
        }
        [RaisePropertyChanged]
        public int SendAlarm_PatientOutRoomValue { get; set; }
        [RaisePropertyChanged]
        public int SendAlarm_PatientInProhibitedZone { get; set; }

        private int _connectionLost_SendWarning;
        [RaisePropertyChanged]
        public int ConnectionLost_SendWarning 
        {
            get 
            {
                return _connectionLost_SendWarning; 
            }
            set
            {
                if(ConnectionLost_SendAlarm <= value && IsConnectionLost_SendAlarm)
                {
                    ConnectionLost_SendAlarm = value + MINIMUM_VALUE_OFFSET;
                }

                _connectionLost_SendWarning = value;
            }
        }
        [RaisePropertyChanged]
        public int ConnectionLost_SendAlarm { get; set; }
        [RaisePropertyChanged]
        public int SelectedTypeIndex { get; set; }
        [RaisePropertyChanged]
        public int DetailsSensitivity { get; set; }
        [RaisePropertyChanged]
        public int DetailsAvgCalcTimeS { get; set; }
        [RaisePropertyChanged]
        public int DetailsSwitchDelayS { get; set; }

        [RaisePropertyChanged]
        public string detailsId { get; set; }

        [RaisePropertyChanged]
        public string DetailsIP { get; set; }
        [RaisePropertyChanged]
        public string DetailsMAC { get; set; }
        [RaisePropertyChanged]
        public string DetailsMSISDN { get; set; }
        [RaisePropertyChanged]
        public string InstallationDescription { get; set; }
        [RaisePropertyChanged]
        public string DetailsComment { get; set; }
        [RaisePropertyChanged]
        public string DetailsInstalationLocation { get; set; }

        [RaisePropertyChanged]
        public bool IsTransmiterLost_SendWarning { get; set; }

        private short _transmitterLost_SendWarning;
        [RaisePropertyChanged]
        public short TransmiterLost_SendWarning 
        {
            get 
            {
                return _transmitterLost_SendWarning; 
            }
            set
            {
                if (TransmiterLost_SendAlarm <= value && IsTransmiterLost_SendAlarm)
                {
                    TransmiterLost_SendAlarm = (short)(value + MINIMUM_VALUE_OFFSET);
                }

                _transmitterLost_SendWarning = value;
            }
        }

        [RaisePropertyChanged]
        public bool IsTransmiterLost_SendAlarm { get; set; }

        [RaisePropertyChanged]
        public short TransmiterLost_SendAlarm { get; set; }

        [RaisePropertyChanged]
        public SliderConfigurator SensitivityConfiguration { get; set; }
        [RaisePropertyChanged]
        public SliderConfigurator AvgCalcTimeS { get; set; }
        [RaisePropertyChanged]
        public SliderConfigurator SwitchDelayS { get; set; }
        [RaisePropertyChanged]
        public System.Windows.Visibility VisibilityState { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<Contact> CallNumbersList { get; set; }
        [RaisePropertyChanged]
        public ObservableCollection<UnitDTO> ProhibitedZonesList { get; set; }
        [RaisePropertyChanged]
        public ObservableCollection<GridViewUnitModel> ProhibitedZonesListToView { get; set; }

        public string _reason { get; set; }

        public string Reason
        {
            get
            {
                return _reason;
            }

            set
            {
                _reason = value;
                NotifyPropertyChange("Reason");
            }
        }

        private IWindowsManager _windowsManager;
        private PhoneNumberData _phoneData;
        private ProhibitedZoneData _prohibitedZoneData;

        private UnitsServiceClient Proxy { get; set; }

        public string WindowKey
        {
            get { return WindowsKeys.LocalizationConfig; }
        }

        public object Data
        {
            set 
            {
                _data = (LocalizationConfigData)value;

                SetDataToWindowField();
            }
        }

        public void OnDeleteProhibitedZone()
        {
            if (SelectedProhibitedZonesIndex < 0 || SelectedProhibitedZonesIndex >= ProhibitedZonesList.Count)
                return;
            else
            {
                ProhibitedZonesList.RemoveAt(SelectedProhibitedZonesIndex);
                ProhibitedZonesListToView.RemoveAt(SelectedProhibitedZonesIndex);
            }
        }

        private void SetDataToWindowField()
        {

            if (_data.TransmitterLocalizationConfig == null)
            {
                IsVisiblityTransmiterDetails = System.Windows.Visibility.Collapsed;
            }
            else
            {
                detailsId = _data.LocalizationConfigurationData.TransmitterId.ToString();
                IsVisiblityTransmiterDetails = System.Windows.Visibility.Visible;
                DetailsMAC = _data.TransmitterLocalizationConfig.MacString;
                SelectedTypeIndex = _data.TransmitterLocalizationConfig.Type - 1;
                SensitivityConfiguration.ValueIndex = _data.TransmitterLocalizationConfig.Sensitivity;
                AvgCalcTimeS.ValueIndex = _data.TransmitterLocalizationConfig.AvgCalcTimeS;
                SwitchDelayS.ValueIndex = _data.TransmitterLocalizationConfig.SwitchDelayS;
                BandUpdater = _data.TransmitterLocalizationConfig.IsBandUpdater;
                DetailsMSISDN = _data.TransmitterLocalizationConfig.MSISDN;
                DetailsInstalationLocation = _data.TransmitterLocalizationConfig.UnitId != null ? UnitManager.Instance.Units.FirstOrDefault(x=> x.ID == _data.TransmitterLocalizationConfig.UnitId).GetPath("/") : String.Empty;
            }

            _data.LocalizationConfigurationData.Id = (short?)SelectedUnit.Identity;
            IsConnectionLost_SendAlarm = _data.LocalizationConfigurationData.IsConnectionLost_SendAlarm;
            IsConnectionLost_SendWarning = _data.LocalizationConfigurationData.IsConnectionLost_SendWarning;
            ConnectionLost_SendWarning = _data.LocalizationConfigurationData.ConnectionLost_SendWarning;
            ConnectionLost_SendAlarm = _data.LocalizationConfigurationData.ConnectionLost_SendAlarm;
            IsAlarmFromAccelerometer = _data.LocalizationConfigurationData.IsAlarmFromAccelerometer;
            IsAlarmSOS = _data.LocalizationConfigurationData.IsAlarmSOS;
            IsSendAlarm_MoveDay = _data.LocalizationConfigurationData.IsSendAlarm_MoveDay;
            IsSendAlarm_MoveNight = _data.LocalizationConfigurationData.IsSendAlarm_MoveNight;
            IsSendAlarm_NotMoveDay = _data.LocalizationConfigurationData.IsSendAlarm_NotMoveDay;
            IsSendAlarm_NotMoveNight = _data.LocalizationConfigurationData.IsSendAlarm_NotMoveNight;
            IsSendWarning_PatientOutRoom = _data.LocalizationConfigurationData.IsSendWarning_PatientOutRoom;            
            IsSendAllarm_PatientOutRoom = _data.LocalizationConfigurationData.IsSendAllarm_PatientOutRoom;
            IsSendWarnings_MoveDay = _data.LocalizationConfigurationData.IsSendWarnings_MoveDay;
            IsSendWarnings_MoveNight = _data.LocalizationConfigurationData.IsSendWarnings_MoveNight;
            IsSendWarnings_NotMoveDay = _data.LocalizationConfigurationData.IsSendWarnings_NotMoveDay;
            IsSendWarnings_NotMoveNight = _data.LocalizationConfigurationData.IsSendWarnings_NotMoveNight;

            //RADOM
            if (_data.LocalizationConfigurationData.PhoneNumbers != null)
                CallNumbersList = new ObservableCollection<Contact>(_data.LocalizationConfigurationData.PhoneNumbers);
            else
                CallNumbersList = new ObservableCollection<Contact>();

            if (_data.LocalizationConfigurationData.ProhibitedZonesList != null)
                ProhibitedZonesList = new ObservableCollection<UnitDTO>(_data.LocalizationConfigurationData.ProhibitedZonesList);
            else
                ProhibitedZonesList = new ObservableCollection<UnitDTO>();

            SendWarning_PatientOutRoomValue = _data.LocalizationConfigurationData.SendWarning_PatientOutRoomValue;
            SendWarningsAfter_MoveDay = _data.LocalizationConfigurationData.SendWarningsAfter_MoveDay;
            SendWarningsAfter_MoveNight = _data.LocalizationConfigurationData.SendWarningsAfter_MoveNight;
            SendWarningsAfter_NotMoveDay = _data.LocalizationConfigurationData.SendWarningsAfter_NotMoveDay;
            SendWarningsAfter_NotMoveNight = _data.LocalizationConfigurationData.SendWarningsAfter_NotMoveNight;
            SendAlarm_PatientInProhibitedZone = _data.LocalizationConfigurationData.SendAlarm_PatientInProhibitedZone;
            SendAlarm_PatientOutRoomValue = _data.LocalizationConfigurationData.SendAlarm_PatientOutRoomValue;
            SendAlarmAfter_MoveDay = _data.LocalizationConfigurationData.SendAlarmAfter_MoveDay;
            SendAlarmAfter_MoveNight = _data.LocalizationConfigurationData.SendAlarmAfter_MoveNight;
            SendAlarmAfter_NotMoveDay = _data.LocalizationConfigurationData.SendAlarmAfter_NotMoveDay;
            SendAlarmAfter_NotMoveNight = _data.LocalizationConfigurationData.SendAlarmAfter_NotMoveNight;
            IsProhibitedZone = _data.LocalizationConfigurationData.IsProhibitedZone;
            IsTransmiterLost_SendWarning = _data.LocalizationConfigurationData.NoTransmitterWrActive;
            TransmiterLost_SendWarning = _data.LocalizationConfigurationData.NoTransmitterWrMins;
            IsTransmiterLost_SendAlarm = _data.LocalizationConfigurationData.NoTransmitterAlActive;
            TransmiterLost_SendAlarm = _data.LocalizationConfigurationData.NoTransmitterAlMins;
            IsMostImportand = _data.LocalizationConfigurationData.MoveSettingsPriority;
            Reason = "";
        }

        // Wywala ponieważ nie zinicjalizowano pola _data.
        private void SaveData()
        {
            if (_data == null)
                return;

            if (_data.LocalizationConfigurationData == null)
                return;

            if (_data.LocalizationConfigurationData.TransmitterId != null)
            {
                _data.TransmitterLocalizationConfig.Type = Convert.ToByte(SelectedTypeIndex);
                _data.TransmitterLocalizationConfig.MSISDN = DetailsMSISDN;
                _data.TransmitterLocalizationConfig.Description = InstallationDescription;
                _data.TransmitterLocalizationConfig.Sensitivity = SensitivityConfiguration.ValueIndex;
                _data.TransmitterLocalizationConfig.AvgCalcTimeS = AvgCalcTimeS.ValueIndex;
                _data.TransmitterLocalizationConfig.SwitchDelayS = SwitchDelayS.ValueIndex;
                _data.TransmitterLocalizationConfig.IsBandUpdater = BandUpdater;
            }

            _data.LocalizationConfigurationData.Id = Convert.ToInt16(SelectedUnit.Identity);
            _data.LocalizationConfigurationData.ConnectionLost_SendAlarm = ConnectionLost_SendAlarm;
            _data.LocalizationConfigurationData.ConnectionLost_SendWarning = ConnectionLost_SendWarning;
            _data.LocalizationConfigurationData.IsAlarmFromAccelerometer = IsAlarmFromAccelerometer;
            _data.LocalizationConfigurationData.IsAlarmSOS = IsAlarmSOS;
            _data.LocalizationConfigurationData.IsConnectionLost_SendAlarm = IsConnectionLost_SendAlarm;
            _data.LocalizationConfigurationData.IsConnectionLost_SendWarning = IsConnectionLost_SendWarning;
            _data.LocalizationConfigurationData.IsSendAlarm_MoveDay = IsSendAlarm_MoveDay;
            _data.LocalizationConfigurationData.IsSendAlarm_MoveNight = IsSendAlarm_MoveNight;
            _data.LocalizationConfigurationData.IsSendAlarm_NotMoveDay = IsSendAlarm_NotMoveDay;
            _data.LocalizationConfigurationData.IsSendAlarm_NotMoveNight = IsSendAlarm_NotMoveNight;
            _data.LocalizationConfigurationData.IsSendWarning_PatientOutRoom = IsSendWarning_PatientOutRoom;
            _data.LocalizationConfigurationData.IsSendAllarm_PatientOutRoom = IsSendAllarm_PatientOutRoom;
            _data.LocalizationConfigurationData.IsSendWarnings_MoveDay = IsSendWarnings_MoveDay;
            _data.LocalizationConfigurationData.IsSendWarnings_MoveNight = IsSendWarnings_MoveNight;
            _data.LocalizationConfigurationData.IsSendWarnings_NotMoveDay = IsSendWarnings_NotMoveDay;
            _data.LocalizationConfigurationData.IsSendWarnings_NotMoveNight = IsSendWarnings_NotMoveNight;
            _data.LocalizationConfigurationData.PhoneNumbers = ConvertObservableCollectionToList(CallNumbersList);
            _data.LocalizationConfigurationData.ProhibitedZonesList = new List<UnitDTO>(ProhibitedZonesList);
            _data.LocalizationConfigurationData.SendAlarm_PatientInProhibitedZone = SendAlarm_PatientInProhibitedZone;
            _data.LocalizationConfigurationData.SendWarning_PatientOutRoomValue = SendWarning_PatientOutRoomValue;
            _data.LocalizationConfigurationData.SendAlarm_PatientOutRoomValue = SendAlarm_PatientOutRoomValue;
            _data.LocalizationConfigurationData.SendAlarmAfter_MoveDay = SendAlarmAfter_MoveDay;
            _data.LocalizationConfigurationData.SendAlarmAfter_MoveNight = SendAlarmAfter_MoveNight;
            _data.LocalizationConfigurationData.SendAlarmAfter_NotMoveDay = SendAlarmAfter_NotMoveDay;
            _data.LocalizationConfigurationData.SendAlarmAfter_NotMoveNight = SendAlarmAfter_NotMoveNight;
            _data.LocalizationConfigurationData.SendWarningsAfter_MoveDay = SendWarningsAfter_MoveDay;
            _data.LocalizationConfigurationData.SendWarningsAfter_MoveNight = SendWarningsAfter_MoveNight;
            _data.LocalizationConfigurationData.SendWarningsAfter_NotMoveDay = SendWarningsAfter_NotMoveDay;
            _data.LocalizationConfigurationData.SendWarningsAfter_NotMoveNight = SendWarningsAfter_NotMoveNight;
            _data.LocalizationConfigurationData.IsProhibitedZone = IsProhibitedZone;
            _data.LocalizationConfigurationData.NoTransmitterWrActive = IsTransmiterLost_SendWarning;
            _data.LocalizationConfigurationData.NoTransmitterWrMins = TransmiterLost_SendWarning;
            _data.LocalizationConfigurationData.NoTransmitterAlActive = IsTransmiterLost_SendAlarm;
            _data.LocalizationConfigurationData.NoTransmitterAlMins = TransmiterLost_SendAlarm;
            _data.LocalizationConfigurationData.MoveSettingsPriority = IsMostImportand;
            _data.LocalizationConfigurationData.Reason = Reason;

            SendDataToDataBase();

            Reason = ""; // czyszczenie przyczyny po zapisie
        }

        private void SendDataToDataBase()
        {
            Proxy.SetLocalizationConfigurationAsync(_data.LocalizationConfigurationData);
        }

        private void SetDefaultDataToField()
        {
           
        }

        public List<Contact> ConvertObservableCollectionToList(ObservableCollection<Contact> input)
        {
            List<Contact> output = new List<Contact>();

            for(int i = 0; i < input.Count; i++)
            {
                output.Add(input[i]);
            }

            return output;
        }

        public void OnEditProhibitedZone()
        {
            
        }

        public void OnCancelActionCommand()
        {
            Refresh(SelectedUnit);
        }

        public void OnSaveActionCommand()
        {
            Close(CloseButton.OK);
        }

        public void OnDownNumber()
        {
            try
            {
                ChangePhoneNumberPosition(Direction.Down);
            }
            catch { }
        }

        public void OnUpNumber()
        {
            try
            {
                ChangePhoneNumberPosition(Direction.Up);
            }
            catch { }
        }

        private void ChangePhoneNumberPosition(Direction directionToChange)
        {
            Contact _temp = CallNumbersList[SelectedPhoneNumberIndex];

            if (directionToChange == Direction.Up && SelectedPhoneNumberIndex == CallNumbersList.Count - 1)
            {
                CallNumbersList.RemoveAt(SelectedPhoneNumberIndex);
                CallNumbersList.Insert(SelectedPhoneNumberIndex, _temp);
                CallNumbersList[SelectedPhoneNumberIndex].SequenceNr = Convert.ToByte(SelectedPhoneNumberIndex);
                SelectedPhoneNumberIndex = SelectedPhoneNumberIndex - 1;
            }
            else if (directionToChange == Direction.Up && SelectedPhoneNumberIndex != CallNumbersList.Count - 1)
            {
                if (SelectedPhoneNumberIndex > 0)
                {
                    CallNumbersList.RemoveAt(SelectedPhoneNumberIndex);
                    CallNumbersList.Insert(SelectedPhoneNumberIndex - 1, _temp);
                    CallNumbersList[SelectedPhoneNumberIndex - 1].SequenceNr = Convert.ToByte(SelectedPhoneNumberIndex - 1);
                    SelectedPhoneNumberIndex = SelectedPhoneNumberIndex - 2;
                }
            }
            else
            {
                CallNumbersList.RemoveAt(SelectedPhoneNumberIndex);
                CallNumbersList.Insert(SelectedPhoneNumberIndex + 1, _temp);
                CallNumbersList[SelectedPhoneNumberIndex + 1].SequenceNr = Convert.ToByte(SelectedPhoneNumberIndex + 1);
                SelectedPhoneNumberIndex = SelectedPhoneNumberIndex + 1;
            }

        }

        public void OnAddNumber()
        {
            _phoneData = new PhoneNumberData()
            {
                PhoneNumber = new Contact(),
                IsEdited = false
            };

            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(ClosePhoneWindowEventHandling);
            _windowsManager.ShowWindow(WindowsKeys.EditNumber, _phoneData);
        }

        public void OnAddProhibitedZone()
        {
            if (_prohibitedZoneData == null)
            {
                _prohibitedZoneData = new ProhibitedZoneData()
                {
                    ProhibitedZone = new ObservableCollection<UnitDTO>()
                };
            }

            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseProhibitedWindowEventHandling);
            _windowsManager.ShowWindow(WindowsKeys.EditProhibitedZoneModel, _prohibitedZoneData);
        }

        public void CloseProhibitedWindowEventHandling(string obj)
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(CloseProhibitedWindowEventHandling);
            ObservableCollection<Contact> _temp = CallNumbersList;

            if (obj.Equals(WindowsKeys.EditProhibitedZoneModel))
            {
                ProhibitedZonesList = _prohibitedZoneData.ProhibitedZone;
                ProhibitedZonesListToView = new ObservableCollection<GridViewUnitModel>();

                for(int i = 0; i < ProhibitedZonesList.Count; i++)
                {
                    var manager = UnitManager.Instance;
                    var _tmpUnit = manager.Units.ToList().FirstOrDefault(x => x.ID == ProhibitedZonesList[i].ID);

                    GridViewUnitModel _t = new GridViewUnitModel();
                    _t.RoomName = ProhibitedZonesList[i].Name;
                    _t.DepartmentName = UnitManager.Instance.SearchDepartment(_tmpUnit).Name;

                    ProhibitedZonesListToView.Add(_t);
                }
            }
        }

        public void Close(CloseButton option)
        {
            string msg = String.Empty;

            if (option == CloseButton.OK)
            {
                if (msg != String.Empty)
                {
                    MsgBox.Warning(msg);
                    return;
                }

                SaveData();
            }

            //Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        public void OnDeleteNumbe()
        {
            if (CallNumbersList != null &&
                SelectedPhoneNumberIndex >= 0 &&
                SelectedPhoneNumberIndex < CallNumbersList.Count)
            {
                if (SelectedPhoneNumberIndex < 0 && SelectedPhoneNumberIndex >= CallNumbersList.Count)
                {
                    return;
                }

                CallNumbersList.RemoveAt(SelectedPhoneNumberIndex);

                for (int i = 0; i < CallNumbersList.Count; i++)
                {
                    CallNumbersList[i].SequenceNr = Convert.ToByte(i);
                }
            }
            else
            {
                MsgBox.Error(LanguageManager.GetError(ErrorCodes.NON_SELLECTION_PHONE_NUMBER));
            }
        }

        public void OnEditNumber()
        {
            if (CallNumbersList != null &&
                SelectedPhoneNumberIndex >= 0 &&
                SelectedPhoneNumberIndex < CallNumbersList.Count)
            {
                _phoneData = new PhoneNumberData()
                {
                    PhoneNumber = new Contact()
                    {
                        SequenceNr = CallNumbersList[SelectedPhoneNumberIndex].SequenceNr,
                        Phone = CallNumbersList[SelectedPhoneNumberIndex].Phone,
                        Description = CallNumbersList[SelectedPhoneNumberIndex].Description
                    },
                    IsEdited = true
                };

                EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(ClosePhoneWindowEventHandling);
                _windowsManager.ShowWindow(WindowsKeys.EditNumber, _phoneData);
            }
            else
            {
                MsgBox.Error(LanguageManager.GetError(ErrorCodes.NON_SELLECTION_PHONE_NUMBER));
            }
        }

        public void ClosePhoneWindowEventHandling(string obj)
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(ClosePhoneWindowEventHandling);
            ObservableCollection<Contact> _temp = CallNumbersList;

            if (obj.Equals(WindowsKeys.EditNumber))
            {
                if (_phoneData.IsCanceled)
                {

                }
                else
                {
                    if (!_phoneData.IsEdited)
                    {
                        _phoneData.PhoneNumber.SequenceNr = Convert.ToByte(CallNumbersList.Count);
                        _temp.Add(_phoneData.PhoneNumber);
                        CallNumbersList = _temp;
                    }
                    else
                    {
                        CallNumbersList[SelectedPhoneNumberIndex] = _phoneData.PhoneNumber;
                    }
                }
            }
        }

        private void OnPasteDefaultData()
        {
            if(defaultData != null)
            {
                SelectedTypeIndex = defaultData.Type;
                DetailsSensitivity = defaultData.Sensitivity;
                BandUpdater = defaultData.BandUpdater;
                ConnectionLost_SendAlarm = defaultData.ConnectionLost_SendAlarm;
                ConnectionLost_SendWarning = defaultData.ConnectionLost_SendWarning;
                IsAlarmFromAccelerometer = defaultData.IsAlarmFromAccelerometer;
                IsAlarmSOS = defaultData.IsAlarmSOS;
                IsConnectionLost_SendAlarm = defaultData.IsConnectionLost_SendAlarm;
                IsConnectionLost_SendWarning = defaultData.IsConnectionLost_SendWarning;
                IsSendAlarm_MoveDay = defaultData.IsSendAlarm_MoveDay;
                IsSendAlarm_MoveNight = defaultData.IsSendAlarm_MoveNight;
                IsSendAlarm_NotMoveDay = defaultData.IsSendAlarm_NotMoveDay;
                IsSendAlarm_NotMoveNight = defaultData.IsSendAlarm_NotMoveNight;
                IsSendWarning_PatientOutRoom = defaultData.IsSendWarning_PatientOutRoom;
                IsSendAllarm_PatientOutRoom = defaultData.IsSendAllarm_PatientOutRoom;
                IsSendWarnings_MoveDay = defaultData.IsSendWarnings_MoveDay;
                IsSendWarnings_MoveNight = defaultData.IsSendWarnings_MoveNight;
                IsSendWarnings_NotMoveDay = defaultData.IsSendWarnings_NotMoveDay;
                IsSendWarnings_NotMoveNight = defaultData.IsSendWarnings_NotMoveNight;
                CallNumbersList = new ObservableCollection<Contact>(defaultData.PhoneNumbers);
                ProhibitedZonesList = new ObservableCollection<UnitDTO>(defaultData.ProhibitedZonesList);
                ProhibitedZonesListToView = new ObservableCollection<GridViewUnitModel>(GetGridViewUnitModel(defaultData.ProhibitedZonesList));
                SendAlarm_PatientInProhibitedZone = defaultData.SendAlarm_PatientInProhibitedZone;
                SendAlarm_PatientOutRoomValue = defaultData.SendAlarm_PatientOutRoomValue;
                SendAlarmAfter_MoveDay = defaultData.SendAlarmAfter_MoveDay;
                SendAlarmAfter_MoveNight = defaultData.SendAlarmAfter_MoveNight;
                SendAlarmAfter_NotMoveDay = defaultData.SendAlarmAfter_NotMoveDay;
                SendAlarmAfter_NotMoveNight = defaultData.SendAlarmAfter_NotMoveNight;
                SendWarning_PatientOutRoomValue = defaultData.SendWarning_PatientOutRoomValue;
                SendWarningsAfter_MoveDay = defaultData.SendWarningsAfter_MoveDay;
                SendWarningsAfter_MoveNight = defaultData.SendWarningsAfter_MoveNight;
                SendWarningsAfter_NotMoveDay = defaultData.SendWarningsAfter_NotMoveDay;
                SendWarningsAfter_NotMoveNight = defaultData.SendWarningsAfter_NotMoveNight;
                IsTransmiterLost_SendAlarm = defaultData.NoTransmitterAlActive;
                IsProhibitedZone = defaultData.IsProhibitedZone;
                TransmiterLost_SendAlarm = defaultData.NoTransmitterAlMins;
                IsTransmiterLost_SendWarning = defaultData.NoTransmitterWrActive;
                TransmiterLost_SendWarning = defaultData.NoTransmitterWrMins;
                IsMostImportand = defaultData.MoveSettingsPriority;
            }
        }

        public void OnPasteFromClipboard()
        {
            if (Clipboard.LocalizationConfigurationClipboard == null)
            {
                return;
            }

            SelectedTypeIndex = Clipboard.LocalizationConfigurationClipboard.Type;
            DetailsSensitivity = Clipboard.LocalizationConfigurationClipboard.Sensitivity;
            BandUpdater = Clipboard.LocalizationConfigurationClipboard.BandUpdater;
            ConnectionLost_SendAlarm = Clipboard.LocalizationConfigurationClipboard.ConnectionLost_SendAlarm;
            ConnectionLost_SendWarning = Clipboard.LocalizationConfigurationClipboard.ConnectionLost_SendWarning;
            IsAlarmFromAccelerometer = Clipboard.LocalizationConfigurationClipboard.IsAlarmFromAccelerometer;
            IsAlarmSOS = Clipboard.LocalizationConfigurationClipboard.IsAlarmSOS;
            IsConnectionLost_SendAlarm = Clipboard.LocalizationConfigurationClipboard.IsConnectionLost_SendAlarm;
            IsConnectionLost_SendWarning = Clipboard.LocalizationConfigurationClipboard.IsConnectionLost_SendWarning;
            IsSendAlarm_MoveDay = Clipboard.LocalizationConfigurationClipboard.IsSendAlarm_MoveDay;
            IsSendAlarm_MoveNight = Clipboard.LocalizationConfigurationClipboard.IsSendAlarm_MoveNight;
            IsSendAlarm_NotMoveDay = Clipboard.LocalizationConfigurationClipboard.IsSendAlarm_NotMoveDay;
            IsSendAlarm_NotMoveNight = Clipboard.LocalizationConfigurationClipboard.IsSendAlarm_NotMoveNight;
            IsSendWarning_PatientOutRoom = Clipboard.LocalizationConfigurationClipboard.IsSendWarning_PatientOutRoom;
            IsSendAllarm_PatientOutRoom =  Clipboard.LocalizationConfigurationClipboard.IsSendAllarm_PatientOutRoom;
            IsSendWarnings_MoveDay = Clipboard.LocalizationConfigurationClipboard.IsSendWarnings_MoveDay;
            IsSendWarnings_MoveNight = Clipboard.LocalizationConfigurationClipboard.IsSendWarnings_MoveNight;
            IsSendWarnings_NotMoveDay = Clipboard.LocalizationConfigurationClipboard.IsSendWarnings_NotMoveDay;
            IsSendWarnings_NotMoveNight = Clipboard.LocalizationConfigurationClipboard.IsSendWarnings_NotMoveNight;
            CallNumbersList = new ObservableCollection<Contact>(Clipboard.LocalizationConfigurationClipboard.PhoneNumbers);
            ProhibitedZonesList = new ObservableCollection<UnitDTO>(Clipboard.LocalizationConfigurationClipboard.ProhibitedZonesList);
            SendWarning_PatientOutRoomValue = Clipboard.LocalizationConfigurationClipboard.SendWarning_PatientOutRoomValue;
            SendWarningsAfter_MoveDay = Clipboard.LocalizationConfigurationClipboard.SendWarningsAfter_MoveDay;
            SendWarningsAfter_MoveNight = Clipboard.LocalizationConfigurationClipboard.SendWarningsAfter_MoveNight;
            SendWarningsAfter_NotMoveDay = Clipboard.LocalizationConfigurationClipboard.SendWarningsAfter_NotMoveDay;
            SendWarningsAfter_NotMoveNight = Clipboard.LocalizationConfigurationClipboard.SendWarningsAfter_NotMoveNight;
            SendAlarm_PatientInProhibitedZone = Clipboard.LocalizationConfigurationClipboard.SendAlarm_PatientInProhibitedZone;
            SendAlarm_PatientOutRoomValue = Clipboard.LocalizationConfigurationClipboard.SendAlarm_PatientOutRoomValue;
            SendAlarmAfter_MoveDay = Clipboard.LocalizationConfigurationClipboard.SendAlarmAfter_MoveDay;
            SendAlarmAfter_MoveNight = Clipboard.LocalizationConfigurationClipboard.SendAlarmAfter_MoveNight;
            SendAlarmAfter_NotMoveDay = Clipboard.LocalizationConfigurationClipboard.SendAlarmAfter_NotMoveDay;
            SendAlarmAfter_NotMoveNight = Clipboard.LocalizationConfigurationClipboard.SendAlarmAfter_NotMoveNight;
            ProhibitedZonesListToView = new ObservableCollection<GridViewUnitModel>(GetGridViewUnitModel(Clipboard.LocalizationConfigurationClipboard.ProhibitedZonesList));
            IsMostImportand = Clipboard.LocalizationConfigurationClipboard.MoveSettingsPriority;
            IsTransmiterLost_SendWarning = Clipboard.LocalizationConfigurationClipboard.NoTransmitterWrActive;
            TransmiterLost_SendWarning = Clipboard.LocalizationConfigurationClipboard.NoTransmitterWrMins;    
            IsTransmiterLost_SendAlarm = Clipboard.LocalizationConfigurationClipboard.NoTransmitterAlActive;
            TransmiterLost_SendAlarm = Clipboard.LocalizationConfigurationClipboard.NoTransmitterAlMins;
        }

        public List<GridViewUnitModel> GetGridViewUnitModel(List<UnitDTO> input)
        {
            List<GridViewUnitModel> output = new List<GridViewUnitModel>();

            for(int i = 0; i < input.Count; i++)
            {
                output.Add(new GridViewUnitModel(){
                    RoomName = input[i].Name,
                    DepartmentName = UnitManager.Instance.SearchDepartment(input[i]).Name
                });
            }

            return output;
        }

        public void OnCopyToClipboard()
        {
            Clipboard.LocalizationConfigurationClipboard = new LocalizationConfigDTO();
            Clipboard.LocalizationConfigurationClipboard.Type = Convert.ToByte(SelectedTypeIndex);
            Clipboard.LocalizationConfigurationClipboard.Sensitivity = Convert.ToByte(DetailsSensitivity);
            Clipboard.LocalizationConfigurationClipboard.BandUpdater = BandUpdater;
            Clipboard.LocalizationConfigurationClipboard = new LocalizationConfigDTO();
            Clipboard.LocalizationConfigurationClipboard.ConnectionLost_SendAlarm = ConnectionLost_SendAlarm;
            Clipboard.LocalizationConfigurationClipboard.ConnectionLost_SendWarning = ConnectionLost_SendWarning;
            Clipboard.LocalizationConfigurationClipboard.IsAlarmFromAccelerometer = IsAlarmFromAccelerometer;
            Clipboard.LocalizationConfigurationClipboard.IsAlarmSOS = IsAlarmSOS;
            Clipboard.LocalizationConfigurationClipboard.IsConnectionLost_SendAlarm = IsConnectionLost_SendAlarm;
            Clipboard.LocalizationConfigurationClipboard.IsConnectionLost_SendWarning = IsConnectionLost_SendWarning;
            Clipboard.LocalizationConfigurationClipboard.IsSendAlarm_MoveDay = IsSendAlarm_MoveDay;
            Clipboard.LocalizationConfigurationClipboard.IsSendAlarm_MoveNight = IsSendAlarm_MoveNight;
            Clipboard.LocalizationConfigurationClipboard.IsSendAlarm_NotMoveDay = IsSendAlarm_NotMoveDay;
            Clipboard.LocalizationConfigurationClipboard.IsSendAlarm_NotMoveNight = IsSendAlarm_NotMoveNight;
            Clipboard.LocalizationConfigurationClipboard.IsSendWarning_PatientOutRoom = IsSendWarning_PatientOutRoom;
            Clipboard.LocalizationConfigurationClipboard.IsSendAllarm_PatientOutRoom = IsSendAllarm_PatientOutRoom;
            Clipboard.LocalizationConfigurationClipboard.IsSendWarnings_MoveDay = IsSendWarnings_MoveDay;
            Clipboard.LocalizationConfigurationClipboard.IsSendWarnings_MoveNight = IsSendWarnings_MoveNight;
            Clipboard.LocalizationConfigurationClipboard.IsSendWarnings_NotMoveDay = IsSendWarnings_NotMoveDay;
            Clipboard.LocalizationConfigurationClipboard.IsSendWarnings_NotMoveNight = IsSendWarnings_NotMoveNight;
            Clipboard.LocalizationConfigurationClipboard.PhoneNumbers = new List<Contact>(CallNumbersList);
            Clipboard.LocalizationConfigurationClipboard.ProhibitedZonesList = new List<UnitDTO>(ProhibitedZonesList);
            Clipboard.LocalizationConfigurationClipboard.SendAlarm_PatientInProhibitedZone = SendAlarm_PatientInProhibitedZone;
            Clipboard.LocalizationConfigurationClipboard.SendAlarmAfter_MoveDay = SendAlarmAfter_MoveDay;
            Clipboard.LocalizationConfigurationClipboard.SendAlarmAfter_MoveNight = SendAlarmAfter_MoveNight;
            Clipboard.LocalizationConfigurationClipboard.SendAlarmAfter_NotMoveDay = SendAlarmAfter_NotMoveDay;
            Clipboard.LocalizationConfigurationClipboard.SendAlarmAfter_NotMoveNight = SendAlarmAfter_NotMoveNight;
            Clipboard.LocalizationConfigurationClipboard.SendWarning_PatientOutRoomValue = SendWarning_PatientOutRoomValue;
            Clipboard.LocalizationConfigurationClipboard.SendAlarm_PatientOutRoomValue = SendAlarm_PatientOutRoomValue;
            Clipboard.LocalizationConfigurationClipboard.SendWarningsAfter_MoveDay = SendWarningsAfter_MoveDay;
            Clipboard.LocalizationConfigurationClipboard.SendWarningsAfter_MoveNight = SendWarningsAfter_MoveNight;
            Clipboard.LocalizationConfigurationClipboard.SendWarningsAfter_NotMoveDay = SendWarningsAfter_NotMoveDay;
            Clipboard.LocalizationConfigurationClipboard.SendWarningsAfter_NotMoveNight = SendWarningsAfter_NotMoveNight;
            Clipboard.LocalizationConfigurationClipboard.MoveSettingsPriority = IsMostImportand;
            Clipboard.LocalizationConfigurationClipboard.NoTransmitterWrActive = IsTransmiterLost_SendWarning;
            Clipboard.LocalizationConfigurationClipboard.NoTransmitterWrMins = TransmiterLost_SendWarning;
            Clipboard.LocalizationConfigurationClipboard.NoTransmitterAlActive = IsTransmiterLost_SendAlarm;
            Clipboard.LocalizationConfigurationClipboard.NoTransmitterAlMins = TransmiterLost_SendAlarm;
        }
    
        public event EventHandler LoadingCompleted = delegate { };

        private IUnit SelectedUnit = null;

        public LocalizationConfigViewModel(IUnityContainer container)
            : base(container)
        {
            this.ViewAttached += new EventHandler(ViewModel_ViewAttached);

            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(OnClosedOthersWindow);

            CopyToClipboard = new RelayCommand(OnCopyToClipboard);
            PasteFromClipboard = new RelayCommand(OnPasteFromClipboard);
            PasteDefaultData = new RelayCommand(OnPasteDefaultData);

            AddNumber = new RelayCommand(OnAddNumber);
            EditNumber = new RelayCommand(OnEditNumber);
            DeleteNumber = new RelayCommand(OnDeleteNumbe);
            UpNumber = new RelayCommand(OnUpNumber);
            DownNumber = new RelayCommand(OnDownNumber);

            AddProhibitedZone = new RelayCommand(OnAddProhibitedZone);
            EditProhibitedZone = new RelayCommand(OnEditProhibitedZone);
            DeleteProhibitedZone = new RelayCommand(OnDeleteProhibitedZone);

            SaveActionCommand = new RelayCommand(OnSaveActionCommand);
            RefreshActionCommand = new RelayCommand(OnCancelActionCommand);

            AssignTransmitterCommand = new RelayCommand(OnAssignTransmitter);
            RemoveTransmitterCommand = new RelayCommand(OnRemoveTransmitter);

            CallNumbersList = new ObservableCollection<Contact>();
            ProhibitedZonesList = new ObservableCollection<UnitDTO>();

            SensitivityChangeCommand = new RelayCommand(OnSensitivityChangeCommand);

            var viewFactory = container.Resolve<ViewFactory>();
            _windowsManager = this.Container.Resolve<IWindowsManager>();
            _windowsManager.RegisterWindow(WindowsKeys.EditNumber, String.Empty, viewFactory.CreateView<EditNumber>(), new System.Windows.Size(400, 230), true);
            _windowsManager.RegisterWindow(WindowsKeys.EditProhibitedZoneModel, String.Empty, viewFactory.CreateView<EditProhibitedZone>(), new System.Windows.Size(400, 450), true);
            _windowsManager.RegisterWindow(WindowsKeys.AssignTransmitterModel, String.Empty, viewFactory.CreateView<AssignTransmitter>(), new System.Windows.Size(450, 290), true);
            _windowsManager.RegisterWindow(WindowsKeys.SuccessDialog, "Powodzenie", viewFactory.CreateView<Success>(), new System.Windows.Size(440, 170), false);

            Proxy = ServiceFactory.GetService<UnitsServiceClient>();
            Proxy.GetLocalizationConfigForUnitCompleted += Proxy_GetLocalizationConfigForUnitCompleted;
            Proxy.TransmitterConfigurationGetCompleted += Proxy_TransmitterConfigurationGetCompleted;
            Proxy.SetLocalizationConfigurationCompleted += Proxy_SetLocalizationConfigurationCompleted;
            Proxy.TransmitterConfigurationSetCompleted += Proxy_TransmitterConfigurationSetCompleted;
            Proxy.TransmitterRemoveFromUnitCompleted += Proxy_TransmitterRemoveFromUnitCompleted;

            SensitivityConfiguration = new SliderConfigurator(container,"dB","dB",-20, 20);
            SwitchDelayS = new SliderConfigurator(container, "s", "s", 3, 60);
            AvgCalcTimeS = new SliderConfigurator(container, "s", "s", 0, 60);

            if (_data == null)
            {
                IsVisiblityTransmiterDetails = System.Windows.Visibility.Collapsed;
                return;
            }
            else
            {
                if (_data.LocalizationConfigurationData.TransmitterId == null)
                {
                   IsVisiblityTransmiterDetails = System.Windows.Visibility.Collapsed;
                }
                else
                {
                   IsVisiblityTransmiterDetails = System.Windows.Visibility.Visible;
                }
            }

            Proxy.GetLocalizationConfigForUnitAsync(0);
        }

        public void Proxy_TransmitterRemoveFromUnitCompleted(object sender, TransmitterRemoveFromUnitCompletedEventArgs e)
        {
            int t;

            if(int.TryParse(e.Result, out t))
            {
                _windowsManager.ShowWindow(WindowsKeys.SuccessDialog, "Transmiter został odinstalowany pomyślnie");
            }
            else
            {
                MsgBox.Warning(e.Result);
            }
        }

        public void OnRemoveTransmitter()
        {
            MsgBox.Confirm("Czy na pewno chcesz odinstalować transmitter?", OnUninstallTransmitter);
        }

        private void OnUninstallTransmitter(object sender, Telerik.Windows.Controls.WindowClosedEventArgs e)
        {
            if(e.DialogResult == true && _data.LocalizationConfigurationData.Id != null)
            {
                Proxy.TransmitterRemoveFromUnitAsync((short)_data.LocalizationConfigurationData.Id, String.Empty);
            }
        }

        public void OnClosedOthersWindow(string obj)
        {

        }

        public void OnAssignTransmitter()
        {
            _windowsManager.ShowWindow(WindowsKeys.AssignTransmitterModel, SelectedUnit.Identity);
        }

        void Proxy_TransmitterConfigurationSetCompleted(object sender, TransmitterConfigurationSetCompletedEventArgs e)
        {
            _windowsManager.ShowWindow(WindowsKeys.SuccessDialog, "Zapisano pomyślnie!");
            Refresh(SelectedUnit);
        }

        void Proxy_SetLocalizationConfigurationCompleted(object sender, SetLocalizationConfigurationCompletedEventArgs e)
        {
            if (_data.TransmitterLocalizationConfig != null)
                Proxy.TransmitterConfigurationSetAsync(_data.TransmitterLocalizationConfig);
            else
            {
                _windowsManager.ShowWindow(WindowsKeys.SuccessDialog, "Zapisano pomyślnie!");
                Refresh(SelectedUnit);
            }
        }

        public void OnSensitivityChangeCommand()
        {
            
        }

        private void ViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public void Refresh(IUnit unit)
        {
            Reason = String.Empty;
            SelectedUnit = unit;

            if (unit != null && UnitManager.Instance.Units.FirstOrDefault(x => x.ID == unit.Identity).UnitTypeIDv2 != 29 && UnitManager.Instance.Units.FirstOrDefault(x => x.ID == unit.Identity).Children.Count <= 0)
            {
                AssignTransmitterButtonVisibility = System.Windows.Visibility.Visible;
            }
            else
            {
                AssignTransmitterButtonVisibility = System.Windows.Visibility.Collapsed;
            }

            Proxy.GetLocalizationConfigForUnitAsync(SelectedUnit.Identity);

            if(defaultData == null)
                Proxy.GetLocalizationConfigForUnitAsync(0);
        }

        void Proxy_TransmitterConfigurationGetCompleted(object sender, TransmitterConfigurationGetCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                _data.TransmitterLocalizationConfig = e.Result.FirstOrDefault();
                SetDataToWindowField();
            }
        }

        LocalizationConfigDTO defaultData = null;

        void Proxy_GetLocalizationConfigForUnitCompleted(object sender, GetLocalizationConfigForUnitCompletedEventArgs e)
        {
            if (e.Result.Id == 0)
            {
                defaultData = e.Result;
            }
            else
            {
                _data = new LocalizationConfigData()
                {
                    LocalizationConfigurationData = e.Result,
                    TransmitterLocalizationConfig = null
                };

                if (_data.LocalizationConfigurationData.ProhibitedZonesList != null)
                    ProhibitedZonesList = new ObservableCollection<UnitDTO>(_data.LocalizationConfigurationData.ProhibitedZonesList);
                else
                    ProhibitedZonesList = new ObservableCollection<UnitDTO>();

                List<GridViewUnitModel> _dynamicList = new List<GridViewUnitModel>();

                for (int i = 0; i < ProhibitedZonesList.Count; i++)
                {
                    UnitDTO _temp = UnitManager.Instance.Units.Where(f => f.ID == ProhibitedZonesList[i].ID).FirstOrDefault();

                    ProhibitedZonesList[i] = _temp;

                    _dynamicList.Add(new GridViewUnitModel()
                    {
                        DepartmentName = UnitManager.Instance.SearchDepartment(_temp) != null ? UnitManager.Instance.SearchDepartment(_temp).Name : String.Empty,
                        RoomName = _temp != null ? _temp.Name : String.Empty
                    });
                }

                ProhibitedZonesListToView = new ObservableCollection<GridViewUnitModel>(_dynamicList);

                if (_data.LocalizationConfigurationData.TransmitterId != null)
                    Proxy.TransmitterConfigurationGetAsync((short)_data.LocalizationConfigurationData.TransmitterId);
                else
                    SetDataToWindowField();
            }
        }

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        public string Title
        {
            get { return ""; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }
}
