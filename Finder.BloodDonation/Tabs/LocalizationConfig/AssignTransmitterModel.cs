﻿using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.LocalizationConfig
{
    [OnCompleted]
    [UIException]
    public class AssignTransmitterModel : ViewModelBase, IDialogWindow
    {
        private object unitId = -1;

        [RaisePropertyChanged]
        public string Code { get; set; }

        [RaisePropertyChanged]
        public string Title {get; set;}

        public string Reason { get; set; }

        public RelayCommand SaveActionCommand { get; set; }
        public RelayCommand CancelActionCommand { get; set; }

        protected internal UnitsServiceClient proxy;
        protected internal UnitsServiceClient Proxy
        {
            get
            {
                if (proxy == null)
                    proxy = ServiceFactory.GetService<UnitsServiceClient>();

                return proxy;
            }
            set
            {
                proxy = value;
            }
        }

        public string WindowKey
        {
            get { return WindowsKeys.AssignTransmitterModel; }
        }

        public AssignTransmitterModel(IUnityContainer container)
            : base(container)
        {
            Title = "Przypisz transmiter";

            SaveActionCommand = new RelayCommand(OnSaveCommand);
            CancelActionCommand = new RelayCommand(OnCloseCommand);

            Proxy.AssignTransmitterToUnitCompleted += Proxy_AssignTransmitterToUnitCompleted;
            ClearFields();
        }

        private void OnCloseCommand()
        {
            Close(CloseButton.Cancel);
        }

        public void OnSaveCommand()
        {
            Close(CloseButton.OK);
        }

        void Proxy_AssignTransmitterToUnitCompleted(object sender, AssignTransmitterToUnitCompletedEventArgs e)
        {
            int result;

            if (int.TryParse(e.Result, out result))
                MsgBox.Succes("Pomyślnie przypisano transmiter!");
            else
                MsgBox.Succes(e.Result);
        }

        private void ClearFields()
        {
            Code = string.Empty;
        }

        public object Data
        {
            set
            {
                unitId = value;

                ClearFields();
            }
        }

        private void Close(CloseButton closeButton)
        {
            string errMsg = string.Empty;

            if(closeButton == CloseButton.OK)
            {
                SaveData();   
            }

            Container.Resolve<IWindowsManager>().CloseWindow(this.WindowKey);
        }

        private void SaveData()
        {
            if((int)unitId != -1)
            {
                Proxy.AssignTransmitterToUnitAsync(Code, (int)unitId, Reason);
            }
        }
    }
}