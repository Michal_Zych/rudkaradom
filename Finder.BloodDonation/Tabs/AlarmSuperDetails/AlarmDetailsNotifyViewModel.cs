﻿using System;
using System.Net;
using System.Linq;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Common.Events;
using System.Collections.ObjectModel;
using System.Windows.Media.Animation;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using System.Collections.Generic;
using System.Collections;

namespace Finder.BloodDonation.Tabs.AlarmSuperDetails
{
    [OnCompleted]
    [UIException]
    public class AlarmDetailsNotifyViewModel : ViewModelBase
    {
        [RaisePropertyChanged]
        public ObservableCollection<AlarmDetailsNotifyRowViewModel> Alarms { get; set; }

        [RaisePropertyChanged]
        public AlarmDetailsNotifyRowViewModel SelectedAlarm { get; set; }
        
        [RaisePropertyChanged]
        public bool IsBusy2 { get; set; }

        [RaisePropertyChanged]
        public bool ShowPopup { get; set; }

        public DelegateCommand<object> ShowAlarmDetails { get; set; }

        public DelegateCommand<object> CloseAlarms { get; set; }

        private AlarmsServiceClient LoadService
        {
            get
            {
                AlarmsServiceClient proxy = ServiceFactory.GetService<AlarmsServiceClient>(ServicesUri.AlarmsService);
                proxy.CloseAlarmsCompleted += new EventHandler<CloseAlarmsCompletedEventArgs>(proxy_CloseAlarmsCompleted);
                return proxy;
            }
        }

        public DelegateCommand<object> SelectAlarms { get; set; }

        public IList<AlarmDetailsNotifyRowViewModel> SelectedAlarms { get; set; }

        void proxy_CloseAlarmsCompleted(object sender, CloseAlarmsCompletedEventArgs e)
        {
            EventAggregator.GetEvent<AlarmsCloseAlarms>().Publish(null);
	        SelectAlarms_Command(null);
        }

        public AlarmDetailsNotifyViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            EventAggregator.GetEvent<AlarmsDetailsReceived>().Subscribe(AlarmsNewReceived_Event);
            EventAggregator.GetEvent<AlarmsCloseAlarms>().Subscribe(AlarmsClosed_Event);
            EventAggregator.GetEvent<AlarmsDetailsRecived>().Subscribe(AlarmsDetailsReceiver);

            ShowAlarmDetails = new DelegateCommand<object>(ShowAlarmDetails_Command);
            CloseAlarms = new DelegateCommand<object>(CloseAlarms_Command);
            SelectAlarms = new DelegateCommand<object>(SelectAlarms_Command);
            ShowPopup = false;
        }

        public void AlarmsDetailsReceiver(List<AlarmDetailsDto> obj)
        {
            ObservableCollection<AlarmDetailsNotifyRowViewModel> r = null;
            if (obj != null)
            {
                r = new ObservableCollection<AlarmDetailsNotifyRowViewModel>();
                for (int i = 0; i < obj.Count; i++)
                {
                    r.Add(new AlarmDetailsNotifyRowViewModel(Container) { Alarm = obj[i] });
                }
            }

            AlarmDetailsNotifyRowViewModel selected = null;
            if (SelectedAlarm != null)
                selected = r.Where(x => x.Alarm.Id == SelectedAlarm.Alarm.Id).FirstOrDefault();

            Alarms = r;
            SelectedAlarm = selected;

            if (Alarms != null && Alarms.Count > 0 && Bootstrapper.UserMonitoringState && ShowPopup)
            {
                EventAggregator.GetEvent<ShowWindow>().Publish("alarnew");
            }
        }

        public void AlarmsClosed_Event(IList<AlarmDto> obj)
        {
            SelectAlarms_Command(null);
        }

        public void SelectAlarms_Command(object o)
        {
            if (SelectedAlarms == null)
            {
                SelectedAlarms = new List<AlarmDetailsNotifyRowViewModel>();
            }

            SelectedAlarms.Clear();

			if (o != null)
			{
				IList l = o as IList;
				for (int i = 0; i < l.Count; i++)
				{
                    SelectedAlarms.Add(l[i] as AlarmDetailsNotifyRowViewModel);
				}
			}
        }

        public void ShowAlarmDetails_Command(object o)
        {
            if(SelectedAlarm!=null)
                SelectedAlarm.AlarmDetails_Command(null);
            if (SelectedAlarms != null && SelectedAlarms.Count == 1)
            {
                SelectedAlarms[0].AlarmDetails_Command(null);
            }
        }

        public void CloseAlarms_Command(object o)
        {
            if (SelectedAlarms != null)
            {
                var ids = SelectedAlarms.Select(a => a.Alarm.Id).ToCommaSeparatedValues();
                if (ids != null)
                {
                    LoadService.CloseAlarmsAsync(ids, string.Empty);
                }
            }
        }

        public void AlarmsNewReceived_Event(List<AlarmDetailsDto> e)
        {
            ObservableCollection<AlarmDetailsNotifyRowViewModel> r = null;
            if (e != null)
            {
                r = new ObservableCollection<AlarmDetailsNotifyRowViewModel>();
                for (int i = 0; i < e.Count; i++)
                {
                    r.Add(new AlarmDetailsNotifyRowViewModel(Container) { Alarm = e[i] });
                }
            }

            AlarmDetailsNotifyRowViewModel selected = null;
            if(SelectedAlarm != null)
                selected = r.Where(x => x.Alarm.Id == SelectedAlarm.Alarm.Id).FirstOrDefault();

            Alarms = r;
            SelectedAlarm = selected;

            if (Alarms != null && Alarms.Count > 0 && Bootstrapper.UserMonitoringState)
            {
                EventAggregator.GetEvent<ShowWindow>().Publish("alarnew");
            }
        }
    }
}
