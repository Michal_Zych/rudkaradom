﻿using Finder.BloodDonation.Model.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.AlarmSuperDetails
{
    public class EditDetailsAlarmData
    {
        public AlarmDetailsDto dto { get; set; }
    }
}
