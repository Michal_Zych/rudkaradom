﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;

namespace Finder.BloodDonation.Tabs.AlarmSuperDetails
{
    [ViewModel(typeof(EditDetailsAlarmViewModel))]
    public partial class EditDetailsAlarm : UserControl
    {
        public EditDetailsAlarm()
        {
            InitializeComponent();
        }

        void btnClose_KeyDown(object sender, KeyEventArgs e)
        {

        }

        void txtType_Loaded(object sender, RoutedEventArgs e)
        {
            if (txtType.Text == "4" || txtType.Text == "8" || txtType.Text == "32")
            {
                Dispatcher.BeginInvoke(() => { btnClose.UpdateLayout(); btnClose.Focus(); });
            }
        }

        void txtID_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
