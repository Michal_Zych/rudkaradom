﻿using System;
using System.Collections.Generic;
using System.Net;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;

namespace Finder.BloodDonation.Tabs.AlarmSuperDetails
{
    [UIException]
    public class AlarmDetailsNotifyRowViewModel : ViewModelBase
    {
        [RaisePropertyChanged]
        public AlarmDetailsDto Alarm { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> AlarmDetails { get; set; }

        public DelegateCommand<object> CloseAlarm { get; set; }

        private AlarmsServiceClient LoadService
        {
            get
            {
                AlarmsServiceClient proxy = ServiceFactory.GetService<AlarmsServiceClient>(ServicesUri.AlarmsService);
                proxy.CloseAlarmsCompleted += new EventHandler<CloseAlarmsCompletedEventArgs>(proxy_CloseAlarmsCompleted);
                return proxy;
            }
        }

        void proxy_CloseAlarmsCompleted(object sender, CloseAlarmsCompletedEventArgs e)
        {
            EventAggregator.GetEvent<AlarmsDetailsCloseAlarms>().Publish(new List<AlarmDetailsDto> { Alarm });
        }


        public AlarmDetailsNotifyRowViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            this.AlarmDetails = new DelegateCommand<object>(this.AlarmDetails_Command);
            this.CloseAlarm = new DelegateCommand<object>(CloseAlarm_Command);
        }

        public void AlarmDetails_Command(object o)
        {
            if (Alarm != null)
            {
                EventAggregator.GetEvent<LoadAlarmDetailsDetails>().Publish(Alarm);
                EventAggregator.GetEvent<ShowWindow>().Publish("alaredit");
            }
        }

        public void CloseAlarm_Command(object o)
        {
            if (Alarm != null)
            {
                LoadService.CloseAlarmsAsync(Alarm.Id.ToString(), string.Empty);
            }
        }
    }
}
