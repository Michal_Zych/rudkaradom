﻿using Finder.BloodDonation.AlarmsService;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.LanguageHelpher;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Tools;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.AlarmSuperDetails
{
    //Dokończyć!
    [OnCompleted]
    [UIException]
    public class EditDetailsAlarmViewModel : ViewModelBase, IDialogWindow
    {
        private EditDetailsAlarmData _data = null;

        [RaisePropertyChanged]
        public AlarmDetailsDto Alarm { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> CloseAlarm { get; set; }

        private AlarmsServiceClient LoadService
        {
            get
            {
                AlarmsServiceClient proxy = ServiceFactory.GetService<AlarmsServiceClient>(ServicesUri.AlarmsService);
                proxy.CloseAlarmCompleted += proxy_CloseAlarmCompleted;
                return proxy;
            }
        }

        void proxy_CloseAlarmCompleted(object sender, CloseAlarmCompletedEventArgs e)
        {
            EventAggregator.GetEvent<AlarmsDetailsCloseAlarms>().Publish(new List<AlarmDetailsDto>() { Alarm });
            MsgBox.Warning("Alarm został pomyślnie zamknięty!");
            Container.Resolve<IWindowsManager>().CloseWindow(this.WindowKey);
        }

        public EditDetailsAlarmViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            EventAggregator.GetEvent<LoadAlarmDetailsDetails>().Subscribe(LoadAlarmDetails_Event);
            EventAggregator.GetEvent<LoadAlarmEdit>().Subscribe(OnLoadAlarmEdit_Event);

            CloseAlarm = new DelegateCommand<object>(this.CloseAlarm_Command);
        }

        public void OnLoadAlarmEdit_Event(EventsDetailsDto obj)
        {

        }

        public void CloseAlarm_Command(object o)
        {
            if (Alarm != null)
            {
                LoadService.CloseAlarmAsync(Alarm);
            }
        }

        public void LoadAlarmDetails_Event(AlarmDetailsDto o)
        {
            o.AlarmCloseComment = "";
            Alarm = o;
        }

        public string WindowKey
        {
            get { return WindowsKeys.CloseAlarmDialog; }
        }

        public object Data
        {
            set 
            {
                _data = (EditDetailsAlarmData)value;
   
                if(_data != null && _data.dto != null)
                {
                    _data.dto.AlarmCloseComment = String.Empty;
                    Alarm = _data.dto;
                    SetDataToField();
                }
                else
                {
                    MsgBox.Warning(LanguageManager.Errors.Error(ErrorCodes.OTHER_ERROR));
                    Container.Resolve<IWindowsManager>().CloseWindow(this.WindowKey);
                }
            }
        }

        private void SetDataToField()
        {
            
        }
    }
}
