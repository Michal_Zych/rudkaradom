﻿using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.UsersService;
using FinderFX.SL.Core.Communication;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.Tools;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Helpers;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Dialogs.Success;

namespace Finder.BloodDonation.Tabs.MenuAdministration.SettingsSystem
{
    public class SettingsManager : ISettingsRespository
    {
        private SystemSettingDto _settings;

        private static IWindowsManager _windowsManager;
        private static IUnityContainer _container;
        public static IUnityContainer Container 
        {
            get
            {
                return _container;
            }
            set
            {
                if(value != null)
                {
                    _container = value;
                    _windowsManager = _container.Resolve<IWindowsManager>();
                    var viewFactory = _container.Resolve<ViewFactory>();
                    _windowsManager.RegisterWindowIfNotRegister(WindowsKeys.SuccessDialog, "Powodzenie", viewFactory.CreateView<Success>(), new Size(440, 170), true);
                }
            }
        }
        public Model.Dto.SystemSettingDto Settings
        {
            get { return _settings; }
        }

        private UsersServiceClient _usersService;
        private UsersServiceClient UsersService
        {
            get
            {
                if(_usersService == null)
                {
                    _usersService = ServiceFactory.GetService<UsersServiceClient>();
                    _usersService.GetSystemSettingsCompleted += _usersService_GetSystemSettingsCompleted;
                    _usersService.SetSystemSettingsCompleted += _usersService_SetSystemSettingsCompleted;
                }

                return _usersService;
            }
        }

        private bool _showErrorMessage;

        private void _usersService_SetSystemSettingsCompleted(object sender, SetSystemSettingsCompletedEventArgs e)
        {
            int i;

            if (int.TryParse(e.Result, out i))
            {
                if (_windowsManager != null)
                {
                    _windowsManager.ShowWindow(WindowsKeys.SuccessDialog, "Zapisano pomyślnie!");
                }
                else
                {
                    MsgBox.Succes("Zapisano pomyślnie");
                }
            }
            else
            {
                MsgBox.Warning("Zapisywanie nie powiodło się!");
            }
        }

        private void _usersService_GetSystemSettingsCompleted(object sender, GetSystemSettingsCompletedEventArgs e)
        {
            if(e.Result != null)
            {
                _settings = e.Result.FirstOrDefault();
            }
        }

        public void Save(Model.Dto.SystemSettingDto settings, bool showErrorMessage = false, bool saveInDataBase = true)
        {
            _showErrorMessage = showErrorMessage;
 
            if(settings != null)
            {
                _settings = settings;

                if(saveInDataBase)
                {
                    bool saved = SaveInDataBase();

                    if(!saved && showErrorMessage)
                    {
                        MsgBox.Warning("Zapisywanie nie powiodło się!");
                    }
                }
            }
        }

        private bool SaveInDataBase()
        {
            if (Settings == null)
                return false;

            UsersService.SetSystemSettingsAsync(this.Settings);
            return true;
        }

        public void Load()
        {
            UsersService.GetSystemSettingsAsync();
        }


        public void SaveT(SystemSettingDto _dataToSave, bool p1, bool p2, IUnityContainer _container)
        {
            Container = _container;

            _showErrorMessage = p1;

            if (_dataToSave != null)
            {
                _settings = _dataToSave;

                if (p2)
                {
                    bool saved = SaveInDataBase();

                    if (!saved && p1)
                    {
                        MsgBox.Warning("Zapisywanie nie powiodło się!");
                    }
                }
            }
        }
    }
}
