﻿using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Tabs.MenuAdministration.SettingsSystem
{
    public interface ISettingsRespository
    {
        SystemSettingDto Settings { get; }

        void Save(SystemSettingDto settings, bool showErrorMessage = false, bool saveInDataBase = true);
        void Load();

        void SaveT(SystemSettingDto _dataToSave, bool p1, bool p2, IUnityContainer Container);
    }
}
