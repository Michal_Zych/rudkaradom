﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Dialogs.Users;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.PermissionsProxy;
using Finder.BloodDonation.UsersService;
using FinderFX.SL.Core.Authentication;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.Extensions;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;
using Telerik.Windows.Controls;
using ViewModelBase = FinderFX.SL.Core.MVVM.ViewModelBase;
using Finder.BloodDonation.Tabs.Contents;
using Global = Finder.BloodDonation.Settings;
namespace Finder.BloodDonation.Tabs.MenuAdministration.LoggedUsers
{
    [UIException]
    public class LoggedUsersViewModel : ViewModelBase, ITabViewModel
    {
        private ViewModelState _state = ViewModelState.Loading;

        [RaisePropertyChanged]
        public ObservableCollection<LoggedUserDto> LoggedUsers { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> RefreshList { get; set; }

        public LoggedUsersViewModel(IUnityContainer container)
            : base(container)
        {
            ViewAttached += UnitsListViewModel_ViewAttached;
            RefreshList = new DelegateCommand<object>(this.RefreshList_Command);
            RefreshList_Command(null);
        }

        public void RefreshList_Command(object obj)
        {
            Service.GetLoggedUsersAsync();
        }


        private UsersServiceClient Service
        {
            get
            {
                var proxy = GetService<UsersServiceClient>(ServicesUri.UsersService);
                proxy.GetLoggedUsersCompleted+=proxy_GetLoggedUsersCompleted;
                return proxy;
            }
        }

        private void proxy_GetLoggedUsersCompleted(object sender, GetLoggedUsersCompletedEventArgs e)
        {
            LoggedUsers = new ObservableCollection<LoggedUserDto>(e.Result);
        }


        private void UnitsListViewModel_ViewAttached(object sender, EventArgs e)
        {
            _state = ViewModelState.Loaded;
            LoadingCompleted(this, EventArgs.Empty);
        }


        public event EventHandler LoadingCompleted = delegate { };

        public void Refresh(IUnit unit)
        {
            //nothing to do
        }

        public ViewModelState State
        {
            get { return _state; }
        }


        public string Title
        {
            get { return "Administracja"; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }
}
