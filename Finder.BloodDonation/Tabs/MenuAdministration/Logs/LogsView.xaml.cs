﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Common.Events;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Telerik.Windows.Controls;

namespace Finder.BloodDonation.Tabs.MenuAdministration.Logs
{
    [ViewModel(typeof(LogsViewModel))]
    public partial class LogsView : UserControl
    {
        public LogsView(IUnityContainer container)
        {
            InitializeComponent();
        }
    }
}
