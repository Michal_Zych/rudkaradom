﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Dialogs.Users;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.PermissionsProxy;
using Finder.BloodDonation.UsersService;
using FinderFX.SL.Core.Authentication;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.Extensions;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;
using Telerik.Windows.Controls;
using ViewModelBase = FinderFX.SL.Core.MVVM.ViewModelBase;
using Finder.BloodDonation.Tabs.Contents;
using Global = Finder.BloodDonation.Settings;

namespace Finder.BloodDonation.Tabs.MenuAdministration.Logs
{
    [UIException]
    public class LogsViewModel : ViewModelBase, ITabViewModel
    {
        private ViewModelState _state = ViewModelState.Loading;

        [RaisePropertyChanged]
        public ObservableCollection<LogEntryDto> LogEntries { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterDateStart { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterDateEnd { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> SearchLogs { get; set; }

        public LogsViewModel(IUnityContainer container)
            : base(container)
        {
            ViewAttached += UnitsListViewModel_ViewAttached;

            SearchLogs = new DelegateCommand<object>(this.SearchLogs_Command);

            InitializeDates();
        }


        private UsersServiceClient Service
        {
            get
            {
                var proxy = GetService<UsersServiceClient>(ServicesUri.UsersService);
                proxy.GetLogsCompleted +=proxy_GetLogsCompleted;
                return proxy;
            }
        }

        public void proxy_GetLogsCompleted(object sender, GetLogsCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                var list = e.Result.ToObservableCollection();

                LogEntries = list;
            }
        }

        public void SearchLogs_Command(object obj)
        {
            if (SearchFilterDateStart.HasValue && SearchFilterDateEnd.HasValue)
            {
                InitializeDates();
            }

            Service.GetLogsAsync(
                new DateTime(
                    SearchFilterDateStart.Value.Year,
                    SearchFilterDateStart.Value.Month,
                    SearchFilterDateStart.Value.Day,
                    0, 0, 0),
                    new DateTime(
                    SearchFilterDateEnd.Value.Year,
                    SearchFilterDateEnd.Value.Month,
                    SearchFilterDateEnd.Value.Day,
                    23, 59, 59));
        }

        private void InitializeDates()
        {
            if (!SearchFilterDateStart.HasValue || !SearchFilterDateEnd.HasValue)
            {
                SearchFilterDateStart = DateTime.Now.AddDays(-Global.Settings.Current.GetInt(Global.ConfigurationKeys.SearchLogsDaysBefore));
                SearchFilterDateEnd = DateTime.Now.AddDays(Global.Settings.Current.GetInt(Global.ConfigurationKeys.SearchLogsDaysAfter));
            }
        }


        private void UnitsListViewModel_ViewAttached(object sender, EventArgs e)
        {
            _state = ViewModelState.Loaded;
            LoadingCompleted(this, EventArgs.Empty);
        }


        public event EventHandler LoadingCompleted = delegate { };

        public void Refresh(IUnit unit)
        {
            //nothing to do
        }

        public ViewModelState State
        {
            get { return _state; }
        }



        public string Title
        {
            get { return "Administracja"; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }
}
