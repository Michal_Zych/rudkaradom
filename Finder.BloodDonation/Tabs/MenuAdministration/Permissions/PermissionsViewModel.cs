﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Dialogs.Users;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.PermissionsProxy;
using Finder.BloodDonation.UsersService;
using FinderFX.SL.Core.Authentication;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.Extensions;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;
using Telerik.Windows.Controls;
using ViewModelBase = FinderFX.SL.Core.MVVM.ViewModelBase;
using Finder.BloodDonation.Tabs.Contents;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Tools;

namespace Finder.BloodDonation.Tabs.MenuAdministration.Permissions
{
    [UIException]
    public class PermissionsViewModel : ViewModelBase, ITabViewModel, IRfidEventTarget
    {
        private readonly IDialogWindow _editWindowDataContext;
        private readonly List<UnitDTO> _allChildren = new List<UnitDTO>();
        private readonly UnitDTO _root;
        private readonly IWindowsManager _windowsManager;
        private List<DBRoleDTO> _availableRoles;
        private UnitDTO _highestUnit;
        private ViewModelState _state = ViewModelState.Loading;
        private UserEditData _userEditData;


        private bool _tagClear;
        private long _uid;

        public string RfidEventTargetName
        {
            get { return "PermissionsView"; }
        }

        public PermissionsViewModel(IUnityContainer container)
            : base(container)
        {
            ViewAttached += UnitsListViewModel_ViewAttached;
            UserAdd = new DelegateCommand<object>(UserAdd_Command);
            UserDeactivate = new DelegateCommand<object>(UserDeactivate_Command);
            UserEdit = new DelegateCommand<object>(UserEdit_Command);
            UserEditPermissions = new DelegateCommand<object>(UserEditPermissions_Command);
            PropertyChanged += OnPropertyChanged;
            Busy = true;


            EventAggregator.GetEvent<TagInfoReceivedEvent>().Subscribe(HandleReadTagInfo);
            
            System.Diagnostics.Debug.WriteLine("PermissionsView; Subscribing to HandleReadTagInfo");

            var viewFactory = container.Resolve<ViewFactory>();
            _windowsManager = container.Resolve<IWindowsManager>();

            
            var editPermissions = viewFactory.CreateView<EditUserPermissionsView>();

            _editWindowDataContext = editPermissions.DataContext as EditUserPermissionsViewModel;

            _windowsManager.RegisterWindow(WindowsKeys.EditUserPermissions, "Edytuj uprawnienia", editPermissions,
                                           new Size(400, 550), true);

            _windowsManager.RegisterWindow(WindowsKeys.EditUser, "Edycja", viewFactory.CreateView<EditUser>(), new Size(400, 430), true);
            _windowsManager.RegisterWindow(WindowsKeys.DeleteUser, "Kasowanie", viewFactory.CreateView<DeleteUser>(), new Size(400, 270), true);

            _root = Container.Resolve<UnitViewModel>("Root").Unit;

            GetData();
        }

        [RaisePropertyChanged]
        public DelegateCommand<object> UserAdd { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> UserDeactivate { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> UserEdit { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> UserEditPermissions { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<UserPermissions> FullPermissions { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<UserPermissions> FilteredPermissions { get; set; }

        [RaisePropertyChanged]
        public UserPermissions CurrentUser { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<UnitDTO> UserTreePermissions { get; set; }

        [RaisePropertyChanged]
        public bool Busy { get; set; }

        private PermissionsServiceClient PermissionsService
        {
            get
            {
                var proxy =
                    GetService<PermissionsServiceClient>(ServicesUri.PermissionsService);
                proxy.GetUserUnitsRolesByUserCompleted += ProxyOnGetUserUnitsRolesByUserCompleted;
                //proxy.GetPermissionsCompleted += ProxyOnGetPermissionsCompleted;
                proxy.GetPermissionsByUserCompleted += ProxyOnGetPermissionsByUserCompleted;
                proxy.GetDataToAdministrationCompleted += new EventHandler<GetDataToAdministrationCompletedEventArgs>(proxy_GetDataToAdministrationCompleted);
                
                return proxy;
            }
        }

        void proxy_GetDataToAdministrationCompleted(object sender, GetDataToAdministrationCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                _availableRoles = new List<DBRoleDTO>(e.Result.AllRoles);
            }
            PermissionsService.GetUserUnitsRolesByUserAsync();
        }

        private UsersServiceClient UsersService
        {
            get
            {
                var proxy = ServiceFactory.GetService<UsersServiceClient>(ServicesUri.UsersService);
                proxy.CreateAccountCompleted +=proxy_CreateAccountCompleted;
                proxy.EditAccountCompleted +=proxy_EditAccountCompleted;
                proxy.DeleteAccountCompleted +=proxy_DeleteAccountCompleted;

                return proxy;
            }
        }




        private const int ERROR_ACCOUNT_EXISTS = -2;
        private const int ERROR = -1;

        private void proxy_DeleteAccountCompleted(object sender, DeleteAccountCompletedEventArgs e)
        {
            if (e.Result < 0)
            {
                MsgBox.Error("Usunięcie konta nie powiodło się.");
            }
            else
            {
                GetData();
            }
        }


        private void proxy_EditAccountCompleted(object sender, EditAccountCompletedEventArgs e)
        {
            if (e.Result < 0)
            {
                MsgBox.Error("Edycja konta nie powiodła się.");
            }
            else
            {
                GetData();
            }
        }

        
        private void proxy_CreateAccountCompleted(object sender, CreateAccountCompletedEventArgs e)
        {
            if (e.Result < 0)
            {
                string msg = "";
                if (e.Result == ERROR_ACCOUNT_EXISTS)
                {
                    msg = "Podany login jest zajęty";
                }
                else if (e.Result == ERROR)
                {
                    msg = "Tworzenie użytkownika nie powiodło się";
                }
                else
                {
                    msg = "Tworzenie użytkownika nie powiodło się";
                }
                MsgBox.Error(msg);
            }
            else
            {
                GetData();
            }
        }

        private UnitsServiceClient UnitsService
        {
            get
            {
                var proxy =
                    GetService<UnitsServiceClient>(ServicesUri.UnitsService);
                proxy.GetUnitForUserCompleted += ProxyOnGetUnitForUserCompleted;
                return proxy;
            }
        }

        #region ITabViewModel Members

        public event EventHandler LoadingCompleted = delegate { };

        public void Refresh(IUnit unit)
        {
            //nothing to do
            EventAggregator.GetEvent<ActiveRfidInfoTargetSetEvent>().Publish(RfidEventTargetName);
        }

        public ViewModelState State
        {
            get { return _state; }
        }

        #endregion

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            switch (propertyChangedEventArgs.PropertyName)
            {
                case "CurrentUser":
                    UserTreePermissions = new ObservableCollection<UnitDTO>();
                    if (CurrentUser == null)
                        return;

                    UnitsService.GetUnitForUserAsync(CurrentUser.Data.User.ID);


                    break;
                default:
                    break;
            }
        }

        private void ProxyOnGetUnitForUserCompleted(object sender, GetUnitForUserCompletedEventArgs eventArgs)
        {
            UserTreePermissions = new ObservableCollection<UnitDTO> { eventArgs.Result };
            EventAggregator.GetEvent<ExpandTreeInEditPermissionWindow>().Publish(null);
            Busy = false;
        }

        private void UserEditPermissions_Command(object obj)
        {
            if (CurrentUser == null)
                return;

            if (!System.Diagnostics.Debugger.IsAttached && CurrentUser.Data.User.ID == User.Current.ID)
            {
                MsgBox.Error("Nie możesz zmieniać uprawnień swojego użytkownika");
            }
            else
            {
                ;
                var d =
                    FullPermissions.FirstOrDefault(x => x.Data.User.ID == CurrentUser.Data.User.ID).Data.Units.Select(
                        x => x.ID).ToList();

                var initialEditData = new EditUserPermissionsInitializationData
                                                  {
                                                      Account = CurrentUser.Data.User,

                                                      AvailableRoles = _availableRoles,
                                                      CurrentRole = CurrentUser.Data.Role,

                                                      Unit = _highestUnit,
                                                      CurrentPermissionsUnitIDs = d,
                                                  };
                _windowsManager.ShowWindow(WindowsKeys.EditUserPermissions, initialEditData);
                EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling, ThreadOption.UIThread,
                                                                       true);
            }
        }

        private void CloseWindowEventHandling(string key)
        {
            if (key == WindowsKeys.EditUserPermissions || key == WindowsKeys.EditUser)
            {
                EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(CloseWindowEventHandling);
            }
            if (key == WindowsKeys.EditUserPermissions)
            {
                GetData();
            }
            
            
            if (key == WindowsKeys.EditUser)
            {
                if (_userEditData.Canceled)
                   ;// MessageBox.Show("Anulowano edycje");
                else
                {
                    if (_userEditData.Account.ID == 0)
                        CreateUser(_userEditData);
                    else EditUser(_userEditData);
                }
            }
             if (key == WindowsKeys.DeleteUser)
             {
                 if(!_userEditData.Canceled)
                 DeleteUser(_userEditData);
             }
        }

        private void EditUser(UserEditData _userEditData)
        {
            UsersService.EditAccountAsync(_userEditData.Account, _userEditData.Reason, _userEditData.LogInfo);
        }


        private void CreateUser(UserEditData _userEditData)
        {
            UsersService.CreateAccountAsync(_userEditData.Account, _highestUnit.ID, _userEditData.Reason, _userEditData.LogInfo);
        }


        private void DeleteUser(UserEditData _userEditData)
        {
           UsersService.DeleteAccountAsync(_userEditData.Account.ID, _userEditData.Reason);
        }

        public void UserEdit_Command(object obj)
        {
            if (CurrentUser != null)
            {
                _userEditData = new UserEditData
                {
                    Account = CurrentUser.Data.User
                };
                EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling, ThreadOption.UIThread, true);
                this._windowsManager.ShowWindow(WindowsKeys.EditUser, _userEditData);
            }
        }

        public void UserAdd_Command(object obj)
        {
            var acc = new AccountDto();
            _userEditData = new UserEditData
            {
                Account = acc
            };
            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling, ThreadOption.UIThread, true);
            this._windowsManager.ShowWindow(WindowsKeys.EditUser, _userEditData);
        }

        public void UserDeactivate_Command(object obj)
        {
            if (CurrentUser != null)
            {
                if (CurrentUser.Data.User.ID == User.Current.ID)
                {
                    MsgBox.Error("Nie możesz skasować swojego użytkownika");
                }
                else
                {
                    _userEditData = new UserEditData
                    {
                        Account = CurrentUser.Data.User
                    };

                    EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling, ThreadOption.UIThread, true);
                    this._windowsManager.ShowWindow(WindowsKeys.DeleteUser, _userEditData);
                }
            }
        }

        public void HandleTagRead(string result)
        {
            string[] parts = result.Split(";".ToCharArray());
            if (parts[2].Equals("00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00"))
            {
                _tagClear = true;
                _uid = unchecked((long)Convert.ToUInt64(parts[1]));
            }
            else
                _tagClear = false;
        }

        public void HandleReadTagInfo(RfidInfo info)
        {
            if (info.ActiveTarget != RfidEventTargetName)
            {
                System.Diagnostics.Debug.WriteLine("PermissionsView; Current view is not rfid active");
                return;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("PermissionsView; Handling rfid info");
            }

            switch (info.Type)
            {
                case Finder.BloodDonation.JsInterop.Bridge.TagTypeLogin:
                case Finder.BloodDonation.JsInterop.Bridge.TagTypeRead:

                    if (!info.Content.Equals(String.Empty))
                    {
                        System.Diagnostics.Debug.WriteLine("uwaga, niepusty tag: {0}", info.Content);
                        MsgBox.Warning("Do zaprogramowania nowego taga użytkownika potrzebny jest czysty tag.");
                    }
                    else
                    {
                        _tagClear = true;
                        _uid = info.Uid;
                    }

                    break;
                case Finder.BloodDonation.JsInterop.Bridge.TagTypeWritten:

                    if (info.Uid == _uid)
                    {
                        var ownTagReprogramming = "Twój tag użytkownika został zmieniony, należy ponownie zalogować się do aplikacji.";
                        var generic = "Nowy tag użytkownika został zaprogramowany, stary jest unieważniony.";

                        var complete = (CurrentUser.Data.User.ID == User.Current.ID) ? generic + ownTagReprogramming : generic;

                        MsgBox.Warning(complete);

                        if (CurrentUser.Data.User.ID == User.Current.ID)
                            EventAggregator.GetEvent<ExternalLogoutEvent>().Publish(0);
                    }
                    else
                    {
                        MsgBox.Error("Błąd podczas programowania taga użytkownika.");
                    }

                    break;
            }
        }

        private void HandleGetAccountCompleted(object sender, GetAccountCompletedEventArgs e)
        {
            Debug.WriteLine("writing tag for user: " + CurrentUser.Data.User.ID);

            // update user uid to new uid from clear tag
            AccountDto account = e.Result;
            account.RfidUid = _uid;
            _tagClear = false;
        }


        private void GetData()
        {
            EventAggregator.GetEvent<ReloadRootEvent>().Publish(false);
            PermissionsService.GetDataToAdministrationAsync();
        }

        private void ProxyOnGetPermissionsByUserCompleted(object sender, GetPermissionsByUserCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                FullPermissions = new ObservableCollection<UserPermissions>();
                //UsersRoles = new ObservableCollection<UserRole>();

                foreach (var r in e.Result)
                {
                    foreach (var unit in r.Units)
                    {
                        if (r.User.ID != ConfigurationKeys.SYSTEM_ADMIN_ID)
                            FullPermissions.Add(new UserPermissions(base.Container)
                            {
                                Data = r,
                                RoleName = r.Role.Name,
                                UnitName = unit.Name,
                                UserName = r.User.Name + " " + r.User.LastName,
                                Login = r.User.Login
                            });
                    }
                }

                FilteredPermissions = new ObservableCollection<UserPermissions>();
                FilteredPermissions = FullPermissions.Distinct(p => p.Data.User.ID).ToObservableCollection();
            }
        }

        private void ProxyOnGetUserUnitsRolesByUserCompleted(object sender,
                                                             GetUserUnitsRolesByUserCompletedEventArgs eventArgs)
        {
            if (eventArgs.Result != null)
            {
                var tmp = new List<UnitDTO>();

                IEnumerable<UserUnitRole> units = eventArgs.Result.ToList()
                    .Where(e => (e.UserID == User.Current.ID)).Select(r => r);


                if (units.Any())
                {
                    units.ToList().ForEach(u =>
                                               {
                                                   var x = _root.FindUnit(u.UnitID);
                                                   if (x != null)
                                                       tmp.Add(x);
                                               });
                }

                _highestUnit = GetHighestUnit(tmp);
                SetAllChildren(_highestUnit); // _allChildren variable

                PermissionsService.GetPermissionsByUserAsync(User.Current.ID);
            }
        }

        private UnitDTO GetHighestUnit(List<UnitDTO> units)
        {
            UnitDTO toReturn = units[0];

            foreach (UnitDTO u in units)
            {
                if (IsUnitChild(u, toReturn))
                    toReturn = u;
            }
            return toReturn;
        }

        private bool IsUnitChild(UnitDTO parrent, UnitDTO child)
        {
            if (parrent.Children.Any())
            {
                UnitDTO tmp = parrent.Children.FirstOrDefault(ch => child.ID == ch.ID);
                if (tmp != null)
                    return true;
                else
                    foreach (UnitDTO unitDto in parrent.Children)
                    {
                        return IsUnitChild(parrent, unitDto);
                    }
            }
            return false;
        }

        private void SetAllChildren(UnitDTO unit)
        {
            if (unit.Children.Any())
            {
                foreach (UnitDTO child in unit.Children)
                {
                    _allChildren.Add(child);
                    SetAllChildren(child);
                }
            }
        }

        private void UnitsListViewModel_ViewAttached(object sender, EventArgs e)
        {
            //fake

            _state = ViewModelState.Loaded;
            LoadingCompleted(this, EventArgs.Empty);
            //InitilizeViewModel(this.Container.Resolve<ViewFactory>().CreateView<UserListView>());
        }



        public string Title
        {
            get { return "Administracja"; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }

    public class UserPermissions : ViewModelBase
    {
        public UserPermissions(IUnityContainer unityContainer)
            : base(unityContainer)
        {
        }

        public UserPermissionsResponse Data { get; set; }

        [RaisePropertyChanged]
        [Display(Name = "Użytkownik")]
        public string UserName { get; set; }

        [RaisePropertyChanged]
        [Display(Name = "Rola")]
        public string RoleName { get; set; }

        [RaisePropertyChanged]
        [Display(Name = "Jednostka")]
        public string UnitName { get; set; }

        [RaisePropertyChanged]
        [Display(Name = "Login")]
        public string Login { get; set; }
    }
}
