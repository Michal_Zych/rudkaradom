﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Microsoft.Practices.Composite.Presentation.Commands;
using System.Collections.Generic;
using System.Linq;
using Finder.BloodDonation.Common;

namespace Finder.BloodDonation.Tabs.Permissions
{
    [UIException]
    public class PermissionUnitViewModel : ViewModelBase
    {
        [RaisePropertyChanged]
        public UnitDTO Unit { get; private set; }

        [RaisePropertyChanged]
        public ObservableCollection<PermissionUnitViewModel> Children { get; private set; }

        [RaisePropertyChanged]
        public bool? IsChecked { get; set; }

        [RaisePropertyChanged]
        public bool? IsCheckedDefault { get; set; }

        [RaisePropertyChanged]
        public Visibility VisibilityDefault { get; set; }

        //[RaisePropertyChanged]
        //public DelegateCommand<bool?> CheckedChangedCommand { get; set; }

        public event EventHandler CheckedChanged = delegate { };

        public PermissionUnitViewModel(IUnityContainer container)
            : base(container)
        {
            VisibilityDefault = Visibility.Collapsed;
            IsCheckedDefault = false;

            this.Children = new ObservableCollection<PermissionUnitViewModel>();

            //CheckedChangedCommand = new DelegateCommand<bool?>(OnCheckedChangedCommand);

            SubscribeForPropertyChanged();
        }

        private void SubscribeForPropertyChanged()
        {
            this.PropertyChanged += PermissionUnitViewModel_PropertyChanged;
        }

        private void UnsubscribeFromPropertyChanged()
        {
            this.PropertyChanged -= PermissionUnitViewModel_PropertyChanged;
        }

        void PermissionUnitViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsChecked")
                OnCheckedChangedCommand(this.IsChecked);
        }

        public void SetUnit(UnitDTO unit)
        {
            this.Unit = unit;

            if (this.Unit.UnitType == Model.Enum.UnitType.DEPARTMENT || this.Unit.UnitType == Model.Enum.UnitType.CENTRE)
            {
                VisibilityDefault = Visibility.Visible;
            }

            if (this.Unit.Children == null)
                return;

            foreach (UnitDTO u in this.Unit.Children)
            {
                PermissionUnitViewModel vm = this.Container.Resolve<PermissionUnitViewModel>();
                vm.SetUnit(u);

                vm.CheckedChanged += new EventHandler(vm_CheckedChanged);

                this.Children.Add(vm);
            }
        }
        private DBUserDTO User = null;
        public void SetUnit(UnitDTO unit, DBUserDTO user)
        {
            this.Unit = unit;

            if (this.Unit.UnitType == Model.Enum.UnitType.DEPARTMENT)// || this.Unit.UnitType == Model.Enum.UnitType.CENTRE)
            {
                VisibilityDefault = Visibility.Visible;
            }

            User = user;

            if (this.User.DefaultUnits != null)
            {
                DefaultUsersUnitsDTO def = this.User.DefaultUnits.FirstOrDefault(x => x.UnitID == unit.ID);
                if (def != null)
                {
                    IsCheckedDefault = true;
                }
                else
                {
                    IsCheckedDefault = false;
                }
            }
            else
            {
                IsCheckedDefault = false;
            }

            if (this.Unit.Children == null)
                return;

            foreach (UnitDTO u in this.Unit.Children)
            {
                PermissionUnitViewModel vm = this.Container.Resolve<PermissionUnitViewModel>();
                vm.SetUnit(u);

                vm.CheckedChanged += new EventHandler(vm_CheckedChanged);

                this.Children.Add(vm);
            }
        }

        void vm_CheckedChanged(object sender, EventArgs e)
        {
            UnsubscribeFromPropertyChanged();
            this.IsChecked = CheckChildren();
            SubscribeForPropertyChanged();
            CheckedChanged(this, EventArgs.Empty);
        }

        private bool? CheckChildren()
        {
            int count = this.Children.Count;
            int checkedCount = 0;
            int nullCount = 0;

            foreach (PermissionUnitViewModel vm in this.Children)
            {
                if (vm.IsChecked.HasValue && vm.IsChecked.Value)
                    checkedCount++;
                if (!vm.IsChecked.HasValue)
                    nullCount++;
            }

            if (checkedCount == count)
                return true;
            else if (checkedCount + nullCount == 0)
                return false;
            else
                return null;
        }

        public void FillCheckedDefaultDepartments(List<UnitDTO> units)
        {
            if (IsCheckedDefault.HasValue && IsCheckedDefault.Value)
            {
                units.Add(Unit);
            }

            if (Children != null)
            {
                for (int i = 0; i < Children.Count; i++)
                {
                    Children[i].FillCheckedDefaultDepartments(units);
                }
            }
        }


        public void OnCheckedChangedCommand(bool? isChecked)
        {
            ChangeChecked(isChecked);

            CheckedChanged(this, EventArgs.Empty);
        }

        public void ChangeChecked(bool? isChecked)
        {
            if (!isChecked.HasValue)
                return;

            UnsubscribeFromPropertyChanged();
            this.IsChecked = isChecked;
            SubscribeForPropertyChanged();

            foreach (PermissionUnitViewModel vm in this.Children)
            {
                vm.ChangeChecked(isChecked);
            }
        }


        public void SetPermissions(IList<UserUnitRoleDTO> uurs)
        {
            UserUnitRoleDTO u = uurs.FirstOrDefault(x => x.UnitID == this.Unit.ID);
            if (u != null)
            {
                IsChecked = true;
            }
            else
            {
                IsChecked = false;
                foreach (PermissionUnitViewModel vm in this.Children)
                {
                    vm.SetPermissions(uurs);
                }
            }


        }

        public void GetCheckedUnits(IList<int> units)
        {
            if (this.IsChecked.HasValue && this.IsChecked.Value)
            {
                units.Add(this.Unit.ID);
            }
            else
            {
                foreach (PermissionUnitViewModel vm in this.Children)
                {
                    vm.GetCheckedUnits(units);
                }
            }
        }


    }

}
