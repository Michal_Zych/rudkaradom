﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.ModelServices;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.PermissionsProxy;
using System.Collections.Generic;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Common;

namespace Finder.BloodDonation.Tabs.Permissions
{
    [UIException]
    public class UnitPermissionsViewModel : ViewModelBase
    {

        [RaisePropertyChanged]
        public UserUnitRoleDTO UserUnitRole { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<PermissionUnitViewModel> AvailableUnits { get; set; }


        [RaisePropertyChanged]
        public DelegateCommand<object> SavePermissionsCommand { get; set; }

        [RaisePropertyChanged]
        public string CurrentState { get; set; }

        public UnitPermissionsViewModel(IUnityContainer container)
            : base(container)
        {
            this.EventAggregator.GetEvent<SelectedUserChangedEvent>()
                .Subscribe(OnSelectedUserChangedEvent);

            this.SavePermissionsCommand = new DelegateCommand<object>(OnSavePermissionsCommand);

            this.CurrentState = "Hidden";
        }

        public void OnSelectedUserChangedEvent(UserUnitRoleDTO uur)
        {
            this.UserUnitRole = uur;

            if (this.UserUnitRole == null)
            {
                this.CurrentState = "Hidden";
                return;
            }

            this.CurrentState = "Normal";

            UnitsServiceClient proxy = ServiceFactory.GetService<UnitsServiceClient>("Finder-BloodDonation-Model-Services-UnitsService.svc");
            proxy.GetUnitCompleted += new EventHandler<GetUnitCompletedEventArgs>(proxy_GetUnitCompleted);
            proxy.GetUnitAsync(this.UserUnitRole.UnitID);

        }

        void proxy_GetUnitCompleted(object sender, GetUnitCompletedEventArgs e)
        {
            AvailableUnits = new ObservableCollection<PermissionUnitViewModel>();

            PermissionUnitViewModel vm = this.Container.Resolve<PermissionUnitViewModel>();
            if (this.UserUnitRole != null)
            {
                vm.SetUnit(e.Result, this.UserUnitRole.User);
            }
            else
            {
                vm.SetUnit(e.Result);
            }

            AvailableUnits.Add(vm);

            if (this.UserUnitRole == null)
                return;

            PermissionsServiceClient proxy = ServiceFactory.GetService<PermissionsServiceClient>("Finder-BloodDonation-Model-Services-PermissionsService.svc");
            proxy.GetUserUnitsRolesCompleted += new EventHandler<GetUserUnitsRolesCompletedEventArgs>(proxy_GetUserUnitsRolesCompleted);
            proxy.GetUserUnitsRolesAsync(this.UserUnitRole.UserID, this.UserUnitRole.RoleID);
        }

        void proxy_GetUserUnitsRolesCompleted(object sender, GetUserUnitsRolesCompletedEventArgs e)
        {
            ObservableCollection<UserUnitRoleDTO> uurs = e.Result;
            SetPermissions(uurs);
        }

        private void SetPermissions(IList<UserUnitRoleDTO> uurs)
        {
            foreach (PermissionUnitViewModel vm in AvailableUnits)
            {
                vm.SetPermissions(uurs);
            }
        }

        public void OnSavePermissionsCommand(object obj)
        {
            IList<int> checkedUnits = new List<int>();
            List<UnitDTO> default_units = new List<UnitDTO>();
            foreach (PermissionUnitViewModel vm in this.AvailableUnits)
            {
                vm.GetCheckedUnits(checkedUnits);
                vm.FillCheckedDefaultDepartments(default_units);
            }

            PermissionsServiceClient proxy = ServiceFactory.GetService<PermissionsServiceClient>("Finder-BloodDonation-Model-Services-PermissionsService.svc");
            proxy.UpdateUserUnitRoleCompleted += new EventHandler<UpdateUserUnitRoleCompletedEventArgs>(proxy_UpdateUserUnitRoleCompleted);

            ObservableCollection<UserUnitRoleDTO> uurs = new ObservableCollection<UserUnitRoleDTO>();
            foreach (int u in checkedUnits)
            {
                UserUnitRoleDTO dto = new UserUnitRoleDTO
                {
                    UserID = this.UserUnitRole.UserID,
                    RoleID = this.UserUnitRole.RoleID,
                    UnitID = u
                };
                uurs.Add(dto);
            }

            PermissionsServiceClient proxyp = GetService<PermissionsServiceClient>("Finder-BloodDonation-Model-Services-PermissionsService.svc");
            proxy.SetDefaultUnitsUserCompleted += new EventHandler<SetDefaultUnitsUserCompletedEventArgs>(proxy_SetDefaultUnitsUserCompleted);

            proxy.SetDefaultUnitsUserAsync(
                this.UserUnitRole.UserID,
                new ObservableCollection<int>((from uu in default_units select uu.ID).ToList()));

            proxy.UpdateUserUnitRoleAsync(uurs);
        }

        void proxy_SetDefaultUnitsUserCompleted(object sender, SetDefaultUnitsUserCompletedEventArgs e)
        {

        }

        void proxy_UpdateUserUnitRoleCompleted(object sender, UpdateUserUnitRoleCompletedEventArgs e)
        {

        }
    }
}
