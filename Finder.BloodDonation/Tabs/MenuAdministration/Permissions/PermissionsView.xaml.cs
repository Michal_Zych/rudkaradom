﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Common.Events;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Telerik.Windows.Controls;

namespace Finder.BloodDonation.Tabs.MenuAdministration.Permissions
{
	[ViewModel(typeof(PermissionsViewModel))]
	public partial class PermissionsView : UserControl
	{
		public PermissionsView(IUnityContainer container)
		{
			InitializeComponent();
            container.Resolve<IDynamicEventAggregator>().GetEvent<ExpandTreeInEditPermissionWindow>().Subscribe(ExpandTreeInEditPermissionWindowHandler, true);
		}

	    private void ExpandTreeInEditPermissionWindowHandler(object obj)
	    {
			Dispatcher.BeginInvoke(() =>
									   {
										   //tree.ExpandAll();
										   //tree.ExpandItemByPath("RCKiK");
									   });
	    }
	}
}
