using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.ServiceStatus
{
    public class ServiceStatusFilter : SortedFilter
    {
       [RaisePropertyChanged]
public string ServiceName { get; set; }

[RaisePropertyChanged]
public string Description { get; set; }

[RaisePropertyChanged]
public DateTime? StartTimeUtcFrom { get; set; }
[RaisePropertyChanged]
public DateTime? StartTimeUtcTo { get; set; }

[RaisePropertyChanged]
public DateTime? PingTimeUtcFrom { get; set; }
[RaisePropertyChanged]
public DateTime? PingTimeUtcTo { get; set; }

[RaisePropertyChanged]
public DateTime? PreviousPingTimeUtcFrom { get; set; }
[RaisePropertyChanged]
public DateTime? PreviousPingTimeUtcTo { get; set; }

[RaisePropertyChanged]
public bool? HasPartner { get; set; }

[RaisePropertyChanged]
public DateTime? PartnerPingTimeUtcFrom { get; set; }
[RaisePropertyChanged]
public DateTime? PartnerPingTimeUtcTo { get; set; }

[RaisePropertyChanged]
public string InfoTxt { get; set; }



        public ServiceStatusFilter(IUnityContainer container)
            : base(container)
        {

        }
    }
}
