using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Events;
using System.Collections.Generic;
using System.Collections;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Dialogs.Patients;
using Finder.BloodDonation.Dialogs.Helpers;
using Microsoft.Practices.Composite.Presentation.Events;
using System.Diagnostics;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.Dialogs.Bands;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.UsersService;

namespace Finder.BloodDonation.Tabs.ServiceStatus
{
    [OnCompleted]
    [UIException]
    public class ServiceStatusViewModel : ViewModelBase, ITabViewModel
    {
        private readonly IWindowsManager _windowsManager;

        public event EventHandler LoadingCompleted = delegate { };
        private IUnit SelectedUnit = null;
        private IUnit _currentUnit;

        private ServiceStatusDto selectedItem;

        private UsersServiceClient Proxy { get; set; }

        private IUnitFiltered _details;
        [RaisePropertyChanged]
        public IUnitFiltered Details
        {
            get
            {
                if (_details == null)
                {
                    _details = Container.Resolve<ServiceStatusDetailsListViewModel>();
                }
                return _details;
            }
            set
            {
                _details = value;
            }
        }

        public ServiceStatusViewModel(IUnityContainer container)
            : base(container)
        {
            this.ViewAttached += new EventHandler(ViewModel_ViewAttached);
            EventAggregator.GetEvent<SelectedItemChangedEvent<ServiceStatusDto>>().Subscribe(OnSelectedItemChangedEvent);

            var viewFactory = container.Resolve<ViewFactory>();
            _windowsManager = this.Container.Resolve<IWindowsManager>();
            
            Proxy = ServiceFactory.GetService<UsersServiceClient>();
        }

        public void OnSelectedItemChangedEvent(IItemViewModel<ServiceStatusDto> obj)
        {
            if(obj == null)
            {
                selectedItem = null;
            }
            else
            {
                selectedItem = obj.Item;
            }
            SelectedItemChanged();
        }

        private void SelectedItemChanged()
        {

        }
        
        private void ViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public void Refresh(IUnit unit)
        {
            SelectedUnit = unit;
            _currentUnit = unit;

            Details.Refresh(unit);
		}

        
        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        public string Title
        {
            get { return ""; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }

        private void CloseWindowEventHandling(string key)
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(CloseWindowEventHandling);
        }
    }
}
