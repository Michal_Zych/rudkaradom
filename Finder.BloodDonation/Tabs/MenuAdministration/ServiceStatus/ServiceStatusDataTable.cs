using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataTable;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Filters;

namespace Finder.BloodDonation.Tabs.ServiceStatus
{
    public class ServiceStatusDataTable : BaseDataTable
    {

        public ServiceStatusDataTable(IUnityContainer container)
            : base(container)
        {
        }

        public override ObservableCollection<IDataTableColumn> GetColumns()
        {
            return new ObservableCollection<IDataTableColumn>()
             .Add(50)
             .Add(100, "ServiceName", "Nazwa", FilterDataType.String)
.Add(100, "Description", "Opis", FilterDataType.String)
.Add(100, "StartTimeUtc", "Data uruchomienia", FilterDataType.DateTime)
.Add(100, "PingTimeUtc", "Ostatnia komunikacja", FilterDataType.DateTime)
.Add(100, "PreviousPingTimeUtc", "Poprzednia komunikacja", FilterDataType.DateTime)
.Add(100, "HasPartner", "Ma serwis wsp�pracuj�cy", FilterDataType.Bool)
.Add(100, "PartnerPingTimeUtc", "Komunikacja z serwisem wsp�pracuj�cym", FilterDataType.DateTime)
.Add(100, "InfoTxt", "Status", FilterDataType.String)
             ;
        }


        public override ISortedFilter GetFilter(IUnityContainer container)
        {
            return new ServiceStatusFilter(container);
        }

		public override bool StartWithDefaultColumns
        {
            get
            {
                return false;
            }
        }
    }
}
