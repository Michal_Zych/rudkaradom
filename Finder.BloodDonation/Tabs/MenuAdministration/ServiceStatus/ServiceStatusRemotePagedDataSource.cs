using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.ServiceStatus
{
    public class ServiceStatusRemotePagedDataSource: IPagedDataSource<ServiceStatusItemViewModel>
    {
        protected IUnityContainer _container;
        protected int _pageSize;
        protected IDataTable _dataTable;
        protected IUnitDataClient _dataClient;

        public ServiceStatusRemotePagedDataSource(IUnityContainer container, int pageSize, IDataTable dataTable, IUnitDataClient dataClient)
        {
            _container = container;
            _pageSize = pageSize;
            _dataTable = dataTable;
            _dataClient = dataClient;
        }


		 public void FetchData(int pageNumber, Action<PagedDataResponse<ServiceStatusItemViewModel>> responseCallback)
        {
            var proxy = _dataClient.GetProxy();
            var list = new List<ServiceStatusItemViewModel>();
            
            proxy.GetServiceStatusFilteredCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<ServiceStatusItemViewModel>();
                    vm.Item = item;
                    vm.DataTable = _dataTable;
                    list.Add(vm);
                }
                _container.Resolve<ServiceStatusItemViewModel>().SelectionChanged(-1);
                proxy.GetServiceStatusFilteredTotalCountAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary());
            };

            proxy.GetServiceStatusFilteredTotalCountCompleted += (s, e) =>
            {
                responseCallback(new PagedDataResponse<ServiceStatusItemViewModel>()
                {
                    Items = list,
                    TotalItemCount = e.Result
                });
            };

            proxy.GetServiceStatusFilteredAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary(), pageNumber, _pageSize);
            
        }
    }
}
