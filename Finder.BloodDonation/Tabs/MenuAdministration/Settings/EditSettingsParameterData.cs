﻿using Finder.BloodDonation.UsersService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.MenuAdministration.Settings
{
    public class EditSettingsParameterData
    {
        public SettingItem SettingItem { get; set; }
        public bool IsEdited { get; set; }
        
        public bool Canceled = true;
    }
}
