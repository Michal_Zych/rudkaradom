﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Dialogs.Base;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.Dialogs.Helpers;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Dialogs;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.Common.Layout;

namespace Finder.BloodDonation.Tabs.MenuAdministration.Settings
{
    public class EditSettingsParameterModel : FinderFX.SL.Core.MVVM.ViewModelBase, IDialogWindow
    {
        public EditSettingsParameterData _data;

        public RelayCommand OkClick { get; set; }
        public RelayCommand CancelClick { get; set; }

        [RaisePropertyChanged]
        public string ParameterValue { get; set; }
        [RaisePropertyChanged]
        public string ParameterName { get; set; }


        public string WindowKey
        {
            get { return WindowsKeys.EditSettingsParameter; }
        }

        public object Data
        {
            set
            {
                _data = (EditSettingsParameterData)value;

                if (_data.IsEdited)
                {
                    ParameterName = _data.SettingItem.Name;
                    ParameterValue = _data.SettingItem.Value;
                }
                else
                {
                    ParameterName = _data.SettingItem.Name;
                    ParameterValue = _data.SettingItem.Default;
                }
            }
        }

        public EditSettingsParameterModel(IUnityContainer container)
            : base(container)
        {
            OkClick = new RelayCommand(OnOkClick);
            CancelClick = new RelayCommand(OnCanceledClick);
        }

        public void OnCanceledClick()
        {
            Close(CloseButton.Cancel);
        }

        public void OnOkClick()
        {
            Close(CloseButton.OK);
        }

        private void Close(CloseButton closeAction)
        {
            if(closeAction.Equals(CloseButton.OK))
            {
                string errorMsg = "";
                
                if (errorMsg != "")
                {
                    MsgBox.Error(errorMsg);
                    return;
                }

                SaveData();
            }

            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        private void SaveData()
        {
            _data.Canceled = false;

            _data.SettingItem.Value = ParameterValue;
        }
    }
}
