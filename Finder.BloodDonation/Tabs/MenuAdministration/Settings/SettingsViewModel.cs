﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Dialogs.Users;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.PermissionsProxy;
using Finder.BloodDonation.UsersService;
using FinderFX.SL.Core.Authentication;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.Extensions;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;
using Telerik.Windows.Controls;
using ViewModelBase = FinderFX.SL.Core.MVVM.ViewModelBase;
using Finder.BloodDonation.Tabs.Contents;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Tools;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.LanguageHelpher;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Dialogs;
using Finder.BloodDonation.Tabs.MenuAdministration.SettingsSystem;
using Finder.BloodDonation.Dialogs.Success;

namespace Finder.BloodDonation.Tabs.MenuAdministration.Settings
{
    [UIException]
    public class SettingsViewModel : ViewModelBase, ITabViewModel
    {
        // TODO: Stworzenie okienka do edycji parametrów,
        //       Edycja parametrów.
        // 

        private List<SystemSettingDto> _data;
        private SystemSettingDto _dataToSave;
        private IUnit _currentUnitId = null;

        public RelayCommand SaveSetting { get; set; }
        public RelayCommand RefreshSetting { get; set; }

        private readonly IWindowsManager _windowsManager;
        private EditSettingsParameterData _parameterData;

        [RaisePropertyChanged]
        public int SelectedIndex { get; set; }

        [RaisePropertyChanged]
        public bool EditEnabled { get; set; }

        [RaisePropertyChanged]
        public bool ValueEnabled { get; set; }

        [RaisePropertyChanged]
        public bool IsReasonInSystemView { get; set; }

        [RaisePropertyChanged]
        public string Cause { get; set; }

        [RaisePropertyChanged]
        public string SaveTextButton
        {
            get
            {
                return LanguageHelpher.LanguageManager.ControlsText.Text(ControlsTextCodes.SAVE_BUTTON);
            }
        }

        [RaisePropertyChanged]
        public string RefreshTextButton
        {
            get
            {
                return LanguageHelpher.LanguageManager.ControlsText.Text(ControlsTextCodes.REFRESH_BUTTON);
            }
        }

        [RaisePropertyChanged]
        public string UserId
        {
            get
            {
                return BloodyUser.Current.ID.ToString();
            }
        }

        [RaisePropertyChanged]
        public string UserName
        {
            get
            {
                return BloodyUser.Current.Identity.Name;
            }
        }

        [RaisePropertyChanged]
        public bool   NoConnectionFinishingAlarms { get; set; }

        [RaisePropertyChanged]
        public bool   FullMonitoringUnassignedBands { get; set; }

        [RaisePropertyChanged]
        public string WarningAutoCloseMessage { get; set; }

        [RaisePropertyChanged]
        public int    EventToArchiveAfterDays { get; set; }

        [RaisePropertyChanged]
        public int    MaxPasswordAgeDays { get; set; }

        [RaisePropertyChanged]
        public int    MonitoringSoundIntervalSecs { get; set; }

        [RaisePropertyChanged]
        public int    DefMessageActivityMinutes { get; set; }

        [RaisePropertyChanged]
        public int    MaxRowsAlarmsPopup { get; set; }

        [RaisePropertyChanged]
        public string UnitsLocationSeparator { get; set; }

        [RaisePropertyChanged]
        public int    MaxDaysReports { get; set; }

        [RaisePropertyChanged]
        public bool   LoBatteryWrActive { get; set; }

        [RaisePropertyChanged]
        public int    LoBatteryWrMins { get; set; }

        [RaisePropertyChanged]
        public bool   LoBatteryAlActive { get; set; }

        [RaisePropertyChanged]
        public int    LoBatteryAlMins { get; set; }

        [RaisePropertyChanged]
        public int    BatteryTermWrDaysBefore { get; set; }

        [RaisePropertyChanged]
        public string BatteryTermWrMessage { get; set; }

        [RaisePropertyChanged]
        public int    BatteryTermAlDaysBefore { get; set; }

        [RaisePropertyChanged]
        public string    BatteryTermAlMessage { get; set; }

        [RaisePropertyChanged]
        public int    DayStartHour { get; set; }

        [RaisePropertyChanged]
        public int    DayStartHourMin { get; set; }

        [RaisePropertyChanged]
        public int    DayEndHour { get; set; }

        [RaisePropertyChanged]
        public int    DayEndHourMin { get; set; }

        [RaisePropertyChanged]
        public bool   NoTransmitterWrActive { get; set; }

        [RaisePropertyChanged]
        public int    NoTransmitterWrMins { get; set; }

        [RaisePropertyChanged]
        public bool   NoTransmitterAlActive { get; set; }

        [RaisePropertyChanged]
        public int    NoTransmitterAlMins { get; set; }

        [RaisePropertyChanged]
        public string Reason { get; set; }

        [RaisePropertyChanged]
        public string FirstBandAssignmentReason { get; set; }

        public DelegateCommand<SettingItem> SelectedSettingChanged { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<SettingItem> SettingItems { get; set; }

        public SettingsViewModel(IUnityContainer container)
            : base(container)
        {
            SaveSetting = new RelayCommand(OnSaveSettings);
            RefreshSetting = new RelayCommand(OnRefreshSettings);

            _windowsManager = this.Container.Resolve<IWindowsManager>();
            var viewFactory = container.Resolve<ViewFactory>();

            Container.Resolve<ISettingsRespository>().Load();
            

            _data = new List<SystemSettingDto>();
            _data.Add(Container.Resolve<ISettingsRespository>().Settings);
            SetDataToFields();
        }

        [RaisePropertyChanged]
        public int RoleDefForNewUser { get; set; }

        private void SetDataToFields()
        {
            var a = _data.FirstOrDefault();

            NoConnectionFinishingAlarms = a.NoConnectionFinishingAlarms;
            FullMonitoringUnassignedBands = a.FullMonitoringUnassignedBands;
            WarningAutoCloseMessage = a.WarningAutoCloseMessage;
            EventToArchiveAfterDays = a.EventToArchiveAfterDays;
            MaxPasswordAgeDays = a.MaxPasswordAgeDays;
            MonitoringSoundIntervalSecs = a.MonitoringSoundIntervalSecs;
            DefMessageActivityMinutes = a.DefMessageActivityMinutes;
            MaxRowsAlarmsPopup = a.MaxRowsAlarmsPopup;
            UnitsLocationSeparator = a.UnitsLocationSeparator;
            MaxDaysReports = a.MaxDaysReports;
            LoBatteryWrActive = a.LoBatteryWrActive;
            LoBatteryWrMins = a.LoBatteryWrMins;
            LoBatteryAlActive = a.LoBatteryAlActive;
            LoBatteryAlMins = a.LoBatteryAlMins;
            BatteryTermWrDaysBefore = a.BatteryTermWrDaysBefore;
            BatteryTermWrMessage = a.BatteryTermWrMessage;
            BatteryTermAlDaysBefore = a.BatteryTermAlDaysBefore;
            BatteryTermAlMessage  = a.BatteryTermAlMessage;
            DayStartHour = a.DayStartHour.Hour;
            DayStartHourMin = a.DayStartHour.Minute;
            DayEndHour = a.DayEndHour.Hour;
            DayEndHourMin = a.DayEndHour.Minute;
            NoTransmitterWrActive  = a.NoTransmitterWrActive;
            NoTransmitterWrMins = a.NoTransmitterWrMins;
            NoTransmitterAlActive = a.NoTransmitterAlActive;
            NoTransmitterAlMins = a.NoTransmitterAlMins;
            FirstBandAssignmentReason = a.FirstBandAssignmentReason;
            IsReasonInSystemView = a.ShowReasonField;
            RoleDefForNewUser = a.RoleDefForNewUser;
        }

        public void OnRefreshSettings()
        {
            Cancel(CloseButton.Refresh);
        }

        private void Cancel(CloseButton closeButton)
        {
            if (closeButton.Equals(CloseButton.OK))
            {
                SaveData();
            }
            else
            {
                Refresh(_currentUnitId);
            }
        }

        private void SaveData()
        {
            if(ValidateFields())
            {
                _dataToSave = new SystemSettingDto();

                _dataToSave.NoConnectionFinishingAlarms = NoConnectionFinishingAlarms;
                _dataToSave.FullMonitoringUnassignedBands = FullMonitoringUnassignedBands;
                _dataToSave.WarningAutoCloseMessage = WarningAutoCloseMessage;
                _dataToSave.EventToArchiveAfterDays = EventToArchiveAfterDays;
                _dataToSave.MaxPasswordAgeDays = MaxPasswordAgeDays;
                _dataToSave.MonitoringSoundIntervalSecs = MonitoringSoundIntervalSecs;
                _dataToSave.DefMessageActivityMinutes = DefMessageActivityMinutes;
                _dataToSave.MaxRowsAlarmsPopup = MaxRowsAlarmsPopup;
                _dataToSave.UnitsLocationSeparator = UnitsLocationSeparator;
                _dataToSave.MaxDaysReports = MaxDaysReports;
                _dataToSave.LoBatteryWrActive = LoBatteryWrActive;
                _dataToSave.LoBatteryWrMins = LoBatteryWrMins;
                _dataToSave.LoBatteryAlActive = LoBatteryAlActive;
                _dataToSave.LoBatteryAlMins = LoBatteryAlMins;
                _dataToSave.BatteryTermWrDaysBefore = BatteryTermWrDaysBefore;
                _dataToSave.BatteryTermWrMessage = BatteryTermWrMessage;
                _dataToSave.BatteryTermAlDaysBefore = BatteryTermAlDaysBefore;
                _dataToSave.BatteryTermAlMessage = BatteryTermAlMessage;
                _dataToSave.DayStartHour = new DateTime(1900, 1, 1, DayStartHour, DayStartHourMin, 0);
                _dataToSave.DayEndHour = new DateTime(1900, 1, 1, DayEndHour, DayEndHourMin, 0);
                _dataToSave.NoTransmitterWrActive = NoTransmitterWrActive;
                _dataToSave.NoTransmitterWrMins = NoTransmitterWrMins;
                _dataToSave.NoTransmitterAlActive = NoTransmitterAlActive;
                _dataToSave.NoTransmitterAlMins = NoTransmitterAlMins;
                _dataToSave.FirstBandAssignmentReason = FirstBandAssignmentReason;
                _dataToSave._LocalAdminPhone = String.Empty;
                _dataToSave.OldDataColor = String.Empty;
                _dataToSave.ShowReasonField = IsReasonInSystemView;
                _dataToSave.RoleDefForNewUser = RoleDefForNewUser;
                _dataToSave.Reason = Reason;
                Container.Resolve<ISettingsRespository>().SaveT(_dataToSave, false, true, Container);
            }
        }

        private void OnSaveSettings()
        {
            Cancel(CloseButton.OK);
        }

        public bool ValidateFields()
        {
            bool somethingChanged = false;
            var a = _data.FirstOrDefault();

            if (a.NoConnectionFinishingAlarms != NoConnectionFinishingAlarms)
                somethingChanged = true;

            if (a.FullMonitoringUnassignedBands != FullMonitoringUnassignedBands)
                somethingChanged = true;

            if (a.WarningAutoCloseMessage != WarningAutoCloseMessage)
                somethingChanged = true;

            if (a.EventToArchiveAfterDays != EventToArchiveAfterDays)
                somethingChanged = true;

            if (a.MaxPasswordAgeDays != MaxPasswordAgeDays)
                somethingChanged = true;

            if (a.MonitoringSoundIntervalSecs != MonitoringSoundIntervalSecs)
                somethingChanged = true;

            if (a.DefMessageActivityMinutes != DefMessageActivityMinutes)
                somethingChanged = true;

            if (a.MaxRowsAlarmsPopup != MaxRowsAlarmsPopup)
                somethingChanged = true;

            if (a.UnitsLocationSeparator != UnitsLocationSeparator)
                somethingChanged = true;

            if (a.MaxDaysReports != MaxDaysReports)
                somethingChanged = true;

            if (a.LoBatteryWrActive != LoBatteryWrActive)
                somethingChanged = true;

            if (a.LoBatteryWrMins != LoBatteryWrMins)
                somethingChanged = true;

            if (a.LoBatteryAlActive != LoBatteryAlActive)
                somethingChanged = true;

            if (a.LoBatteryAlMins != LoBatteryAlMins)
                somethingChanged = true;

            if (a.BatteryTermWrDaysBefore != BatteryTermWrDaysBefore)
                somethingChanged = true;

            if (a.BatteryTermWrMessage != BatteryTermWrMessage)
                somethingChanged = true;

            if (a.BatteryTermAlDaysBefore != BatteryTermAlDaysBefore)
                somethingChanged = true;

            if (a.BatteryTermAlMessage != BatteryTermAlMessage)
                somethingChanged = true;

            if (a.DayStartHour != new DateTime(1900, 1, 1, DayStartHour, DayStartHourMin, 0))
                somethingChanged = true;

            if (a.DayEndHour != new DateTime(1900, 1, 1, DayEndHour, DayEndHourMin, 0))
                somethingChanged = true;

            if (a.NoTransmitterWrActive != NoTransmitterWrActive)
                somethingChanged = true;

            if (a.NoTransmitterWrMins != NoTransmitterWrMins)
                somethingChanged = true;

            if (a.NoTransmitterAlActive != NoTransmitterAlActive)
                somethingChanged = true;

            if (a.NoTransmitterAlMins != NoTransmitterAlMins)
                somethingChanged = true;

            if (a.FirstBandAssignmentReason != FirstBandAssignmentReason)
                somethingChanged = true;

            if (a.ShowReasonField != IsReasonInSystemView)
                somethingChanged = true;

            if (a.RoleDefForNewUser != RoleDefForNewUser)
                somethingChanged = true;

            if (!somethingChanged)
                MsgBox.Warning("Nie wykryto zmian!");

            return somethingChanged;
        }

        public void Refresh(IUnit unit)
        {
            _currentUnitId = unit;

            Container.Resolve<ISettingsRespository>().Load();
            _data = new List<SystemSettingDto>();
            _data.Add(Container.Resolve<ISettingsRespository>().Settings);
            SetDataToFields();
        }

        public event EventHandler LoadingCompleted = delegate { };

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        public string Title
        {
            get { return "Administracja"; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }
}
