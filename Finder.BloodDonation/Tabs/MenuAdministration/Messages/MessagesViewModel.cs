﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Dialogs.Users;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.PermissionsProxy;
using Finder.BloodDonation.UsersService;
using FinderFX.SL.Core.Authentication;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.Extensions;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;
using Telerik.Windows.Controls;
using ViewModelBase = FinderFX.SL.Core.MVVM.ViewModelBase;
using Finder.BloodDonation.Tabs.Contents;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.Converters;
using System.Windows.Controls;
using Finder.BloodDonation.Dialogs.Messages;
using Global = Finder.BloodDonation.Settings;

namespace Finder.BloodDonation.Tabs.MenuAdministration.Messages
{
    [UIException]
    public class MessagesViewModel : ViewModelBase, ITabViewModel
    {
        [RaisePropertyChanged]
        public ObservableCollection<MessageDto> Messages { get; set; }

        private MessageDto _selectedMessage;
        private ReceiverDto _selectedReceiver;
        private MessageDialogData _messageDialogData;
        private readonly IWindowsManager _windowsManager;

        [RaisePropertyChanged]
        public ObservableCollection<ReceiverDto> Receivers { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterDateStart { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterTimeStart { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterTimeEnd { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterDateEnd { get; set; }

        [RaisePropertyChanged]
        public bool IsNewMessageEnabled { get; set; }

        [RaisePropertyChanged]
        public bool IsCloseMessageEnabled { get; set; }

        [RaisePropertyChanged]
        public bool IsDeleteMessageEnabled { get; set; }

        [RaisePropertyChanged]
        public bool IsUnlockReceiverEnabled { get; set; }

        [RaisePropertyChanged]
        public bool IsUnlockAllReceiversEnabled { get; set; }




        public DelegateCommand<object> SearchMessages { get; set; }

        public DelegateCommand<object> NewMessage { get; set; }
        
        public DelegateCommand<object> CloseMessage { get; set; }
        
        public DelegateCommand<object> DeleteMessage { get; set; }
        
        public DelegateCommand<object> UnlockReceiver { get; set; }
        
        public DelegateCommand<object> UnlockAllReceivers { get; set; }

        public DelegateCommand<MessageDto> SelectedMessageChanged { get; set; }

        public DelegateCommand<ReceiverDto> SelectedReceiverChanged { get; set; }
        

        private ViewModelState _state = ViewModelState.Loading;

        private UsersServiceClient Service
        {
            get
            {
                var proxy = GetService<UsersServiceClient>(ServicesUri.UsersService);
                proxy.GetMessagesCompleted += proxy_GetMessagesCompleted;
                proxy.GetMessageReceiversCompleted += proxy_GetMessageReceiversCompleted;
                proxy.DeleteReceiversCompleted += proxy_DeleteReceiversCompleted;
                proxy.DeleteMessagesCompleted += proxy_DeleteMessagesCompleted;
                proxy.CloseMessagesCompleted +=proxy_CloseMessagesCompleted;
                proxy.CreateMessageCompleted += proxy_CreateMessageCompleted;
                return proxy;
            }
        }

        const int ErrorValue = -1;
        void proxy_CreateMessageCompleted(object sender, CreateMessageCompletedEventArgs e)
        {
            if (e.Result == ErrorValue)
            {
                MsgBox.Error("Nie udało się utworzyć komunikatu.");
            }
            else
            {
                SearchMessages_Command(null);
            }
        }

        


        private void proxy_GetMessageReceiversCompleted(object sender, GetMessageReceiversCompletedEventArgs e)
        {
            bool receiversEnabled = false;
            if (e.Result != null)
            {
                var list = e.Result.ToObservableCollection();

                Receivers = list;
                if(Receivers.Count > 0 && _selectedMessage.MessageType == MessageType.Instant)
                    receiversEnabled = true;
            }

            IsUnlockAllReceiversEnabled = receiversEnabled;
        }

        private void proxy_GetMessagesCompleted(object sender, GetMessagesCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                var list = e.Result.ToObservableCollection();

                Messages = list;
            }    
        }


        public MessagesViewModel(IUnityContainer container)
            : base(container)
        {
            InitializeDates();
            SearchMessages = new DelegateCommand<object>(SearchMessages_Command);
            NewMessage = new DelegateCommand<object>(NewMessage_Command);
            CloseMessage = new DelegateCommand<object>(CloseMessage_Command);
            DeleteMessage = new DelegateCommand<object>(DeleteMessage_Command);
            UnlockReceiver = new DelegateCommand<object>(UnlockReceiver_Command);
            UnlockAllReceivers = new DelegateCommand<object>(UnlockAllReceivers_Command);

            SetDefaultButtonsState();

            SelectedMessageChanged = new DelegateCommand<MessageDto>(SelectedMessageChanged_Command);
            SelectedReceiverChanged = new DelegateCommand<ReceiverDto>(SelectedReceiverChanged_Command);
            ViewAttached += UnitsListViewModel_ViewAttached;

            _selectedMessage = null;
            _selectedReceiver = null;
            _windowsManager = this.Container.Resolve<IWindowsManager>();
            var viewFactory = container.Resolve<ViewFactory>();
            _windowsManager.RegisterWindow(WindowsKeys.MessageDialog, "Komunikat", viewFactory.CreateView<MessageDialog>(), new Size(410, 310), true);
        }

        private void SetDefaultButtonsState()
        {
            IsNewMessageEnabled = true;
            IsCloseMessageEnabled = false;
            IsDeleteMessageEnabled = false;
            IsUnlockReceiverEnabled = false;
            IsUnlockAllReceiversEnabled = false;
        }

        public void UnlockAllReceivers_Command(object obj)
        {
            MsgBox.Confirm("Czy chcesz skasować wszystkich odbiorców?", new EventHandler<WindowClosedEventArgs>(OnConfirmUnlockAllReceivers));
        }

        private void OnConfirmUnlockAllReceivers(object sender, WindowClosedEventArgs e)
        {
            bool? confirmed = (bool?)e.DialogResult;

            if (confirmed.HasValue && confirmed.Value)
                DeleteReceivers("");
        }

        public void UnlockReceiver_Command(object obj)
        {
            DeleteReceivers(_selectedReceiver.ID.ToString());
        }

        private void DeleteReceivers(string receivers)
        {
            Service.DeleteReceiversAsync(_selectedMessage.ID, receivers);
        }

        private void proxy_DeleteReceiversCompleted(object sender, DeleteReceiversCompletedEventArgs e)
        {
            if (e.Result)
            {
                Service.GetMessageReceiversAsync(_selectedMessage.ID);
            }
            else
            {
                MsgBox.Error("Usunięcie nie powiodło się.");
            }
        }


        public void DeleteMessage_Command(object obj)
        {
            MsgBox.Confirm("Czy chcesz skasować komunikat?", new EventHandler<WindowClosedEventArgs>(OnConfirmDeleteMessage));
        }

        private void OnConfirmDeleteMessage(object sender, WindowClosedEventArgs e)
        {
            bool? confirmed = (bool?)e.DialogResult;

            if (confirmed.HasValue && confirmed.Value)
            {
                Service.DeleteMessagesAsync(_selectedMessage.ID.ToString());
            }
        }


        void proxy_DeleteMessagesCompleted(object sender, DeleteMessagesCompletedEventArgs e)
        {
            if (e.Result)
            {
                SearchMessages_Command(null);
            }
            else
            {
                MsgBox.Error("Usunięcie nie powiodło się.");
            }          
        }


        public void CloseMessage_Command(object obj)
        {
            MsgBox.Confirm("Czy chcesz zakończyć wyświetlanie komunikatu?", new EventHandler<WindowClosedEventArgs>(OnConfirmCloseMessage));
        }

        private void OnConfirmCloseMessage(object sender, WindowClosedEventArgs e)
        {
            bool? confirmed = (bool?)e.DialogResult;

            if (confirmed.HasValue && confirmed.Value)
            {
                Service.CloseMessagesAsync(_selectedMessage.ID.ToString());
            }
        }

        private void proxy_CloseMessagesCompleted(object sender, CloseMessagesCompletedEventArgs e)
        {
            if (e.Result)
            {
                SearchMessages_Command(null);
            }
            else
            {
                MsgBox.Error("Dezaktywacja komunikatu nie powiodło się.");
            }      
        }

        public void NewMessage_Command(object obj)
        {
            _messageDialogData = new MessageDialogData();

            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling, ThreadOption.UIThread, true);
            this._windowsManager.ShowWindow(WindowsKeys.MessageDialog, _messageDialogData);
        }

        private void CloseWindowEventHandling(string key)
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(CloseWindowEventHandling);
            if (key == WindowsKeys.MessageDialog)
            {
                if (!_messageDialogData.Canceled)
                {
                    Service.CreateMessageAsync(User.Current.ClientID, _messageDialogData.Message,
                        _messageDialogData.ActiveFrom, _messageDialogData.ActiveTo, _messageDialogData.Mode);
                }
            }
        }


        private void UnitsListViewModel_ViewAttached(object sender, EventArgs e)
        {
            _state = ViewModelState.Loaded;
            LoadingCompleted(this, EventArgs.Empty);
        }

        public void SelectedMessageChanged_Command(MessageDto msg)
        {
            _selectedMessage = msg;
            if (msg == null)
            {
                Receivers = null;
                SetDefaultButtonsState();
            }
            else
            {
                IsCloseMessageEnabled = ((msg.EndDate > DateTime.Now) && (msg.StartDate != msg.EndDate));
                IsDeleteMessageEnabled = true;
                Service.GetMessageReceiversAsync(msg.ID);
            }
        }

        public void SelectedReceiverChanged_Command(ReceiverDto user)
        {
            if (user == null || _selectedMessage == null || _selectedMessage.MessageType == MessageType.AfterLogon)
            {
                IsUnlockReceiverEnabled = false;
            }
            else IsUnlockReceiverEnabled = true;

            _selectedReceiver = user;
        }

        public void SearchMessages_Command(object obj)
        {
            if (SearchFilterDateStart.HasValue && SearchFilterDateEnd.HasValue)
            {
                InitializeDates();
            }

            Service.GetMessagesAsync(
                new DateTime(
                    SearchFilterDateStart.Value.Year,
                    SearchFilterDateStart.Value.Month,
                    SearchFilterDateStart.Value.Day,
                    0, 0, 0),
                    new DateTime(
                    SearchFilterDateEnd.Value.Year,
                    SearchFilterDateEnd.Value.Month,
                    SearchFilterDateEnd.Value.Day,
                    23, 59, 59));
        }


        private void InitializeDates()
        {
            if (!SearchFilterDateStart.HasValue || !SearchFilterDateEnd.HasValue)
            {
                SearchFilterDateStart = DateTime.Now.AddDays(-Global.Settings.Current.GetInt(Global.ConfigurationKeys.SearchMessagesDaysBefore));
                SearchFilterDateEnd = DateTime.Now.AddDays(Global.Settings.Current.GetInt(Global.ConfigurationKeys.SearchMessagesDaysAfter));
            }
        }


        public void Refresh(IUnit unit)
        {

        }

        public event EventHandler LoadingCompleted = delegate { };

        public ViewModelState State
        {
            get { return _state; }
        }



        public string Title
        {
            get { return "Administracja"; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }
}
