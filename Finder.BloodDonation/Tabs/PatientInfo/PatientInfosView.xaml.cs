using System.Windows.Controls;
using FinderFX.SL.Core.MVVM;

namespace Finder.BloodDonation.Tabs.PatientInfo
{
    [ViewModel(typeof(PatientInfosViewModel))]
    public partial class PatientInfosView : UserControl
    {
        public PatientInfosView()
        {
            InitializeComponent();
        }
    }
}
