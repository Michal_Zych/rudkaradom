using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Controls;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Dialogs.Patients;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.UsersService;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;

namespace Finder.BloodDonation.Tabs.PatientInfo
{
    [OnCompleted]
    [UIException]
    public class PatientInfosViewModel: ViewModelBase, ITabViewModel
    {
        //TODO: Zmieni� PatientInfosViewModel doda� do niego id oddzia��w oraz id sal, b�d� pobiera� PatientDTO po id pacjenta.
        public event EventHandler LoadingCompleted = delegate { };
        private IUnit SelectedUnit = null;
        private IUnit _currentUnit;

        public RelayCommand AdministrationSelectionChanged { get; set; }

        private readonly IWindowsManager _windowsManager;

		private IUnitFiltered _details;
        private PatientEditData _patientEditData;

        public UnitsServiceClient Proxy { get; set; }

        public UsersServiceClient UsersServiceProxy { get; set; }

        [RaisePropertyChanged]
        public bool RegistrationVisible { get; set; }

        [RaisePropertyChanged]
        public int AdministrationSelectedIndex { get; set; }

        private PatientAssignData _patientAssignData;
        private AssignBandData _assignBandData;

        private PatientDTO selectedItem;
        private PatientInfoDto selected;

        [RaisePropertyChanged]
        public IUnitFiltered Details
        {
            get
            {
                if (_details == null)
                {
                    _details = Container.Resolve<PatientInfosDetailsListViewModel>();
                }
                return _details;
            }
            set
            {
                _details = value;
            }
        }

        [RaisePropertyChanged]
        public Image BaseImage { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<StatusImage> StatusImages { get; set; }

        public RelayCommand CreatePatientInfoCommand { get; set; }

        public PatientInfosViewModel(IUnityContainer container)
            : base(container)
        {
            Image tmp =new Image();
            tmp.Source = new BitmapImage(new Uri("/Finder.BloodDonation;component/Images/Exit.png", UriKind.Relative));
            
            BaseImage = tmp;

            IList<StatusImage> tmps = new List<StatusImage>();
            for (int i = 0; i < 5; i++)
            {
                tmp = new Image();
                tmp.Source =
                    new BitmapImage(new Uri("/Finder.BloodDonation;component/Images/Exit.png", UriKind.Relative));

                StatusImage s = new StatusImage();
                s.Image = tmp;
                s.ActionType = ActionType.Flashes;
                s.IsActiveAction = true;
                tmps.Add(s);
            }

            StatusImages = new ObservableCollection<StatusImage>(tmps);

            this.ViewAttached += new EventHandler(ViewModel_ViewAttached);
            EventAggregator.GetEvent<SelectedItemChangedEvent<PatientInfoDto>>().Subscribe(OnSelectedItemChangedEvent);

            var viewFactory = container.Resolve<ViewFactory>();

            CreatePatientInfoCommand = new RelayCommand(OnCreatePatientInfo);
            AdministrationSelectionChanged = new RelayCommand(OnAdministrationSelectionChanged);

            _windowsManager = this.Container.Resolve<IWindowsManager>();

            if (_windowsManager != null)
            {
                /*_windowsManager.RegisterWindow(WindowsKeys.EditPatient, "Dodaj",
                    viewFactory.CreateView<EditPatient>(), new Size(400, 430), true);
                _windowsManager.RegisterWindow(WindowsKeys.PatientAssign, "Przypisanie",
                    viewFactory.CreateView<PatientAssign>(), new Size(290, 400), true);
                _windowsManager.RegisterWindow(WindowsKeys.AssignBand, "Przypisanie",
                    viewFactory.CreateView<AssignBand>(), new Size(290,400), true);*/
            }
            Proxy = ServiceFactory.GetService<UnitsServiceClient>();
            Proxy.AssignPatientToWardRoomCompleted += Proxy_AssignPatientToWardRoomCompleted;
            Proxy.AssignBandToObjectCompleted += Proxy_AssignBandToObjectCompleted;
            UsersServiceProxy = ServiceFactory.GetService<UsersServiceClient>();
            UsersServiceProxy.GetSettingsTCompleted += Proxy_GetSettingsTCompleted;
            UsersServiceProxy.GetSettingsTAsync();
        }

        private void Proxy_AssignBandToObjectCompleted(object sender, AssignBandToObjectCompletedEventArgs e)
        {
            Refresh(_currentUnit);
        }

        public void OnSelectedItemChangedEvent(IItemViewModel<PatientInfoDto> obj)
        {
            if (obj == null)
            {
                selectedItem = null;
                selected = null;
            }
            else
            {
                selected = obj.Item;
                SelectedItemChanged();
            }
        }

        private void SelectedItemChanged()
        {
            PatientDTO dto = new PatientDTO();
            dto.ID = selected.Id == null ? (short)-1 : (short)selected.Id;
            dto.FirstName = selected.FirstName;
            dto.LastName = selected.LastName;
            dto.Pesel = selected.Pesel.ToString();
            dto.BandId = selected.BandId;
            var dtoDepartmentId = dto.DepartmentId;
            var dtoRoomId = dto.RoomId;
            if (int.TryParse(selected.Department, out dtoDepartmentId))
            {
                dto.DepartmentId = dtoDepartmentId;
            }

            int roomId;
            if (int.TryParse(selected.Room, out roomId))
            {
                dto.RoomId = roomId;
            }

            _patientAssignData = new PatientAssignData();
            _patientAssignData.Patient = dto;
        }

        private void Proxy_AssignPatientToWardRoomCompleted(object sender, AssignPatientToWardRoomCompletedEventArgs e)
        {
            Refresh(_currentUnit);
        }

        public void OnAdministrationSelectionChanged()
        {
            if (selected != null)
            {
                if (AdministrationSelectedIndex == 0)
                {
                    _patientAssignData.AssignWindowType = AssignWindowType.AssignToDepartment;
                    _windowsManager.ShowWindow(WindowsKeys.PatientAssign, _patientAssignData);
                    EventAggregator.GetEvent<CloseWindowEvent>()
                        .Subscribe(CloseAssignWindowEventHandling, ThreadOption.UIThread, true);
                }
                else if (AdministrationSelectedIndex == 3)
                {
                    _patientAssignData.AssignWindowType = AssignWindowType.AssignToRoom;
                    _windowsManager.ShowWindow(WindowsKeys.PatientAssign, _patientAssignData);
                    EventAggregator.GetEvent<CloseWindowEvent>()
                        .Subscribe(CloseAssignWindowEventHandling, ThreadOption.UIThread, true);
                }
                else if (AdministrationSelectedIndex == 5)
                {
                    const byte PATIENT_OBJECT_TYPE = 4;

                    _assignBandData = new AssignBandData();
                    _assignBandData.AssignedBandInfo = new AssignedBandInfoDto();
                    _assignBandData.AssignedBandInfo.ObjectId = (int) selected.Id;
                    _assignBandData.AssignedBandInfo.ObjectType = PATIENT_OBJECT_TYPE;
                    _assignBandData.AssignedBandInfo.OperatorId = BloodyUser.Current.ID;

                    _windowsManager.ShowWindow(WindowsKeys.AssignBand, _assignBandData);
                    EventAggregator.GetEvent<CloseWindowEvent>()
                        .Subscribe(CloseAssignBandWindowEventHandling, ThreadOption.UIThread, true);
                }
            }
        }

        private void CloseAssignBandWindowEventHandling(string obj)
        {
            if (_assignBandData != null && _assignBandData.AssignedBandInfo != null)
                Proxy.AssignBandToObjectAsync(_assignBandData.AssignedBandInfo, false);
        }

        private void CloseAssignWindowEventHandling(string obj)
        {
            //if (_patientAssignData != null && _patientAssignData.Patient != null)
            //    Proxy.AssignPatientToWardRoomAsync(_patientAssignData.Patient);
        }

        private void Proxy_GetSettingsTCompleted(object sender, GetSettingsTCompletedEventArgs e)
        {
            if(e.Result == null)
                return;

            for (int i = 0; i < e.Result.Count; i++)
            {
                if (e.Result[i].Name.Equals("RegistrationWardId"))
                {
                    UnitTypeOrganizer.SetRegistrationUnitId(int.Parse(e.Result[i].Value));
                }
                else if(e.Result[i].Name.Equals("UnassignedPatientsUnitId"))
                {
                     UnitTypeOrganizer.SetUnassignedPatientsUnitId(int.Parse(e.Result[i].Value));
                }
                else if (e.Result[i].Name.Equals("ForceBandAssignment"))
                {
                    UnitTypeOrganizer.SetForceBandAssignment(bool.Parse(e.Result[i].Value));
                }
            }

            if (CheckIfSelectedUnitIsRegistration(_currentUnit))
            {
                RegistrationVisible = true;
            }
        }

        private bool CheckIfSelectedUnitIsRegistration(IUnit selectedUnit)
        {
            UnitDTO _temp = (UnitDTO)UnitManager.Instance.Units.Where(e => e.ID == selectedUnit.Identity).FirstOrDefault();

            if (_temp == null)
                return false;
            if (_temp.ID == UnitTypeOrganizer.GetRegistartionUnitId())
                return true;

            if (_temp.Parent == null)
                return false;

            UnitDTO _parent = _temp.Parent;

            while (_parent.ID != UnitTypeOrganizer.GetRegistartionUnitId())
            {
                _parent = _parent.Parent;

                if (_parent == null)
                {
                    return false;
                }
            }

            if (_parent == null)
            {
                return false;
            }
            if (_parent.ID == UnitTypeOrganizer.GetRegistartionUnitId())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Proxy_PatientToHospitalCompleted(object sender, PatientToHospitalCompletedEventArgs e)
        {
            
        }

        private void ViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public void Refresh(IUnit unit)
        {
            SelectedUnit = unit;
            _currentUnit = unit;

            Details.Refresh(unit);

            if (CheckIfSelectedUnitIsRegistration(SelectedUnit))
                RegistrationVisible = true;
            else
                RegistrationVisible = false;  // Zmieni� na false.
        }

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        public string Title
        {
            get { return ""; }
        }
        
        public void OnCreatePatientInfo()
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling);

            _patientEditData = new PatientEditData();
            _patientEditData.Patient = new PatientDTO();

            this._windowsManager.ShowWindow(WindowsKeys.EditPatient, _patientEditData);
        }

        public void CloseWindowEventHandling(string key)
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(CloseWindowEventHandling);

            if (key.Equals(WindowsKeys.EditPatient))
            {
                if (_patientEditData.Canceled)
                {

                }
                else
                {
                    //Proxy.PatientToHospitalAsync();
                    //EditPatient(_patientEditData);
                }
            }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }
}
