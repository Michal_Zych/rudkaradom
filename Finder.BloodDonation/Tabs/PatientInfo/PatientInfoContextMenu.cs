using Finder.BloodDonation.Context;
using Finder.BloodDonation.Tools;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;

namespace Finder.BloodDonation.Tabs.PatientInfo
{
    [FinderFX.SL.Core.Communication.OnCompleted(AttributeTargetMembers = "regex:Completed")]
    public class PatientInfoContextMenu : GenericContextMenuItemsCollection<PatientInfoItemViewModel>
    {
        private IUnityContainer container { get; set; }

        public PatientInfoContextMenu(PatientInfoItemViewModel vm, IUnityContainer container)
            : base(vm)
        {
            this.container = container;
        }

        public override void RegisterItems(System.Collections.Generic.IList<IContextMenuItem> items)
        {
            items.Add(new PatientInfoContextItem(this.AssociatedObject)
            {
                Header = "Opcja 1",
                Command = new DelegateCommand<object>(OnOption1)
            });
        }

        private void OnOption1(object obj)
        {
            //container.Resolve<IDynamicEventAggregator>().GetEvent<eventName>().Publish(AssociatedObject.Item);
            MsgBox.Warning("Opcja 1");
        }
    }


    public class PatientInfoContextItem : GenericMenuItem
    {
        private PatientInfoItemViewModel Item { get; set; }
        public PatientInfoContextItem(PatientInfoItemViewModel vm)
        {
            Item = vm;
        }

        public override bool IsAvailable()
        {
            return Item.Item != null;
        }
    }

}
