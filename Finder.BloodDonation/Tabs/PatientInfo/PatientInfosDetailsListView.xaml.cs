using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Finder.BloodDonation.Behaviors;
using Finder.BloodDonation.Tools;
using FinderFX.SL.Core.MVVM;
using Telerik.Windows.Controls;

namespace Finder.BloodDonation.Tabs.PatientInfo
{
    [ViewModel(typeof(PatientInfosDetailsListViewModel))]
    public partial class PatientInfosDetailsListView : UserControl, IDataContextChangedHandler<PatientInfosDetailsListView>
    {
        private const string SCROLLERS_SYNCHRO_GROUP = "PatientInfo";


        public PatientInfosDetailsListView()
        {
            InitializeComponent();
            DataContextChangedHelper<PatientInfosDetailsListView>.Bind(this);

            DetailsList.LayoutUpdated += RegisterScrollsAfterLayoutUpdated;
        }


        private void RegisterScrollsAfterLayoutUpdated(object sender, EventArgs e)
        {
            if (ScrollViewersSynchronizer.IsGroupRegistered(SCROLLERS_SYNCHRO_GROUP))
            {
               //throw new Exception("Powtórzona grupa synchronizacji suwaków: " + SCROLLERS_SYNCHRO_GROUP);
            }
            ScrollViewersSynchronizer.Register(Filters, SCROLLERS_SYNCHRO_GROUP);
            ScrollViewersSynchronizer.Register(headerScroller, SCROLLERS_SYNCHRO_GROUP);
            ScrollViewersSynchronizer.Register(DetailsList, SCROLLERS_SYNCHRO_GROUP);
            DetailsList.LayoutUpdated -= RegisterScrollsAfterLayoutUpdated;
        }



        public void DataContextChangedm(PatientInfosDetailsListView sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                var vm = e.NewValue as PatientInfosDetailsListViewModel;

                CommandManager.SetCommandBindings(this, vm.GetCommandBindings());
            }
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {


            BindingExpression binding = null;
            if (sender is TextBox)
            {
                binding = (sender as TextBox).GetBindingExpression(TextBox.TextProperty);
            }
            else if (sender is RadDateTimePicker)
            {
                binding = (sender as RadDateTimePicker).GetBindingExpression(RadDateTimePicker.SelectedValueProperty);
            }
            if (binding != null)
            {
                binding.UpdateSource();
            }
            base.OnKeyUp(e);
        }
        
    }
}
