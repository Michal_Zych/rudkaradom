using System;
using System.Collections.Generic;
using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DynamicData;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;

namespace Finder.BloodDonation.Tabs.PatientInfo
{
    public class PatientInfosDataSource : IPagedDataSource<PatientInfoItemViewModel>
    {
        protected int _pageSize;
        protected IUnityContainer _container;
        private IFilter _filter;
        private IAscyncDataSource<PatientInfoDto> _dataSource;

        public PatientInfosDataSource(IUnityContainer container, int pageSize, IFilter filter, IAscyncDataSource<PatientInfoDto> dataSource)
        {
            _container = container;
            _pageSize = pageSize;
            _filter = filter;
            _dataSource = dataSource;
        }

        public void FetchData(int pageNumber, Action<PagedDataResponse<PatientInfoItemViewModel>> responseCallback)
        {
            var items = new List<PatientInfoItemViewModel>();
            
            _dataSource.GetData(_pageSize, pageNumber, _filter, (list) =>
                {
                    if(list != null)
                        foreach(var item in list)
                        {
                            var vm = _container.Resolve<PatientInfoItemViewModel>();
                            vm.Item = item;
                            items.Add(vm);
                        }

                    _dataSource.GetTotalCount(_filter, (count) =>
                        {
                            responseCallback(new PagedDataResponse<PatientInfoItemViewModel>()
                            {
                                Items = items,
                                TotalItemCount = count
                            });
                        });
                });
        }
    }
}
