using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Context;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Tools;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;

namespace Finder.BloodDonation.Tabs.PatientInfo
{
    public class PatientInfoItemViewModel : ViewModelBase, IItemViewModel<PatientInfoDto>
    {
        [RaisePropertyChanged]
        public PatientInfoDto Item { get; set; }

        [RaisePropertyChanged]
        public GenericContextMenuItemsCollection<PatientInfoItemViewModel> Context { get; set; }

        public DelegateCommand<object> ItemDoubleClick { get; set; }

        public PatientInfoItemViewModel(IUnityContainer container)
            : base(container)
        {
            SetContext();
            ItemDoubleClick = new DelegateCommand<object>(OnItemDoubleClick);
        }

        private void OnItemDoubleClick(object obj)
        {

            //MsgBox.Warning("dobule click ");
        }

        private void SetContext()
        {
            Context = new PatientInfoContextMenu(this, this.Container);
        }
        

     
        public void SelectionChanged(long id)
        {
            EventAggregator.GetEvent<DetailsItemClickedEvent>().Publish(id);
        }
        public override string ToString()
        {
            return Item.ToString();
        }


        public Common.Filters.IDataTable DataTable
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
