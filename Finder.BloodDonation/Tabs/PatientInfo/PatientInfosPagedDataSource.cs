using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.Filters;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;

namespace Finder.BloodDonation.Tabs.PatientInfo
{
    public class PatientInfoPagedDataSource: IPagedDataSource<PatientInfoItemViewModel>
    {
        protected IUnityContainer _container;
        protected int _pageSize;
        protected IFilter _filter;
        protected IUnitDataClient _dataClient;
		protected int unitId;

        public PatientInfoPagedDataSource(IUnityContainer container, int pageSize, IFilter filter, IUnitDataClient dataClient, int unitId)
        {
            _container = container;
            _pageSize = pageSize;
            _filter = filter;
            _dataClient = dataClient;
			this.unitId = unitId;
        }

        public void FetchData(int pageNumber, Action<PagedDataResponse<PatientInfoItemViewModel>> responseCallback)
        {
            var proxy = _dataClient.GetProxy();
            var list = new List<PatientInfoItemViewModel>();

            proxy.GetPatientsCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<PatientInfoItemViewModel>();
                    vm.Item = item;
                    list.Add((vm));
                }
                responseCallback(new PagedDataResponse<PatientInfoItemViewModel>()
                {
                    Items = list,
                    TotalItemCount = list.Count
                });
            };

            proxy.GetPatientsAsync();

            

            /*proxy.GetPatientInfosFilteredCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<PatientInfoItemViewModel>();
                    vm.Item = item;
                    list.Add(vm);
                }
                _container.Resolve<PatientInfoItemViewModel>().SelectionChanged(-1);
                proxy.GetPatientInfosFilteredTotalCountAsync((Dictionary<string, string>)_filter.GetFilterDictionary());
            };

            proxy.GetPatientInfosFilteredTotalCountCompleted += (s, e) =>
            {
                responseCallback(new PagedDataResponse<PatientInfoItemViewModel>()
                {
                    Items = list,
                    TotalItemCount = e.Result
                });
            };

            proxy.GetPatientInfosFilteredAsync((Dictionary<string, string>)_filter.GetFilterDictionary(), pageNumber, _pageSize, unitId, 0);*/
        }
    }
}
