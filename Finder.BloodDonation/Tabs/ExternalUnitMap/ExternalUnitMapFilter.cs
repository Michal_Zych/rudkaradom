using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.ExternalUnitMap
{
    public class ExternalUnitMapFilter : SortedFilter
    {
       [RaisePropertyChanged]
public string ExtId { get; set; }

[RaisePropertyChanged]
public string ExtDescription { get; set; }

[RaisePropertyChanged]
public string M2mId { get; set; }

[RaisePropertyChanged]
public string M2mDescription { get; set; }



        public ExternalUnitMapFilter(IUnityContainer container)
            : base(container)
        {

        }
    }
}
