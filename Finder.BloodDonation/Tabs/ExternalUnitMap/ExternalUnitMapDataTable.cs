using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataTable;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Filters;

namespace Finder.BloodDonation.Tabs.ExternalUnitMap
{
    public class ExternalUnitMapDataTable : BaseDataTable
    {

        public ExternalUnitMapDataTable(IUnityContainer container)
            : base(container)
        {
        }

        public override ObservableCollection<IDataTableColumn> GetColumns()
        {
            return new ObservableCollection<IDataTableColumn>()
             .Add(50)
			 .Add(100, "ExtId", "ID AMMS", FilterDataType.Int)
.Add(100, "ExtDescription", "Opis AMMS", FilterDataType.String)
.Add(100, "M2mId", "ID M2M", FilterDataType.Int)
.Add(100, "M2mDescription", "Opis M2M", FilterDataType.String)
             ;
        }


        public override ISortedFilter GetFilter(IUnityContainer container)
        {
            return new ExternalUnitMapFilter(container);
        }

		public override bool StartWithDefaultColumns
        {
            get
            {
                return false;
            }
        }
    }
}
