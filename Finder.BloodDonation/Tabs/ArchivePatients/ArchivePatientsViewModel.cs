using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Events;
using System.Collections.Generic;
using System.Collections;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Dialogs.Patients;
using Finder.BloodDonation.Dialogs.Helpers;
using Microsoft.Practices.Composite.Presentation.Events;
using System.Diagnostics;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.Dialogs.Bands;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.UsersService;
using Finder.BloodDonation.Tabs.Reports.BandsTracking;

namespace Finder.BloodDonation.Tabs.ArchivePatients
{
    [OnCompleted]
    [UIException]
    public class ArchivePatientsViewModel : ViewModelBase, ITabViewModel
    {
        private readonly IWindowsManager _windowsManager;

        public RelayCommand OnlyAlarms { get; set; }
        public RelayCommand AllEvents { get; set; }
        public RelayCommand HistoryOfStay { get; set; }

        public event EventHandler LoadingCompleted = delegate { };
        private IUnit SelectedUnit = null;
        private IUnit _currentUnit;

        private ArchivePatientsDto selectedItem;

        private UsersServiceClient Proxy { get; set; }

        public ReportInfo ReportInfo { get; set; }

        private ITabViewModel _reportDetails;
        [RaisePropertyChanged]
        public ITabViewModel ReportDetails
        {
            get
            {
                if(_reportDetails == null)
                {
                    _reportDetails = Container.Resolve<BandsTrackingReportsListViewModel>();

                }

                ReportInfo = new ReportInfo() { ReportTypes = Enums.ReportTypes.PatientHistory };
                EventAggregator.GetEvent<SetBandsTrackingReportViewTypeEvent>().Publish(ReportInfo);

                return _reportDetails;
            }
            set
            {
                _reportDetails = value;
            }
        }

        private IUnitFiltered _details;
        [RaisePropertyChanged]
        public IUnitFiltered Details
        {
            get
            {
                if (_details == null)
                {
                    _details = Container.Resolve<ArchivePatientsDetailsListViewModel>();
                }
                return _details;
            }
            set
            {
                _details = value;
            }
        }

        public ArchivePatientsViewModel(IUnityContainer container)
            : base(container)
        {
            this.ViewAttached += new EventHandler(ViewModel_ViewAttached);
            EventAggregator.GetEvent<SelectedItemChangedEvent<ArchivePatientsDto>>().Subscribe(OnSelectedItemChangedEvent);
            EventAggregator.GetEvent<RequestToRepeatMessageEvent>().Subscribe(OnRepeatSend);

            var viewFactory = container.Resolve<ViewFactory>();
            _windowsManager = this.Container.Resolve<IWindowsManager>();

            Proxy = ServiceFactory.GetService<UsersServiceClient>(/*ServicesUri.UsersService*/);

            OnlyAlarms = new RelayCommand(OnOnlyAlarms);
            AllEvents = new RelayCommand(OnAllEvents);
            HistoryOfStay = new RelayCommand(OnHistoryOfStay);
        }

        public void OnRepeatSend(object obj)
        {
            EventAggregator.GetEvent<SetBandsTrackingReportViewTypeEvent>().Publish(ReportInfo);
        }

        private void OnHistoryOfStay()
        {
            EventAggregator.GetEvent<GetArchiveHistoryOfStayEvent>().Publish(null);
        }

        private void OnAllEvents()
        {
            EventAggregator.GetEvent<GetAllArchiveEventsEvent>().Publish(null);
        }

        private void OnOnlyAlarms()
        {
            EventAggregator.GetEvent<GetOnlyArchiveAlarmsEvent>().Publish(null);
        }

        public void OnSelectedItemChangedEvent(IItemViewModel<ArchivePatientsDto> obj)
        {
            if(obj == null)
            {
                selectedItem = null;
            }
            else
            {
                selectedItem = obj.Item;
            }
            SelectedItemChanged();
        }

        private void SelectedItemChanged()
        {

        }
        
        private void ViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public void Refresh(IUnit unit)
        {
            SelectedUnit = unit;
            _currentUnit = unit;

            ReportInfo = new ReportInfo() { ReportTypes = Enums.ReportTypes.PatientHistory };
            EventAggregator.GetEvent<SetBandsTrackingReportViewTypeEvent>().Publish(ReportInfo);

            Details.Refresh(unit);
		}
        
        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        public string Title
        {
            get { return "Archiwum"; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }


        private void CloseWindowEventHandling(string key)
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(CloseWindowEventHandling);
        }
    }
}
