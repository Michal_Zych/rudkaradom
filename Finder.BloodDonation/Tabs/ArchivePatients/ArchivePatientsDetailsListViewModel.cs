using System.Collections;
using System.Linq;
using Finder.BloodDonation.Behaviors;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.DynamicData.Paging;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;
using Telerik.Windows.Controls.DragDrop;
using Finder.BloodDonation.Model.Dto;
using FinderFX.SL.Core.Extensions;
using FinderFX.SL.Core.Authentication;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Common.Events;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Windows;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.Settings;
using System.Windows.Input;
using Finder.BloodDonation.DynamicData.Memory;
using Finder.BloodDonation.DynamicData.DBs;
using System.Globalization;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Dialogs.DataTable;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.Tabs.EventsDetails;

namespace Finder.BloodDonation.Tabs.ArchivePatients
{
    [OnCompleted]
    [UIException]
    public class ArchivePatientsDetailsListViewModel : ViewModelBase, IUnitFiltered, IUnitDataClient
    {
        private IUnit _currentUnit;


        private const string ALARMS_OR_WARNINGS_SEVERITY_QUERY_FROM = "1";
        private const string ALARMS_OR_WARNINGS_SEVERITY_QUERY_TO = "2";
        private const string HISTORY_OF_STAY_TYPE_QUERY_FROM = "3";
        private const string HISTORY_OF_STAY_TYPE_QUERY_TO = "8";

        public int UnitId
        {
            get
            {
                return _currentUnit.Identity;
            }
        }


        private ArchivePatientsItemViewModel selectedItem;
        [RaisePropertyChanged]
        public ArchivePatientsItemViewModel SelectedItem
        {
            get
            {
                return selectedItem;
            }
            set
            {
                selectedItem = value;
                EventAggregator.GetEvent<SelectedItemChangedEvent<ArchivePatientsDto>>().Publish(selectedItem);
            }
        }

        [RaisePropertyChanged]
        public ServerSidePagedCollectionView<ArchivePatientsItemViewModel> Items { get; set; }


        private int _pageSize = 0;
        [RaisePropertyChanged]
        public int PageSize
        {
            get
            {
                if (_pageSize == 0)
                {
                    return Int32.MaxValue;
                }
                else if (_pageSize < 0)
                {
                    var actualHeight = System.Windows.Application.Current.Host.Content.ActualHeight - 330;
                    return (int)Math.Floor(actualHeight / 27);
                }
                return _pageSize;
            }
            set
            {
                _pageSize = value;
            }
        }

        [RaisePropertyChanged]
        public Visibility QuickSearchVisibility { get { return Visibility.Visible; } }

        [RaisePropertyChanged]
        public string QuickSearch { get; set; }

        public DelegateCommand<object> FilterContentsApply { get; set; }
        public DelegateCommand<object> FilterContentsClear { get; set; }
        public DelegateCommand<object> HeaderClicked { get; set; }
        public DelegateCommand<object> QuickSearchCommand { get; set; }

        public RelayCommand<KeyEventArgs> OnKeyUpOnFilter { get; set; }
        public RelayCommand<KeyEventArgs> OnKeyUpOnQuickSearch { get; set; }

        [RaisePropertyChanged]
        public IDataTable DataTable { get; set; }

        private FilterAfter FilterAfter { get; set; }

        public ArchivePatientsDetailsListViewModel(IUnityContainer container)
            : base(container)
        {
            DbBandStatus.Initialize(container);
            DataTable = new ArchivePatientsDataTable(container);

            FilterContentsApply = new DelegateCommand<object>(OnFilterContentsApply);
            FilterContentsClear = new DelegateCommand<object>(OnFilterContentsClear);
            OnKeyUpOnFilter = new RelayCommand<KeyEventArgs>(OnKeyUpOnFilterEvent);
            OnKeyUpOnQuickSearch = new RelayCommand<KeyEventArgs>(OnKeyUpOnQuickSearchEvent);
            HeaderClicked = new DelegateCommand<object>(OnHeaderClicked);
            QuickSearchCommand = new DelegateCommand<object>(OnQuickSearchCommand);

            EventAggregator.GetEvent<SaveSettingsEvent>().Subscribe(HandleSaveSettings);

            EventAggregator.GetEvent<GetOnlyArchiveAlarmsEvent>().Subscribe(OnOnlyArchiveAlarmsEventReceiver);
            EventAggregator.GetEvent<GetArchiveHistoryOfStayEvent>().Subscribe(OnArchiveHistoryOfStayReceiver);
            EventAggregator.GetEvent<GetAllArchiveEventsEvent>().Subscribe(OnAllArchiveEventsEventReceiver);
            EventAggregator.GetEvent<PingUserSessionEvent>().Subscribe(OnPingUserSession);
            PageSize = 50;
        }

        private bool IsUpToDate { get; set; }

        [RaisePropertyChanged]
        public string IconPath
        {
            get
            {
                if (IsUpToDate)
                    return "../../Images/apply_icon.png";
                else
                    return "../../Images/refresh-icon.png";
            }
        }

        public void OnPingUserSession(string arg)
        {
            IsUpToDate = false;
            NotifyPropertyChange("IconPath");
        }

        public void OnOnlyArchiveAlarmsEventReceiver(object obj)
        {
            FilterAfter = EventsDetails.FilterAfter.ONLY_ALARMS;
            Refresh(null);
        }

        public void OnAllArchiveEventsEventReceiver(object obj)
        {
            FilterAfter = EventsDetails.FilterAfter.ALL_EVENTS;
            Refresh(null);
        }

        public void OnArchiveHistoryOfStayReceiver(object obj)
        {
            FilterAfter = EventsDetails.FilterAfter.HISTORY_OF_STAY;
            Refresh(null);
        }

        public void OnQuickSearchCommand(object obj)
        {
            int newIndex = DataTable.SearchItem<ArchivePatientsItemViewModel>(Items, SelectedItem, QuickSearch);
            if (newIndex >= 0)
            {
                SelectedItem = Items[newIndex];
            }
        }

        public void HandleSaveSettings(bool obj)
        {
            DataTable.SaveColumns();
        }

        private void OnKeyUpOnFilterEvent(KeyEventArgs obj)
        {
            if (obj.Key == Key.Enter)
            {
                FilterContentsApply.Execute(null);
            }
        }

        private void OnKeyUpOnQuickSearchEvent(KeyEventArgs obj)
        {
            if (obj.Key == Key.Enter)
            {
                QuickSearchCommand.Execute(null);
            }
            if (obj.Key == Key.Escape)
            {
                QuickSearch = String.Empty;
            }
        }

        private void OnFilterContentsClear(object obj)
        {
            OnFilterContentsApply(true);
        }

        private void OnFilterContentsApply(object state)
        {
			bool clearFilter = (bool?)state ?? false;
            
            if(clearFilter)
            {
                DataTable.ClearFilter();
            }
            Refresh(_currentUnit);
        }

        private void SetSelectionAfterFiltering(string selectedIdentity)
        {
            if (selectedIdentity!=null && DataTableSettings.AfterFilteringKeepSelection)
            {
                SelectItem(selectedIdentity);
                if (SelectedItem == null && DataTableSettings.AfterFilteringSelectFirst)
                {
                    SelectItem(0);
                }
            }
            else if (selectedIdentity==null && DataTableSettings.AfterFilteringSelectFirst)
            {
                SelectItem(0);
            }
        }

        private void OnHeaderClicked(object param)
        {
            string selectedId = SelectedItemIdentity();

            string state = (string)param;

            DataTable.SortOrder = state;

            if (state == null || state == String.Empty)
                return;
            Refresh(_currentUnit);

            if (DataTableSettings.AfterSortingKeepSelection)
            {
                SelectItem(selectedId);
            }
            else
            {
                SelectItem(0);
            }
        }

        private string SelectedItemIdentity()
        {
            return SelectedItem == null ? null : SelectedItem.Item.Identity;
        }
        private void SelectItem(string identity)
        {
            SelectedItem = Items.Where(x => x.Item.Identity == identity).FirstOrDefault();
        }
        private void SelectItem(int position)
        {
            if (Items != null && Items.Count > 0)
            {
                if (Items.Count > position)
                {
                    SelectedItem = Items[0];
                }
                else
                {
                    SelectedItem = Items[Items.Count - 1];
                }
            }
        }

        public void Refresh(IUnit unit)
        {

            var newItems = new ServerSidePagedCollectionView<ArchivePatientsItemViewModel>(
                new ArchivePatientsRemotePagedDataSource(Container, DataTable, this,
                    FilterAfter == EventsDetails.FilterAfter.ONLY_ALARMS ? ALARMS_OR_WARNINGS_SEVERITY_QUERY_FROM : String.Empty,
                    FilterAfter == EventsDetails.FilterAfter.ONLY_ALARMS ? ALARMS_OR_WARNINGS_SEVERITY_QUERY_TO : String.Empty,
                    FilterAfter == EventsDetails.FilterAfter.HISTORY_OF_STAY ? HISTORY_OF_STAY_TYPE_QUERY_FROM : String.Empty,
                    FilterAfter == EventsDetails.FilterAfter.HISTORY_OF_STAY ? HISTORY_OF_STAY_TYPE_QUERY_TO : String.Empty),PageSize
                );
            
            newItems.PageChanged += newItems_PageChanged;
            
        }

        private void newItems_PageChanged(object sender, EventArgs e)
        {
            string selectedId = SelectedItemIdentity();
            Items = (ServerSidePagedCollectionView<ArchivePatientsItemViewModel>)sender;
            IsUpToDate = true;
            NotifyPropertyChange("IconPath");
            SetSelectionAfterFiltering(selectedId);
        }

        public Telerik.Windows.Controls.CommandBindingCollection GetCommandBindings()
        {
            var bindings = new Telerik.Windows.Controls.CommandBindingCollection();

            return bindings;
        }

		 public UnitsServiceClient GetProxy()
        {
            return ServiceFactory.GetService<UnitsServiceClient>(ServicesUri.UnitsService);    
        }

    }
}