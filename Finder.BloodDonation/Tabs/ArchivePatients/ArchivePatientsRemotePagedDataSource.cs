using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.ArchivePatients
{
    public class ArchivePatientsRemotePagedDataSource: IPagedDataSource<ArchivePatientsItemViewModel>
    {
        private const int PAGE_SIZE = 50;

        protected IUnityContainer _container;
        protected int _pageSize;
        protected IDataTable _dataTable;
        protected IUnitDataClient _dataClient;

        private string severityFrom;
        private string severityTo;
        private string typeFrom;
        private string typeTo;

        public ArchivePatientsRemotePagedDataSource(IUnityContainer container, IDataTable dataTable, IUnitDataClient dataClient, string severityFrom, string severityTo, string typeFrom, string typeTo)
        {
            _container = container;
            _pageSize = PAGE_SIZE;
            _dataTable = dataTable;
            _dataClient = dataClient;

            if (String.IsNullOrEmpty(severityFrom))
                this.severityFrom = String.Empty;
            else
                this.severityFrom = severityFrom;

            if (String.IsNullOrEmpty(severityTo))
                this.severityTo = String.Empty;
            else
                this.severityTo = severityTo;

            if (String.IsNullOrEmpty(typeFrom))
                this.typeFrom = String.Empty;
            else
                this.typeFrom = typeFrom;

            if (String.IsNullOrEmpty(typeTo))
                this.typeTo = String.Empty;
            else
                this.typeTo = typeTo;
        }

	    public void FetchData(int pageNumber, Action<PagedDataResponse<ArchivePatientsItemViewModel>> responseCallback)
        {
            var proxy = _dataClient.GetProxy();
            var list = new List<ArchivePatientsItemViewModel>();
            
            proxy.GetArchivePatientsFilteredCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<ArchivePatientsItemViewModel>();
                    vm.Item = item;
                    vm.DataTable = _dataTable;
                    list.Add(vm);
                }
                _container.Resolve<ArchivePatientsItemViewModel>().SelectionChanged(-1);
                Dictionary<string, string> filterDictionary = (Dictionary<string, string>)_dataTable.GetFilterDictionary();

                if (!String.IsNullOrEmpty(severityFrom))
                {
                    filterDictionary["SeverityFrom"] = severityFrom;
                }
                if (!String.IsNullOrEmpty(severityTo))
                {
                    filterDictionary["SeverityTo"] = severityTo;
                }

                if (!String.IsNullOrEmpty(typeFrom))
                {
                    filterDictionary["TypeFrom"] = typeFrom;
                }

                if (!String.IsNullOrEmpty(typeTo))
                {
                    filterDictionary["TypeTo"] = typeTo;
                }

                proxy.GetArchivePatientsFilteredTotalCountAsync(filterDictionary);
            };

            proxy.GetArchivePatientsFilteredTotalCountCompleted += (s, e) =>
            {
                responseCallback(new PagedDataResponse<ArchivePatientsItemViewModel>()
                {
                    Items = list,
                    TotalItemCount = e.Result
                });
            };

            Dictionary<string, string> fdict = (Dictionary<string, string>)_dataTable.GetFilterDictionary();
            if (!String.IsNullOrEmpty(severityFrom))
            {
                fdict["SeverityFrom"] = severityFrom;
            }
            if (!String.IsNullOrEmpty(severityTo))
            {
                fdict["SeverityTo"] = severityTo;
            }

            if (!String.IsNullOrEmpty(typeFrom))
            {
                fdict["TypeFrom"] = typeFrom;
            }

            if (!String.IsNullOrEmpty(typeTo))
            {
                fdict["TypeTo"] = typeTo;
            }

            proxy.GetArchivePatientsFilteredAsync(fdict, pageNumber,_pageSize);
        }
    }
}
