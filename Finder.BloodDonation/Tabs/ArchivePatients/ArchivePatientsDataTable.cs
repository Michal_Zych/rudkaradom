using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataTable;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Authentication;

namespace Finder.BloodDonation.Tabs.ArchivePatients
{
    public class ArchivePatientsDataTable : BaseDataTable
    {

        public ArchivePatientsDataTable(IUnityContainer container)
            : base(container)
        {

        }

        public override ObservableCollection<IDataTableColumn> GetColumns()
        {
            bool isHiddenForUser = !BloodyUser.Current.IsInRole(PermissionNames.HyperAdmin);

            return new ObservableCollection<IDataTableColumn>()
             .Add(50)
             .Add(100, "LastName", "Nazwisko", FilterDataType.String, TextAlignment.Left) //v
             .Add(100, "Name", "Imi�", FilterDataType.String, TextAlignment.Left) //v
             .Add(100, "DisplayName", "Nazwa", FilterDataType.String, TextAlignment.Left, true, false) //v
             .Add(100, "Pesel", "PESEL", FilterDataType.String, TextAlignment.Left) //v
             .Add(100, "EventId", "ID zdarzenia", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "DateUtc", "Data zdarzenia", FilterDataType.DateTime, TextAlignment.Left) //v
             .Add(100, "TypeDescription", "Zdarzenie", FilterDataType.String, TextAlignment.Left) //v
             .Add(100, "SeverityDescription", "Waga zdarzenia", FilterDataType.String, TextAlignment.Left) //v
             .Add(100, "UnitName", "Strefa", FilterDataType.String, TextAlignment.Left) //v
             .Add(100, "UnitLocation", "Lokalizacja strefy", FilterDataType.String, TextAlignment.Left) //v
             .Add(100, "ObjectTypeDescription", "Typ obiektu", FilterDataType.String, TextAlignment.Left) //v
             .Add(100, "ObjectUnitLocation", "Lokalizacja oddzia�u", FilterDataType.String, TextAlignment.Left, false, false) //v
             .Add(100, "StoreDateUtc", "Czas zapisu", FilterDataType.DateTime, TextAlignment.Left, isHiddenForUser)
             .Add(100, "EndDateUtc", "Zamkni�cie alarmu", FilterDataType.DateTime, TextAlignment.Left)
             .Add(100, "ClosingUserName", "Imi� zamykaj�cego", FilterDataType.String, TextAlignment.Left) //v
             .Add(100, "ClosingUserLastName", "Nazwisko zamykaj�cego", FilterDataType.String, TextAlignment.Left) //v
             .Add(100, "ClosingComment", "Komentarz", FilterDataType.String, TextAlignment.Left) //v
             .Add(100, "SenderName", "Imi� wysy�aj�cego", FilterDataType.String, TextAlignment.Left, isHiddenForUser)
             .Add(100, "SenderLastName", "Nazwisko wysy�aj�cego", FilterDataType.String, TextAlignment.Left, isHiddenForUser)
             .Add(100, "Message", "Komunikat", FilterDataType.String, TextAlignment.Left, isHiddenForUser)
             .Add(100, "Reason", "Przyczyna", FilterDataType.String, TextAlignment.Left, isHiddenForUser);
        }

        public override ISortedFilter GetFilter(IUnityContainer container)
        {
            return new ArchivePatientsFilter(container);
        }

		public override bool StartWithDefaultColumns
        {
            get
            {
                return false;
            }
        }
    }
}
