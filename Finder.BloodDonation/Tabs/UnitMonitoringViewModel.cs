﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;

namespace Finder.BloodDonation.Tabs
{
	public class UnitMonitoringViewModel : ViewModelBase, ITabViewModel
	{

		public UnitMonitoringViewModel(IUnityContainer container)
			: base(container)
		{
			this.ViewAttached += new EventHandler(UnitsListViewModel_ViewAttached);
		}

		void UnitsListViewModel_ViewAttached(object sender, EventArgs e)
		{
			LoadingCompleted(this, EventArgs.Empty);
		}


		public void Refresh(IUnit unit)
		{
			
		}

	

		public event EventHandler LoadingCompleted = delegate { };




		#region ITabViewModel Members


		public ViewModelState State
		{
			get { throw new NotImplementedException(); }
		}

		#endregion
	}
}
