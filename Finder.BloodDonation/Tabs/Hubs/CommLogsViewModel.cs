﻿using System;
using System.Net;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Common.Events;
using System.Collections.ObjectModel;
using System.Windows.Media.Animation;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.HubsProxy;

namespace Finder.BloodDonation.Tabs.Hubs
{
    [OnCompleted]
    [UIException]
    public class CommLogsViewModel : ViewModelBase, ITabViewModel
    {
        [RaisePropertyChanged]
        public IUnit SelectedUnit { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<CommunicationLogDto> Logs { get; set; }

        [RaisePropertyChanged]
        public string SearchFilter { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterDateStart { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterTimeStart { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterTimeEnd { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterDateEnd { get; set; }

        public DelegateCommand<object> SearchLogs { get; set; }

        private HubsServiceClient LoadService
        {
            get
            {
                HubsServiceClient proxy = ServiceFactory.GetService<HubsServiceClient>(ServicesUri.HubsService);
                proxy.GetCommunicationLogsCompleted += new EventHandler<GetCommunicationLogsCompletedEventArgs>(proxy_GetCommunicationLogsCompleted);
                return proxy;
            }
        }

        void proxy_GetCommunicationLogsCompleted(object sender, GetCommunicationLogsCompletedEventArgs e)
        {
            //rxxr Logs = new ObservableCollection<CommunicationLogDto>(e.Result);
        }

        public CommLogsViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            InitializeDates();

            SearchLogs = new DelegateCommand<object>(SearchLogs_Command);
        }

        public void SearchLogs_Command(object o)
        {
            if (SelectedUnit != null)
            {
                InitializeDates();
                LoadService.GetCommunicationLogsAsync(SelectedUnit.Identity,
                    new DateTime(
                        SearchFilterDateStart.Value.Year,
                        SearchFilterDateStart.Value.Month,
                        SearchFilterDateStart.Value.Day,
                        SearchFilterTimeStart.Value.Hour,
                        SearchFilterTimeStart.Value.Minute,
                        SearchFilterTimeStart.Value.Second
                        ),
                    new DateTime(
                        SearchFilterDateEnd.Value.Year,
                        SearchFilterDateEnd.Value.Month,
                        SearchFilterDateEnd.Value.Day,
                        SearchFilterTimeEnd.Value.Hour,
                        SearchFilterTimeEnd.Value.Minute,
                        SearchFilterTimeEnd.Value.Second
                        ),
                    SearchFilter
                    );
            }
        }

        private void InitializeDates()
        {
            if (!SearchFilterDateStart.HasValue || !SearchFilterDateEnd.HasValue)
            {
                SearchFilterDateStart = DateTime.Now.AddDays(-1);
                SearchFilterTimeStart = SearchFilterDateStart;
                SearchFilterDateEnd = DateTime.Now.AddDays(1);
                SearchFilterTimeEnd = SearchFilterDateEnd;
            }
        }

        public void Refresh(IUnit unit)
        {
            SelectedUnit = unit;
            Logs = null;
        }

        public event EventHandler LoadingCompleted;

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }


        public string Title
        {
            get { return "Logs"; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }
}
