﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.HubsProxy;
using FinderFX.SL.Core.Communication;

namespace Finder.BloodDonation.Tabs.Hubs
{
	public class HubListViewModel : ViewModelBase, ITabViewModel
	{
		public HubListViewModel(IUnityContainer unityContainer)
			: base(unityContainer)
		{

		}

		#region ITabViewModel Members

		public void Refresh(IUnit unit)
		{
			HubsServiceClient proxy = ServiceFactory.GetService<HubsServiceClient>();
			proxy.GetHubsCompleted += new EventHandler<GetHubsCompletedEventArgs>(proxy_GetHubsCompleted);
			proxy.GetHubsAsync(unit.Identity);
		}

		void proxy_GetHubsCompleted(object sender, GetHubsCompletedEventArgs e)
		{
			
		}

		public event EventHandler LoadingCompleted;
		private ViewModelState _state = ViewModelState.Loaded;
		public ViewModelState State
		{
			get { return _state; }
		}

		#endregion
	}
}
