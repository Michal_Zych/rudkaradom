﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Model.Dto;
using FinderFX.SL.Core.Extensions;
using Finder.BloodDonation.HubsProxy;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common.Events;
using System.Collections.Generic;

namespace Finder.BloodDonation.Tabs.Hubs
{
    public class HubLevel
    {
        public int LogLevel { get; set; }
        public string Display { get; set; }
    }

    public class HubDetailsViewModel : ViewModelBase, ITabViewModel
    {
        public static IList<HubLevel> AllHubLevels { get; set; }

        [RaisePropertyChanged]
        public IList<HubLevel> HubLevels { get; set; }


        public HubDetailsViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            HubLevels = new List<HubLevel>();
            HubLevels.Add(new HubLevel() { LogLevel = 0, Display = "O:ALL" });
            HubLevels.Add(new HubLevel() { LogLevel = 1, Display = "1:DEBUG" });
            HubLevels.Add(new HubLevel() { LogLevel = 2, Display = "2:INFO" });
            HubLevels.Add(new HubLevel() { LogLevel = 3, Display = "3:WARN" });
            HubLevels.Add(new HubLevel() { LogLevel = 4, Display = "4:ERROR" });
            HubLevels.Add(new HubLevel() { LogLevel = 5, Display = "5:OFF" });
            AllHubLevels = HubLevels;
        }

        #region ITabViewModel Members

        public void Refresh(IUnit unit)
        {

        }






        public event EventHandler LoadingCompleted;
        private ViewModelState _state = ViewModelState.Loaded;
        public ViewModelState State
        {
            get { return _state; }
        }

        #endregion


        public string Title
        {
            get { return "hubs"; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }
}
