﻿using System;
using System.Linq;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Model.Dto;

namespace Finder.BloodDonation.Tabs.Hubs
{
    public class HubLevelConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int dID = (int)value;

            return HubDetailsViewModel.AllHubLevels.FirstOrDefault(x => x.LogLevel == dID);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            HubLevel d = value as HubLevel;
            if (d != null)
            {
                return d.LogLevel;
            }

            return null;
        }
    }
}
