﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.HubsProxy;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Tabs.UnitsList;

namespace Finder.BloodDonation.Tabs.Hubs
{
	public class HubViewModel : ViewModelBase, ITabViewModel
	{
		[RaisePropertyChanged]
		public object UnitList { get; set; }

		[RaisePropertyChanged]
		public object HubDetails { get; set; }


		public HubViewModel(IUnityContainer unityContainer)
			: base(unityContainer)
		{
			this.ViewAttached += new EventHandler(HubViewModel_ViewAttached);
		}

		void HubViewModel_ViewAttached(object sender, EventArgs e)
		{
			//UnitList = this.Container.Resolve<ViewFactory>().CreateView<UnitsListView>();
			HubDetails = this.Container.Resolve<ViewFactory>().CreateView<HubDetailsView>();
			

			InitilizeViewModel(UnitList);
			InitilizeViewModel(HubDetails);
		}

		private void InitilizeViewModel(object view)
		{
			FrameworkElement element = view as FrameworkElement;
			if (element == null)
				return;

			ITabViewModel vm = element.DataContext as ITabViewModel;
			if (vm == null)
				return;

			if (vm.State != ViewModelState.Loaded)
			{
				vm.LoadingCompleted += new EventHandler(vm_LoadingCompleted);
			}
			else
			{
				LoadingCompleted(this, EventArgs.Empty);
			}
		}

		void vm_LoadingCompleted(object sender, EventArgs e)
		{
			LoadingCompleted(this, EventArgs.Empty);
		}


		#region ITabViewModel Members

		public void Refresh(IUnit unit)
		{
			RefreshViewModels(this.UnitList, unit);
			RefreshViewModels(this.HubDetails, unit);
		}

		private void RefreshViewModels(object view, IUnit unit)
		{
			FrameworkElement element = view as FrameworkElement;
			if (element == null)
				return;

			ITabViewModel vm = element.DataContext as ITabViewModel;
			if (vm == null)
				return;

			vm.Refresh(unit);
		}

		

		public event EventHandler LoadingCompleted = delegate { };
		private ViewModelState _state = ViewModelState.Loaded;
		public ViewModelState State
		{
			get { return _state; }
		}

		#endregion


        public string Title
        {
            get { return "hubs"; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }
}
