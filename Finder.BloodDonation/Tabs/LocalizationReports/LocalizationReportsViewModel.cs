﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Events;
using System.Collections.Generic;
using System.Collections;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Enums;
using Finder.BloodDonation.Tabs.Reports.BandsTracking;

namespace Finder.BloodDonation.Tabs.LocalizationReports
{
    [OnCompleted]
    [UIException]
    public class LocalizationReportsViewModel: ViewModelBase, ITabViewModel
    {
        public ReportInfo ReportInfo { get; set; }

        public event EventHandler LoadingCompleted = delegate { };
        public IUnit SelectedUnit = null;

        private ITabViewModel _details;
        public ITabViewModel Details
        {
            get
            {
                if(_details == null)
                {
                    _details = Container.Resolve<BandsTrackingReportsListViewModel>();
                }
                return _details;
            }
            set
            {
                _details = value;
            }
        }

        public LocalizationReportsViewModel(IUnityContainer container)
            : base(container)
        {
            this.ViewAttached += new EventHandler(ViewModel_ViewAttached);
            EventAggregator.GetEvent<RequestToRepeatMessageEvent>().Subscribe(OnRepeatSend);
        }

        public void OnRepeatSend(object obj)
        {
            EventAggregator.GetEvent<SetBandsTrackingReportViewTypeEvent>().Publish(ReportInfo);
        }

        private void ViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public void Refresh(IUnit unit)
        {
            SelectedUnit = unit;
            ReportInfo = new ReportInfo(){ ReportTypes = Enums.ReportTypes.Unit, ObjectId = SelectedUnit.Identity };

            EventAggregator.GetEvent<SetBandsTrackingReportViewTypeEvent>().Publish(ReportInfo);
        }

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        public string Title
        {
            get { return ""; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }
}
