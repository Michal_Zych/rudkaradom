﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Tabs.Reports.BandsTracking;
using Telerik.Windows.Controls;

namespace Finder.BloodDonation.Tabs.LocalizationReports
{
    [ViewModel(typeof(LocalizationReportsViewModel))]
    public partial class LocalizationReportsView : UserControl
    {
        public LocalizationReportsView()
        {
            InitializeComponent();
        }

        public void DataContextChangedm(BandsTrackingRaportsListView sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                var vm = e.NewValue as BandsTrackingReportsListViewModel;

                CommandManager.SetCommandBindings(this, vm.GetCommandBindings());
            }
        }
    }
}
