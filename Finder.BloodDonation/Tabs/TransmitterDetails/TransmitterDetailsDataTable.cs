using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataTable;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Authentication;

namespace Finder.BloodDonation.Tabs.TransmitterDetails
{
    public class TransmitterDetailsDataTable : BaseDataTable
    {

        public TransmitterDetailsDataTable(IUnityContainer container)
            : base(container)
        {

        }

        public override ObservableCollection<IDataTableColumn> GetColumns()
        {
		
			bool isHiddenForUser = !BloodyUser.Current.IsInRole(PermissionNames.HyperAdmin);
		
            return new ObservableCollection<IDataTableColumn>()
             .Add(50)
             .Add(100, "Id", "ID", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "Mac", "MAC", FilterDataType.Long, TextAlignment.Left, isHiddenForUser)
             .Add(100, "MacString", "MAC", FilterDataType.String, TextAlignment.Left) // v
             .Add(100, "ClientId", "ID Klienta", FilterDataType.Int, TextAlignment.Left, true, false)
             .Add(100, "InstallationUnitId", "ID Strefy", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "InstallationUnitName", "Strefa", FilterDataType.String, TextAlignment.Left) // v
             .Add(100, "Name", "Nazwa", FilterDataType.String, TextAlignment.Left) //v
             .Add(100, "BarCode", "Kod kreskowy", FilterDataType.String, TextAlignment.Left) // v
             .Add(100, "TypeId", "ID typu transmitera", FilterDataType.Int, TextAlignment.Left, isHiddenForUser) 
             .Add(100, "ReportIntervalSec", "Częstotliwość raportowania (s)", FilterDataType.Int, TextAlignment.Left) // v
             .Add(100, "Sensitivity", "Czułość", FilterDataType.Int, TextAlignment.Left) // v
             .Add(100, "AvgCalcTimeS", "Czas liczenia średniej (s)", FilterDataType.Int, TextAlignment.Left) // v
             .Add(100, "Msisdn", "Numer telefonu", FilterDataType.String, TextAlignment.Left) // v
             .Add(100, "IsBandUpdater", "Aktualizator", FilterDataType.Bool, TextAlignment.Left)
             .Add(100, "Description", "Opis", FilterDataType.String, TextAlignment.Left) // v
             .Add(100, "RssiTreshold", "Próg siły sygnału (dBm)", FilterDataType.Short, TextAlignment.Left) //v
             .Add(100, "GroupId", "ID grupy stref", FilterDataType.Byte, TextAlignment.Left, true, false) // v
             .Add(100, "GroupName", "Nazwa grupy", FilterDataType.String, TextAlignment.Left, true, false) // v
             .Add(100, "IpAddress", "Adres IP", FilterDataType.String, TextAlignment.Left) // v
             .Add(100, "IsConnected", "Czy połączony", FilterDataType.Bool, TextAlignment.Left) //v
             .Add(100, "AlarmOngoing", "Stan alarmowy", FilterDataType.Bool, TextAlignment.Left); // v
        }

        public override ISortedFilter GetFilter(IUnityContainer container)
        {
            return new TransmitterDetailsFilter(container);
        }

		public override bool StartWithDefaultColumns
        {
            get
            {
                return false;
            }
        }
    }
}
