using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.TransmitterDetails
{
    public class TransmitterDetailsFilter : SortedFilter
    {
       [RaisePropertyChanged]
public string Id { get; set; }

[RaisePropertyChanged]
public string Mac { get; set; }

[RaisePropertyChanged]
public string MacString { get; set; }

[RaisePropertyChanged]
public string ClientId { get; set; }

[RaisePropertyChanged]
public string InstallationUnitId { get; set; }

[RaisePropertyChanged]
public string InstallationUnitName { get; set; }

[RaisePropertyChanged]
public string Name { get; set; }

[RaisePropertyChanged]
public string BarCode { get; set; }

[RaisePropertyChanged]
public string TypeId { get; set; }

[RaisePropertyChanged]
public string ReportIntervalSec { get; set; }

[RaisePropertyChanged]
public string Sensitivity { get; set; }

[RaisePropertyChanged]
public string AvgCalcTimeS { get; set; }

[RaisePropertyChanged]
public string Msisdn { get; set; }

[RaisePropertyChanged]
public bool? IsBandUpdater { get; set; }

[RaisePropertyChanged]
public string Description { get; set; }

[RaisePropertyChanged]
public string IpAddress { get; set; }

[RaisePropertyChanged]
public string RssiTreshold { get; set; }

[RaisePropertyChanged]
public string GroupId { get; set; }

[RaisePropertyChanged]
public string GroupName { get; set; }

[RaisePropertyChanged]
public bool? IsConnected { get; set; }

[RaisePropertyChanged]
public bool? AlarmOngoing { get; set; }


        public TransmitterDetailsFilter(IUnityContainer container)
            : base(container)
        {

        }
    }
}
