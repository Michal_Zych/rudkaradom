using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Events;
using System.Collections.Generic;
using System.Collections;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Dialogs.Patients;
using Finder.BloodDonation.Dialogs.Helpers;
using Microsoft.Practices.Composite.Presentation.Events;
using System.Diagnostics;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.Dialogs.Bands;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.UsersService;
using Finder.BloodDonation.Dialogs.Transmiters;
using Finder.BloodDonation.Dialogs.Transmitters;

namespace Finder.BloodDonation.Tabs.TransmitterDetails
{
    [OnCompleted]
    [UIException]
    public class TransmitterDetailsViewModel : ViewModelBase, ITabViewModel
    {
        private readonly IWindowsManager _windowsManager;

        public event EventHandler LoadingCompleted = delegate { };
        private IUnit SelectedUnit = null;
        private IUnit _currentUnit;

        private TransmitterDetailsDto selectedItem;

        private UsersServiceClient Proxy { get; set; }

        public RelayCommand CreateTransmitterCommand { get; set; }
        public RelayCommand EditTransmitterCommand { get; set; }
        public RelayCommand RemoveTransmitterCommand { get; set; }
        public RelayCommand OpenTransmitterEvents { get; set; }

        private UnitsServiceClient UnitProxy { get; set; }

        private IUnitFiltered _details;
        [RaisePropertyChanged]
        public IUnitFiltered Details
        {
            get
            {
                if (_details == null)
                {
                    _details = Container.Resolve<TransmitterDetailsDetailsListViewModel>();
                }
                return _details;
            }
            set
            {
                _details = value;
            }
        }

        public TransmitterDetailsViewModel(IUnityContainer container)
            : base(container)
        {
            this.ViewAttached += new EventHandler(ViewModel_ViewAttached);
            EventAggregator.GetEvent<SelectedItemChangedEvent<TransmitterDetailsDto>>().Subscribe(OnSelectedItemChangedEvent);
            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(OnCloseWindow);
            CreateTransmitterCommand = new RelayCommand(OnCreateTransmitterCommand);
            EditTransmitterCommand = new RelayCommand(OnEditTransmitterCommand);
            RemoveTransmitterCommand = new RelayCommand(OnRemoveTransmitterCommand);
            OpenTransmitterEvents = new RelayCommand(OnOpenTransmitterEvents);

            var viewFactory = container.Resolve<ViewFactory>();
            _windowsManager = this.Container.Resolve<IWindowsManager>();
            _windowsManager.RegisterWindowIfNotRegister(WindowsKeys.EditTransmitter, "", viewFactory.CreateView<EditTransmiter>(), new Size(600, 580), false, true);
            _windowsManager.RegisterWindow(WindowsKeys.TransmitterEventsDialog,"", viewFactory.CreateView<TransmitterEventsDialog>(), new Size(1270, 750), false, true);

            Proxy = ServiceFactory.GetService<UsersServiceClient>();
            UnitProxy = ServiceFactory.GetService<UnitsServiceClient>();

            UnitProxy.TransmitterRemoveFromUnitCompleted += UnitProxy_TransmitterRemoveFromUnitCompleted;
            UnitProxy.TransmitterDeleteCompleted += UnitProxy_TransmitterDeleteCompleted;
        }

        public void OnOpenTransmitterEvents()
        {
            if(selectedItem != null)
            {
                _windowsManager.ShowWindow(WindowsKeys.TransmitterEventsDialog, new TransmitterEventsData() { SelectedTransmitterId = selectedItem.Id });
            }
            else
            {
                MsgBox.Warning("Nie wybrano transmitera!");
            }
        }

        public void UnitProxy_TransmitterDeleteCompleted(object sender, TransmitterDeleteCompletedEventArgs e)
        {
            int t;

            if(int.TryParse(e.Result, out t))
            {
                MsgBox.Succes("Transmitter usuni�ty pomy�lnie!");
            }
            else
            {
                MsgBox.Warning(e.Result);
            }
        }

        public void UnitProxy_TransmitterRemoveFromUnitCompleted(object sender, TransmitterRemoveFromUnitCompletedEventArgs e)
        {
            int s;

            if(int.TryParse(e.Result, out s))
            {
                MsgBox.Confirm("Czy na pewno chcesz usun�� transmitter o\n adresie MAC: " + selectedItem.MacString + "?", OnDeleteTransmitter);
            }
            else
            {
                MsgBox.Warning(e.Result);
            }
        }

        private void OnDeleteTransmitter(object sender, Telerik.Windows.Controls.WindowClosedEventArgs e)
        {
            if (e.DialogResult == true)
            {
                UnitProxy.TransmitterDeleteAsync(selectedItem.Id, String.Empty);
                Refresh(_currentUnit);
            }
            else
            {
                MsgBox.Warning("Operacja usuwania zosta�a anulowana");
            }
        }

        private void OnRemoveTransmitterCommand()
        {
            if(selectedItem != null)
            {
                if(selectedItem.InstallationUnitId != null)
                {
                    MsgBox.Confirm("Czy na pewno chcesz odinstalowa� transmitter\n z sali " + selectedItem.InstallationUnitName + "( ID: " + selectedItem.InstallationUnitId + " )", OnRemoveTransmitter);
                }
            }
            else
            {
                MsgBox.Warning("Nie wybrano transmittera do usuni�cia.");
            }
        }

        public void OnRemoveTransmitter(object sender, Telerik.Windows.Controls.WindowClosedEventArgs e)
        {
            if (e.DialogResult == true)
            {
                UnitProxy.TransmitterRemoveFromUnitAsync((int)selectedItem.InstallationUnitId, String.Empty);
            }
            else
            {
                MsgBox.Warning("Operacja odinstalowania zosta�a anulowana.");
                Refresh(_currentUnit);
            }
        }

        public void OnCloseWindow(string obj)
        {
            Refresh(_currentUnit);
        }

        private void OnEditTransmitterCommand()
        {
            if(selectedItem == null)
            {
                MsgBox.Warning("Nie wybrano transmitera do edycji!");
                return;
            }

            TransmitterEditData ted = new TransmitterEditData()
            {
                Transmitter = new TransmitterDTO()
                {
                    Id = selectedItem.Id,
                    AssignUnitId = selectedItem.InstallationUnitId,
                    AvgCalcTimeS = selectedItem.AvgCalcTimeS,
                    BarCode = selectedItem.BarCode,
                    Description = selectedItem.Description,
                    IsBandUpdater = selectedItem.IsBandUpdater,
                    Mac = selectedItem.Mac,
                    MacString = selectedItem.MacString,
                    MSISDN = selectedItem.Msisdn,
                    Name = selectedItem.Name,
                    ReportIntervalSec = selectedItem.ReportIntervalSec,
                    Sensitivity = selectedItem.Sensitivity,
                    TransmitterInstallationUnit = selectedItem.InstallationUnitName,
                    Type = selectedItem.TypeId,
                    Transitions = selectedItem.Transitions,
                    IpAddress = selectedItem.IpAddress,
                    RssiTreshold = selectedItem.RssiTreshold,
                    Group = selectedItem.GroupId
                }
            };

            _windowsManager.ShowWindow(WindowsKeys.EditTransmitter, ted);
        }

        private void OnCreateTransmitterCommand()
        {
            TransmitterEditData ted = new TransmitterEditData()
            {
                Transmitter = new TransmitterDTO()
            };

            _windowsManager.ShowWindow(WindowsKeys.EditTransmitter, ted);
        }

        public void OnSelectedItemChangedEvent(IItemViewModel<TransmitterDetailsDto> obj)
        {
            if(obj == null)
            {
                selectedItem = null;
            }
            else
            {
                selectedItem = obj.Item;
            }
            
            SelectedItemChanged();
        }

        private void SelectedItemChanged()
        {

        }
        
        private void ViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public void Refresh(IUnit unit)
        {
            SelectedUnit = unit;
            _currentUnit = unit;

            Details.Refresh(unit);
		}
        
        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        public string Title
        {
            get { return ""; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }

        private void CloseWindowEventHandling(string key)
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(CloseWindowEventHandling);
        }
    }
}
