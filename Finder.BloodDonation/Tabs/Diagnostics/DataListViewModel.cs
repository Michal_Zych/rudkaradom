﻿using System;
using System.Net;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Common.Events;
using System.Collections.ObjectModel;
using System.Windows.Media.Animation;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.Helpers;
using System.Windows.Controls;
using System.Windows;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;

namespace Finder.BloodDonation.Tabs.Diagnostics
{
    [OnCompleted]
    [UIException]
    public class DataListViewModel : ViewModelBase, ITabViewModel
    {
        [RaisePropertyChanged]
        public IUnit SelectedUnit { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<DataDto> Data { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterDateStart { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterTimeStart { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterTimeEnd { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterDateEnd { get; set; }

        public DelegateCommand<object> SearchData { get; set; }

        public DelegateCommand<object> ExportData { get; set; }



        //DataGrid widoczne w Modelu jako DependencyProperty
        public static readonly DependencyProperty DataGridProperty =
            DependencyProperty.RegisterAttached("DataGrid", typeof(DataGrid), typeof(DataListViewModel),
            new PropertyMetadata(OnDataGridChanged));

        public static void SetDataGrid(DependencyObject element, DataGrid value)
        {
            element.SetValue(DataGridProperty, value);
        }

        public static DataGrid GetDataGrid(DependencyObject element)
        {
            return (DataGrid)element.GetValue(DataGridProperty);
        }

        private static DataGrid dataGrid = null;

        public static void OnDataGridChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            dataGrid = obj as DataGrid;
        }





        private AlarmsServiceClient LoadService
        {
            get
            {
                AlarmsServiceClient proxy = ServiceFactory.GetService<AlarmsServiceClient>(ServicesUri.AlarmsService);
                proxy.GetSensorDataCompleted += new EventHandler<GetSensorDataCompletedEventArgs>(proxy_GetSensorDataCompleted);
                return proxy;
            }
        }

        void proxy_GetSensorDataCompleted(object sender, GetSensorDataCompletedEventArgs e)
        {
            Data = new ObservableCollection<DataDto>(e.Result);
        }

        public DataListViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            InitializeDates();

            SearchData = new DelegateCommand<object>(SearchData_Command);
            ExportData = new DelegateCommand<object>(ExportData_Command);
        }

        public void ExportData_Command(object o)
        {
            dataGrid.Export();
        }

        public void SearchData_Command(object o)
        {
            if (SelectedUnit != null)
            {
                InitializeDates();
                DateTime startDate = new DateTime(
                            SearchFilterDateStart.Value.Year,
                            SearchFilterDateStart.Value.Month,
                            SearchFilterDateStart.Value.Day,
                            SearchFilterTimeStart.Value.Hour,
                            SearchFilterTimeStart.Value.Minute,
                            SearchFilterTimeStart.Value.Second),
                         endDate = new DateTime(
                            SearchFilterDateEnd.Value.Year,
                            SearchFilterDateEnd.Value.Month,
                            SearchFilterDateEnd.Value.Day,
                            SearchFilterTimeEnd.Value.Hour,
                            SearchFilterTimeEnd.Value.Minute,
                            SearchFilterTimeEnd.Value.Second);

                if (MsgBox.DatesInRange(Global.Settings.Current.GetInt(Global.ConfigurationKeys.MaxDaysRawData), startDate, endDate))
                {
                    LoadService.GetSensorDataAsync(SelectedUnit.Identity, startDate, endDate);
                }
            }
        }

        private void InitializeDates()
        {
            if (!SearchFilterDateStart.HasValue || !SearchFilterDateEnd.HasValue)
            {
                SearchFilterDateStart = DateTime.Now.AddDays(-Global.Settings.Current.GetInt(Global.ConfigurationKeys.SearchRawDataDaysBefore));
                SearchFilterTimeStart = SearchFilterDateStart;
                SearchFilterDateEnd = DateTime.Now.AddDays(Global.Settings.Current.GetInt(Global.ConfigurationKeys.SearchRawDataDaysAfter));
                SearchFilterTimeEnd = SearchFilterDateEnd;
            }
        }

        public void Refresh(IUnit unit)
        {
            SelectedUnit = unit;
            if (Data != null)
            {
                Data.Clear();
            }
            Data = null;
        }

        public event EventHandler LoadingCompleted;

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }


        public string Title
        {
            get { return "Dane " + SelectedUnit.UnitDescription; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }
}
