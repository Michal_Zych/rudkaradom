﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Threading;

namespace Finder.BloodDonation.Tabs.RefreshTools
{
    public class RefreshManager : IRefreshManager
    {
        private Timer _timer = null;
        private ManualResetEvent waitHandle = new ManualResetEvent(false);
        private const int Interval = 1000 * 60;
        private TimerCallback _timerCallback = null;

        /// <summary>
        /// Stop and dispose Timer
        /// </summary>
        public void Stop()
        {
            this.DisposeTimer();
        }

        public void Start(Action<object> action)
        {
            this.DisposeTimer(); //stop existing action
            this._timer = new Timer(new TimerCallback((a) =>
            {
                if (Deployment.Current.Dispatcher.CheckAccess())
                {
                    action(null);
                }
                else
                {
                    Deployment.Current.Dispatcher.BeginInvoke(() =>
                    {
                        action(null);
                    });
                }

            }), null, 0, Interval); //create new
        }

        private void DisposeTimer()
        {
            if (_timer != null)
            {
                _timer.Dispose(waitHandle);
                waitHandle.WaitOne();
            }
        }
    }
}
