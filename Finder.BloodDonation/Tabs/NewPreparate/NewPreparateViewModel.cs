﻿using System;
using System.Collections.Generic;
using System.Windows.Data;
using DanielVaughan.Logging;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Model;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.RfidProxy;
using Finder.BloodDonation.Tabs.Contents;
using FinderFX.SL.Core.Authentication;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using FinderFX.SL.Core.Extensions;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;
using Telerik.Data;
using Telerik.Windows.Controls;

namespace Finder.BloodDonation.Tabs.NewPreparate
{
    [OnCompleted]
    [UIException]
    public class NewPreparateViewModel : FinderFX.SL.Core.MVVM.ViewModelBase, ITabViewModel, IRfidDataClient
    {
        private static ILog Logger = LogManager.GetLog(typeof(NewPreparateView));

        private IUnit _currentUnit;
        private DataTable _internalDataTable;
        private List<PrepDto> _newPreparates;
        private List<PrepDto> _duplicates;
        private bool _acceptingInProgress;

        [RaisePropertyChanged]
        public IList<PrepTypeDefinitionDto> AvailablePrepTypes { get; set; }

        [RaisePropertyChanged]
        public string TestTagData { get; set; }

        public DelegateCommand<object> TypeComboSelectionChanged { get; set; }

        public event EventHandler LoadingCompleted;

        public DelegateCommand<object> EnableRfidTransmission { get; set; }
        public DelegateCommand<object> DisableRfidTransmission { get; set; }
        public DelegateCommand<object> AcceptNewPreparates { get; set; }
        public DelegateCommand<object> ClearTagList { get; set; }

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        public NewPreparateViewModel(IUnityContainer container)
            : base(container)
        {
            TypeComboSelectionChanged = new DelegateCommand<object>(OnTypeComboSelectionChanged2);

            EventAggregator.GetEvent<TagDataReceivedEvent>().Subscribe(HandleTagDataReceivedEvent);

            EnableRfidTransmission = new DelegateCommand<object>(OnEnableRfidTransmission);
            DisableRfidTransmission = new DelegateCommand<object>(OnDisableRfidTransmission);
            AcceptNewPreparates = new DelegateCommand<object>(OnAcceptNewPreparates);
            ClearTagList = new DelegateCommand<object>(OnClearTagList);

            _newPreparates = new List<PrepDto>();
            _duplicates = new List<PrepDto>();
            _acceptingInProgress = false;
        }

        public void Refresh(IUnit unit)
        {
            _currentUnit = unit;

            var proxy = GetProxy();

            proxy.GetTypeHierarchyCompleted += new EventHandler<GetTypeHierarchyCompletedEventArgs>(HandleGetTypeHierarchyCompleted);
            proxy.GetTypeHierarchyAsync();
        }

        public RfidProxy.RfidServiceClient GetProxy()
        {
            return ServiceFactory.GetService<RfidServiceClient>(ServicesUri.RfidService);
        }

        public void HandleTagDataReceivedEvent(string tagData)
        {
			Logger.Info("Received tag data: " + tagData);

            TestTagData = tagData;

            var userBasketId = Convert.ToInt32(((BloodyUser)User.Current).GetProperty(ConfigurationsKeys.BASKET_UNIT_ID));

            var prep = PrepDto.CreateFromTagData(tagData, _currentUnit.Identity, AvailablePrepTypes);
            prep.UnitId = userBasketId;

            if (prep != null)
            {
                AddPrepToGrid(prep);
            }
            else
            {
                Logger.Warn("Unable to create tag from data: " + tagData);
            }
        }

        private void HandleGetTypeHierarchyCompleted(object sender, GetTypeHierarchyCompletedEventArgs e)
        {
            var availablePrepTypes = new List<PrepTypeDefinitionDto>();

            foreach (var item in e.Result)
            {
                // skip parent nodes
                if (item.ParentID.HasValue && item.ParentID.Value != 0)
                {
                    availablePrepTypes.Add(item);
                }
            }

            AvailablePrepTypes = availablePrepTypes;
        }

        private void OnTypeComboSelectionChanged(object state)
        {
            var prepType = state as PrepTypeDefinitionDto;
            
            //System.Diagnostics.Debug.WriteLine("selected: " + prepType + " with params count: " + prepType.Parameters.Length);
            System.Diagnostics.Debug.WriteLine("selected: " + prepType + " with params count: " + prepType.Parameters.Count);
            
            // update parameter grid
            var grid = (View as NewPreparateView).ParameterGrid;
            grid.Columns.Clear();

            foreach (var param in prepType.Parameters)
            {
                var column = new GridViewDataColumn();

                column.DataMemberBinding = new Binding(param.Code);
                column.Header = param.Code;

                grid.Columns.Add(column);
            }
        }

        private void OnTypeComboSelectionChanged2(object state)
        {
            var prepType = state as PrepTypeDefinitionDto;
            var grid = (View as NewPreparateView).ParameterGrid;

            _internalDataTable = new DataTable();

            foreach (var param in prepType.Parameters)
            {
                _internalDataTable.Columns.Add(new DataColumn() { ColumnName = param.Code, DataType = typeof(string) });
            }

            //var row = dt.NewRow();

            //foreach (var item in prepType.Parameters)
            //{
            //    row[item.Code] = "1";
            //}

            //dt.Rows.Add(row);

            //row = dt.NewRow();

            //foreach (var item in prepType.Parameters)
            //{
            //    row[item.Code] = "2";
            //}

            //dt.Rows.Add(row);

            grid.ItemsSource = _internalDataTable;
        }

        private void AddPrepToGrid(PrepDto prep)
        {
            if (_internalDataTable == null)
                InitializeInternalDataTable(prep.TypeId);

            var grid = (View as NewPreparateView).ParameterGrid;

            if (!ContainsDuplicate(prep))
            {
                _newPreparates.Add(prep);
                AddRow(prep);
            }

            grid.ItemsSource = _internalDataTable;
        }

        private void AddRow(PrepDto prep)
        {
            var row = _internalDataTable.NewRow();

            foreach (var item in prep.Parameters)
            {
                row[item.Name] = item.Value;
            }

            _internalDataTable.Rows.Add(row);
        }

        private void OnEnableRfidTransmission(object state)
        {
            EventAggregator.GetEvent<RfidTransmissionStateChangedEvent>().Publish(true);
        }

        private void OnDisableRfidTransmission(object state)
        {
            EventAggregator.GetEvent<RfidTransmissionStateChangedEvent>().Publish(false);
        }

        private void OnAcceptNewPreparates(object state)
        {
            var proxy = GetProxy();
            proxy.AddNewPrepCompleted += new EventHandler<AddNewPrepCompletedEventArgs>(HandleAddNewPrepCompleted);

            if (_acceptingInProgress)
                return;

            _acceptingInProgress = true;

            if (_newPreparates != null)
            {
                proxy.AddNewPrepAsync(_newPreparates.ToObservableCollection<PrepDto>());
            }
        }

        private void HandleAddNewPrepCompleted(object sender, AddNewPrepCompletedEventArgs e)
        {
            var duplicateIds = e.Result;

            // leave duplicates in the grid for manual clearing
            foreach (var id in duplicateIds)
            {
                foreach (var np in _newPreparates)
                {
                    AddDuplicate(np);
                }
            }
            
            ClearTags(false);

            _acceptingInProgress = false;
        }

        private void OnClearTagList(object state)
        {
            ClearTags(true);
        }

        private void ClearTags(bool forceClear)
        {
            _internalDataTable.Rows.Clear();
			_newPreparates.Clear();

            if (forceClear)
            {
                _duplicates.Clear();
            }
            else
            {
				//_newPreparates.AddRange(_duplicates);

                foreach (var item in _duplicates)
                {
                    AddRow(item);
                }
            }

            var grid = (View as NewPreparateView).ParameterGrid;
            grid.ItemsSource = _internalDataTable;
        }

        private void InitializeInternalDataTable(int typeId)
        {
            _internalDataTable = new DataTable();
            _newPreparates = new List<PrepDto>();

            foreach (var item in AvailablePrepTypes)
            {
                if (item.ID == typeId)
                {
                    foreach (var param in item.Parameters)
                    {
                        _internalDataTable.Columns.Add(new DataColumn() { ColumnName = param.Code, DataType = typeof(string) });
                    }

                    break;
                }
            }
        }

        private bool ContainsDuplicate(PrepDto prep)
        {
            foreach (var item in _newPreparates)
            {
                var matchCount = 0;

                for (var i = 0; i < Math.Min(item.Parameters.Count, prep.Parameters.Count); i++)
                {
                    if (item.Parameters[i].Value == prep.Parameters[i].Value)
                        matchCount++;
                }

                if (matchCount == item.Parameters.Count)
                    return true;
            }

            return false;
        }

        private void AddDuplicate(PrepDto prep)
        {
            // do not duplicate the duplicate
            foreach (var d in _duplicates)
            {
                if (d.Uid == prep.Uid)
                    return;
            }

            _duplicates.Add(prep);
        }
    }
}
