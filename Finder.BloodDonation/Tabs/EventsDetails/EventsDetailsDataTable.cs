using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataTable;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Authentication;

namespace Finder.BloodDonation.Tabs.EventsDetails
{
    public class EventsDetailsDataTable : BaseDataTable
    {

        public EventsDetailsDataTable(IUnityContainer container)
            : base(container)
        {
        }

        public override ObservableCollection<IDataTableColumn> GetColumns()
        {
		
			bool isHiddenForUser = !BloodyUser.Current.IsInRole(PermissionNames.HyperAdmin);
		
            return new ObservableCollection<IDataTableColumn>()
             .Add(50)
			 .Add(100, "Id", "ID", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "Date", "Data", FilterDataType.DateTime, TextAlignment.Left) // v
             .Add(100, "Type", "ID typu zdarzenia", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "TypeDescription", "Zdarzenie", FilterDataType.String, TextAlignment.Left) // v
             .Add(100, "Severity", "ID wagi zdarzenia", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "SeverityDescription", "Waga zdarzenia", FilterDataType.String, TextAlignment.Left) // v
             .Add(100, "UnitId", "ID strefy", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "UnitName", "Strefa", FilterDataType.String, TextAlignment.Left) 
             .Add(100, "UnitLocation", "Lokalizacja strefy", FilterDataType.String, TextAlignment.Left, true, false)
             .Add(100, "ObjectId", "ID obiektu", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "ObjectName", "Nazwa obiektu", FilterDataType.String, TextAlignment.Left)
             .Add(100, "ObjectType", "ID typu obiektu", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "ObjectTypeDescription", "Typ obiektu", FilterDataType.String, TextAlignment.Left)
             .Add(100, "ObjectUnitId", "ID oddzia�u", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "ObjectUnitName", "Oddzia�", FilterDataType.String, TextAlignment.Left, false,false)
             .Add(100, "ObjectUnitLocation", "Lokalizacja sali", FilterDataType.String, TextAlignment.Left, true, false)
             .Add(100, "EndDate", "Zako�czenie zdarzenia", FilterDataType.DateTime, TextAlignment.Left)
             .Add(100, "EndingEventId", "ID zdarzenia ko�cz�cego", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "OperatorId", "ID operatora", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "OperatorName", "Imi� operatora", FilterDataType.String, TextAlignment.Left, false, false)
             .Add(100, "OperatorLastName", "Nazwisko operatora", FilterDataType.String, TextAlignment.Left, false, false)
             .Add(100, "EventData", "Szczeg�y zmiany", FilterDataType.String, TextAlignment.Left, false, false)
             .Add(100, "Reason", "Przyczyna zmiany", FilterDataType.String, TextAlignment.Left, false, false)
             .Add(100, "RaisingEventId", "ID zdarzenia wywo�uj�cego", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "IsClosed", "Czy zamkni�ty", FilterDataType.Bool, TextAlignment.Left)
             .Add(100, "ClosingDate", "Data zamkni�cia", FilterDataType.DateTime, TextAlignment.Left)
             .Add(100, "ClosingUserId", "ID zamykaj�cego", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "ClosingUserName", "Imi� zamykaj�cego", FilterDataType.String, TextAlignment.Left)  // v
             .Add(100, "ClosingUserLastName", "Nazwisko zamykaj�cego", FilterDataType.String, TextAlignment.Left) // v
             .Add(100, "ClosingAlarmId", "Alarm ko�cz�cy zdarzenie", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "ClosingComment", "Komentarz", FilterDataType.String, TextAlignment.Left) // v
             .Add(100, "UserMessageId", "ID komunikatu u�ytkownika", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "SenderId", "ID nadawcy", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "SenderName", "Imi� nadawcy", FilterDataType.String, TextAlignment.Left, isHiddenForUser)
             .Add(100, "SenderLastName", "Nazwisko nadawcy", FilterDataType.String, TextAlignment.Left, isHiddenForUser)
             .Add(100, "Message", "Komunikat", FilterDataType.String, TextAlignment.Left, isHiddenForUser)
                          ;
        }

        public override ISortedFilter GetFilter(IUnityContainer container)
        {
            return new EventsDetailsFilter(container);
        }

		public override bool StartWithDefaultColumns
        {
            get
            {
                return false;
            }
        }
    }
}
