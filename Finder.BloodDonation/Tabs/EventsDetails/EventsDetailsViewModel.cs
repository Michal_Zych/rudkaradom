using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Events;
using System.Collections.Generic;
using System.Collections;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Dialogs.Patients;
using Finder.BloodDonation.Dialogs.Helpers;
using Microsoft.Practices.Composite.Presentation.Events;
using System.Diagnostics;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.Dialogs.Bands;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.UsersService;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.Common.Filters;

namespace Finder.BloodDonation.Tabs.EventsDetails
{
    [OnCompleted]
    [UIException]
    public class EventsDetailsViewModel : ViewModelBase, ITabViewModel, IUnitFiltered
    {
        private readonly IWindowsManager _windowsManager;

        public RelayCommand CloseAlarm { get; set; }

        public RelayCommand OnlyAlarms { get; set; }
        public RelayCommand HistoryOfStay { get; set; }
        public RelayCommand AllEvents { get; set; }

        public event EventHandler LoadingCompleted = delegate { };
        private IUnit SelectedUnit = null;
        private IUnit _currentUnit;

        private EventsDetailsDto selectedItem;

        public string CloseAlarmTag { get; set; }
        private UsersServiceClient Proxy { get; set; }

        [RaisePropertyChanged]
        public Visibility IsVisibleForUnit { get; set; }

        private IUnitFiltered _details;
        [RaisePropertyChanged]
        public IUnitFiltered Details
        {
            get
            {
                if (_details == null)
                {
                    if(CloseAlarmTag == String.Empty || CloseAlarmTag == null)
                    {
                        CloseAlarmTag = "UnitTag";
                    }

                    _details = Container.Resolve<EventsDetailsDetailsListViewModel>();
                    (_details as EventsDetailsDetailsListViewModel).CloseAlarmTag = CloseAlarmTag;
                }
                return _details;
            }
            set
            {
                _details = value;
                Refresh(_currentUnit);
            }
        }

        public EventsDetailsViewModel(IUnityContainer container)
            : base(container)
        {
            this.ViewAttached += new EventHandler(ViewModel_ViewAttached);
            EventAggregator.GetEvent<SelectedItemChangedEvent<EventsDetailsDto>>().Subscribe(OnSelectedItemChangedEvent);
            EventAggregator.GetEvent<ChangeEventButtonVisibilityForUnitEvent>().Subscribe(OnChangeButtonVisibility);

            var viewFactory = container.Resolve<ViewFactory>();
            _windowsManager = this.Container.Resolve<IWindowsManager>();

            CloseAlarm = new RelayCommand(OnCloseAlarm);
            OnlyAlarms = new RelayCommand(OnOnlyAlarms);
            AllEvents = new RelayCommand(OnAllEvents);

            Proxy = ServiceFactory.GetService<UsersServiceClient>(/*ServicesUri.UsersService*/);
        }

        public void OnCloseAlarm()
        {
            EventAggregator.GetEvent<CloseAlarmForUnitEvent>().Publish("UnitTag");
        }

        public void OnAllEvents()
        {
            EventAggregator.GetEvent<GetAllEventsEvent>().Publish(null);
            Refresh(SelectedUnit);
        }

        public void OnHistoryOfStay()
        {
            EventAggregator.GetEvent<GetHistoryOfStayEvent>().Publish(null);
            Refresh(SelectedUnit);
        }

        public void OnOnlyAlarms()
        {
            EventAggregator.GetEvent<GetOnlyAlarmsEvent>().Publish(null);
            Refresh(SelectedUnit);
        }


        public void OnChangeButtonVisibility(Visibility obj)
        {
            IsVisibleForUnit = obj;
        }

        public void OnSelectedItemChangedEvent(IItemViewModel<EventsDetailsDto> obj)
        {
            if(obj == null)
            {
                selectedItem = null;
            }
            else
            {
                selectedItem = obj.Item;
            }
            SelectedItemChanged();
        }

        private void SelectedItemChanged()
        {

        }
        
        private void ViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public void Refresh(IUnit unit)
        {
            if (unit != null)
            {
                EventAggregator.GetEvent<EventTableType>().Publish("Unit");
                SelectedUnit = unit;
                _currentUnit = unit;
                IsVisibleForUnit = Visibility.Visible;
                Details.Refresh(_currentUnit);
            }
		}

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        public string Title
        {
            get { return ""; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }

        private void CloseWindowEventHandling(string key)
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(CloseWindowEventHandling);
        }

        public Telerik.Windows.Controls.CommandBindingCollection GetCommandBindings()
        {
            var bindings = new Telerik.Windows.Controls.CommandBindingCollection();

            return bindings;
		}
    }
}
