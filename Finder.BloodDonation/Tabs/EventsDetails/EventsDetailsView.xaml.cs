using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Tools;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Dialogs.Patients;
using Microsoft.Practices.Composite.Events;
using Finder.BloodDonation.Common.Events;
using Microsoft.Practices.Composite.Presentation.Events;
using Finder.BloodDonation.Common.Layout;

namespace Finder.BloodDonation.Tabs.EventsDetails
{
    [ViewModel(typeof(EventsDetailsViewModel))]
    public partial class EventsDetailsView : UserControl
    {
        public void DataContextChangedm(EventsDetailsDetailsListView sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                var vm = (EventsDetailsDetailsListViewModel)e.NewValue;

                if (vm != null)
                {
                    CommandManager.SetCommandBindings(this, vm.GetCommandBindings());
                }
            }
        }

        public EventsDetailsView()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            onlyAlarm_Button.Style = Application.Current.Resources["TabButtonUnselected"] as Style;
            allEvents_Button.Style = Application.Current.Resources["TabButtonUnselected"] as Style;

            ((Button)sender).Style = Application.Current.Resources["TabButtonSelected"] as Style;
        }

        private void onlyAlarm_Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}