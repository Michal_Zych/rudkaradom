using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.EventsDetails
{
    public class EventsDetailsRemotePagedDataSource : IPagedDataSource<EventsDetailsItemViewModel>
    {
        protected IUnityContainer _container;
        protected int _pageSize;
        protected IDataTable _dataTable;
        protected IUnitDataClient _dataClient;
        private int patientId;
        private const string PATIENT_TYPE = "4";
        private string severityFrom;
        private string severityTo;
        private string typeFrom;
        private string typeTo;
        public static EventsFilteredType filteredType;
        public static IUnit unitToRefreshed;


        public EventsDetailsRemotePagedDataSource(IUnityContainer container, int pageSize, IDataTable dataTable, IUnitDataClient dataClient, int patientId, string severityFrom, string severityTo, string typeFrom, string typeTo)
        {
            _container = container;
            _pageSize = pageSize;
            _dataTable = dataTable;
            _dataClient = dataClient;
            this.patientId = patientId;

            if (String.IsNullOrEmpty(severityFrom))
                this.severityFrom = String.Empty;
            else
                this.severityFrom = severityFrom;

            if (String.IsNullOrEmpty(severityTo))
                this.severityTo = String.Empty;
            else
                this.severityTo = severityTo;

            if (String.IsNullOrEmpty(typeFrom))
                this.typeFrom = String.Empty;
            else
                this.typeFrom = typeFrom;

            if (String.IsNullOrEmpty(typeTo))
                this.typeTo = String.Empty;
            else
                this.typeTo = typeTo;
        }


        public void FetchData(int pageNumber, Action<PagedDataResponse<EventsDetailsItemViewModel>> responseCallback)
        {
            var proxy = _dataClient.GetProxy();
            var list = new List<EventsDetailsItemViewModel>();

            if (filteredType == EventsFilteredType.MonitorableObject)
            {
                proxy.GetEventsDetailsCompleted += (s, e) =>
                {
                    foreach (var item in e.Result)
                    {
                        var vm = _container.Resolve<EventsDetailsItemViewModel>();
                        vm.Item = item;
                        vm.DataTable = _dataTable;
                        list.Add(vm);
                    }
                    _container.Resolve<EventsDetailsItemViewModel>().SelectionChanged(-1);
                    Dictionary<string, string> filterDictionary = (Dictionary<string, string>)_dataTable.GetFilterDictionary();

                    if (filterDictionary.ContainsKey("IsClosed"))
                    {
                        filterDictionary["IsClosedFrom"] = filterDictionary["IsClosed"];
                        filterDictionary["IsClosedTo"] = filterDictionary["IsClosed"];
                    }
                    if (!String.IsNullOrEmpty(severityFrom))
                    {
                        filterDictionary["SeverityFrom"] = severityFrom;
                    }
                    if (!String.IsNullOrEmpty(severityTo))
                    {
                        filterDictionary["SeverityTo"] = severityTo;
                    }

                    if (!String.IsNullOrEmpty(typeFrom))
                    {
                        filterDictionary["TypeFrom"] = typeFrom;
                    }

                    if (!String.IsNullOrEmpty(typeTo))
                    {
                        filterDictionary["TypeTo"] = typeTo;
                    }

                    filterDictionary["TreeUnitId"] = null;
                    filterDictionary["ObjectIdFrom"] = patientId.ToString();
                    filterDictionary["ObjectIdTo"] = patientId.ToString();
                    filterDictionary["ObjectTypeFrom"] = PATIENT_TYPE;
                    filterDictionary["ObjectTypeTo"] = PATIENT_TYPE;
                    proxy.GetEventsDetailsFilteredTotalCountAsync(filterDictionary);
                };

                proxy.GetEventsDetailsFilteredTotalCountCompleted += (s, e) =>
                {
                    responseCallback(new PagedDataResponse<EventsDetailsItemViewModel>()
                    {
                        Items = list,
                        TotalItemCount = e.Result
                    });
                };

                Dictionary<string, string> fdict = (Dictionary<string, string>)_dataTable.GetFilterDictionary();

                if (fdict.ContainsKey("IsClosed"))
                {
                    fdict["IsClosedFrom"] = fdict["IsClosed"];
                    fdict["IsClosedTo"] = fdict["IsClosed"];
                }
                if (!String.IsNullOrEmpty(severityFrom))
                {
                    fdict["SeverityFrom"] = severityFrom;
                }
                if (!String.IsNullOrEmpty(severityTo))
                {
                    fdict["SeverityTo"] = severityTo;
                }

                if (!String.IsNullOrEmpty(typeFrom))
                {
                    fdict["TypeFrom"] = typeFrom;
                }

                if (!String.IsNullOrEmpty(typeTo))
                {
                    fdict["TypeTo"] = typeTo;
                }

                fdict["TreeUnitId"] = null;
                fdict["ObjectIdFrom"] = patientId.ToString();
                fdict["ObjectIdTo"] = patientId.ToString();
                fdict["ObjectTypeFrom"] = PATIENT_TYPE;
                fdict["ObjectTypeTo"] = PATIENT_TYPE;
                proxy.GetEventsDetailsAsync(fdict, pageNumber, _pageSize);
            }
            else
            {
                proxy.GetEventsForUnitsFilteredCompleted += (s, e) =>
                {
                    foreach (var item in e.Result)
                    {
                        var vm = _container.Resolve<EventsDetailsItemViewModel>();
                        vm.Item = item;
                        vm.DataTable = _dataTable;
                        list.Add(vm);
                    }

                    _container.Resolve<EventsDetailsItemViewModel>().SelectionChanged(-1);


                    Dictionary<string, string> filterDictionary = (Dictionary<string, string>)_dataTable.GetFilterDictionary();

                    if (filterDictionary.ContainsKey("IsClosed"))
                    {
                        filterDictionary["IsClosedFrom"] = filterDictionary["IsClosed"];
                        filterDictionary["IsClosedTo"] = filterDictionary["IsClosed"];
                    }
                    if (!String.IsNullOrEmpty(severityFrom))
                    {
                        filterDictionary["SeverityFrom"] = severityFrom;
                    }
                    if (!String.IsNullOrEmpty(severityTo))
                    {
                        filterDictionary["SeverityTo"] = severityTo;
                    }

                    if (!String.IsNullOrEmpty(typeFrom))
                    {
                        filterDictionary["TypeFrom"] = typeFrom;
                    }

                    if (!String.IsNullOrEmpty(typeTo))
                    {
                        filterDictionary["TypeTo"] = typeTo;
                    }

                    filterDictionary["TreeUnitId"] = unitToRefreshed.Identity.ToString();
                    proxy.GetEventsForUnitsFilteredTotalCountAsync(filterDictionary);
                };

                proxy.GetEventsForUnitsFilteredTotalCountCompleted += (s, e) =>
                {
                    responseCallback(new PagedDataResponse<EventsDetailsItemViewModel>()
                    {
                        Items = list,
                        TotalItemCount = e.Result
                    });
                };

                Dictionary<string, string> fdict = (Dictionary<string, string>)_dataTable.GetFilterDictionary();


                if (fdict.ContainsKey("IsClosed"))
                {
                    fdict["IsClosedFrom"] = fdict["IsClosed"];
                    fdict["IsClosedTo"] = fdict["IsClosed"];
                }
                if (!String.IsNullOrEmpty(severityFrom))
                {
                    fdict["SeverityFrom"] = severityFrom;
                }
                if (!String.IsNullOrEmpty(severityTo))
                {
                    fdict["SeverityTo"] = severityTo;
                }

                if (!String.IsNullOrEmpty(typeFrom))
                {
                    fdict["TypeFrom"] = typeFrom;
                }

                if (!String.IsNullOrEmpty(typeTo))
                {
                    fdict["TypeTo"] = typeTo;
                }

                fdict["TreeUnitId"] = unitToRefreshed.Identity.ToString();
                proxy.GetEventsForUnitsFilteredAsync(fdict, pageNumber, _pageSize);
            }
        }
    }
}
