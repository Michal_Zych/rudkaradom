using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.EventsDetails
{
    public class EventsDetailsFilter : SortedFilter
    {
       [RaisePropertyChanged]
        public string Id { get; set; }

        [RaisePropertyChanged]
        public DateTime? DateFrom { get; set; }
        [RaisePropertyChanged]
        public DateTime? DateTo { get; set; }

        [RaisePropertyChanged]
        public string Type { get; set; }

        [RaisePropertyChanged]
        public string TypeDescription { get; set; }

        [RaisePropertyChanged]
        public string Severity { get; set; }

        [RaisePropertyChanged]
        public string SeverityDescription { get; set; }

        [RaisePropertyChanged]
        public string UnitId { get; set; }

        [RaisePropertyChanged]
        public string UnitName { get; set; }

        [RaisePropertyChanged]
        public string UnitLocation { get; set; }

        [RaisePropertyChanged]
        public string ObjectId { get; set; }

        [RaisePropertyChanged]
        public string ObjectName { get; set; }

        [RaisePropertyChanged]
        public string ObjectType { get; set; }

        [RaisePropertyChanged]
        public string ObjectTypeDescription { get; set; }

        [RaisePropertyChanged]
        public string ObjectUnitId { get; set; }

        [RaisePropertyChanged]
        public string ObjectUnitName { get; set; }

        [RaisePropertyChanged]
        public string ObjectUnitLocation { get; set; }

        [RaisePropertyChanged]
        public DateTime? EndDateFrom { get; set; }
        [RaisePropertyChanged]
        public DateTime? EndDateTo { get; set; }

        [RaisePropertyChanged]
        public string EndingEventId { get; set; }

        [RaisePropertyChanged]
        public string OperatorId { get; set; }

        [RaisePropertyChanged]
        public string OperatorName { get; set; }

        [RaisePropertyChanged]
        public string OperatorLastName { get; set; }

        [RaisePropertyChanged]
        public string EventData { get; set; }

        [RaisePropertyChanged]
        public string Reason { get; set; }

        [RaisePropertyChanged]
        public string RaisingEventId { get; set; }

        [RaisePropertyChanged]
        public bool? IsClosed { get; set; }

        [RaisePropertyChanged]
        public DateTime? ClosingDateFrom { get; set; }
        [RaisePropertyChanged]
        public DateTime? ClosingDateTo { get; set; }

        [RaisePropertyChanged]
        public string ClosingUserId { get; set; }

        [RaisePropertyChanged]
        public string ClosingUserName { get; set; }

        [RaisePropertyChanged]
        public string ClosingUserLastName { get; set; }

        [RaisePropertyChanged]
        public string ClosingAlarmId { get; set; }

        [RaisePropertyChanged]
        public string ClosingComment { get; set; }

        [RaisePropertyChanged]
        public string UserMessageId { get; set; }

        [RaisePropertyChanged]
        public string SenderId { get; set; }

        [RaisePropertyChanged]
        public string SenderName { get; set; }

        [RaisePropertyChanged]
        public string SenderLastName { get; set; }

        [RaisePropertyChanged]
        public string Message { get; set; }

        public EventsDetailsFilter(IUnityContainer container)
            : base(container)
        {

        }
    }
}
