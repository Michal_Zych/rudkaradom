using System.Collections;
using System.Linq;
using Finder.BloodDonation.Behaviors;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.DynamicData.Paging;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;
using Telerik.Windows.Controls.DragDrop;
using Finder.BloodDonation.Model.Dto;
using FinderFX.SL.Core.Extensions;
using FinderFX.SL.Core.Authentication;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Common.Events;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Windows;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.Settings;
using System.Windows.Input;
using Finder.BloodDonation.DynamicData.Memory;
using Finder.BloodDonation.DynamicData.DBs;
using System.Globalization;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Dialogs.DataTable;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.UnitsService;
using GalaSoft.MvvmLight.Messaging;
using Finder.BloodDonation.Common.Messages;
using Finder.BloodDonation.Dialogs.Alarms;
using Finder.BloodDonation.Tabs.AlarmsLocalization.Dialogs;
using Finder.BloodDonation.AlarmsService;
using Finder.BloodDonation.Dialogs.Success;

namespace Finder.BloodDonation.Tabs.EventsDetails
{
    [OnCompleted]
    [UIException]
    public class EventsDetailsDetailsListViewModel : ViewModelBase, IUnitFiltered, IUnitDataClient
    {
        private int Clicked = 0;

        private IUnit _currentUnit;
        private int currentPatientId;

        private const int EVENT_SEVERITY = 0;

        private const string ALARMS_OR_WARNINGS_SEVERITY_QUERY_FROM = "1";
        private const string ALARMS_OR_WARNINGS_SEVERITY_QUERY_TO = "2";
        private const string HISTORY_OF_STAY_TYPE_QUERY_FROM = "3";
        private const string HISTORY_OF_STAY_TYPE_QUERY_TO = "9";

        public static string EventsInTableType { get; set; }

        private FilterAfter FilterAfter { get; set; }
        public static IUnit unitToRefreshed = null;

        [RaisePropertyChanged]
        public bool IsLoading { get; set; }

        private EditDetailsAlarmData _alarmsData;

        public Finder.BloodDonation.AlarmsService.AlarmsServiceClient Service { get; set; }

        public string CloseAlarmTag { get; set; }

        public void _alarmsService_CloseAlarmsCompleted(object sender, AlarmsService.CloseAlarmsCompletedEventArgs e)
        {

        }

        public int UnitId
        {
            get
            {
                return _currentUnit.Identity;
            }
        }


        private EventsDetailsItemViewModel selectedItem;
        [RaisePropertyChanged]
        public EventsDetailsItemViewModel SelectedItem
        {
            get
            {
                return selectedItem;
            }
            set
            {
                selectedItem = value;
                EventAggregator.GetEvent<SelectedItemChangedEvent<EventsDetailsDto>>().Publish(selectedItem);
            }
        }

        //[RaisePropertyChanged]
        public ServerSidePagedCollectionView<EventsDetailsItemViewModel> Items { get; set; }

        private int _pageSize = 25;

        [RaisePropertyChanged]
        public int PageSize
        {
            get
            {
                if (_pageSize == 0)
                {
                    return Int32.MaxValue;
                }
                else if (_pageSize < 0)
                {
                    var actualHeight = System.Windows.Application.Current.Host.Content.ActualHeight - 330;
                    return (int)Math.Floor(actualHeight / 27);
                }
                return _pageSize;
            }
            set
            {
                _pageSize = value;
            }
        }

        [RaisePropertyChanged]
        public Visibility QuickSearchVisibility { get { return Visibility.Visible; } }

        [RaisePropertyChanged]
        public string QuickSearch { get; set; }

        private IWindowsManager _windowsManager = null;
        public DelegateCommand<object> FilterContentsApply { get; set; }
        public DelegateCommand<object> FilterContentsClear { get; set; }
        public DelegateCommand<object> HeaderClicked { get; set; }
        public DelegateCommand<object> QuickSearchCommand { get; set; }

        private UnitsServiceClient Proxy { get; set; }
        public RelayCommand<KeyEventArgs> OnKeyUpOnFilter { get; set; }
        public RelayCommand<KeyEventArgs> OnKeyUpOnQuickSearch { get; set; }

        //private InMemoryTable<EventsDetailsDto> EventsDetailsTable;

        [RaisePropertyChanged]
        public IDataTable DataTable { get; set; }


        public EventsDetailsDetailsListViewModel(IUnityContainer container)
            : base(container)
        {
            DataTable = new EventsDetailsDataTable(container);
            IsLoading = true;

            FilterContentsApply = new DelegateCommand<object>(OnFilterContentsApply);
            FilterContentsClear = new DelegateCommand<object>(OnFilterContentsClear);
            OnKeyUpOnFilter = new RelayCommand<KeyEventArgs>(OnKeyUpOnFilterEvent);
            OnKeyUpOnQuickSearch = new RelayCommand<KeyEventArgs>(OnKeyUpOnQuickSearchEvent);
            HeaderClicked = new DelegateCommand<object>(OnHeaderClicked);
            QuickSearchCommand = new DelegateCommand<object>(OnQuickSearchCommand);

            EventAggregator.GetEvent<SaveSettingsEvent>().Subscribe(HandleSaveSettings);

            Messenger.Default.Register<PatientInfoMessage>(this, MessageReceiver);

            EventAggregator.GetEvent<GetAllEventsEvent>().Subscribe(OnAllEvents);
            EventAggregator.GetEvent<GetOnlyAlarmsEvent>().Subscribe(OnOnlyAlarms);
            EventAggregator.GetEvent<GetHistoryOfStayEvent>().Subscribe(OnOnlyEvents);
            EventAggregator.GetEvent<CloseAlarmForPatientEvent>().Subscribe(OnClosePatientEvents);
            EventAggregator.GetEvent<CloseAlarmForUnitEvent>().Subscribe(OnCloseUnitEvents);
            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(OnCloseAlarmWindow);
            EventAggregator.GetEvent<EventTableType>().Subscribe(OnChangeEventTableType);
            EventAggregator.GetEvent<PingUserSessionEvent>().Subscribe(OnPingUserSession);

            EventAggregator.GetEvent<EventsFilteredMethodChanger>().Subscribe(OnFilteredMethodChnger);
            Service = ServiceFactory.GetService<AlarmsServiceClient>(ServicesUri.AlarmsService);
            
            Proxy = ServiceFactory.GetService<UnitsServiceClient>();

            Service.LocalizationCloseAlarmsCompleted += Service_LocalizationCloseAlarmsCompleted;

            FilterAfter = EventsDetails.FilterAfter.ALL_EVENTS;

            var viewFactory = container.Resolve<ViewFactory>();
            _windowsManager = this.Container.Resolve<IWindowsManager>();

            if (_windowsManager != null)
            {
                _windowsManager.RegisterWindow(WindowsKeys.CloseAlarmDialog, "",
                    viewFactory.CreateView<EditDetailsAlarm>(), new Size(400, 300), false, true);

                _windowsManager.RegisterWindow(WindowsKeys.SuccessDialog, "Powodzenie",
                    viewFactory.CreateView<Success>(), new Size(440, 170), true);
            }

            Refresh(null);
        }

        private bool IsUpToDate { get; set; }

        [RaisePropertyChanged]
        public string IconPath
        {
            get
            {
                if (IsUpToDate)
                    return "../../Images/apply_icon.png";
                else
                    return "../../Images/refresh-icon.png";
            }
        }

        public void OnPingUserSession(string arg)
        {
            IsUpToDate = false;
            NotifyPropertyChange("IconPath");
        }

        public void OnChangeEventTableType(string eventsType)
        {
            if (EventsInTableType.Equals(String.Empty))
                EventsInTableType = eventsType;
        }

        public void OnCloseUnitEvents(string obj)
        {
            if (obj == CloseAlarmTag)
                OnCloseAlarm();
        }

        public void Service_LocalizationCloseAlarmsCompleted(object sender, LocalizationCloseAlarmsCompletedEventArgs e)
        {
            _windowsManager.ShowWindow(WindowsKeys.SuccessDialog, "Alarm zosta� zamkni�ty pomy�lnie!");
            Refresh(_currentUnit);
        }

        public void OnCloseAlarmWindow(string key)
        {
            if (key.Equals(WindowsKeys.CloseAlarmDialog) && _alarmsData != null)
            {
                if (_alarmsData.Canceled)
                {
                    MsgBox.Error("Zamkni�cie alarmu zosta�o anulowane!");
                    return;
                }

                if (_alarmsData == null)
                {
                    MsgBox.Error("Zamkni�cie alarmu nie powiod�o si�!");
                    return;
                }

                Service.LocalizationCloseAlarmsAsync(_alarmsData.AlarmsToClose.ToCommaSeparatedValues(), _alarmsData.AlarmCloseComment);
                _alarmsData = null;
            }
        }

        public void OnFilteredMethodChnger(EventsFilteredType obj)
        {
             EventsDetailsRemotePagedDataSource.filteredType = obj;


             if (obj == EventsFilteredType.MonitorableObject)
             {
                 EventsDetailsRemotePagedDataSource.unitToRefreshed = null;
                 Refresh(null);
             }
             else if(obj == EventsFilteredType.Unit)
             {
                 EventsDetailsRemotePagedDataSource.unitToRefreshed = unitToRefreshed;
                 if (EventsDetailsRemotePagedDataSource.unitToRefreshed != null)
                     Refresh(unitToRefreshed);
             }
        }

        public void OnClosePatientEvents(string key)
        {
            if (key == CloseAlarmTag)
            {
                if (selectedItem == null)
                {
                    MsgBox.Error("Nie wybrano alarmu do zamkni�cia!");
                    return;
                }

                if (selectedItem.Item == null)
                {
                    MsgBox.Error("Nie wybrano alarmu do zamkni�cia!");
                    return;
                }

                if (selectedItem.Item.Severity == EVENT_SEVERITY)
                {
                    MsgBox.Error("Wybrane zdarzenie nie jest alarmem / ostrze�eniem.");
                    return;
                }

                _alarmsData = new EditDetailsAlarmData();
                _alarmsData.AlarmId = selectedItem.Item.Id;
                _alarmsData.AlarmObjectName = selectedItem.Item.ObjectName;
                _alarmsData.AlarmTypeDescription = selectedItem.Item.TypeDescription;
                _alarmsData.AlarmUnitName = selectedItem.Item.UnitName + " " + Container.Resolve<SystemSettingDto>().UnitsLocationSeparator + " " + selectedItem.Item.UnitLocation;
                _alarmsData.AlarmDate = selectedItem.Item.Date;
                _alarmsData.AlarmsToClose = new List<int>() { (int)selectedItem.Item.Id };

                _windowsManager.ShowWindow(WindowsKeys.CloseAlarmDialog, _alarmsData);
            }
        }

        private void OnCloseAlarm()
        {
            if (selectedItem == null)
            {
                MsgBox.Error("Nie wybrano alarmu do zamkni�cia!");
                return;
            }

            if (selectedItem.Item == null)
            {
                MsgBox.Error("Nie wybrano alarmu do zamkni�cia!");
                return;
            }

            if (selectedItem.Item.Severity == EVENT_SEVERITY)
            {
                MsgBox.Error("Wybrane zdarzenie nie jest alarmem / ostrze�eniem.");
                return;
            }

            _alarmsData = new EditDetailsAlarmData();
            _alarmsData.AlarmId = selectedItem.Item.Id;
            _alarmsData.AlarmObjectName = selectedItem.Item.ObjectName;
            _alarmsData.AlarmTypeDescription = selectedItem.Item.TypeDescription;
            _alarmsData.AlarmUnitName = selectedItem.Item.UnitName + " " + Container.Resolve<SystemSettingDto>().UnitsLocationSeparator + " " + selectedItem.Item.UnitLocation;
            _alarmsData.AlarmDate = selectedItem.Item.Date;
            _alarmsData.AlarmsToClose = new List<int>() { (int)selectedItem.Item.Id };

            _windowsManager.ShowWindow(WindowsKeys.CloseAlarmDialog, _alarmsData);
        }

        //Zaimplementowa�
        /// <summary>
        /// ////////////
        /// </summary>
        /// <param name="obj"></param>
        public void OnOnlyEvents(object obj)
        {
            FilterAfter = EventsDetails.FilterAfter.HISTORY_OF_STAY;
            Refresh(unitToRefreshed);
        }

        public void OnOnlyAlarms(object obj)
        {
            FilterAfter = EventsDetails.FilterAfter.ONLY_ALARMS;
            Refresh(unitToRefreshed);
        }

        public void OnAllEvents(object obj)
        {
            FilterAfter = EventsDetails.FilterAfter.ALL_EVENTS;
            Refresh(unitToRefreshed);
        }
    

        public void MessageReceiver(PatientInfoMessage obj)
        {
            if(DataTable != null)
            {
                DataTable.Clear();
            }

            if (obj != null)
                currentPatientId = int.Parse(obj.Id);

            Refresh(_currentUnit);
        }

        //EventsDetailsTable = new InMemoryTable<EventsDetailsDto>(container.Resolve<IUnitsManager>(), DbEventsDetails.GetItems()/*.Where(e=>e.ID == 0).ToList()*/);

        public void OnQuickSearchCommand(object obj)
        {
            int newIndex = DataTable.SearchItem<EventsDetailsItemViewModel>(Items, SelectedItem, QuickSearch);
            if (newIndex >= 0)
            {
                SelectedItem = Items[newIndex];
            }
        }

        public void HandleSaveSettings(bool obj)
        {
            DataTable.SaveColumns();
        }

        private void OnKeyUpOnFilterEvent(KeyEventArgs obj)
        {
            if (obj.Key == Key.Enter)
            {
                FilterContentsApply.Execute(null);
            }
        }

        private void OnKeyUpOnQuickSearchEvent(KeyEventArgs obj)
        {
            if (obj.Key == Key.Enter)
            {
                QuickSearchCommand.Execute(null);
            }
            if (obj.Key == Key.Escape)
            {
                QuickSearch = String.Empty;
            }
        }

        private void OnFilterContentsClear(object obj)
        {
            OnFilterContentsApply(true);
        }

        private void OnFilterContentsApply(object state)
        {
			bool clearFilter = (bool?)state ?? false;

            if(clearFilter)
            {
                DataTable.ClearFilter();
            }
            Refresh(_currentUnit);
        }

        private void SetSelectionAfterFiltering(string selectedIdentity)
        {
            if (selectedIdentity!=null && DataTableSettings.AfterFilteringKeepSelection)
            {
                SelectItem(selectedIdentity);
                if (SelectedItem == null && DataTableSettings.AfterFilteringSelectFirst)
                {
                    SelectItem(0);
                }
            }
            else if (selectedIdentity==null && DataTableSettings.AfterFilteringSelectFirst)
            {
                SelectItem(0);
            }
        }


        private void OnHeaderClicked(object param)
        {
            string selectedId = SelectedItemIdentity();

            string state = (string)param;

            DataTable.SortOrder = state;

            if (state == null || state == String.Empty)
                return;
            Refresh(_currentUnit);

            if (DataTableSettings.AfterSortingKeepSelection)
            {
                SelectItem(selectedId);
            }
            else
            {
                SelectItem(0);
            }
        }

        private string SelectedItemIdentity()
        {
            return SelectedItem == null ? null : SelectedItem.Item.Identity;
        }
        private void SelectItem(string identity)
        {
            SelectedItem = Items.Where(x => x.Item.Identity == identity).FirstOrDefault();
        }
        private void SelectItem(int position)
        {
            if (Items != null && Items.Count > 0)
            {
                if (Items.Count > position)
                {
                    SelectedItem = Items[0];
                }
                else
                {
                    SelectedItem = Items[Items.Count - 1];
                }
            }
        }

        private string selectedIdentity;

        public void Refresh(IUnit unit)
        {
            _currentUnit = unit;

            Clicked = 0;
            unitToRefreshed = unit;

            /*if (currentPatientId == 0 && unitToRefreshed != null)
            {
                //EventAggregator.GetEvent<SendAfterRegisterResolveEvent>().Publish(null);
                Items = new ServerSidePagedCollectionView<EventsDetailsItemViewModel>(
                    new EventsDetailsRemotePagedDataSource(Container, PageSize, DataTable, this, unitToRefreshed.Identity, String.Empty, String.Empty, "2", "2"), 50);

            }*/
            if(unitToRefreshed == null && currentPatientId == 0)
            {
                EventAggregator.GetEvent<SendAfterRegisterResolveEvent>().Publish(null);
            }
            else
            {
                //if (Items == null)
                //{
                //    Items = new ServerSidePagedCollectionView<EventsDetailsItemViewModel>(
                //        new EventsDetailsRemotePagedDataSource(Container, PageSize, DataTable, this, currentPatientId,
                //            FilterAfter == EventsDetails.FilterAfter.ONLY_ALARMS ? ALARMS_OR_WARNINGS_SEVERITY_QUERY_FROM : String.Empty,
                //            FilterAfter == EventsDetails.FilterAfter.ONLY_ALARMS ? ALARMS_OR_WARNINGS_SEVERITY_QUERY_TO : String.Empty,
                //            FilterAfter == EventsDetails.FilterAfter.HISTORY_OF_STAY ? HISTORY_OF_STAY_TYPE_QUERY_FROM : String.Empty,
                //            FilterAfter == EventsDetails.FilterAfter.HISTORY_OF_STAY ? HISTORY_OF_STAY_TYPE_QUERY_TO : String.Empty)
                //        , PageSize);

                //    Items.PageChanged += newItems_PageChanged;
                //}
                //else
                //{
                //    selectedIdentity = SelectedItemIdentity();
                //    Items.RefreshData(0);
                //}

                var newItems = new ServerSidePagedCollectionView<EventsDetailsItemViewModel>(
                        new EventsDetailsRemotePagedDataSource(Container, PageSize, DataTable, this, currentPatientId,
                            FilterAfter == EventsDetails.FilterAfter.ONLY_ALARMS ? ALARMS_OR_WARNINGS_SEVERITY_QUERY_FROM : String.Empty,
                            FilterAfter == EventsDetails.FilterAfter.ONLY_ALARMS ? ALARMS_OR_WARNINGS_SEVERITY_QUERY_TO : String.Empty,
                            FilterAfter == EventsDetails.FilterAfter.HISTORY_OF_STAY ? HISTORY_OF_STAY_TYPE_QUERY_FROM : String.Empty,
                            FilterAfter == EventsDetails.FilterAfter.HISTORY_OF_STAY ? HISTORY_OF_STAY_TYPE_QUERY_TO : String.Empty)
                        , PageSize);

                newItems.PageChanged += newItems_PageChanged;
            }
        }

        private void newItems_PageChanged(object sender, EventArgs e)
        {
            selectedIdentity = SelectedItemIdentity();
            //string selectedId = SelectedItemIdentity();
            var items = (ServerSidePagedCollectionView<EventsDetailsItemViewModel>)sender;

            //if (Items != null && Items.PageSize == items.PageSize)
            //{
            //    for (int i = 0; i < items.Count; i++)
            //    {
            //        var source = items[i];
            //        var target = Items[i];

            //        foreach (var property in source.Item.GetType().GetProperties())
            //        {
            //            if (property.GetSetMethod() != null)
            //                property.SetValue(target.Item, property.GetValue(source.Item, null), null);
            //        }
            //    }

            //    GC.Collect();
            //}
            //else
            //{
            //    Items = items;
            //}

            Items = items;

            NotifyPropertyChange("Items");
            IsUpToDate = true;
            NotifyPropertyChange("IconPath");
            SetSelectionAfterFiltering(selectedIdentity);

            //if (currentPatientId == 0 || (currentPatientId != 0 && items.All(x => x.Item.ObjectId == currentPatientId))) 
            //{ 
            //    Items = items;
            //    NotifyPropertyChange("Items");
            //    IsUpToDate = true;
            //    NotifyPropertyChange("IconPath");
            //    SetSelectionAfterFiltering(selectedIdentity);
            //}
        }

        public Telerik.Windows.Controls.CommandBindingCollection GetCommandBindings()
        {
            var bindings = new Telerik.Windows.Controls.CommandBindingCollection();

            return bindings;
        }

		 public UnitsServiceClient GetProxy()
        {
            return ServiceFactory.GetService<UnitsServiceClient>(ServicesUri.UnitsService);    
        }
    }
}


//1031