﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.EventsDetails
{
    public enum FilterAfter
    {
        ONLY_ALARMS = 0,
        ALL_EVENTS = 1,
        HISTORY_OF_STAY
    }
}
