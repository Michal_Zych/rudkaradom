using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.BandStatus
{
    public class BandStatusRemotePagedDataSource : IPagedDataSource<BandStatusItemViewModel>
    {
        protected IUnityContainer _container;
        protected int _pageSize;
        protected IDataTable _dataTable;
        protected IUnitDataClient _dataClient;

        public BandStatusRemotePagedDataSource(IUnityContainer container, int pageSize, IDataTable dataTable, IUnitDataClient dataClient)
        {
            _container = container;
            _pageSize = pageSize;
            _dataTable = dataTable;
            _dataClient = dataClient;
        }


        public void FetchData(int pageNumber, Action<PagedDataResponse<BandStatusItemViewModel>> responseCallback)
        {
            var proxy = _dataClient.GetProxy();
            var list = new List<BandStatusItemViewModel>();

            proxy.GetBandsStatusesCompleted += (s, e) =>
                {
                    foreach (var item in e.Result)
                    {
                        var vm = _container.Resolve<BandStatusItemViewModel>();
                        vm.Item = item;
                        vm.DataTable = _dataTable;
                        list.Add(vm);
                    }
                    _container.Resolve<BandStatusItemViewModel>().SelectionChanged(-1);
                    responseCallback(new PagedDataResponse<BandStatusItemViewModel>()
                    {
                        Items = list,
                        TotalItemCount = list.Count
                    });
                };

            proxy.GetBandsStatusesAsync(null);

            /*
            proxy.GetBandStatusFilteredCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<BandStatusItemViewModel>();
                    vm.Item = item;
                    vm.DataTable = _dataTable;
                    list.Add(vm);
                }
                _container.Resolve<BandStatusItemViewModel>().SelectionChanged(-1);
                proxy.GetBandStatusFilteredTotalCountAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary());
            };

            proxy.GetBandStatusFilteredTotalCountCompleted += (s, e) =>
            {
                responseCallback(new PagedDataResponse<BandStatusItemViewModel>()
                {
                    Items = list,
                    TotalItemCount = e.Result
                });
            };

            proxy.GetBandStatusFilteredAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary(), pageNumber, _pageSize);
            */
        }
    }
}
