using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataTable;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Authentication;

namespace Finder.BloodDonation.Tabs.BandStatus
{
    public class BandStatusDataTable : BaseDataTable
    {

        public BandStatusDataTable(IUnityContainer container)
            : base(container)
        {
        }

        public override ObservableCollection<IDataTableColumn> GetColumns()
        {
		
			bool isHiddenForUser = !BloodyUser.Current.IsInRole(PermissionNames.HyperAdmin);
		
            return new ObservableCollection<IDataTableColumn>()
             .Add(50)
			 .Add(100, "BandId", "ID lokalizatora", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
.Add(100, "ObjectTypeName", "Typ obiektu", FilterDataType.String, TextAlignment.Left)
.Add(100, "ObjectDisplayName", "Nazwa", FilterDataType.String, TextAlignment.Left)
.Add(100, "ObjectUnitName", "Oddzia�", FilterDataType.String, TextAlignment.Left)
.Add(100, "ObjectUnitLocation", "Lokalizacja sali", FilterDataType.String, TextAlignment.Left, true, false)
.Add(100, "CurrentUnitName", "Aktualna strefa", FilterDataType.String, TextAlignment.Left)
.Add(100, "CurrentUnitLocation", "Lokalizacja aktualnej strefy", FilterDataType.String, TextAlignment.Left, true, false)
.Add(100, "InCUrrentUnit", "Wej�cie do strefy", FilterDataType.DateTime, TextAlignment.Left)
.Add(100, "TopAlarmTypeName", "Najwa�niejsze zdarzenie", FilterDataType.String, TextAlignment.Left, true, false)
.Add(100, "TopAlarmSevertityName", "Waga zdarzenia", FilterDataType.String, TextAlignment.Left, true, false)
.Add(100, "TopAlarmMessage", "Komunikat", FilterDataType.String, TextAlignment.Left, true, false)
.Add(100, "TopAlarmDate", "Data", FilterDataType.DateTime, TextAlignment.Left, true, false)
.Add(100, "Severity1Count", "Liczba ostrze�e�", FilterDataType.Int, TextAlignment.Left)
.Add(100, "Severtity2Count", "Liczba alarm�w", FilterDataType.Int, TextAlignment.Left)
             ;
        }


        public override ISortedFilter GetFilter(IUnityContainer container)
        {
            return new BandStatusFilter(container);
        }

		public override bool StartWithDefaultColumns
        {
            get
            {
                return false;
            }
        }
    }
}
