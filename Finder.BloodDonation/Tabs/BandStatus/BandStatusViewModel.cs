using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Events;
using System.Collections.Generic;
using System.Collections;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Dialogs.Patients;
using Finder.BloodDonation.Dialogs.Helpers;
using Microsoft.Practices.Composite.Presentation.Events;
using System.Diagnostics;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.Dialogs.Bands;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.UsersService;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Common.Authentication;

namespace Finder.BloodDonation.Tabs.BandStatus
{
    [OnCompleted]
    [UIException]
    public class BandStatusViewModel : ViewModelBase, ITabViewModel
    {
        private readonly IWindowsManager _windowsManager;

        public event EventHandler LoadingCompleted = delegate { };
        private IUnit SelectedUnit = null;
        private IUnit _currentUnit;

        private BandStatusDto selectedItem;

        private UsersServiceClient Proxy { get; set; }

        [RaisePropertyChanged]
        public bool RegistrationVisible { get; set; }

        [RaisePropertyChanged]
        public int AdministrationSelectedIndex { get; set; }

        private PatientAssignData _patientAssignData;
        private AssignBandData _assignBandData;

        private PatientInfoDto selected;

        private PatientEditData _patientEditData;

        public RelayCommand CreatePatientInfoCommand { get; set; }
        public RelayCommand AdministrationSelectionChanged { get; set; }

        private IUnitFiltered _details;
        [RaisePropertyChanged]
        public IUnitFiltered Details
        {
            get
            {
                if (_details == null)
                {
                    _details = Container.Resolve<BandStatusDetailsListViewModel>();
                }
                return _details;
            }
            set
            {
                _details = value;
            }
        }

        private void Proxy_AssignBandToObjectCompleted(object sender, AssignBandToObjectCompletedEventArgs e)
        {
            Refresh(_currentUnit);
        }

        public void OnSelectedItemChangedEvent(IItemViewModel<PatientInfoDto> obj)
        {
            /*if (obj == null)
            {
                selectedItem = null;
                selected = null;
            }
            else
            {
                selected = obj.Item;
                SelectedItemChanged();
            }*/
        }

        public UnitsServiceClient Proxys { get; set; }

        public BandStatusViewModel(IUnityContainer container)
            : base(container)
        {
            this.ViewAttached += new EventHandler(ViewModel_ViewAttached);
            EventAggregator.GetEvent<SelectedItemChangedEvent<BandStatusDto>>().Subscribe(OnSelectedItemChangedEvent);

            var viewFactory = container.Resolve<ViewFactory>();
            _windowsManager = this.Container.Resolve<IWindowsManager>();

            CreatePatientInfoCommand = new RelayCommand(OnCreatePatientInfo);
            AdministrationSelectionChanged = new RelayCommand(OnAdministrationSelectionChanged);

            if (_windowsManager != null)
            {
                /*_windowsManager.RegisterWindow(WindowsKeys.EditPatient, "Dodaj",
                    viewFactory.CreateView<EditPatient>(), new Size(400, 430), true);
                _windowsManager.RegisterWindow(WindowsKeys.PatientAssign, "Przypisanie",
                    viewFactory.CreateView<PatientAssign>(), new Size(290, 400), true);
                _windowsManager.RegisterWindow(WindowsKeys.AssignBand, "Przypisanie",
                    viewFactory.CreateView<AssignBand>(), new Size(290, 400), true);*/
            }
            Proxys = ServiceFactory.GetService<UnitsServiceClient>();
            Proxys.AssignPatientToWardRoomCompleted += Proxy_AssignPatientToWardRoomCompleted;
            Proxys.AssignBandToObjectCompleted += Proxy_AssignBandToObjectCompleted;
            Proxy = ServiceFactory.GetService<UsersServiceClient>();
            Proxy.GetSettingsTCompleted += Proxy_GetSettingsTCompleted;
            Proxy.GetSettingsTAsync();

            Proxy = ServiceFactory.GetService<UsersServiceClient>(/*ServicesUri.UsersService*/);
        }

        private void Proxy_AssignPatientToWardRoomCompleted(object sender, AssignPatientToWardRoomCompletedEventArgs e)
        {
            Refresh(_currentUnit);
        }

        public void OnAdministrationSelectionChanged()
        {
            if (selected != null)
            {
                if (AdministrationSelectedIndex == 0)
                {
                    _patientAssignData.AssignWindowType = AssignWindowType.AssignToDepartment;
                    _windowsManager.ShowWindow(WindowsKeys.PatientAssign, _patientAssignData);
                    EventAggregator.GetEvent<CloseWindowEvent>()
                        .Subscribe(CloseAssignWindowEventHandling, ThreadOption.UIThread, true);
                }
                else if (AdministrationSelectedIndex == 3)
                {
                    _patientAssignData.AssignWindowType = AssignWindowType.AssignToRoom;
                    _windowsManager.ShowWindow(WindowsKeys.PatientAssign, _patientAssignData);
                    EventAggregator.GetEvent<CloseWindowEvent>()
                        .Subscribe(CloseAssignWindowEventHandling, ThreadOption.UIThread, true);
                }
                else if (AdministrationSelectedIndex == 5)
                {
                    const byte PATIENT_OBJECT_TYPE = 4;

                    _assignBandData = new AssignBandData();
                    _assignBandData.AssignedBandInfo = new AssignedBandInfoDto();
                    _assignBandData.AssignedBandInfo.ObjectId = (int)selected.Id;
                    _assignBandData.AssignedBandInfo.ObjectType = PATIENT_OBJECT_TYPE;
                    _assignBandData.AssignedBandInfo.OperatorId = BloodyUser.Current.ID;

                    _windowsManager.ShowWindow(WindowsKeys.AssignBand, _assignBandData);
                    EventAggregator.GetEvent<CloseWindowEvent>()
                        .Subscribe(CloseAssignBandWindowEventHandling, ThreadOption.UIThread, true);
                }
            }
        }

        private void CloseAssignWindowEventHandling(string obj)
        {
            //if (_patientAssignData != null && _patientAssignData.Patient != null)
            //    Proxys.AssignPatientToWardRoomAsync(_patientAssignData.Patient);
        }

        private void CloseAssignBandWindowEventHandling(string obj)
        {
            if (_assignBandData != null && _assignBandData.AssignedBandInfo != null)
                Proxys.AssignBandToObjectAsync(_assignBandData.AssignedBandInfo,false);
        }

        public void OnCreatePatientInfo()
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling);

            _patientEditData = new PatientEditData();
            _patientEditData.Patient = new PatientDTO();

            this._windowsManager.ShowWindow(WindowsKeys.EditPatient, _patientEditData);
        }

        private void Proxy_GetSettingsTCompleted(object sender, GetSettingsTCompletedEventArgs e)
        {
            if (e.Result == null)
                return;

            for (int i = 0; i < e.Result.Count; i++)
            {
                if (e.Result[i].Name.Equals("RegistrationWardId"))
                {
                    UnitTypeOrganizer.SetRegistrationUnitId(int.Parse(e.Result[i].Value));
                }
                else if (e.Result[i].Name.Equals("UnassignedPatientsUnitId"))
                {
                    UnitTypeOrganizer.SetUnassignedPatientsUnitId(int.Parse(e.Result[i].Value));
                }
                else if (e.Result[i].Name.Equals("ForceBandAssignment"))
                {
                    UnitTypeOrganizer.SetForceBandAssignment(bool.Parse(e.Result[i].Value));
                }
            }
            if (CheckIfSelectedUnitIsRegistration(_currentUnit))
            {
                RegistrationVisible = true;
            }
            else
            {
                RegistrationVisible = false;
            }
        }

        private bool CheckIfSelectedUnitIsRegistration(IUnit selectedUnit)
        {
            UnitDTO _temp = (UnitDTO)UnitManager.Instance.Units.Where(e => e.ID == selectedUnit.Identity).FirstOrDefault();

            if (_temp == null)
                return false;
            if (_temp.ID == UnitTypeOrganizer.GetRegistartionUnitId())
                return true;

            if (_temp.Parent == null)
                return false;

            UnitDTO _parent = _temp.Parent;

            while (_parent.ID != UnitTypeOrganizer.GetRegistartionUnitId())
            {
                _parent = _parent.Parent;

                if (_parent == null)
                {
                    return false;
                }
            }

            if (_parent == null)
            {
                return false;
            }
            if (_parent.ID == UnitTypeOrganizer.GetRegistartionUnitId())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void OnSelectedItemChangedEvent(IItemViewModel<BandStatusDto> obj)
        {
            if(obj == null)
            {
                selectedItem = null;
            }
            else
            {
                selectedItem = obj.Item;
            }
            SelectedItemChanged();
        }

        private void SelectedItemChanged()
        {

        }
        
        private void ViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public void Refresh(IUnit unit)
        {
            SelectedUnit = unit;
            _currentUnit = unit;

            Details.Refresh(unit);

            if (CheckIfSelectedUnitIsRegistration(SelectedUnit))
                RegistrationVisible = true;
            else
                RegistrationVisible = false;  // Zmieni� na false.
		}
        
        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        public string Title
        {
            get { return ""; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }


        public void CloseWindowEventHandling(string key)
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(CloseWindowEventHandling);
        }
    }
}
