using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.BandStatus
{
    public class BandStatusFilter : SortedFilter
    {
       [RaisePropertyChanged]
public string BandId { get; set; }

[RaisePropertyChanged]
public string ObjectTypeName { get; set; }

[RaisePropertyChanged]
public string ObjectDisplayName { get; set; }

[RaisePropertyChanged]
public string ObjectUnitName { get; set; }

[RaisePropertyChanged]
public string ObjectUnitLocation { get; set; }

[RaisePropertyChanged]
public string CurrentUnitName { get; set; }

[RaisePropertyChanged]
public string CurrentUnitLocation { get; set; }

[RaisePropertyChanged]
public DateTime? InCUrrentUnitFrom { get; set; }
[RaisePropertyChanged]
public DateTime? InCUrrentUnitTo { get; set; }

[RaisePropertyChanged]
public string TopAlarmTypeName { get; set; }

[RaisePropertyChanged]
public string TopAlarmSevertityName { get; set; }

[RaisePropertyChanged]
public string TopAlarmMessage { get; set; }

[RaisePropertyChanged]
public DateTime? TopAlarmDateFrom { get; set; }
[RaisePropertyChanged]
public DateTime? TopAlarmDateTo { get; set; }

[RaisePropertyChanged]
public string Severity1Count { get; set; }

[RaisePropertyChanged]
public string Severtity2Count { get; set; }



        public BandStatusFilter(IUnityContainer container)
            : base(container)
        {

        }
    }
}
