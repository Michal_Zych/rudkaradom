using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Common.Events;
using FinderFX.SL.Core.Authentication;
using System.Globalization;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Authentication;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.Context;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.Common.Layout;
using System.Collections.ObjectModel;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Common.Filters;

namespace Finder.BloodDonation.Tabs.BandStatus
{
    public class BandStatusItemViewModel : ViewModelBase, IItemViewModel<BandStatusDto>
    {
        [RaisePropertyChanged]
        public BandStatusDto Item { get; set; }

        [RaisePropertyChanged]
        public IDataTable DataTable { get; set; }

      
        [RaisePropertyChanged]
        public GenericContextMenuItemsCollection<BandStatusItemViewModel> Context { get; set; }

        public DelegateCommand<object> ItemDoubleClick { get; set; }

        public BandStatusItemViewModel(IUnityContainer container)
            : base(container)
        {
            SetContext();
            ItemDoubleClick = new DelegateCommand<object>(OnItemDoubleClick);
        }

        private void SetContext()
        {
            Context = new BandStatusContextMenu(this, this.Container);
        }
     
        public void SelectionChanged(long id)
        {
            EventAggregator.GetEvent<DetailsItemClickedEvent>().Publish(id);
        }
        public override string ToString()
        {
            return Item.ToString();
        }


        private void OnItemDoubleClick(object obj)
        {

            //MsgBox.Warning("dobule click ");
        }

        internal void OnContextmenuClicked(object obj)
        {
            MsgBox.Warning("Context menu " + obj.ToString());
        }
    }
}
