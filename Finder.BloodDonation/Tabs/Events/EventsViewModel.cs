﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Events;
using System.Collections.Generic;
using System.Collections;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Tabs.EventsDetails;
using Finder.BloodDonation.Common.Filters;

namespace Finder.BloodDonation.Tabs.Events
{
    [OnCompleted]
    [UIException]
    public class EventsViewModel: ViewModelBase, ITabViewModel
    {
        public event EventHandler LoadingCompleted = delegate { };
        private IUnit SelectedUnit = null;

        private ITabViewModel _details;
        [RaisePropertyChanged]
        public ITabViewModel Details
        {
            get
            {
                if(_details == null)
                {
                    _details = Container.Resolve<EventsDetailsViewModel>();
                }

                return _details;
            }
            set
            {
                _details = value;
            }
        }

        public EventsViewModel(IUnityContainer container)
            : base(container)
        {
            this.ViewAttached += new EventHandler(ViewModel_ViewAttached);
        }

        private void ViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public void Refresh(IUnit unit)
        {
            SelectedUnit = unit;
            EventsDetailsDetailsListViewModel.unitToRefreshed = SelectedUnit;
            EventAggregator.GetEvent<EventsFilteredMethodChanger>().Publish(EventsFilteredType.Unit);
            EventAggregator.GetEvent<ChangeEventButtonVisibilityForUnitEvent>().Publish(Visibility.Visible);
        }

        public ViewModelState State
        {
            get 
            { 
                return ViewModelState.Loaded;
            }
        }

        public string Title
        {
            get { return ""; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }
}
