﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.ModelServices;
using Finder.BloodDonation.Model.Domain;
using System.Collections.Generic;
using FinderFX.SL.Core.Communication;
using System.Collections.ObjectModel;

namespace Finder.BloodDonation.Tabs
{
	public class UnitsListViewModel : ViewModelBase, ITabViewModel
	{
		[RaisePropertyChanged]
		public ObservableCollection<UnitDTO> ChildUnits { get; set; }

		public UnitsListViewModel(IUnityContainer container)
			: base(container)
		{
			this.ViewAttached += new EventHandler(UnitsListViewModel_ViewAttached);
		}

		void UnitsListViewModel_ViewAttached(object sender, EventArgs e)
		{

			_state = ViewModelState.Loaded;
			LoadingCompleted(this, EventArgs.Empty);
		}


		#region ITabViewModel Members

		public void Refresh(IUnit unit)
		{
			UnitsServiceClient proxy = ServiceFactory.GetService<UnitsServiceClient>("Finder-BloodDonation-Model-Services-UnitsService.svc");

			proxy.GetChildUnitsCompleted += new EventHandler<GetChildUnitsCompletedEventArgs>(proxy_GetChildUnitsCompleted);
			proxy.GetChildUnitsAsync(unit.Identity);
		}

		void proxy_GetChildUnitsCompleted(object sender, GetChildUnitsCompletedEventArgs e)
		{
			this.ChildUnits = e.Result;
		}

		

		public event EventHandler LoadingCompleted = delegate { };


		private ViewModelState _state = ViewModelState.Normal;
		public ViewModelState State
		{
			get { return _state; }
		}

		#endregion
	}
}
