﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Dialogs.Users;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.PermissionsProxy;
using Finder.BloodDonation.UsersService;
using FinderFX.SL.Core.Authentication;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.Extensions;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;
using Telerik.Windows.Controls;
using ViewModelBase = FinderFX.SL.Core.MVVM.ViewModelBase;
using Finder.BloodDonation.Tabs.Contents;
using Finder.BloodDonation.Settings;

namespace Finder.BloodDonation.Tabs.MenuTools.Externals
{
    [UIException]
    public class AssecoInViewModel: ViewModelBase, ITabViewModel
   {
        private ViewModelState _state = ViewModelState.Loading;


        public AssecoInViewModel(IUnityContainer container)
            : base(container)
        {
            ViewAttached += ViewModel_ViewAttached;
            GetData();
        }

        private void GetData()
        {

        }

        private void ViewModel_ViewAttached(object sender, EventArgs e)
        {
            _state = ViewModelState.Loaded;
            LoadingCompleted(this, EventArgs.Empty);
        }


        public void Refresh(IUnit unit)
        {
           
        }

        public event EventHandler LoadingCompleted = delegate { };

        public ViewModelState State
        {
            get { return _state; }
        }


        public string Title
        {
            get { return "Narzędzia"; }
        }

        public bool ExitButtonVisible
        {
            get { return true; }
        }
    }
}
