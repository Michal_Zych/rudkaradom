﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Common.Events;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Telerik.Windows.Controls;
using Finder.BloodDonation.Tabs.Tools.Transmitters;

namespace Finder.BloodDonation.Tabs.MenuTools.Transmitters
{
     [ViewModel(typeof(TransmittersViewModel))]
    public partial class TransmittersView : UserControl
    {
        public TransmittersView()
        {
            InitializeComponent();
        }
    }
}
