﻿using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataTable;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Filters;

namespace Finder.BloodDonation.Tabs.PatientTests
{
    public class PatientTestDataTable : BaseDataTable
    {

        public PatientTestDataTable(IUnityContainer container)
            : base(container)
        {
        }

        public override ObservableCollection<IDataTableColumn> GetColumns()
        {
            return new ObservableCollection<IDataTableColumn>()
             .Add(50)
			 .Add(100, "ID", "ID", FilterDataType.Int)
.Add(100, "FirstName", "FirstName", FilterDataType.String)
.Add(100, "LastName", "LastName", FilterDataType.String)
.Add(100, "Pesel", "Pesel", FilterDataType.String)
.Add(100, "DepartmentId", "DepartmentId", FilterDataType.Int)
.Add(100, "DepartmentName", "DepartmentName", FilterDataType.String)
.Add(100, "RoomId", "RoomId", FilterDataType.Int)
.Add(100, "RoomName", "RoomName", FilterDataType.String)
.Add(100, "BandId", "BandId", FilterDataType.Short)
.Add(100, "InHospitalDate", "InHospitalDate", FilterDataType.DateTime)
.Add(100, "IsActive", "IsActive", FilterDataType.Bool)
             ;
        }


        public override ISortedFilter GetFilter(IUnityContainer container)
        {
            return new PatientTestFilter(container);
        }

		public override bool StartWithDefaultColumns
        {
            get
            {
                return false;
            }
        }
    }
}
