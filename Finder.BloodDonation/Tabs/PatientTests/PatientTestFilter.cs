﻿using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.PatientTests
{
    public class PatientTestFilter : SortedFilter
    {
       [RaisePropertyChanged]
public string ID { get; set; }

[RaisePropertyChanged]
public string FirstName { get; set; }

[RaisePropertyChanged]
public string LastName { get; set; }

[RaisePropertyChanged]
public string Pesel { get; set; }

[RaisePropertyChanged]
public string DepartmentId { get; set; }

[RaisePropertyChanged]
public string DepartmentName { get; set; }

[RaisePropertyChanged]
public string RoomId { get; set; }

[RaisePropertyChanged]
public string RoomName { get; set; }

[RaisePropertyChanged]
public string BandId { get; set; }

[RaisePropertyChanged]
public DateTime? InHospitalDateFrom { get; set; }
[RaisePropertyChanged]
public DateTime? InHospitalDateTo { get; set; }

[RaisePropertyChanged]
public bool? IsActive { get; set; }



        public PatientTestFilter(IUnityContainer container)
            : base(container)
        {

        }
    }
}
