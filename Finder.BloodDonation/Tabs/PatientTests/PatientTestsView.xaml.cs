﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Tools;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Dialogs.Patients;
using Microsoft.Practices.Composite.Events;
using Finder.BloodDonation.Common.Events;
using Microsoft.Practices.Composite.Presentation.Events;
using Finder.BloodDonation.Common.Layout;

namespace Finder.BloodDonation.Tabs.PatientTests
{
    [ViewModel(typeof(PatientTestViewModel))]
    public partial class PatientTestView : UserControl
    {

        public PatientTestView()
        {
            InitializeComponent();
        }

        private void btnAdministration_Click(object sender, RoutedEventArgs e)
        {
            cbAdministration.IsDropDownOpen = true;
        }

        private void cbAdministration_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (cbAdministration.SelectedIndex != -1)
            {
                MsgBox.Warning(cbAdministration.SelectedIndex.ToString());
                cbAdministration.SelectedIndex = -1;
            }
        }
    }
}
