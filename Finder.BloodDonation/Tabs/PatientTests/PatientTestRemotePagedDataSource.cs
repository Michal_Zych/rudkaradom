﻿using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.PatientTests
{
    public class PatientTestRemotePagedDataSource: IPagedDataSource<PatientTestItemViewModel>
    {
        protected IUnityContainer _container;
        protected int _pageSize;
        protected ISortedFilter _filter;
        protected IUnitDataClient _dataClient;
		protected int unitId;

        public PatientTestRemotePagedDataSource(IUnityContainer container, int pageSize, ISortedFilter filter, IUnitDataClient dataClient, int unitId)
        {
            _container = container;
            _pageSize = pageSize;
            _filter = filter;
            _dataClient = dataClient;
			this.unitId = unitId;
        }


        public void FetchData(int pageNumber, Action<PagedDataResponse<PatientTestItemViewModel>> responseCallback)
        {
            var proxy = _dataClient.GetProxy();
            var list = new List<PatientTestItemViewModel>();
            /*
            proxy.GetPatientTestFilteredCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<PatientTestViewModel>();
                    vm.Item = item;
                    list.Add(vm);
                }
                _container.Resolve<PatientTestViewModel>().SelectionChanged(-1);
                proxy.GetPatientTestFilteresdTotalCountAsync((Dictionary<string, string>)_filter.GetFilterDictionary());
            };
            
            proxy.GetPatientTestFilteresdTotalCountCompleted += (s, e) =>
            {
                responseCallback(new PagedDataResponse<PatientTestViewModel>()
                {
                    Items = list,
                    TotalItemCount = e.Result
                });
            };

            proxy.GetPatientTestFilteredAsync((Dictionary<string, string>)_filter.GetFilterDictionary(), pageNumber, _pageSize, unitId , 0);
            */ 
        }

    }
}
