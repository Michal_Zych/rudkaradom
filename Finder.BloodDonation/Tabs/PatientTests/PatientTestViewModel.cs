﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Events;
using System.Collections.Generic;
using System.Collections;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Dialogs.Patients;
using Finder.BloodDonation.Dialogs.Helpers;
using Microsoft.Practices.Composite.Presentation.Events;
using System.Diagnostics;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.Dialogs.Bands;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.UsersService;
using Finder.BloodDonation.Tabs.MenuAdministration.Settings;

namespace Finder.BloodDonation.Tabs.PatientTests
{
    [OnCompleted]
    [UIException]
    public class PatientTestViewModel : ViewModelBase, ITabViewModel
    {
        private PatientEditData _patientEditData;
        private PatientConfigurationData _patientConfigurationData;
        private PatientAssignData _patientAssignData;

        private readonly IWindowsManager _windowsManager;

        public event EventHandler LoadingCompleted = delegate { };
        private IUnit SelectedUnit = null;
        private IUnit _currentUnit;

        [RaisePropertyChanged]
        public bool RegistrationVisible { get; set; }

        BandEditData bandEditData;

        private PatientDTO selectedItem;

        [RaisePropertyChanged]
        public int AdministrationSelectedIndex { get; set; }

        public RelayCommand RegisterPerson { get; set; }
        public RelayCommand EditPatients { get; set; }
        public RelayCommand BandTestCommand { get; set; }
        public RelayCommand ConfigurationCommand { get; set; }
        public RelayCommand AdministrationSelectionChanged { get; set; }
        public RelayCommand DetailsCommand { get; set; }

        private UsersServiceClient Proxy { get; set; }

        private IUnitFiltered _details;
        [RaisePropertyChanged]
        public IUnitFiltered Details
        {
            get
            {
                if (_details == null)
                {
                    _details = Container.Resolve<PatientTestDetailsListViewModel>();
                }
                return _details;
            }
            set
            {
                _details = value;
            }
        }

        [RaisePropertyChanged]
        public PatientDTO Patient { get; private set; }

        public PatientTestViewModel(IUnityContainer container)
            : base(container)
        {
            this.ViewAttached += new EventHandler(ViewModel_ViewAttached);
            EventAggregator.GetEvent<SelectedItemChangedEvent<PatientDTO>>().Subscribe(OnSelectedItemChangedEvent);
            EventAggregator.GetEvent<EditPatientEvent>().Subscribe(OnEditPatientEvent);

            var viewFactory = container.Resolve<ViewFactory>();
            RegisterPerson = new RelayCommand(OnRegisterPerson);
            BandTestCommand = new RelayCommand(OnOpenTestCreateBand);
            EditPatients = new RelayCommand(OnEditPatient);
            ConfigurationCommand = new RelayCommand(OnConfigurationPatient);
            AdministrationSelectionChanged = new RelayCommand(OnAdministrationSelectionChanged);
            DetailsCommand = new RelayCommand(OnDetailsCommand);

            _windowsManager = this.Container.Resolve<IWindowsManager>();
            _windowsManager.RegisterWindow(WindowsKeys.EditPatient, "Edycja", viewFactory.CreateView<EditPatient>(), new Size(400, 430), true);
            _windowsManager.RegisterWindow(WindowsKeys.CreateBand, "", viewFactory.CreateView<BandEdit>(), new Size(750, 1000), true);
            _windowsManager.RegisterWindow(WindowsKeys.ConfigurationPatient, "Edycja", viewFactory.CreateView<ConfigurationPatient>(), new Size(750, 1200), true);
            _windowsManager.RegisterWindow(WindowsKeys.PatientAssign, "Przypisanie", viewFactory.CreateView<PatientAssign>(), new Size(290, 400), true);
            _windowsManager.RegisterWindow(WindowsKeys.SettingsView, "Ustawienia", viewFactory.CreateView<SettingsView>(), new Size(400, 400), true);

            RegistrationVisible = false;
            Proxy = ServiceFactory.GetService<UsersServiceClient>(/*ServicesUri.UsersService*/);
        }


        public void OnDetailsCommand()
        {
            _windowsManager.ShowWindow(WindowsKeys.SettingsView);


            //Proxy.GetSettingsTCompleted += Proxy_GetSettingsTCompleted;
            //Proxy.GetSettingsTAsync();
        }

        public void Proxy_GetSettingsTCompleted(object sender, GetSettingsTCompletedEventArgs e)
        {
            ///TODO: Not implementation - Test magic function.
        }

        public void OnAdministrationSelectionChanged()
        {
            /// Niezgadzające się adresy oddziałów.
            if (selectedItem != null)
            {
                _patientAssignData = new PatientAssignData();
                _patientAssignData.Patient = selectedItem;

                if (AdministrationSelectedIndex == 0)
                {
                    _patientAssignData.AssignWindowType = AssignWindowType.AssignToDepartment;
                    _windowsManager.ShowWindow(WindowsKeys.PatientAssign, _patientAssignData);

                }
                else if (AdministrationSelectedIndex == 3)
                {
                    _patientAssignData.AssignWindowType = AssignWindowType.AssignToRoom;
                    _windowsManager.ShowWindow(WindowsKeys.PatientAssign, _patientAssignData);
                }
            }
        }

        public void OnConfigurationPatient()
        {
            // TODO: Pobrać z bazy danych według zaznaczenia.
            _patientConfigurationData = new PatientConfigurationData()
            {
                PatientConfigData = new PatientConfigurationDTO()
            };

            _windowsManager.ShowWindow(WindowsKeys.ConfigurationPatient, _patientConfigurationData);
        }

        public void OnSelectedItemChangedEvent(IItemViewModel<PatientDTO> obj)
        {
            if (obj == null)
            {
                selectedItem = null;
            }
            else
            {
                selectedItem = obj.Item;
            }
            SelectedItemChanged();
        }

        private void SelectedItemChanged()
        {

        }

        private void ViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public void Refresh(IUnit unit)
        {
            SelectedUnit = unit;
            _currentUnit = unit;

            Details.Refresh(unit);

            if (CheckIfSelectedUnitIsRegistration(SelectedUnit))
                RegistrationVisible = true;
            else
                RegistrationVisible = false;

        }

        private bool CheckIfSelectedUnitIsRegistration(IUnit selectedUnit)
        {
            return true;
        }

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        public string Title
        {
            get { return ""; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }

        public void OnEditPatientCommand(object obj)
        {
            this.EventAggregator.GetEvent<EditPatientEvent>().Publish(this.Patient);
        }

        public void OnEditPatientEvent(PatientDTO patient)
        {
            _patientEditData = new PatientEditData
            {
                Patient = patient
            };

            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling, ThreadOption.UIThread, true);
            this._windowsManager.ShowWindow(WindowsKeys.EditPatient, _patientEditData);
        }

        public void OnOpenTestCreateBand()
        {
            bandEditData = new BandEditData()
            {
                band = new BandDTO()
            };

            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(TestCloseEventHandling);
            this._windowsManager.ShowWindow(WindowsKeys.CreateBand, bandEditData);
        }

        public void TestCloseEventHandling(string obj)
        {

        }

        private void CloseWindowEventHandling(string key)
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(CloseWindowEventHandling);

            if (key.Equals(WindowsKeys.EditPatient))
            {
                if (_patientEditData.Canceled)
                {

                }
                else
                {
                    EditPatient(_patientEditData);
                }
            }
        }

        private void EditPatient(PatientEditData _patientEditData)
        {
            // zapis do bazy danych.
        }

        private void OnRegisterPerson()
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling, ThreadOption.UIThread, true);
            _patientEditData = new PatientEditData();
            _patientEditData.Patient = new PatientDTO();

            this._windowsManager.ShowWindow(WindowsKeys.EditPatient, _patientEditData);
        }

        private void OnEditPatient()
        {
           // _patientEditData = new PatientEditData()
           // {
           //     Patient = selectedItem
           //  };
                
           //  this._windowsManager.ShowWindow(WindowsKeys.EditPatient, _patientEditData);
        }
    }
}
