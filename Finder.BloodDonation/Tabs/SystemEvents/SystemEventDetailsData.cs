﻿using Finder.BloodDonation.Model.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.SystemEvents
{
    public class SystemEventDetailsData
    {
        public LogEntryDto LogEntry { get; set; }
    }
}
