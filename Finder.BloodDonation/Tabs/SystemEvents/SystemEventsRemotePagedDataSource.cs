using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.SystemEvents
{
    public class SystemEventsRemotePagedDataSource: IPagedDataSource<SystemEventsItemViewModel>
    {
        protected IUnityContainer _container;
        protected int _pageSize;
        protected IDataTable _dataTable;
        protected IUnitDataClient _dataClient;
        protected bool _isArchiveSearch;

        public SystemEventsRemotePagedDataSource(IUnityContainer container, int pageSize, IDataTable dataTable, IUnitDataClient dataClient, bool isArchiveSearch)
        {
            _container = container;
            _pageSize = pageSize;
            _dataTable = dataTable;
            _dataClient = dataClient;
            _isArchiveSearch = isArchiveSearch;
        }


		 public void FetchData(int pageNumber, Action<PagedDataResponse<SystemEventsItemViewModel>> responseCallback)
        {
            var proxy = _dataClient.GetProxy();
            var list = new List<SystemEventsItemViewModel>();
            
            proxy.GetSystemEventsFilteredCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<SystemEventsItemViewModel>();
                    vm.Item = item;
                    vm.DataTable = _dataTable;
                    list.Add(vm);
                }
                _container.Resolve<SystemEventsItemViewModel>().SelectionChanged(-1);

                Dictionary<string, string> filteredDictionary = (Dictionary<string, string>)_dataTable.GetFilterDictionary();

                if (_isArchiveSearch)
                    filteredDictionary["FromArchive"] = "1";
                else
                    filteredDictionary["FromArchive"] = "0";

                proxy.GetSystemEventsFilteredTotalCountAsync(filteredDictionary);
            };

            proxy.GetSystemEventsFilteredTotalCountCompleted += (s, e) =>
            {
                responseCallback(new PagedDataResponse<SystemEventsItemViewModel>()
                {
                    Items = list,
                    TotalItemCount = e.Result
                });
            };

            Dictionary<string, string> fD = (Dictionary<string, string>)_dataTable.GetFilterDictionary();

            if (_isArchiveSearch)
                fD["FromArchive"] = "1";
            else
                fD["FromArchive"] = "0";

            proxy.GetSystemEventsFilteredAsync(fD, pageNumber, _pageSize);
            
        }
    }
}
