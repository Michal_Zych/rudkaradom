﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Tabs.SystemEvents.Dialogs;

namespace Finder.BloodDonation.Tabs.SystemEvents
{
    [ViewModel(typeof(SystemEventDialogModel))]
    public partial class SystemEventDialog : UserControl
    {
        public SystemEventDialog()
        {
            InitializeComponent();
        }
    }
}
