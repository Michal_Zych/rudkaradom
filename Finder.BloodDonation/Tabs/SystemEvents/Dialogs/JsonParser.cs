﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Tabs.SystemEvents.Dialogs
{
    public class JsonParser
    {
        const string ASCII_OPEN_BRACKET = "###1";
        const string ASCII_END_BRACKET = "###2";

        private string inputJson;

        public JsonParser() { }

        public string ConvertToString(string json)
        {
            string input = json;
            StringBuilder output = new StringBuilder();

            input = input.Replace("\",\"", "\n ,");
            input = input.Replace("\":\"", "=");
            input = input.Replace("[{", "\n");
            input.Insert(0, "\r");

            return input;
        }
    }
}
