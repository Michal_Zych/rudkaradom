﻿using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Unity;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace Finder.BloodDonation.Tabs.SystemEvents.Dialogs
{
    public class SystemEventDialogModel : FinderFX.SL.Core.MVVM.ViewModelBase, IDialogWindow
    {
        private SystemEventDetailsData _data;
        private LogEntryExtendedDto _extendedData;

        private UnitsServiceClient Proxy
        {
            get;
            set;
        }

        public RelayCommand CloseCommand { get; set; }

        [RaisePropertyChanged]
        public string Title { get; set; }

        [RaisePropertyChanged]
        public string EventData { get; set; }

        [RaisePropertyChanged]
        public string EventAction { get; set; }

        [RaisePropertyChanged]
        public string EventLocation { get; set; }

        [RaisePropertyChanged]
        public string ObjectType { get; set; }

        [RaisePropertyChanged]
        public string ObjectDisplayName { get; set; }

        [RaisePropertyChanged]
        public string ObjectId { get; set; }

        [RaisePropertyChanged]
        public string OperatorDisplayData { get; set; }

        [RaisePropertyChanged]
        public string EventDetailsData { get; set; }

        [RaisePropertyChanged]
        public string Reason { get; set; }

        public string WindowKey
        {
            get { return WindowsKeys.SystemEventDialog; }
        }

        public object Data
        {
            set
            {
                _data = value as SystemEventDetailsData;

                if (_data != null)
                {
                    Title = "Szczegóły";
                    Proxy.GetSystemEventStringsAsync(_data.LogEntry.Id);
                    EventAction = _data.LogEntry.TypeDescription;
                    EventLocation = _data.LogEntry.UnitLocation;
                    EventData = _data.LogEntry.Date.ToLocalTime().ToString("yyyy-MM-dd HH:mm:ss");
                    ObjectType = _data.LogEntry.ObjectTypeDescription;
                    ObjectDisplayName = _data.LogEntry.ObjectName;
                    ObjectId = " ( " + _data.LogEntry.ObjectId + " ) ";
                    OperatorDisplayData = _data.LogEntry.OperatorName + " ( " + _data.LogEntry.OperatorId + " )";
                    Reason = _data.LogEntry.Reason;
                }
            }
        }

        public SystemEventDialogModel(IUnityContainer container)
            : base(container)
        {
            Proxy = ServiceFactory.GetService<UnitsServiceClient>(/*ServicesUri.UsersService*/);
            Proxy.GetSystemEventStringsCompleted += Proxy_GetSystemEventStringsCompleted;
            CloseCommand = new RelayCommand(OnCloseDialog);
        }

        private void OnCloseDialog()
        {
            this.Container.Resolve<IWindowsManager>().CloseWindow(WindowsKeys.SystemEventDialog);
        }

        public void Proxy_GetSystemEventStringsCompleted(object sender, GetSystemEventStringsCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                _extendedData = e.Result;

                EventDetailsData = new JsonParser().ConvertToString(_extendedData.EventData);
            }
        }
    } 
}
