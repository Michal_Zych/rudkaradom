using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.SystemEvents
{
    public class SystemEventsFilter : SortedFilter
    {
       [RaisePropertyChanged]
public string Id { get; set; }

[RaisePropertyChanged]
public DateTime? DateFrom { get; set; }
[RaisePropertyChanged]
public DateTime? DateTo { get; set; }

[RaisePropertyChanged]
public string TypeDescription { get; set; }

[RaisePropertyChanged]
public string UnitLocation { get; set; }

[RaisePropertyChanged]
public string ObjectTypeDescription { get; set; }

[RaisePropertyChanged]
public string ObjectName { get; set; }

[RaisePropertyChanged]
public string OperatorName { get; set; }

[RaisePropertyChanged]
public string EventData { get; set; }

[RaisePropertyChanged]
public string Reason { get; set; }



        public SystemEventsFilter(IUnityContainer container)
            : base(container)
        {

        }
    }
}
