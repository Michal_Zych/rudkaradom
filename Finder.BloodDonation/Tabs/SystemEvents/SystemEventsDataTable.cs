using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataTable;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Common.Authentication;

namespace Finder.BloodDonation.Tabs.SystemEvents
{
    public class SystemEventsDataTable : BaseDataTable
    {

        public SystemEventsDataTable(IUnityContainer container)
            : base(container)
        {
        }

        public override ObservableCollection<IDataTableColumn> GetColumns()
        {
		
			bool isHiddenForUser = !BloodyUser.Current.IsInRole(PermissionNames.HyperAdmin);
		
            return new ObservableCollection<IDataTableColumn>()
             .Add(50)
			 .Add(100, "Id", "ID", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "Date", "Data", FilterDataType.DateTime, TextAlignment.Left)
             .Add(100, "TypeDescription", "Zdarzenie", FilterDataType.String)
             .Add(100, "UnitLocation", "Strefa", FilterDataType.String)
             .Add(100, "ObjectTypeDescription", "Typ obiektu", FilterDataType.String)
             .Add(100, "ObjectName", "Nazwa", FilterDataType.String)
             .Add(100, "OperatorName", "Operator", FilterDataType.String)
             .Add(100, "EventData", "Szczegóły zmiany", FilterDataType.String)
             .Add(100, "Reason", "Przyczyna zmiany", FilterDataType.String)
             ;
        }


        public override ISortedFilter GetFilter(IUnityContainer container)
        {
            return new SystemEventsFilter(container);
        }

		public override bool StartWithDefaultColumns
        {
            get
            {
                return false;
            }
        }
    }
}
