﻿using Finder.BloodDonation.Model.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.AlarmsLocalization.Dialogs
{
    public class EditDetailsAlarmData
    {
        public IEnumerable<int> AlarmsToClose;

        public int? AlarmId;
        public string AlarmObjectName;
        public string AlarmTypeDescription;
        public string AlarmUnitName;
        public DateTime? AlarmDate;
        public int AlarmSeverity;

        public string AlarmCloseComment;
        public bool Canceled = true;
    }
}
