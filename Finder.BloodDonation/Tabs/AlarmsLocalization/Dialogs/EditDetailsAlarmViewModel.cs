﻿using Finder.BloodDonation.AlarmsService;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Converters;
using Finder.BloodDonation.Dialogs.Base;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.LanguageHelpher;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Tools;
using FinderFX.SL.Core.Authentication;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace Finder.BloodDonation.Tabs.AlarmsLocalization.Dialogs
{
    //Dokończyć!
    [OnCompleted]
    [UIException]
    public class EditDetailsAlarmViewModel : ViewModelBase, IDialogWindow
    {
        private const int WarningSeverity = 1;

        private EditDetailsAlarmData _data = null;

        [RaisePropertyChanged]
        public string AlarmId { get; set; }

        [RaisePropertyChanged]
        public Visibility AlarmIdVisibility { get; set; }

        [RaisePropertyChanged]
        public string AlarmObjectName { get; set; }

        [RaisePropertyChanged]
        public string AlarmTypeDescription { get; set; }

        [RaisePropertyChanged]
        public DateTime AlarmDate { get; set; }

        [RaisePropertyChanged]
        public string AlarmUnitName { get; set; }

        [RaisePropertyChanged]
        public Visibility SingleAlarmVisibility { get; set; }

        [RaisePropertyChanged]
        public Visibility MultiAlarmVisibility { get; set; }

        [RaisePropertyChanged]
        public string AlarmIds { get; set; }

        [RaisePropertyChanged]
        public string MultiAlarmLabel { get; set; }

        [RaisePropertyChanged]
        public string AlarmCloseComment { get; set; }

        [RaisePropertyChanged]
        public Brush AlarmColor { get; set; }

        [RaisePropertyChanged]
        public string CloseAlarmButtonLabel { get; set; }


        [RaisePropertyChanged]
        public DelegateCommand<object> CloseAlarmClicked { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> CancelClicked { get; set; }


        public EditDetailsAlarmViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            CloseAlarmClicked = new DelegateCommand<object>(CloseAlarmClicked_Command);
            CancelClicked = new DelegateCommand<object>(CancelClicked_Command);
        }

        public void CancelClicked_Command(object obj)
        {
            _data.Canceled = true;
            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }


        public void CloseAlarmClicked_Command(object o)
        {
            _data.AlarmCloseComment = AlarmCloseComment;
            _data.Canceled = false;
            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }
        
        public string WindowKey
        {
            get { return WindowsKeys.CloseAlarmDialog; }
        }

        public object Data
        {
            set
            {
                _data = (EditDetailsAlarmData)value;
                SetDataToFields();
            }
        }

        private void SetDataToFields()
        {
            AlarmCloseComment = "";
            _data.AlarmCloseComment = AlarmCloseComment;

            AlarmColor = LocalizationAlarmColorConverter.AlarmBrush;

            bool isAdmin = BloodyUser.Current.IsInRole(PermissionNames.HyperAdmin);

            AlarmIdVisibility = isAdmin ? Visibility.Visible : Visibility.Collapsed;
            SetVisibilityForSingleAlarm(_data.AlarmId.HasValue);

            if (_data.AlarmId.HasValue)
            {
                AlarmId = _data.AlarmId.Value.ToString();
                AlarmObjectName = _data.AlarmObjectName;
                AlarmTypeDescription = _data.AlarmTypeDescription;
                AlarmDate = _data.AlarmDate.HasValue ? _data.AlarmDate.Value : DateTime.MinValue;
                AlarmUnitName = _data.AlarmUnitName;
                if (_data.AlarmSeverity == WarningSeverity) 
                {
                    AlarmColor = LocalizationAlarmColorConverter.WarningBrush;
                    CloseAlarmButtonLabel = "Zamknij ostrzeżenie";
                }
                else{
                    CloseAlarmButtonLabel = "Zamknij alarm";
                }
            }
            else
            {
                AlarmId = null;
                AlarmIds = _data.AlarmsToClose.ToCommaSeparatedValues();
                MultiAlarmLabel = String.Format("Liczba alarmów/ostrzeżeń\ndo zamknięcia: {0}", _data.AlarmsToClose == null ? 0 : _data.AlarmsToClose.Count());
                CloseAlarmButtonLabel = "Zamknij alarmy";
            }
        }

        private void SetVisibilityForSingleAlarm(bool singleAlarm)
        {
            if (singleAlarm)
            {
                SingleAlarmVisibility = Visibility.Visible;
                MultiAlarmVisibility = Visibility.Collapsed;
            }
            else
            {
                SingleAlarmVisibility = Visibility.Collapsed;
                MultiAlarmVisibility = Visibility.Visible;
            }
        }

    }
}
