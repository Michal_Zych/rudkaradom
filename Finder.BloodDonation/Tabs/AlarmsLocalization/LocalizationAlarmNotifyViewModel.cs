﻿using System;
using System.Net;
using System.Linq;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Common.Events;
using System.Collections.ObjectModel;
using System.Windows.Media.Animation;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using System.Collections.Generic;
using System.Collections;
using Finder.BloodDonation.Tools;
using System.Windows.Controls;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Common.Layout;
using System.Windows;
using Microsoft.Practices.Composite.Presentation.Events;
using Finder.BloodDonation.Tabs.AlarmsLocalization.Dialogs;
using GalaSoft.MvvmLight.Command;

namespace Finder.BloodDonation.Tabs.AlarmsLocalization
{
    [OnCompleted]
    [UIException]
    public class LocalizationAlarmNotifyViewModel : ViewModelBase
    {
        [RaisePropertyChanged]
        public ObservableCollection<LocalizationAlarmRowViewModel> Alarms{get;set;}

        [RaisePropertyChanged]
        public bool ShowPopup { get; set; }

        public RelayCommand CloseWindow { get; set; }

        public DelegateCommand<object> CloseAlarms { get; set; }

        private AlarmsServiceClient LoadService
        {
            get
            {
                AlarmsServiceClient proxy = ServiceFactory.GetService<AlarmsServiceClient>(ServicesUri.AlarmsService);
                proxy.LocalizationCloseAlarmsCompleted += proxy_LocalizationCloseAlarmsCompleted;
                return proxy;
            }
        }

        

        private IWindowsManager _windowsManager = null;
        public LocalizationAlarmNotifyViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            EventAggregator.GetEvent<LocalizationAlarmsReceived>().Subscribe(LocalizationAlarmsReceived_Event);
            EventAggregator.GetEvent<LocalizationAlarmsClosed>().Subscribe(LocalizationAlarmsClosed_Event);

            CloseAlarms = new DelegateCommand<object>(CloseAlarms_Command);
            CloseWindow = new RelayCommand(OnClosePopup);
            ShowPopup = true;

            _windowsManager = Container.Resolve<IWindowsManager>();
            var viewFactory = Container.Resolve<ViewFactory>();

            _windowsManager.RegisterWindow(WindowsKeys.CloseAlarmDialog, "", viewFactory.CreateView<EditDetailsAlarm>(), new Size(420, 300), false, true);
        }

        public void OnClosePopup()
        {
            Container.Resolve<IWindowsManager>().CloseWindow(WindowsKeys.LocalizationAlarmsPopup);
        }

        private void proxy_LocalizationCloseAlarmsCompleted(object sender, LocalizationCloseAlarmsCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                EventAggregator.GetEvent<LocalizationAlarmsClosed>().Publish(e.Result.ToString());
            }
        }

        public void LocalizationAlarmsClosed_Event(string IdsString)
        {
            var list = new List<LocalizationAlarmDto>();
             var ids = IdsString.Split(',');

            foreach(var a in Alarms)
            {
                if(!ids.Contains(a.Alarm.AlarmId.ToString()))
                {
                    list.Add(a.Alarm);
                }
            }
            EventAggregator.GetEvent<LocalizationAlarmsReceived>().Publish(list);
        }

        public void LocalizationAlarmsReceived_Event(List<LocalizationAlarmDto> obj)
        {
            ObservableCollection<LocalizationAlarmRowViewModel> r = null;
            if (obj != null)
            {
                r = new ObservableCollection<LocalizationAlarmRowViewModel>();
                for (int i = 0; i < obj.Count; i++)
                {
                    r.Add(new LocalizationAlarmRowViewModel(Container) { Alarm = obj[i] });
                }
            }


           IList<int> selected = new List<int>();

           if(LocalizationAlarmNotify.AlarmsDataGrid != null)
            {
                foreach(var item in LocalizationAlarmNotify.AlarmsDataGrid.SelectedItems)
                {
                    selected.Add(((LocalizationAlarmRowViewModel)item).Alarm.AlarmId);
                }
               
            }
            Alarms = r;

            if (LocalizationAlarmNotify.AlarmsDataGrid != null)
            {
                LocalizationAlarmNotify.AlarmsDataGrid.SelectedItems.Clear();
                foreach (var row in Alarms)
                {
                    if (selected.Contains(row.Alarm.AlarmId))
                    {
                        LocalizationAlarmNotify.AlarmsDataGrid.SelectedItems.Add(row);
                    }
                }
            }

            if (Alarms != null && Alarms.Count > 0 && Bootstrapper.UserMonitoringState && ShowPopup)
            {
                EventAggregator.GetEvent<ShowWindow>().Publish(WindowsKeys.LocalizationAlarmsPopup);
            }
        }

       
        private EditDetailsAlarmData editAlarmsData;
        public void CloseAlarms_Command(object o)
        {
            if (LocalizationAlarmNotify.AlarmsDataGrid == null || LocalizationAlarmNotify.AlarmsDataGrid.SelectedItems.Count == 0)
            {
                MsgBox.Warning("Nie wybrano alarmu do zamknięcia.");
            }
            else
            {
                editAlarmsData = new EditDetailsAlarmData();
                var list = new List<int>();
                if(LocalizationAlarmNotify.AlarmsDataGrid.SelectedItems.Count == 1)
                {
                    SetEditAlarmData(((LocalizationAlarmRowViewModel)LocalizationAlarmNotify.AlarmsDataGrid.SelectedItems[0]).Alarm, editAlarmsData);
                    list.Add(editAlarmsData.AlarmId.Value);
                }
                else
                {
                    foreach(LocalizationAlarmRowViewModel a in LocalizationAlarmNotify.AlarmsDataGrid.SelectedItems)
                    {
                        list.Add(a.Alarm.AlarmId);
                    }
                }
                editAlarmsData.AlarmsToClose  = list;

                EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling, ThreadOption.UIThread, true);
                _windowsManager.ShowWindow(WindowsKeys.CloseAlarmDialog, editAlarmsData);

            }
        }

        private void SetEditAlarmData(LocalizationAlarmDto alarm, EditDetailsAlarmData data)
        {
            data.AlarmId = alarm.AlarmId;
            data.AlarmObjectName = alarm.ObjectName;
            data.AlarmTypeDescription = alarm.AlarmDescription;
            data.AlarmSeverity = alarm.Severity;
            data.AlarmUnitName = alarm.AlarmLocation;
            data.AlarmDate = alarm.AlarmDate;
        }

        private void CloseWindowEventHandling(string key)
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(CloseWindowEventHandling);
            if(key == WindowsKeys.CloseAlarmDialog)
            {
                if(!editAlarmsData.Canceled)
                {
                    LoadService.LocalizationCloseAlarmsAsync(editAlarmsData.AlarmsToClose.ToCommaSeparatedValues(), editAlarmsData.AlarmCloseComment);
                    editAlarmsData = null;
                }
            }
        }

    }
}
