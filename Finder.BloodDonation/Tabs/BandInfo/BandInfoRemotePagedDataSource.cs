using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.BandInfo
{
    public class BandInfoRemotePagedDataSource: IPagedDataSource<BandInfoItemViewModel>
    {
        protected IUnityContainer _container;
        protected int _pageSize;
        protected IDataTable _dataTable;
        protected IUnitDataClient _dataClient;

        public BandInfoRemotePagedDataSource(IUnityContainer container, int pageSize, IDataTable dataTable, IUnitDataClient dataClient)
        {
            _container = container;
            _pageSize = pageSize;
            _dataTable = dataTable;
            _dataClient = dataClient;
        }


		 public void FetchData(int pageNumber, Action<PagedDataResponse<BandInfoItemViewModel>> responseCallback)
        {
            var proxy = _dataClient.GetProxy();
            var list = new List<BandInfoItemViewModel>();
            
            proxy.GetBandsInfoFilteredCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<BandInfoItemViewModel>();
                    vm.Item = item;
                    vm.DataTable = _dataTable;
                    list.Add(vm);
                }
                _container.Resolve<BandInfoItemViewModel>().SelectionChanged(-1);
                proxy.GetBandsInfoFilteredTotalCountAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary());
            };

            proxy.GetBandsInfoFilteredTotalCountCompleted += (s, e) =>
            {
                responseCallback(new PagedDataResponse<BandInfoItemViewModel>()
                {
                    Items = list,
                    TotalItemCount = e.Result
                });
            };

            proxy.GetBandsInfoFilteredAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary(), pageNumber, _pageSize);
            
        }
    }
}
