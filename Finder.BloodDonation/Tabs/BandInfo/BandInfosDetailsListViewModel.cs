using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.DynamicData.Paging;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Common.Events;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using Finder.BloodDonation.Filters;
using System.Windows.Input;
using Finder.BloodDonation.DynamicData.Memory;
using Finder.BloodDonation.DynamicData.DBs;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.Tabs.BandInfo;
using Finder.BloodDonation.Common.Filters;

namespace Finder.BloodDonation.Tabs.BandInfos
{
    [OnCompleted]
    [UIException]
    public class BandInfosDetailsListViewModel : ViewModelBase, IUnitFiltered, IUnitDataClient
    {

        private int _pageSize = 0;
        private IUnit _currentUnit;
		
        [RaisePropertyChanged]
        public ObservableCollection<GridLength> ColumnWidths { get; set; }

        public int UnitId
        {
            get
            {
                return _currentUnit.Identity;
            }
        }
        public int SelectionSource
        {
            get
            {
                return _currentUnit.SelectionSource;
            }
        }

        private IFilter _filterHelper;
        [RaisePropertyChanged]
        public IFilter FilterHelper
        {
            get
            {
                if (_filterHelper == null)
                    _filterHelper = null;

                return _filterHelper;
            }
            private set
            {
                _filterHelper = value;
            }
        }

        private BandInfoItemViewModel _selectedItem;
        [RaisePropertyChanged]
        public BandInfoItemViewModel SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;
                EventAggregator.GetEvent<SelectedItemChangedEvent<BandInfoDto>>().Publish(_selectedItem);
            }
        }

        [RaisePropertyChanged]
        public ServerSidePagedCollectionView<BandInfoItemViewModel> Items { get; set; }

        [RaisePropertyChanged]
        public int PageSize
        {
            get
            {
                if (_pageSize == 0)
                {
                    return Int32.MaxValue;
                }
                else if (_pageSize < 0)
                {
                    var actualHeight = System.Windows.Application.Current.Host.Content.ActualHeight - 330;
                    return (int)Math.Floor(actualHeight / 27);
                }
                return _pageSize;
            }
        }

        [RaisePropertyChanged]
        public Visibility QuickSearchVisibility { get { return Visibility.Visible; } }

        
        public DelegateCommand<object> FilterContentsApply { get; set; }
        public DelegateCommand<object> FilterContentsClear { get; set; }
        public DelegateCommand<object> HeaderClicked { get; set; }

        public RelayCommand<KeyEventArgs> OnKeyUpOnFilter { get; set; }

        private InMemoryTable<BandInfoDto> BandInfosTable;
        public BandInfosDetailsListViewModel(IUnityContainer container)
            : base(container)
        {
            FilterContentsApply = new DelegateCommand<object>(OnFilterContentsApply);
            FilterContentsClear = new DelegateCommand<object>(OnFilterContentsClear);
            OnKeyUpOnFilter = new RelayCommand<KeyEventArgs>(OnKeyUpOnFilterEvent);
            HeaderClicked = new DelegateCommand<object>(OnHeaderClicked);

            //ColumnWidths = FilterHelper.GetColumnWidths();
            EventAggregator.GetEvent<SaveSettingsEvent>().Subscribe(HandleSaveSettings);

            BandInfosTable = null;// new InMemoryTable<BandInfoDto>(BandInfoDb.GetItems());
        }

        public void HandleSaveSettings(bool obj)
        {
            //FilterHelper.SaveColumnWidths(ColumnWidths);
        }

        private void OnKeyUpOnFilterEvent(KeyEventArgs obj)
        {
            if (obj.Key == Key.Enter)
            {
                FilterContentsApply.Execute(null);
            }
        }

        private void OnFilterContentsClear(object obj)
        {
            FilterHelper.Clear();
            Refresh(_currentUnit);
        }

        private void OnFilterContentsApply(object state)
        {
                Refresh(_currentUnit);
        }

        private void OnHeaderClicked(object state)
        {
			//FilterHelper.SortOrder = (string)state;
            Refresh(_currentUnit);
        }



        /*public void Refresh(IUnit unit)
        {
            _currentUnit = unit;
            FilterHelper.UnitId = unit.Identity;

            Items = new ServerSidePagedCollectionView<BandInfoItemViewModel>(
              new InMemoryDataSource<BandInfoDto, BandInfoItemViewModel>(Container, PageSize, FilterHelper,
                  BandInfosTable
                  )
              , PageSize
              );
        }*/

		
        public void Refresh(IUnit unit)
        {
            _currentUnit = unit;
            //FilterHelper.UnitId = unit.Identity;

            /*
            Items = new ServerSidePagedCollectionView<BandInfoItemViewModel>(
              new BandInfoPagedDataSource(Container, PageSize, FilterHelper, this, _currentUnit.Identity)
              , PageSize
              );
             */
        }


        public void HandleDetailsItemClickedEvent(long prepId)
        {

        }


        public Telerik.Windows.Controls.CommandBindingCollection GetCommandBindings()
        {
            var bindings = new Telerik.Windows.Controls.CommandBindingCollection();

            return bindings;
        }

		public UnitsService.UnitsServiceClient GetProxy()
        {
            return ServiceFactory.GetService<UnitsServiceClient>(ServicesUri.UnitsService);
        }

    }
}