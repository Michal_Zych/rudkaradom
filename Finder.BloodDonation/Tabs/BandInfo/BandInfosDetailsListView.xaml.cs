using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Telerik.Windows.Controls;
using Finder.BloodDonation.Behaviors;
using System.Windows.Printing;
using System.Windows.Controls.Primitives;
using Finder.BloodDonation.Tools;
using System.Windows.Data;

namespace Finder.BloodDonation.Tabs.BandInfos
{
    [ViewModel(typeof(BandInfosDetailsListViewModel))]
    public partial class BandInfosDetailsListView : UserControl, IDataContextChangedHandler<BandInfosDetailsListView>
    {
        private const string SCROLLERS_SYNCHRO_GROUP = "BandInfo";


        public BandInfosDetailsListView()
        {
            InitializeComponent();
            DataContextChangedHelper<BandInfosDetailsListView>.Bind(this);

            DetailsList.LayoutUpdated += RegisterScrollsAfterLayoutUpdated;
        }


        private void RegisterScrollsAfterLayoutUpdated(object sender, EventArgs e)
        {
            if (ScrollViewersSynchronizer.IsGroupRegistered(SCROLLERS_SYNCHRO_GROUP))
            {
               throw new Exception("Powtórzona grupa synchronizacji suwaków: " + SCROLLERS_SYNCHRO_GROUP);
            }
            ScrollViewersSynchronizer.Register(Filters, SCROLLERS_SYNCHRO_GROUP);
            ScrollViewersSynchronizer.Register(headerScroller, SCROLLERS_SYNCHRO_GROUP);
            ScrollViewersSynchronizer.Register(DetailsList, SCROLLERS_SYNCHRO_GROUP);
            DetailsList.LayoutUpdated -= RegisterScrollsAfterLayoutUpdated;
        }



        public void DataContextChangedm(BandInfosDetailsListView sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                var vm = e.NewValue as BandInfosDetailsListViewModel;

                CommandManager.SetCommandBindings(this, vm.GetCommandBindings());
            }
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {


            BindingExpression binding = null;
            if (sender is TextBox)
            {
                binding = (sender as TextBox).GetBindingExpression(TextBox.TextProperty);
            }
            else if (sender is RadDateTimePicker)
            {
                binding = (sender as RadDateTimePicker).GetBindingExpression(RadDateTimePicker.SelectedValueProperty);
            }
            if (binding != null)
            {
                binding.UpdateSource();
            }
            base.OnKeyUp(e);
        }
        
    }
}
