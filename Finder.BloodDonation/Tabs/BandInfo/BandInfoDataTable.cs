using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataTable;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Authentication;

namespace Finder.BloodDonation.Tabs.BandInfo
{
    public class BandInfoDataTable : BaseDataTable
    {

        public BandInfoDataTable(IUnityContainer container)
            : base(container)
        {
        }

        public override ObservableCollection<IDataTableColumn> GetColumns()
        {
			bool isHiddenForUser = !BloodyUser.Current.IsInRole(PermissionNames.HyperAdmin);
            bool isHiddenForSuperUser = !BloodyUser.Current.IsInRole(PermissionNames.SuperUser);

            return new ObservableCollection<IDataTableColumn>()
             .Add(50)
			 .Add(100, "Id", "ID", FilterDataType.Short, TextAlignment.Left, isHiddenForUser)
             .Add(100, "BarCode", "Kod kreskowy", FilterDataType.String, TextAlignment.Left)
             .Add(100, "MacString", "MAC", FilterDataType.String, TextAlignment.Left)
             .Add(100, "Uid", "UID", FilterDataType.Long, TextAlignment.Left, false, false) // h
             .Add(100, "SerialNumber", "Numer seryjny", FilterDataType.String, TextAlignment.Left)
             .Add(100, "BatteryTypeName", "Typ baterii", FilterDataType.String, TextAlignment.Left)
             .Add(100, "BatteryAlarmSeverityName", "Zdarzenie", FilterDataType.String, TextAlignment.Left)
             .Add(100, "BatteryInstallationDateUtc", "Instalacja baterii", FilterDataType.DateTime, TextAlignment.Left, !(isHiddenForUser || isHiddenForSuperUser))
             .Add(100, "BatteryDaysToExchange", "Dni do wymiany baterii", FilterDataType.Int, TextAlignment.Left)
             .Add(100, "Description", "Opis", FilterDataType.String, TextAlignment.Left, true, false)
             .Add(100, "ObjectTypeName", "Typ obiektu", FilterDataType.String, TextAlignment.Left)
             .Add(100, "ObjectDisplayName", "Nazwa", FilterDataType.String, TextAlignment.Left)
             .Add(100, "UnitId", "ID oddzia�u", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "UnitName", "Oddzia�", FilterDataType.String, TextAlignment.Left)
             .Add(100, "UnitLocation", "Lokalizacja strefy", FilterDataType.String, TextAlignment.Left, true, false)
             .Add(100, "CurrentUnitId", "ID aktualnej strefy", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "CurrentUnitName", "Aktualna strefa", FilterDataType.String, TextAlignment.Left)
             .Add(100, "CurrentUnitLocation", "Lokalizacja aktualnej strefy", FilterDataType.String, TextAlignment.Left, true,false)
                         ;
        }

        public override ISortedFilter GetFilter(IUnityContainer container)
        {
            return new BandInfoFilter(container);
        }

		public override bool StartWithDefaultColumns
        {
            get
            {
                return false;
            }
        }
    }
}
