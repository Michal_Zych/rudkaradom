using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Events;
using System.Collections.Generic;
using System.Collections;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Dialogs.Helpers;
using Microsoft.Practices.Composite.Presentation.Events;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.Dialogs.Bands;
using Finder.BloodDonation.Dialogs.Tree;
using Finder.BloodDonation.Debugs;
using Finder.BloodDonation.Dialogs.BatteryReplace;

namespace Finder.BloodDonation.Tabs.BandInfo
{
    [OnCompleted]
    [UIException]
    public class BandInfosViewModel: ViewModelBase, ITabViewModel
    {
        private BandEditData bandEditData;
        public UnitsServiceClient Proxy { get; set; }
        public event EventHandler LoadingCompleted = delegate { };
        private IUnit SelectedUnit = null;
        private IUnit _currentUnit;
        private readonly IWindowsManager _windowsManager;
        private BandDTO selectedItemData;
        private BandInfoDto selectedItem;
        public BandInfoDto SelectedItem
        {
            get
            {
                return selectedItem;
            }
            set
            {
                if (value != null)
                {
                    Proxy.GetBandConfigurationAsync(value.Id);
                }

                selectedItem = value;
            }
        }

		private IUnitFiltered _details;
        [RaisePropertyChanged]
        public IUnitFiltered Details
        {
            get
            {
                if (_details == null)
                {
                    _details = Container.Resolve<BandInfoDetailsListViewModel>();
                }
                return _details;
            }
            set
            {
                _details = value;
            }
        }


        public RelayCommand CreateBandInfoCommand { get; set; }
        public RelayCommand EditBandInfoCommand { get; set; }
        public RelayCommand DetailsBandInfoCommand { get; set; }
        public RelayCommand BatteryRechange { get; set; }

        public BandInfosViewModel(IUnityContainer container)
            : base(container)
        {
            this.ViewAttached += new EventHandler(ViewModel_ViewAttached);
            
            var viewFactory = container.Resolve<ViewFactory>();

            EventAggregator.GetEvent<SelectedItemChangedEvent<BandInfoDto>>().Subscribe(OnSelectedItemChangedEvent);
            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(OnCloseWindowsEvent);

            CreateBandInfoCommand = new RelayCommand(OnCreateBandInfo);
            EditBandInfoCommand = new RelayCommand(OnEditBandInfo);
            DetailsBandInfoCommand = new RelayCommand(OnDetailsBandInfoCommand);
            BatteryRechange = new RelayCommand(OnBatteryRechangeCommand);

            Proxy = ServiceFactory.GetService<UnitsServiceClient>();
            Proxy.GetBandConfigurationCompleted += Proxy_GetBandConfigurationCompleted;

            _windowsManager = this.Container.Resolve<IWindowsManager>();
            _windowsManager.RegisterWindow(WindowsKeys.CreateBand, "", viewFactory.CreateView<BandEdit>(), new Size(590, 650), true, false);
            _windowsManager.RegisterWindow(WindowsKeys.BatteryReplace, "", viewFactory.CreateView<BatteryReplaceView>(), new Size(700, 350), true, false);
        }

        public void OnCloseWindowsEvent(string obj)
        {
            Refresh(_currentUnit);
            EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(CloseWindowEventHandling);
        }

        public void OnBatteryRechangeCommand()
        {
            if(selectedItem != null)
            {
                BatteryReplaceData _data = new BatteryReplaceData()
                {
                    BandId = selectedItem.Id,
                    BandDisplayName = selectedItem.SerialNumber
                };

                _windowsManager.ShowWindow(WindowsKeys.BatteryReplace, _data);
            }
            else
            {
                MsgBox.Warning("Nie wybrano lokalizatora!");
            }
        }

        public void Proxy_GetBandConfigurationCompleted(object sender, GetBandConfigurationCompletedEventArgs e)
        {
            if (e.Result == null)
                return;

            selectedItemData = e.Result.Where(y => y.Id == selectedItem.Id).FirstOrDefault();
        }

        public void OnSelectedItemChangedEvent(IItemViewModel<BandInfoDto> obj)
        {
            if (obj == null)
            {
                SelectedItem = null;
            }
            else
            {
                SelectedItem = obj.Item;
            }

            SelectedItemChanged();
        }

        public void SelectedItemChanged()
        {

        }

        public void OnDetailsBandInfoCommand()
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseTreeUnitHelpher);
            _windowsManager.ShowWindow(WindowsKeys.TreeUnitHelpher);
        }

        public void CloseTreeUnitHelpher(string obj)
        {

        }

        public void OnEditBandInfo()
        {
            if (selectedItemData == null)
            {
                MsgBox.Warning("Nie wybrano lokalizatora do edycji.");
                return;
            }

            bandEditData = new BandEditData()
            {
                band = selectedItemData,
                SelectedUnitId = selectedItemData.UnitId != null ? selectedItemData.UnitId : _currentUnit.Identity,
                IsEdited = true
            };

            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(BandEditWindowCloseEventHandling);
            this._windowsManager.ShowWindow(WindowsKeys.CreateBand, bandEditData);

            selectedItem = null;
        }

        public void BandEditWindowCloseEventHandling(string obj)
        {
            selectedItemData = null;
            Refresh(_currentUnit);
        }

        private void ViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public void Refresh(IUnit unit)
        {
            SelectedUnit = unit;
            _currentUnit = unit;

            Details.Refresh(unit);
        }

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        public string Title
        {
            get { return ""; }
        }

        public void OnCreateBandInfo()
        {
            bandEditData = new BandEditData()
            {
                SelectedUnitId = _currentUnit.Identity,
                IsEdited = false
            };

            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(BandEditWindowCloseEventHandling);
            this._windowsManager.ShowWindow(WindowsKeys.CreateBand, bandEditData);

            EventAggregator.GetEvent<CloseWindowEvent>().Subscribe(CloseWindowEventHandling);
        }

        public void CloseWindowEventHandling(string obj)
        {
            //throw new NotImplementedException();
        }

        public bool ExitButtonVisible
        {
            get 
            { 
                return false;
            }
        }
    }
}
