using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Tabs.BandInfos;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.BandInfo
{
    public class BandInfoPagedDataSource: IPagedDataSource<BandInfoItemViewModel>
    {
        protected IUnityContainer _container;
        protected int _pageSize;
        protected SortedFilter _filter;
        protected IUnitDataClient _dataClient;
		protected int unitId;

        public BandInfoPagedDataSource(IUnityContainer container, int pageSize, SortedFilter filter, IUnitDataClient dataClient, int unitId)
        {
            _container = container;
            _pageSize = pageSize;
            _filter = filter;
            _dataClient = dataClient;
			this.unitId = unitId;
        }


        public void FetchData(int pageNumber, Action<PagedDataResponse<BandInfoItemViewModel>> responseCallback)
        {
            var proxy = _dataClient.GetProxy();
            var list = new List<BandInfoItemViewModel>();

            proxy.GetBandInfosFilteredCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<BandInfoItemViewModel>();
                    vm.Item = item;
                    list.Add(vm);
                }
                _container.Resolve<BandInfoItemViewModel>().SelectionChanged(-1);
                proxy.GetBandInfosFilteresdTotalCountAsync((Dictionary<string, string>)_filter.GetFilterDictionary());
            };

            proxy.GetBandInfosFilteresdTotalCountCompleted += (s, e) =>
            {
                responseCallback(new PagedDataResponse<BandInfoItemViewModel>()
                {
                    Items = list,
                    TotalItemCount = e.Result
                });
            };

            proxy.GetBandInfosFilteredAsync((Dictionary<string, string>)_filter.GetFilterDictionary(), pageNumber, _pageSize, unitId , 0);
             
        }
    }
}
