using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.BandInfo
{
    public class BandInfoFilter : SortedFilter
    {
       [RaisePropertyChanged]
public string Id { get; set; }

[RaisePropertyChanged]
public string BarCode { get; set; }

[RaisePropertyChanged]
public string MacString { get; set; }

[RaisePropertyChanged]
public string Uid { get; set; }

[RaisePropertyChanged]
public string SerialNumber { get; set; }

[RaisePropertyChanged]
public string BatteryTypeName { get; set; }

[RaisePropertyChanged]
public string BatteryAlarmSeverityName { get; set; }

[RaisePropertyChanged]
public DateTime? BatteryInstallationDateUtcFrom { get; set; }
[RaisePropertyChanged]
public DateTime? BatteryInstallationDateUtcTo { get; set; }

[RaisePropertyChanged]
public string BatteryDaysToExchange { get; set; }

[RaisePropertyChanged]
public string Description { get; set; }

[RaisePropertyChanged]
public string ObjectTypeName { get; set; }

[RaisePropertyChanged]
public string ObjectDisplayName { get; set; }

[RaisePropertyChanged]
public string UnitId { get; set; }

[RaisePropertyChanged]
public string UnitName { get; set; }

[RaisePropertyChanged]
public string UnitLocation { get; set; }

[RaisePropertyChanged]
public string CurrentUnitId { get; set; }

[RaisePropertyChanged]
public string CurrentUnitName { get; set; }

[RaisePropertyChanged]
public string CurrentUnitLocation { get; set; }



        public BandInfoFilter(IUnityContainer container)
            : base(container)
        {

        }
    }
}
