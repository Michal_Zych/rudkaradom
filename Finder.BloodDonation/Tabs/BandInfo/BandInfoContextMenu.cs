using Finder.BloodDonation.Context;
using Finder.BloodDonation.Tools;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.BandInfo
{
    [FinderFX.SL.Core.Communication.OnCompleted(AttributeTargetMembers = "regex:Completed")]
    public class BandInfoContextMenu : GenericContextMenuItemsCollection<BandInfoItemViewModel>
    {
        private IUnityContainer container { get; set; }

        public BandInfoContextMenu(BandInfoItemViewModel vm, IUnityContainer container)
            : base(vm)
        {
            this.container = container;
        }

        public override void RegisterItems(System.Collections.Generic.IList<IContextMenuItem> items)
        {
            items.Add(new BandInfoContextItem(this.AssociatedObject)
            {
                Header = "Edytuj",
                Command = new DelegateCommand<object>(AssociatedObject.OnContextmenuClicked),
                CommandParameter = "Edytuj",
            });
        }
    }


    public class BandInfoContextItem : GenericMenuItem
    {
        private BandInfoItemViewModel Item { get; set; }
        public BandInfoContextItem(BandInfoItemViewModel vm)
        {
            Item = vm;
        }

        public override bool IsAvailable()
        {
            return Item.Item != null;
        }
    }

}
