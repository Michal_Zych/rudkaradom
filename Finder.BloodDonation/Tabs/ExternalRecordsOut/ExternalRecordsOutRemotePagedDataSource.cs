using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.ExternalRecordsOut
{
    public class ExternalRecordsOutRemotePagedDataSource: IPagedDataSource<ExternalRecordsOutItemViewModel>
    {
        protected IUnityContainer _container;
        protected int _pageSize;
        protected IDataTable _dataTable;
        protected IUnitDataClient _dataClient;

        public ExternalRecordsOutRemotePagedDataSource(IUnityContainer container, int pageSize, IDataTable dataTable, IUnitDataClient dataClient)
        {
            _container = container;
            _pageSize = pageSize;
            _dataTable = dataTable;
            _dataClient = dataClient;
        }


		 public void FetchData(int pageNumber, Action<PagedDataResponse<ExternalRecordsOutItemViewModel>> responseCallback)
        {
            var proxy = _dataClient.GetProxy();
            var list = new List<ExternalRecordsOutItemViewModel>();
            
             
            proxy.GetExternalRecordsOutFilteredCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<ExternalRecordsOutItemViewModel>();
                    vm.Item = item;
                    vm.DataTable = _dataTable;
                    list.Add(vm);
                }
                _container.Resolve<ExternalRecordsOutItemViewModel>().SelectionChanged(-1);
                proxy.GetExternalRecordsOutTotalCountAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary());
            };

            proxy.GetExternalRecordsOutTotalCountCompleted += (s, e) =>
            {
                responseCallback(new PagedDataResponse<ExternalRecordsOutItemViewModel>()
                {
                    Items = list,
                    TotalItemCount = e.Result
                });
            };

            proxy.GetExternalRecordsOutFilteredAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary(), pageNumber, _pageSize);
        }
    }
}
