using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.ExternalRecordsOut
{
    public class ExternalRecordsOutFilter : SortedFilter
    {
       [RaisePropertyChanged]
public string Id { get; set; }

[RaisePropertyChanged]
public string EventId { get; set; }

[RaisePropertyChanged]
public DateTime? DateUtcFrom { get; set; }
[RaisePropertyChanged]
public DateTime? DateUtcTo { get; set; }

[RaisePropertyChanged]
public string RecordType { get; set; }

[RaisePropertyChanged]
public string Record { get; set; }

[RaisePropertyChanged]
public string ParentId { get; set; }

[RaisePropertyChanged]
public string Try { get; set; }

[RaisePropertyChanged]
public string TryAgain { get; set; }

[RaisePropertyChanged]
public DateTime? SentDateUtcFrom { get; set; }
[RaisePropertyChanged]
public DateTime? SentDateUtcTo { get; set; }

[RaisePropertyChanged]
public string Error { get; set; }



        public ExternalRecordsOutFilter(IUnityContainer container)
            : base(container)
        {

        }
    }
}
