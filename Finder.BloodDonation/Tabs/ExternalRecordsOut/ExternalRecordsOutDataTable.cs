using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataTable;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Filters;

namespace Finder.BloodDonation.Tabs.ExternalRecordsOut
{
    public class ExternalRecordsOutDataTable : BaseDataTable
    {

        public ExternalRecordsOutDataTable(IUnityContainer container)
            : base(container)
        {
        }

        public override ObservableCollection<IDataTableColumn> GetColumns()
        {
            return new ObservableCollection<IDataTableColumn>()
             .Add(50)
			 .Add(100, "Id", "ID", FilterDataType.Int)
             .Add(100, "EventId", "ID zdarzenia", FilterDataType.Int)
             .Add(100, "DateUtc", "Data", FilterDataType.DateTime)
             .Add(100, "RecordType", "Typ rekordu", FilterDataType.String)
             .Add(100, "Record", "Rekord", FilterDataType.String)
             .Add(100, "ParentId", "ID rodzica", FilterDataType.Int)
             .Add(100, "Try", "Pr�ba wys�ania", FilterDataType.Int)
             .Add(100, "TryAgain", "Pr�buj ponownie", FilterDataType.Int)
             .Add(100, "SentDateUtc", "Data wys�ania", FilterDataType.DateTime)
             .Add(100, "Error", "Opis b��du", FilterDataType.String)
             ;
        }


        public override ISortedFilter GetFilter(IUnityContainer container)
        {
            return new ExternalRecordsOutFilter(container);
        }

		public override bool StartWithDefaultColumns
        {
            get
            {
                return false;
            }
        }
    }
}
