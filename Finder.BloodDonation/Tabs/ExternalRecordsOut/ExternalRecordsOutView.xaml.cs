using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Tools;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Dialogs.Patients;
using Microsoft.Practices.Composite.Events;
using Finder.BloodDonation.Common.Events;
using Microsoft.Practices.Composite.Presentation.Events;
using Finder.BloodDonation.Common.Layout;

namespace Finder.BloodDonation.Tabs.ExternalRecordsOut
{
    [ViewModel(typeof(ExternalRecordsOutViewModel))]
    public partial class ExternalRecordsOutView : UserControl
    {

        public ExternalRecordsOutView()
        {
            InitializeComponent();
        }

    }
}
