﻿using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Events;
using System.Collections.Generic;
using System.Collections;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;

namespace Finder.BloodDonation.Tabs.Alarms
{
        [OnCompleted]
        [UIException]
    public class AlarmsListViewModel : ViewModelBase, ITabViewModel
    {
        private IUnit SelectedUnit = null;

        [RaisePropertyChanged]
        public ObservableCollection<AlarmRowViewModel> AlarmsList { get; set; }

        public IList<AlarmRowViewModel> SelectedAlarms { get; set; }

        public DelegateCommand<object> SelectAlarms { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> ShowChart { get; set; }

        [RaisePropertyChanged]
        public AlarmRowViewModel SelectedAlarm { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterDateStart { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterDateEnd { get; set; }

        [RaisePropertyChanged]
        public string SearchFilterObject { get; set; }

        [RaisePropertyChanged]
        public bool IsBusy2 { get; set; }

        [RaisePropertyChanged]
        public string AlarmsRowsLimit { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> SearchAlarms { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> CloseAlarms { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> ShowAlarmDetails { get; set; }
        

        private AlarmsServiceClient LoadService
        {
            get
            {
                AlarmsServiceClient proxy = ServiceFactory.GetService<AlarmsServiceClient>(ServicesUri.AlarmsService);
                proxy.SearchAlarmsCompleted += new EventHandler<SearchAlarmsCompletedEventArgs>(proxy_SearchAlarmsCompleted);
                proxy.CloseAlarmsCompleted += new EventHandler<CloseAlarmsCompletedEventArgs>(proxy_CloseAlarmsCompleted);
                return proxy;
            }
        }

        private ITabManager TabManager = null;


        void proxy_SearchAlarmsCompleted(object sender, SearchAlarmsCompletedEventArgs e)
        {
            ObservableCollection<AlarmRowViewModel> r = new ObservableCollection<AlarmRowViewModel>();
            if (e.Result != null)
            {
                for (int i = 0; i < e.Result.Count; i++)
                {
                    r.Add(new AlarmRowViewModel(Container) { Alarm = e.Result[i] });
                }
            }
            AlarmsList = r;

            AlarmsRowsLimit = "";
            int maxRowsAlarmsPopup = Global.Settings.Current.GetInt(Global.ConfigurationKeys.MaxRowsAlarmsPopup);
            if (AlarmsList != null && AlarmsList.Count == maxRowsAlarmsPopup)
            {
                AlarmsRowsLimit = "Ilość alarmów ograniczona do " + maxRowsAlarmsPopup.ToString();
            }
            IsBusy2 = false;
        }

        public AlarmsListViewModel(IUnityContainer container)
            : base(container)
        {
            this.ViewAttached += new EventHandler(UnitsListViewModel_ViewAttached);

            SelectAlarms = new DelegateCommand<object>(SelectAlarms_Command);
            SearchAlarms = new DelegateCommand<object>(this.SearchAlarms_Command);
            CloseAlarms = new DelegateCommand<object>(this.CloseAlarms_Command);
            ShowAlarmDetails = new DelegateCommand<object>(this.ShowAlarmDetails_Command);
            ShowChart = new DelegateCommand<object>(ShowChart_Command);
            

            InitializeDates();

            TabManager = container.Resolve<ITabManager>();
        }

        

        public void ShowChart_Command(object o)
        {
            //MessageBox.Show("zakładka monitorowanie");
            TabManager.SelectTab(TabNames.UnitPlan, ShowChart_MonitoringTabLoaded);
        }

        public void ShowChart_MonitoringTabLoaded(string report_tab_name)
        {
            if (SelectedAlarm != null)
            {
                EventAggregator.GetEvent<ShowMonitoringTabEvent>().Publish(
                                new MonitoringTab()
                                {
                                    DateStart = SelectedAlarm.Alarm.AlarmTime.AddHours(-2),
                                    DateEnd = SelectedAlarm.Alarm.AlarmTime.AddHours(2)
                                }
                            );
            }
        }

        public void SearchAlarms_Command(object o)
        {
            if (SearchFilterDateStart.HasValue && SearchFilterDateEnd.HasValue)
            {
                var datesOk = InitializeDates();
                if (!datesOk) return;
            }

            IsBusy2 = true;
            SelectedAlarms = null;

            LoadService.SearchAlarmsAsync(
                SelectedUnit.Identity,
                SelectedUnit.SelectionSource,
                SearchFilterObject,
                new DateTime(
                    SearchFilterDateStart.Value.Year,
                    SearchFilterDateStart.Value.Month,
                    SearchFilterDateStart.Value.Day,
                    0, 0, 0),
                    new DateTime(
                    SearchFilterDateEnd.Value.Year,
                    SearchFilterDateEnd.Value.Month,
                    SearchFilterDateEnd.Value.Day,
                    23, 59, 59));
        }

        public void CloseAlarms_Command(object o)
        {
            /*EditAlarmComentViewModel wnd = Container.Resolve<EditAlarmComentViewModel>();
            wnd.UnitName = "nazwa unita";
            wnd.AlarmsCount = "34";
            wnd.AlarmsCloseComment = "komentarz";
            EventAggregator.GetEvent<ShowWindow>().Publish("alarcomedit");
            */

            if (SelectedAlarms != null)
            {
                var ids = SelectedAlarms.Select(a => a.Alarm.ID).ToCommaSeparatedValues();
                if (ids != null)
                {
                    LoadService.CloseAlarmsAsync(ids, string.Empty);
                }
            }
        }

        void proxy_CloseAlarmsCompleted(object sender, CloseAlarmsCompletedEventArgs e)
        {
            SearchAlarms_Command(null);
        }


        public void ShowAlarmDetails_Command(object o)
        {
            if (SelectedAlarms != null && SelectedAlarms.Count == 1)
            {
                SelectedAlarms[0].AlarmDetails_Command(null);
            }
        }

        private bool InitializeDates()
        {
            bool result = true;
            if (!SearchFilterDateStart.HasValue || !SearchFilterDateEnd.HasValue)
            {
                SearchFilterDateStart = DateTime.Now.AddDays(-Global.Settings.Current.GetInt(Global.ConfigurationKeys.SearchAlarmsDaysBefore));
                SearchFilterDateEnd = DateTime.Now.AddDays(Global.Settings.Current.GetInt(Global.ConfigurationKeys.SearchAlarmsDaysAfter));
            }
            if (SearchFilterDateEnd < SearchFilterDateStart)
            {
                MsgBox.Error("Data końcowa nie może być wcześniejsza niż data początkowa");
                SearchFilterDateEnd = SearchFilterDateStart;
                result = false;
            }
            return result;
        }

        public void SelectAlarms_Command(object o)
        {
            if (SelectedAlarms == null)
            {
                SelectedAlarms = new List<AlarmRowViewModel>();
            }

            SelectedAlarms.Clear();

            if (o != null)
            {
                IList l = o as IList;
                for (int i = 0; i < l.Count; i++)
                {
                    SelectedAlarms.Add(l[i] as AlarmRowViewModel);
                }
            }
        }

        void UnitsListViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public void Refresh(IUnit unit)
        {
            AlarmsList = null;
            SelectedUnit = unit;
            SearchAlarms_Command(null);
        }

        public event EventHandler LoadingCompleted = delegate { };

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }


        public string Title
        {
            get { return ""; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }
}
