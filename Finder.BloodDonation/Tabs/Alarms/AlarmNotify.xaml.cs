﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;

namespace Finder.BloodDonation.Tabs.Alarms
{
    [ViewModel(typeof(AlarmNotifyViewModel))]
    public partial class AlarmNotify : UserControl
    {
        ContextMenu cm_alarmy = new ContextMenu();

        public AlarmNotify()
        {
            InitializeComponent();

            gridAlarmsAll.MouseRightButtonDown += new MouseButtonEventHandler(gridAlarmsAll_MouseRightButtonDown);
            gridAlarmsAll.MouseRightButtonUp += new MouseButtonEventHandler(gridAlarmsAll_MouseRightButtonUp);
        }

        void gridAlarmsAll_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            IEnumerable<UIElement> elements = VisualTreeHelper.FindElementsInHostCoordinates(e.GetPosition(Application.Current.RootVisual), gridAlarmsAll);
            DataGridRow row = null;
            foreach (UIElement ele in elements)
            {
                if (ele is DataGridRow)
                {
                    row = ele as DataGridRow;
                    break;
                }
            }

            if (row != null && row.DataContext != null)
            {
                cm_alarmy.Items.Clear();
                cm_alarmy.DataContext = row.DataContext;

                MenuItem mi = new MenuItem { Header = "Szczegóły" };
                mi.Command = ((AlarmNotifyRowViewModel)row.DataContext).AlarmDetails;
                cm_alarmy.Items.Add(mi);

                gridAlarmsAll.SetValue(ContextMenuService.ContextMenuProperty, cm_alarmy);
                cm_alarmy.IsOpen = true;
            }
        }

        void gridAlarmsAll_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
    }
}
