﻿using System;
using System.Net;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using System.Collections.Generic;
using Finder.BloodDonation.Common.Layout;

namespace Finder.BloodDonation.Tabs.Alarms
{
    [OnCompleted]
    [UIException]
    public class EditAlarmViewModel : ViewModelBase
    {
        [RaisePropertyChanged]
        public AlarmDto Alarm { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> CloseAlarm { get; set; }

        private AlarmsServiceClient LoadService
        {
            get
            {
                AlarmsServiceClient proxy = ServiceFactory.GetService<AlarmsServiceClient>(ServicesUri.AlarmsService);
                proxy.CloseAlarmsCompleted += new EventHandler<CloseAlarmsCompletedEventArgs>(proxy_CloseAlarmsCompleted);
                return proxy;
            }
        }

        public EditAlarmViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            EventAggregator.GetEvent<LoadAlarmDetails>().Subscribe(LoadAlarmDetails_Event);

            CloseAlarm = new DelegateCommand<object>(this.CloseAlarm_Command);
        }

        void proxy_CloseAlarmsCompleted(object sender, CloseAlarmsCompletedEventArgs e)
        {
            EventAggregator.GetEvent<AlarmsCloseAlarms>().Publish(new List<AlarmDto>() {Alarm});
            EventAggregator.GetEvent<CloseWindowEvent>().Publish("alaredit");
            
            Container.Resolve<IWindowsManager>().CloseWindow("alaredit");
        }

        public void CloseAlarm_Command(object o)
        {
            if (Alarm != null)
            {
                LoadService.CloseAlarmsAsync(Alarm.ID.ToString(), Alarm.AlarmCloseComment);
            }
        }

        public void LoadAlarmDetails_Event(AlarmDto o)
        {
            o.AlarmCloseComment = "";
            Alarm = o;
        }
    }
}
