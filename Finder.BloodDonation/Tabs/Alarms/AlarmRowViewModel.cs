﻿using System;
using System.Net;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Layout;
using System.Windows;

namespace Finder.BloodDonation.Tabs.Alarms
{
    [UIException]
    public class AlarmRowViewModel : ViewModelBase
    {
        [RaisePropertyChanged]
        public AlarmDto Alarm { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> AlarmDetails { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> ShowChart { get; set; }

        private ITabManager TabManager = null;

        public AlarmRowViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            this.AlarmDetails = new DelegateCommand<object>(this.AlarmDetails_Command);
            ShowChart = new DelegateCommand<object>(ShowChart_Command);

            TabManager = unityContainer.Resolve<ITabManager>();
        }

        public void ShowChart_Command(object o)
        {
            MessageBox.Show("zakładka monitorowanie");
           // TabManager.SelectTab(TabNames.UnitMonitoring, ShowChart_MonitoringTabLoaded);
        }

        public void ShowChart_MonitoringTabLoaded(string report_tab_name)
        {
            if (Alarm != null)
            {
                EventAggregator.GetEvent<ShowMonitoringTabEvent>().Publish(
                                new MonitoringTab()
                                {
                                    DateStart = Alarm.AlarmTime.AddHours(-2),
                                    DateEnd = Alarm.AlarmTime.AddHours(2)
                                }
                            );
            }
        }

        public void AlarmDetails_Command(object o)
        {
            if (Alarm != null)
            {
                EventAggregator.GetEvent<LoadAlarmDetails>().Publish(Alarm);
                EventAggregator.GetEvent<ShowWindow>().Publish("alaredit");
            }
        }
    }
}
