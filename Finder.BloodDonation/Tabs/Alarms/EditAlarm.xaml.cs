﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;

namespace Finder.BloodDonation.Tabs.Alarms
{
    [ViewModel(typeof(EditAlarmViewModel))]
    public partial class EditAlarm : UserControl
    {
        public EditAlarm()
        {
            InitializeComponent();
            //txtType.Loaded += new RoutedEventHandler(txtType_Loaded);
            //txtID.Loaded += new RoutedEventHandler(txtID_Loaded);
            //btnClose.KeyDown += new KeyEventHandler(btnClose_KeyDown);
        }

        void btnClose_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        void txtType_Loaded(object sender, RoutedEventArgs e)
        {
            if (txtType.Text == "4" || txtType.Text == "8" || txtType.Text == "32")
            {
                Dispatcher.BeginInvoke(() => { btnClose.UpdateLayout(); btnClose.Focus(); });
            }
        }

        void txtID_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}