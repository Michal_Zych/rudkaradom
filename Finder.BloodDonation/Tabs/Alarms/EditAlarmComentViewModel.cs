﻿using System;
using System.Net;
using System.Windows;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using System.Collections.Generic;

namespace Finder.BloodDonation.Tabs.Alarms
{
    [OnCompleted]
    [UIException]
    public class EditAlarmComentViewModel : ViewModelBase
    {
        [RaisePropertyChanged]
        public string UnitName { get; set; }

        [RaisePropertyChanged]
        public string AlarmsCount { get; set; }

        [RaisePropertyChanged]
        public string AlarmsCloseComment { get; set; }


        [RaisePropertyChanged]
        public DelegateCommand<object> CloseAlarms { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> Cancel { get; set; }


        public EditAlarmComentViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            //EventAggregator.GetEvent<LoadAlarmDetails>().Subscribe(LoadAlarmDetails_Event);

            CloseAlarms = new DelegateCommand<object>(this.CloseAlarms_Command);
            Cancel = new DelegateCommand<object>(this.Cancel_Command);
        }


        public void CloseAlarms_Command(object o)
        {

            MessageBox.Show("Close alarms");
        }

        public void Cancel_Command(object o)
        {
            MessageBox.Show("Cancel");
        }

        public void LoadAlarmDetails_Event(int o)
        {

        }
    }
}