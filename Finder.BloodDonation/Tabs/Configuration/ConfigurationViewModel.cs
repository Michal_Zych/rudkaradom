﻿using System;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Extensions;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.HubsProxy;
using System.Collections.Generic;
using Finder.BloodDonation.Common.Events;
using System.Linq;
using Finder.BloodDonation.CommService;
using Finder.BloodDonation.Events;
using Finder.BloodDonation.Model.Enum;
using System.Windows;
using System.Windows.Media;
using Finder.BloodDonation.DBBuffers;
using System.Collections;
using Telerik.Windows.Controls;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Authentication;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Tabs.Configuration.Converters;
using System.ComponentModel;

namespace Finder.BloodDonation.Tabs.Configuration
{
    [OnCompleted]
    //[UIException]
    public class ConfigurationViewModel : FinderFX.SL.Core.MVVM.ViewModelBase, ITabViewModel
    {
        private const short ValueEvent = 0;
        private const short NoSensorEvent = 3;
        private const short NoDeviceEvent = 4;
        private const short NoHubEvent = 5;

        private const string NOT_SET = "wyłączone";
        private const string NOT_CONNECTED = "nie podłączono";


        private EditSensorDto _sensor;
        private IList<NotificationReceiverDto> _notifications;
        private IList<OmittedAlarmRangeDto> _ranges;
        private bool _testAlarmsSaved;

        #region Properties

        //SmsRange
        private bool _smsRangeEnabled;
        [RaisePropertyChanged]
        public bool SmsRangeEnabled
        {
            get
            {
                return _smsRangeEnabled;
            }
            set
            {
                _smsRangeEnabled = value;
                SmsRangeRepeatElementsEnabled = SmsRangeEnabled && SmsRangeRepeat;
            }
        }

        [RaisePropertyChanged]
        public double SmsRangeDelay { get; set; }

        [RaisePropertyChanged]
        public bool SmsRangeAfterFinish { get; set; }

        [RaisePropertyChanged]
        public bool SmsRangeAfterClose { get; set; }

        private bool _smsRangeRepeat;
        [RaisePropertyChanged]
        public bool SmsRangeRepeat
        {
            get
            {
                return _smsRangeRepeat;
            }
            set
            {
                _smsRangeRepeat = value;
                SmsRangeRepeatElementsEnabled = SmsRangeEnabled && SmsRangeRepeat;
            }
        }

        [RaisePropertyChanged]
        public double SmsRangeRepeatInterval { get; set; }

        [RaisePropertyChanged]
        public int SmsRangeRepeatUntil { get; set; }

        [RaisePropertyChanged]
        public bool SmsRangeRepeatCanBreak { get; set; }

        [RaisePropertyChanged]
        public bool SmsRangeRepeatElementsEnabled { get; set; }

        [RaisePropertyChanged]
        public Visibility RepeatCanBreakVisible { get; set; }


        // ////////////////////////////////////////////////////////

        private bool _smsCommEnabled;
        [RaisePropertyChanged]
        public bool SmsCommEnabled
        {
            get
            {
                return _smsCommEnabled;
            }
            set
            {
                _smsCommEnabled = value;
                SmsCommRepeatElementsEnabled = SmsCommEnabled && SmsCommRepeat;
            }
        }

        [RaisePropertyChanged]
        public double SmsCommDelay { get; set; }

        [RaisePropertyChanged]
        public bool SmsCommAfterFinish { get; set; }

        [RaisePropertyChanged]
        public bool SmsCommAfterClose { get; set; }

        private bool _smsCommRepeat;
        [RaisePropertyChanged]
        public bool SmsCommRepeat
        {
            get
            {
                return _smsCommRepeat;
            }
            set
            {
                _smsCommRepeat = value;
                SmsCommRepeatElementsEnabled = SmsCommEnabled && SmsCommRepeat;
            }
        }

        [RaisePropertyChanged]
        public double SmsCommRepeatInterval { get; set; }

        [RaisePropertyChanged]
        public int SmsCommRepeatUntil { get; set; }

        [RaisePropertyChanged]
        public bool SmsCommRepeatCanBreak { get; set; }

        [RaisePropertyChanged]
        public bool SmsCommRepeatElementsEnabled { get; set; }

        //////////////////Mail
        //MailRange
        private bool _MailRangeEnabled;
        [RaisePropertyChanged]
        public bool MailRangeEnabled
        {
            get
            {
                return _MailRangeEnabled;
            }
            set
            {
                _MailRangeEnabled = value;
                MailRangeRepeatElementsEnabled = MailRangeEnabled && MailRangeRepeat;
            }
        }

        [RaisePropertyChanged]
        public double MailRangeDelay { get; set; }

        [RaisePropertyChanged]
        public bool MailRangeAfterFinish { get; set; }

        [RaisePropertyChanged]
        public bool MailRangeAfterClose { get; set; }

        private bool _MailRangeRepeat;
        [RaisePropertyChanged]
        public bool MailRangeRepeat
        {
            get
            {
                return _MailRangeRepeat;
            }
            set
            {
                _MailRangeRepeat = value;
                MailRangeRepeatElementsEnabled = MailRangeEnabled && MailRangeRepeat;
            }
        }

        [RaisePropertyChanged]
        public double MailRangeRepeatInterval { get; set; }

        [RaisePropertyChanged]
        public int MailRangeRepeatUntil { get; set; }

        [RaisePropertyChanged]
        public bool MailRangeRepeatCanBreak { get; set; }

        [RaisePropertyChanged]
        public bool MailRangeRepeatElementsEnabled { get; set; }


        // ////////////////////////////////////////////////////////

        private bool _mailCommEnabled;
        [RaisePropertyChanged]
        public bool MailCommEnabled
        {
            get
            {
                return _mailCommEnabled;
            }
            set
            {
                _mailCommEnabled = value;
                MailCommRepeatElementsEnabled = MailCommEnabled && MailCommRepeat;
            }
        }

        [RaisePropertyChanged]
        public double MailCommDelay { get; set; }

        [RaisePropertyChanged]
        public bool MailCommAfterFinish { get; set; }

        [RaisePropertyChanged]
        public bool MailCommAfterClose { get; set; }

        private bool _mailCommRepeat;
        [RaisePropertyChanged]
        public bool MailCommRepeat
        {
            get
            {
                return _mailCommRepeat;
            }
            set
            {
                _mailCommRepeat = value;
                MailCommRepeatElementsEnabled = MailCommEnabled && MailCommRepeat;
            }
        }

        [RaisePropertyChanged]
        public double MailCommRepeatInterval { get; set; }

        [RaisePropertyChanged]
        public int MailCommRepeatUntil { get; set; }

        [RaisePropertyChanged]
        public bool MailCommRepeatCanBreak { get; set; }

        [RaisePropertyChanged]
        public bool MailCommRepeatElementsEnabled { get; set; }

        ////////////////////////////
        //GUI

        [RaisePropertyChanged]
        public double GuiRangeDelay { get; set; }

        [RaisePropertyChanged]
        public double GuiCommDelay { get; set; }

        [RaisePropertyChanged]
        public bool GuiRangeRepeat { get; set; }
        [RaisePropertyChanged]
        public double GuiRangeRepeatMins { get; set; }
        [RaisePropertyChanged]
        public bool GuiRangeRepeatBreakable { get; set; }
        [RaisePropertyChanged]
        public bool GuiCommRepeat { get; set; }
        [RaisePropertyChanged]
        public double GuiCommRepeatMins { get; set; }
        [RaisePropertyChanged]
        public bool GuiCommRepeatBreakable { get; set; }

        /// 
        [RaisePropertyChanged]
        public bool SettingsEnabled { get; set; }

        [RaisePropertyChanged]
        public bool RangeEnabled { get; set; }

        [RaisePropertyChanged]
        public bool AlarmingEnabled { get; set; } //dostęp do sekscji alarmowanie

        private bool _notificationsEnabled;
        [RaisePropertyChanged]
        public bool NotificationsEnabled
        {
            get
            {
                return _notificationsEnabled;
            }
            set
            {
                _notificationsEnabled = value;
                SetEnabledStatusFroEndOfAlarms();
            }
        }

        private void SetEnabledStatusFroEndOfAlarms()
        {
            SmsEndOfRangeEnabled = SmsEndOfRangeEnabled && SmsRangeDelayEnabled;
            SmsEndOfCommEnabled = SmsEndOfCommEnabled && SmsCommDelayEnabled;
            EmailEndOfRangeEnabled = EmailEndOfRangeEnabled && EmailRangeDelayEnabled;
            EmailEndOfCommEnabled = EmailEndOfCommEnabled && EmailCommDelayEnabled;

            IsSmsEndOfRangeEnabled = NotificationsEnabled && SmsRangeDelayEnabled;
            IsSmsEndOfCommEnabled = NotificationsEnabled && SmsCommDelayEnabled;
            IsEmailEndOfRangeEnabled = NotificationsEnabled && EmailRangeDelayEnabled;
            IsEmailEndOfCommEnabled = NotificationsEnabled && EmailCommDelayEnabled;
        }

        [RaisePropertyChanged]
        public bool ChartEnabled { get; set; }

        [RaisePropertyChanged]
        public bool SaveEnabled { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<SocketDto> PossibleSockets { get; set; }

        [RaisePropertyChanged]
        public SocketDto SelectedSocket { get; set; }

        [RaisePropertyChanged]
        public double Interval { get; set; }

        [RaisePropertyChanged]
        public double Calibration { get; set; }

        [RaisePropertyChanged]
        public bool Enabled { get; set; }

        [RaisePropertyChanged]
        public string MU { get; set; }

        [RaisePropertyChanged]
        public double LoRange { get; set; }

        [RaisePropertyChanged]
        public double UpRange { get; set; }

        [RaisePropertyChanged]
        public string RangeAlarmLabel { get; set; }

        [RaisePropertyChanged]
        public string LegendDescription { get; set; }

        [RaisePropertyChanged]
        public string LegendShortcut { get; set; }

        [RaisePropertyChanged]
        public string LegendRangeDescription { get; set; }

        private Color _lineColor;

        [RaisePropertyChanged]
        public Color LineColor
        {
            get
            {
                return _lineColor;
            }
            set
            {
                _lineColor = value;
                RangeLineColor = LightColor(_lineColor);
            }
        }

        [RaisePropertyChanged]
        public double LineWidth { get; set; }

        [RaisePropertyChanged]
        public ChartLineStyle LineStyle { get; set; }

        [RaisePropertyChanged]
        public Color RangeLineColor { get; set; }

        [RaisePropertyChanged]
        public double RangeLineWidth { get; set; }

        [RaisePropertyChanged]
        public ChartLineStyle RangeLineStyle { get; set; }

        [RaisePropertyChanged]
        public Color AlarmColor { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<ChartLineStyle> LineStyles { get; set; }

        [RaisePropertyChanged]
        public bool AlDisabled { get; set; }

        [RaisePropertyChanged]
        public bool AlDisabledForHours { get; set; }

        [RaisePropertyChanged]
        public DateTime? AlDisableHoursFrom1 { get; set; }

        [RaisePropertyChanged]
        public DateTime? AlDisableHoursTo1 { get; set; }

        [RaisePropertyChanged]
        public DateTime? AlDisableHoursFrom2 { get; set; }

        [RaisePropertyChanged]
        public DateTime? AlDisableHoursTo2 { get; set; }

        [RaisePropertyChanged]
        public bool AlDisabledForRanges { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<UserContactInfoDto> UsersList { get; set; }

        [RaisePropertyChanged]
        public UserContactInfoDto SmsSelectedReceiver { get; set; }

        [RaisePropertyChanged]
        public UserContactInfoDto MailSelectedReceiver { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<ReceiverViewModel> SmsReceivers { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<ReceiverViewModel> EmailReceivers { get; set; }

        [RaisePropertyChanged]
        public DateTime? OmittedRangeDateEnd { get; set; }

        [RaisePropertyChanged]
        public DateTime? OmittedRangeDateStart { get; set; }

        [RaisePropertyChanged]
        public DateTime? OmittedRangeTimeEnd { get; set; }

        [RaisePropertyChanged]
        public DateTime? OmittedRangeTimeStart { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<OmittedAlarmRangeViewModel> OmittedAlarmRanges { get; set; }

        [RaisePropertyChanged]
        public bool ReasonNeeded { get; set; }

        [RaisePropertyChanged]
        public double GuiRangeDelay_ { get; set; }

        [RaisePropertyChanged]
        public double GuiCommDelay_ { get; set; }

        [RaisePropertyChanged]
        public bool SmsEnabled { get; set; }

        private bool _smsRangeDelayEnabled;
        [RaisePropertyChanged]
        public bool SmsRangeDelayEnabled
        {
            get { return _smsRangeDelayEnabled; }
            set { _smsRangeDelayEnabled = value; SetEnabledStatusFroEndOfAlarms(); }
        }

        [RaisePropertyChanged]
        public bool SmsEndOfRangeEnabled { get; set; }

        [RaisePropertyChanged]
        public bool IsSmsEndOfRangeEnabled { get; set; }

        private bool _smsCommDelayEnabled;
        [RaisePropertyChanged]
        public bool SmsCommDelayEnabled
        {
            get { return _smsCommDelayEnabled; }
            set { _smsCommDelayEnabled = value; SetEnabledStatusFroEndOfAlarms(); }
        }

        [RaisePropertyChanged]
        public bool SmsEndOfCommEnabled { get; set; }

        [RaisePropertyChanged]
        public bool IsSmsEndOfCommEnabled { get; set; }

        [RaisePropertyChanged]
        public double SmsRangeDelay_ { get; set; }

        [RaisePropertyChanged]
        public double SmsCommDelay_ { get; set; }

        [RaisePropertyChanged]
        public bool EmailEnabled { get; set; }

        private bool _emailRangeDelayEnabled;
        [RaisePropertyChanged]
        public bool EmailRangeDelayEnabled
        {
            get { return _emailRangeDelayEnabled; }
            set { _emailRangeDelayEnabled = value; SetEnabledStatusFroEndOfAlarms(); }
        }

        [RaisePropertyChanged]
        public bool EmailEndOfRangeEnabled { get; set; }

        [RaisePropertyChanged]
        public bool IsEmailEndOfRangeEnabled { get; set; }

        public bool _emailCommDelayEnabled;
        [RaisePropertyChanged]
        public bool EmailCommDelayEnabled
        {
            get { return _emailCommDelayEnabled; }
            set { _emailCommDelayEnabled = value; SetEnabledStatusFroEndOfAlarms(); }
        }

        [RaisePropertyChanged]
        public bool EmailEndOfCommEnabled { get; set; }

        [RaisePropertyChanged]
        public bool IsEmailEndOfCommEnabled { get; set; }

        [RaisePropertyChanged]
        public double EmailRangeDelay { get; set; }

        [RaisePropertyChanged]
        public double EmailCommDelay { get; set; }

        [RaisePropertyChanged]
        public string Reason { get; set; }

        [RaisePropertyChanged]
        public Visibility IsRealVisible { get; set; }

        [RaisePropertyChanged]
        public Visibility IsStateVisible { get; set; }

        private string _highState;
        [RaisePropertyChanged]
        public string HighState
        {
            get { return _highState; }
            set
            {
                _highState = value;
                SetAlarmingLabel();
            }
        }

        private string _lowState;
        [RaisePropertyChanged]
        public string LowState
        {
            get { return _lowState; }
            set
            {
                _lowState = value;
                SetAlarmingLabel();
            }
        }
        private bool _highStateAlarming;


        [RaisePropertyChanged]
        public bool TestAlarmsVisible { get; set; }

        [RaisePropertyChanged]
        public bool HighStateAlarming
        {
            get
            {
                return _highStateAlarming;
            }
            set
            {
                _highStateAlarming = value;
                SetAlarmingLabel();
            }
        }

        private void SetAlarmingLabel()
        {
            if (!_sensor.IsBitSensor)
            {
                RangeAlarmLabel = "Przekroczenie zakresu";
            }
            else if (HighStateAlarming)
            {
                RangeAlarmLabel = String.Format("Stan wysoki \"{0}\"", HighState);
            }
            else
            {
                RangeAlarmLabel = String.Format("Stan niski \"{0}\"", LowState);
            }
        }

        [RaisePropertyChanged]
        public bool LowStateAlarming { get; set; }

        [RaisePropertyChanged]
        public string TestValueText { get; set; }

        [RaisePropertyChanged]
        public short TestValueMin { get; set; }

        [RaisePropertyChanged]
        public string TestValueMinText { get; set; }

        [RaisePropertyChanged]
        public short TestValueMax { get; set; }

        [RaisePropertyChanged]
        public string TestValueMaxText { get; set; }

        [RaisePropertyChanged]
        public bool TestAlarmModeForBackground { get; set; }

        [RaisePropertyChanged]
        public bool EnableValueTestControls { get; set; }

        [RaisePropertyChanged]
        public bool EnableNoSensorTestControls { get; set; }

        [RaisePropertyChanged]
        public bool NoSensorTestActive { get; set; }

        [RaisePropertyChanged]
        public bool ValueTestActive { get; set; }

        public short? TestAlarmEventType { get; set; }

        [RaisePropertyChanged]
        public short? TestAlarmValue { get; set; }

        [RaisePropertyChanged]
        public IUnit SelectedUnit { get; set; }

        #endregion Properties

        public DelegateCommand<object> ReloadUsers { get; set; }

        public DelegateCommand<object> Save { get; set; }

        public DelegateCommand<object> AddSmsReceiver { get; set; }

        public DelegateCommand<object> AddEmailReceiver { get; set; }

        public DelegateCommand<object> RefreshConfiguration { get; set; }

        public DelegateCommand<object> AddAlarmRange { get; set; }

        public DelegateCommand<object> AddSelectedScan { get; set; }

        public DelegateCommand<object> CreateMeasureDevice { get; set; }

        static ConfigurationViewModel()
        {

        }

        public ConfigurationViewModel(IUnityContainer container)
            : base(container)
        {
            this.ViewAttached += new EventHandler(UnitsListViewModel_ViewAttached);

            CreateMeasureDevice = new DelegateCommand<object>(OnCreateMeasureDevice);


            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            Save = new DelegateCommand<object>(Save_Command);
            AddSmsReceiver = new DelegateCommand<object>(AddSmsReceiver_Command);
            AddEmailReceiver = new DelegateCommand<object>(AddEmailReceiver_Command);
            RefreshConfiguration = new DelegateCommand<object>(RefreshConfiguration_Command);
            AddAlarmRange = new DelegateCommand<object>(AddAlarmRange_Command);

            EventAggregator.GetEvent<UsersContactInforBufferRefreshedEvent>().Subscribe(HandleDBBufferRefreshedEventEvent);
            EventAggregator.GetEvent<RemoveReceiverInfoEvent>().Subscribe(HandleRemoveReceiverEvent);
            EventAggregator.GetEvent<RemoveAlarmRangeEvent>().Subscribe(HandleRemoveAlarmRangeEvent);

            UsersContactInforBuffer.SetContainer(container);
            if (UsersContactInforBuffer.GetAllItems() == null)
                UsersContactInforBuffer.Reload();
            ReloadUsers = new DelegateCommand<object>(ReloadUsers_Command);

            LineStyles = new ObservableCollection<ChartLineStyle>(ChartLineStyle.GetLineStyles);

            ReasonNeeded = Global.Settings.Current.GetInt(ConfigurationKeys.ShowReasonField) == ConfigurationKeys.ENABLED;
            IsRealVisible = Visibility.Visible;
            IsStateVisible = Visibility.Collapsed;

            RepeatCanBreakVisible = Visibility.Collapsed;
            InitializeTestAlarmControls();
        }

        private void InitializeTestAlarmControls()
        {
            TestAlarmModeForBackground = false;
            EnableValueTestControls = true;
            EnableNoSensorTestControls = true;
        }

        public void RefreshConfiguration_Command(object obj)
        {
            System.Diagnostics.Debug.WriteLine("refreshing selected unit");
            Refresh(SelectedUnit);
        }

        private void AddReceiver(int receiverType)
        {
            ObservableCollection<ReceiverViewModel> list;
            UserContactInfoDto user;

            if (receiverType == ReceiverViewModel.SMS_RECEIVER)
            {
                list = SmsReceivers;
                user = SmsSelectedReceiver;
            }
            else
            {
                list = EmailReceivers;
                user = MailSelectedReceiver;
            }


            bool exists = false;
            for (int i = 0; i < list.Count && !exists; i++)
            {
                if (list[i].Contact.Id == user.Id) exists = true;
            }

            if (!exists)
            {
                var model = Container.Resolve<ReceiverViewModel>();
                model.Contact = user;
                model.ReceiverType = receiverType;
                list.Add(model);
                var newList = list.ToObservableCollection();
                list = newList;
            }
        }

        public void AddSmsReceiver_Command(object obj)
        {
            AddReceiver(ReceiverViewModel.SMS_RECEIVER);
        }

        public void AddEmailReceiver_Command(object obj)
        {
            AddReceiver(ReceiverViewModel.EMAIL_RECEIVER);
        }

        public void Save_Command(object obj)
        {
            if (_testAlarmsSaved)
            {
                ProcessConfiguration();
                _testAlarmsSaved = false;
            }
            else
            {
                if (!SaveTestAlarmChanges())
                    ProcessConfiguration();
                else
                    _testAlarmsSaved = true;
            }
        }

        private void ProcessConfiguration()
        {
            var s = GetError();
            if (!string.IsNullOrEmpty(s))
            {
                MsgBox.Error(s);
                return;
            }

            var logInfo = CreateLogInfo();

            if (logInfo == "")
            {
                MsgBox.Warning("Nie dokonano zmian.");
                return;
            }

            var sensor = CreateSensorChangeInfo();

            var omittedRanges = CreateOmittedRanges();

            var smsReceivers = SmsReceivers.Select(o => o.Contact.Id).ToList().ToCommaSeparatedValues();
            var emailReceivers = EmailReceivers.Select(o => o.Contact.Id).ToList().ToCommaSeparatedValues();

            //var testAlarmChanges = SaveTestAlarmChanges();

            //            if (!testAlarmChanges)
            LoadService.UpdateSensorAsync(sensor, omittedRanges, smsReceivers, emailReceivers, Reason, logInfo);
            //else
            //{
            //    var alarmLog = "";
            //    SaveOperationLog(BloodyUser.Current.ID, SelectedUnit.Identity, string.Empty, alarmLog);
            //}
        }

        private string CreateLogInfo()
        {
            var newSocket = (SelectedSocket == null) ? (int?)null : SelectedSocket.Id;

            var logInfo = "";

            logInfo = logInfo.LogEdit("Gniazdo", GetOldSocketName(), _sensor.SocketId, GetNewSocketName(), newSocket)
                .LogEdit("Interwał", ((int)(_sensor.Interval)).ToString(), ((int)(Interval * 1000)).ToString())
                .LogEdit("Kalibracja", _sensor.Calibration.ToString(), Calibration.ToString())
                .LogEdit("Aktywność", _sensor.Enabled.ToYesNoString(), Enabled.ToYesNoString());
            if (_sensor.IsBitSensor)
            {
                logInfo = logInfo.LogEdit("Stan wysoki", _sensor.HiStateDesc, HighState)
                    .LogEdit("Stan niski", _sensor.LoStateDesc, LowState)
                    .LogEdit("Wysoki stan, stanem alarmowym", _sensor.IsHighAlarming.ToYesNoString(),
                        HighStateAlarming.ToYesNoString());
            }
            else
            {
                logInfo = logInfo.LogEdit("Jednostka miary", _sensor.MU, MU)
                    .LogEdit("Dolny zakres", _sensor.LoRange.ToString(), LoRange.ToString())
                    .LogEdit("Górny zakres", _sensor.UpRange.ToString(), UpRange.ToString());
            }
            logInfo = logInfo.LogEdit("Wyłącz alarmowanie", _sensor.AlDisabled.ToYesNoString(), AlDisabled.ToYesNoString());
            logInfo = logInfo.LogEdit("Wyłącz  alarmowanie w zakresach godzin", _sensor.AlDisabledForHours.ToYesNoString(), AlDisabledForHours.ToYesNoString());
            logInfo = logInfo.LogEdit("Pierwszy przedział godzin", GetTimeRangeStr(_sensor.AlDisableHoursFrom1, _sensor.AlDisableHoursTo1), GetTimeRangeStr(AlDisableHoursFrom1, AlDisableHoursTo1));
            logInfo = logInfo.LogEdit("Drugi przedział godzin", GetTimeRangeStr(_sensor.AlDisableHoursFrom2, _sensor.AlDisableHoursTo2), GetTimeRangeStr(AlDisableHoursFrom2, AlDisableHoursTo2));
            logInfo = logInfo.LogEdit("Wyłącz alarmowanie w zakresach", _sensor.AlDisabledForRanges.ToYesNoString(), AlDisabledForRanges.ToYesNoString());
            logInfo = logInfo.LogEdit("Zakresy", RangesToString(_ranges), RangesToString(OmittedAlarmRanges));
            logInfo = logInfo.LogEdit("Powiadomienia przekroczenia zakresu na ekran", _sensor.GuiRangeDelay.ToString(), ((short)GuiRangeDelay).ToString());
            logInfo = logInfo.LogEdit("Wznawianie alarmów przekroczenia zakresu", _sensor.GuiRangeRepeat.ToYesNoString(), GuiRangeRepeat.ToYesNoString());
            logInfo = logInfo.LogEdit("Czas wznowienia alarmów przekroczenia zakresu", _sensor.GuiRangeRepeatMins.ToString(), ((short)GuiRangeRepeatMins).ToString());
            logInfo = logInfo.LogEdit("Przerywanie wznowień alarmów przekroczenia zakresu", _sensor.GuiRangeRepeatBreakable.ToYesNoString(), GuiRangeRepeatBreakable.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadomienia utraty łączności na ekran", _sensor.GuiCommDelay.ToString(), ((short)GuiCommDelay).ToString());
            logInfo = logInfo.LogEdit("Wznawianie alarmów utraty łączności", _sensor.GuiCommRepeat.ToYesNoString(), GuiCommRepeat.ToYesNoString());
            logInfo = logInfo.LogEdit("Czas wznowienia alarmów utraty łączności", _sensor.GuiCommRepeatMins.ToString(), ((short)GuiCommRepeatMins).ToString());
            logInfo = logInfo.LogEdit("Przerywanie wznowień alarmów utraty łączności", _sensor.GuiCommRepeatBreakable.ToYesNoString(), GuiCommRepeatBreakable.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadomienia przez SMS o przekroczeniu zakresu", _sensor.SmsRangeEnabled.ToYesNoString(), SmsRangeEnabled.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadomienia SMS (zakres) po czasie", GetDelayText(_sensor.SmsRangeDelay, _sensor.SmsRangeEnabled), GetDelayText(SmsRangeDelay, SmsRangeEnabled));
            logInfo = logInfo.LogEdit("Powiadomienia SMS po zakończeniu (zakres)", _sensor.SmsRangeAfterFinish.ToYesNoString(), SmsRangeAfterFinish.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadomienia SMS po zamknięciu (zakres)", _sensor.SmsRangeAfterClose.ToYesNoString(), SmsRangeAfterClose.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadamianie cykliczne SMS (zakres)", _sensor.SmsRangeRepeat.ToYesNoString(), SmsRangeRepeat.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadamianie cykliczne SMS (zakres) co", _sensor.SmsRangeRepeatMins.ToString(), ((short)SmsRangeRepeatInterval).ToString());
            logInfo = logInfo.LogEdit("Powiadamianie cykliczne SMS (zakres) aż do", UntilString.GetUntilText(_sensor.SmsRangeRepeatUntil), UntilString.GetUntilText(SmsRangeRepeatUntil));
            logInfo = logInfo.LogEdit("Przerywanie powiadomień cyklicznych SMS (zakres)", _sensor.SmsRangeRepeatBreakable.ToYesNoString(), SmsRangeRepeatCanBreak.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadomienia przez SMS o braku łączności", _sensor.SmsCommEnabled.ToYesNoString(), SmsCommEnabled.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadomienia SMS (łączność) po czasie", GetDelayText(_sensor.SmsCommDelay, _sensor.SmsCommEnabled), GetDelayText(SmsCommDelay, SmsCommEnabled));
            logInfo = logInfo.LogEdit("Powiadomienia SMS po zakończenieu (łączność)", _sensor.SmsCommAfterFinish.ToYesNoString(), SmsCommAfterFinish.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadomienia SMS po zamknięciu (łączność)", _sensor.SmsCommAfterClose.ToYesNoString(), SmsCommAfterFinish.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadamianie cykliczne SMS (łączność)", _sensor.SmsCommRepeat.ToYesNoString(), SmsCommRepeat.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadamianie cykkliczne SMS (łącznosć) co", _sensor.SmsCommRepeatMins.ToString(), ((short)SmsCommRepeatInterval).ToString());
            logInfo = logInfo.LogEdit("Powiadamianie cylkiczne SMS (łączność) aż do", UntilString.GetUntilText(_sensor.SmsCommRepeatUntil), UntilString.GetUntilText(SmsCommRepeatUntil));
            logInfo = logInfo.LogEdit("Przerywanie powiadomień cyklicznych SMS (łączność)", _sensor.SmsCommRepeatBreakable.ToYesNoString(), SmsCommRepeatCanBreak.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadomienia przez email o przekroczeniu zakresu", _sensor.MailRangeEnabled.ToYesNoString(), MailRangeEnabled.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadomienia email (zakres) po czasie", GetDelayText(_sensor.MailRangeDelay, _sensor.MailRangeEnabled), GetDelayText(MailRangeDelay, MailRangeEnabled));
            logInfo = logInfo.LogEdit("Powiadomienia email po zakończeniu (zakres)", _sensor.MailRangeAfterFinish.ToYesNoString(), MailRangeAfterFinish.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadomienia email po zamknięciu (zakres)", _sensor.MailRangeAfterClose.ToYesNoString(), MailRangeAfterClose.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadamianie cykliczne email (zakres)", _sensor.MailRangeRepeat.ToYesNoString(), MailRangeRepeat.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadamianie cykliczne email (zakres) co", _sensor.MailRangeRepeatMins.ToString(), ((short)MailRangeRepeatInterval).ToString());
            logInfo = logInfo.LogEdit("Powiadamianie cykliczne email (zakres) aż do", UntilString.GetUntilText(_sensor.MailRangeRepeatUntil), UntilString.GetUntilText(MailRangeRepeatUntil));
            logInfo = logInfo.LogEdit("Przerywanie powiadomień cylkicznych email (zakres)", _sensor.MailRangeRepeatBreakable.ToYesNoString(), MailRangeRepeatCanBreak.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadomienia przez email o braku łączności", _sensor.MailCommEnabled.ToYesNoString(), MailCommEnabled.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadomienia email (łączność) po czasie", GetDelayText(_sensor.MailCommDelay, _sensor.MailCommEnabled), GetDelayText(MailCommDelay, MailCommEnabled));
            logInfo = logInfo.LogEdit("Powiadomienie email po zakończeniu (łączność)", _sensor.MailCommAfterFinish.ToYesNoString(), MailCommAfterFinish.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadomienie email po zamknięciu (łączność)", _sensor.MailCommAfterClose.ToYesNoString(), MailCommAfterFinish.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadamianie cykliczne email (łączność)", _sensor.MailCommRepeat.ToYesNoString(), MailCommRepeat.ToYesNoString());
            logInfo = logInfo.LogEdit("Powiadamianie cykliczne email (łączność) co", _sensor.MailCommRepeatMins.ToString(), ((short)MailCommRepeatInterval).ToString());
            logInfo = logInfo.LogEdit("Powiadamianie cykliczne email (łączność) aż do", UntilString.GetUntilText(_sensor.MailCommRepeatUntil), UntilString.GetUntilText(MailCommRepeatUntil));
            logInfo = logInfo.LogEdit("Przerywanie powiadomień cyklicznych email (łączność)", _sensor.MailCommRepeatBreakable.ToYesNoString(), MailCommRepeatCanBreak.ToYesNoString());
            logInfo = logInfo.LogEdit("Odbiorcy SMS'ów", GetOldSmsReceivers(), GetNewReceivers(SmsReceivers));
            logInfo = logInfo.LogEdit("Odbiorcy e-mail'i", GetOldEmailReceivers(), GetNewReceivers(EmailReceivers));
            logInfo = logInfo.LogEdit("Legenda, linia danych", _sensor.LegendDescription, LegendDescription);
            logInfo = logInfo.LogEdit("Legenda, skrót", _sensor.LegendShortcut, LegendShortcut);
            logInfo = logInfo.LogEdit("Legenda, linia zakresu", _sensor.LegendRangeDescription, LegendRangeDescription);
            logInfo = logInfo.LogEdit("Linia danych, kolor", _sensor.LineColor, LineColor.ToRGBString());
            logInfo = logInfo.LogEdit("Linia danych, grubość", _sensor.LineWidth.ToString(), ((short)LineWidth).ToString());
            logInfo = logInfo.LogEdit("Linia danych, styl", _sensor.LineStyle, LineStyle.ToString());
            logInfo = logInfo.LogEdit("Linia zakresu, grubość", _sensor.RangeLineWidth.ToString(), ((short)RangeLineWidth).ToString());
            logInfo = logInfo.LogEdit("Linia zakresu, styl", _sensor.RangeLineStyle, RangeLineStyle.ToString());
            logInfo = logInfo.LogEdit("Alarmy, kolor", _sensor.AlarmColor, AlarmColor.ToRGBString());
            logInfo = logInfo.LogEdit("Test powiadomień, tryb", GetTestAlarmModeDescription(_sensor.TestAlarmEventType), GetTestAlarmModeDescription(TestAlarmEventType));
            logInfo = logInfo.LogEdit("Test powiadomień, wartość", GetNoValueText(_sensor.TestAlarmValue.ToString()), GetNoValueText(TestAlarmValue.ToString()));

            return logInfo;
        }

        private string CreateOmittedRanges()
        {
            string omittedRanges = "";
            foreach (var range in OmittedAlarmRanges)
            {
                omittedRanges = omittedRanges + (
                    (omittedRanges == "")
                        ? FormatRangeToStr(range.Range.DateStart, range.Range.DateEnd)
                        : "," + FormatRangeToStr(range.Range.DateStart, range.Range.DateEnd));
            }
            return omittedRanges;
        }

        private EditSensorDto CreateSensorChangeInfo()
        {
            var sensor = new EditSensorDto
            {
                ID = _sensor.ID,
                SocketId = (SelectedSocket == null) ? (int?)null : SelectedSocket.Id,
                Interval = (int)this.Interval * 1000,
                Calibration = this.Calibration,
                Enabled = this.Enabled,
                MU = this.MU,
                LoRange = this.LoRange,
                UpRange = this.UpRange,
                LegendDescription = this.LegendDescription,
                LegendShortcut = this.LegendShortcut,
                LegendRangeDescription = this.LegendRangeDescription,
                LineColor = this.LineColor.ToRGBString(),
                LineWidth = (byte)this.LineWidth,
                LineStyle = this.LineStyle.ToString(),
                RangeLineWidth = (byte)this.RangeLineWidth,
                RangeLineStyle = this.RangeLineStyle.ToString(),
                AlarmColor = this.AlarmColor.ToRGBString(),
                AlarmCommColor = _sensor.AlarmCommColor,
                AlDisabled = this.AlDisabled,
                AlDisabledForRanges = this.AlDisabledForRanges,
                AlDisabledForHours = this.AlDisabledForHours,
                AlDisableHoursFrom1 = this.AlDisableHoursFrom1,
                AlDisableHoursTo1 = this.AlDisableHoursTo1,
                AlDisableHoursFrom2 = this.AlDisableHoursFrom2,
                AlDisableHoursTo2 = this.AlDisableHoursTo2,
                GuiRangeDelay = (short)this.GuiRangeDelay,
                GuiCommDelay = (short)this.GuiCommDelay,
                SmsEnabled = true,
                SmsRangeDelay = (short)this.SmsRangeDelay,
                SmsCommDelay = (short)this.SmsCommDelay,
                MailEnabled = true,
                MailRangeDelay = (short)this.MailRangeDelay,
                MailCommDelay = (short)this.MailCommDelay,
                IsBitSensor = _sensor.IsBitSensor,
                IsHighAlarming = this.HighStateAlarming,
                HiStateDesc = this.HighState,
                LoStateDesc = this.LowState,
                GuiRangeRepeat = this.GuiRangeRepeat,
                GuiRangeRepeatMins = (short)this.GuiRangeRepeatMins,
                GuiRangeRepeatBreakable = this.GuiRangeRepeatBreakable,
                GuiCommRepeat = this.GuiCommRepeat,
                GuiCommRepeatMins = (short)this.GuiCommRepeatMins,
                GuiCommRepeatBreakable = this.GuiCommRepeatBreakable,
                SmsRangeEnabled = this.SmsRangeEnabled,
                SmsRangeAfterFinish = this.SmsRangeAfterFinish,
                SmsRangeAfterClose = this.SmsRangeAfterClose,
                SmsRangeRepeat = this.SmsRangeRepeat,
                SmsRangeRepeatMins = (short)this.SmsRangeRepeatInterval,
                SmsRangeRepeatUntil = UntilString.FromInt(SmsRangeRepeatUntil),
                SmsRangeRepeatBreakable = this.SmsRangeRepeatCanBreak,
                SmsCommEnabled = this.SmsCommEnabled,
                SmsCommAfterFinish = this.SmsCommAfterFinish,
                SmsCommAfterClose = this.SmsCommAfterClose,
                SmsCommRepeat = this.SmsCommRepeat,
                SmsCommRepeatMins = (short)this.SmsCommRepeatInterval,
                SmsCommRepeatUntil = UntilString.FromInt(SmsCommRepeatUntil),
                SmsCommRepeatBreakable = this.SmsCommRepeatCanBreak,
                MailRangeEnabled = this.MailRangeEnabled,
                MailRangeAfterFinish = this.MailRangeAfterFinish,
                MailRangeAfterClose = this.MailRangeAfterClose,
                MailRangeRepeat = this.MailRangeRepeat,
                MailRangeRepeatMins = (short)this.MailRangeRepeatInterval,
                MailRangeRepeatUntil = UntilString.FromInt(MailRangeRepeatUntil),
                MailRangeRepeatBreakable = this.MailRangeRepeatCanBreak,
                MailCommEnabled = this.MailCommEnabled,
                MailCommAfterFinish = this.MailCommAfterFinish,
                MailCommAfterClose = this.MailCommAfterClose,
                MailCommRepeat = this.MailCommRepeat,
                MailCommRepeatMins = (short)this.MailCommRepeatInterval,
                MailCommRepeatUntil = UntilString.FromInt(MailCommRepeatUntil),
                MailCommRepeatBreakable = this.MailCommRepeatCanBreak
            };
            return sensor;
        }

        private bool SaveTestAlarmChanges()
        {
            var testAlarmChanges = false;

            TestAlarmEventType = SetTestModeFromControls();

            if (IsTestAlarmEventTypeChanged())
            {
                var mode = GetTestModeFromEventType(TestAlarmEventType);
                var oldMode = GetTestModeFromEventType(_sensor.TestAlarmEventType);

                switch (mode)
                {
                    case TestMode.NoTest:
                        if (oldMode == TestMode.ValueTest)
                            LoadService.ToggleValueTestAlarmAsync(false, _sensor.ID, 0, ValueEvent);

                        if (oldMode == TestMode.NoSensorTest)
                            LoadService.ToggleNoSensorTestAlarmAsync(false, _sensor.ID, NoSensorEvent);

                        TestAlarmEventType = null;
                        break;
                    case TestMode.ValueTest:
                        TestAlarmEventType = ValueEvent;
                        LoadService.ToggleValueTestAlarmAsync(true, _sensor.ID, TestAlarmValue.Value, ValueEvent);
                        break;
                    case TestMode.NoSensorTest:
                        TestAlarmEventType = NoSensorEvent;
                        LoadService.ToggleNoSensorTestAlarmAsync(true, _sensor.ID, NoSensorEvent);
                        break;
                }
                testAlarmChanges = true;
            }

            if (ValueTestActive && !testAlarmChanges && IsTestAlarmValueChanged())
            {
                LoadService.SetAlarmTestValueAsync(TestAlarmValue.Value, _sensor.ID);
                testAlarmChanges = true;
            }

            return testAlarmChanges;
        }

        private static TestMode GetTestModeFromEventType(short? value)
        {
            return !value.HasValue ? TestMode.NoTest : ((value.Value == 0) ? TestMode.ValueTest : TestMode.NoSensorTest);
        }

        private static short? GetEventTypeFromTestMode(TestMode mode)
        {
            switch (mode)
            {
                case TestMode.NoTest:
                    return null;
                case TestMode.ValueTest:
                    return 0;
                case TestMode.NoSensorTest:
                    return 3;
            }
            return null;
        }

        private string FormatRangeToStr(DateTime start, DateTime end)
        {
            return start.ToSQLstring() + "do" + end.ToSQLstring();
        }

        private string RangesToString(ObservableCollection<OmittedAlarmRangeViewModel> ranges)
        {
            return RangesToString(ranges.Select(o => o.Range).ToList());
        }

        private string RangesToString(IList<OmittedAlarmRangeDto> ranges)
        {
            List<OmittedAlarmRangeDto> list = ranges.OrderBy(o => o.DateStart).ToList();
            var result = "";
            foreach (var range in list)
            {
                var rangestr = "(" + range.DateStart.ToSQLstring() + ")-(" + range.DateEnd.ToSQLstring() + ")";

                result = result + ((result == "") ? rangestr : ", " + rangestr);
            }
            return "{" + result + "}";
        }

        private string GetTimeRangeStr(DateTime? start, DateTime? end)
        {
            if (start.HasValue && end.HasValue)
                return "(" + start.Value.ToShortTimeString() + "-" + end.Value.ToShortTimeString() + ")";
            else return "";
        }

        private string GetNewReceivers(ObservableCollection<ReceiverViewModel> receivers)
        {
            var list = new List<int>();
            foreach (var i in receivers)
            {
                list.Add(i.Contact.Id);
            }

            return GetUsersStrList(list);
        }

        private string GetOldEmailReceivers()
        {
            var list = new List<int>();
            foreach (var i in _notifications)
            {
                if (i.Email) list.Add(i.UserId);
            }

            return GetUsersStrList(list);
        }

        private string GetOldSmsReceivers()
        {
            var list = new List<int>();
            foreach (var i in _notifications)
            {
                if (i.Sms) list.Add(i.UserId);
            }

            return GetUsersStrList(list);
        }

        private string GetUsersStrList(List<int> list)
        {
            string result = "";
            var sorted = list.OrderBy(o => o);
            foreach (var i in sorted)
            {
                var user = UsersContactInforBuffer.GetItem(i);
                var usrStr = user.LastName + " " + user.Name + "<" + i.ToString() + ">";
                result = result + ((result == "") ? usrStr : ", " + usrStr);
            }
            return "{" + result + "}";
        }

        private string GetDelayText(double? value, bool set)
        {

            if (set && value.HasValue)
                return ((short)value).ToString();
            else
                return NOT_SET;
        }

        private string GetDelayText(short? value)
        {
            if (value.HasValue)
                return value.Value.ToString();
            else
                return NOT_SET;
        }

        private string GetNewSocketName()
        {
            if (SelectedSocket == null)
                return NOT_CONNECTED;
            else
                return SelectedSocket.Name;
        }

        private string GetOldSocketName()
        {
            if (_sensor.SocketId == null) return NOT_CONNECTED;
            var result = "";
            for (int i = 0; i < PossibleSockets.Count && result == ""; i++)
            {
                if (PossibleSockets[i].Id == _sensor.SocketId.Value)
                    result = PossibleSockets[i].Name;
            }
            return result;
        }

        private string GetError()
        {
            if (LoRange > UpRange)
                return "Zakres pomiarowy." + Environment.NewLine + Environment.NewLine + "Dolna granica zakresu nie może być większa od górnej.";

            if (AlDisableHoursFrom1.HasValue != AlDisableHoursTo1.HasValue)
                return "Cykliczne alarmowanie." + Environment.NewLine + Environment.NewLine + "Podano niepełny przedział(1) godzin.";

            if (AlDisableHoursFrom2.HasValue != AlDisableHoursTo2.HasValue)
                return "Cykliczne alarmowanie." + Environment.NewLine + Environment.NewLine + "Podano niepełny przedział(2) godzin.";

            if (AlDisableHoursFrom1.HasValue && AlDisableHoursTo1.HasValue)
                if (AlDisableHoursFrom1.Value >= AlDisableHoursTo1.Value)
                    return "Cykliczne alarmowanie." + Environment.NewLine + Environment.NewLine + "Godzina początkowa przedziału(1) musi być mniejsza od końcowej.";

            if (AlDisableHoursFrom2.HasValue && AlDisableHoursTo2.HasValue)
                if (AlDisableHoursFrom2.Value >= AlDisableHoursTo2.Value)
                    return "Cykliczne alarmowanie." + Environment.NewLine + Environment.NewLine + "Godzina początkowa przedziału(2) musi być mniejsza od końcowej.";

            return "";
        }

        public event EventHandler LoadingCompleted = delegate { };

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        private UnitsServiceClient LoadService
        {
            get
            {
                UnitsServiceClient proxy = ServiceFactory.GetService<UnitsServiceClient>(ServicesUri.UnitsService);

                proxy.GetSensorForUnitCompleted += new EventHandler<GetSensorForUnitCompletedEventArgs>(proxy_GetSensorForUnitCompleted);
                proxy.GetPossibleSocketsForUnitCompleted += new EventHandler<GetPossibleSocketsForUnitCompletedEventArgs>(proxy_GetPossibleSocketsForUnitCompleted);
                proxy.GetNotificationReceiversForUnitCompleted += new EventHandler<GetNotificationReceiversForUnitCompletedEventArgs>(proxy_GetNotificationReceiversForUnitCompleted);
                proxy.GetOmittedAlarmRangedForUnitCompleted += new EventHandler<GetOmittedAlarmRangedForUnitCompletedEventArgs>(proxy_GetOmittedAlarmRangedForUnitCompleted);
                proxy.UpdateSensorCompleted += new EventHandler<UpdateSensorCompletedEventArgs>(proxy_UpdateSensorCompleted);

                proxy.ToggleValueTestAlarmCompleted += Proxy_ToggleValueTestAlarmCompleted;
                proxy.ToggleNoSensorTestAlarmCompleted += Proxy_ToggleNoSensorTestAlarmCompleted;
                proxy.SetAlarmTestValueCompleted += Proxy_SetTestAlarmValueCompleted;

                return proxy;
            }
        }

        private void proxy_UpdateSensorCompleted(object sender, UpdateSensorCompletedEventArgs e)
        {
            if (e.Result < 0)
            {
                MsgBox.Error("Edycja sensora nie powiodła się.");
            }
            else
            {
                Refresh(SelectedUnit);
            }
        }

        public void AddAlarmRange_Command(object o)
        {
            if (OmittedRangeDateEnd.HasValue && OmittedRangeDateStart.HasValue /*&& Device != null*/)
            {
                if (!OmittedRangeTimeStart.HasValue)
                {
                    OmittedRangeTimeStart = DateTime.Now.Date + new TimeSpan(0, 0, 0);
                }
                if (!OmittedRangeTimeEnd.HasValue)
                {
                    OmittedRangeTimeEnd = DateTime.Now.Date + new TimeSpan(23, 59, 59);
                }
                DateTime dateStart = new DateTime(OmittedRangeDateStart.Value.Year, OmittedRangeDateStart.Value.Month, OmittedRangeDateStart.Value.Day, OmittedRangeTimeStart.Value.Hour, OmittedRangeTimeStart.Value.Minute, OmittedRangeTimeStart.Value.Second);
                DateTime dateEnd = new DateTime(OmittedRangeDateEnd.Value.Year, OmittedRangeDateEnd.Value.Month, OmittedRangeDateEnd.Value.Day, OmittedRangeTimeEnd.Value.Hour, OmittedRangeTimeEnd.Value.Minute, OmittedRangeTimeEnd.Value.Second);
                if (dateStart < DateTime.Now)
                {
                    MsgBox.Error("Nie można ustawić daty wstecznej.");
                    return;
                }
                if (dateEnd <= dateStart)
                {
                    MsgBox.Error("Data końcowa musi być późniejsza od początkowej.");
                    return;
                }


                var range = new OmittedAlarmRangeDto() { DateStart = dateStart, DateEnd = dateEnd };
                var model = Container.Resolve<OmittedAlarmRangeViewModel>();
                model.Range = range;
                OmittedAlarmRanges.Add(model);

                var list = new ObservableCollection<OmittedAlarmRangeViewModel>(OmittedAlarmRanges);
                OmittedAlarmRanges = list;
            }
        }

        public void ReloadUsers_Command(object o)
        {
            UsersContactInforBuffer.Reload(SelectedUnit.Identity);
        }

        public void HandleDBBufferRefreshedEventEvent(int i)
        {
            UsersList = UsersContactInforBuffer.GetAllItems().ToObservableCollection();
            SmsSelectedReceiver = UsersList[0];
            MailSelectedReceiver = UsersList[0];

            UpdateNotifications();
        }

        public void HandleRemoveReceiverEvent(Tuple<object, int> item)
        {
            ReceiverViewModel vm = item.Item1 as ReceiverViewModel;
            ObservableCollection<ReceiverViewModel> list;
            if (item.Item2 == ReceiverViewModel.SMS_RECEIVER)
            {
                list = new ObservableCollection<ReceiverViewModel>(SmsReceivers);
                list.Remove(vm);
                SmsReceivers = list;
            }
            else
            {
                list = new ObservableCollection<ReceiverViewModel>(EmailReceivers);
                list.Remove(vm);
                EmailReceivers = list;
            }
        }

        public void HandleRemoveAlarmRangeEvent(object item)
        {
            OmittedAlarmRangeViewModel vm = item as OmittedAlarmRangeViewModel;
            var list = new ObservableCollection<OmittedAlarmRangeViewModel>(OmittedAlarmRanges);
            list.Remove(vm);
            OmittedAlarmRanges = list;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private Color GetColor(string param)
        {
            Color c;
            try
            {
                var t = param.Split(new Char[] { ',', '.' });
                c = Color.FromArgb(255, byte.Parse(t[0]), byte.Parse(t[1]), byte.Parse(t[2]));
            }
            catch
            {
                c = Color.FromArgb(255, 0, 0, 0);
            }
            return c;
        }

        private Color LightColor(Color color)
        {
            int per = Global.Settings.Current.GetInt(Global.ConfigurationKeys.RangeColorIntensityPercent);
            if (per < 0 || per > 100) per = 50; //domyślne nasycenie koloru lini zakresu - 50%
            float correctionFactor = (float)per / 100;

            float red = (255 - color.R) * correctionFactor + color.R;
            float green = (255 - color.G) * correctionFactor + color.G;
            float blue = (255 - color.B) * correctionFactor + color.B;
            return Color.FromArgb(color.A, (byte)red, (byte)green, (byte)blue);
        }

        private void ClearData()
        {
            _sensor = null;
            PossibleSockets = null;
            SmsReceivers = null;
            EmailReceivers = null;
            OmittedAlarmRanges = null;
            _testAlarmsSaved = false;
        }

        private void SetData(EditSensorDto sensor)
        {
            _sensor = sensor;

            if (_sensor.IsBitSensor)
            {
                IsStateVisible = Visibility.Visible;
                IsRealVisible = Visibility.Collapsed;
            }
            else
            {
                IsRealVisible = Visibility.Visible;
                IsStateVisible = Visibility.Collapsed;
            }

            Interval = _sensor.Interval / 1000;
            Calibration = _sensor.Calibration;
            Enabled = _sensor.Enabled;

            MU = _sensor.MU;
            LoRange = _sensor.LoRange;
            UpRange = _sensor.UpRange;

            LegendDescription = _sensor.LegendDescription;
            LegendShortcut = _sensor.LegendShortcut;
            LegendRangeDescription = _sensor.LegendRangeDescription;

            LineColor = GetColor(_sensor.LineColor);
            LineWidth = _sensor.LineWidth;
            LineStyle = ChartLineStyle.GetStyle(_sensor.LineStyle);

            RangeLineColor = LightColor(GetColor(_sensor.LineColor));
            RangeLineWidth = _sensor.RangeLineWidth;
            RangeLineStyle = ChartLineStyle.GetStyle(_sensor.RangeLineStyle);

            AlarmColor = GetColor(_sensor.AlarmColor);

            AlDisabled = _sensor.AlDisabled;
            AlDisabledForHours = _sensor.AlDisabledForHours;
            AlDisableHoursFrom1 = _sensor.AlDisableHoursFrom1;
            AlDisableHoursTo1 = _sensor.AlDisableHoursTo1;
            AlDisableHoursFrom2 = _sensor.AlDisableHoursFrom2;
            AlDisableHoursTo2 = _sensor.AlDisableHoursTo2;
            AlDisabledForRanges = _sensor.AlDisabledForRanges;

            HighState = _sensor.HiStateDesc;
            LowState = _sensor.LoStateDesc;
            HighStateAlarming = _sensor.IsHighAlarming;
            LowStateAlarming = !HighStateAlarming;

            /*
            GuiRangeDelay = _sensor.GuiRangeDelay;
            GuiCommDelay = _sensor.GuiCommDelay;

            SmsEnabled = _sensor.SmsEnabled;
            SmsRangeDelayEnabled = _sensor.SmsRangeDelay.HasValue;
            SmsCommDelayEnabled = _sensor.SmsCommDelay.HasValue;
            SmsRangeDelay = (_sensor.SmsRangeDelay.HasValue) ? _sensor.SmsRangeDelay.Value : 0;
            SmsCommDelay = (_sensor.SmsCommDelay.HasValue) ? _sensor.SmsCommDelay.Value : 0;
            EmailEnabled = _sensor.MailEnabled;
            EmailRangeDelayEnabled = _sensor.MailRangeDelay.HasValue;
            EmailCommDelayEnabled = _sensor.MailCommDelay.HasValue;
            EmailRangeDelay = (_sensor.MailRangeDelay.HasValue) ? _sensor.MailRangeDelay.Value : 0;
            EmailCommDelay = (_sensor.MailCommDelay.HasValue) ? _sensor.MailCommDelay.Value : 0;
             */

            SmsRangeEnabled = _sensor.SmsRangeEnabled;
            SmsRangeDelay = (_sensor.SmsRangeDelay.HasValue) ? _sensor.SmsRangeDelay.Value : 0;
            SmsRangeAfterFinish = _sensor.SmsRangeAfterFinish;
            SmsRangeAfterClose = _sensor.SmsRangeAfterClose;
            SmsRangeRepeat = _sensor.SmsRangeRepeat;
            SmsRangeRepeatInterval = _sensor.SmsRangeRepeatMins;
            SmsRangeRepeatUntil = UntilString.GetInt(_sensor.SmsRangeRepeatUntil);
            SmsRangeRepeatCanBreak = _sensor.SmsRangeRepeatBreakable;

            SmsCommEnabled = _sensor.SmsCommEnabled;
            SmsCommDelay = (_sensor.SmsCommDelay.HasValue) ? _sensor.SmsCommDelay.Value : 0;
            SmsCommAfterFinish = _sensor.SmsCommAfterFinish;
            SmsCommAfterClose = _sensor.SmsCommAfterClose;
            SmsCommRepeat = _sensor.SmsCommRepeat;
            SmsCommRepeatInterval = _sensor.SmsCommRepeatMins;
            SmsCommRepeatUntil = UntilString.GetInt(_sensor.SmsCommRepeatUntil);
            SmsCommRepeatCanBreak = _sensor.SmsCommRepeatBreakable;

            MailRangeEnabled = _sensor.MailRangeEnabled;
            MailRangeDelay = (_sensor.MailRangeDelay.HasValue) ? _sensor.MailRangeDelay.Value : 0;
            MailRangeAfterFinish = _sensor.MailRangeAfterFinish;
            MailRangeAfterClose = _sensor.MailRangeAfterClose;
            MailRangeRepeat = _sensor.MailRangeRepeat;
            MailRangeRepeatInterval = _sensor.MailRangeRepeatMins;
            MailRangeRepeatUntil = UntilString.GetInt(_sensor.MailRangeRepeatUntil);
            MailRangeRepeatCanBreak = _sensor.MailRangeRepeatBreakable;

            MailCommEnabled = _sensor.MailCommEnabled;
            MailCommDelay = (_sensor.MailCommDelay.HasValue) ? _sensor.MailCommDelay.Value : 0;
            MailCommAfterFinish = _sensor.MailCommAfterFinish;
            MailCommAfterClose = _sensor.MailCommAfterClose;
            MailCommRepeat = _sensor.MailCommRepeat;
            MailCommRepeatInterval = _sensor.MailCommRepeatMins;
            MailCommRepeatUntil = UntilString.GetInt(_sensor.MailCommRepeatUntil);
            MailCommRepeatCanBreak = _sensor.MailCommRepeatBreakable;

            GuiRangeDelay = _sensor.GuiRangeDelay;
            GuiCommDelay = _sensor.GuiCommDelay;
            GuiRangeRepeat = _sensor.GuiRangeRepeat;
            GuiRangeRepeatMins = _sensor.GuiRangeRepeatMins;
            GuiRangeRepeatBreakable = _sensor.GuiRangeRepeatBreakable;
            GuiCommRepeat = _sensor.GuiCommRepeat;
            GuiCommRepeatMins = _sensor.GuiCommRepeatMins;
            GuiCommRepeatBreakable = _sensor.GuiCommRepeatBreakable;
            TestAlarmEventType = _sensor.TestAlarmEventType;
            TestAlarmValue = _sensor.TestAlarmValue;

            SetInitialTestAlarmValues(_sensor);

            if (BloodyUser.Current.HasPermission(SelectedUnit.Identity, PermissionNames.Admin))
            {
                SettingsEnabled = true;
            }
            if (BloodyUser.Current.HasPermission(SelectedUnit.Identity, PermissionNames.SuperUser))
            {
                RangeEnabled = true;
                AlarmingEnabled = true;
                NotificationsEnabled = true;
                ChartEnabled = true;
                SaveEnabled = true;
            }
        }

        private void SetInitialTestAlarmValues(EditSensorDto sensor)
        {
            TestAlarmsVisible = BloodyUser.Current.IsInRole(PermissionNames.Admin);

            SetInitialTestAlarmsForControls(GetTestModeFromEventType(sensor.TestAlarmEventType));

            if (_sensor.IsBitSensor)
            {
                TestValueMin = 0;
                TestValueMax = 1;
                //TestAlarmValue = (short) (_sensor.IsHighAlarming ? 0 : 1);
                if (_sensor.TestAlarmValue.HasValue)
                    TestAlarmValue = _sensor.TestAlarmValue;

                TestValueMinText = _sensor.LoStateDesc;
                TestValueMaxText = _sensor.HiStateDesc;
            }
            else
            {
                TestValueMin = (short)Math.Round(_sensor.LoRange - 2);
                TestValueMax = (short)Math.Round(_sensor.UpRange + 2);

                TestValueMinText = TestValueMin.ToString();
                TestValueMaxText = TestValueMax.ToString();
            }
        }

        private void SetNotifications(IList<NotificationReceiverDto> notifications)
        {
            _notifications = notifications;

            var smses = new ObservableCollection<ReceiverViewModel>();
            var emails = new ObservableCollection<ReceiverViewModel>();

            for (int i = 0; i < notifications.Count; i++)
            {
                var model = Container.Resolve<ReceiverViewModel>();
                model.Contact = UsersContactInforBuffer.GetItem(notifications[i].UserId);
                if (notifications[i].Email && notifications[i].Sms)
                {
                    var model1 = Container.Resolve<ReceiverViewModel>();
                    model1.Contact = UsersContactInforBuffer.GetItem(notifications[i].UserId);
                    model.ReceiverType = ReceiverViewModel.SMS_RECEIVER;
                    model1.ReceiverType = ReceiverViewModel.EMAIL_RECEIVER;
                    smses.Add(model);
                    emails.Add(model1);
                }
                else
                {
                    if (notifications[i].Email)
                    {
                        model.ReceiverType = ReceiverViewModel.EMAIL_RECEIVER;
                        emails.Add(model);
                    }
                    else
                    {
                        model.ReceiverType = ReceiverViewModel.SMS_RECEIVER;
                        smses.Add(model);
                    }
                }
            }

            SmsReceivers = smses;
            EmailReceivers = emails;
        }

        private void RefreshContacts(ObservableCollection<ReceiverViewModel> collection)
        {
            ObservableCollection<ReceiverViewModel> list = null;
            if (collection != null)
            {
                foreach (var vm in collection)
                {
                    vm.Contact = UsersContactInforBuffer.GetItem(vm.Contact.Id);
                }
                list = new ObservableCollection<ReceiverViewModel>(collection);
            }
            collection = list;
        }

        private void UpdateNotifications()
        {
            RefreshContacts(SmsReceivers);
            RefreshContacts(EmailReceivers);
        }

        private void SetomittedAlarmRanges(IList<OmittedAlarmRangeDto> ranges)
        {
            _ranges = ranges;
            var list = new ObservableCollection<OmittedAlarmRangeViewModel>();
            foreach (var range in ranges)
            {
                var model = Container.Resolve<OmittedAlarmRangeViewModel>();
                model.Range = range;
                list.Add(model);
            }
            OmittedAlarmRanges = list;
        }

        private void SetSockets(IList<SocketDto> sockets)
        {
            //PossibleSockets = new ObservableCollection<SocketDto>(sockets);
            PossibleSockets = sockets.ToObservableCollection();

            if (_sensor.SocketId.HasValue)
            {
                SocketDto socket = null;
                for (int i = 0; i < sockets.Count && socket == null; i++)
                {
                    if (sockets[i].Id == _sensor.SocketId.Value) socket = sockets[i];
                }

                SelectedSocket = socket;
            }
        }

        public void Refresh(IUnit unit)
        {
            SelectedUnit = unit;
            ClearData();

            LoadService.GetSensorForUnitAsync(unit.Identity);
        }

        private void OnCreateMeasureDevice(object state)
        {
            System.Diagnostics.Debug.WriteLine("creating new measure device");
        }

        private void UnitsListViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        private void proxy_GetSensorForUnitCompleted(object sender, GetSensorForUnitCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                SetData(e.Result);

                LoadService.GetPossibleSocketsForUnitAsync(SelectedUnit.Identity);
            }
        }

        private void proxy_GetPossibleSocketsForUnitCompleted(object sender, GetPossibleSocketsForUnitCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                SetSockets(e.Result);
            }

            LoadService.GetNotificationReceiversForUnitAsync(SelectedUnit.Identity);
        }

        private void proxy_GetNotificationReceiversForUnitCompleted(object sender, GetNotificationReceiversForUnitCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                SetNotifications(e.Result);
            }

            LoadService.GetOmittedAlarmRangedForUnitAsync(SelectedUnit.Identity);
        }

        private void proxy_GetOmittedAlarmRangedForUnitCompleted(object sender, GetOmittedAlarmRangedForUnitCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                SetomittedAlarmRanges(e.Result);
            }
        }

        private void SetInitialTestAlarmsForControls(TestMode mode)
        {
            switch (mode)
            {
                case TestMode.NoTest:
                    NoSensorTestActive = false;
                    ValueTestActive = false;
                    TestAlarmModeForBackground = false;
                    return;
                case TestMode.NoSensorTest:
                    NoSensorTestActive = true;
                    ValueTestActive = false;
                    TestAlarmModeForBackground = true;
                    return;
                case TestMode.ValueTest:
                    NoSensorTestActive = false;
                    ValueTestActive = true;
                    TestAlarmModeForBackground = true;
                    return;
            }
        }

        private short? SetTestModeFromControls()
        {
            if (NoSensorTestActive)
            {
                return GetEventTypeFromTestMode(TestMode.NoSensorTest);
            }
            if (ValueTestActive)
            {
                return GetEventTypeFromTestMode(TestMode.ValueTest);

            }
            return GetEventTypeFromTestMode(TestMode.NoTest);
        }

        private void Proxy_ToggleValueTestAlarmCompleted(object sender, ToggleValueTestAlarmCompletedEventArgs e)
        {
            if (e.Result)
                Save_Command(null);
        }

        private void Proxy_ToggleNoSensorTestAlarmCompleted(object sender, ToggleNoSensorTestAlarmCompletedEventArgs e)
        {
            if (e.Result)
                Save_Command(null);
        }

        private void Proxy_SetTestAlarmValueCompleted(object sender, AsyncCompletedEventArgs e)
        {
            Save_Command(null);
        }

        private bool IsTestAlarmEventTypeChanged()
        {
            return TestAlarmEventType != _sensor.TestAlarmEventType;
        }

        private bool IsTestAlarmValueChanged()
        {
            return TestAlarmValue != _sensor.TestAlarmValue;
        }

        private static string GetTestAlarmModeDescription(short? mode)
        {
            const string noTest = "[test nieaktywny]";
            switch (mode)
            {
                case ValueEvent:
                    return "[test wartości]";
                case NoDeviceEvent:
                    return "[test braku urządzenia]";
                case NoHubEvent:
                    return "[test braku koncentratora]";
                case NoSensorEvent:
                    return "[test braku czujnika]";
                default:
                    return noTest;
            }
        }

        private static string GetNoValueText(string text)
        {
            return text.Equals(string.Empty) ? "[brak]" : text;
        }


        public string Title
        {
            get { throw new NotImplementedException(); }
        }

        public bool ExitButtonVisible
        {
            get { throw new NotImplementedException(); }
        }
    }

    class SensorAlarmTestConfig
    {
        public short? PreviousEventType { get; set; }
        public short? PreviousValue { get; set; }
    }

    enum TestMode
    {
        NoTest,
        ValueTest,
        NoSensorTest
    }
}
