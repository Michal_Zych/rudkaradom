﻿using System;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Extensions;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.HubsProxy;
using System.Collections.Generic;
using Finder.BloodDonation.Common.Events;
using System.Linq;
using Finder.BloodDonation.CommService;
using Finder.BloodDonation.Events;
namespace Finder.BloodDonation.Tabs.Configuration
{
    [OnCompleted]
    public class ReceiverViewModel : ViewModelBase
    {
        public const int SMS_RECEIVER = 0;
        public const int EMAIL_RECEIVER = 1;

        [RaisePropertyChanged]
        public UserContactInfoDto Contact { get; set; }

        public DelegateCommand<object> RemoveReceiver { get; set; }

        public int ReceiverType { get; set; }

        public ReceiverViewModel(IUnityContainer container)
            : base(container)
        {
            RemoveReceiver = new DelegateCommand<object>(RemoveReceiver_Command);
        }

        public void RemoveReceiver_Command(object o)
        {
            if (Contact != null)
            {
                EventAggregator.GetEvent<RemoveReceiverInfoEvent>().Publish(new Tuple<object, int>(this, ReceiverType) );
            }
        }


    }
}
