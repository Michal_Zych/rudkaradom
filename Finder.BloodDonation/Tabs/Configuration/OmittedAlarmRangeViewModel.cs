﻿using System;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Extensions;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.HubsProxy;
using System.Collections.Generic;
using Finder.BloodDonation.Common.Events;
using System.Linq;
using Finder.BloodDonation.CommService;
using Finder.BloodDonation.Events;

namespace Finder.BloodDonation.Tabs.Configuration
{
    [OnCompleted]
    public class OmittedAlarmRangeViewModel : ViewModelBase
    {
        [RaisePropertyChanged]
        public OmittedAlarmRangeDto Range { get; set; }

        public DelegateCommand<object> RemoveAlarmRange { get; set; }

       
        public OmittedAlarmRangeViewModel(IUnityContainer container)
            : base(container)
        {
            RemoveAlarmRange = new DelegateCommand<object>(RemoveAlarmRange_Command);
        }

        public void RemoveAlarmRange_Command(object obj)
        {
            if (Range != null)
            {
                EventAggregator.GetEvent<RemoveAlarmRangeEvent>().Publish(this);
            }
        }

    }
}
