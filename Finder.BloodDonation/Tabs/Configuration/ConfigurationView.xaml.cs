﻿using System;
using System.Collections.Generic;
using System.Linq;
using FinderFX.SL.Core.MVVM;
using System.Windows.Controls;
using mpost.SilverlightMultiFileUpload.Core;
using mpost.SilverlightMultiFileUpload.Controls;
using System.Windows;
using System.IO;
using mpost.SilverlightMultiFileUpload.Utils.Constants;
using mpost.SilverlightFramework;
using Finder.BloodDonation.Tools;

namespace Finder.BloodDonation.Tabs.Configuration
{
    [ViewModel(typeof(ConfigurationViewModel))]
    public partial class ConfigurationView : UserControl
    {

        public ConfigurationView()
        {
            InitializeComponent();

            
            disableDateFrom.ValueChanged += disableDateFrom_ValueChanged;
            disableDateTo.ValueChanged += disableDateTo_ValueChanged;
			disableDateFrom2.ValueChanged += DisableDateFrom2_ValueChanged;
			disableDateTo2.ValueChanged += DisableDateTo2_ValueChanged;
        }

		void disableDateTo_ValueChanged(object sender, RoutedPropertyChangedEventArgs<DateTime?> e)
        {
            ValidateDateDisable(1);
        }

        void disableDateFrom_ValueChanged(object sender, RoutedPropertyChangedEventArgs<DateTime?> e)
        {
            ValidateDateDisable(1);
        }

		private void DisableDateFrom2_ValueChanged(object sender, RoutedPropertyChangedEventArgs<DateTime?> e)
		{
			ValidateDateDisable(2);
		}

		private void DisableDateTo2_ValueChanged(object sender, RoutedPropertyChangedEventArgs<DateTime?> e)
		{
			ValidateDateDisable(2);
		}

        private void ValidateDateDisable(int part)
        {
	        if (part == 1)
	        {
		        if (disableDateFrom.Value.HasValue && disableDateTo.Value.HasValue)
		        {
			        {
				        if (disableDateTo.Value.Value < disableDateFrom.Value.Value)
				        {
					        MsgBox.Error("Godzina końcowa nie może być wcześniejsza niż początkowa.");
					        disableDateTo.Value = disableDateFrom.Value.Value;
				        }
			        }
		        }
	        }
	        else
	        {
				if (disableDateFrom2.Value.HasValue && disableDateTo2.Value.HasValue)
				{
					{
						if (disableDateTo2.Value.Value < disableDateFrom2.Value.Value)
						{
                            MsgBox.Error("Godzina końcowa nie może być wcześniejsza niż początkowa.");
							disableDateTo2.Value = disableDateFrom2.Value.Value;
						}
					}
				}
	        }
        }

        void ConfigurationView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            VisualStateManager.GoToState(this, "Empty", false);
        }




 
        private void disable_times_Click(object sender, RoutedEventArgs e)
        {
            if (disable_times.IsChecked.HasValue)
            {
                if (!disable_times.IsChecked.Value)
                {
                    disableDateFrom.Value = null;
                    disableDateTo.Value = null;
                    disableDateFrom2.Value = null;
                    disableDateTo2.Value = null;

                }
            }
            else
            {
                disableDateFrom.Value = null;
                disableDateTo.Value = null;
                disableDateFrom2.Value = null;
                disableDateTo2.Value = null;

            }
        }
    }
}
