﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Tabs.Configuration
{
    public class ChartLineStyle
    {
        public string Name{get;set;}
        public string DashArray { get; set; }

        private static IList<ChartLineStyle> _lineStyles;

        public static IList<ChartLineStyle> GetLineStyles
        {
            get
            {
                if (_lineStyles == null)
                    _lineStyles = _getLineStyles();
                return _lineStyles;
            }
        }


        private static IList<ChartLineStyle> _getLineStyles()
        {
            var result = new List<ChartLineStyle>();

            result.Add(new ChartLineStyle(){
                Name = "Solid",
                DashArray =""
            });

            result.Add(new ChartLineStyle(){
                Name = "Dash",
                DashArray="3,1,3,1"
            });

            result.Add(new ChartLineStyle()
            {
                Name = "Dot",
                DashArray="1"
            });

            result.Add(new ChartLineStyle()
            {
                Name = "DashDot",
                DashArray = "3,1,1,1"
            });

            result.Add(new ChartLineStyle()
            {
                Name = "DashDotDot",
                DashArray="3, 1, 1, 1, 1, 1"
            });
            return result;
        }


        public static ChartLineStyle GetStyle(string styleName)
        {
            ChartLineStyle style = null;

            for (int i = 0; i < GetLineStyles.Count && style == null; i++)
                if (GetLineStyles[i].Name.ToUpper() == styleName.ToUpper()) style = GetLineStyles[i];

            return style == null ? GetLineStyles[0] : style;
        }

        public override string ToString()
        {
            return Name;
        }
    }

 
}
