﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Model.Dto;

namespace Finder.BloodDonation.Converters
{
    public class TimeoutConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                int v = (int)value;
                return System.Convert.ToDouble(v / 1000);
            }
            catch (Exception)
            {
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                if (value != null)
                {
                    double v = (double)value;
                    return (int)Math.Floor(v * 1000);
                }
            }
            catch (Exception)
            {
            }

            return null;
        }
    }
}
