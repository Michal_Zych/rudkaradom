﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Tabs.Configuration.Converters
{
    public static class UntilString
    {
        public static int GetInt(string p)
        {
            p = p.ToUpper().Trim();
            if (p == "FC")
            {
                return 2;
            }
            if (p == "C")
            {
                return 1;
            }
            return 0;
        }

        public static string FromInt(int i)
        {
            if(i == 2)
            {
                return "FC";
            }
            if (i == 1)
            {
                return "C";
            }

            return "F";
        }

        public static string GetUntilText(string p)
        {
            return GetUntilText(GetInt(p));
        }

        public static string GetUntilText(int i)
        {
            if(i==2)
            {
                return "Przy zakończeniu lub zamknięciu";
            }
            if(i == 1)
            {
                return "Przy zamknięciu";
            }
            return "Przy zakończeniu";
        }

    }
}
