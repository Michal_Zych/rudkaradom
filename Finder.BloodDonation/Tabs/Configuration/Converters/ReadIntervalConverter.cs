﻿using System;
using System.Linq;
using System.Windows.Data;
using Finder.BloodDonation.Model.Enum;

namespace Finder.BloodDonation.Tabs.Configuration
{
    public class ReadIntervalConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                int dID = (int)value;
                dID = (int)Math.Floor(dID / 1000);
				//return new DateTime(1900, 1, 1,
				//    (int)Math.Floor(dID / 3600),
				//    (int)(Math.Floor(dID / 60) - Math.Floor(dID / 3600) * 60),
				//    System.Convert.ToInt32(dID - Math.Floor(dID / 60) - Math.Floor(dID / 3600) * 60));

				return new DateTime().Add(new TimeSpan(0, 0, dID));
            }
            catch (Exception exc)
            {
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            try
            {
                DateTime? v = (DateTime?)value;
                if (v.HasValue)
                {
                    return (v.Value.Hour * 3600 + v.Value.Minute * 60 + v.Value.Second) * 1000;
                }
            }
            catch (Exception)
            {
            }

            return null;
        }
    }
}
