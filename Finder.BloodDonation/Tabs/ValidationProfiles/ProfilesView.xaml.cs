﻿using System;
using System.Collections.Generic;
using System.Linq;
using FinderFX.SL.Core.MVVM;
using System.Windows.Controls;
using mpost.SilverlightMultiFileUpload.Core;
using mpost.SilverlightMultiFileUpload.Controls;
using System.Windows;
using mpost.SilverlightMultiFileUpload.Utils.Constants;
using System.IO;
using mpost.SilverlightFramework;

namespace Finder.BloodDonation.Tabs.ValidationProfiles
{
    [ViewModel(typeof(ProfilesViewModel))]
    public partial class ProfilesView : UserControl
    {
        private FileCollection _files;

        public ProfilesView()
        {
            InitializeComponent();

            mpost.SilverlightMultiFileUpload.Core.Configuration.Instance.ChunkSize = 4194304;
            mpost.SilverlightMultiFileUpload.Core.Configuration.Instance.UploadHandlerName = "ProfileUploadHandler.ashx";

            SetRowTemplate(typeof(FileRowControl));

            _files = new FileCollection("profiles", 10, this.Dispatcher);

            FileList.ItemsSource = _files;
            FilesCount.DataContext = _files;
            TotalProgress.DataContext = _files;
            PercentLabel.DataContext = _files;
            TotalKB.DataContext = _files;

            this.Loaded += new System.Windows.RoutedEventHandler(ProfilesView_Loaded);
            _files.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(_files_CollectionChanged);
            _files.AllFilesFinished += new EventHandler(_files_AllFilesFinished);
            _files.TotalPercentageChanged += new EventHandler(_files_TotalPercentageChanged);
        }

        void ProfilesView_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            VisualStateManager.GoToState(this, "Empty", false);
        }

        void _files_TotalPercentageChanged(object sender, EventArgs e)
        {
            // if the percentage is decreasing, don't use an animation
            if (_files.Percentage < TotalProgress.Value)
                TotalProgress.Value = _files.Percentage;
            else
            {
                sbProgressFrame.Value = _files.Percentage;
                sbProgress.Begin();
            }
        }

        void _files_AllFilesFinished(object sender, EventArgs e)
        {
            VisualStateManager.GoToState(this, "Finished", true);
            ((ProfilesViewModel)this.DataContext).UploadProfilesScans_Command(null);
        }

        void _files_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (_files.Count == 0)
            {
                VisualStateManager.GoToState(this, "Empty", true);
            }
            else
            {

                if (_files.FirstOrDefault(f => f.State == Enums.FileStates.Uploading) != null)
                    VisualStateManager.GoToState(this, "Uploading", true);
                else if (_files.FirstOrDefault(f => f.State == Enums.FileStates.Finished) != null)
                    VisualStateManager.GoToState(this, "Finished", true);
                else
                    VisualStateManager.GoToState(this, "Selected", true);
            }
        }

        private void SelectFilesButton_Click(object sender, RoutedEventArgs e)
        {
            SelectUserFiles();
        }

        private void LayoutRoot_Drop(object sender, DragEventArgs e)
        {
            FileInfo[] files = (FileInfo[])e.Data.GetData(System.Windows.DataFormats.FileDrop);

            foreach (FileInfo file in files)
            {
                AddFile(file);
            }

        }

        private void SelectUserFiles()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;

            if (ofd.ShowDialog() == true)
            {
                foreach (FileInfo file in ofd.Files)
                {
                    AddFile(file);
                }
            }
        }

        private void UploadButton_Click(object sender, RoutedEventArgs e)
        {
            UploadFiles();
        }

        private void UploadFiles()
        {
            if (_files.Count == 0)
            {
                MessageBox.Show("Nie wybrano żadnych plików.");
            }
            else
            {
                _files.UploadFiles();
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearFilesList();
        }

        private void ClearFilesList()
        {
            _files.Clear();

        }

        private void AddFile(FileInfo file)
        {
            string fileName = file.Name;

            UserFile userFile = new UserFile();
            userFile.FileName = file.Name;
            userFile.FileStream = file.OpenRead();

            if (userFile.FileStream.Length <= 1000000)
            {
                _files.Add(userFile);
            }
            else
            {
                MessageBox.Show("Plik jest za duży.");
            }
        }

        public void SetRowTemplate(Type type)
        {
            FileList.ItemTemplate = DataTemplateHelper.CreateDataTemplate(type);
        }
    }
}
