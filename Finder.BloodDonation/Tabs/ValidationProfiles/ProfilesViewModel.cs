﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using System.Collections.ObjectModel;
using FinderFX.SL.Core.Extensions;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using FinderFX.SL.Core.Communication;
using Microsoft.Practices.Composite.Presentation.Commands;
using System.Windows.Data;
using Finder.BloodDonation.ValidationService;

namespace Finder.BloodDonation.Tabs.ValidationProfiles
{
    [OnCompleted]
    public class ProfilesViewModel : ViewModelBase, ITabViewModel
    {
        [RaisePropertyChanged]
        public ValidationProfileDto SelectedProfile { get; set; }

        [RaisePropertyChanged]
        public ProfileScanDto SelectedProfileScan { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<ProfileScanDto> ProfilesScans { get; set; }

        [RaisePropertyChanged]
        public ReactiveCollection<ValidationProfileDto> Profiles { get; set; }

        [RaisePropertyChanged]
        public PagedCollectionView ProfilesPaged { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> ChangeProfileScan { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> UploadProfilesScans { get; set; }

        private ValidationServiceClient LoadService
        {
            get
            {
                ValidationServiceClient proxy = ServiceFactory.GetService<ValidationServiceClient>(ServicesUri.ValidationService);
                proxy.AddProfileCompleted += new EventHandler<AddProfileCompletedEventArgs>(proxy_AddProfileCompleted);
                proxy.GetProfilesCompleted += new EventHandler<GetProfilesCompletedEventArgs>(proxy_GetProfilesCompleted);
                proxy.RemoveProfileCompleted += new EventHandler<RemoveProfileCompletedEventArgs>(proxy_RemoveProfileCompleted);
                proxy.UpdateProfileCompleted += new EventHandler<UpdateProfileCompletedEventArgs>(proxy_UpdateProfileCompleted);
                proxy.GetProfilesScansCompleted += new EventHandler<GetProfilesScansCompletedEventArgs>(proxy_GetProfilesScansCompleted);
                return proxy;
            }
        }

        [RaisePropertyChanged]
        public string SearchProfilesFilter { get; set; }
        public DelegateCommand<object> FilterProfiles { get; set; }
        public DelegateCommand<object> ClearFilterProfiles { get; set; }
        public DelegateCommand<object> SearchProfiles { get; set; }
        public DelegateCommand<object> RefreshProfiles { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<ValidationProfileDto> SelectProfile { get; set; }

        public ProfilesViewModel(IUnityContainer container)
            : base(container)
        {
            this.ViewAttached += new EventHandler(UnitsListViewModel_ViewAttached);

            FilterProfiles = new DelegateCommand<object>(RefreshProfiles_Command);
            ClearFilterProfiles = new DelegateCommand<object>(RefreshProfiles_Command);
            SearchProfiles = new DelegateCommand<object>(RefreshProfiles_Command);
            RefreshProfiles = new DelegateCommand<object>(RefreshProfiles_Command);

            SelectProfile = new DelegateCommand<ValidationProfileDto>(SelectProfile_Command);
            ChangeProfileScan = new DelegateCommand<object>(ChangeProfileScan_Command);
            UploadProfilesScans = new DelegateCommand<object>(UploadProfilesScans_Command);

            this.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(ProfilesViewModel_PropertyChanged);
        }

        void ProfilesViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedProfile")
            {
                SelectScan();
            }
        }

        public void SelectProfile_Command(ValidationProfileDto p)
        {
            if (p != null)
            {
                SelectedProfile = p;
            }
        }

        public void ChangeProfileScan_Command(object obj)
        {
            if (SelectedProfileScan != null)
            {
                SelectedProfile.ProfileScanID = SelectedProfileScan.ID;
                LoadService.UpdateProfileAsync(SelectedProfile);
            }
        }

        public void UploadProfilesScans_Command(object obj)
        {
            LoadService.GetProfilesScansAsync();
        }

        private void SelectScan()
        {
            SelectedProfileScan = null;
            if (ProfilesScans != null && SelectedProfile != null && SelectedProfile.ProfileScanID.HasValue)
            {
                for (int i = 0; i < ProfilesScans.Count; i++)
                {
                    if (ProfilesScans[i].ID == SelectedProfile.ProfileScanID.Value)
                    {
                        SelectedProfileScan = ProfilesScans[i];
                        return;
                    }
                }
            }
        }

        public void RefreshProfiles_Command(object obj)
        {
            ReloadProfiles();
        }

        void proxy_GetProfilesScansCompleted(object sender, GetProfilesScansCompletedEventArgs e)
        {
            ProfilesScans = new ObservableCollection<ProfileScanDto>(e.Result);
            SelectScan();
        }

        void proxy_UpdateProfileCompleted(object sender, UpdateProfileCompletedEventArgs e)
        {
            ReloadProfiles();
        }

        void proxy_RemoveProfileCompleted(object sender, RemoveProfileCompletedEventArgs e)
        {
            ReloadProfiles();
        }

        void proxy_AddProfileCompleted(object sender, AddProfileCompletedEventArgs e)
        {
            ReloadProfiles();
        }

        void proxy_GetProfilesCompleted(object sender, GetProfilesCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    ReactiveCollection<ValidationProfileDto> hr = new ReactiveCollection<ValidationProfileDto>(e.Result);
                    hr.Added += new EventHandler<ReactiveCollection<ValidationProfileDto>.ReactiveObjectEventArg>(ValidationProfileDto_Added);
                    hr.Removed += new EventHandler<ReactiveCollection<ValidationProfileDto>.ReactiveObjectEventArg>(ValidationProfileDto_Removed);
                    hr.Updated += new EventHandler<ReactiveCollection<ValidationProfileDto>.ReactiveObjectEventArg>(ValidationProfileDto_Updated);
                    Profiles = hr;
                    ProfilesPaged = new PagedCollectionView(Profiles);
                    if (SelectedProfile != null && Profiles != null)
                    {
                        for (int i = 0; i < Profiles.Count; i++)
                        {
                            if (Profiles[i].ID == SelectedProfile.ID)
                            {
                                SelectedProfile = Profiles[i];
                                break;
                            }
                        }
                    }
                });
            }
            LoadService.GetProfilesScansAsync();
        }

        void ValidationProfileDto_Updated(object sender, ReactiveCollection<ValidationProfileDto>.ReactiveObjectEventArg e)
        {
            LoadService.UpdateProfileAsync(e.Source);
        }

        void ValidationProfileDto_Removed(object sender, ReactiveCollection<ValidationProfileDto>.ReactiveObjectEventArg e)
        {
            LoadService.RemoveProfileAsync(e.Source.ID);
        }

        void ValidationProfileDto_Added(object sender, ReactiveCollection<ValidationProfileDto>.ReactiveObjectEventArg e)
        {
            e.Source.DepartmentID = selected_unit_id;
            LoadService.AddProfileAsync(e.Source);
        }

        void UnitsListViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public void ReloadProfiles()
        {
            LoadService.GetProfilesAsync(selected_unit_id);
        }

        private int selected_unit_id = 0;

        public void Refresh(IUnit unit)
        {
            selected_unit_id = unit.Identity;
            Profiles = null;
            SelectedProfile = null;
            ProfilesPaged = null;
            ReloadProfiles();
        }

        public event EventHandler LoadingCompleted = delegate { };

        private ViewModelState _state = ViewModelState.Loaded;
        public ViewModelState State
        {
            get { return _state; }
        }
    }
}
