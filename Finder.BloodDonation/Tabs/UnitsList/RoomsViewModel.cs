﻿using System;
using System.Linq;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Layout;
using System.Collections.ObjectModel;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using System.Collections.Generic;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Tabs.UnitsList;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Converters;
using Finder.BloodDonation.Layout;
using Global = Finder.BloodDonation.Settings;
using System.Windows;
using Finder.BloodDonation.Tabs.Monitorable;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.DynamicData.DBs;
using Finder.BloodDonation.Model.Dto.Enums;
using Finder.BloodDonation.DBBuffers;
using Finder.BloodDonation.Controls;
using System.ServiceModel.Channels;
using Diagnostics;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Login;
using Finder.BloodDonation.Tabs.PatientDetails.PatientAlarming;
using Finder.BloodDonation.Dialogs.Patients;
using Finder.BloodDonation.DataManagers;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.Tabs.MenuAdministration.SettingsSystem;

namespace Finder.BloodDonation.Tabs
{
    [OnCompleted]
    [UIException]
    public class RoomsViewModel : ViewModelBase, ITabViewModel, IUnitView
    {
        private static IUnityContainer _unityContainer;
        public static IUnityContainer UnityContainer
        { 
            get 
            {
                return _unityContainer;
            } 
            set 
            {
                _unityContainer = value;
            } 
        }
        public static bool fullScreen = false;
        public static void RaiseEvent()
        {
            PatientOutOfZoneData data = new PatientOutOfZoneData() { RoomsIds = new List<int>(roomsIds) };

            _windowsManager.ShowWindow(WindowsKeys.PatientOutOfZone, data);
        }

        public static void RaiseChangeUserInterface()
        {
            fullScreen = !fullScreen;

            if(fullScreen)
            {
                _unityContainer.Resolve<IDynamicEventAggregator>().GetEvent<ChangeVisibilityUserInterfaceEvent>().Publish(false);
            }
            else
            {
                _unityContainer.Resolve<IDynamicEventAggregator>().GetEvent<ChangeVisibilityUserInterfaceEvent>().Publish(true);
            }

        }

        private static LocalizationState ChangeLocalizationState(LocalizationState _oldLocalizationState)
        {
            if (_oldLocalizationState == LocalizationState.ShowBorderShowList)
            {
                return LocalizationState.ShowBorderHideList;
            }
            else if (_oldLocalizationState == LocalizationState.ShowBorderHideList)
            {
                return LocalizationState.HideBorderShowList;
            }
            else if (_oldLocalizationState == LocalizationState.HideBorderShowList)
            {
                return LocalizationState.HideBorderHideList;
            }
            else
            {
                return LocalizationState.ShowBorderShowList;
            }
        }

        public RelayCommand UserInterfaceVisibilityChanged { get; set; }

        private static IObjectable _selectedObject;
        public static IObjectable SelectedObject
        {
            get
            {
                return _selectedObject;
            }
            set
            {
                _selectedObject = value;
            }
        }

        private static DetailsMonitorableObjectData data;
        public static DetailsMonitorableObjectData Data
        {
            get
            {
                return data;
            }

            set
            {
                data = value;

                EventHandler handler = DataChanged;
                if (handler != null)
                    handler(value, new EventArgs());
            }
        }

        public static event EventHandler DataChanged;

        public ObservableCollection<IRoom> _rooms;
        [RaisePropertyChanged]
        public ObservableCollection<IRoom> Rooms
        {
            get
            {
                return _rooms;
            }
            set
            {
                _rooms = value;
            }
        }

        public static IWindowsManager _windowsManager;

        public ObservableCollection<IObjectable> _objectables;

        [RaisePropertyChanged]
        public ObservableCollection<IObjectable> Objectables
        {
            get { return _objectables; }
            set { _objectables = value; }
        }

        [RaisePropertyChanged]
        public PreviewDto UnitPreview { get; set; }

        [RaisePropertyChanged]
        public string Path { get; set; }

        [RaisePropertyChanged]
        public bool IsRoomEnabled { get; set; }


        public ObservableCollection<ZonePreviewDto> PreviewUnits { get; set; }

        IUserSessionManager _userSessionManager = null;

        private UnitsServiceClient LoadService
        {
            get
            {
                UnitsServiceClient proxy = ServiceFactory.GetService<UnitsServiceClient>(ServicesUri.UnitsService);
                proxy.GetUnitPreviewCompleted += new EventHandler<GetUnitPreviewCompletedEventArgs>(proxy_GetUnitPreviewCompleted);
                proxy.GetBandsStatusesCompleted += proxy_GetBandsStatusesCompleted;
                proxy.PreviewForUnitGetCompleted += proxy_PreviewForUnitGetCompleted;
                proxy.GetPatientCompleted += proxy_GetPatientCompleted;
                return proxy;
            }
        }

        void proxy_GetPatientCompleted(object sender, GetPatientCompletedEventArgs e)
        {
            if (data == null)
                return;

            if (e.Result == null)
                return;

            _patientAlarmingData = new PatientAlarmingData();
            _patientAlarmingData.PatientId = e.Result.Id;
            _patientAlarmingData.PatientConfiguration = new PatientConfigurationData() { PatientConfigData = new PatientConfigurationDTO() };
            _patientAlarmingData.PatientRaport = new RaportsData();
            _patientAlarmingData.PatientAlarmsList = new List<Controls.Alarm>();
            _patientAlarmingData.Pesel = e.Result.Pesel;
            _patientAlarmingData.FirstName = e.Result.Name;
            _patientAlarmingData.LastName = e.Result.LastName;

            if (data.MonitorableObject.DisplayId.Contains("Pesel: "))
            {
                if (data.MonitorableObject.AssignUnitId > 0)
                {
                    var s = UnitManager.Instance.Units.FirstOrDefault(x => x != null && x.ID == data.MonitorableObject.AssignUnitId);

                    if (s != null && s.UnitTypeIDv2 != 29)
                    {
                        _patientAlarmingData.AssignmentDepartment = UnitManager.Instance.SearchDepartment(UnitManager.Instance.Units.FirstOrDefault(x => x.ID == data.MonitorableObject.AssignUnitId)).Name;
                        _patientAlarmingData.AssignmentRoom = UnitManager.Instance.Units.FirstOrDefault(x => x.ID == data.MonitorableObject.AssignUnitId).Name;
                    }
                    else if (s != null && s.UnitTypeIDv2 == 29)
                    {
                        _patientAlarmingData.AssignmentDepartment = UnitManager.Instance.Units.FirstOrDefault(x => x.ID == data.MonitorableObject.AssignUnitId).Name;
                    }
                }

                _windowsManager.ShowWindow(WindowsKeys.PatientAlarmingView, _patientAlarmingData);
            }
        }

        private void GetBandStatusesRefreshed()
        {

        }

        void proxy_GetBandsStatusesCompleted(object sender, GetBandsStatusesCompletedEventArgs e)
        {
            try
            {
                if (e.Result != null)
                {
                    BandStatusDto = e.Result;

                    if (BandStatusDto == null)
                        return;

                    List<Finder.BloodDonation.Tabs.UnitsList.Object> monitorableObjects = new List<UnitsList.Object>();

                    try
                    {
                        for (int i = 0; i < BandStatusDto.Count; i++)
                        {
                            switch (BandStatusDto[i].ObjectType)
                            {
                                case (byte)ObjectType.Patient:
                                    monitorableObjects.Add(CreatePatient(BandStatusDto[i]));
                                    break;

                                case (byte)ObjectType.Band:
                                    monitorableObjects.Add(CreateBand(BandStatusDto[i]));
                                    break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.ToString());
                    }

                    PreviewObjects = new ObservableCollection<Finder.BloodDonation.Tabs.UnitsList.Object>(monitorableObjects);

                    if (PreviewObjects != null && PreviewUnits != null)
                    {
                        try
                        {
                            ObservableCollection<IObjectable> lr = new ObservableCollection<IObjectable>();

                            for (int i = 0; i < PreviewObjects.Count; i++)
                            {
                                for (int j = 0; j < PreviewUnits.Count; j++)
                                {
                                    if ((PreviewObjects[i].CurrentUnitId != null && PreviewUnits[j].Id != null && PreviewObjects[i].CurrentUnitId == PreviewUnits[j].Id) ||
                                        (PreviewObjects[i].CurrentUnitId == null && PreviewObjects[i].PreviousUnitId != null && PreviewObjects[i].PreviousUnitId == PreviewUnits[j].Id))
                                    {
                                        MonitorableObjectViewModel r = Container.Resolve<MonitorableObjectViewModel>();
                                        
                                        if (SelectedObject != null && SelectedObject.MonitorableObject.Id == PreviewObjects[i].Id && SelectedObject.MonitorableObject.ObjectType == PreviewObjects[i].ObjectType)
                                        {
                                            r.Style = "sensor_object_selected";
                                        }
                                        else
                                        {
                                            r.Style = "sensor_object";
                                        }

                                        r.MonitorableObject = PreviewObjects[i];
                                        r.MonitorableObject.IsHitTransparencyChanged += r.MonitorableObject_IsHitTransparencyChanged;
                                        r.MinSize = new Point(0.06d, 0.06d);
                                        r.MaxSize = new Point(0.4d, 0.4d);
                                        lr.Add(r);

                                        if (localizeObjectId != null && localizeObjectId == PreviewObjects[i].Id && PreviewObjects[i].ObjectType == ObjectType.Patient)
                                        {
                                            r.Style = "sensor_object_selected";
                                            SelectedObject = r;
                                            localizeObjectId = null;
                                        }
                                    }
                                }
                            }

                            Objectables = lr;
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.ToString());
                        }
                    }
                }

                if (UnitPreview != null)
                    LoadService.GetPreviewsForUnitAsync(UnitPreview.UnitID);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        void proxy_PreviewForUnitGetCompleted(object sender, PreviewForUnitGetCompletedEventArgs e)
        {
            LoadUnits(e.Result.ToList());
        }

        void proxy_GetUnitPreviewCompleted(object sender, GetUnitPreviewCompletedEventArgs e)
        {
            UnitPreview = e.Result;
            if (UnitPreview != null)
            {
                LoadService.GetBandsStatusesAsync(null);

                if (UnitPreview.ID > 0)
                {
                    Path = string.Format("{0}.prev", UnitPreview.ID);
                }
                else
                {
                    UnitPreview.Path = Global.Settings.Current["UnitImagesFolder"] + UnitTypeOrganizer.GetPictureName(UnitPreview.PictureID) + Global.Settings.Current["ImagesExtention"];
                    Path = UnitPreview.Path;
                }

                IsRoomEnabled = true;
            }
            else
            {
                Path = null;
                IsRoomEnabled = false;
            }
        }

        public RoomsViewModel(IUnityContainer container)
            : base(container)
        {
            this.ViewAttached += new EventHandler(UnitsListViewModel_ViewAttached);
            _userSessionManager = container.Resolve<IUserSessionManager>();
            RoomsViewModel.UnityContainer = container;
            EventAggregator.GetEvent<AlarmsUnitAlarmSet>().Subscribe(AlarmsUnitAlarmSet_Event);
            EventAggregator.GetEvent<AlarmsUnitAlarmDispose>().Subscribe(AlarmsUnitAlarmDispose_Event);
            EventAggregator.GetEvent<UnitMouseActionEvent>().Subscribe(UnitMouseActionEvent_Event);
            EventAggregator.GetEvent<UnitPreviewUpdate>().Subscribe(UnitPreviewUpdate_Event);
            EventAggregator.GetEvent<RefreshDeviceDataEvent>().Subscribe(RefreshDeviceDataEvent_Event);

            EventAggregator.GetEvent<ForwardingMessageEvent>().Subscribe(OnReadMessage);
            EventAggregator.GetEvent<BandStatusesRefreshedEvent>().Subscribe(BandStatusesRefreshed);

            _windowsManager = this.Container.Resolve<IWindowsManager>();
            var viewFactory = container.Resolve<ViewFactory>();

            if (_windowsManager != null)
            {
                _windowsManager.RegisterWindow(WindowsKeys.PatientAlarmingView, "",
                    viewFactory.CreateView<PatientAlarmingView>(), new Size(1280, 750), true, true);
                _windowsManager.RegisterWindow(WindowsKeys.PatientOutOfZone, "Obiekty monitorowane poza strefą macierzystą",
                    viewFactory.CreateView<PatientOutOfZone>(), new Size(700, 430), false);
            }

            UserInterfaceVisibilityChanged = new RelayCommand(OnUserInterfaceVisibilityChanged);
            DataChanged += RoomsViewModel_DataChanged;

            RoomDesigner.LocalizationSeparator = Container.Resolve<ISettingsRespository>().Settings.UnitsLocationSeparator;
        }

        bool isFullScreen = false;

        public void OnUserInterfaceVisibilityChanged()
        {
            if (!isFullScreen)
                EventAggregator.GetEvent<ChangeVisibilityUserInterfaceEvent>().Publish(false);
            else
                EventAggregator.GetEvent<ChangeVisibilityUserInterfaceEvent>().Publish(true);

            isFullScreen = !isFullScreen;
        }

        private int? localizeObjectId = null;

        public void OnReadMessage(int obj)
        {
            localizeObjectId = obj;
        }

        void RoomsViewModel_DataChanged(object sender, EventArgs e)
        {
            LoadService.GetPatientAsync(data.MonitorableObject.Id);
        }
      
        public void RefreshDeviceDataEvent_Event(int unit)
        {
            if (PreviewUnits != null && PreviewUnits.Count > 0)
                LoadService.GetUnitPreviewAsync(SelectedUnit.Identity);
        }

        public void UnitPreviewUpdate_Event(int i)
        {
            var id = (SelectedUnit == null) ? 0 : SelectedUnit.Identity;
            LoadService.GetUnitPreviewAsync(id);
        }

        private void ObjectMouseActionEvent_Event(ObjectMouseAction obj)
        {
            if(Objectables != null && typeof(MonitorableObjectViewModel) != obj.Invoker)
            {
                for(int i = 0; i < Objectables.Count; i++)
                {
                    if(((MonitorableObjectViewModel)Objectables[i]).MonitorableObject.Id == obj.ObjectID)
                    {
                        if(obj.Action == MouseActionEnum.SET)
                        {
                            ((MonitorableObjectViewModel)Objectables[i]).CurrentState = "MouseOver";
                        }
                        else if(obj.Action == MouseActionEnum.UNSET)
                        {
                            ((MonitorableObjectViewModel)Objectables[i]).CurrentState = "Normal";
                        }
                    }
                }
            }
        }

        public void UnitMouseActionEvent_Event(UnitMouseAction o)
        {
            if (Rooms != null && typeof(RoomViewModel) != o.Invoker)
            {
                for (int i = 0; i < Rooms.Count; i++)
                {
                    if (((RoomViewModel)Rooms[i]).Unit.Id == o.UnitID)
                    {
                        if (o.Action == MouseActionEnum.SET)
                        {
                            ((RoomViewModel)Rooms[i]).CurrentState = "MouseOver";
                        }
                        else if (o.Action == MouseActionEnum.UNSET)
                        {
                            ((RoomViewModel)Rooms[i]).CurrentState = "Normal";
                        }
                    }
                }
            }
        }

        public void AlarmsUnitAlarmSet_Event(int unit)
        {
            if (Rooms != null)
            {
                for (int i = 0; i < Rooms.Count; i++)
                {
                    RoomViewModel vm = Rooms[i] as RoomViewModel;
                    if (vm != null && vm.Unit.Id == unit)
                    {
                        vm.IsAlarm = true;
                    }
                }
            }
        }

        public void AlarmsUnitAlarmDispose_Event(int unit)
        {
            if (Rooms != null)
            {
                for (int i = 0; i < Rooms.Count; i++)
                {
                    RoomViewModel vm = Rooms[i] as RoomViewModel;
                    if (vm != null && vm.Unit.Id == unit)
                    {
                        vm.IsAlarm = false;
                    }
                }
            }
        }

        public ObservableCollection<Finder.BloodDonation.Tabs.UnitsList.Object> PreviewObjects
        {
            get;
            set;
        }

        public void LoadMonitorableObject()
        {
            if (PreviewObjects != null)
            {
                try
                {
                    ObservableCollection<IObjectable> lr = new ObservableCollection<IObjectable>();
                    
                    for (int i = 0; i < PreviewObjects.Count; i++)
                    {
                        for (int j = 0; j < PreviewUnits.Count; j++)
                        {
                            if (PreviewObjects[i].CurrentUnitId == PreviewUnits[j].Id || (PreviewObjects[i].CurrentUnitId == null && PreviewObjects[i].PreviousUnitId == PreviewUnits[j].Id))
                            {
                                MonitorableObjectViewModel r = Container.Resolve<MonitorableObjectViewModel>();
                                if (SelectedObject != null && SelectedObject.MonitorableObject.Id == PreviewObjects[i].Id && SelectedObject.MonitorableObject.ObjectType == PreviewObjects[i].ObjectType)
                                {
                                    r.Style = "sensor_object_selected";
                                }
                                else
                                {
                                    r.Style = "sensor_object";
                                }
                                r.MonitorableObject = PreviewObjects[i];
                                r.MonitorableObject.IsHitTransparencyChanged += r.MonitorableObject_IsHitTransparencyChanged;
                                r.MinSize = new Point(0.06d, 0.06d);
                                r.MaxSize = new Point(0.4d, 0.4d);
                                if (!CheckIfUnitIsInMonitorableObjectsCollection(lr, PreviewObjects[i]))
                                {
                                    lr.Add(r);
                                }
                            }
                        }
                    }
                    
                    Objectables = lr;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());
                }
            }
        }

        private bool CheckIfUnitIsInMonitorableObjectsCollection(IList<IObjectable> collection, Finder.BloodDonation.Tabs.UnitsList.Object previewObject)
        {
            for(int i = 0; i < collection.Count; i++)
            {
                if (previewObject.Id == collection[i].MonitorableObject.Id && previewObject.ObjectType == collection[i].MonitorableObject.ObjectType)
                    return true;
            }

            return false;
        }

        private Patient CreatePatient(BandStatusDto dto)
        {
            if (dto != null)
            {
                return new Patient()
                {
                    Id = dto.ObjectId,
                    ObjectType = ObjectType.Patient,
                    CurrentUnitId = dto.CurrentUnitId,
                    PreviousUnitId = dto.PreviousUnitId,
                    Name = dto.ObjectDisplayName,
                    DisplayColor = GetColor(dto),
                    AssignUnitId = dto.ObjectUnitId != null ? (int)dto.ObjectUnitId : 0,
                    EventTypes = (EventTypes)dto.TopAlarmSeverity,

                    AlarmDate
                        = dto.TopAlarmDate == null
                        ? new DateTime()
                        : (DateTime)dto.TopAlarmDate,

                    AlarmType = (AlarmType)dto.TopAlarmType,
                    DisplayId = String.Empty,
                    Location = dto.CurrentUnitLocation,
                    AlarmInfo = "S"
                };
            }
            else
            {
                return new Patient();
            }
        }

        private Band CreateBand(BandStatusDto dto)
        {
            if (dto != null)
            {
                return new Band()
                {
                    Id = dto.ObjectId,
                    ObjectType = ObjectType.Band,
                    CurrentUnitId = dto.CurrentUnitId,
                    PreviousUnitId = dto.PreviousUnitId,
                    Name = dto.ObjectDisplayName,

                    DisplayColor = GetColor(dto),

                    EventTypes = (EventTypes)dto.TopAlarmSeverity,

                    AlarmDate
                        = dto.TopAlarmDate == null
                        ? new DateTime()
                        : (DateTime)dto.TopAlarmDate,

                    AlarmType = (AlarmType)dto.TopAlarmType,
                    DisplayId = String.Empty,
                    Location = dto.CurrentUnitLocation,
                    AlarmInfo = "S"
                };
            }
            else
            {
                return new Band();
            }
        }

        private BitmapMode GetColor(BandStatusDto dto)
        {
            if (dto.TopAlarmSeverity == (byte)EventTypes.Alarm)
            {
                return BitmapMode.Red;
            }
            else if (dto.TopAlarmSeverity == (byte)EventTypes.Warning)
            {
                return BitmapMode.Yellow;
            }
            else
            {
                if (dto.ObjectUnitId != null && dto.ObjectUnitId != 0)
                {
                    if (dto.CurrentUnitId == null || dto.ObjectUnitId == 0)
                        return BitmapMode.Gray;

                    if (dto.CurrentUnitId == dto.ObjectUnitId)
                        return BitmapMode.Green;
                    else
                        return BitmapMode.Green;
                }
                else
                {
                    return BitmapMode.Cyan;
                }
            }
        }

        private IList<BandStatusDto> BandStatusDto { get; set; }

        public void BandStatusesRefreshed(object obj)
        {
            try
            {
                BandStatusDto = DbBandStatus.GetItems();

                if (BandStatusDto == null)
                    return;

                List<Finder.BloodDonation.Tabs.UnitsList.Object> monitorableObjects = new List<UnitsList.Object>();


                for (int i = 0; i < BandStatusDto.Count; i++)
                {
                    switch (BandStatusDto[i].ObjectType)
                    {
                        case (byte)ObjectType.Patient:
                            monitorableObjects.Add(CreatePatient(BandStatusDto[i]));
                            break;

                        case (byte)ObjectType.Band:
                            monitorableObjects.Add(CreateBand(BandStatusDto[i]));
                            break;
                    }
                }

                PreviewObjects = new ObservableCollection<Finder.BloodDonation.Tabs.UnitsList.Object>(monitorableObjects);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
            if (PreviewObjects != null && PreviewUnits != null)
            {
                try
                {
                    ObservableCollection<IObjectable> lr = new ObservableCollection<IObjectable>();

                    for (int i = 0; i < PreviewObjects.Count; i++)
                    {
                        for (int j = 0; j < PreviewUnits.Count; j++)
                        {
                            if (PreviewObjects[i].CurrentUnitId == PreviewUnits[j].Id || (PreviewObjects[i].CurrentUnitId == null && PreviewObjects[i].PreviousUnitId == PreviewUnits[j].Id))
                            {
                                MonitorableObjectViewModel r = Container.Resolve<MonitorableObjectViewModel>();
                                if (SelectedObject != null && SelectedObject.MonitorableObject.Id == PreviewObjects[i].Id && SelectedObject.MonitorableObject.ObjectType == PreviewObjects[i].ObjectType)
                                {
                                    r.Style = "sensor_object_selected";
                                }
                                else
                                {
                                    r.Style = "sensor_object";
                                }

                                r.MonitorableObject = PreviewObjects[i];
                                r.MonitorableObject.IsHitTransparencyChanged += r.MonitorableObject_IsHitTransparencyChanged;
                                r.MinSize = new Point(0.06d, 0.06d);
                                r.MaxSize = new Point(0.4d, 0.4d);
                                lr.Add(r);
                            }
                        }
                    }

                    Objectables = lr;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString());
                }
            }
        }

        public static IEnumerable<int> roomsIds;
        private PatientAlarmingData _patientAlarmingData;

        public void LoadUnits(List<ZonePreviewDto> units)
        {
            PreviewUnits = new ObservableCollection<ZonePreviewDto>(units);
            if (PreviewUnits != null)
            {
                try
                {
                    ObservableCollection<IRoom> lr = new ObservableCollection<IRoom>();

                    for (int i = 0; i < PreviewUnits.Count; i++)
                    {
                        RoomViewModel r = Container.Resolve<RoomViewModel>();
                        r.Unit = PreviewUnits[i];
                        r.IsAlarm = _userSessionManager.IsUnitAlarm(r.Unit.Id);
                        r.IsAlarmDisabled = _userSessionManager.IsUnitAlarmingDisabled(r.Unit.Id);
                        r.Style = "room_elevator";

                        lr.Add(r);
                    }

                    Rooms = lr;
                    roomsIds = PreviewUnits.Select(x => x.Id);
                    PatientOutOfZoneData data = new PatientOutOfZoneData() { RoomsIds = new List<int>(roomsIds) };

                    if(_windowsManager.CheckIfWindowIsShowed(WindowsKeys.PatientOutOfZone))
                    {
                        _windowsManager.ShowWindow(WindowsKeys.PatientOutOfZone, data);
                    }

                    LoadMonitorableObject();
                }
                catch (Exception e)
                {
                    throw new Exception(e.ToString());
                }
            }
        }

        void UnitsListViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        private IUnit SelectedUnit = null;

        public void Refresh(IUnit unit)
        {
            LocalStorageSettings.MonitorableObjectScale = LocalStorageManager.LoadSettings("MonitorableObjectScale");
            LocalStorageSettings.LocalizationPlanViewScale = LocalStorageManager.LoadSettings("LocalizationPlanViewScale");
            RoomDesigner.IsFirstLoad = true;
            SelectedUnit = unit;
            Rooms = null;
            UnitPreview = null;
            PreviewUnits = null;
            PreviewObjects = null;

            if (unit.SelectionSource == SelectionSources.MainTree || unit.SelectionSource == SelectionSources.Favorites)
            {
                LoadService.GetUnitPreviewAsync(unit.Identity);
            }
            else
            {
                Path = null;
                IsRoomEnabled = false;
            }
        }

        public event EventHandler LoadingCompleted = delegate { };

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        public string Title
        {
            get { return SelectedUnit.UnitDescription; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }

        public bool IsUnitNameVisibility = false;
    }

    public enum LocalizationState
    {
        ShowBorderShowList,
        ShowBorderHideList,
        HideBorderShowList,
        HideBorderHideList
        
    }
}
