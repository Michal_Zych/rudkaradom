﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Layout;
using System.Collections.ObjectModel;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using System.Collections.Generic;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Helpers;

namespace Finder.BloodDonation.Tabs.UnitsList
{
    [UIException]
    public class RoomPlanViewModel : ViewModelBase, IRoom
    {
        //można przez interface
        public PreviewEditModel ParentManager { get; set; }

        [RaisePropertyChanged]
        public ZonePreviewDto Unit { get; set; }

        public string TempName { get; set; }

        public string NoSensor
        {
            get
            {
                return Unit == null ? string.Empty : MeasurementValueExtension.NoSensor;
            }
        }

        public string NoDevice
        {
            get
            {
                return Unit == null ? string.Empty : MeasurementValueExtension.NoDevice;
            }
        }

        public string NA
        {
            get
            {
                return Unit == null ? string.Empty : MeasurementValueExtension.NA;
            }
        }

        [RaisePropertyChanged]
        public bool IsAlarm { get; set; }

		[RaisePropertyChanged]
        public DelegateCommand<object> OpenUnit { get; set; }

		[RaisePropertyChanged]
		public string CurrentState { get; set; }
		
		public RoomPlanViewModel(IUnityContainer container)
            : base(container)
        {
            OpenUnit = new DelegateCommand<object>(OpenUnit_Command);
        }

	    public void OpenUnit_Command(object o)
        {
            //EventAggregator.GetEvent<UnitOpenEvent>().Publish(Unit.ID);
        }

        public void SetSizeLocation()
        {
            PercentSizeToPointSize();
            ParentManager.RedrawRoom(this);
        }

        private void PercentSizeToPointSize()
        {
            _tl = new Point();
            _br = new Point();
        }

        public void MoveUnit_Command(Point tl, Point br)
        {
            Unit.TLX = tl.X;
            Unit.TLY = tl.Y;
            Unit.BRX = br.X;
            Unit.BRY = br.Y;

            PercentSizeToPointSize();

            SizeLocationChanged_Command();
        }

        public void SizeLocationChanged_Command()
        {
            ParentManager.SizeLocationChanged(this);
        }

        public void RemoveUnit_Command()
        {
            ParentManager.RemoveRoom(this);
        }

        public void MouseEnter_Trigger(object sender, MouseEventArgs e)
        {
            //go to state over
            //CurrentState = "MouseOver";
            //EventAggregator.GetEvent<UnitMouseActionEvent>().Publish(new UnitMouseAction()
            //{
            //Action = MouseActionEnum.SET,
            //Invoker = this.GetType(),
            //UnitID = Unit.ID
            //});
        }

        public void MouseLeave_Trigger(object sender, MouseEventArgs e)
        {
            //go to normal state
            //CurrentState = "Normal";
            //EventAggregator.GetEvent<UnitMouseActionEvent>().Publish(new UnitMouseAction()
            //{
            //Action = MouseActionEnum.UNSET,
            //Invoker = this.GetType(),
            //UnitID = Unit.ID
            //});
        }

        private Point _tl = new Point();
        public Point TL
        {
            get
            {
                if (_tl == new Point())
                {
                    _tl = new Point(Unit.TLX, Unit.TLY);
                }

                return _tl;
            }
        }

        private Point _br = new Point();
        public Point BR
        {
            get
            {
                if (_br == new Point())
                {
                    _br = new Point(Unit.BRX, Unit.BRY);
                }

                return _br;
            }
        }

        public string Style { get; set; }

        public int Order
        {
            get { return OrderHelper.GetOrder(Style); }
        }
    }
}
