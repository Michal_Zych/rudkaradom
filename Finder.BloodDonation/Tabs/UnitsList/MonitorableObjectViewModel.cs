﻿using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Tabs.UnitsList
{
    [UIException]
    public class MonitorableObjectViewModel : ViewModelBase, IObjectable
    {
        [RaisePropertyChanged]
        public Object MonitorableObject { get; set; }

        [RaisePropertyChanged]
        public string CurrentState { get; set; }

        public RelayCommand OnClick { get; set; }
        public RelayCommand OnShowDetails { get; set; }

        public string TempName { get; set; }

        public string NA
        {
            get
            {
                return MonitorableObject == null ? string.Empty : MeasurementValueExtension.NA;
            }
        }

        private Point _tl = new Point();
        public Point TL
        {
            get
            {
                if (_tl == new Point())
                {
                    _tl = new Point(MonitorableObject.TLX, MonitorableObject.TLY);
                }

                return _tl;
            }
        }

        private Point _br = new Point();
        public Point BR
        {
            get
            {
                if (_br == new Point())
                {
                    _br = new Point(MonitorableObject.BRX, MonitorableObject.BRY);
                }

                return _br;
            }
        }

        public Point MinSize { get; set; }
        public Point MaxSize { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> ShowCloud { get; set; }

        public UnitsServiceClient Proxy { get; set; }

        public MonitorableObjectViewModel(IUnityContainer container)
            : base(container)
        {
            ShowCloud = new DelegateCommand<object>(ShowCloud_Command);
            OnClick = new RelayCommand(OnClick_Command);
            OnShowDetails = new RelayCommand(OnShowDetails_Command);
        }

        private void OnShowDetails_Command()
        {
            if (MonitorableObject != null)
            {
                RoomsViewModel.Data = new Monitorable.DetailsMonitorableObjectData(MonitorableObject);
            }
        }

        public void MonitorableObject_IsHitTransparencyChanged(object sender, EventArgs e)
        {
            if (MonitorableObject.IsHitTransparency == null)
                return;

            if((bool)MonitorableObject.IsHitTransparency)
            {
                MsgBox.Warning("Transparency click! :)" + MonitorableObject.Name);

            }
            else if(!(bool)MonitorableObject.IsHitTransparency)
            {
                MsgBox.Warning("Monitorable Object click!" + MonitorableObject.Name);
                RoomDesigner.MonitorableObjectToShowDetails = this.MonitorableObject;
            }
        }

        private void OnClick_Command()
        {
            RoomDesigner.MonitorableObjectToShowDetails = null;
           
        }

        private void ShowCloud_Command(object obj)
        {

        }

        public void MouseEnter_Trigger(object sender, MouseEventArgs e)
        {
            //go to state over
            CurrentState = "MouseOver";
            EventAggregator.GetEvent<ObjectMouseActionEvent>().Publish(new ObjectMouseAction()
            {
                Action = MouseActionEnum.SET,
                Invoker = this.GetType(),
                ObjectID = MonitorableObject.Id
            });
        }

        public void MouseLeave_Trigger(object sender, MouseEventArgs e)
        {
            //go to normal state
            CurrentState = "Normal";
            EventAggregator.GetEvent<ObjectMouseActionEvent>().Publish(new ObjectMouseAction()
            {
                Action = MouseActionEnum.UNSET,
                Invoker = this.GetType(),
                ObjectID = MonitorableObject.Id
            });
        }

        public int Order { get; set; }


        public string Style
        {
            get;
            set;
        }

        public Point Size
        {
            get { return new Point(0, 0); }
        }

        private HitPoint hitPoint;
        [RaisePropertyChanged]
        public HitPoint HitPoint
        {
            get
            {
                return hitPoint;
            }
            set
            {
                hitPoint = value;
                
                if (value != null)
                {
                    RoomDesigner.IsRefreshed = true;
                }
            }
        }
    }
}
