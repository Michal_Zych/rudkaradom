﻿using System;
using System.Linq;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.Model.Dto;
using System.Collections.Generic;
using FinderFX.SL.Core.Communication;
using System.Collections.ObjectModel;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Tabs.UnitsList;
using System.Windows;
using Finder.BloodDonation.Tabs.RefreshTools;
using Finder.BloodDonation.Helpers;

namespace Finder.BloodDonation.Tabs
{
    [UIException]
    public class FavoriteUnitsListViewModel : ViewModelBase, ITabViewModel, IUnitView
    {
        [RaisePropertyChanged]
        public ObservableCollection<UnitItemModel> ChildUnits { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<UnitItemModel> ChildUnitsDevices { get; set; }

        IUserSessionManager _userSessionManager = null;

        private UnitsServiceClient LoadService
        {
            get
            {
                UnitsServiceClient proxy = ServiceFactory.GetService<UnitsServiceClient>(ServicesUri.UnitsService);
                proxy.GetChildsFavoritePreviewsCompleted += new EventHandler<GetChildsFavoritePreviewsCompletedEventArgs>(proxy_GetChildsFavoritePreviewsCompleted);
                return proxy;
            }
        }

        void proxy_GetChildsFavoritePreviewsCompleted(object sender, GetChildsFavoritePreviewsCompletedEventArgs e)
        {
            //LoadUnits(e.Result.ToList());
        }

        void proxy_GetChildsPreviewsByHubCompleted(object sender, GetChildsPreviewsByHubCompletedEventArgs e)
        {
            //LoadUnits(e.Result.ToList());
        }

        public FavoriteUnitsListViewModel(IUnityContainer container)
            : base(container)
        {
            this.ViewAttached += new EventHandler(UnitsListViewModel_ViewAttached);
            _userSessionManager = container.Resolve<IUserSessionManager>();

            EventAggregator.GetEvent<AlarmsUnitAlarmSet>().Subscribe(AlarmsUnitAlarmSet_Event);
            EventAggregator.GetEvent<AlarmsUnitAlarmDispose>().Subscribe(AlarmsUnitAlarmDispose_Event);
            EventAggregator.GetEvent<UnitMouseActionEvent>().Subscribe(UnitMouseActionEvent_Event);
        }

        public void UnitMouseActionEvent_Event(UnitMouseAction o)
        {
            if (ChildUnits != null && typeof(UnitItemModel) != o.Invoker)
            {
                for (int i = 0; i < ChildUnits.Count; i++)
                {
                    if (((UnitItemModel)ChildUnits[i]).Unit.Id == o.UnitID)
                    {
                        if (o.Action == MouseActionEnum.SET)
                        {
                            ((UnitItemModel)ChildUnits[i]).CurrentState = "ShowHover";
                        }
                        else if (o.Action == MouseActionEnum.UNSET)
                        {
                            ((UnitItemModel)ChildUnits[i]).CurrentState = "HideHover";
                        }
                    }
                }
            }

            if (ChildUnitsDevices != null && typeof(UnitItemModel) != o.Invoker)
            {
                for (int i = 0; i < ChildUnitsDevices.Count; i++)
                {
                    if (((UnitItemModel)ChildUnitsDevices[i]).Unit.Id == o.UnitID)
                    {
                        if (o.Action == MouseActionEnum.SET)
                        {
                            ((UnitItemModel)ChildUnitsDevices[i]).CurrentState = "ShowHover";
                        }
                        else if (o.Action == MouseActionEnum.UNSET)
                        {
                            ((UnitItemModel)ChildUnitsDevices[i]).CurrentState = "HideHover";
                        }
                    }
                }
            }
        }

        public void LoadUnits(List<ZonePreviewDto> e)
        {
            /*if (e != null)
            {
                ObservableCollection<UnitItemModel> r = new ObservableCollection<UnitItemModel>();
                ObservableCollection<UnitItemModel> d = new ObservableCollection<UnitItemModel>();
                UnitPreviewDto[] input_rooms = (from a in e where (a.UnitType.IsNotMonitoredDevice()) select a).ToArray();
                UnitPreviewDto[] input_devices = (from b in e where (b.UnitType.IsMonitoredDevice()) select b).ToArray();
                for (int i = 0; i < input_rooms.Length; i++)
                {
                    r.Add(new UnitItemModel(Container) { Unit = input_rooms[i], IsAlarm = _userSessionManager.IsUnitAlarm(input_rooms[i].ID) });
                }
                if (input_rooms == null || input_rooms.Length == 0)
                {
                    for (int i = 0; i < input_devices.Length; i++)
                    {
                        d.Add(new UnitItemModel(Container) { Unit = input_devices[i], IsAlarm = _userSessionManager.IsUnitAlarm(input_devices[i].ID) });
                    }
                }
                ChildUnits = r;
                ChildUnitsDevices = d;
            }
            else
            {
                ChildUnits = null;
                ChildUnitsDevices = null;
            }*/
        }

        public void AlarmsUnitAlarmSet_Event(int unit)
        {
            if (ChildUnits != null)
            {
                for (int i = 0; i < ChildUnits.Count; i++)
                {
                    if (ChildUnits[i].Unit.Id == unit)
                    {
                        ChildUnits[i].IsAlarm = true;
                    }
                }
            }

            if (ChildUnitsDevices != null)
            {
                for (int i = 0; i < ChildUnitsDevices.Count; i++)
                {
                    if (ChildUnitsDevices[i].Unit.Id == unit)
                    {
                        ChildUnitsDevices[i].IsAlarm = true;
                    }
                }
            }
        }

        public void AlarmsUnitAlarmDispose_Event(int unit)
        {
            if (ChildUnits != null)
            {
                for (int i = 0; i < ChildUnits.Count; i++)
                {
                    if (ChildUnits[i].Unit.Id == unit)
                    {
                        ChildUnits[i].IsAlarm = false;
                    }
                }
            }

            if (ChildUnitsDevices != null)
            {
                for (int i = 0; i < ChildUnitsDevices.Count; i++)
                {
                    if (ChildUnitsDevices[i].Unit.Id == unit)
                    {
                        ChildUnitsDevices[i].IsAlarm = false;
                    }
                }
            }
        }

        void UnitsListViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }
        int? crtUnitIdentity = null;
        public void Refresh(IUnit unit)
        {
            ChildUnits = null;
            ChildUnitsDevices = null;

            LoadService.GetChildsFavoritePreviewsAsync(unit.Identity);
            
            crtUnitIdentity = unit.Identity;
            var timer = this.Container.Resolve<IRefreshManager>();
            timer.Start(RefreshData);
        }

        private void RefreshData(object state)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                LoadService.GetChildsFavoritePreviewsAsync(crtUnitIdentity.Value);
            });
        }

        public event EventHandler LoadingCompleted = delegate { };

        private ViewModelState _state = ViewModelState.Loaded;
        public ViewModelState State
        {
            get { return _state; }
        }


        public string Title
        {
            get { return "Tytuł ulubione"; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }
}
