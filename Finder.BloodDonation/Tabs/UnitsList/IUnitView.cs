﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Finder.BloodDonation.Model.Dto;
using System.Collections.Generic;

namespace Finder.BloodDonation.Tabs.UnitsList
{
    internal interface IUnitView
    {
        void LoadUnits(List<ZonePreviewDto> units);
    }
}
