﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.Model.Dto;
using System.Collections.Generic;
using FinderFX.SL.Core.Communication;
using System.Collections.ObjectModel;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common;

namespace Finder.BloodDonation.Tabs
{
    [UIException]
    public class UnitItemModel : ViewModelBase
    {
        [RaisePropertyChanged]
        public bool IsAlarm { get; set; }

        [RaisePropertyChanged]
        public ZonePreviewDto Unit { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> OpenUnit { get; set; }

        [RaisePropertyChanged]
        public string CurrentState { get; set; }

        public UnitItemModel(IUnityContainer container)
            : base(container)
        {
            OpenUnit = new DelegateCommand<object>(OpenUnit_Command);
        }

        public void OpenUnit_Command(object o)
        {
            EventAggregator.GetEvent<UnitOpenEvent>().Publish(Unit.Id);
        }

        public void MouseEnter_Trigger(object sender, MouseEventArgs e)
        {
            EventAggregator.GetEvent<UnitMouseActionEvent>().Publish(new UnitMouseAction()
            {
                Action = MouseActionEnum.SET,
                Invoker = this.GetType(),
                UnitID = Unit.Id
            });
        }

        public void MouseLeave_Trigger(object sender, MouseEventArgs e)
        {
            EventAggregator.GetEvent<UnitMouseActionEvent>().Publish(new UnitMouseAction()
            {
                Action = MouseActionEnum.UNSET,
                Invoker = this.GetType(),
                UnitID = Unit.Id
            });
        }
    }
}
