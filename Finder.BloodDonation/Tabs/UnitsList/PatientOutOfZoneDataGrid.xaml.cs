﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Tabs.UnitsList
{
    public partial class PatientOutOfZoneDataGrid : UserControl
    {
        private static MonitorableObjectItem selectedObject;
        public static MonitorableObjectItem SelectedObject
        {
            get
            {
                return selectedObject;
            }
            set
            {
                PatientOutOfZoneModel.SelectedMonitorableObjectChanged(value);

                selectedObject = value;
            }
        }

        public static readonly DependencyProperty MonitorablesObjectsProperty =
            DependencyProperty.Register("MonitorablesObjects", typeof(IList<Object>), typeof(PatientOutOfZoneDataGrid),
                new PropertyMetadata(OnMonitorablesObjectsChanged));

        private static void OnMonitorablesObjectsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetMonitorablesObjects = (PatientOutOfZoneDataGrid)d;
            var oldMonitorablesObjects = (IList<Object>)e.OldValue;

            var newMonitorablesObjects = targetMonitorablesObjects.MonitorablesObjects;
            targetMonitorablesObjects.OnMonitorablesObjectsChanged(oldMonitorablesObjects, newMonitorablesObjects);
        }

        [System.ComponentModel.Category("MonitorablesObjects Settings")]
        public IList<Object> MonitorablesObjects
        {
            get { return (IList<Object>)(GetValue(MonitorablesObjectsProperty)); }
            set { SetValue(MonitorablesObjectsProperty, value); }
        }

        protected virtual void OnMonitorablesObjectsChanged(IList<Object> oldMonitorablesObjects, IList<Object> newMonitorablesObjects)
        {
            BuildMonitorableObjectsList();
        }

        public void BuildMonitorableObjectsList()
        {
            if (MonitorablesObjects == null)
                throw new NullReferenceException("MonitorablesObjects is not initialized!");

            if (LayoutRoot.Children.Count > 0)
            {
                LayoutRoot.Children.Clear();
                LayoutRoot.ColumnDefinitions.Clear();
                LayoutRoot.RowDefinitions.Clear();
            }

            for (int i = 0; i < MonitorablesObjects.Count; i++)
            {
                MonitorableObjectItem monitorableObjectItem = new MonitorableObjectItem();
                monitorableObjectItem.ObjectType = MonitorablesObjects[i].ObjectType;
                monitorableObjectItem.Name = MonitorablesObjects[i].ObjectType + "_" + MonitorablesObjects[i].Id;
                monitorableObjectItem.DisplayColor = MonitorablesObjects[i].DisplayColor;
                monitorableObjectItem.DisplayName = MonitorablesObjects[i].Name;
                monitorableObjectItem.LocationPath = MonitorablesObjects[i].Location;
                monitorableObjectItem.Id = MonitorablesObjects[i].Id;
                monitorableObjectItem.CurrentUnitId = MonitorablesObjects[i].CurrentUnitId;
                monitorableObjectItem.PreviousUnitId = MonitorablesObjects[i].PreviousUnitId;

                monitorableObjectItem.MouseEnter += monitorableObjectItem_MouseMove;
                monitorableObjectItem.MouseLeave += monitorableObjectItem_MouseLeave;
                monitorableObjectItem.MouseLeftButtonDown += monitorableObjectItem_MouseLeftButtonDown;
                monitorableObjectItem.MouseLeftButtonUp += monitorableObjectItem_MouseLeftButtonUp;

                LayoutRoot.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(50d) });
                LayoutRoot.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1650d) });
                LayoutRoot.Children.Add(monitorableObjectItem);
                
                Grid.SetRow(monitorableObjectItem, i);
            }
        }

        void monitorableObjectItem_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            MonitorableObjectItem _movedElement = (MonitorableObjectItem)sender;

            if (_movedElement != null && _movedElement != SelectedObject)
            {
                if (SelectedObject != null)
                {
                    SelectedObject.DisplayLayoutColor = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
                }

                _movedElement.DisplayLayoutColor = new SolidColorBrush(Color.FromArgb(100, 200, 200, 200));
                SelectedObject = _movedElement;
            }
        }

        void monitorableObjectItem_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MonitorableObjectItem _movedElement = (MonitorableObjectItem)sender;

            if (_movedElement != null && _movedElement != SelectedObject)
            {
                //SelectedObject.DisplayLayoutColor = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
                //SelectedObject = null;

                _movedElement.DisplayLayoutColor = new SolidColorBrush(Color.FromArgb(100, 137, 137, 137));
                //SelectedObject = _movedElement;
            }
        }

        void monitorableObjectItem_MouseLeave(object sender, MouseEventArgs e)
        {
            MonitorableObjectItem _movedElement = (MonitorableObjectItem)sender;

            if(_movedElement != null && _movedElement != SelectedObject)
            {
                _movedElement.DisplayLayoutColor = new SolidColorBrush(Color.FromArgb(0, 0, 0, 0));
            }
        }

        void monitorableObjectItem_MouseMove(object sender, MouseEventArgs e)
        {
            MonitorableObjectItem _movedElement = (MonitorableObjectItem)sender;

            if(_movedElement != null && _movedElement != SelectedObject)
            {
                _movedElement.DisplayLayoutColor = new SolidColorBrush(Color.FromArgb(100, 156, 156, 156));
                //((MonitorableObjectItem)sender).BorderThickness = new Thickness(5d, 5d, 5d, 5d);
            }
        }

        public PatientOutOfZoneDataGrid()
        {
            InitializeComponent();
        }
    }
}
