﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;

namespace Finder.BloodDonation.Tabs
{
    [ViewModel(typeof(FavoriteUnitsListViewModel))]
    public partial class FavoriteUnitsListView : UserControl
    {
        public FavoriteUnitsListView()
        {
            InitializeComponent();

            LayoutRoot.MouseLeftButtonDown += new MouseButtonEventHandler(LayoutRoot_MouseLeftButtonDown);
        }

        void LayoutRoot_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            
        }
    }
}
