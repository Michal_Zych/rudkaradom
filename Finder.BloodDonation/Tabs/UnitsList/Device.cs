﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Tabs.UnitsList
{
    [TemplatePart(Name = PART_MainBorder, Type = typeof(Border))]
    public class Device : Control
    {
        private const string PART_MainBorder = "PART_MainBorder";

        private Border MainBorder = null;

        public Device()
        {
            this.DefaultStyleKey = typeof(Device);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            MainBorder = GetTemplateChild(PART_MainBorder) as Border;
        }
    }
}
