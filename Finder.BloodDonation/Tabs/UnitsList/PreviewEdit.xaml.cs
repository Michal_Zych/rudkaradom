﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using System.IO;
using mpost.SilverlightMultiFileUpload.Core;
using mpost.SilverlightMultiFileUpload.Utils.Constants;
using mpost.SilverlightFramework;
using mpost.SilverlightMultiFileUpload.Controls;
using Finder.BloodDonation.Model;
using Finder.BloodDonation.Tools;

namespace Finder.BloodDonation.Tabs.UnitsList
{
    [ViewModel(typeof(PreviewEditModel))]
    public partial class PreviewEdit : UserControl
    {
        private FileCollection _files;

        public PreviewEdit()
        {
            InitializeComponent();

            mpost.SilverlightMultiFileUpload.Core.Configuration.Instance.ChunkSize = 4194304;
            mpost.SilverlightMultiFileUpload.Core.Configuration.Instance.UploadHandlerName = "PreviewUploadHandler.ashx";

            SetRowTemplate(typeof(FileRowControl));


            _files = new FileCollection(App.Current.Resources[ConfigurationsKeys.CLIENT_PARAM_NAME].ToString(), 10, this.Dispatcher);

            FileList.ItemsSource = _files;
            FilesCount.DataContext = _files;
            TotalProgress.DataContext = _files;
            //PercentLabel.DataContext = _files;
            TotalKB.DataContext = _files;

            _files.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(_files_CollectionChanged);
            _files.AllFilesFinished += new EventHandler(_files_AllFilesFinished);
            _files.TotalPercentageChanged += new EventHandler(_files_TotalPercentageChanged);

            LayoutRoot.KeyUp += new KeyEventHandler(KeyUp_Event);
        }

        private void KeyUp_Event(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.L:

                    break;
                case Key.T:

                    break;
                case Key.W:

                    break;
                case Key.S:

                    break;
                default:
                    break;
            }
        }

        void _files_TotalPercentageChanged(object sender, EventArgs e)
        {
            // if the percentage is decreasing, don't use an animation
            if (_files.Percentage < TotalProgress.Value)
                TotalProgress.Value = _files.Percentage;
            else
            {
                sbProgressFrame.Value = _files.Percentage;
                sbProgress.Begin();
            }
        }

        void _files_AllFilesFinished(object sender, EventArgs e)
        {
            VisualStateManager.GoToState(this, "Finished", true);

            ((PreviewEditModel)this.DataContext).UploadPreview();
            TotalProgress.Value = 0;
        }

        void _files_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (_files.Count == 0)
            {
                VisualStateManager.GoToState(this, "Empty", true);
            }
            else
            {

                if (_files.FirstOrDefault(f => f.State == mpost.SilverlightMultiFileUpload.Utils.Constants.Enums.FileStates.Uploading) != null)
                    VisualStateManager.GoToState(this, "Uploading", true);
                else if (_files.FirstOrDefault(f => f.State == mpost.SilverlightMultiFileUpload.Utils.Constants.Enums.FileStates.Finished) != null)
                    VisualStateManager.GoToState(this, "Finished", true);
                else
                    VisualStateManager.GoToState(this, "Selected", true);
            }
        }

        private void SelectFilesButton_Click(object sender, RoutedEventArgs e)
        {
            SelectUserFiles();
        }

        private void LayoutRoot_Drop(object sender, DragEventArgs e)
        {
            FileInfo[] files = (FileInfo[])e.Data.GetData(System.Windows.DataFormats.FileDrop);

            foreach (FileInfo file in files)
            {
                AddFile(file);
            }

        }

        private void SelectUserFiles()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;

            if (ofd.ShowDialog() == true)
            {
                foreach (FileInfo file in ofd.Files)
                {
                    AddFile(file);
                }
            }
        }

        private void UploadButton_Click(object sender, RoutedEventArgs e)
        {
            UploadFiles();
        }

        private void UploadFiles()
        {
            if (_files.Count == 0)
            {
                MsgBox.Error("Nie wybrano żadnych plików.");
            }
            else
            {
                _files.UploadFiles();
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearFilesList();
        }

        private void ClearFilesList()
        {
            _files.Clear();

        }

        private void AddFile(FileInfo file)
        {
            string fileName = file.Name;

            UserFile userFile = new UserFile();
            userFile.FileName = file.Name;
            userFile.FileStream = file.OpenRead();

            if (userFile.FileStream.Length <= 1000000)
            {
                _files.Add(userFile);
            }
            else
            {
                MsgBox.Error("Plik jest za duży.");
            }
        }

        public void SetRowTemplate(Type type)
        {
            FileList.ItemTemplate = DataTemplateHelper.CreateDataTemplate(type);
        }
    }
}
