﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using Finder.BloodDonation.Tabs.UnitsList;
using System.Collections.ObjectModel;
using Finder.BloodDonation.Common;
using System.Windows.Interactivity;
using Finder.BloodDonation.Behaviors;
using Finder.BloodDonation.Settings;
using System.Windows.Controls.Primitives;
using Finder.BloodDonation.Common.Events;
using Microsoft.Practices.Composite.Events;
using Finder.BloodDonation.Controls;
using Finder.BloodDonation.DBBuffers;
using Finder.BloodDonation.Tabs.Monitorable;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Model.Dto.Enums;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.Login;
using Telerik.Windows.Input;

namespace Finder.BloodDonation.Tabs
{
    [UIException]
    public partial class RoomDesigner : UserControl
    {
        public static string LocalizationSeparator { get; set; }
        private const string ROOMS_PREFIX = "Room_";
        private const string MONITORABLE_OBJECT_PREFIX = "MonitorableObject_";
        private const string CLOUD_PREFIX = "Cloud";
        public static bool IsFirstLoad = true;

        #region Rooms
        public static readonly DependencyProperty RoomsProperty =
            DependencyProperty.Register(
            "Rooms", typeof(ObservableCollection<IRoom>),
            typeof(RoomDesigner),
             new PropertyMetadata(RoomsPropertyChanged)
        );

        public ObservableCollection<IRoom> Rooms
        {
            get
            {
                return (ObservableCollection<IRoom>)base.GetValue(RoomsProperty);
            }
            set
            {
                base.SetValue(RoomsProperty, value);
            }
        }

        private static Finder.BloodDonation.Tabs.UnitsList.Object monitorableObjectToShowDetails;
        public static Finder.BloodDonation.Tabs.UnitsList.Object MonitorableObjectToShowDetails
        {
            get
            {
                return monitorableObjectToShowDetails;
            }
            set
            {
                monitorableObjectToShowDetails = value;

                if (value == null)
                    RoomsViewModel.SelectedObject = null;

                EventHandler handler = MonitorableObjectChanged;
                if (handler != null)
                    handler(value, new EventArgs());
            }
        }

        public static event EventHandler MonitorableObjectChanged;
        public static event EventHandler Refresh;

        private static bool isRefreshed;
        public static bool IsRefreshed
        {
            get
            {
                return isRefreshed;
            }

            set
            {
                isRefreshed = value;

                EventHandler handler = Refresh;
                if (handler != null)
                    handler(value, new EventArgs());
            }
        }

        private static bool isShowedCloud;
        public static bool IsShowedCloud
        {
            get
            {
                return isShowedCloud;
            }
            set
            {
                isShowedCloud = value;

                if (isShowedCloud)
                {

                }
            }
        }

        private Image image;
        private SelectedObject selectedObject;

        public struct SelectedObject
        {
            public double positionX { get; set; }
            public double positionY { get; set; }

            public double Width { get; set; }
            public double Height { get; set; }
        }

        private static void RoomsPropertyChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            try
            {
                RoomDesigner rd = sender as RoomDesigner;

                if (rd != null)
                {
                    rd.DrawedCloud = null;
                    rd.selectedMonitorable = null;
                    monitorableObjectToShowDetails = null;
                    rd.DeselectMonitorableObject();

                    if (rd.selectManager != null)
                        rd.selectManager.SelectedObject = null;

                    if (rd.viewport != null && rd.viewport.Children != null)
                    {
                        rd.viewport.Children.Clear();
                    }

                    if (rd.MonitorableObjects != null)
                        for (int i = 0; i < rd.MonitorableObjects.Count; i++)
                        {
                            rd.MonitorableObjects[i].Style = "sensor_object";
                        }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private SelectManager selectManager;
        #endregion

        #region Patients
        public static readonly DependencyProperty MonitorableObjectsProperty =
            DependencyProperty.Register("MonitorableObjects", typeof(ObservableCollection<IObjectable>), typeof(RoomDesigner), new PropertyMetadata(PatientsPropertyChanged));

        private void LoadImage()
        {
            try
            {
                var sourceImage = new ImageBrush()
                {
                    ImageSource =
                        new BitmapImage(new Uri(BitmapBuffer.GetFileLocation("ludzik.png", DBBuffers.ImagesFolder.Objects),
                            UriKind.RelativeOrAbsolute)),
                    Stretch = Stretch.Uniform
                };

                BitmapImage bi = sourceImage.ImageSource as BitmapImage;

                image = new Image();
                image.Source = bi;
                image.Stretch = Stretch.Uniform;

                bi.ImageOpened += (sender, e) =>
                {
                    var brush = new ImageBrush();
                    brush.ImageSource = sourceImage.ImageSource;
                    image.Source = brush.ImageSource;
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public ObservableCollection<IObjectable> MonitorableObjects
        {
            get
            {
                return (ObservableCollection<IObjectable>)base.GetValue(MonitorableObjectsProperty);
            }
            set
            {

                if (!CheckIfChangeInMonitorableObjects(value))
                    base.SetValue(MonitorableObjectsProperty, value);
            }
        }

        private bool CheckIfChangeInMonitorableObjects(ObservableCollection<IObjectable> value)
        {
            for (int i = 0; i < value.Count; i++)
            {
                if (MonitorableObjects == null)
                    return false;

                if (value.Count > MonitorableObjects.Count)
                    return false;


                if (value[i].MonitorableObject.Id != MonitorableObjects[i].MonitorableObject.Id)
                    return false;

                if (value[i].MonitorableObject.DisplayColor != MonitorableObjects[i].MonitorableObject.DisplayColor)
                    return false;

                if (value[i].MonitorableObject.CurrentUnitId != MonitorableObjects[i].MonitorableObject.CurrentUnitId)
                    return false;

                if (value[i].MonitorableObject.PreviousUnitId != MonitorableObjects[i].MonitorableObject.PreviousUnitId)
                    return false;
            }

            return true;
        }

        private static void PatientsPropertyChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            RoomDesigner rd = sender as RoomDesigner;

            if (rd != null)
            {
                rd.UpdateViewport();
            }
        }
        #endregion

        #region Path
        public

        static readonly DependencyProperty PathProperty =
            DependencyProperty.Register(
            "Path", typeof(string),
            typeof(RoomDesigner),
             new PropertyMetadata(PathPropertyChanged)
        );

        public string Path
        {
            get { return (string)base.GetValue(PathProperty); }
            set { base.SetValue(PathProperty, value); }
        }

        private static void PathPropertyChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            RoomDesigner rd = sender as RoomDesigner;

            if (rd != null)
            {
                rd.UpdatePath();
            }
        }
        #endregion

        public bool EditMode { get; set; }

        public List<Room> DrawedRooms { get; set; }
        public List<Room> DrawedMonitorableObject { get; set; }
        public List<Room> DrawedListElement { get; set; }
        public Room DrawedCloud { get; set; }
        public List<Room> DrawedMiniClouds { get; set; }

        public RoomDesigner()
        {
            try
            {
                InitializeComponent();

                MonitorableObjectToShowDetails = null;
                DrawedRooms = new List<Room>();
                DrawedMonitorableObject = new List<Room>();
                DrawedListElement = new List<Room>();
                viewport.Height = Double.NaN;
                viewport.Width = Double.NaN;

                this.Loaded += new RoutedEventHandler(RoomDesigner_Loaded);
                this.SizeChanged += new SizeChangedEventHandler(RoomDesigner_SizeChanged);
                MonitorableObjectChanged += RoomDesigner_MonitorableObjectChanged;
                Refresh += RoomDesigner_Refresh;
                LoadImage();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        void RoomDesigner_Refresh(object sender, EventArgs e)
        {
            UpdateViewPortForRescale();
        }

        private void RoomDesigner_MonitorableObjectChanged(object sender, EventArgs e)
        {
            DeselectAllMonitorableObjectsIfSelected();
        }

        double im_w = 0;
        double im_h = 0;

        void ib_ImageOpened(object sender, RoutedEventArgs e)
        {
            try
            {
                viewport.Visibility = System.Windows.Visibility.Visible;
                ImageBrush ib = sender as ImageBrush;
                BitmapImage bi = ib.ImageSource as BitmapImage;
                im_w = bi.PixelWidth;
                im_h = bi.PixelHeight;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public void ClearDetailsList()
        {
            try
            {
                if (DrawedListElement == null)
                    return;

                if (DrawedListElement.Count <= 0)
                    return;

                for (int i = 0; i < DrawedListElement.Count; i++)
                    details_viewport.Children.Remove(DrawedListElement[i]);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private DetailsListBuilder dlb;

        private void UpdateDetailsViewport()
        {
            try
            {
                ClearDetailsList();

                if (MonitorableObjects == null)
                {
                    return;
                }

                details_viewport.Width = basesGrid.ColumnDefinitions[0].ActualWidth;

                List<IObjectable> mo = null;
                List<IRoom> r = null;
                if (MonitorableObjects != null)
                {
                    mo = new List<IObjectable>(MonitorableObjects);
                }

                if (Rooms != null)
                {
                    r = new List<IRoom>(Rooms);
                }

                if (dlb == null && visibilityState)
                {
                    dlb = new DetailsListBuilder(r, mo, details_viewport, basesGrid, LocalizationSeparator);
                    dlb.SelectedMonitorableObjectChanged += dlb_SelectedMonitorableObjectChanged;
                }

                if (visibilityState)
                    dlb.Update(r, mo);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        private bool IsRescale = false;
        private bool IsInitialized = false;

        private void UpdateViewport()
        {
            try
            {
                if (slScale == null) return;
                if (!IsInitialized) IsRescale = false;

                double scroll_width = 12;
                double scroll_height = 12;

                double proportion = Math.Min((scroll.ActualWidth - scroll_width) / im_w, (scroll.ActualHeight - scroll_height) / im_h);
                proportion = Math.Max(proportion * slScale.Value, 0);
                LayoutRoot.Opacity = 1d;
                LayoutRoot.Width = im_w * proportion;
                LayoutRoot.Height = im_h * proportion;

                if (LayoutRoot.Width > scroll.ActualWidth)
                {
                    scroll.HorizontalScrollBarVisibility = ScrollBarVisibility.Visible;
                }
                else
                {
                    scroll.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
                }

                if (LayoutRoot.Height > scroll.ActualHeight)
                {
                    scroll.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
                }
                else
                {
                    scroll.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
                }

                viewport.Width = LayoutRoot.Width;
                viewport.Height = LayoutRoot.Height;

                if (IsFirstLoad)
                {

                    if (!String.IsNullOrEmpty(LocalStorageSettings.MonitorableObjectScale))
                        objScale.Value = double.Parse(LocalStorageSettings.MonitorableObjectScale);

                    if (!String.IsNullOrEmpty(LocalStorageSettings.LocalizationPlanViewScale))
                        slScale.Value = double.Parse(LocalStorageSettings.LocalizationPlanViewScale);

                    IsFirstLoad = false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }

            try
            {
                //if (!IsRescale)
                //{
                    LoadImage();
                    RedrawRooms();
                    DrawCloud();
                    UpdateDetailsViewport();
                //}
                //else
                //{
                 //   ResizeRooms();
                //}

                IsRescale = false;
                IsInitialized = true;
                SetMonitorableObjectWithAlarmsToFront();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public void UpdatePath()
        {
            try
            {
                if (String.IsNullOrEmpty(LocalStorageSettings.LocalizationPlanViewScale))
                    slScale.Value = 0.7;

                viewport.Children.Clear();
                viewport.Visibility = System.Windows.Visibility.Collapsed;
                if (!string.IsNullOrEmpty(Path))
                {
                    ImageBrush ib = new ImageBrush()
                    {
                        ImageSource = new BitmapImage(new Uri(Path, UriKind.RelativeOrAbsolute)),
                        Stretch = Stretch.Uniform
                    };

                    ib.ImageOpened += new EventHandler<RoutedEventArgs>(ib_ImageOpened);
                    LayoutRoot.Opacity = 0d;
                    LayoutRoot.Background = ib;
                }
                else
                {
                    LayoutRoot.Background = null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        void RoomDesigner_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //UpdateViewport();
        }

        void RoomDesigner_Loaded(object sender, RoutedEventArgs e)
        {
            //UpdateViewport();
        }

        private Brush fill = new SolidColorBrush(Color.FromArgb(128, 255, 0, 0));

        private void DeselectMonitorableObject()
        {
            try
            {
                if (DrawedCloud != null)
                {
                    viewport.Children.Remove(DrawedCloud);
                    DrawedCloud = null;
                }

                if (MonitorableObjects == null)
                    return;

                selectManager = null;

                for (int i = 0; i < MonitorableObjects.Count; i++)
                {
                    MonitorableObjects[i].Style = "sensor_object";
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        // Rysowanie chmury.
        private void SelectMonitorableObject(Room r, IObjectable selectedMonitorableObject)
        {
            try
            {
                if (MonitorableObjects == null)
                    return;

                if (selectedMonitorableObject == null)
                    return;

                if (r != null)
                {
                    if (MonitorableObjectToShowDetails != null && selectedMonitorableObject.MonitorableObject == MonitorableObjectToShowDetails)
                    {
                        DrawedMonitorableObject.Add(r);
                        viewport.Children.Add(r);

                        selectedObject = new SelectedObject();
                        selectedObject.Height = selectedMonitorableObject.MonitorableObject.Height;
                        selectedObject.Width = selectedMonitorableObject.MonitorableObject.Width;
                        selectedObject.positionX = selectedMonitorableObject.MonitorableObject.PositionInViewportX;
                        selectedObject.positionY = selectedMonitorableObject.MonitorableObject.PositionInViewportY;

                        Canvas.SetTop(r, selectedMonitorableObject.MonitorableObject.PositionInViewportY);
                        Canvas.SetLeft(r, selectedMonitorableObject.MonitorableObject.PositionInViewportX);
                    }
                }

                CheckHit();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void DrawMiniClouds()
        {
            try
            {
                if (MonitorableObjects == null)
                    return;

                if (DrawedMiniClouds != null
                    && DrawedMiniClouds.Count >= 0)
                {
                    for (int i = 0; i < DrawedMiniClouds.Count; i++)
                        viewport.Children.Remove(DrawedMiniClouds[i]);

                    DrawedMiniClouds.Clear();
                }
                if (DrawedMiniClouds == null)
                    DrawedMiniClouds = new List<Room>();

                for (int i = 0; i < MonitorableObjects.Count; i++)
                {
                    Room cloud = new Room();

                    double h = 30d;
                    double w = 60d;

                    cloud.Width = w;
                    cloud.Height = h;

                    if (!EditMode)
                    {
                        cloud.DataContext = MonitorableObjects[i];
                    }
                    if (!string.IsNullOrEmpty(MonitorableObjects[i].Style))
                    {
                        cloud.Style = Application.Current.Resources["miniObject_view"] as Style;
                    }

                    double positionX = MonitorableObjects[i].MonitorableObject.PositionInViewportX - ((0.5 * w) + 0.1d * MonitorableObjects[i].MonitorableObject.Width);
                    double positionY = MonitorableObjects[i].MonitorableObject.PositionInViewportY - h;

                    DrawedMiniClouds.Add(cloud);
                    viewport.Children.Add(cloud);

                    Canvas.SetTop(cloud, positionY);
                    Canvas.SetLeft(cloud, positionX);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void DrawCloudWithoutReload()
        {
            try
            {
                if (DrawedCloud != null)
                {
                    viewport.Children.Remove(DrawedCloud);
                    DrawedCloud = null;
                }

                if (monitorableObjectToShowDetails != null)
                {
                    IObjectable monitorableObjectToShowDetailss = MonitorableObjects.FirstOrDefault(e => e.MonitorableObject == MonitorableObjectToShowDetails);

                    if (monitorableObjectToShowDetailss != null && monitorableObjectToShowDetailss.Style != null)
                    {
                        Room cloud = new Room();

                        double h = 75d;
                        double w = 200d;

                        cloud.Width = w;
                        cloud.Height = h;

                        if (!EditMode)
                        {
                            cloud.DataContext = monitorableObjectToShowDetailss;
                        }
                        if (!string.IsNullOrEmpty(monitorableObjectToShowDetailss.Style) && monitorableObjectToShowDetailss.MonitorableObject.ObjectType == ObjectType.Patient)
                        {
                            cloud.Style = Application.Current.Resources["object_view"] as Style;
                        }
                        else if (!string.IsNullOrEmpty(monitorableObjectToShowDetailss.Style) && monitorableObjectToShowDetailss.MonitorableObject.ObjectType == ObjectType.Band)
                        {
                            cloud.Style = Application.Current.Resources["object_view_ForBands"] as Style;
                        }

                        double positionX = monitorableObjectToShowDetailss.MonitorableObject.PositionInViewportX - (100d - (0.5d * monitorableObjectToShowDetailss.MonitorableObject.Width));
                        double positionY = monitorableObjectToShowDetailss.MonitorableObject.PositionInViewportY - h;

                        DrawedCloud = cloud;
                        viewport.Children.Add(cloud);

                        Canvas.SetTop(cloud, positionY);
                        Canvas.SetLeft(cloud, positionX);
                        Canvas.SetZIndex(cloud, 1);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void DrawCloud()
        {
            try
            {
                if (DrawedCloud != null)
                {
                    viewport.Children.Remove(DrawedCloud);
                    DrawedCloud = null;
                }

                if (MonitorableObjectToShowDetails != null)
                {
                    IObjectable monitorableObjectToShowDetailss = MonitorableObjects.FirstOrDefault(e => e.MonitorableObject.Id == MonitorableObjectToShowDetails.Id);

                    Room cloud = new Room();

                    double h = 75d;
                    double w = 200d;

                    cloud.Width = w;
                    cloud.Height = h;
                    cloud.TempName = CLOUD_PREFIX;

                    if (monitorableObjectToShowDetailss != null)
                    {
                        if (!EditMode)
                        {
                            cloud.DataContext = monitorableObjectToShowDetailss;
                        }
                        if (!string.IsNullOrEmpty(monitorableObjectToShowDetailss.Style) && monitorableObjectToShowDetailss.MonitorableObject.ObjectType == ObjectType.Patient)
                        {
                            cloud.Style = Application.Current.Resources["object_view"] as Style;
                        }
                        else if(!string.IsNullOrEmpty(monitorableObjectToShowDetailss.Style) && monitorableObjectToShowDetailss.MonitorableObject.ObjectType == ObjectType.Band)
                        {
                            cloud.Style = Application.Current.Resources["object_view_ForBands"] as Style;
                        }

                        double positionX = monitorableObjectToShowDetailss.MonitorableObject.PositionInViewportX - (100d - (0.5d * monitorableObjectToShowDetailss.MonitorableObject.Width));
                        double positionY = monitorableObjectToShowDetailss.MonitorableObject.PositionInViewportY - h;

                        DrawedCloud = cloud;
                        viewport.Children.Add(cloud);

                        Canvas.SetZIndex(cloud, 1);
                        Canvas.SetTop(cloud, positionY);
                        Canvas.SetLeft(cloud, positionX);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void CheckHit()
        {
            selectManager = new SelectManager(new List<IObjectable>(MonitorableObjects), image);
        }

        private int selectedIndexX = 0;
        private int selectedIndexY = 0;

        IObjectable selectedMonitorable = null;
        Room selectedRoom = null;

        private bool CheckIfMonitorableObjectIsInParent(IObjectable objectToCheck,int parentUnitId)
        {
            return objectToCheck.MonitorableObject.CurrentUnitId == parentUnitId || (objectToCheck.MonitorableObject.CurrentUnitId == null && objectToCheck.MonitorableObject.PreviousUnitId == parentUnitId);
        }

        private List<IObjectable> GetMonitorableObjectWithoutDouble(List<IObjectable> listToSearch)
        {
            List<IObjectable> l = new List<IObjectable>();
            bool isChecked = true;

            for(int i = 0; i < listToSearch.Count; i++)
            {
                for(int j = 0; j < l.Count; j++)
                {
                    if(listToSearch[i].MonitorableObject.Id == l[j].MonitorableObject.Id)
                    {
                        isChecked = false;
                    }
                }

                if(isChecked == true)
                {
                    l.Add(listToSearch[i]);
                }

                isChecked = true;
            }

            return l;
        }

        private void RedrawMonitorableObjectsForParent(int parentUnitId)
        {
            try
            {
                int indexX = 0;
                int indexY = 0;
                int selectedMonitorableIndex = -1;

                if (MonitorableObjects != null && !double.IsNaN(viewport.Height) && !double.IsNaN(viewport.Width))
                {
                    List<IObjectable> monitorablesForUnit = MonitorableObjects.Where(e => CheckIfMonitorableObjectIsInParent(e, parentUnitId)).ToList();

                    if (monitorablesForUnit == null)
                        return;

                    for (int i = 0; i < monitorablesForUnit.Count; i++)
                    {
                        var parent = Rooms.FirstOrDefault(e => CheckIfMonitorableObjectIsInParent(monitorablesForUnit[i],e.Unit.Id));
                            

                        if (parent == null)
                            break;

                        Room r = new Room();

                        r.TempName = MONITORABLE_OBJECT_PREFIX + monitorablesForUnit[i].MonitorableObject.Id;
                        monitorablesForUnit[i].TempName = MONITORABLE_OBJECT_PREFIX + monitorablesForUnit[i].MonitorableObject.Id;

                        double roomHeight = Convert.ToDouble(viewport.Height * Rooms.FirstOrDefault(e => CheckIfMonitorableObjectIsInParent(monitorablesForUnit[i], e.Unit.Id)).BR.X) - Convert.ToDouble(viewport.Height * Rooms.FirstOrDefault(e => CheckIfMonitorableObjectIsInParent(monitorablesForUnit[i], e.Unit.Id)).TL.X);
                        double roomWidth = Convert.ToDouble(viewport.Width * Rooms.FirstOrDefault(e => CheckIfMonitorableObjectIsInParent(monitorablesForUnit[i], e.Unit.Id)).BR.Y) - Convert.ToDouble(viewport.Width * Rooms.FirstOrDefault(e => CheckIfMonitorableObjectIsInParent(monitorablesForUnit[i], e.Unit.Id)).TL.Y);

                        monitorablesForUnit[i].MonitorableObject.Height = 100d * objScale.Value;
                        monitorablesForUnit[i].MonitorableObject.Width = 100d * objScale.Value;

                        r.Width = monitorablesForUnit[i].MonitorableObject.Width;
                        r.Height = monitorablesForUnit[i].MonitorableObject.Height;

                        if (!EditMode)
                        {
                            r.DataContext = monitorablesForUnit[i];
                        }
                        if (!string.IsNullOrEmpty(monitorablesForUnit[i].Style))
                        {
                            r.Style = Application.Current.Resources[monitorablesForUnit[i].Style] as Style;
                        }

                        monitorablesForUnit[i].MonitorableObject.PositionInViewportX = (viewport.Width * parent.TL.Y) + (((roomWidth * 0.1d) * indexX) - (0.3 * monitorablesForUnit[i].MonitorableObject.Width));
                        monitorablesForUnit[i].MonitorableObject.PositionInViewportY = (viewport.Height * parent.TL.X) + (((roomHeight * 0.2d) * indexY) - (0.3 * monitorablesForUnit[i].MonitorableObject.Height));

                        if (monitorablesForUnit[i].MonitorableObject.PositionInViewportX >= ((viewport.Width * parent.TL.Y) + roomWidth) - (0.5 * monitorablesForUnit[i].MonitorableObject.Width))
                        {
                            indexY++;
                            indexX = 0;
                        }
                        else
                        {
                            indexX++;
                        }

                        if (monitorablesForUnit[i].Style == "sensor_object_selected")
                        {
                            selectedRoom = r;
                            selectedMonitorableIndex = i;
                            monitorableObjectToShowDetails = monitorablesForUnit[i].MonitorableObject;
                        }
                        else
                        {
                            DrawedMonitorableObject.Add(r);
                            viewport.Children.Add(r);

                            if (MonitorableObjectToShowDetails != null && monitorablesForUnit[i].MonitorableObject == MonitorableObjectToShowDetails)
                            {
                                selectedObject = new SelectedObject();
                                selectedObject.Height = monitorablesForUnit[i].MonitorableObject.Height;
                                selectedObject.Width = monitorablesForUnit[i].MonitorableObject.Width;
                                selectedObject.positionX = monitorablesForUnit[i].MonitorableObject.PositionInViewportX;
                                selectedObject.positionY = monitorablesForUnit[i].MonitorableObject.PositionInViewportY;
                            }

                            Canvas.SetTop(r, monitorablesForUnit[i].MonitorableObject.PositionInViewportY);
                            Canvas.SetLeft(r, monitorablesForUnit[i].MonitorableObject.PositionInViewportX);
                        }
                    }

                    IObjectable mon = null;

                    if (selectedMonitorableIndex != -1)
                    {
                        mon = monitorablesForUnit[selectedMonitorableIndex];
                    }

                    SelectMonitorableObject(selectedRoom, mon);
                }
            }
            catch (Exception exception)
            {
                throw new Exception(exception.ToString());
            }
        }

        private void RedrawMonitorableObjects()
        {
            try
            {
                if (Rooms == null)
                    return;

                if (MonitorableObjects == null)
                    return;

                if (MonitorableObjects.Count <= 0)
                    return;

                ClearMonitorableObjects();

                for (int i = 0; i <= Rooms.Count - 1; i++)
                {
                    RedrawMonitorableObjectsForParent(Rooms[i].Unit.Id);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void ResizeRooms()
        {
            try
            {
                if (viewport != null && viewport.Children != null)
                {
                    if (Rooms != null)
                    {
                        for (int i = 0; i < Rooms.Count; i++)
                        {
                            for (int j = 0; j < viewport.Children.Count; j++)
                            {
                                if (((Room)viewport.Children[j]) != null && ((Room)viewport.Children[j]).TempName != null && Rooms[i] != null)
                                {
                                    if (((Room)viewport.Children[j]).TempName.Contains(ROOMS_PREFIX + Rooms[i].Unit.Id))
                                    {
                                        Room r = (Room)viewport.Children[j];

                                        double h = Convert.ToDouble(viewport.Height * Rooms[i].BR.X) - Convert.ToDouble(viewport.Height * Rooms[i].TL.X);
                                        double w = Convert.ToDouble(viewport.Width * Rooms[i].BR.Y) - Convert.ToDouble(viewport.Width * Rooms[i].TL.Y);

                                        r.Width = w;
                                        r.Height = h;

                                        Canvas.SetTop(r, viewport.Height * Rooms[i].TL.X);
                                        Canvas.SetLeft(r, viewport.Width * Rooms[i].TL.Y);
                                    }
                                }
                            }

                            ResizeMonitorableObjectForParent(Rooms[i].Unit.Id);
                            ResizeCloud();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void ResizeCloud()
        {
            try
            {
                if (MonitorableObjectToShowDetails != null && DrawedCloud != null)
                {
                    IObjectable monitorableObjectToShowDetailss = MonitorableObjects.FirstOrDefault(e => e.MonitorableObject.Id == MonitorableObjectToShowDetails.Id);

                    Room cloud = DrawedCloud;

                    double h = 75d;
                    double w = 200d;

                    cloud.Width = w;
                    cloud.Height = h;

                    double positionX = monitorableObjectToShowDetailss.MonitorableObject.PositionInViewportX - (100d - (0.5d * monitorableObjectToShowDetailss.MonitorableObject.Width));
                    double positionY = monitorableObjectToShowDetailss.MonitorableObject.PositionInViewportY - h;

                    Canvas.SetTop(cloud, positionY);
                    Canvas.SetLeft(cloud, positionX);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void ResizeMonitorableObjectForParent(int parentUnitId)
        {
            try
            {
                int indexX = 0;
                int indexY = 0;

                if (MonitorableObjects != null && !double.IsNaN(viewport.Height) && !double.IsNaN(viewport.Width))
                {
                    List<IObjectable> monitorablesForUnit = MonitorableObjects.Where(e => CheckIfMonitorableObjectIsInParent(e, parentUnitId)).ToList();

                    if (monitorablesForUnit == null)
                        return;

                    for (int i = 0; i < monitorablesForUnit.Count; i++)
                    {
                        var parent = Rooms.FirstOrDefault(e => CheckIfMonitorableObjectIsInParent(monitorablesForUnit[i], e.Unit.Id));

                        if (parent == null)
                            break;

                        for (int j = 0; j < viewport.Children.Count; j++)
                        {
                            if (((Room)viewport.Children[j]).TempName != null && monitorablesForUnit[i] != null)
                            {
                                if (((Room)viewport.Children[j]).TempName.Contains(MONITORABLE_OBJECT_PREFIX + monitorablesForUnit[i].MonitorableObject.Id))
                                {
                                    Room r = (Room)viewport.Children[j];

                                    monitorablesForUnit[i].TempName = MONITORABLE_OBJECT_PREFIX + monitorablesForUnit[i].MonitorableObject.Id;

                                    double roomHeight = Convert.ToDouble(viewport.Height * Rooms.FirstOrDefault(e => CheckIfMonitorableObjectIsInParent(monitorablesForUnit[i], e.Unit.Id)).BR.X) - Convert.ToDouble(viewport.Height * Rooms.FirstOrDefault(e => CheckIfMonitorableObjectIsInParent(monitorablesForUnit[i], e.Unit.Id)).TL.X);
                                    double roomWidth = Convert.ToDouble(viewport.Width * Rooms.FirstOrDefault(e => CheckIfMonitorableObjectIsInParent(monitorablesForUnit[i], e.Unit.Id)).BR.Y) - Convert.ToDouble(viewport.Width * Rooms.FirstOrDefault(e => CheckIfMonitorableObjectIsInParent(monitorablesForUnit[i], e.Unit.Id)).TL.Y);

                                    monitorablesForUnit[i].MonitorableObject.Height = 100d * objScale.Value;
                                    monitorablesForUnit[i].MonitorableObject.Width = 100d * objScale.Value;

                                    monitorablesForUnit[i].MonitorableObject.PositionInViewportX = (viewport.Width * parent.TL.Y) + (((roomWidth * 0.1d) * indexX) - (0.3 * monitorablesForUnit[i].MonitorableObject.Width));
                                    monitorablesForUnit[i].MonitorableObject.PositionInViewportY = (viewport.Height * parent.TL.X) + (((roomHeight * 0.2d) * indexY) - (0.3 * monitorablesForUnit[i].MonitorableObject.Height));

                                    if (monitorablesForUnit[i].MonitorableObject.PositionInViewportX >= ((viewport.Width * parent.TL.Y) + roomWidth) - (0.5 * monitorablesForUnit[i].MonitorableObject.Width))
                                    {
                                        indexY++;
                                        indexX = 0;
                                    }
                                    else
                                    {
                                        indexX++;
                                    }

                                    r.Width = monitorablesForUnit[i].MonitorableObject.Width;
                                    r.Height = monitorablesForUnit[i].MonitorableObject.Height;

                                    Canvas.SetTop(r, monitorablesForUnit[i].MonitorableObject.PositionInViewportY);
                                    Canvas.SetLeft(r, monitorablesForUnit[i].MonitorableObject.PositionInViewportX);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void SetMonitorableObjectWithAlarmsToFront()
        {
            try
            {
                if (Rooms != null && !double.IsNaN(viewport.Height) && !double.IsNaN(viewport.Width))
                {
                    var x = GetAlarmsMonitorableObject();

                    if (x == null)
                        return;

                    if (x != null)
                        for (int i = 0; i < x.Count(); i++)
                            Canvas.SetZIndex(x.ElementAt(i), 0);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void SetMonitorableObjectsTypeVisibility(Visibility visibilityState, ObjectType objectType)
        {
            try
            {
                if (Rooms != null && !double.IsNaN(viewport.Height) && !double.IsNaN(viewport.Width))
                {
                    var x = GetMonitorableObjectsForType(objectType);

                    if (x == null)
                        return;

                    for (int i = 0; i < x.Count(); i++)
                        x.ElementAt(i).Visibility = visibilityState;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private IEnumerable<Room> GetMonitorableObjectsForType(ObjectType objectType)
        {
            if (MonitorableObjects != null)
                for (int i = 0; i < MonitorableObjects.Count; i++)
                    if (MonitorableObjects[i].MonitorableObject.ObjectType == objectType &&
                        MonitorableObjects[i].MonitorableObject.DisplayColor != BitmapMode.Red)
                        yield return viewport.Children
                            .OfType<Room>()
                            .FirstOrDefault(x => x.TempName.Contains(MONITORABLE_OBJECT_PREFIX + MonitorableObjects[i].MonitorableObject.Id));
        }

        private List<Room> GetAlarmsMonitorableObject()
        {
            try
            {
                List<Room> outputRooms = new List<Room>();

                if (MonitorableObjects == null)
                    return outputRooms;

                if (viewport == null)
                    return outputRooms;

                if (viewport.Children == null)
                    return outputRooms;

                for (int i = 0; i < MonitorableObjects.Count; i++)
                {
                    if (MonitorableObjects[i].MonitorableObject != null)
                    {
                        if (MonitorableObjects[i].MonitorableObject.DisplayColor.Equals(BitmapMode.Red))
                        {
                            var s = GetRoomWithName(MONITORABLE_OBJECT_PREFIX + MonitorableObjects[i].MonitorableObject.Id);

                            if (s != null)
                                outputRooms.Add(s);
                        }
                    }
                }

                return outputRooms;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private Room GetRoomWithName(string name)
        {
            try
            {
                for (int i = 0; i < viewport.Children.Count; i++)
                {
                    if (viewport.Children[i].GetType().Equals(typeof(Room)))
                    {
                        if ((viewport.Children[i] as Room) != null)
                        {
                            if ((viewport.Children[i] as Room).TempName.Contains(name))
                            {
                                return (viewport.Children[i] as Room);
                            }
                        }
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void RedrawRooms()
        {
            try
            {
                ClearRooms();

                if (Rooms != null && !double.IsNaN(viewport.Height) && !double.IsNaN(viewport.Width))
                {
                    for (int i = 0; i < Rooms.Count; i++)
                    {
                        Room r = new Room();
                        r.TempName = ROOMS_PREFIX + Rooms[i].Unit.Id;
                        double h = Convert.ToDouble(viewport.Height * Rooms[i].BR.X) - Convert.ToDouble(viewport.Height * Rooms[i].TL.X);
                        double w = Convert.ToDouble(viewport.Width * Rooms[i].BR.Y) - Convert.ToDouble(viewport.Width * Rooms[i].TL.Y);

                        r.Width = w;
                        r.Height = h;
                        if (!string.IsNullOrEmpty(Rooms[i].Style))
                        {
                            r.Style = Application.Current.Resources[Rooms[i].Style] as Style;
                        }

                        if (!EditMode)
                        {
                            r.DataContext = Rooms[i];
                        }

                        viewport.Children.Add(r);

                        if (EditMode)
                        {
                            Interaction.GetBehaviors(r).Add(new RoomMoveBehavior());
                        }

                        Canvas.SetTop(r, viewport.Height * Rooms[i].TL.X);
                        Canvas.SetLeft(r, viewport.Width * Rooms[i].TL.Y);
                        DrawedRooms.Add(r);
                    }

                    UpdateObjectLayer();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void ClearMonitorableObjects()
        {
            for (int i = 0; i < DrawedMonitorableObject.Count; i++)
            {
                viewport.Children.Remove(DrawedMonitorableObject[i]);
            }

            DrawedMonitorableObject.Clear();
        }

        private void ClearRooms()
        {
            for (int i = 0; i < DrawedRooms.Count; i++)
            {
                viewport.Children.Remove(DrawedRooms[i]);
            }

            DrawedRooms.Clear();
        }

        private void btnFill_Click(object sender, RoutedEventArgs e)
        {
            slScale.Value = 1;
        }

        private void btn100_Click(object sender, RoutedEventArgs e)
        {
            UpdateViewport();
        }

        private void slScale_ValueChanged(object sender, RoutedEventArgs e)
        {
            if (slScale != null)
                LocalStorageSettings.LocalizationPlanViewScale = slScale.Value.ToString();

            IsRescale = true;
            UpdateViewport();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void objScale_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                //UpdateObjectLayer();
                //LocalStorageManager.SaveSettings("MonitorableObjectScale", objScale.Value.ToString());
                if (objScale != null)
                    LocalStorageSettings.MonitorableObjectScale = objScale.Value.ToString();

                UpdateViewPortForRescale();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void UpdateObjectLayer()
        {
            RedrawMonitorableObjects();
        }

        public object ShowCloudEvent_Event { get; set; }

        private double spliterWidth = 0d;
        private bool visibilityState = true;

        private void testChanged(object sender, RoutedEventArgs e)
        {
            if (visibilityState.Equals(true))
            {
                spliterWidth = basesGrid.ColumnDefinitions[0].ActualWidth;
                basesGrid.ColumnDefinitions[0].Width = new GridLength(0d);
                gridSpliter.IsEnabled = false;
                visibilityState = false;

                details_viewport.Width = basesGrid.ColumnDefinitions[0].ActualWidth;

                if (dlb != null)
                    dlb.FixedUpdate();

                UpdateViewPortForRescale();
            }
            else
            {
                basesGrid.ColumnDefinitions[0].Width = new GridLength(spliterWidth);
                gridSpliter.IsEnabled = true;
                visibilityState = true;
            }
        }

        private void viewport_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (sender != null && e != null)
                SelectedMonitorableObjectsWithoutUpdated(sender, e);
        }

        private void SelectedMonitorableObjectsWithoutUpdated(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                if (MonitorableObjects == null)
                    return;

                DeselectAllMonitorableObjectsIfSelected();
                LoadImage();
                selectManager = new SelectManager(new List<IObjectable>(MonitorableObjects), image);

                Point mouseClick = new Point(e.GetPosition(viewport).X, e.GetPosition(viewport).Y);
                selectManager.Click(mouseClick);

                if (selectManager.SelectedObject != null && selectManager.SelectedObject.Count > 0)
                {
                    for (int i = 0; i < MonitorableObjects.Count; i++)
                    {
                        if (selectManager.SelectedObject.Count >= 1 && selectManager.SelectedObject[0] == MonitorableObjects[i])
                        {
                            RoomsViewModel.SelectedObject = MonitorableObjects[i];
                            MonitorableObjects[i].Style = "sensor_object_selected";
                            monitorableObjectToShowDetails = MonitorableObjects[i].MonitorableObject;

                            if (dlb != null)
                            {
                                var monitorable = viewport.Children.ToArray();
                                for (int j = 0; j < monitorable.Length; j++)
                                {
                                    if (((Room)monitorable[j]).TempName != null)
                                    {
                                        if (((Room)monitorable[j]).TempName.Equals(MONITORABLE_OBJECT_PREFIX + MonitorableObjects[i].MonitorableObject.Id))
                                        {
                                            IsSelectedWithoutReloaded = true;
                                            ((Room)monitorable[j]).Style = Application.Current.Resources["sensor_object_selected"] as Style;
                                            Canvas.SetZIndex(monitorable[j], 1);
                                            dlb.SelectedMonitorableObjectSetterWithoutEvent = MonitorableObjects[i];
                                            IsSelectedWithoutReloaded = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                DrawCloudWithoutReload();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private bool IsSelectedWithoutReloaded { get; set; }

        private void DeselectAllMonitorableObjectsIfSelected()
        {
            try
            {
                DeselectMonitorableObject();

                var monitorable = viewport.Children.ToArray();
                for (int j = 0; j < monitorable.Length; j++)
                {
                    if (((Room)monitorable[j]).Style == Application.Current.Resources["sensor_object_selected"] as Style)
                    {
                        Canvas.SetZIndex(monitorable[j], 0);
                        ((Room)monitorable[j]).Style = Application.Current.Resources["sensor_object"] as Style;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void OldSelectedChangedVersionMethod(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            DeselectMonitorableObject();
            LoadImage();
            selectManager = new SelectManager(new List<IObjectable>(MonitorableObjects), image);

            Point mouseClick = new Point(e.GetPosition(viewport).X, e.GetPosition(viewport).Y);

            selectManager.Click(mouseClick);

            if (selectManager.SelectedObject != null && selectManager.SelectedObject.Count > 0)
            {
                for (int i = 0; i < MonitorableObjects.Count; i++)
                {
                    if (selectManager.SelectedObject.Count >= 1 && selectManager.SelectedObject[0] == MonitorableObjects[i])
                    {
                        MonitorableObjects[i].Style = "sensor_object_selected";
                        if (dlb != null)
                        {
                            dlb.SelectedMonitorableObject = MonitorableObjects[i];
                        }
                    }
                }
            }
        }

        private void dlb_SelectedMonitorableObjectChanged(object sender, EventArgs e)
        {
            try
            {
                double scroll_width = 12;
                double scroll_height = 12;
                DeselectAllMonitorableObjectsIfSelected();

                if (dlb != null && dlb.SelectedMonitorableObject != null)
                {
                    var monitorable = viewport.Children.ToArray();

                    for (int i = 0; i < monitorable.Length; i++)
                    {
                        if (((Room)monitorable[i]).TempName != null)
                        {
                            if (((Room)monitorable[i]).TempName.Equals(MONITORABLE_OBJECT_PREFIX + dlb.SelectedMonitorableObject.MonitorableObject.Id))
                            {
                                ((Room)monitorable[i]).Style = Application.Current.Resources["sensor_object_selected"] as Style;
                                monitorableObjectToShowDetails = dlb.SelectedMonitorableObject.MonitorableObject;
                                Canvas.SetZIndex(monitorable[i], 1);
                            }
                        }
                    }
                }

                DrawCloudWithoutReload();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void details_viewport_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            details_viewport.Width = basesGrid.ColumnDefinitions[0].ActualWidth;

            if (dlb != null)
                dlb.FixedUpdate();

            UpdateViewPortForRescale();
        }

        private void gridSpliter_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        { 

        }

        public bool IsSpliterSizeChanged { get; set; }

        private void basesGrid_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (IsSpliterSizeChanged)
            {
                if (basesGrid.ColumnDefinitions[0].Width.Value >= 100d)
                {
                    details_viewport.Width = basesGrid.ColumnDefinitions[0].ActualWidth;

                    if (dlb != null)
                        dlb.Update(null, null, false);
                }
                else
                {
                    basesGrid.ColumnDefinitions[0].Width = new GridLength(100d);
                }
            }
        }

        private void UpdateViewPortForRescale()
        {
            try
            {
                if (slScale == null) return;

                if (viewport.Children.Count > 0)
                {
                    double scroll_width = 12;
                    double scroll_height = 12;

                    double proportion = Math.Min((scroll.ActualWidth - scroll_width) / im_w, (scroll.ActualHeight - scroll_height) / im_h);
                    proportion = Math.Max(proportion * slScale.Value, 0);

                    LayoutRoot.Width = im_w * proportion;
                    LayoutRoot.Height = im_h * proportion;

                    if (LayoutRoot.Width > scroll.ActualWidth)
                    {
                        scroll.HorizontalScrollBarVisibility = ScrollBarVisibility.Visible;
                    }
                    else
                    {
                        scroll.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
                    }

                    if (LayoutRoot.Height > scroll.ActualHeight)
                    {
                        scroll.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
                    }
                    else
                    {
                        scroll.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
                    }

                    viewport.Width = LayoutRoot.Width;
                    viewport.Height = LayoutRoot.Height;

                    ResizeRooms();
                    SetMonitorableObjectWithAlarmsToFront();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void gridSpliter_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            IsSpliterSizeChanged = true;
        }

        private void gridSpliter_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            IsSpliterSizeChanged = false;
        }

        private void OptionClick(object sender, RoutedEventArgs e)
        {

        }

        private void windowOpener_Button_Click(object sender, RoutedEventArgs e)
        {
            RoomsViewModel.RaiseEvent();
        }

        private void OnShowWindows(object sender, EventArgs e)
        {

        }

        private void viewport_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
            {
                slScale.Value += 0.1;
            }
            else
            {
                slScale.Value -= 0.1;
            }

            scroll.ScrollToVerticalOffset(e.GetPosition(scroll).Y);
            scroll.ScrollToHorizontalOffset(e.GetPosition(scroll).X);
        }

        private Point _mouseStartPosition;
        private Point _mouseEndPosition;
        private bool isDraged = false;

        private void LayoutRoot_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _mouseStartPosition = new Point(e.GetPosition(scroll).X, e.GetPosition(scroll).Y);
            isDraged = true;
        }

        private void LayoutRoot_MouseButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            isDraged = false;
        }

        private void LayoutRoot_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if(isDraged)
            {
                _mouseEndPosition = new Point(e.GetPosition(scroll).X, e.GetPosition(scroll).Y);

                if (
                    (_mouseEndPosition.Y > _mouseStartPosition.Y + 10d || _mouseEndPosition.Y < _mouseStartPosition.Y - 10d) ||
                    (_mouseEndPosition.X > _mouseEndPosition.X + 10d || _mouseEndPosition.X < _mouseStartPosition.X - 10d)
                    )
                {
                    scroll.ScrollToVerticalOffset(scroll.VerticalOffset + (_mouseStartPosition.Y - _mouseEndPosition.Y));
                    scroll.ScrollToHorizontalOffset(scroll.HorizontalOffset + (_mouseStartPosition.X - _mouseEndPosition.X));

                    _mouseStartPosition = _mouseEndPosition;
                }
            }
        }

        public void fullscreenChanged(object sender, RoutedEventArgs e)
        {
            RoomsViewModel.RaiseChangeUserInterface();
        }
    }
}
