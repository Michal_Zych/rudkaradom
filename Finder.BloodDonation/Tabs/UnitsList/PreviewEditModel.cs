﻿using System;
using System.Linq;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Layout;
using System.Collections.ObjectModel;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using System.Collections.Generic;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Tabs.UnitsList;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Common.Events;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Converters;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Dialogs.Plan;
using System.Windows;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Common.Authentication;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.Tools;

namespace Finder.BloodDonation.Tabs
{
    [OnCompleted]
    [UIException]
    public class PreviewEditModel : ViewModelBase, ITabViewModel, IUnitView
    {
        private bool IsUnitNameDisplay { get { return true; } }

        [RaisePropertyChanged]
        public ObservableCollection<UnitDTO> Units { get; set; }

        [RaisePropertyChanged]
        public UnitDTO Units_Selected { get; set; }

        [RaisePropertyChanged]
        public IRoom Rooms_Selected { get; set; }

        [RaisePropertyChanged]
        public int PreviewsSelectedIndex { get; set; }

        [RaisePropertyChanged]
        public int SelectedRoomIndex { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<EditPreviewDto> Previews { get; set; }

        [RaisePropertyChanged]
        public EditPreviewDto Previews_Selected { get; set; }

        private ObservableCollection<IRoom> _rooms;
        [RaisePropertyChanged]
        public ObservableCollection<IRoom> Rooms
        {
            get
            {
                return _rooms;
            }
            set
            {
                _rooms = value;
            }
        }

        private ObservableCollection<IObjectable> _monitorableObjects;

        public ObservableCollection<IObjectable> MonitorableObjects
        {
            get
            {
                return _monitorableObjects;
            }
            set
            {
                _monitorableObjects = value;
            }
        }

        [RaisePropertyChanged]
        public PreviewDto UnitPreview { get; set; }

        [RaisePropertyChanged]
        public string Path { get; set; }

        public bool IsPathDisplay = false;

        [RaisePropertyChanged]
        public bool IsRoomEnabled { get; set; }

        public DelegateCommand<object> SavePreview { get; set; }
        public DelegateCommand<object> AddUnit { get; set; }

        public RelayCommand SelectionChangedCommand { get; set; }
        public RelayCommand DefaultView { get; set; }

        public ObservableCollection<ZonePreviewDto> PreviewUnits { get; set; }

        IUnitsManager units_manager = null;
        private readonly IWindowsManager _windowsManager;
        private readonly ToolsWindow _toolsWindow;

        public PreviewEditModel(IUnityContainer container)
            : base(container)
        {
            units_manager = container.Resolve<IUnitsManager>();

            this.ViewAttached += UnitsListViewModel_ViewAttached;

            SavePreview = new DelegateCommand<object>(SavePreview_Command);
            AddUnit = new DelegateCommand<object>(AddUnit_Command);

            SelectionChangedCommand = new RelayCommand(OnSelectionChangedCommand);
            DefaultView = new RelayCommand(OnDefaultViewCommand);

            EventAggregator.GetEvent<SavePreviewEvent>().Subscribe(HandleSavePreviewEvent);
            EventAggregator.GetEvent<ShowToolsWindow>().Subscribe(HandleShowToolsWindowEvent);

            _windowsManager = container.Resolve<IWindowsManager>();

            var viewFactory = container.Resolve<ViewFactory>();
            _toolsWindow = viewFactory.CreateView<ToolsWindow>();

            _windowsManager.RegisterWindow(WindowsKeys.ToolsWindow, "Położenie/rozmiar elementu", _toolsWindow,
                                           new Size(320, 235), false, false);
        }

        private void OnDefaultViewCommand()
        {
            PreviewsSelectedIndex = 0;
        }

        private void OnSelectionChangedCommand()
        {
            if(Rooms != null && SelectedRoomIndex < Rooms.Count && SelectedRoomIndex >= 0)
            {
                var dc = _toolsWindow.DataContext as ToolsWindowModel;

                if (dc != null)
                {
                    _windowsManager.CloseWindow(WindowsKeys.ToolsWindow);
                }
            }

        }

        public void HandleShowToolsWindowEvent(int obj)
        {
            if (SelectedRoomIndex < Rooms.Count && SelectedRoomIndex >= 0)
            {
                _windowsManager.ShowWindow(WindowsKeys.ToolsWindow, Rooms[SelectedRoomIndex]);
            }
            else
            {
                MsgBox.Warning("Wybierz pokój na liście elementów podglądu!");
            }
        }

        private UnitsServiceClient LoadService
        {
            get
            {
                UnitsServiceClient proxy = ServiceFactory.GetService<UnitsServiceClient>(ServicesUri.UnitsService);
                proxy.GetUnitPreviewCompleted += new EventHandler<GetUnitPreviewCompletedEventArgs>(proxy_GetUnitPreviewCompleted);
                proxy.PreviewForUnitGetCompleted += new EventHandler<PreviewForUnitGetCompletedEventArgs>(proxy_GetPreviewsForUnitCompleted);
                proxy.GetPreviewsCompleted += new EventHandler<GetPreviewsCompletedEventArgs>(proxy_GetPreviewsCompleted);
                proxy.UpdatePreviewUnitsCompleted += new EventHandler<UpdatePreviewUnitsCompletedEventArgs>(proxy_UpdatePreviewUnitsCompleted);
                proxy.GetPatientsCompleted += proxy_GetPatientsCompleted;
                return proxy;
            }
        }


        void proxy_UpdatePreviewUnitsCompleted(object sender, UpdatePreviewUnitsCompletedEventArgs e)
        {
            EventAggregator.GetEvent<UnitPreviewUpdate>().Publish(SelectedUnit);
        }

        void proxy_GetPreviewsCompleted(object sender, GetPreviewsCompletedEventArgs e)
        {
            try
            {
                Previews = new ObservableCollection<EditPreviewDto>(e.Result);

                for (int i = 0; i < Previews.Count; i++)
                {
                    Previews[i].Path = String.Format("{0}.prev", Previews[i].ID);
                }

                var defPreview = new EditPreviewDto()
                {
                    ID = -1,
                    Path = Global.Settings.Current["UnitImagesFolder"] + UnitPreview.PictureID.ToString() + Global.Settings.Current["ImagesExtention"],
                    Name = "widok domyślny",
                    ClientID = 1
                };

                Previews.Insert(0, defPreview);

                if (UnitPreview != null)
                {
                    for (int i = 0; i < Previews.Count; i++)
                    {
                        if (Previews[i].ID == UnitPreview.ID)
                        {
                            Previews_Selected = Previews[i];
                            return;
                        }
                    }
                }
                else
                {
                    if (Previews != null)
                    {
                        if (Previews.Count > 0)
                        {
                            Previews_Selected = Previews[0];
                        }
                    }
                }
            }
            catch (Exception error)
            {

            }
        }

        void proxy_GetPreviewsForUnitCompleted(object sender, PreviewForUnitGetCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                LoadUnits(e.Result.ToList());
            }
            else
            {
                LoadUnits(null);
            }
            LoadService.GetPreviewsAsync();
        }

        void proxy_GetUnitPreviewCompleted(object sender, GetUnitPreviewCompletedEventArgs e)
        {
            UnitPreview = e.Result;
            if (UnitPreview != null)
            {
                if (UnitPreview.ID > 0)
                {
                    Path = string.Format("{0}.prev", UnitPreview.ID);
                }
                else
                {
                    UnitPreview.Path = Global.Settings.Current["UnitImagesFolder"] + UnitTypeOrganizer.GetPictureName(UnitPreview.PictureID) + Global.Settings.Current["ImagesExtention"];
                    Path = UnitPreview.Path;
                }

                IsRoomEnabled = true;
                LoadService.PreviewForUnitGetAsync(UnitPreview.UnitID, BloodyUser.Current.ID);
            }
            else
            {
                Path = null;
                IsRoomEnabled = false;
                LoadUnits(null);
                LoadService.GetPreviewsAsync();
            }
        }

        public void AddUnit_Command(object o)
        {
            if (Units_Selected != null)
            {
                ZonePreviewDto up = new ZonePreviewDto();

                up.Id = Units_Selected.ID;
                up.Name = Units_Selected.Name;

                var dc = _toolsWindow.DataContext as ToolsWindowModel;

                up.TLX = dc.MemoryTop / ToolsWindowModel.SCALE;
                up.TLY = dc.MemoryLeft / ToolsWindowModel.SCALE;
                up.BRX = dc.MemoryHeight + dc.MemoryTop / ToolsWindowModel.SCALE;
                up.BRY = dc.MemoryWidth + dc.MemoryLeft / ToolsWindowModel.SCALE;
                AddRomm(up);
            }
        }

        
        private void OnDefaultTileView()
        {
            
            for(int i = 0; i < Rooms.Count; i++)
            {
                ZonePreviewDto up = new ZonePreviewDto();

                up.Id = Rooms[i].Unit.Id;
                up.Name = Rooms[i].Unit.Name;

                var dc = _toolsWindow.DataContext as ToolsWindowModel;
                var field = dc.MemoryHeight * dc.MemoryWidth;
                //var n = 
                //up.TLX = (dc.MemoryTop / ToolsWindowModel.SCALE)/
            }
        }

        void proxy_GetPatientsCompleted(object sender, GetPatientsCompletedEventArgs e)
        {
            if (MonitorableObjects == null)
                MonitorableObjects = new ObservableCollection<IObjectable>();

            if(Rooms == null || Rooms.Count <= 0)
                return;

        }

        public void SavePreview_Command(object o)
        {
            List<ZonePreviewDto> list = new List<ZonePreviewDto>();
            if (Rooms != null)
            {
                list = (from r in Rooms select r.Unit).ToList();
            }

            LoadService.UpdatePreviewUnitsAsync(list, SelectedUnit, Previews_Selected.ID);
        }

        public void HandleSavePreviewEvent(int obj)
        {
            SavePreview_Command(null);
        }

        public void LoadUnits(List<ZonePreviewDto> units)
        {
            if (units != null)
            {
                PreviewUnits = new ObservableCollection<ZonePreviewDto>(units);
                ObservableCollection<IRoom> lr = new ObservableCollection<IRoom>();

                for (int i = 0; i < PreviewUnits.Count; i++)
                {
                    RoomPlanViewModel r = Container.Resolve<RoomPlanViewModel>();
                    r.ParentManager = this;
                    r.Unit = PreviewUnits[i];

                    if (!IsUnitNameDisplay)
                    {
                        r.Unit.Name = String.Empty;//String.Empty;
                    }

                    r.Style = "room_elevator_edit";

                    lr.Add(r);
                }

                Rooms = lr;
                LoadService.GetPatientsAsync();
            }
            else
            {
                PreviewUnits = null;
            }
        }

        private void AddRomm(ZonePreviewDto preview)
        {
            if (Rooms == null)
            {
                Rooms = new ObservableCollection<IRoom>();
            }

            var room = (from rr in Rooms where rr.Unit.Id == preview.Id select rr).FirstOrDefault();
            if (room != null)
            {
                Rooms.Remove(room);
            }

            RoomPlanViewModel r = Container.Resolve<RoomPlanViewModel>();
            r.ParentManager = this;
            r.Unit = preview;
            
            r.Style = "room_elevator_edit";

            Rooms.Add(r);
            Rooms = new ObservableCollection<IRoom>(Rooms);
            NotifyPropertyChange("Rooms");
        }

        void UnitsListViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public int SelectedUnit = 0;
        public UnitType SelectedUnitType;

        public void Refresh(IUnit unit)
        {
            SelectedUnit = unit.Identity;
            SelectedUnitType = unit.UnitType;
            Rooms = null;
            UnitPreview = null;
            PreviewUnits = null;
            LoadService.GetUnitPreviewAsync(unit.Identity);

            if (Units != null)
            {
                Units.Clear();
            }

            Units = new ObservableCollection<UnitDTO>(units_manager.GetFlatChilds(SelectedUnit));
        }

        public void UploadPreview()
        {
            LoadService.GetPreviewsAsync();
        }

        public void RemoveRoom(IRoom room)
        {
            //Rooms.Remove(room);
            //Rooms = new ObservableCollection<IRoom>(Rooms);
            //NotifyPropertyChange("Rooms");
        }

        public void RedrawRoom(IRoom room)
        {
            var r = new ObservableCollection<IRoom>(Rooms);
            Rooms = r;
            NotifyPropertyChange("Rooms");
        }

        public event EventHandler LoadingCompleted = delegate { };

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        internal void SizeLocationChanged(IRoom room)
        {
            EventAggregator.GetEvent<SizeLocationChangedEvent>().Publish(room);
        }

        public string Title
        {
            get { return ""; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }
}
