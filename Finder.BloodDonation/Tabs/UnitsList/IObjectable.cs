﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Tabs.UnitsList
{
    public interface IObjectable
    {
        Point TL { get; }
        Point BR { get; }
        Point MinSize { get; set; }
        Point MaxSize { get; set; }
        Point Size { get; }
        HitPoint HitPoint { get; set; }
        Object MonitorableObject { get; set; }
        string Style { get; set; }
        string TempName { get; set; }
        int Order { get; set; }
    }
}
