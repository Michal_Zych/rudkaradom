﻿using Finder.BloodDonation.Controls;
using Finder.BloodDonation.DBBuffers;
using Finder.BloodDonation.Model.Dto.Enums;
using FinderFX.SL.Core.MVVM;
using System;
using System.Net;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Tabs.UnitsList
{
    //TODO: 
    [DataContract]
    public abstract class Object
    {
        public const string IMAGE_EXTENSION = ".png";

        [DataMember]
        public virtual int Id { get; set; }

        [DataMember]
        public virtual string Name { get; set; }

        [DataMember]
        public virtual string Location { get; set; }

        [DataMember]
        public virtual double TLX { get; set; }

        [DataMember]
        public virtual double TLY { get; set; }

        [DataMember]
        public virtual double BRX { get; set; }

        [DataMember]
        public virtual double BRY { get; set; }

        [DataMember]
        public virtual ObjectType ObjectType { get; set; }

        [DataMember]
        public virtual int? CurrentUnitId { get; set; }

        [DataMember]
        public virtual int? PreviousUnitId { get; set; }

        [DataMember]
        public virtual BitmapMode DisplayColor { get; set; }

        [DataMember]
        public virtual int AssignUnitId { get; set; }

        private bool? isHitTransparency;
        [DataMember]
        public virtual bool? IsHitTransparency
        {
            get { return isHitTransparency; }
            set
            {
                isHitTransparency = value;

                EventHandler handler = IsHitTransparencyChanged;
                if (handler != null)
                    handler(value, new EventArgs());
            }
        }

        [DataMember]
        public virtual double PositionInViewportX { get; set; }

        [DataMember]
        public virtual double PositionInViewportY { get; set; }

        [DataMember]
        public virtual double Width { get; set; }

        [DataMember]
        public virtual double Height { get; set; }

        [DataMember]
        public virtual string ImageName { get { return this.GetType().Name + IMAGE_EXTENSION; } }

        public event EventHandler IsHitTransparencyChanged;

        [DataMember]
        public virtual AlarmType AlarmType { get; set; }

        [DataMember]
        public virtual DateTime AlarmDate { get; set; }

        [DataMember]
        public virtual string AlarmInfo { get; set; }

        [DataMember]
        public virtual string DisplayId { get; set; }

        [DataMember]
        public virtual string ParentUnitName { get; set; }

        [DataMember]
        public virtual string DisplayAssigmentName { get; set; }

        [DataMember]
        public virtual EventTypes EventTypes { get; set; }
    }

    [DataContract]
    public class Band : Object
    {
        private string barCode;

        public override string DisplayId
        {
            get
            {
                return base.DisplayId;
            }
            set
            {
                base.DisplayId = "Kod Kreskowy: " + value;
            }
        }

        public override string AlarmInfo
        {
            get
            {
                return base.AlarmInfo;
            }
            set
            {
                double minutes = DateTime.Now.Subtract(AlarmDate).TotalMinutes;

                switch(AlarmType)
                {
                    case UnitsList.AlarmType.SOS:
                        base.AlarmInfo = "Alarm SOS " + AlarmDate;
                        break;
                        
                    case UnitsList.AlarmType.NoMoving:
                        base.AlarmInfo = "Lokalizator się nie rusza od " + Math.Round(minutes, 0) + " min";
                        break;
                        
                    case UnitsList.AlarmType.NoGoZoneIn:
                        base.AlarmInfo = "Lokalizator w strefie zakazanej od " + Math.Round(minutes, 0) + " min";
                        break;

                    default:
                        base.AlarmInfo = String.Empty;
                        break;
                }
            }
        }

        public override string DisplayAssigmentName
        {
            get
            {
                return base.DisplayAssigmentName;
            }
            set
            {
                base.DisplayAssigmentName = "Przypisana do " + value;
            }
        }
    }

    [DataContract]
    public class Patient : Object
    {
        private string pesel;

        public override string DisplayId
        {
            get
            {
                return base.DisplayId;
            }
            set
            {
                base.DisplayId = "Pesel: " + value;
            }
        }

        public string DisplayLastUnitLocation { get; set; }

        public override string DisplayAssigmentName
        {
            get
            {
                return base.DisplayAssigmentName;
            }
            set
            {
                base.DisplayAssigmentName = "Przydzielony(na) do " + value;
            }
        }

        public override string AlarmInfo
        {
            get
            {
                return base.AlarmInfo;
            }
            set
            {
                double minutes = DateTime.Now.Subtract(AlarmDate).TotalMinutes;

                switch (AlarmType)
                {
                    case UnitsList.AlarmType.SOS:
                        base.AlarmInfo = "Alarm SOS " + AlarmDate;
                        break;

                    case UnitsList.AlarmType.NoMoving:
                        base.AlarmInfo = "Pacjent się nie rusza od " + Math.Round(minutes, 0) + " min";
                        break;

                    case UnitsList.AlarmType.NoGoZoneIn:
                        base.AlarmInfo = "Pacjent w strefie zakazanej od " + Math.Round(minutes, 0) + " min";
                        break;

                    default:
                        base.AlarmInfo = String.Empty;
                        break;
                }
            }
        }
    }
}
