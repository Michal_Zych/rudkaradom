﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Tabs.UnitsList
{
    public class OrderHelper
    {
        public static int GetOrder(string style)
        {
            if (style.StartsWith("room_"))
            {
                return 0;
            }

            if (style.StartsWith("device_"))
            {
                return 1;
            }

            return int.MaxValue;
        }
    }
}
