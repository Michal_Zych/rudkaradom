﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.UnitsList
{
    public enum AlarmType
    {
      Delete = 0
      ,Create = 1
      ,Update = 2
      ,RegisterInHostpital = 3
      ,RegisterOutHostpital = 4
      ,RegisterInDepartment = 5
      ,RegisterOutDepartment = 6
      ,RegisterInRoom = 7
      ,RegisterOutRoom = 8
      ,BandAssignment = 9
      ,BatteryOk = 10
      ,Connected = 11
      ,ZoneIn = 12
      ,NoGoZoneOut = 13
      ,UserMessage = 14
      ,LowBattery = 15
      ,Moving = 16
      ,NoMoving = 17
      ,ZoneOut = 18
      ,ACC = 19
      ,Disconnected = 20
      ,NoGoZoneIn = 21
      ,SOS = 22
    }
}