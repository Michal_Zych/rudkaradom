﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DBBuffers;
using Finder.BloodDonation.Controls;
using Finder.BloodDonation.Model.Dto.Enums;
using Finder.BloodDonation.DynamicData.Memory;
using Finder.BloodDonation.DataManagers;
using System.Collections;
using Finder.BloodDonation.Layout;
using System.Windows.Threading;
using System.Text.RegularExpressions;
using Finder.BloodDonation.Model.Dto;
using System.Text;
using Telerik.Windows.Controls;

namespace Finder.BloodDonation.Tabs.UnitsList
{
    public class DetailsListBuilder
    {
        #region const statments
        private const string FILTERED_PREFIX = "Filtered_";
        private const string ALARMS_PREFIX = "Alarming_";
        private const string MONITORABLEOBJECT_PREFIX = "MonitorableObject_";
        private const string ROOMS_PREFIX = "Rooms_";

        private const string BANDS_TYPE_PREFIX = "_Band";
        private const string PATIENT_TYPE_PREFIX = "_Patient";

        #endregion

        #region properties
        public bool[] IsRoomsExpand { get; set; }
        public bool IsAlarmsExpand { get; set; }
        public bool IsFilteredExpand { get; set; }

        public Grid BaseGrid { get; set; }
        public IObjectable SelectedMonitorableObject
        {
            get
            {
                return selectedMonitorableObject;
            }
            set
            {
                selectedMonitorableObject = value;
                RoomsViewModel.SelectedObject = selectedMonitorableObject;
                FixedUpdate();
                
                EventHandler handler = SelectedMonitorableObjectChanged;
                if (handler != null)
                    handler(value, new EventArgs());
            }
        }

        public IObjectable SelectedMonitorableObjectSetterWithoutEvent
        {
            set
            {
                selectedMonitorableObject = value;
                FixedUpdate(true);
            }
        }

        private IObjectable selectedMonitorableObject;
        #endregion

        #region Collections
        private List<IObjectable> monitorableObjects;
        private List<IObjectable> filteredResults;
        #endregion

        #region Others Controls
        private int Row { get; set; }

        private Canvas viewport;
        private List<IRoom> rooms;

        private Grid DetailsGrid;
        private Border filteredField = null;
        private Grid baseGrid = null;
        #endregion

        public event EventHandler SelectedMonitorableObjectChanged;

        #region Initialization helphers method
        private void InitializeBaseGrid()
        {
            baseGrid = new Grid();
            baseGrid.Width = viewport.Width - 40d;
            baseGrid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
            baseGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            baseGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            viewport.Children.Add(baseGrid);
            Grid.SetColumn(baseGrid, 0);
        }

        public DetailsListBuilder(List<IRoom> roomList, List<IObjectable> monitorableObjects, Canvas viewport, Grid baseGrid, string localizationSeparator)
        {
            try
            {
                this.rooms = roomList.OrderBy(e=>e.Unit.Name).ToList();
                this.monitorableObjects = monitorableObjects;
                this.viewport = viewport;

                this.BaseGrid = baseGrid;

                if (IsRoomsExpand == null || IsRoomsExpand.Length < this.rooms.Count)
                {
                    this.IsRoomsExpand = new bool[rooms.Count];
                    for (int i = 0; i < rooms.Count; i++)
                    {
                        IsRoomsExpand[i] = false;
                    }
                }

                this.LocationSeparator = localizationSeparator;
                this.LocationSeparatorChar = localizationSeparator.FirstOrDefault();
                InitializeBaseGrid();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// This method removed 'BaseGrid' item from viewport if its is exist.
        /// </summary>
        private void ClearBaseGrid()
        {
            if (viewport.Children.Count >= 1)
                if (((Grid)viewport.Children[0]).Children.Count > 1)
                    ((Grid)viewport.Children[0]).Children.RemoveAt(1);
        }

        /// <summary>
        /// This method created filtered field if its not created.
        /// </summary>
        private void CreateFilteredFiledIfNotCreate()
        {
            try
            {
                if (filteredField == null)
                {
                    filteredField = CreateFilteredField();
                    baseGrid.Children.Add(filteredField);
                    Grid.SetRow(filteredField, 0);
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// This method adding finding monitorable object items to filtered category.
        /// </summary>
        private void InitializeFilteredCategory()
        {
            try
            {
                if (filteredResults != null && filteredResults.Count > 0)
                {
                    Border filteredCategory = DrawFilteredCategory();

                    AddFilteredCategoryItemToDetailsList(filteredCategory);
                    AddFilteredResultsItemsToDetailsList();
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// This method added filtered results items to details list.
        /// </summary>
        private void AddFilteredResultsItemsToDetailsList()
        {
            try
            {
                for (int i = 0; i < filteredResults.Count; i++)
                {
                    Border b = CreateFilteredMonitorableObjectItem(i, filteredResults[i].MonitorableObject.Id.ToString());
                    DetailsGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
                    DetailsGrid.Children.Add(b);
                    Grid.SetRow(b, Row);
                    Row++;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// This method added filtered category item to details list.
        /// </summary>
        /// <param name="filteredCategory"></param>
        private void AddFilteredCategoryItemToDetailsList(Border filteredCategory)
        {
            try
            {
                if (filteredCategory.Child != null)
                {
                    DetailsGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
                    DetailsGrid.Children.Add(filteredCategory);
                    Grid.SetRow(filteredCategory, Row);
                    Row++;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void InitializeAlarmsCategory(Border alarmsCategory)
        {
            try
            {
                if (alarmsCategory != null)
                {
                    AddAlarmsCategoryToBaseGrid(alarmsCategory);
                    var t = GetAlarmsMonitorableObjects();
                    AddAlarmItemsToDetailsGrid(t);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void AddAlarmsCategoryToBaseGrid(Border alarmsCategory)
        {
            try
            {
                IsAlarmsExpand = true;
                DetailsGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
                DetailsGrid.Children.Add(alarmsCategory);
                Grid.SetRow(alarmsCategory, Row);
                Row++;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// This method selected monitorable objects with alarms.
        /// </summary>
        /// <returns>Monitorable objects with alarms ordered by date and type.</returns>
        private IEnumerable<IObjectable> GetAlarmsMonitorableObjects()
        {
            try
            {
                return this.monitorableObjects
                        .Where(e => e.MonitorableObject.DisplayColor == BitmapMode.Red &&
                            (e.MonitorableObject.EventTypes == EventTypes.Alarm || e.MonitorableObject.EventTypes == EventTypes.Warning))
                            .OrderBy(e => e.MonitorableObject.AlarmDate)
                            .OrderByDescending(e => e.MonitorableObject.AlarmType).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// This method added monitorables objects with alarm to details grid.
        /// </summary>
        /// <param name="alarmsMonitorableObject"></param>
        private void AddAlarmItemsToDetailsGrid(IEnumerable<IObjectable> alarmsMonitorableObject)
        {
            try
            {
                if (alarmsMonitorableObject == null)
                    return;

                for (int i = 0; i < alarmsMonitorableObject.Count(); i++)
                {
                    Border alarmBorder = CreateAlarmsMonitorableObjectItem(alarmsMonitorableObject.ElementAt(i));
                    DetailsGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
                    DetailsGrid.Children.Add(alarmBorder);
                    Grid.SetRow(alarmBorder, Row);
                    Row++;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void InitializeRoomsCategory()
        {
            try
            {
                for (int i = 0; i < this.rooms.Count; i++)
                {
                    IEnumerable<IObjectable> monitorableObjectsForRoom = GetMonitorablesObjectsForRoom(this.rooms[i].Unit.Id);
                    var query = monitorableObjectsForRoom.GroupBy(x => x.TempName).Select(y => y.FirstOrDefault());
                    monitorableObjectsForRoom = query;

                    ChangeRoomExpandStatusIfItContainsSelectedMonitorableObject(this.rooms[i].Unit.Id, i);
                    AddRoomItemToDetailsGrid(i);

                    for (int j = 0; j < monitorableObjectsForRoom.Count(); j++)
                    {
                        Border mo = CreateMonitorableObjectItem(j, monitorableObjectsForRoom.ElementAt(j).MonitorableObject.Id.ToString(), monitorableObjectsForRoom.ElementAt(j).MonitorableObject.ObjectType.ToString(), false);
                        SetMonitorableObjectVisibilityForExpandStatus(mo, i);

                        AddMonitorableObjectItemToDetailsGrid(mo);
                    }

                    IEnumerable<IObjectable> monitorableObjectsForRoomForPrevious = GetMonitorablesWithPreviosLocation(this.rooms[i].Unit.Id);
                    var c = monitorableObjectsForRoomForPrevious.GroupBy(x => x.TempName).Select(y => y.FirstOrDefault());
                    monitorableObjectsForRoomForPrevious = c;

                    for(int j = 0; j < monitorableObjectsForRoomForPrevious.Count(); j++)
                    {
                        Border mo = CreateMonitorableObjectItem(j, monitorableObjectsForRoomForPrevious.ElementAt(j).MonitorableObject.Id.ToString(), monitorableObjectsForRoomForPrevious.ElementAt(j).MonitorableObject.ObjectType.ToString(), false);
                        SetMonitorableObjectVisibilityForExpandStatus(mo, i);

                        AddMonitorableObjectItemToDetailsGrid(mo);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void AddMonitorableObjectItemToDetailsGrid(Border monitorableObject)
        {
            try
            {
                DetailsGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
                DetailsGrid.Children.Add(monitorableObject);
                Grid.SetRow(monitorableObject, Row);
                Row++;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void SetMonitorableObjectVisibilityForExpandStatus(Border monitorableObject, int positionInRoomsArray)
        {
            try
            {
                if (IsRoomsExpand[positionInRoomsArray])
                    monitorableObject.Visibility = Visibility.Visible;
                else
                    monitorableObject.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// This method added room item to DetailsGrid.
        /// </summary>
        /// <param name="positionInRoomsArray">Room position ( Index ) in rooms array.</param>
        private void AddRoomItemToDetailsGrid(int positionInRoomsArray)
        {
            try
            {
                Border r = CreateRoomsItem(positionInRoomsArray);
                DetailsGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
                DetailsGrid.Children.Add(r);
                Grid.SetRow(r, Row);
                Row++;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// Change room expand status if it contains selected monitorable object.
        /// </summary>
        /// <param name="roomId">Room identificator.</param>
        /// <param name="roomIndexInExpandArray">Room position in rooms or romms expand array.</param>
        private void ChangeRoomExpandStatusIfItContainsSelectedMonitorableObject(int roomId, int roomIndexInExpandArray)
        {
            try
            {
                if (SelectedMonitorableObject != null && (SelectedMonitorableObject.MonitorableObject.CurrentUnitId == roomId ||
                    (SelectedMonitorableObject.MonitorableObject.CurrentUnitId == null &&
                    SelectedMonitorableObject.MonitorableObject.PreviousUnitId == roomId)))
                    IsRoomsExpand[roomIndexInExpandArray] = false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        /// <summary>
        /// This method is responsible for selected monitorables objects for room.
        /// </summary>
        /// <param name="roomId">Room identificator.</param>
        /// <returns>Monitorables Objects collection for room.</returns>
        private IEnumerable<IObjectable> GetMonitorablesObjectsForRoom(int roomId)
        {
            return this.monitorableObjects.Where(e => e.MonitorableObject.CurrentUnitId == roomId).ToList();
        }

        private IEnumerable<IObjectable> GetMonitorablesWithPreviosLocation(int roomId)
        {
            return this.monitorableObjects.Where(x => x.MonitorableObject.CurrentUnitId == null && x.MonitorableObject.PreviousUnitId == roomId).ToList();
        }
        #endregion

        private List<IObjectable> RemoveDuplicatsFromMonitorableObjectsList(List<IObjectable> monitorableObjects)
        {
            string nameToCheck = String.Empty;

            for(int i = 0; i < monitorableObjects.Count; i++)
            {
                nameToCheck = monitorableObjects[i].TempName;

                for(int j = 0; j < monitorableObjects.Count; j++)
                {
                    if (monitorableObjects[j].TempName == nameToCheck && i != j)
                        monitorableObjects.RemoveAt(j);
                }
            }

            return monitorableObjects;
        }

        #region Updated Method
        public void Update(List<IRoom> roomList = null, List<IObjectable> monitorableObjects = null, bool redraw = true)
        {
            try
            {
                if (this.monitorableObjects == null)
                    return;

                //monitorableObjects = RemoveDuplicatsFromMonitorableObjectsList(this.monitorableObjects);

                if (this.rooms == null)
                    return;

                if (DetailsGrid != null)
                {
                    DetailsGrid.Width = viewport.Width - 40d;
                }

                if (redraw)
                {
                    Row = 0;

                    ClearBaseGrid();

                    ChangeData(roomList, monitorableObjects);
                    Random rld = new Random();
                    DetailsGrid = new Grid();
                    DetailsGrid.Width = viewport.Width - 40d;

                    CreateFilteredFiledIfNotCreate();
                    InitializeFilteredCategory();

                    Border alarmsCategory = DrawSpecjalAlarmCategory();
                    InitializeAlarmsCategory(alarmsCategory);

                    InitializeRoomsCategory();

                    if (baseGrid != null && baseGrid.Children != null && baseGrid.Children.Count > 0)
                    {
                        for(int i = 0; i < baseGrid.Children.Count; i++)
                        {
                            var r = baseGrid.Children[i];
                        }
                    }

                    var it = DetailsGrid.Children;
                    baseGrid.Children.Add(DetailsGrid);
                    Grid.SetRow(DetailsGrid, 1);

                    viewport.Height = CalcViewPortHeight();
                }
            }
            catch(Exception ex) 
            {
                throw new Exception(ex.ToString());
            }
        }

        // Oblicza wysokość canvasa znajdującego się w scrollview.
        private double CalcViewPortHeight()
        {
            try
            {
                const double FILTERED_FIELD_HEIGHT = 25d;
                const double CATEGORY_HEIGHT = 52d;
                const double ITEM_HEIGHT = 66d;
                const double BORDER_HEIGHT = 2d;

                double vh = FILTERED_FIELD_HEIGHT;

                for (int i = 0; i < DetailsGrid.Children.Count; i++)
                {
                    Border b = (Border)DetailsGrid.Children[i];

                    if (b.Name.Contains(ROOMS_PREFIX) || b.Name.Contains(FILTERED_PREFIX + 0) || b.Name.Contains(ALARMS_PREFIX + 0))
                        vh += DetailsGrid.Children[i].Visibility.Equals(Visibility.Visible) ? CATEGORY_HEIGHT + BORDER_HEIGHT : 0d;
                    else
                        vh += DetailsGrid.Children[i].Visibility.Equals(Visibility.Visible) ? ITEM_HEIGHT + BORDER_HEIGHT : 0d;
                }

                return vh;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        Brush lastBackground = null;

        public void FixedUpdate(bool fromSetterWithoutEvent = false)
        {
            try
            {
                const int FINDERIMAGE_POSITION_IN_GRID = 2;

                for (int i = 0; i < DetailsGrid.Children.Count; i++)
                {
                    Border bs = (Border)DetailsGrid.Children[i];
                    Grid g = (Grid)bs.Child;

                    g.Background = new SolidColorBrush(Color.FromArgb(50, 199, 199, 199));

                }

                foreach (Border b in DetailsGrid.Children)
                {
                    if (b != null && b.Name.Contains(ROOMS_PREFIX))
                    {
                        if (rooms.Count > int.Parse(b.Name.Replace(ROOMS_PREFIX, String.Empty)))
                        {
                            int roomId = rooms[int.Parse(b.Name.Replace(ROOMS_PREFIX, String.Empty))].Unit.Id;

                            IObjectable[] mo = monitorableObjects.Where(e => e.MonitorableObject.CurrentUnitId == roomId || (e.MonitorableObject.CurrentUnitId == null && e.MonitorableObject.PreviousUnitId == roomId)).ToArray();

                            for (int i = 0; i < DetailsGrid.Children.Count; i++)
                            {
                                if (SelectedMonitorableObject != null &&
                                    (SelectedMonitorableObject.MonitorableObject.CurrentUnitId == rooms.FirstOrDefault(e => e.Unit.Id == roomId).Unit.Id ||
                                    (SelectedMonitorableObject.MonitorableObject.CurrentUnitId == null &&
                                    SelectedMonitorableObject.MonitorableObject.PreviousUnitId == rooms.FirstOrDefault(x=>x.Unit.Id == roomId).Unit.Id)))
                                {
                                    IsRoomsExpand[rooms.IndexOf(rooms.FirstOrDefault(e => e.Unit.Id == roomId))] = true;
                                    Border bs = (Border)DetailsGrid.Children[i];
                                    Grid g = (Grid)bs.Child;

                                    if (bs.Name.Contains(MONITORABLEOBJECT_PREFIX + SelectedMonitorableObject.MonitorableObject.Id + "_" + SelectedMonitorableObject.MonitorableObject.ObjectType))
                                    {
                                        if (fromSetterWithoutEvent)
                                            g.Background = new SolidColorBrush(Color.FromArgb(50, 133, 133, 133));
                                    }
                                }

                                for (int j = 0; j < mo.Count(); j++)
                                {
                                    if (((Border)DetailsGrid.Children[i]).Name.Contains(MONITORABLEOBJECT_PREFIX + mo[j].MonitorableObject.Id + "_" + mo[j].MonitorableObject.ObjectType) && !((Border)DetailsGrid.Children[i]).Name.Contains(ALARMS_PREFIX))      //ALARMS_PREFIX
                                    {
                                        int roomIndex = rooms.IndexOf(rooms.FirstOrDefault(e => e.Unit.Id == roomId));

                                        if (IsRoomsExpand[roomIndex])
                                        {
                                            ((Border)DetailsGrid.Children[i]).Visibility = Visibility.Visible;
                                            ((FinderImage)((Grid)b.Child).Children[FINDERIMAGE_POSITION_IN_GRID]).BaseImageNameInResources = "expand.png";
                                        }
                                        else
                                        {
                                            ((Border)DetailsGrid.Children[i]).Visibility = Visibility.Collapsed;
                                            ((FinderImage)((Grid)b.Child).Children[FINDERIMAGE_POSITION_IN_GRID]).BaseImageNameInResources = "fold.png";
                                        }
                                    }
                                }
                            }
                        }

                    }
                }

                viewport.Height = CalcViewPortHeight();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void ChangeMonitorableObjectExpandStateForRoom(int roomId)
        {
            try
            {
                var mo = monitorableObjects.Where(e => e.MonitorableObject.CurrentUnitId == roomId ||
                    (e.MonitorableObject.CurrentUnitId == null && e.MonitorableObject.PreviousUnitId == roomId)).ToArray();

                for (int i = 0; i < mo.Count(); i++)
                {
                    if (IsRoomsExpand[rooms.IndexOf(rooms.FirstOrDefault(x => x.Unit.Id == roomId))])
                    {
                        Border b = DetailsGrid.Children.OfType<Border>().FirstOrDefault(x => x.Name.Contains(MONITORABLEOBJECT_PREFIX + mo[i].MonitorableObject.Id + "_" + mo[i].MonitorableObject.ObjectType));
                        b.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        Border b = DetailsGrid.Children.OfType<Border>().FirstOrDefault(x => x.Name.Contains(MONITORABLEOBJECT_PREFIX + mo[i].MonitorableObject.Id + "_" + mo[i].MonitorableObject.ObjectType));
                        b.Visibility = Visibility.Collapsed;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        #endregion

        #region Creaated Controls Helphers Method
        private Border CreateFilteredField()
        {
            try
            {
                var text = this.filteredField != null ? ((TextBox)this.filteredField.Child).Text : String.Empty;
                Border b = new Border();
                TextBox filteredField = new TextBox();
                filteredField.Text = text;
                filteredField.TextChanged += filteredField_TextChanged;
                b.Child = filteredField;
                return b;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private Border CreateAlarmsMonitorableObjectItem(IObjectable monitorableObject)
        {
            try
            {
                Grid control = new Grid();
                Border border = new Border();
                border.BorderBrush = new SolidColorBrush(Color.FromArgb(50, 200, 150, 100));
                border.BorderThickness = new Thickness(2);
                border.Name = ALARMS_PREFIX + MONITORABLEOBJECT_PREFIX + monitorableObject.MonitorableObject.Id + "_" + monitorableObject.MonitorableObject.ObjectType;
                border.Width = viewport.Width - 40d; ;

                control.ColumnDefinitions.Clear();

                control.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(32d) });
                control.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(32d) });

                control.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
                control.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
                control.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });

                FinderImage objectImage = new FinderImage();
                objectImage.Width = 50d;
                objectImage.Height = 35d;
                objectImage.DisplayBaseImageColor = monitorableObject.MonitorableObject.DisplayColor;
                objectImage.IsBaseImageFromResources = true;
                objectImage.BaseImageFolderInResources = ImagesFolder.Objects;
                objectImage.Margin = new Thickness(0, 0, 5, 0);

                switch (monitorableObject.MonitorableObject.ObjectType)
                {
                    case ObjectType.Band:
                        objectImage.BaseImageNameInResources = ((Band)monitorableObject.MonitorableObject).ImageName;
                        break;
                    case ObjectType.Patient:
                        objectImage.BaseImageNameInResources = ((Patient)monitorableObject.MonitorableObject).ImageName;
                        break;
                }

                switch (monitorableObject.MonitorableObject.DisplayColor)
                {
                    case BitmapMode.Green:
                        control.Background = CheckIfMonitorableObjectIsSelectedAndGetColor((Color.FromArgb(50, 222, 222, 222)), monitorableObject.MonitorableObject.Id.ToString());
                        break;

                    case BitmapMode.Red:
                        control.Background = CheckIfMonitorableObjectIsSelectedAndGetColor(Color.FromArgb(50, 222, 222, 222), monitorableObject.MonitorableObject.Id.ToString());
                        break;

                    case BitmapMode.Gray:
                        control.Background = CheckIfMonitorableObjectIsSelectedAndGetColor(Color.FromArgb(50, 222, 222, 222), monitorableObject.MonitorableObject.Id.ToString());
                        break;
                }

                control.Children.Add(objectImage);
                Grid.SetColumn(objectImage, 0);
                Grid.SetRow(objectImage, 0);
                Grid.SetRowSpan(objectImage, 2);

                Telerik.Windows.Controls.Label label = new Telerik.Windows.Controls.Label();
                label.Content = monitorableObject.MonitorableObject.Name;
                control.Children.Add(label);
                Grid.SetColumn(label, 1);
                border.Child = control;

                Telerik.Windows.Controls.Label location
                    = new Telerik.Windows.Controls.Label();

                ZonePreviewDto zonePreview = null;

                for (int i = 0; i < rooms.Count; i++)
                {
                    for (int j = 0; j < monitorableObjects.Count; j++)
                    {
                        if (monitorableObjects[j].MonitorableObject.Id == monitorableObject.MonitorableObject.Id && monitorableObjects[j].MonitorableObject.ObjectType == monitorableObject.MonitorableObject.ObjectType)
                        {
                            if (rooms[i].Unit.Id == monitorableObjects[j].MonitorableObject.CurrentUnitId ||
                                (rooms[i].Unit.Id == monitorableObjects[j].MonitorableObject.PreviousUnitId &&
                                monitorableObjects[j].MonitorableObject.CurrentUnitId == null))
                            {
                                zonePreview = rooms[i].Unit;
                            }
                        }
                    }
                }

                if (zonePreview != null)
                {
                    location.Content =
                        "Aktualna lokalizacja: " + ReverseLocationPath(zonePreview.Location + LocationSeparator + zonePreview.Name, LocationSeparatorChar);

                    control.Children.Add(location);
                    Grid.SetColumn(location, 1);
                    Grid.SetColumnSpan(location, 4);
                    Grid.SetRow(location, 1);
                }

                border.MouseLeftButtonDown += border_MouseLeftButtonDown;
                border.MouseLeftButtonUp += monitorableObjectBorder_MouseLeftButtonUp;

                border.MouseEnter += monitorableObject_MouseEnter;
                border.MouseLeave += monitorableObject_MouseLeave;

                return border;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private Grid SetBackgroundsColorsToMonitorableObject(Grid control, Object monitorableObject, string name)
        {
            try
            {
                switch (monitorableObject.DisplayColor)
                {
                    case BitmapMode.Green:
                        control.Background = CheckIfMonitorableObjectIsSelectedAndGetColor((Color.FromArgb(50, 222, 222, 222)), name);
                        break;

                    case BitmapMode.Red:
                        control.Background = CheckIfMonitorableObjectIsSelectedAndGetColor(Color.FromArgb(50, 222, 222, 222), name);
                        break;

                    case BitmapMode.Gray:
                        control.Background = CheckIfMonitorableObjectIsSelectedAndGetColor(Color.FromArgb(50, 222, 222, 222), name);
                        break;
                }

                return control;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private Border CreateMonitorableObjectItem(int positionInMonitorables, string name, string type, bool isDisconected)
        {
            try
            {
                Grid control = new Grid();
                Border border = new Border();
                border.BorderBrush = new SolidColorBrush(Color.FromArgb(50, 200, 150, 100));
                border.BorderThickness = new Thickness(2);
                border.Name = MONITORABLEOBJECT_PREFIX + name + "_" + type;
                border.Width = viewport.Width - 40d;

                control.ColumnDefinitions.Clear();

                control.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(32d) });
                control.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(32d) });

                control.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
                control.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
                control.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });

                FinderImage objectImage = new FinderImage();
                objectImage.Width = 50d;
                objectImage.Height = 35d;
                objectImage.DisplayBaseImageColor = monitorableObjects.FirstOrDefault(e => e.MonitorableObject.Id == int.Parse(name)).MonitorableObject.DisplayColor;
                objectImage.IsBaseImageFromResources = true;
                objectImage.BaseImageFolderInResources = ImagesFolder.Objects;

                objectImage.Margin = new Thickness(0, 0, 5, 0);

                var mo = monitorableObjects.FirstOrDefault(e => e.MonitorableObject.Id == int.Parse(name)).MonitorableObject;

                switch (mo.ObjectType)
                {
                    case ObjectType.Band:
                        objectImage.BaseImageNameInResources = ((Band)mo).ImageName;
                        break;

                    case ObjectType.Patient:
                        objectImage.BaseImageNameInResources = ((Patient)mo).ImageName;
                        break;
                }

                control = SetBackgroundsColorsToMonitorableObject(control, monitorableObjects.FirstOrDefault(e => e.MonitorableObject.Id == int.Parse(name)).MonitorableObject, name);

                control.Children.Add(objectImage);
                Grid.SetColumn(objectImage, 0);
                Grid.SetRow(objectImage, 0);
                Grid.SetRowSpan(objectImage, 2);


                Telerik.Windows.Controls.Label label = new Telerik.Windows.Controls.Label();

                label.Content = monitorableObjects.FirstOrDefault(e => e.MonitorableObject.Id == int.Parse(name)).MonitorableObject.Name;
                control.Children.Add(label);
                Grid.SetColumn(label, 1);

                Telerik.Windows.Controls.Label location
                    = new Telerik.Windows.Controls.Label();
                ZonePreviewDto preview = null;

                for (int i = 0; i < rooms.Count; i++)
                {
                    for(int j = 0; j < monitorableObjects.Count; j++)
                    {
                        if(monitorableObjects[j].MonitorableObject.Id == int.Parse(name))
                        {
                            if(monitorableObjects[j].MonitorableObject.CurrentUnitId != null || 
                                (monitorableObjects[j].MonitorableObject.CurrentUnitId == null && monitorableObjects[j].MonitorableObject.PreviousUnitId != null))
                            {
                                if (monitorableObjects[j].MonitorableObject.CurrentUnitId != null)
                                {
                                    if (monitorableObjects[j].MonitorableObject.CurrentUnitId == rooms[i].Unit.Id)
                                    {
                                        preview = rooms[i].Unit;
                                    }
                                }
                                else
                                {
                                    if (monitorableObjects[j].MonitorableObject.PreviousUnitId == rooms[i].Unit.Id)
                                    {
                                        preview = rooms[i].Unit;
                                    }
                                }
                            }
                        }
                    }
                }

                location.Content =
                    "Aktualna lokalizacja: " + ReverseLocationPath(preview.Location + LocationSeparator + preview.Name, LocationSeparatorChar);

                control.Children.Add(location);
                Grid.SetColumn(location, 1);
                Grid.SetColumnSpan(location, 4);
                Grid.SetRow(location, 1);

                border.Child = control;

                border.MouseLeftButtonDown += border_MouseLeftButtonDown;
                border.MouseLeftButtonUp += monitorableObjectBorder_MouseLeftButtonUp;

                border.MouseEnter += monitorableObject_MouseEnter;
                border.MouseLeave += monitorableObject_MouseLeave;

                return border;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private Border CreateFilteredMonitorableObjectItem(int positionInFilteredResultsList, string name)
        {
            try
            { 
            if (filteredResults == null)
                return new Border();
            if (filteredResults.Count <= 0)
                return new Border();

            Grid control = new Grid();
            Border border = new Border();
            border.BorderBrush = new SolidColorBrush(Color.FromArgb(50, 200, 150, 100));
            border.BorderThickness = new Thickness(2);
            border.Name = FILTERED_PREFIX + name;
            border.Width = viewport.Width - 40d;//= Double.NaN;

            control.ColumnDefinitions.Clear();
            control.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
            control.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
            control.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });

            FinderImage objectImage = new FinderImage();
            objectImage.Width = 50d;
            objectImage.Height = 35d;
            objectImage.DisplayBaseImageColor = filteredResults[positionInFilteredResultsList].MonitorableObject.DisplayColor;
            objectImage.IsBaseImageFromResources = true;
            objectImage.BaseImageFolderInResources = ImagesFolder.Objects;
            objectImage.Margin = new Thickness(0, 0, 5, 0);

            switch (filteredResults[positionInFilteredResultsList].MonitorableObject.ObjectType)
            {
                case ObjectType.Band:
                    objectImage.BaseImageNameInResources = ((Band)filteredResults[positionInFilteredResultsList].MonitorableObject).ImageName;
                    break;
                case ObjectType.Patient:
                    objectImage.BaseImageNameInResources = ((Patient)filteredResults[positionInFilteredResultsList].MonitorableObject).ImageName;
                    break;
            }

            switch (filteredResults[positionInFilteredResultsList].MonitorableObject.DisplayColor)
            {
                case BitmapMode.Green:
                    control.Background = CheckIfMonitorableObjectIsSelectedAndGetColor((Color.FromArgb(50, 222, 222, 222)), name);
                    break;

                case BitmapMode.Red:
                    control.Background = CheckIfMonitorableObjectIsSelectedAndGetColor(Color.FromArgb(50, 222, 222, 222), name);
                    break;

                case BitmapMode.Gray:
                    control.Background = CheckIfMonitorableObjectIsSelectedAndGetColor(Color.FromArgb(50, 222, 222, 222), name);
                    break;
            }

            control.Children.Add(objectImage);
            Grid.SetColumn(objectImage, 0);
            Grid.SetRow(objectImage, 0);
            Grid.SetRowSpan(objectImage, 2);

            Telerik.Windows.Controls.Label label = new Telerik.Windows.Controls.Label();
            label.Content = filteredResults[positionInFilteredResultsList].MonitorableObject.Name;
            control.Children.Add(label);
            Grid.SetColumn(label, 1);

            Telerik.Windows.Controls.Label location
                 = new Telerik.Windows.Controls.Label();
            ZonePreviewDto preview
                = rooms
                .FirstOrDefault(x => x.Unit.Id == monitorableObjects
                    .FirstOrDefault(y => y.MonitorableObject.Id == int.Parse(name))
                    .MonitorableObject.CurrentUnitId)
                    .Unit;

            location.Content =
                "Aktualna lokalizacja: " + ReverseLocationPath(preview.Location + LocationSeparator + preview.Name, LocationSeparatorChar);

            control.Children.Add(location);
            Grid.SetColumn(location, 1);
            Grid.SetColumnSpan(location, 4);
            Grid.SetRow(location, 1);

            border.Child = control;

            border.MouseLeftButtonDown += border_MouseLeftButtonDown;
            border.MouseLeftButtonUp += monitorableObjectBorder_MouseLeftButtonUp;

            border.MouseEnter += monitorableObject_MouseEnter;
            border.MouseLeave += monitorableObject_MouseLeave;

            return border;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private Border CreateRoomsItem(int positionInRooms)
        {
            try
            {
                Border border = new Border();
                border.BorderBrush = new SolidColorBrush(Color.FromArgb(50, 200, 150, 100));
                border.BorderThickness = new Thickness(2);
                border.Name = ROOMS_PREFIX + positionInRooms;
                border.Width = viewport.Width - 40d;

                Grid grid = new Grid();
                grid.Height = 50d;
                grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
                grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
                grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });

                grid.Background = new SolidColorBrush(Color.FromArgb(50, 199, 199, 199));

                Telerik.Windows.Controls.Label label = new Telerik.Windows.Controls.Label();
                label.Content = rooms[positionInRooms].Unit.Name;
                grid.Children.Add(label);
                Grid.SetColumn(label, 0);

                Telerik.Windows.Controls.Label childrenCount = new Telerik.Windows.Controls.Label();
                childrenCount.Content = "( " + monitorableObjects.Where(e => e.MonitorableObject.CurrentUnitId == rooms[positionInRooms].Unit.Id
                    || (e.MonitorableObject.CurrentUnitId == null && e.MonitorableObject.PreviousUnitId == rooms[positionInRooms].Unit.Id)).Count().ToString() + " )";
                grid.Children.Add(childrenCount);
                Grid.SetColumn(childrenCount, 1);

                FinderImage expandImage = new FinderImage();
                expandImage.Width = 50d;
                expandImage.Height = 30d;
                expandImage.DisplayBaseImageColor = BitmapMode.Gray;
                expandImage.IsBaseImageFromResources = true;
                expandImage.BaseImageFolderInResources = ImagesFolder.OthersIcon;

                if (!childrenCount.Content.Equals("( 0 )"))
                {
                    if (!IsRoomsExpand[positionInRooms])
                        expandImage.BaseImageNameInResources = "fold.png";
                    else
                        expandImage.BaseImageNameInResources = "expand.png";
                }

                expandImage.TextHorizontalAlignment = HorizontalAlignment.Right;
                grid.Children.Add(expandImage);
                Grid.SetColumn(expandImage, 2);

                border.Child = grid;
                border.MouseLeftButtonDown += roomBorder_MouseLeftButtonDown;
                border.MouseLeftButtonUp += border_MouseLeftButtonUp;
                border.MouseEnter += categoryBorder_MouseEnter;
                border.MouseLeave += categoryBorder_MouseLeave;

                return border;
            }

            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private Border DrawFilteredCategory()
        {
            try
            {
                if (monitorableObjects == null)
                    return new Border();

                if (filteredResults == null)
                    return new Border();

                Border border = new Border();
                border.BorderBrush = new SolidColorBrush(Color.FromArgb(50, 243, 100, 100));
                border.BorderThickness = new Thickness(2);
                border.Name = FILTERED_PREFIX + 0;
                border.Width = viewport.Width - 40d;

                Grid grid = new Grid();
                grid.Height = 50d;
                grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
                grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
                grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });

                grid.Background = new SolidColorBrush(Color.FromArgb(50, 199, 199, 199));

                Telerik.Windows.Controls.Label label = new Telerik.Windows.Controls.Label();
                label.Content = "Rezultat Wyszukiwania ";
                grid.Children.Add(label);
                Grid.SetColumn(label, 0);

                Telerik.Windows.Controls.Label childrenCount = new Telerik.Windows.Controls.Label();
                childrenCount.Content = "( " + filteredResults.Count + " )";
                grid.Children.Add(childrenCount);
                Grid.SetColumn(childrenCount, 1);

                FinderImage expandImage = new FinderImage();
                expandImage.Width = 50d;
                expandImage.Height = 30d;
                expandImage.DisplayBaseImageColor = BitmapMode.Red;
                expandImage.IsBaseImageFromResources = true;
                expandImage.BaseImageFolderInResources = ImagesFolder.OthersIcon;

                if (!IsFilteredExpand)
                    expandImage.BaseImageNameInResources = "expand.png";
                else
                    expandImage.BaseImageNameInResources = "fold.png";

                expandImage.TextHorizontalAlignment = HorizontalAlignment.Right;
                grid.Children.Add(expandImage);
                Grid.SetColumn(expandImage, 2);

                border.Child = grid;
                border.MouseLeftButtonDown += roomBorder_MouseLeftButtonDown;
                border.MouseLeftButtonUp += borderFiltered_MouseLeftButtonUp;
                border.MouseEnter += categoryBorder_MouseEnter;
                border.MouseLeave += categoryBorder_MouseLeave;

                return border;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private Border DrawSpecjalAlarmCategory()
        {
            try
            { 
            if (monitorableObjects == null)
                return null;

            IEnumerable<IObjectable> alarmingObjects = monitorableObjects.Where(e => e.MonitorableObject.DisplayColor.Equals(BitmapMode.Red));

            if (alarmingObjects == null)
                return null;

            Border border = new Border();
            border.BorderBrush = new SolidColorBrush(Color.FromArgb(50, 243, 100, 100));
            border.BorderThickness = new Thickness(2);
            border.Name = ALARMS_PREFIX + 0;
            border.Width = viewport.Width - 40d ; //= Double.NaN;


            Grid grid = new Grid();
            grid.Height = 50d;
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });
            grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = GridLength.Auto });

            grid.Background = new SolidColorBrush(Color.FromArgb(50, 199, 199, 199));

            Telerik.Windows.Controls.Label label = new Telerik.Windows.Controls.Label();
            label.Content = "Alarmy ";
            grid.Children.Add(label);
            Grid.SetColumn(label, 0);

            Telerik.Windows.Controls.Label childrenCount = new Telerik.Windows.Controls.Label();
            childrenCount.Content = "( " + alarmingObjects.Count() + " )";
            grid.Children.Add(childrenCount);
            Grid.SetColumn(childrenCount, 1);

            FinderImage expandImage = new FinderImage();
            expandImage.Width = 50d;
            expandImage.Height = 30d;
            expandImage.DisplayBaseImageColor = BitmapMode.Red;
            expandImage.IsBaseImageFromResources = true;
            expandImage.BaseImageFolderInResources = ImagesFolder.OthersIcon;

            if (!IsAlarmsExpand)
                expandImage.BaseImageNameInResources = "fold.png";
            else
                expandImage.BaseImageNameInResources = "expand.png";


            expandImage.TextHorizontalAlignment = HorizontalAlignment.Right;
            grid.Children.Add(expandImage);
            Grid.SetColumn(expandImage, 2);

            border.Child = grid;
            border.MouseLeftButtonDown += roomBorder_MouseLeftButtonDown;
            border.MouseLeftButtonUp += borderAlarming_MouseLeftButtonUp;
            border.MouseEnter += categoryBorder_MouseEnter;
            border.MouseLeave += categoryBorder_MouseLeave;

            return border;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private SolidColorBrush CheckIfMonitorableObjectIsSelectedAndGetColor(Color baseColor, string name)
        {
            try
            { 
            if (SelectedMonitorableObject != null && SelectedMonitorableObject.MonitorableObject != null)
            {
                if (SelectedMonitorableObject.MonitorableObject.Id == int.Parse(name))
                {
                    int R = baseColor.R + 100 < 250 ? baseColor.R + 100 : baseColor.R + 50 < 250 ? baseColor.R + 50 : baseColor.R - 100;
                    int G = baseColor.G + 100 < 250 ? baseColor.G + 100 : baseColor.G + 50 < 250 ? baseColor.G + 50 : baseColor.G - 100;
                    int B = baseColor.B + 100 < 250 ? baseColor.B + 100 : baseColor.B + 50 < 250 ? baseColor.B + 50 : baseColor.B - 100;

                    // Color for selected monitorable object.
                    return new SolidColorBrush(Color.FromArgb(baseColor.A, (byte)R, (byte)G, (byte)B));
                }
                else
                {
                    return new SolidColorBrush(baseColor);
                }
            }
            else
            {
                // Color for normal monitorable object.
                return new SolidColorBrush(baseColor);
            }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        #endregion

        #region Select events Helphers Method
        private void SelectMonitorableObjectFromId(IObjectable objectToSelect)
        {
            try
            { 
            DeselectAllMonitorableObjects();

            if (objectToSelect == null)
                return;

            if (objectToSelect.MonitorableObject == null)
                return;

            string name = MONITORABLEOBJECT_PREFIX + objectToSelect.MonitorableObject.Id + "_" + (int)objectToSelect.MonitorableObject.ObjectType;

            foreach (Border b in DetailsGrid.Children)
                if (b.Name.Equals(name))
                    ((Grid)b.Child).Background = new SolidColorBrush(Color.FromArgb(50, 233, 233, 233));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void DeselectAllMonitorableObjects()
        {
            try
            {
                SelectedMonitorableObject = null;

                foreach (Border b in DetailsGrid.Children)
                {

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        #endregion

        #region Initialization Helphers Method
        private void RebuilGrid()
        {
            try
            { 
            int totalRowCount = this.rooms.Count + this.monitorableObjects.Count;

            DetailsGrid = new Grid();
            DetailsGrid.RowDefinitions.Clear();

            for (int i = 0; i < totalRowCount; i++)
                DetailsGrid.RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void ChangeData(List<IRoom> roomList = null, List<IObjectable> monitorableObjects = null)
        {
            try
            { 
            if (roomList != null)
            {
                this.rooms = roomList.OrderBy(e=>e.Unit.Name).ToList();

                if (IsRoomsExpand == null || IsRoomsExpand.Length != this.rooms.Count)
                    IsRoomsExpand = new bool[rooms.Count]; 
            }

            if (monitorableObjects != null)
                this.monitorableObjects = monitorableObjects;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        #endregion

        #region Control Events
        private void filteredField_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            { 
            filteredResults = new List<IObjectable>();
            filteredResults.Clear();
            char[] SEPARATORS = { '&', '|' };

            string filterText = ((TextBox)sender).Text;

            if (String.IsNullOrEmpty(filterText))
                return;

            List<string> operands = new List<string>();

            IDictionary<string, string> wordAndOperand = new Dictionary<string, string>();

            wordAndOperand.Add("Name", filterText);

            InMemoryTable<Object> search = new InMemoryTable<Object>(null, new List<Object>(monitorableObjects.Select(f => f.MonitorableObject)));
            var searchItems = search.WhereResult(new List<Object>(monitorableObjects.Select(f => f.MonitorableObject)), wordAndOperand);

            if (searchItems != null)
                filteredResults = new List<IObjectable>(monitorableObjects.Where(g => searchItems.Any(f => f.Id == g.MonitorableObject.Id)));

            Update();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void monitorableObject_MouseLeave(object sender, MouseEventArgs e)
        {
            try
            {
                Border b = (Border)sender;
                Grid g = (Grid)b.Child;
                string id = String.Empty;

                SolidColorBrush scb = g.Background as SolidColorBrush;

                Color newColor = new Color();
                int A = scb.Color.A;
                int R = scb.Color.R + 50;
                int G = scb.Color.G + 50;
                int B = scb.Color.B + 50;

                g.Background = new SolidColorBrush(Color.FromArgb((byte)A, (byte)R, (byte)G, (byte)B));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void monitorableObject_MouseEnter(object sender, MouseEventArgs e)
        {
            try
            {
                Border b = (Border)sender;
                Grid g = (Grid)b.Child;
                string id = String.Empty;

                SolidColorBrush scb = g.Background as SolidColorBrush;

                int A = scb.Color.A;
                int R = scb.Color.R - 50;
                int G = scb.Color.G - 50;
                int B = scb.Color.B - 50;

                g.Background = new SolidColorBrush(Color.FromArgb((byte)A, (byte)R, (byte)G, (byte)B));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void monitorableObjectBorder_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            { 
            DeselectAllMonitorableObjects();
            
            Border b = (Border)sender;
            ((Grid)b.Child).Background = new SolidColorBrush(Color.FromArgb(50, 173, 255, 47));

            ObjectInfo info = GetIndexFromItemName(b);

            SelectedMonitorableObject = monitorableObjects.FirstOrDefault(h => h.MonitorableObject.Id == info.Index 
                && h.MonitorableObject.ObjectType == (ObjectType)info.Type);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public struct ObjectInfo
        {
            public int Index { get; set; }
            public int Type { get; set; }
        }

        private ObjectInfo GetIndexFromItemName(Border b)
        {
            try
            { 
            ObjectInfo oi = new ObjectInfo();
            int index = -1;
            int type = -1;

            if (b.Name.Contains(ALARMS_PREFIX + MONITORABLEOBJECT_PREFIX))
            {
                string _temp = b.Name.Replace(ALARMS_PREFIX + MONITORABLEOBJECT_PREFIX, String.Empty);

                if (_temp.Contains(BANDS_TYPE_PREFIX))
                {
                    type = (int)ObjectType.Band;

                    _temp = _temp.Replace(BANDS_TYPE_PREFIX, string.Empty);
                }
                else
                {
                    type = (int)ObjectType.Patient;

                    _temp = _temp.Replace(PATIENT_TYPE_PREFIX, string.Empty);
                }

                index = int.Parse(_temp);

                //index = int.Parse(b.Name.Replace(ALARMS_PREFIX + MONITORABLEOBJECT_PREFIX, String.Empty));
            }
            else if (b.Name.Contains(FILTERED_PREFIX))
            {
                string _temp = b.Name.Replace(FILTERED_PREFIX, String.Empty);

                if (_temp.Contains(BANDS_TYPE_PREFIX))
                {
                    type = (int)ObjectType.Band;

                    _temp = _temp.Replace(BANDS_TYPE_PREFIX, string.Empty);
                }
                else
                {
                    type = (int)ObjectType.Patient;

                    _temp = _temp.Replace(PATIENT_TYPE_PREFIX, string.Empty);
                }

                index = int.Parse(_temp);

                //index = int.Parse(b.Name.Replace(FILTERED_PREFIX, String.Empty));
            }
            else
            {
                string _temp = b.Name.Replace(MONITORABLEOBJECT_PREFIX, String.Empty);

                if (_temp.Contains(BANDS_TYPE_PREFIX))
                {
                    type = (int)ObjectType.Band;

                    _temp = _temp.Replace(BANDS_TYPE_PREFIX, string.Empty);
                }
                else
                {
                    type = (int)ObjectType.Patient;

                    _temp = _temp.Replace(PATIENT_TYPE_PREFIX, string.Empty);
                }

                index = int.Parse(_temp);

                //index = int.Parse(b.Name.Replace(MONITORABLEOBJECT_PREFIX, String.Empty));
            }

            oi.Index = index;
            oi.Type = type;

            return oi;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void roomBorder_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            { 
            Border b = (Border)sender;
            Grid g = (Grid)b.Child;
            g.Background = new SolidColorBrush(Color.FromArgb(50, 190, 184, 222));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void border_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            { 
            const string TEXT_TO_REPLACE = ROOMS_PREFIX;
            Border b = (Border)sender;

            int index = int.Parse(b.Name.Replace(TEXT_TO_REPLACE, String.Empty));  // Zmienić dla alarmów!
            
            ChangeRoomsExpandForId(index);
            FixedUpdate();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void ChangeRoomsExpandForId(int index)
        {
            try
            { 
            if (IsRoomsExpand[index])
                IsRoomsExpand[index] = false;
            else
                IsRoomsExpand[index] = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void border_MouseLeave(object sender, MouseEventArgs e)
        {
            try
            { 
            Border b = (Border)sender;
            Grid g = (Grid)b.Child;
            g.Background = new SolidColorBrush(Color.FromArgb(50, 100, 150, 100));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void border_MouseEnter(object sender, MouseEventArgs e)
        {
            try
            { 
            Border b = (Border)sender;
            Grid g = (Grid)b.Child;
            g.Background = new SolidColorBrush(Color.FromArgb(50, 150, 150, 100));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void categoryBorder_MouseEnter(object sender, MouseEventArgs e)
        {
            try
            { 
            Border b = (Border)sender;
            Grid g = (Grid)b.Child;
            g.Background = new SolidColorBrush(Color.FromArgb(50, 133, 133, 133));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void categoryBorder_MouseLeave(object sender, MouseEventArgs e)
        {
            try
            { 
            Border b = (Border)sender;
            Grid g = (Grid)b.Child;
            g.Background = new SolidColorBrush(Color.FromArgb(50, 199, 199, 199));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            { 
            Border b = (Border)sender;
            Grid g = (Grid)b.Child;
            g.Background = new SolidColorBrush(Color.FromArgb(50, 133, 133, 133));

            int index = -1;
            int type = -1;

            if (b.Name.Contains(MONITORABLEOBJECT_PREFIX) && !b.Name.Contains(ALARMS_PREFIX))
            {
                string _temp = b.Name.Replace(MONITORABLEOBJECT_PREFIX, String.Empty);

                if(_temp.Contains(BANDS_TYPE_PREFIX))
                {
                    type = (int)ObjectType.Band;

                    _temp = _temp.Replace(BANDS_TYPE_PREFIX, string.Empty);
                }
                else
                {
                    type = (int)ObjectType.Patient;

                    _temp = _temp.Replace(PATIENT_TYPE_PREFIX, string.Empty);
                }
                
                index = int.Parse(_temp);
            }
            else if (b.Name.Contains(FILTERED_PREFIX))
            {
                string _temp = b.Name.Replace(FILTERED_PREFIX, String.Empty);

                if (_temp.Contains(BANDS_TYPE_PREFIX))
                {
                    type = (int)ObjectType.Band;

                    _temp = _temp.Replace(BANDS_TYPE_PREFIX, string.Empty);
                }
                else
                {
                    type = (int)ObjectType.Patient;

                    _temp = _temp.Replace(PATIENT_TYPE_PREFIX, string.Empty);
                }

                index = int.Parse(_temp);
            }
            else if (b.Name.Contains(ALARMS_PREFIX + MONITORABLEOBJECT_PREFIX))
            {
                string _temp = b.Name.Replace(ALARMS_PREFIX + MONITORABLEOBJECT_PREFIX, String.Empty);

                if (_temp.Contains(BANDS_TYPE_PREFIX))
                {
                    type = (int)ObjectType.Band;

                    _temp = _temp.Replace(BANDS_TYPE_PREFIX, string.Empty);
                }
                else
                {
                    type = (int)ObjectType.Patient;

                    _temp = _temp.Replace(PATIENT_TYPE_PREFIX, string.Empty);
                }

                index = int.Parse(_temp);
            }

            if (selectedMonitorableObject != null && selectedMonitorableObject.MonitorableObject.Id == index)
            {
                if (index != -1 && type != -1)
                {
                    RoomsViewModel.Data = new Monitorable.DetailsMonitorableObjectData(monitorableObjects.FirstOrDefault(f => f.MonitorableObject.Id == index 
                        & f.MonitorableObject.ObjectType == (ObjectType)type).MonitorableObject);
                }
            }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void borderFiltered_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            { 
            const string TEXT_TO_REPLACE = FILTERED_PREFIX;
            Border b = (Border)sender;

            int index = int.Parse(b.Name.Replace(TEXT_TO_REPLACE, String.Empty));

            if (IsFilteredExpand)
                IsFilteredExpand = false;
            else
                IsFilteredExpand = true;
            
            SelectedMonitorableObject = null;

            CheckIfUIElementsIsFilteredItemAndChangeVisibilityState();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void CheckIfUIElementsIsFilteredItemAndChangeVisibilityState()
        {
            try
            { 
            foreach (Border control in DetailsGrid.Children)
            {
                if (control.Name.Contains(FILTERED_PREFIX) && !control.Name.Contains(FILTERED_PREFIX + 0))
                    if (IsFilteredExpand)
                    {
                        control.Visibility = Visibility.Collapsed;
                        ((FinderImage)((Grid)control.Child).Children[2]).BaseImageNameInResources = "expand.png";
                    }
                    else
                    {
                        control.Visibility = Visibility.Visible;
                        ((FinderImage)((Grid)control.Child).Children[2]).BaseImageNameInResources = "fold.png";
                    }
            }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        private void borderAlarming_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Update();
        }
        #endregion

        private string ReverseLocationPath(string stringPath, char separator)
        {
            try
            { 
            List<string> wordList = new List<string>();
            StringBuilder sb = new StringBuilder(String.Empty);

            for(int i = 0; i < stringPath.Length; i++)
            {
                if (stringPath[i] != separator)
                    sb.Append(stringPath[i]);
                else
                {
                    wordList.Add(sb.ToString());
                    sb = new StringBuilder(String.Empty);
                }
            }

            wordList.Add(sb.ToString());
            sb = new StringBuilder(String.Empty);

            for (int i = wordList.Count - 1; i >= 0; i--)
            {
                sb.Append(wordList[i]);

                if(i > 0)
                {
                    sb.Append(" ").Append(separator).Append(" ");
                }
            }

            return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        public string LocationSeparator { get; set; }

        public char LocationSeparatorChar { get; set; }
    }
}
