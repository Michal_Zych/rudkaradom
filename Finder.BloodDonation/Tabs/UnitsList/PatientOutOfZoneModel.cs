﻿using Finder.BloodDonation.Common;
using Finder.BloodDonation.Dialogs.Helpers;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using FinderFX.SL.Core.MVVM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.Model.Dto.Enums;
using Finder.BloodDonation.DBBuffers;
using Finder.BloodDonation.Controls;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Tools;
using Finder.BloodDonation.DataManagers;

namespace Finder.BloodDonation.Tabs.UnitsList
{
    [OnCompleted]
    [UIException]
    public class PatientOutOfZoneModel : FinderFX.SL.Core.MVVM.ViewModelBase, Finder.BloodDonation.Dialogs.Base.IDialogWindow
    {
        private PatientOutOfZoneData _data;
        private const string TITLE = "Pacjenci poza strefą";

        public RelayCommand CancelActionCommand { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<Object> MonitorablesObjects { get; set; }

        public RelayCommand Localize { get; set; }

        private UnitsServiceClient Proxy { get; set; }

        public string WindowKey
        {
            get { return WindowsKeys.PatientOutOfZone; }
        }

        public object Data
        {
            set
            {
                _data = (PatientOutOfZoneData)value;

                if(_data != null)
                {
                    Proxy.GetBandsStatusesAsync(null);
                }
            }
        }

        private static MonitorableObjectItem selectedObject;

        public static void SelectedMonitorableObjectChanged(MonitorableObjectItem so)
        {
            PatientOutOfZoneModel.selectedObject = so;
        }

        public PatientOutOfZoneModel(Microsoft.Practices.Unity.IUnityContainer container) : base(container)
        {
            Proxy = ServiceFactory.GetService<UnitsServiceClient>(ServicesUri.UnitsService);
            Proxy.GetBandsStatusesCompleted += proxy_GetBandsStatusesCompleted;

            Localize = new RelayCommand(OnLocalizeCommand);
            CancelActionCommand = new RelayCommand(OnCancelActionCommand);
        }

        public void OnCancelActionCommand()
        {
            Container.Resolve<IWindowsManager>().CloseWindow(WindowKey);
        }

        public void OnLocalizeCommand()
        {
            if (selectedObject != null)
            {
                if((selectedObject.CurrentUnitId == null || selectedObject.CurrentUnitId < 0) &&
                    (selectedObject.PreviousUnitId == null || selectedObject.PreviousUnitId < 0))
                {
                    MsgBox.Warning("Obiekt po za zasięgiem!");
                    return;
                }
                if(UnitManager.Instance.Units.Where(x=>x.ID == selectedObject.CurrentUnitId).Count() <= 0 &&
                    UnitManager.Instance.Units.Where(x=>x.ID == selectedObject.PreviousUnitId).Count() <= 0)
                {
                    string loc = selectedObject.LocationPath == "" ? "Lokalizacja nieznana" : selectedObject.LocationPath;
                    MsgBox.Warning("Nie masz uprawnień do przeglądania tej sekcji! \n Pacjent przebywa aktualnie w lokalizacji: " + loc);
                    return;
                }

                EventAggregator.GetEvent<ExpandAccordionEvent>().Publish(AccordionItemsNames.UNITS);
                var unitIdToView = selectedObject.CurrentUnitId ?? selectedObject.PreviousUnitId;

                if (unitIdToView != null)
                    EventAggregator.GetEvent<ChangeSelectedUnitEvent>().Publish((int)unitIdToView);

                ITabManager TabManager = Container.Resolve<ITabManager>();
                TabManager.SelectTab(TabNames.Localization,
                    (e) =>
                    {
                        EventAggregator.GetEvent<ForwardingMessageEvent>().Publish((int)selectedObject.Id);
                    });

                selectedObject = null;
            }
        }

        private void proxy_GetBandsStatusesCompleted(object sender, GetBandsStatusesCompletedEventArgs e)
        {
            if(e.Result != null)
            {
                IList<BandStatusDto> bandStatusDto = e.Result;
                List<Object> _tempList = new List<Object>();
 
                ObservableCollection<Object> _tempMonitorableObject = new ObservableCollection<Object>();

                for(int i = 0; i < bandStatusDto.Count; i++)
                {
                    if(bandStatusDto[i].CurrentUnitId != bandStatusDto[i].ObjectUnitId)
                    {
                        switch (bandStatusDto[i].ObjectType)
                        {
                            case (byte)ObjectType.Patient:
                                _tempMonitorableObject.Add(CreatePatient(bandStatusDto[i]));
                                break;

                            case (byte)ObjectType.Band:
                                _tempMonitorableObject.Add(CreateBand(bandStatusDto[i]));
                                break;
                        }
                    }
                }

                for (int i = 0; i < _tempMonitorableObject.Count; i++ )
                {
                    if(!CheckIfMonitorableObjectIsInAssignedLocation(_tempMonitorableObject[i]) &&
                        !CheckIfMonitorableObjectsIsInOpenLocation(_tempMonitorableObject[i]) &&
                        CheckIfMonitorableObjectIsAssignedToDisplayUnits(_tempMonitorableObject[i]))
                    {
                        _tempList.Add(_tempMonitorableObject[i]);
                    }
                }

                MonitorablesObjects = new ObservableCollection<Object>(_tempList);
            }
        }

        private bool CheckIfMonitorableObjectIsAssignedToDisplayUnits(Object monitorableObject)
        {
            for(int i = 0; i < _data.RoomsIds.Count; i++)
            {
                if (monitorableObject.AssignUnitId == _data.RoomsIds[i])
                    return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectToCheck"></param>
        /// <returns>true, if object is in location.</returns>
        private bool CheckIfMonitorableObjectsIsInOpenLocation(Object objectToCheck)
        {
            for(int i = 0; i < _data.RoomsIds.Count; i++)
            {
                if(objectToCheck.CurrentUnitId == _data.RoomsIds[i])
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectToCheck"></param>
        /// <returns>true, if is in assigned location.</returns>
        private bool CheckIfMonitorableObjectIsInAssignedLocation(Object objectToCheck)
        {
            if (objectToCheck.CurrentUnitId == objectToCheck.AssignUnitId)
                return true;
            else
                return false;
        }


        private Patient CreatePatient(BandStatusDto dto)
        {
            return new Patient()
            {
                Id = dto.ObjectId,
                ObjectType = ObjectType.Patient,
                CurrentUnitId = dto.CurrentUnitId,
                PreviousUnitId = dto.PreviousUnitId,
                Name = dto.ObjectDisplayName,
                DisplayColor = GetDisplayColor(dto),
                EventTypes = (EventTypes)dto.TopAlarmSeverity,

                AlarmDate
                    = dto.TopAlarmDate == null
                    ? new DateTime()
                    : (DateTime)dto.TopAlarmDate,
                AssignUnitId = dto.ObjectUnitId == null ? -1 : (int)dto.ObjectUnitId,
                AlarmType = (AlarmType)dto.TopAlarmType,
                DisplayId = String.Empty,
                Location = dto.CurrentUnitLocation != null ? dto.CurrentUnitLocation : dto.PreviousUnitLocation != null ? dto.PreviousUnitLocation : "Lokalizacja nieznana",
                AlarmInfo = "S"
            };
        }

        private BitmapMode GetDisplayColor(BandStatusDto dto)
        {
            BitmapMode bm = BitmapMode.Green;

            if(dto.TopAlarmSeverity == null)
                bm = BitmapMode.Green;
            if (dto.TopAlarmSeverity == (byte)EventTypes.Alarm)
                bm = BitmapMode.Red;
            if (dto.TopAlarmSeverity == (byte)EventTypes.Warning)
                bm = BitmapMode.Yellow;
            if (dto.PreviousUnitId == null && dto.CurrentUnitId == null)
                bm = BitmapMode.Gray;

            return bm;
        }

        private Band CreateBand(BandStatusDto dto)
        {
            return new Band()
            {
                Id = dto.ObjectId,
                ObjectType = ObjectType.Band,
                CurrentUnitId = dto.CurrentUnitId,
                PreviousUnitId = dto.PreviousUnitId,
                Name = dto.ObjectDisplayName,
                DisplayColor
                    = dto.TopAlarmSeverity == null
                    ? BitmapMode.Green
                    : dto.TopAlarmSeverity == (byte)EventTypes.Alarm
                    ? BitmapMode.Red
                    : dto.TopAlarmSeverity == (byte)EventTypes.Warning
                    ? BitmapMode.Yellow
                    : BitmapMode.Green,

                EventTypes = (EventTypes)dto.TopAlarmSeverity,
                AssignUnitId = dto.ObjectUnitId == null ? -1 : (int)dto.ObjectUnitId,

                AlarmDate
                    = dto.TopAlarmDate == null
                    ? new DateTime()
                    : (DateTime)dto.TopAlarmDate,

                AlarmType = (AlarmType)dto.TopAlarmType,
                DisplayId = String.Empty,
                Location = dto.CurrentUnitLocation != null ? dto.CurrentUnitLocation : dto.PreviousUnitLocation != null ? dto.PreviousUnitLocation : "Lokalizacja nieznana",
                AlarmInfo = "S"
            };
        }

        
    }
}
