﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Tabs.UnitsList;
using System.Collections.ObjectModel;
using Finder.BloodDonation.Common;
using System.Windows.Interactivity;
using Finder.BloodDonation.Behaviors;
using FinderFX.SL.Core.MVVM;

namespace Finder.BloodDonation.Tabs
{
    [UIException]
    public partial class RoomPlanDesigner : UserControl
    {
        private readonly IDynamicEventAggregator _eventAggregator;
        private readonly bool IsUnitNameVisibility = false;

        #region Rooms
        public static readonly DependencyProperty RoomsProperty =
            DependencyProperty.Register(
            "Rooms", typeof(ObservableCollection<IRoom>),
            typeof(RoomPlanDesigner),
             new PropertyMetadata(RoomsPropertyChanged)
        );

        public ObservableCollection<IRoom> Rooms
        {
            get { 
                return (ObservableCollection<IRoom>)base.GetValue(RoomsProperty); 
            }
            set 
            { 
                base.SetValue(RoomsProperty, value); 
            }
        }

        private static void RoomsPropertyChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            RoomPlanDesigner rd = sender as RoomPlanDesigner;

            if (rd != null)
            {
                rd.UpdateViewport();
            }
        }
        #endregion

        #region Path
        public static readonly DependencyProperty PathProperty =
            DependencyProperty.Register(
            "Path", typeof(string),
            typeof(RoomPlanDesigner),
             new PropertyMetadata(PathPropertyChanged)
        );

        public string Path
        {
            get { return (string)base.GetValue(PathProperty); }
            set { base.SetValue(PathProperty, value); }
        }

        private static void PathPropertyChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            RoomPlanDesigner rd = sender as RoomPlanDesigner;

            if (rd != null)
            {
                rd.UpdatePath();
            }
        }
        #endregion

        public bool EditMode { get; set; }

        public List<Room> DrawedRooms { get; set; }

        public RoomPlanDesigner()
        {
            InitializeComponent();

            _eventAggregator = Bootstrapper.StaticContainer.Resolve<IDynamicEventAggregator>();

            DrawedRooms = new List<Room>();

            this.Loaded += RoomDesigner_Loaded;
            this.SizeChanged += RoomDesigner_SizeChanged;
        }

        double im_w = 0;
        double im_h = 0;

        void ib_ImageOpened(object sender, RoutedEventArgs e)
        {
            viewport.Visibility = System.Windows.Visibility.Visible;
            ImageBrush ib = sender as ImageBrush;
            BitmapImage bi = ib.ImageSource as BitmapImage;

            im_w = bi.PixelWidth;
            im_h = bi.PixelHeight;
            UpdateViewport();
        }

        private void UpdateViewport()
        {
            if (slScale == null) return;

            double scroll_width = 12;
            double scroll_height = 12;

            double proportion = Math.Min((scroll.ActualWidth - scroll_width) / im_w, (scroll.ActualHeight - scroll_height) / im_h);
            proportion = Math.Max(proportion * slScale.Value, 0);

            LayoutRoot.Width = im_w * proportion;
            LayoutRoot.Height = im_h * proportion;

            if (LayoutRoot.Width > scroll.ActualWidth)
            {
                scroll.HorizontalScrollBarVisibility = ScrollBarVisibility.Visible;
            }
            else
            {
                scroll.HorizontalScrollBarVisibility = ScrollBarVisibility.Disabled;
            }

            if (LayoutRoot.Height > scroll.ActualHeight)
            {
                scroll.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
            }
            else
            {
                scroll.VerticalScrollBarVisibility = ScrollBarVisibility.Disabled;
            }

            viewport.Width = LayoutRoot.Width;
            viewport.Height = LayoutRoot.Height;
            RedrawRooms();
        }

        public void UpdatePath()
        {
            viewport.Children.Clear();
            viewport.Visibility = System.Windows.Visibility.Collapsed;
            if (!string.IsNullOrEmpty(Path))
            {
                ImageBrush ib = new ImageBrush()
                {
                    ImageSource = new BitmapImage(new Uri(Path, UriKind.RelativeOrAbsolute)),
                    Stretch = Stretch.Uniform
                };

                ib.ImageOpened += new EventHandler<RoutedEventArgs>(ib_ImageOpened);
                LayoutRoot.Background = ib;
            }
            else
            {
                LayoutRoot.Background = null;
            }
        }

        void RoomDesigner_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateViewport();
        }

        void RoomDesigner_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateViewport();
        }

        private Brush fill = new SolidColorBrush(Color.FromArgb(128, 255, 0, 0));

        private void RedrawRooms()
        {
            ClearRooms();
            if (Rooms != null && !double.IsNaN(viewport.Height) && !double.IsNaN(viewport.Width))
            {
                IRoom[] _Rooms = Rooms.OrderBy(x => x.Order).ToArray();

                for (int i = 0; i < _Rooms.Length; i++)
                {
                    Room r = new Room();

                    double h = Convert.ToDouble(viewport.Height * _Rooms[i].BR.X) - Convert.ToDouble(viewport.Height * _Rooms[i].TL.X);
                    double w = Convert.ToDouble(viewport.Width * _Rooms[i].BR.Y) - Convert.ToDouble(viewport.Width * _Rooms[i].TL.Y);

                    r.Width = w;
                    r.Height = h;
                    if (!string.IsNullOrEmpty(Rooms[i].Style))
                    {
                        r.Style = Application.Current.Resources[_Rooms[i].Style] as Style;
                    }

                    r.DataContext = _Rooms[i];

                    viewport.Children.Add(r);

                    Interaction.GetBehaviors(r).Add(new RoomMoveBehavior());

                    Canvas.SetTop(r, viewport.Height * _Rooms[i].TL.X);
                    Canvas.SetLeft(r, viewport.Width * _Rooms[i].TL.Y);
                    DrawedRooms.Add(r);
                }
            }

            AddFrame();
        }

        private Border _border;
        private void AddFrame()
        {
            const int FRAME_THICKNESS = 2;
            viewport.Children.Remove(_border);
            _border = new Border();
            _border.BorderBrush = new SolidColorBrush(Colors.Red);
            _border.BorderThickness = new Thickness(FRAME_THICKNESS);
            _border.Width = viewport.Width;
            _border.Height = viewport.Height;
            viewport.Children.Add(_border);
            Canvas.SetTop(_border, 0);
            Canvas.SetLeft(_border, 0);
        }

        private void ClearRooms()
        {
            for (int i = 0; i < DrawedRooms.Count; i++)
            {
                viewport.Children.Remove(DrawedRooms[i]);
            }

            DrawedRooms.Clear();
        }

        private void btnFill_Click(object sender, RoutedEventArgs e)
        {
            slScale.Value = 1;
        }

        private void btn100_Click(object sender, RoutedEventArgs e)
        {
            UpdateViewport();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            _eventAggregator.GetEvent<SavePreviewEvent>().Publish(0);
        }

        private void slScale_ValueChanged(object sender, RoutedEventArgs e)
        {
            UpdateViewport();
        }

        private void btnTools_Click(object sender, RoutedEventArgs e)
        {
            _eventAggregator.GetEvent<ShowToolsWindow>().Publish(0);
        }
    }
}
