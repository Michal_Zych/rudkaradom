﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Tabs.UnitsList
{
    [TemplatePart(Name = PART_MainBorder, Type = typeof(Border))]
    [TemplatePart(Name = PART_MainGrid, Type = typeof(Grid))]
    public class MonitorableObjectControl : Control
    {
            private const string PART_MainBorder = "PART_MainBorder";
            private const string PART_MainGrid = "PART_MainGrid";

            private Border MainBorder = null;
            public Grid MainGrid = null;

            public MonitorableObjectControl()
            {
                this.DefaultStyleKey = typeof(MonitorableObjectControl);
            }

            public override void OnApplyTemplate()
            {
                base.OnApplyTemplate();

                MainBorder = GetTemplateChild(PART_MainBorder) as Border;
                MainGrid = GetTemplateChild(PART_MainGrid) as Grid;

                this.MouseEnter += new MouseEventHandler(Room_MouseEnter);
                this.MouseLeave += new MouseEventHandler(Room_MouseLeave);
            }

            void Room_MouseLeave(object sender, MouseEventArgs e)
            {
                //VisualStateManager.GoToState(this, "Normal", false);
            }

            void Room_MouseEnter(object sender, MouseEventArgs e)
            {
                //VisualStateManager.GoToState(this, "MouseOver", false);
            }
        }
    
}
