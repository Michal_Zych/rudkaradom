﻿using Finder.BloodDonation.Controls;
using Finder.BloodDonation.DBBuffers;
using Finder.BloodDonation.Model.Dto.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Finder.BloodDonation.Tabs.UnitsList
{
    public partial class MonitorableObjectItem : UserControl
    {
        public int Id { get; set; }
        public int? CurrentUnitId { get; set; }
        public int? PreviousUnitId { get; set; }

        public static readonly DependencyProperty DisplayLayoutColorProperty =
    DependencyProperty.Register("DisplayLayoutColor", typeof(Brush), typeof(MonitorableObjectItem),
        new PropertyMetadata(DisplayLayoutColorChanged));

        public static readonly DependencyProperty DisplayNameProperty =
    DependencyProperty.Register("DisplayName", typeof(string), typeof(MonitorableObjectItem),
        new PropertyMetadata(OnDisplayNameChanged));

        private static void DisplayLayoutColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetDisplayLayoutColor = (MonitorableObjectItem)d;
            var oldDisplayLayoutColor = (Brush)e.OldValue;

            var newDisplayLayoutColor = targetDisplayLayoutColor.DisplayLayoutColor;
            targetDisplayLayoutColor.OnDisplayNameChanged(oldDisplayLayoutColor, newDisplayLayoutColor);
        }

        private void OnDisplayNameChanged(Brush oldDisplayLayoutColor, Brush newDisplayLayoutColor)
        {
            LayoutRoot.Background = newDisplayLayoutColor;
        }

        private static void OnDisplayNameChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetDisplayName = (MonitorableObjectItem)d;
            var oldDisplayName = (string)e.OldValue;

            var newDisplayName = targetDisplayName.DisplayName;
            targetDisplayName.OnDisplayNameChanged(oldDisplayName, newDisplayName);
        }

        [System.ComponentModel.Category("Item Settings")]
        public string DisplayName
        {
            get { return (string)(GetValue(DisplayNameProperty)); }
            set { SetValue(DisplayNameProperty, value); }
        }

        public Brush DisplayLayoutColor
        {
            get { return (Brush)(GetValue(DisplayLayoutColorProperty)); }
            set { SetValue(DisplayLayoutColorProperty, value); }
        }

        protected virtual void OnDisplayNameChanged(string oldDisplayName, string newDisplayName)
        {
            DisplayName = newDisplayName;
            //OnStatusImagesChanged(StatusImages, StatusImages);
        }

        public static readonly DependencyProperty LocationPathProperty =
    DependencyProperty.Register("LocationPath", typeof(string), typeof(MonitorableObjectItem),
        new PropertyMetadata(OnLocationPathChanged));

        private static void OnLocationPathChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetLocationPath = (MonitorableObjectItem)d;
            var oldDisplayName = (string)e.OldValue;

            var newDisplayName = targetLocationPath.LocationPath;
            targetLocationPath.OnLocationPathChanged(oldDisplayName, newDisplayName);
        }

        [System.ComponentModel.Category("Item Settings")]
        public string LocationPath
        {
            get { return (string)(GetValue(LocationPathProperty)); }
            set { SetValue(LocationPathProperty, value); }
        }

        protected virtual void OnLocationPathChanged(string oldLocationPath, string newLocationPath)
        {
            if (LocationPath != String.Empty)
                LocationPath = newLocationPath;
            //OnStatusImagesChanged(StatusImages, StatusImages);
        }

        public static readonly DependencyProperty StateProperty =
    DependencyProperty.Register("State", typeof(State), typeof(MonitorableObjectItem),
        new PropertyMetadata(OnStateChanged));

        private static void OnStateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetState = (MonitorableObjectItem)d;
            var oldState = (State)e.OldValue;

            var newState = targetState.State;
            targetState.OnStateChanged(oldState, newState);
        }

        [System.ComponentModel.Category("Item Settings")]
        public State State
        {
            get { return (State)(GetValue(StateProperty)); }
            set { SetValue(StateProperty, value); }
        }

        protected virtual void OnStateChanged(State oldState, State newState)
        {
            State = newState;
            SetSelectedColor();
        }

        public static readonly DependencyProperty SelectedColorProperty =
DependencyProperty.Register("State", typeof(SelectedColor), typeof(MonitorableObjectItem),
new PropertyMetadata(OnSelectedColorChanged));

        private static void OnSelectedColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetSelectedColor = (MonitorableObjectItem)d;
            var oldSelectedColor = (SelectedColor)e.OldValue;

            var newSelectedColor = targetSelectedColor.SelectedColor;
            targetSelectedColor.OnSelectedColorChanged(oldSelectedColor, newSelectedColor);
        }

        [System.ComponentModel.Category("Item Settings")]
        public SelectedColor SelectedColor
        {
            get { return (SelectedColor)(GetValue(SelectedColorProperty)); }
            set { SetValue(SelectedColorProperty, value); }
        }

        protected virtual void OnSelectedColorChanged(SelectedColor oldSelectedColor, SelectedColor newSelectedColor)
        {
            SelectedColor = newSelectedColor;
            SetSelectedColor();
        }

        private void SetSelectedColor()
        {
            if (State == Controls.State.Selected)
            {
                switch (SelectedColor)
                {
                    case Controls.SelectedColor.LightBlue:
                        LayoutRoot.Background = new SolidColorBrush(Color.FromArgb(100, 196, 220, 255));
                        break;

                    case Controls.SelectedColor.LightGray:
                        LayoutRoot.Background = new SolidColorBrush(Colors.LightGray);
                        break;

                    case Controls.SelectedColor.LightRed:
                        LayoutRoot.Background = new SolidColorBrush(Color.FromArgb(100, 255, 214, 224));
                        break;

                    case Controls.SelectedColor.LightGreen:
                        LayoutRoot.Background = new SolidColorBrush(Color.FromArgb(100, 167, 255, 165));
                        break;
                }
            }
            else
            {
                LayoutRoot.Background = new SolidColorBrush(Colors.Transparent);
            }
        }

        public static readonly DependencyProperty ObjectTypeProperty =
DependencyProperty.Register("ObjectType", typeof(ObjectType), typeof(MonitorableObjectItem),
new PropertyMetadata(OnObjectTypeChanged));

        private static void OnObjectTypeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetObjectType = (MonitorableObjectItem)d;
            var oldObjectType = (ObjectType)e.OldValue;

            var newObjectType = targetObjectType.ObjectType;
            targetObjectType.OnObjectTypeChanged(oldObjectType, newObjectType);
        }

        [System.ComponentModel.Category("Item Settings")]
        public ObjectType ObjectType
        {
            get { return (ObjectType)(GetValue(ObjectTypeProperty)); }
            set { SetValue(ObjectTypeProperty, value); }
        }

        protected virtual void OnObjectTypeChanged(ObjectType oldObjectType, ObjectType newObjectType)
        {
            ObjectType = newObjectType;
            image.IsBaseImageFromResources = true;
            image.BaseImageFolderInResources = DBBuffers.ImagesFolder.Objects;
            string ot = this.ObjectType.ToString() + ".png";
            image.BaseImageNameInResources = this.ObjectType.ToString() + ".png";
            image.DisplayBaseImageColor = DisplayColor;
        }

        public static readonly DependencyProperty DisplayColorProperty =
DependencyProperty.Register("DisplayColor", typeof(BitmapMode), typeof(MonitorableObjectItem),
new PropertyMetadata(OnDisplayColorChanged));

        private static void OnDisplayColorChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var targetDisplayColor = (MonitorableObjectItem)d;
            var oldDisplayColor = (BitmapMode)e.OldValue;

            var newDisplayColor = targetDisplayColor.DisplayColor;
            targetDisplayColor.OnDisplayColorChanged(oldDisplayColor, newDisplayColor);
        }

        [System.ComponentModel.Category("Item Settings")]
        public BitmapMode DisplayColor
        {
            get { return (BitmapMode)(GetValue(DisplayColorProperty)); }
            set { SetValue(DisplayColorProperty, value); }
        }

        protected virtual void OnDisplayColorChanged(BitmapMode oldDisplayColor, BitmapMode newDisplayColor)
        {
            DisplayColor = newDisplayColor;
            image.DisplayBaseImageColor = DisplayColor;
        }

        public MonitorableObjectItem()
        {
            InitializeComponent();
            image.IsBaseImageFromResources = true;
            image.BaseImageFolderInResources = DBBuffers.ImagesFolder.Objects;
            string ot = this.ObjectType.ToString() + ".png";
            image.BaseImageNameInResources = this.ObjectType.ToString() + ".png";
        }
    }
}