﻿using System;
using System.Linq;
using System.Windows;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.ReportsService;
using Finder.BloodDonation.Common;
using FinderFX.SL.Core.Authentication;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model;
using Finder.BloodDonation.Common.Events;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.Model.Authentication;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Tabs.MenuAdministration.SettingsSystem;

namespace Finder.BloodDonation.Tabs.Reports
{
    [OnCompleted]
    [UIException]
    public class ReportsListViewModel : ViewModelBase, ITabViewModel
    {
        private const int ReportTypeTemperature = 1;

        [RaisePropertyChanged]
        public DateTime? ReportFilterDateStart { get; set; }

        [RaisePropertyChanged]
        public DateTime? ReportFilterDateEnd { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterTimeStart { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterTimeEnd { get; set; }

        [RaisePropertyChanged]
        public ReportDto SelectedReport { get; set; }

        [RaisePropertyChanged]
        public UserReportDto PreparedReport { get; set; }

        [RaisePropertyChanged]
        public bool HideCharts { get; set; }

        [RaisePropertyChanged]
        public bool IsMono { get; set; }

        [RaisePropertyChanged]
        public ObservableCollection<ReportDto> Reports { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> ShowReportHTML { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> ShowReportPDF { get; set; }

        [RaisePropertyChanged]
        public DelegateCommand<object> ShowReportExcel { get; set; }

        public ITabManager TabManager = null;

        private ReportsServiceClient LoadService
        {
            get
            {
                ReportsServiceClient proxy = ServiceFactory.GetService<ReportsServiceClient>(ServicesUri.ReportsService);
                proxy.GetReportsListCompleted += proxy_GetReportsListCompleted;
                proxy.PrepareReportCompleted += proxy_PrepareReportCompleted;
                proxy.PrepareReportForUnitCompleted += ProxyOnPrepareReportForUnitCompleted;
                return proxy;
            }
        }

        void proxy_GetReportsListCompleted(object sender, GetReportsListCompletedEventArgs e)
        {
            Reports = new ObservableCollection<ReportDto>(e.Result.Where(r => r.Type == ReportTypeTemperature));
        }

        public ReportsListViewModel(IUnityContainer container)
            : base(container)
        {
            InitializeDates();

            ShowReportHTML = new DelegateCommand<object>(this.ShowReportHTML_Command);
            ShowReportPDF = new DelegateCommand<object>(this.ShowReportPDF_Command);
            ShowReportExcel = new DelegateCommand<object>(this.ShowReportExcel_Command);

            this.ViewAttached += new EventHandler(UnitsListViewModel_ViewAttached);

            LoadService.GetReportsListAsync();

            EventAggregator.GetEvent<ShowReportTabEvent>().Subscribe(ShowReportTabEvent_Event);
            EventAggregator.GetEvent<SelectExtendedUnitEvent>().Subscribe(SelectExtendedUnitEventHandler);


            TabManager = this.Container.Resolve<ITabManager>();
        }

        public void SelectExtendedUnitEventHandler(Common.Events.SelectedUnit e)
        {
            //_seletedUnitSource = e.SelectionSource;
        }

        public void ShowReportTabEvent_Event(ReportTab o)
        {
            ReportFilterDateStart = o.DateStart;
            SearchFilterTimeStart = ReportFilterDateStart;

            ReportFilterDateEnd = o.DateEnd;
            SearchFilterTimeEnd = ReportFilterDateEnd;
        }

        void UnitsListViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        private IUnit SelectedUnit = null;
        private int _seletedUnitSource;

        public void Refresh(IUnit unit)
        {
            SelectedUnit = unit;
        }

        public event EventHandler LoadingCompleted = delegate { };

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        void proxy_PrepareReportCompleted(object sender, PrepareReportCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                PreparedReport = e.Result;
                ShowReport(e.UserState);
            }
        }

        private void ProxyOnPrepareReportForUnitCompleted(object sender, PrepareReportForUnitCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                PreparedReport = e.Result;
                ShowReport(e.UserState);
            }
        }

        public void ShowReportHTML_Command(object o)
        {
            PrepareReport("HTML");
        }

        public void ShowReportPDF_Command(object o)
        {
            PrepareReport("PDF");
        }

        public void ShowReportExcel_Command(object o)
        {
            PrepareReport("EXCEL");
        }

        private void PrepareReport(string type)
        {
            if (SelectedReport == null || !ReportFilterDateEnd.HasValue || !ReportFilterDateStart.HasValue)
                return;

            InitializeDates();
            DateTime startDate = new DateTime(
                            ReportFilterDateStart.Value.Year,
                            ReportFilterDateStart.Value.Month,
                            ReportFilterDateStart.Value.Day,
                            SearchFilterTimeStart.Value.Hour,
                            SearchFilterTimeStart.Value.Minute,
                            SearchFilterTimeStart.Value.Second),
                         endDate = new DateTime(
                            ReportFilterDateEnd.Value.Year,
                            ReportFilterDateEnd.Value.Month,
                            ReportFilterDateEnd.Value.Day,
                            SearchFilterTimeEnd.Value.Hour,
                            SearchFilterTimeEnd.Value.Minute,
                            SearchFilterTimeEnd.Value.Second);

            if (MsgBox.DatesInRange(/*Global.Settings.Current.GetInt("MaxDaysReports")*/ Container.Resolve<ISettingsRespository>().Settings.MaxDaysReports, startDate, endDate))
            {
                if (SelectedReport.Type == ReportTypeTemperature)
                    LoadService.PrepareReportAsync(
                        SelectedReport.ID,
                        SelectedUnit.Identity,
                        startDate,
                        endDate,
                        HideCharts,
                        IsMono,
                        type);

            }
        }


        private void InitializeDates()
        {
            if (!ReportFilterDateStart.HasValue)
            {
                DateTime yesterday = DateTime.Now.AddDays(-1);
                ReportFilterDateStart = new DateTime(
                   yesterday.Year,
                   yesterday.Month,
                   yesterday.Day,
                   0, 0, 0
                );
                SearchFilterTimeStart = ReportFilterDateStart;
            }

            if (!SearchFilterTimeStart.HasValue)
            {
                SearchFilterTimeStart = ReportFilterDateStart;
            }

            if (!ReportFilterDateEnd.HasValue)
            {
                DateTime today = DateTime.Now;
                ReportFilterDateEnd = new DateTime(
                   today.Year,
                   today.Month,
                   today.Day,
                   23, 59, 59
                );
                SearchFilterTimeEnd = ReportFilterDateEnd;
            }

            if (!SearchFilterTimeEnd.HasValue)
            {
                SearchFilterTimeEnd = ReportFilterDateEnd;
            }
        }

        private void ShowReport(object type)
        {
            string url = string.Format(
                    "Report.aspx?Type={0}&Id={1}"
                    , type.ToString()
                    , PreparedReport.ID);

            System.Windows.Browser.HtmlPage.Window.Navigate(
                new Uri(url, UriKind.RelativeOrAbsolute),
                "__blank");
        }


        public string Title
        {
            get { return SelectedUnit.UnitDescription; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }
}
