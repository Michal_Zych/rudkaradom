﻿using System;
using FinderFX.SL.Core.MVVM;
using System.Windows.Controls;

namespace Finder.BloodDonation.Tabs.Reports
{
    [ViewModel(typeof(ReportsListViewModel))]
    public partial class ReportsListView : UserControl
    {
        public ReportsListView()
        {
            InitializeComponent();
        }
    }
}
