﻿using System;
using System.Net;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Common.Events;
using System.Collections.ObjectModel;
using System.Windows.Media.Animation;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.UnitsService;
using System.Windows.Media.Imaging;
using Finder.BloodDonation.Common.Authentication;
using FinderFX.SL.Core.Authentication;
using Finder.BloodDonation.Model;
using Finder.BloodDonation.Layout;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Finder.BloodDonation.Tabs.Contents;
using Finder.BloodDonation.Model.Authentication;
using System.Windows.Controls;
using System.Windows;
using Finder.BloodDonation.Helpers;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;

namespace Finder.BloodDonation.Tabs.Plan
{
    [OnCompleted]
    [UIException]
    public class ChartViewModel : ViewModelBase, ITabViewModel
    {
        Guid g = Guid.NewGuid();

        [RaisePropertyChanged]
        public IUnit SelectedUnit { get; set; }

        [RaisePropertyChanged]
        public BitmapImage ImageSource { get; set; }
        
        [RaisePropertyChanged]
        public bool IsBusyImage { get; set; }


        [RaisePropertyChanged]
        //[CustomValidation(typeof(MonitoringValidator), "IsAddressValid")]
        public DateTime? SearchFilterDateTimeStart { get; set; }

        //[RaisePropertyChanged]
        //public DateTime? SearchFilterTimeStart { get; set; }

        //[RaisePropertyChanged]
        //public DateTime? SearchFilterTimeEnd { get; set; }

        [RaisePropertyChanged]
        public DateTime? SearchFilterDateTimeEnd { get; set; }


        [RaisePropertyChanged]
        public IList<SensorInfoDto> Sensors { get; set; }

        private bool IsInitializeData = false;

        public DelegateCommand<object> ChangeRange { get; set; }
        public DelegateCommand<object> ChangeRangeHour { get; set; }
        public DelegateCommand<object> ChangeRangeDay { get; set; }
        public DelegateCommand<object> ChangeRangeMonth { get; set; }
        public DelegateCommand<object> ShowReport { get; set; }
        public DelegateCommand<object> ChangeStartDate { get; set; }

        IUserSessionManager _userSessionManager = null;
        ITabManager TabManager = null;

        private AlarmsServiceClient LoadService
        {
            get
            {
                AlarmsServiceClient proxy = ServiceFactory.GetService<AlarmsServiceClient>(ServicesUri.AlarmsService);
                return proxy;
            }
        }

        private UnitsServiceClient LoadServiceUnits
        {
            get
            {
                UnitsServiceClient proxy = ServiceFactory.GetService<UnitsServiceClient>(ServicesUri.UnitsService);
                proxy.GetSensorsInUnitCompleted += new EventHandler<GetSensorsInUnitCompletedEventArgs>(proxy_GetSensorsInUnitCompleted);
                return proxy;
            }
        }

        void proxy_GetSensorsInUnitCompleted(object sender, GetSensorsInUnitCompletedEventArgs e)
        {
            var list = new List<SensorInfoDto>(e.Result);
            Sensors = list;

            RefreshImage();
        }



        public ChartViewModel(IUnityContainer unityContainer)
            : base(unityContainer)
        {
            //ping_monitoring = new Storyboard()
            //{
            //Duration = new System.Windows.Duration(new TimeSpan(0, 1, 0))
            //};
            //ping_monitoring.Completed += new EventHandler(ping_monitoring_Completed);
            //ping_monitoring.Begin();

            InitializeDates();

            ChangeRange = new DelegateCommand<object>(this.ChangeRange_Command);
            ChangeRangeHour = new DelegateCommand<object>(this.ChangeRangeHour_Command);
            ChangeRangeDay = new DelegateCommand<object>(this.ChangeRangeDay_Command);
            ChangeRangeMonth = new DelegateCommand<object>(this.ChangeRangeMonth_Command);
            ShowReport = new DelegateCommand<object>(this.ShowReport_Command);
            //ChangeStartDate = new DelegateCommand<object>(this.ChangeStartDate_Command);

            _userSessionManager = unityContainer.Resolve<IUserSessionManager>();
            TabManager = unityContainer.Resolve<ITabManager>();

            EventAggregator.GetEvent<RefreshDeviceDataEvent>().Subscribe(RefreshDeviceDataEvent_Event);
            EventAggregator.GetEvent<ShowMonitoringTabEvent>().Subscribe(ShowMonitoringTabEvent_Event);
        }

        public void ShowMonitoringTabEvent_Event(MonitoringTab o)
        {
            SearchFilterDateTimeStart = o.DateStart;

            SearchFilterDateTimeEnd = o.DateEnd;

            if (o.UnitId.HasValue)
                EventAggregator.GetEvent<SelectStructureUnitEvent>().Publish(o.UnitId.Value);
        }

        public void RefreshDeviceDataEvent_Event(int unit)
        {
            RefreshImage();
            /*
            if (SelectedUnit.Identity == unit)
            {
                LoadService.GetCurrentDataAsync(unit);
            }
             */
        }



        public void ShowReport_Command(object o)
        {
            InitializeDates();

            TabManager.SelectTab(TabNames.Reports, ShowReport_ReportTabLoaded);
        }

        public void ShowReport_ReportTabLoaded(string report_tab_name)
        {
            EventAggregator.GetEvent<ShowReportTabEvent>().Publish(
                            new ReportTab()
                            {
                                DateStart = SearchFilterDateTimeStart.Value,
                                DateEnd = SearchFilterDateTimeEnd.Value
                            }
                        );
        }

        public void ChangeRange_Command(object o)
        {

            if (SearchFilterDateTimeStart.HasValue && SearchFilterDateTimeEnd.HasValue)
            {
                InitializeDates();
            }

            RefreshImage();
        }

        public void ChangeRangeHour_Command(object o)
        {
            SearchFilterDateTimeStart = DateTime.Now.AddHours(-1);
            SearchFilterDateTimeEnd = DateTime.Now;
            ChangeRange_Command(null);
        }

        public void ChangeRangeDay_Command(object o)
        {
            DateTime b = DateTime.Now.AddDays(-1);
            SearchFilterDateTimeStart = new DateTime(b.Year, b.Month, b.Day, 0, 0, 0);
            SearchFilterDateTimeEnd = DateTime.Now;
            ChangeRange_Command(null);
        }

        public void ChangeRangeMonth_Command(object o)
        {
            DateTime b = DateTime.Now.AddDays(-31);
            SearchFilterDateTimeStart = new DateTime(b.Year, b.Month, b.Day, 0, 0, 0);
            SearchFilterDateTimeEnd = new DateTime(SearchFilterDateTimeEnd.Value.Year, SearchFilterDateTimeEnd.Value.Month, SearchFilterDateTimeEnd.Value.Day, 23, 59, 59);
            ChangeRange_Command(null);
        }

        private void InitializeDates()
        {
            if (!SearchFilterDateTimeStart.HasValue || !SearchFilterDateTimeEnd.HasValue)
            {
                SearchFilterDateTimeStart = DateTime.Now.AddHours(-Global.Settings.Current.GetInt(Global.ConfigurationKeys.SearchChartDataHoursBefore));
                SearchFilterDateTimeEnd = DateTime.Now.AddHours(Global.Settings.Current.GetInt(Global.ConfigurationKeys.SearchChartDataHoursAfter));
            }
        }

        public void NotifyChange()
        {
            this.NotifyPropertyChange("Data");
            this.NotifyPropertyChange("IsAlarm");
            this.NotifyPropertyChange("HistoryData");
            this.NotifyPropertyChange("A1L");
            this.NotifyPropertyChange("A1H");
            this.NotifyPropertyChange("A2L");
            this.NotifyPropertyChange("A2H");
            this.NotifyPropertyChange("Temp1");
            this.NotifyPropertyChange("Temp2");
        }

        public void Refresh(IUnit unit)
        {
            IsInitializeData = false;
            NotifyChange();
            SelectedUnit = unit;
            LoadServiceUnits.GetSensorsInUnitAsync(unit.Identity);
            _userSessionManager.SetMonitoringDevice(SelectedUnit.Identity, null, g);
           // LoadService.GetCurrentDataAsync(SelectedUnit.Identity);
            //- w monitorowaniu widać tylko temperatury, ukryć historię - P 23-10-2012
            //if (SearchFilterDateTimeStart.HasValue && SearchFilterDateTimeEnd.HasValue)
            //    History.Refresh(unit, SearchFilterDateTimeStart.Value, SearchFilterDateTimeEnd.Value);

        }

        void ImageSource_ImageOpened(object sender, System.Windows.RoutedEventArgs e)
        {
            IsBusyImage = false;
        }

        private void RefreshImage()
        {
            if (Sensors != null && Sensors.Count > 0)
            {
                InitializeDates();
                if (ImageSource != null)
                {
                    ImageSource.ImageOpened -= ImageSource_ImageOpened;
                }
                string add_guid = string.Empty;
                if (SearchFilterDateTimeEnd >= DateTime.Now)
                {
                    add_guid = "&Guid=" + Guid.NewGuid().ToString();
                }

                //R.S.Jak ustalić rozmiar obrazka jaki chcemy dostać?
                string width = "&Width=" + Global.Settings.Current[Global.ConfigurationKeys.ChartImageWidth];
                string height = "&Height=" + Global.Settings.Current[Global.ConfigurationKeys.ChartImageHeight];

                string sensors = "&Sensors=";

                for (int i = Sensors.Count-1; i>=0; i--)
                {
                    sensors = sensors + Sensors[i].ID.ToString();
                    if (i != 0) sensors = sensors + ",";
                }

                string showAllAlarms = "&ShowAllAlarms=" +
                    (BloodyUser.Current.CanSeeAllAlarms ? "1" : "0");

                var s = Application.Current.Host.Source;
                var k = new UriBuilder(s.Scheme, s.Host, Int32.Parse(((BloodyUser)User.Current).GetProperty(ConfigurationsKeys.CHART_SERVER_PORT)));
                ImageSource = null;
                ImageSource = new BitmapImage(new Uri(
                    string.Format(
                        "{0}DeviceChart.ashx?From={1}&To={2}{3}{4}{5}{6}{7}"
                        , k.ToString()
                        , SearchFilterDateTimeStart.Value.ToShortDateString() + " " + SearchFilterDateTimeStart.Value.ToShortTimeString()
                        , SearchFilterDateTimeEnd.Value.ToShortDateString() + " " + SearchFilterDateTimeEnd.Value.ToShortTimeString()
                        , add_guid
                        , width
                        , height
                        , sensors
                        , showAllAlarms
                    ),
                    UriKind.RelativeOrAbsolute));
                IsBusyImage = true;
                ImageSource.ImageOpened += ImageSource_ImageOpened;
            }
        }

        public event EventHandler LoadingCompleted;

        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        private void validateDates()
        {
            if (SearchFilterDateTimeStart.HasValue && SearchFilterDateTimeEnd.HasValue
                && SearchFilterDateTimeStart.HasValue && SearchFilterDateTimeEnd.HasValue)
            {
                if (SearchFilterDateTimeStart.Value.Year > 2000 && SearchFilterDateTimeEnd.Value.Year > 2000)
                {
                    DateTime start = SearchFilterDateTimeStart.Value;
                    DateTime end = SearchFilterDateTimeEnd.Value;

                    if (end < start)
                    {
                        MsgBox.Error("Data końcowa nie może być wcześniejsza niż data początkowa");
                        SearchFilterDateTimeEnd = SearchFilterDateTimeStart;
                    }
                }
            }
        }

        public void DropDownClosed(object sender, RoutedEventArgs e)
        {
            this.validateDates();
        }


        public string Title
        {
            get { return ""; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }
}
