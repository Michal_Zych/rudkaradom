﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.UnitsService;
using FinderFX.SL.Core.Communication;
using System.Collections.Generic;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Tabs.UnitsList;
using Finder.BloodDonation.Model;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Tabs.Plan;

namespace Finder.BloodDonation.Tabs
{
    [UIException]
    public class UnitPlanModel : ViewModelBase, ITabViewModel
    {
        [RaisePropertyChanged]
        public object ChartView { get; set; }

        [RaisePropertyChanged]
        public object PlanView { get; set; }

        [RaisePropertyChanged]
        public bool IsChartVisible { get; set; }

        [RaisePropertyChanged]
        public double SplitterColumnWidth { get; set; }

        private List<ZonePreviewDto> PreviewUnits { get; set; }

        private UnitsServiceClient LoadService
        {
            get
            {
                UnitsServiceClient proxy = ServiceFactory.GetService<UnitsServiceClient>(ServicesUri.UnitsService);
                proxy.PreviewForUnitGetCompleted += proxy_PreviewForUnitGetCompleted;
                return proxy;
            }
        }

        void proxy_PreviewForUnitGetCompleted(object sender, PreviewForUnitGetCompletedEventArgs e)
        {
            PreviewUnits = new List<ZonePreviewDto>(e.Result);
            RefreshUnitView(PlanView);
        }

        public UnitPlanModel(IUnityContainer container)
            : base(container)
        {
            this.ViewAttached += new EventHandler(UnitsListViewModel_ViewAttached);
        }

        void UnitsListViewModel_ViewAttached(object sender, EventArgs e)
        {
            IsChartVisible = false;
            PlanView = this.Container.Resolve<ViewFactory>().CreateView<RoomsView>();
            ChartView = this.Container.Resolve<ViewFactory>().CreateView<ChartView>();
            
            InitilizeViewModel(PlanView);
            InitilizeViewModel(ChartView);
        }

       private void InitilizeViewModel(object view)
        {
            FrameworkElement element = view as FrameworkElement;
            if (element == null)
                return;

            ITabViewModel vm = element.DataContext as ITabViewModel;
            if (vm == null)
                return;

            if (vm.State != ViewModelState.Loaded)
            {
                vm.LoadingCompleted += new EventHandler(vm_LoadingCompleted);
            }
            else
            {
                LoadingCompleted(this, EventArgs.Empty);
            }
        }

        private void RefreshViewModels(object view, IUnit unit)
        {
            FrameworkElement element = view as FrameworkElement;
            if (element == null)
                return;

            ITabViewModel vm = element.DataContext as ITabViewModel;
            if (vm == null)
                return;
            
            //if(vm.GetType())

            vm.Refresh(unit);
        }

        private void RefreshUnitView(object view)
        {
            FrameworkElement element = view as FrameworkElement;
            if (element == null)
                return;

            IUnitView vm = element.DataContext as IUnitView;
            if (vm == null)
                return;

            vm.LoadUnits(PreviewUnits);
        }

        void vm_LoadingCompleted(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public event EventHandler LoadingCompleted = delegate { };

        #region ITabViewModel Members


        public void Refresh(IUnit unit)
        {
            IsChartVisible = unit.IsMonitored;
            LoadService.PreviewForUnitGetAsync(unit.Identity, BloodyUser.Current.ID);

            RefreshViewModels(PlanView, unit);
            if (IsChartVisible) RefreshViewModels(ChartView, unit);

        }
        #endregion

        #region ITabViewModel Members
        private ViewModelState _state = ViewModelState.Loaded;
        public ViewModelState State
        {
            get { return _state; }
        }
        #endregion

        public string Title
        {
            get { return ""; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }
    }
}
