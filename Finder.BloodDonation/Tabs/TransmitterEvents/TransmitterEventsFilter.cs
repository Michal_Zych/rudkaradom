using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.TransmitterEvents
{
    public class TransmitterEventsFilter : SortedFilter
    {
       [RaisePropertyChanged]
public string Id { get; set; }

[RaisePropertyChanged]
public DateTime? DateUtcFrom { get; set; }
[RaisePropertyChanged]
public DateTime? DateUtcTo { get; set; }

[RaisePropertyChanged]
public string TypeDescription { get; set; }

[RaisePropertyChanged]
public string SeverityDescription { get; set; }

[RaisePropertyChanged]
public string UnitName { get; set; }

[RaisePropertyChanged]
public string UnitLocation { get; set; }

[RaisePropertyChanged]
public string ObjectTypeDescription { get; set; }

[RaisePropertyChanged]
public string ObjectUnitLocation { get; set; }

[RaisePropertyChanged]
public DateTime? StoreDateUtcFrom { get; set; }
[RaisePropertyChanged]
public DateTime? StoreDateUtcTo { get; set; }

[RaisePropertyChanged]
public DateTime? EndDateUtcFrom { get; set; }
[RaisePropertyChanged]
public DateTime? EndDateUtcTo { get; set; }

[RaisePropertyChanged]
public string ClosingUserName { get; set; }

[RaisePropertyChanged]
public string ClosingUserLastName { get; set; }

[RaisePropertyChanged]
public string ClosingComment { get; set; }

[RaisePropertyChanged]
public string SenderName { get; set; }

[RaisePropertyChanged]
public string SenderLastName { get; set; }

[RaisePropertyChanged]
public string Message { get; set; }

[RaisePropertyChanged]
public string Reason { get; set; }



        public TransmitterEventsFilter(IUnityContainer container)
            : base(container)
        {

        }
    }
}
