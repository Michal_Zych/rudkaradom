using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.TransmitterEvents
{
    public class TransmitterEventsRemotePagedDataSource: IPagedDataSource<TransmitterEventsItemViewModel>
    {
        protected IUnityContainer _container;
        protected int _pageSize;
        protected IDataTable _dataTable;
        protected IUnitDataClient _dataClient;
        protected int _transmitterId;
        protected int _severityFrom;
        protected int _severityTo;
        protected const int TRANSMITTER_TYPE_ID = 3;

        public TransmitterEventsRemotePagedDataSource(IUnityContainer container, int pageSize, IDataTable dataTable, IUnitDataClient dataClient, int transmitterId, int severityFrom, int severityTo)
        {
            _container = container;
            _pageSize = pageSize;
            _dataTable = dataTable;
            _dataClient = dataClient;
            _transmitterId = transmitterId;
            _severityFrom = severityFrom;
            _severityTo = severityTo;
        }

		 public void FetchData(int pageNumber, Action<PagedDataResponse<TransmitterEventsItemViewModel>> responseCallback)
        {
            var proxy = _dataClient.GetProxy();
            var list = new List<TransmitterEventsItemViewModel>();

            proxy.GetTransmitterEventsCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<TransmitterEventsItemViewModel>();
                    vm.Item = item;
                    vm.DataTable = _dataTable;
                    list.Add(vm);
                }

                Dictionary<string, string> filteredDictionary = (Dictionary<string, string>)_dataTable.GetFilterDictionary();
                filteredDictionary["ObjectIdFrom"] = _transmitterId.ToString();
                filteredDictionary["ObjectIdTo"] = _transmitterId.ToString();
                filteredDictionary["ObjectTypeFrom"] = TRANSMITTER_TYPE_ID.ToString();
                filteredDictionary["SeverityFrom"] = _severityFrom.ToString();
                filteredDictionary["SeverityTo"] = _severityTo.ToString();
                filteredDictionary["ObjectTypeTo"] = TRANSMITTER_TYPE_ID.ToString();

                _container.Resolve<TransmitterEventsItemViewModel>().SelectionChanged(-1);
                proxy.GetTransmitterEventsFilteredTotalCountAsync(filteredDictionary);
            };

            proxy.GetTransmitterEventsFilteredTotalCountCompleted += (s, e) =>
            {
                responseCallback(new PagedDataResponse<TransmitterEventsItemViewModel>()
                {
                    Items = list,
                    TotalItemCount = e.Result
                });
            };

            Dictionary<string, string> fD = (Dictionary<string, string>)_dataTable.GetFilterDictionary();
            fD["ObjectIdFrom"] = _transmitterId.ToString();
            fD["ObjectIdTo"] = _transmitterId.ToString();
            fD["ObjectTypeFrom"] = TRANSMITTER_TYPE_ID.ToString();
            fD["ObjectTypeTo"] = TRANSMITTER_TYPE_ID.ToString();
            fD["SeverityFrom"] = _severityFrom.ToString();
            fD["SeverityTo"] = _severityTo.ToString();
            proxy.GetTransmitterEventsAsync(fD, pageNumber, _pageSize);
            
        }
    }
}
