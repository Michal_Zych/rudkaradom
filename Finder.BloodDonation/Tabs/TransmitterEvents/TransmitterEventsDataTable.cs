using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataTable;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.Common.Authentication;
using Finder.BloodDonation.Model.Authentication;

namespace Finder.BloodDonation.Tabs.TransmitterEvents
{
    public class TransmitterEventsDataTable : BaseDataTable
    {

        public TransmitterEventsDataTable(IUnityContainer container)
            : base(container)
        {
        }

        public override ObservableCollection<IDataTableColumn> GetColumns()
        {
		
			bool isHiddenForUser = !BloodyUser.Current.IsInRole(PermissionNames.HyperAdmin);

            return new ObservableCollection<IDataTableColumn>()
             .Add(50)
             .Add(100, "Id", "Id", FilterDataType.Int, TextAlignment.Left, isHiddenForUser)
             .Add(100, "DateUtc", "Data zapisu", FilterDataType.DateTime, TextAlignment.Left)
             .Add(100, "TypeDescription", "Zdarzenie", FilterDataType.String, TextAlignment.Left)
             .Add(100, "SeverityDescription", "Waga", FilterDataType.String, TextAlignment.Left)
             .Add(100, "UnitName", "Nazwa jednostko organizacyjnej", FilterDataType.String, TextAlignment.Left)
             .Add(100, "UnitLocation", "Lokalizacja jednostki organizacyjnej", FilterDataType.String, TextAlignment.Left)
             .Add(100, "ObjectTypeDescription", "ObjectTypeDescription", FilterDataType.String, TextAlignment.Left)
             .Add(100, "ObjectUnitLocation", "ObjectUnitLocation", FilterDataType.String, TextAlignment.Left)
             .Add(100, "StoreDateUtc", "StoreDateUtc", FilterDataType.DateTime, TextAlignment.Left, true)
             .Add(100, "EndDateUtc", "Data zakończenia", FilterDataType.DateTime, TextAlignment.Left)
             .Add(100, "ClosingUserName", "Imię zamykającego", FilterDataType.String, TextAlignment.Left)
             .Add(100, "ClosingUserLastName", "Nazwisko zamykającego", FilterDataType.String, TextAlignment.Left)
             .Add(100, "ClosingComment", "Komentarz", FilterDataType.String, TextAlignment.Left)
             .Add(100, "SenderName", "Imię wysyłającego", FilterDataType.String, TextAlignment.Left)
             .Add(100, "SenderLastName", "Nazwisko wysyłającego", FilterDataType.String, TextAlignment.Left)
             .Add(100, "Message", "Wiadomość", FilterDataType.String, TextAlignment.Left)
             .Add(100, "Reason", "Przyczyna", FilterDataType.String, TextAlignment.Left);
        }


        public override ISortedFilter GetFilter(IUnityContainer container)
        {
            return new TransmitterEventsFilter(container);
        }

		public override bool StartWithDefaultColumns
        {
            get
            {
                return false;
            }
        }
    }
}
