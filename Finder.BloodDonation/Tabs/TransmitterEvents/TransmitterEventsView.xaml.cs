using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Tools;
using System.Windows.Data;
using Telerik.Windows.Controls;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Dialogs.Patients;
using Microsoft.Practices.Composite.Events;
using Finder.BloodDonation.Common.Events;
using Microsoft.Practices.Composite.Presentation.Events;
using Finder.BloodDonation.Common.Layout;
using Finder.BloodDonation.Dialogs.Transmitters;
using Finder.BloodDonation.Behaviors;

namespace Finder.BloodDonation.Tabs.TransmitterEvents
{
    [ViewModel(typeof(TransmitterEventsViewModel))]
    public partial class TransmitterEventsView : UserControl, IDataContextChangedHandler<TransmitterEventsView>
    {
        public TransmitterEventsView()
        {
            InitializeComponent();
        }

        public void DataContextChangedm(TransmitterEventsView sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                var vm = (TransmitterEventsViewModel)e.NewValue;

                if (vm != null)
                {
                    CommandManager.SetCommandBindings(this, vm.GetCommandBindings());
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ((Button)sender).Style = Application.Current.Resources["TabButtonSelected"] as Style;
        }
    }
}
