using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Telerik.Windows.Controls;
using Finder.BloodDonation.Behaviors;
using System.Windows.Printing;
using System.Windows.Controls.Primitives;
using Finder.BloodDonation.Tools;
using System.Windows.Data;

namespace Finder.BloodDonation.Tabs.AlarmDetails
{
    [ViewModel(typeof(AlarmDetailsDetailsListViewModel))]
    public partial class AlarmDetailsDetailsListView : UserControl, IDataContextChangedHandler<AlarmDetailsDetailsListView>
    {
        public AlarmDetailsDetailsListView()
        {
            InitializeComponent();
            DataContextChangedHelper<AlarmDetailsDetailsListView>.Bind(this);

            DetailsList.LayoutUpdated += RegisterScrollsAfterLayoutUpdated;
        }

        private void RegisterScrollsAfterLayoutUpdated(object sender, EventArgs e)
        {
            var synchroGroup = this.GetType().Name;


            if (ScrollViewersSynchronizer.IsGroupRegistered(synchroGroup))
            {
                //throw new Exception("Powtórzona grupa synchronizacji suwaków: " + synchroGroup);
            }
            ScrollViewersSynchronizer.Register(MainScroll, synchroGroup);
            ScrollViewersSynchronizer.Register(Filters, synchroGroup);
            ScrollViewersSynchronizer.Register(headerScroller, synchroGroup);
            ScrollViewersSynchronizer.Register(DetailsList, synchroGroup);

            DetailsList.LayoutUpdated -= RegisterScrollsAfterLayoutUpdated;
        }

        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            BindingExpression binding = null;
            if (sender is TextBox)
            {
                binding = (sender as TextBox).GetBindingExpression(TextBox.TextProperty);
            }
            else if (sender is RadDateTimePicker)
            {
                binding = (sender as RadDateTimePicker).GetBindingExpression(RadDateTimePicker.SelectedValueProperty);
            }
            if (binding != null)
            {
                binding.UpdateSource();
            }
            base.OnKeyUp(e);
        }


        public void DataContextChangedm(AlarmDetailsDetailsListView sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                var vm = e.NewValue as AlarmDetailsDetailsListViewModel;

                CommandManager.SetCommandBindings(this, vm.GetCommandBindings());
            }
        }
    }
}
