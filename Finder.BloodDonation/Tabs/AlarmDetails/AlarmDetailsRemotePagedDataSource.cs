using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.AlarmDetails
{
    public class AlarmDetailsRemotePagedDataSource: IPagedDataSource<AlarmDetailsItemViewModel>
    {
        protected IUnityContainer _container;
        protected int _pageSize;
        protected IDataTable _dataTable;
        protected IUnitDataClient _dataClient;

        public AlarmDetailsRemotePagedDataSource(IUnityContainer container, int pageSize, IDataTable dataTable, IUnitDataClient dataClient)
        {
            _container = container;
            _pageSize = pageSize;
            _dataTable = dataTable;
            _dataClient = dataClient;
        }


		 public void FetchData(int pageNumber, Action<PagedDataResponse<AlarmDetailsItemViewModel>> responseCallback)
        {
            var proxy = _dataClient.GetProxy();
            var list = new List<AlarmDetailsItemViewModel>();
            const string ALARM_SEVERITY = "2";
            const string OPEN = "0";
            const int PAGE_SIZE = 20;

             /*
            proxy.GetAlarmDetailsFilteredCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<AlarmDetailsItemViewModel>();
                    vm.Item = item;
                    vm.DataTable = _dataTable;
                    list.Add(vm);
                }
                _container.Resolve<AlarmDetailsItemViewModel>().SelectionChanged(-1);
                proxy.GetAlarmDetailsFilteredTotalCountAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary());
            };

            proxy.GetAlarmDetailsFilteredTotalCountCompleted += (s, e) =>
            {
                responseCallback(new PagedDataResponse<AlarmDetailsItemViewModel>()
                {
                    Items = list,
                    TotalItemCount = e.Result
                });
            };

            proxy.GetAlarmDetailsFilteredAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary(), pageNumber, _pageSize);
              */
            proxy.GetEventsDetailsCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<AlarmDetailsItemViewModel>();
                    vm.Item = item;
                    vm.DataTable = _dataTable;
                    list.Add(vm);
                }
                _container.Resolve<AlarmDetailsItemViewModel>().SelectionChanged(-1);
                Dictionary<string, string> filterDictionary = (Dictionary<string, string>)_dataTable.GetFilterDictionary();
                filterDictionary["SeverityFrom"] = ALARM_SEVERITY;
                filterDictionary["SeverityTo"] = ALARM_SEVERITY;
                filterDictionary["IsClosedFrom"] = OPEN;
                filterDictionary["IsClosedTo"] = OPEN;
                proxy.GetEventsDetailsFilteredTotalCountAsync(filterDictionary);
            };

            proxy.GetEventsDetailsFilteredTotalCountCompleted += (s, e) =>
            {
                responseCallback(new PagedDataResponse<AlarmDetailsItemViewModel>()
                {
                    Items = list,
                    TotalItemCount = e.Result
                });
            };

            Dictionary<string, string> fdict = (Dictionary<string, string>)_dataTable.GetFilterDictionary();
            fdict["SeverityFrom"] = ALARM_SEVERITY;
            fdict["SeverityTo"] = ALARM_SEVERITY;
            fdict["IsClosedFrom"] = OPEN;
            fdict["IsClosedTo"] = OPEN;
            proxy.GetEventsDetailsAsync(fdict, pageNumber, PAGE_SIZE);
            
            
        }
    }
}
