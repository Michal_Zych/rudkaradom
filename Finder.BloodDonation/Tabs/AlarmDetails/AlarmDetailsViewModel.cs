using System;
using System.Net;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using FinderFX.SL.Core.MVVM;
using Finder.BloodDonation.Common.Layout;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Presentation.Commands;
using Finder.BloodDonation.Model.Dto;
using System.Collections.ObjectModel;
using Finder.BloodDonation.AlarmsService;
using FinderFX.SL.Core.Communication;
using Finder.BloodDonation.Common;
using Finder.BloodDonation.Layout;
using Finder.BloodDonation.Common.Events;
using System.Collections.Generic;
using System.Collections;
using Finder.BloodDonation.Tools;
using Global = Finder.BloodDonation.Settings;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Settings;
using Finder.BloodDonation.Dialogs.Patients;
using Finder.BloodDonation.Dialogs.Helpers;
using Microsoft.Practices.Composite.Presentation.Events;
using System.Diagnostics;
using GalaSoft.MvvmLight.Command;
using Finder.BloodDonation.Dialogs.Bands;
using Finder.BloodDonation.UnitsService;
using Finder.BloodDonation.Model.Enum;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.UsersService;
using Finder.BloodDonation.LanguageHelpher;

namespace Finder.BloodDonation.Tabs.AlarmDetails
{
    [OnCompleted]
    [UIException]
    public class AlarmDetailsViewModel : ViewModelBase, ITabViewModel
    {
        private readonly IWindowsManager _windowsManager;

        public RelayCommand CloseSelectedAlarm { get; set; }

        public event EventHandler LoadingCompleted = delegate { };
        private IUnit SelectedUnit = null;
        private IUnit _currentUnit;

        private EventsDetailsDto selectedItem;

        private UsersServiceClient Proxy { get; set; }
        private UnitsServiceClient UnitsProxy { get; set; }

        private IUnitFiltered _details;
        [RaisePropertyChanged]
        public IUnitFiltered Details
        {
            get
            {
                if (_details == null)
                {
                    _details = Container.Resolve<AlarmDetailsDetailsListViewModel>();
                }
                return _details;
            }
            set
            {
                _details = value;
            }
        }

        public AlarmDetailsViewModel(IUnityContainer container)
            : base(container)
        {
            this.ViewAttached += new EventHandler(ViewModel_ViewAttached);
            EventAggregator.GetEvent<SelectedItemChangedEvent<EventsDetailsDto>>().Subscribe(OnSelectedItemChangedEvent);

            var viewFactory = container.Resolve<ViewFactory>();
            _windowsManager = this.Container.Resolve<IWindowsManager>();
            //_windowsManager.RegisterWindow(WindowsKeys. , "", viewFactory.CreateView<>(), new Size(290, 400), true);
            CloseSelectedAlarm = new RelayCommand(OnCloseSelectedAlarm);

            Proxy = ServiceFactory.GetService<UsersServiceClient>(/*ServicesUri.UsersService*/);
            UnitsProxy = ServiceFactory.GetService<UnitsServiceClient>();
            UnitsProxy.CloseAlarmsCompleted += UnitsProxy_CloseAlarmsCompleted;
        }

        void UnitsProxy_CloseAlarmsCompleted(object sender, UnitsService.CloseAlarmsCompletedEventArgs e)
        {
            MsgBox.Succes("Alarm zamkni�to pomy�lnie!");
            Refresh(null);
        }

        public void OnCloseSelectedAlarm()
        {
            if(selectedItem == null)
            {
                MsgBox.Error(LanguageManager.GetError(ErrorCodes.NON_SELLECTION_ALARM));
                return;
            }
            else
            {
                UnitsProxy.CloseAlarmsAsync(selectedItem);
                // Closing method from service!.
            }
        }


        public void OnSelectedItemChangedEvent(IItemViewModel<EventsDetailsDto> obj)
        {
            if(obj == null)
            {
                selectedItem = null;
            }
            else
            {
                selectedItem = obj.Item;
            }
            SelectedItemChanged();
        }

        private void SelectedItemChanged()
        {

        }
        
        private void ViewModel_ViewAttached(object sender, EventArgs e)
        {
            LoadingCompleted(this, EventArgs.Empty);
        }

        public void Refresh(IUnit unit)
        {
            SelectedUnit = unit;
            _currentUnit = unit;

            Details.Refresh(unit);
		}
        
        public ViewModelState State
        {
            get { return ViewModelState.Loaded; }
        }

        public string Title
        {
            get { return ""; }
        }

        public bool ExitButtonVisible
        {
            get { return false; }
        }


        private void CloseWindowEventHandling(string key)
        {
            EventAggregator.GetEvent<CloseWindowEvent>().Unsubscribe(CloseWindowEventHandling);

            /*
            if(key.Equals(WindowsKeys.))
            {
                if (_EditData.Canceled)
                {

                }
                else
                {
                    Edited(_EditData);
                }
            }
             */
        }
        /*
        private void Edited(EditData _EditData)
        {
            // zapis do bazy danych.
        }
        */

    }
}
