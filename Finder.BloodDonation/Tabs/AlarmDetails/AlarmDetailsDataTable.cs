using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Finder.BloodDonation.DataTable;
using Microsoft.Practices.Unity;
using Finder.BloodDonation.Common.Filters;

namespace Finder.BloodDonation.Tabs.AlarmDetails
{
    public class AlarmDetailsDataTable : BaseDataTable
    {

        public AlarmDetailsDataTable(IUnityContainer container)
            : base(container)
        {
        }

        public override ObservableCollection<IDataTableColumn> GetColumns()
        {
            return new ObservableCollection<IDataTableColumn>()
             .Add(50)
			 .Add(100, "Id", "Id", FilterDataType.Int)
.Add(100, "Date", "Date", FilterDataType.DateTime)
.Add(100, "TypeDescription", "TypeDescription", FilterDataType.String)
.Add(100, "SeverityDescription", "SeverityDescription", FilterDataType.String)
.Add(100, "UnitName", "UnitName", FilterDataType.String)
.Add(100, "UnitLocation", "UnitLocation", FilterDataType.String)
.Add(100, "ObjectTypeDescription", "ObjectTypeDescription", FilterDataType.String)
.Add(100, "ObjectUnitLocation", "ObjectUnitLocation", FilterDataType.String)
.Add(100, "StoreDate", "StoreDate", FilterDataType.DateTime)
.Add(100, "EndDate", "EndDate", FilterDataType.DateTime)
.Add(100, "ClosingUserName", "ClosingUserName", FilterDataType.String)
.Add(100, "ClosingUserLastName", "ClosingUserLastName", FilterDataType.String)
.Add(100, "ClosingComment", "ClosingComment", FilterDataType.String)
.Add(100, "SenderName", "SenderName", FilterDataType.String)
.Add(100, "SenderLastName", "SenderLastName", FilterDataType.String)
.Add(100, "Message", "Message", FilterDataType.String)
.Add(100, "Reason", "Reason", FilterDataType.String)
             ;
        }


        public override ISortedFilter GetFilter(IUnityContainer container)
        {
            return new AlarmDetailsFilter(container);
        }

		public override bool StartWithDefaultColumns
        {
            get
            {
                return false;
            }
        }
    }
}
