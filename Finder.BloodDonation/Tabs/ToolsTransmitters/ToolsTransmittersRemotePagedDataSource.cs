using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataManagers;
using Finder.BloodDonation.DynamicData.Paging;
using Finder.BloodDonation.DynamicData.Remote;
using Finder.BloodDonation.Filters;
using Finder.BloodDonation.Model.Dto;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.ToolsTransmitters
{
    public class ToolsTransmittersRemotePagedDataSource: IPagedDataSource<ToolsTransmittersItemViewModel>
    {
        protected IUnityContainer _container;
        protected int _pageSize;
        protected IDataTable _dataTable;
        protected IUnitDataClient _dataClient;

        public ToolsTransmittersRemotePagedDataSource(IUnityContainer container, int pageSize, IDataTable dataTable, IUnitDataClient dataClient)
        {
            _container = container;
            _pageSize = pageSize;
            _dataTable = dataTable;
            _dataClient = dataClient;
        }


		 public void FetchData(int pageNumber, Action<PagedDataResponse<ToolsTransmittersItemViewModel>> responseCallback)
        {
            var proxy = _dataClient.GetProxy();
            var list = new List<ToolsTransmittersItemViewModel>();

            proxy.GetTransmitterDetailsFilteredCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<ToolsTransmittersItemViewModel>();
                    vm.Item = item;
                    vm.DataTable = _dataTable;
                    list.Add(vm);
                }
                _container.Resolve<ToolsTransmittersItemViewModel>().SelectionChanged(-1);
                proxy.GetTransmitterDetailsFilteredTotalCountAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary());
            };

            proxy.GetTransmitterDetailsFilteredTotalCountCompleted += (s, e) =>
            {
                responseCallback(new PagedDataResponse<ToolsTransmittersItemViewModel>()
                {
                    Items = list,
                    TotalItemCount = e.Result
                });
            };

            proxy.GetTransmitterDetailsFilteredAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary(), pageNumber, _pageSize);
            

            /*
            proxy.GetToolsTransmittersFilteredCompleted += (s, e) =>
            {
                foreach (var item in e.Result)
                {
                    var vm = _container.Resolve<ToolsTransmittersItemViewModel>();
                    vm.Item = item;
                    vm.DataTable = _dataTable;
                    list.Add(vm);
                }
                _container.Resolve<ToolsTransmittersItemViewModel>().SelectionChanged(-1);
                proxy.GetToolsTransmittersFilteredTotalCountAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary());
            };

            proxy.GetToolsTransmittersFilteredTotalCountCompleted += (s, e) =>
            {
                responseCallback(new PagedDataResponse<ToolsTransmittersItemViewModel>()
                {
                    Items = list,
                    TotalItemCount = e.Result
                });
            };

            proxy.GetToolsTransmittersFilteredAsync((Dictionary<string, string>)_dataTable.GetFilterDictionary(), pageNumber, _pageSize);
            */
        }
    }
}
