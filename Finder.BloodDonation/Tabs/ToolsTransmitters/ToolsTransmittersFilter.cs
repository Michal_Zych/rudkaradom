using Finder.BloodDonation.Common.Filters;
using Finder.BloodDonation.DataTable;
using Finder.BloodDonation.Filters;
using FinderFX.SL.Core.MVVM;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Tabs.ToolsTransmitters
{
    public class ToolsTransmittersFilter : SortedFilter
    {
       [RaisePropertyChanged]
public string Id { get; set; }

[RaisePropertyChanged]
public string Mac { get; set; }

[RaisePropertyChanged]
public string MacString { get; set; }

[RaisePropertyChanged]
public string ClientId { get; set; }

[RaisePropertyChanged]
public string Name { get; set; }

[RaisePropertyChanged]
public string InstallationUnitId { get; set; }

[RaisePropertyChanged]
public string InstallationUnitName { get; set; }

[RaisePropertyChanged]
public string BarCode { get; set; }

[RaisePropertyChanged]
public string TypeId { get; set; }

[RaisePropertyChanged]
public string ReportIntervalSec { get; set; }

[RaisePropertyChanged]
public string Sensitivity { get; set; }

[RaisePropertyChanged]
public string AvgCalcTimeS { get; set; }

[RaisePropertyChanged]
public string Msisdn { get; set; }

[RaisePropertyChanged]
public BoolFilterValue IsBandUpdater { get; set; }

[RaisePropertyChanged]
public string Description { get; set; }

[RaisePropertyChanged]
public string RssiTreshold { get; set; }

[RaisePropertyChanged]
public string Group { get; set; }

[RaisePropertyChanged]
public bool? IsConnected { get; set; }



        public ToolsTransmittersFilter(IUnityContainer container)
            : base(container)
        {

        }
    }
}
