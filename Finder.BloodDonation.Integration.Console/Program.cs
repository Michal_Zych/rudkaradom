﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Finder.BloodDonation.Integration.Console
{
	class Program
	{
		static void Main(string[] args)
		{
			log4net.Config.XmlConfigurator.Configure(new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "logging.config"));

			System.Console.WriteLine("starting application");

			var controller = new Controller(new ControllerConfiguration());

			//controller.SynchronizeManually();
			controller.Start();

			System.Console.ReadKey();
		}
	}
}
