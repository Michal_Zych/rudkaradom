﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using m2mExportService.Control;

namespace m2mExportServiceConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            IController c = new Controller();

            c.Start();

            Console.ReadKey();
        }
    }
}
