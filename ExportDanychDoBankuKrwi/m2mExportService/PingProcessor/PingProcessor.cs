﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finder.Communications.Core.Async;
using Finder.Communications.Core.Logging;

namespace m2mExportService.PingProcessor
{
    public class PingProcessor : BackgroundPulseOperation
    {
        private IPingDataAccess _dataAccess;
        private volatile bool _operate;

        public PingProcessor(IPingDataAccess dataAccess)
        {
            _dataAccess = dataAccess;

            _operate = false;
        }


        public void Start()
        {
            Start(Configuration.Instance.PingIntervalSec * 1000, true);
        }

        public override void Stop()
        {
            base.Stop();
        }

        protected override void Operate()
        {
            if (_operate) return;

            _operate = true;

            int d = _dataAccess.PingDatabase();
            if ((d> 0) && (d != Configuration.Instance.PingIntervalSec))
            {
                Logger2.Instance.Log(LogLevel.Info, LogType2.Activity, "PingProcessor", "Odpowiedź (nowy interwał): " + d.ToString(), null);
                Configuration.Instance.PingIntervalSec = d;
            }
            _operate = false;
        }
    }

}
