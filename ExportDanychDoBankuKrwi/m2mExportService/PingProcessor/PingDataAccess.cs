﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finder.Communications.Core.DataAccess;
using Finder.Communications.Core.Logging;
using System.Data;

namespace m2mExportService.PingProcessor
{
    class PingDataAccess: IPingDataAccess
    {
        private const string _pingProc = "dbo.srv_ServicePing";
        private static bool Starting = true;

        public int PingDatabase()
        {
            int result;
            DataAccessContext dac;

            dac = DataAccessContextFactory.CreateQueryDataAccessContext(true, _pingProc);
            dac.AddQueryParameter("ClientID", Configuration.Instance.ClientID);
            dac.AddQueryParameter("ServiceTag", Configuration.Instance.ServiceTag);
            dac.AddQueryParameter("IsRestart", Starting.ToString());
            dac.Commit();

            result = ConvertToInt(dac.Results);
            Starting = false;

            return result;
        }

        private int ConvertToInt(DataSet dataset)
        {
            int result;

            if (dataset != null && dataset.Tables[0] != null && dataset.Tables[0].Rows.Count > 0)
            {
                var table = dataset.Tables[0];

                result = Convert.ToInt32(table.Rows[0][0]);
            }

            return 0;
        }
    }
}
