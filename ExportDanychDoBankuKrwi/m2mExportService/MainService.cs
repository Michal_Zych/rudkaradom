﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using m2mExportService.Control;

namespace m2mExportService
{
    public partial class MainService : ServiceBase
    {
        private IController _controller;

        public MainService()
        {
            InitializeComponent();
            _controller = new Controller();
        }

        protected override void OnStart(string[] args)
        {
            _controller.Start();
        }

        protected override void OnStop()
        {
            _controller.Stop();
        }
    }
}
