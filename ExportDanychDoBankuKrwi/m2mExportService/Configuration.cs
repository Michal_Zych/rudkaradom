﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using Finder.Communications.Core.Logging;

namespace m2mExportService
{
    class Configuration
    {
        private static Configuration _instance = new Configuration();
       
        public static Configuration Instance
        {
            get {return _instance;}
        }

        private Configuration()
        {
            Load();
        }

        static Configuration() { }



        public string ConfigVersion { get; private set; }
        public int PingIntervalSec { get; set; }


        public string ClientUserName { get; private set; }
        public string ClientPassword { get; private set; }

        public int ClientID { get; private set; }
        public int MaxRows { get; private set; }

        public string DateTimeFormat { get; private set; }
        public bool LogRequests { get; private set; }


        public string ServiceTag { get { return "Export"; } }
        
        private void Load()
        {
            try
            {
                ConfigVersion = ConfigurationManager.AppSettings["ConfigVersion"];


                ClientUserName = ConfigurationManager.AppSettings["ClientUserName"];
                ClientPassword = ConfigurationManager.AppSettings["ClientPassword"];

                ClientID = Int32.Parse(ConfigurationManager.AppSettings["ClientID"]);
                MaxRows = Int32.Parse(ConfigurationManager.AppSettings["MaxRows"]);

                DateTimeFormat = ConfigurationManager.AppSettings["DateTimeFormat"];

                LogRequests = Boolean.Parse(ConfigurationManager.AppSettings["LogRequests"]);

                PingIntervalSec = Int32.Parse(ConfigurationManager.AppSettings["PingIntervalSec"]);

            }
            catch(Exception e)
            {
                Logger2.Instance.Log(LogLevel.Fatal, LogType2.Activity, "Controller", "Failed initializing the service: " + e.ToString(), null);
                throw new ArgumentException("Invalid configruation: " + e.ToString());
            }
        }

    }
}
