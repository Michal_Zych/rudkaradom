﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using m2mExportService.ExportServer.DataAccess;
using m2mExportService.ExportServer.Entities;
using m2mExportService.ExportServer.Converters;
using Finder.Communications.Core.Logging;

namespace m2mExportService.ExportServer
{
    public class m2mMonitoring : Im2mMonitoring
    {
        private const string ERROR = "Błąd";
        private static IServerDataAccess DataAccess = null;

        public static void Initialize(IServerDataAccess dataAccess)
        {
            DataAccess = dataAccess;
        }

                
        public string JednostkiOrganizacyjne()
        {
            try
            {
                if (Configuration.Instance.LogRequests)
                {
                    Logger2.Instance.Log(LogLevel.Info, LogType2.Activity, "Units", "()", null);
                }
                return DataAccess.GetUnits(Configuration.Instance.ClientID).ToStringList();
            }
            catch (Exception e)
            {
                Logger2.Instance.Log(LogLevel.Error, LogType2.Activity, "Units", e.ToString(), null);
                return ERROR;
            }
        }

        public string Czujniki()
        {
            try
            {
                if (Configuration.Instance.LogRequests)
                {
                    Logger2.Instance.Log(LogLevel.Info, LogType2.Activity, "Sensors", "()", null);
                }
                return DataAccess.GetSensors(Configuration.Instance.ClientID).ToStringList();
            }
            catch (Exception e)
            {
                Logger2.Instance.Log(LogLevel.Error, LogType2.Activity, "Sensors", e.ToString(), null);
                return ERROR;
            }
        }

        public string Pomiary(int Czujnik, DateTime DataOd, DateTime DataDo)
        {
            try
            {
                if (Configuration.Instance.LogRequests)
                {
                    Logger2.Instance.Log(LogLevel.Info, LogType2.Activity, "Measurements", "(" + Czujnik.ToString() + "," + DataOd.ToString() + "," + DataDo.ToString() + ")", null);
                }
                return DataAccess.GetMeasurements(Configuration.Instance.ClientID, Configuration.Instance.MaxRows, Czujnik, DataOd, DataDo).ToStringList();
            }
            catch (Exception e)
            {
                Logger2.Instance.Log(LogLevel.Error, LogType2.Activity, "Measurements", e.ToString(), null);
                return ERROR;
            }
        }

        public string AktualnyPomiar(int Czujnik)
        {
            try
            {
                if (Configuration.Instance.LogRequests)
                {
                    Logger2.Instance.Log(LogLevel.Info, LogType2.Activity, "Measurement", "(" + Czujnik.ToString() + ")", null);
                }
                return DataAccess.GetMeasurement(Configuration.Instance.ClientID, Czujnik).ToString();
            }
            catch (Exception e)
            {
                Logger2.Instance.Log(LogLevel.Error, LogType2.Activity, "Measurement", e.ToString(), null);
                return ERROR;
            }
        }

        public string Alarmy(int Czujnik, DateTime DataOd, DateTime DataDo)
        {
            try
            {
                if (Configuration.Instance.LogRequests)
                {
                    Logger2.Instance.Log(LogLevel.Info, LogType2.Activity, "Alarms", "(" + Czujnik.ToString() + "," + DataOd.ToString() + "," + DataDo.ToString() + ")", null);
                }
                return DataAccess.GetAlarms(Configuration.Instance.ClientID, Configuration.Instance.MaxRows, Czujnik, DataOd, DataDo).ToStringList();
            }
            catch (Exception e)
            {
                Logger2.Instance.Log(LogLevel.Error, LogType2.Activity, "Alarms", e.ToString(), null);
                return ERROR;
            }
        }

        public string Uzytkownicy()
        {
            try
            {
                if (Configuration.Instance.LogRequests)
                {
                    Logger2.Instance.Log(LogLevel.Info, LogType2.Activity, "Users", "()", null);
                }
                return DataAccess.GetUsers(Configuration.Instance.ClientID).ToStringList();
            }
            catch (Exception e)
            {
                Logger2.Instance.Log(LogLevel.Error, LogType2.Activity, "Users", e.ToString(), null);
                return ERROR;
            }
        }

        public string DataCzas()
        {
            try
            {
                if (Configuration.Instance.LogRequests)
                {
                    Logger2.Instance.Log(LogLevel.Info, LogType2.Activity, "DateTime", "()", null);
                }
                return DateTimeConverter.ToClientFormat(DataAccess.GetDatabaseDateTime()).ToQuotedString();
            }
            catch (Exception e)
            {
                Logger2.Instance.Log(LogLevel.Error, LogType2.Activity, "DateTime", e.ToString(), null);
                return ERROR;
            }
        }

        public string Echo(string EchoText)
        {
            try
            {
                if (Configuration.Instance.LogRequests)
                {
                    Logger2.Instance.Log(LogLevel.Info, LogType2.Activity, "Echo", EchoText, null);
                }
                return EchoText;
            }
            catch (Exception e)
            {
                Logger2.Instance.Log(LogLevel.Error, LogType2.Activity, "Echo", e.ToString(), null);
                return ERROR;
            }
        }


    }
}
