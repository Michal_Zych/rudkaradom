﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using m2mExportService.ExportServer.Converters;


namespace m2mExportService.ExportServer.Entities
{
    public class UserEntity: Entity
    {
        public int ID { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }

        public void FillFromDataRow(DataRow row)
        {
            ID = Convert.ToInt32(row["ID"]);
            LastName = Convert.ToString(row["LastName"]);
            Name = Convert.ToString(row["Name"]);
        }

        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture,
                "{0};{1};{2}",
                ID
                , LastName.ToQuotedString()
                , Name.ToQuotedString()
                );
        }

    }
}
