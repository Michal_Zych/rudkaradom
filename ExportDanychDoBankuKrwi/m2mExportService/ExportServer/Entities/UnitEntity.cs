﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using m2mExportService.ExportServer.Converters;

namespace m2mExportService.ExportServer.Entities
{
    public class UnitEntity: Entity
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? ParentUnitID { get; set; }

        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture,
                "{0};{1};{2}",
                ID
                , Name.ToQuotedString()
                //, Description.ToQuotedString()
                , ParentUnitID
                );
        }

        public void FillFromDataRow(System.Data.DataRow row)
        {
            ID = Convert.ToInt32(row["ID"]);
            Name = Convert.ToString(row["Name"]);
            Description = Convert.ToString(row["Description"]);
            ParentUnitID = row["ParentUnitID"] as int?;
        }
    }
}
