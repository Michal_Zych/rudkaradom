﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using m2mExportService.ExportServer.Converters;

namespace m2mExportService.ExportServer.Entities
{
    public class MeasurementEntity : Entity
    {
        public DateTime? Date { get; set; }
        public double? Value { get; set; }

        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture,
                "{0};{1}",
                Date.HasValue ? Date.Value.ToString() : "",
                Value.HasValue ? Value.Value.ToString("N1") : ""
                );
        }

        public void FillFromDataRow(System.Data.DataRow row)
        {
            Date = row["Date"] as DateTime?;
            Value = (row["Value"] == DBNull.Value) ? (double?)null : Convert.ToDouble(row["Value"]);
        }
    }
}
