﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using m2mExportService.ExportServer.Converters;

namespace m2mExportService.ExportServer.Entities
{
    public class SensorEntity : Entity
    {
        public int ID { get; set; }
        public string MU { get; set; }
        public double Calibration { get; set; }
        public bool Enabled { get; set; }
        public string Socket { get; set; }
        public bool AlarmingEnabled { get; set; }
        public double LoRange { get; set; }
        public double UpRange { get; set; }



        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture,
                "{0};{1};{2:N1};{3};{4};{5};{6:N1};{7:N1}",
                ID
                , MU.ToQuotedString()
                , Calibration
                , Enabled.ToYesNoValue()
                , Socket.ToQuotedString()
                , AlarmingEnabled.ToYesNoValue()
                , LoRange
                , UpRange
                );
        }

        public void FillFromDataRow(System.Data.DataRow row)
        {
            ID = Convert.ToInt32(row["ID"]);
            MU = Convert.ToString(row["MU"]);
            Calibration = Convert.ToDouble(row["Calibration"]);
            Enabled = Convert.ToBoolean(row["Enabled"]);
            Socket = Convert.ToString(row["Socket"]);
            AlarmingEnabled = Convert.ToBoolean(row["AlarmingEnabled"]);
            LoRange = Convert.ToDouble(row["LoRange"]);
            UpRange = Convert.ToDouble(row["UpRange"]);
        }
    }
}
