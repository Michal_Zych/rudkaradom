﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using m2mExportService.ExportServer.Converters;

namespace m2mExportService.ExportServer.Entities
{
    public class AlarmEntity : Entity
    {
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public double? LoRange { get; set; }
        public double? UpRange { get; set; }
        public double? Value { get; set; }
        public string UserStatus { get; set; }
        public DateTime? DateStatus { get; set; }
        public string CommentStatus { get; set; }

        public override string ToString()
        {
            return String.Format(CultureInfo.InvariantCulture,
                "{0};{1};{2};{3};{4};{5};{6};{7}",
                DateStart.HasValue ? DateStart.Value.ToString() : "",
                DateEnd.HasValue ? DateEnd.Value.ToString() : "",
                LoRange.HasValue ?LoRange.Value.ToString("N1"):"",
                UpRange.HasValue? UpRange.Value.ToString("N1"): "",
                Value.HasValue ? Value.Value.ToString("N1") : "",
                UserStatus,
                DateStatus.HasValue ? DateStatus.Value.ToString() : "",
                CommentStatus
                );
        }

        public void FillFromDataRow(System.Data.DataRow row)
        {
            DateStart = (row["DateStart"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(row["DateStart"]);
            DateEnd = (row["DateEnd"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(row["DateEnd"]);
            LoRange = (row["LoRange"] == DBNull.Value) ? (double?)null : Convert.ToDouble(row["LoRange"]);
            UpRange = (row["UpRange"] == DBNull.Value) ? (double?)null : Convert.ToDouble(row["UpRange"]);
            Value = (row["Value"] == DBNull.Value) ? (double?)null : Convert.ToDouble(row["Value"]);
            UserStatus = Convert.ToString(row["UserStatus"]);
            DateStatus = (row["DateStatus"] == DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(row["DateStatus"]);
            CommentStatus = Convert.ToString(row["CommentStatus"]);
        }
    }
}