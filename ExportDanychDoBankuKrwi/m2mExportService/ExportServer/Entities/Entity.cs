﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace m2mExportService.ExportServer.Entities
{
    public interface Entity
    {
        void FillFromDataRow(DataRow row);
    }
}
