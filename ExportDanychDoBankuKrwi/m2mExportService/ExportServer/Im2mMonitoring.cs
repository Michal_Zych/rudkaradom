﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace m2mExportService.ExportServer
{
    [ServiceContract]
    public interface Im2mMonitoring
    {
        [OperationContract]
        string DataCzas();

        [OperationContract]
        string Echo(string EchoText);

        [OperationContract]
        string Uzytkownicy();

        [OperationContract]
        string JednostkiOrganizacyjne();

        [OperationContract]
        string Czujniki();

        [OperationContract]
        string Pomiary(int Czujnik, DateTime DataOd, DateTime DataDo);

        [OperationContract]
        string AktualnyPomiar(int Czujnik);

        [OperationContract]
        string Alarmy(int Czujnik, DateTime DataOd, DateTime DataDo);
    }
}
