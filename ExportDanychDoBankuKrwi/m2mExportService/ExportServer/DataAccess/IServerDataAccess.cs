﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using m2mExportService.ExportServer.Entities;

namespace m2mExportService.ExportServer.DataAccess
{
    public interface IServerDataAccess
    {
        IList<UserEntity> GetUsers(int ClientID);

        IList<UnitEntity> GetUnits(int ClientID);

        IList<SensorEntity> GetSensors(int ClientID);

        IList<MeasurementEntity> GetMeasurements(int ClientID, int MaxRows, int Unit, DateTime DateFrom, DateTime DateTo);

        MeasurementEntity GetMeasurement(int ClientID, int Unit);

        IList<AlarmEntity> GetAlarms(int ClinetID, int MaxRows, int Unit, DateTime DateFrom, DateTime DateTo);

        DateTime GetDatabaseDateTime();
        
    }
}
