﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Finder.Communications.Core.DataAccess;
using m2mExportService.ExportServer.Entities;
using m2mExportService.ExportServer.Converters;

namespace m2mExportService.ExportServer.DataAccess
{
    public class ExportServerDataAccess : IServerDataAccess
    {
        private const string GetUsersProc = "exp_GetUsers";
        public IList<UserEntity> GetUsers(int ClientID)
        {
            DataAccessContext dac = DataAccessContextFactory.CreateQueryDataAccessContext(true, GetUsersProc);
            dac.AddQueryParameter("clientID", ClientID);

            dac.Commit();

            return ConvertToList<UserEntity>(dac.Results);
        }

        private const string GetUnitsProc = "exp_GetUnits";
        public IList<UnitEntity> GetUnits(int ClientID)
        {
            DataAccessContext dac = DataAccessContextFactory.CreateQueryDataAccessContext(true, GetUnitsProc);
            dac.AddQueryParameter("clientID", ClientID);

            dac.Commit();

            return ConvertToList<UnitEntity>(dac.Results);
        }

        private const string GetSensorsProc = "exp_GetSensors";
        public IList<SensorEntity> GetSensors(int ClientID)
        {
            DataAccessContext dac = DataAccessContextFactory.CreateQueryDataAccessContext(true, GetSensorsProc);
            dac.AddQueryParameter("clientID", ClientID);

            dac.Commit();

            return ConvertToList<SensorEntity>(dac.Results);
        }

        private const string GetMeasurementsProc = "exp_GetMeasurements";
        public IList<MeasurementEntity> GetMeasurements(int ClientID, int MaxRows, int Unit, DateTime DateFrom, DateTime DateTo)
        {
            DataAccessContext dac = DataAccessContextFactory.CreateQueryDataAccessContext(true, GetMeasurementsProc);
            dac.AddQueryParameter("ClientID", ClientID);
            dac.AddQueryParameter("MaxRows", MaxRows);
            dac.AddQueryParameter("UnitID", Unit);
            dac.AddQueryParameter("DateFrom", DateFrom);
            dac.AddQueryParameter("DateTo", DateTo);
            dac.Commit();

            return ConvertToList<MeasurementEntity>(dac.Results);
        }


        private const string GetMeasurementProc = "exp_GetActualMeasurement";
        public MeasurementEntity GetMeasurement(int ClientID, int Unit)
        {
            DataAccessContext dac = DataAccessContextFactory.CreateQueryDataAccessContext(true, GetMeasurementProc);
            dac.AddQueryParameter("ClientID", ClientID);
            dac.AddQueryParameter("UnitID", Unit);
            dac.Commit();

            return GetFirstItem(ConvertToList<MeasurementEntity>(dac.Results));
        }

        private const string GetAlarmsProc  ="exp_GetAlarms";
        public IList<AlarmEntity> GetAlarms(int ClientID, int MaxRows, int Unit, DateTime DateFrom, DateTime DateTo)
        {
            DataAccessContext dac = DataAccessContextFactory.CreateQueryDataAccessContext(true, GetAlarmsProc);
            dac.AddQueryParameter("ClientID", ClientID);
            dac.AddQueryParameter("MaxRows", MaxRows);
            dac.AddQueryParameter("UnitID", Unit);
            dac.AddQueryParameter("DateFrom", DateFrom);
            dac.AddQueryParameter("DateTo", DateTo);
            dac.Commit();

            return ConvertToList<AlarmEntity>(dac.Results);
        }

        
        private T GetFirstItem<T>(IList<T> list) where T: class, new()
        {
            if (list == null || list.Count == 0)
                return new T();
            else
                return list[0];
        }

        private IList<T> ConvertToList<T>(DataSet dataSet) where T: Entity, new()
        {
            var list = new List<T>();
            T item;

            if (dataSet != null && dataSet.Tables[0] != null && dataSet.Tables[0].Rows.Count > 0)
            {
                var table = dataSet.Tables[0];

                for (int i = 0; i < table.Rows.Count; i++)
                {
                    var row = table.Rows[i];

                    item = new T();
                    item.FillFromDataRow(row);

                    list.Add(item);
                }
            }
            return list;
        }


        private const string GetDatabaseTime = "dbo.GetDatabaseTime";
        public DateTime GetDatabaseDateTime()
        {
            DateTime result;
            DataAccessContext dac;

            dac = DataAccessContextFactory.CreateQueryDataAccessContext(true, GetDatabaseTime);
            dac.Commit();

            result = ConvertDatabaseTimeData(dac.Results);

            return result;
        }

        private DateTime ConvertDatabaseTimeData(DataSet dataSet)
        {
            var result = new DateTime();

            if (dataSet != null && dataSet.Tables[0] != null && dataSet.Tables[0].Rows.Count > 0)
            {
                var table = dataSet.Tables[0];

                result = Convert.ToDateTime(table.Rows[0][0]);
            }

            return result;
        }



    }
}
