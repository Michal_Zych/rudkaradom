﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;
using m2mExportService;


namespace m2mExportService.ExportServer
{
        public class CustomUserPasswordValidator : UserNamePasswordValidator
        {
            public override void Validate(string userName, string password)
            {
                if (null == userName || null == password)
                {
                    throw new ArgumentNullException();
                }
                if (!(userName == Configuration.Instance.ClientUserName
                        && password == Configuration.Instance.ClientPassword))
                {
                    throw new SecurityTokenException("Złe hasło lub nazwa użytkownika");
                }

            }
        }
    }
