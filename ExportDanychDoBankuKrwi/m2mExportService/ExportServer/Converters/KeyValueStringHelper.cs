﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace m2mExportService.ExportServer.Converters
{
    public static class KeyValueStringHelper
    {
        private const string QUOT_REPLACEMENT = "&quot;";
        private const char STRING_PARENTHESIS = '\'';
        private const string SEPARATORS = ";, ";

        public static string GetValueForKey(this string rec, string key)
        {
            string result;
            // ToDo zadanie
            rec = SEPARATORS.ElementAt<char>(0) + rec;
            int keyStart = 0;

            while (keyStart != -1)
            {
                keyStart = rec.IndexOf(key + "=", keyStart + 1, StringComparison.InvariantCultureIgnoreCase);

                if ((keyStart > -1) && (SEPARATORS.Contains(rec.ElementAt<char>(keyStart - 1))))
                    break;
            }

            if (keyStart == -1) return string.Empty;

            int valueStart = keyStart + key.Length + 1;
            int valueEnd;
            if (rec.ElementAt<char>(valueStart) == STRING_PARENTHESIS)
                valueEnd = rec.IndexOf(STRING_PARENTHESIS, valueStart + 1);
            else
                valueEnd = rec.IndexOfAny(SEPARATORS.ToCharArray(), valueStart + 1);
            if (valueEnd == -1) valueEnd = 999;

            string res = FromQuotedText(rec.Substring(valueStart, valueEnd - valueStart + 1));

            return res;
        }

        public static string ToQuotedString(this string s)
        {
            s = s.Replace(STRING_PARENTHESIS.ToString(), QUOT_REPLACEMENT);
            if (!String.IsNullOrEmpty(s))
                s = STRING_PARENTHESIS + s + STRING_PARENTHESIS;
            return s;
        }

        private static string FromQuotedText(string s)
        {
            if (s.IndexOf(STRING_PARENTHESIS.ToString()) == 0)
            {
                s = s.Replace(QUOT_REPLACEMENT, STRING_PARENTHESIS.ToString());
                return s.Substring(1, s.Length - 2);
            }

            return s;
        }

        public static string ToYesNoValue(this bool b)
        {
            return b ? "tak" : "nie";
        }


    }
}
