﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace m2mExportService.ExportServer.Converters
{
    public static class ListToStringConverter
    {
        public static string ToStringList<T>(this IList<T> list)
        {
            StringBuilder s = new StringBuilder();
            foreach (T item in list)
            {
                if (s.Length == 0)
                {
                    s.Append(item.ToString());
                }
                else
                {
                    s.Append(Environment.NewLine).Append(item.ToString());
                }
            }
            return s.ToString();
        }
    }
}
