﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace m2mExportService.ExportServer.DataAccess
{
    public static class DateTimeConverter
    {
        public static string ToClientFormat(DateTime dt)
        {
            return dt.ToString(Configuration.Instance.DateTimeFormat);
        }

        public static DateTime FromClientFormat(string clientTime)
        {
            return DateTime.ParseExact(clientTime, Configuration.Instance.DateTimeFormat, null);
        }
    }

}
