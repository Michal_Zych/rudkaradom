﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finder.Communications.Core.Logging;
using System.ServiceModel;
using m2mExportService.PingProcessor;
using m2mExportService.ExportServer.DataAccess;
using m2mExportService.ExportServer;

namespace m2mExportService.Control
{
    public class Controller : IController
    {
        private IPingDataAccess _pingDataAccess;
        private PingProcessor.PingProcessor _pingProcessor;

        private IServerDataAccess _serverDataAccess;
        private ServiceHost _server;

        public Controller()
        {
            Initialize();
        }

        private void Initialize()
        {
            try
            {
                _pingDataAccess = new PingDataAccess();
                _pingProcessor = new PingProcessor.PingProcessor(_pingDataAccess);

                _serverDataAccess = new ExportServerDataAccess();
                m2mMonitoring.Initialize(_serverDataAccess);
                _server = new ServiceHost(typeof(m2mMonitoring));

            }
            catch (Exception e)
            {
                Logger2.Instance.Log(LogLevel.Fatal, LogType2.Activity, "Controller", "Nie udało się wystartować serwisu: " + e.ToString(), null);
                Environment.Exit(-1);
            }
        }

        public void Start()
        {
            Logger2.Instance.Log(LogLevel.Info, LogType2.Activity, "Controller", "Wesja konfiguracji: " + Configuration.Instance.ConfigVersion.ToString(), null);
            Logger2.Instance.Log(LogLevel.Info, LogType2.Activity, "Controller", "Uruchomiony", null);
            
            _pingProcessor.Start();

            try
            {
                _server.Open();
            }
            catch (Exception e)
            {
                Logger2.Instance.Log(LogLevel.Fatal, LogType2.Activity, "Controller", e.ToString(), null);
                Environment.Exit(-1);
            }
        }

        public void Stop()
        {
            _server.Close();
            _pingProcessor.Stop();
        }
    }
}
