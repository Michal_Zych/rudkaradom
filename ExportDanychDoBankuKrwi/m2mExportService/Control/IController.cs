﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace m2mExportService.Control
{
    public interface IController
    {
        void Start();
        void Stop();
    }
}
