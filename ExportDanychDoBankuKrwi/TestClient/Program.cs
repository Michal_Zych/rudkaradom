﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using TestClient.m2mExportService;

namespace TestClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var userName = ConfigurationManager.AppSettings["ClientUserName"];
            var password = ConfigurationManager.AppSettings["ClientPassword"];

            Im2mMonitoringClient proxy = new Im2mMonitoringClient("Binding_Im2mMonitoring");

            proxy.ClientCredentials.UserName.UserName = userName;
            proxy.ClientCredentials.UserName.Password = password;

            string result;

            string menu;
            do
            {
                Console.Clear();
                Console.WriteLine("MENU");
                Console.WriteLine();
                Console.WriteLine("1  - Użytkownicy");
                Console.WriteLine("2  - Jednostki organizacyjne");
                Console.WriteLine("3  - Czujniki");
                Console.WriteLine("4  - Pomiary");
                Console.WriteLine("5  - Aktualny pomiar");
                Console.WriteLine("6  - Alarmy");
                Console.WriteLine("7  - Data czas");
                Console.WriteLine("8  - echo");

                Console.WriteLine();
                Console.WriteLine("Enter - koniec");
                menu = Console.ReadLine();

                switch (menu)
                {
                    case "1":
                        Uzytkownicy(proxy);
                        break;
                    case "2":
                        JednostkiOrganizacyjne(proxy);
                        break;
                    case "3":
                        Czujniki(proxy);
                        break;
                    case "4":
                        Pomiary(proxy);
                        break;
                    case "5":
                        AktualnyPomiar(proxy);
                        break;
                    case "6":
                        Alarmy(proxy);
                        break;
                    case "7":
                        DataCzas(proxy);
                        break;
                    case "8":
                        Echo(proxy);
                        break;
                }
            } while (menu != "");

            proxy.Close();
        }
        
        private static string GetParam(string desc)
        {
            Console.Write(desc + ": ");
            return Console.ReadLine();
        }

        private static void Echo(Im2mMonitoringClient proxy)
        {
            Console.WriteLine("Echo:");
            string result;

            try
            {
                result = proxy.Echo(GetParam("komunikat"));
                Console.WriteLine("Wysłano");
                Console.WriteLine("Rezultat: " + result);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            Console.ReadLine();
        }

        private static void DataCzas(Im2mMonitoringClient proxy)
        {
            Console.WriteLine("Data i czas na serwerze");
            string result;
            try
            {
                result = proxy.DataCzas();
                Console.WriteLine("Rezultat: " + result);

            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            Console.ReadLine();
        }

        private static void Uzytkownicy(Im2mMonitoringClient proxy)
        {
            Console.WriteLine("Lista użytkowników");
            string result;
            try
            {
                result = proxy.Uzytkownicy();
                Console.WriteLine("Rezultat: " + result);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            Console.ReadLine();
        }

        private static void JednostkiOrganizacyjne(Im2mMonitoringClient proxy)
        {
            Console.WriteLine("Lista jednostek organizacyjnych");
            string result;
            try
            {
                result = proxy.JednostkiOrganizacyjne();
                Console.WriteLine("Rezultat: " + result);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            Console.ReadLine();
        }

        private static void Czujniki(Im2mMonitoringClient proxy)
        {
            Console.WriteLine("Lista czujników");
            string result;
            try
            {
                result = proxy.Czujniki();
                Console.WriteLine("Rezultat: " + result);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            Console.ReadLine();
        }

        private static void Pomiary(Im2mMonitoringClient proxy)
        {
            Console.WriteLine("Wyniki pomaiarów dla czujnika");
            string result;
            try
            {
                result = proxy.Pomiary(Convert.ToInt32(GetParam("id czujnika")), Convert.ToDateTime(GetParam("data początkowa")), Convert.ToDateTime(GetParam("data końcowa")));
                Console.WriteLine("Wysłano");
                Console.WriteLine("Rezultat: " + result);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            Console.ReadLine();
        }

        private static void AktualnyPomiar(Im2mMonitoringClient proxy)
        {
            Console.WriteLine("Aktualne wskazanie czujnika");
            string result;
            try
            {
                result = proxy.AktualnyPomiar(Convert.ToInt32(GetParam("id czujnika")));
                Console.WriteLine("Wysłano");
                Console.WriteLine("Rezultat: " + result);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            Console.ReadLine();
        }

        private static void Alarmy(Im2mMonitoringClient proxy)
        {
            Console.WriteLine("Alarmy dla czujnika");
            string result;
            try
            {
                result = proxy.Alarmy(Convert.ToInt32(GetParam("id czujnika")), Convert.ToDateTime(GetParam("data początkowa")), Convert.ToDateTime(GetParam("data końcowa")));
                Console.WriteLine("Wysłano");
                Console.WriteLine("Rezultat: " + result);

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            Console.ReadLine();
        }



    }
}
