﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace M2MConnectorService
{
    public partial class M2MImpicodeConnector : ServiceBase
    {
        public M2MImpicodeConnector()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            M2MConnectorService.impicode.Main.Run.Start();
        }

        protected override void OnStop()
        {
        }
    }
}
