﻿using Apache.NMS;
using Apache.NMS.ActiveMQ.Commands;
using M2MConnectorService.impicode.Main;
using M2MConnectorService.impicode.QueueEvents;
using System;
using System.Collections.Generic;
using System.Threading;

namespace M2MConnectorService.impicode
{
    class QueueHandler
    {
        public static void ConsumeMessage(SimpleDBConnection dBConnection, IMessage innerMessage)
        {
            try
            {
                PrintLogHeader();
                if (innerMessage is ActiveMQMapMessage mapMessage)
                {
                    QueueConfig.Log.Info("message id: " + mapMessage.NMSMessageId);
                    IPrimitiveMap headerMap = mapMessage.Properties;
                    String eventDate = headerMap.GetString("EVENT_DATE");
                    String eventType = headerMap.GetString("EVENT_TYPE");
                    String originSystem = headerMap.GetString("ORIGIN_SYSTEM");

                    QueueConfig.Log.Info("EVENT DATE: " + eventDate);
                    QueueConfig.Log.Info("EVENT TYPE: " + eventType);
                    QueueConfig.Log.Info("ORIGIN SYSTEM" + originSystem);
                    IPrimitiveMap messageMap = mapMessage.Body;
                    PrintMapToLog(messageMap);
                    EventMeta meta = new EventMeta(eventDate, eventType, originSystem);
                    QueueEvent queueEvent = TryProcessMessage(eventType, messageMap, meta);
                    if (queueEvent != null)
                    {
                        ExecuteCommandEvent(queueEvent, dBConnection);
                    }
                }
                else if (innerMessage is ITextMessage textMessage)
                {
                    QueueConfig.Log.Info("debug message");
                    QueueConfig.Log.Info(textMessage.Text);
                }
                else
                {
                    QueueConfig.Log.Info("unrecognized message type");
                }
                QueueConfig.Log.Info("message raw");
                QueueConfig.Log.Info(innerMessage.ToString());
                QueueConfig.Log.Info("End message processing...");
            }
            catch (Exception e)
            {
                QueueConfig.Log.Info(e.Message);
                QueueConfig.Log.Info("queuehandler stack", e);
            }
        }


        private const string SaveEvent_EventMessage = "ERROR DURING SAVE EVENT COMMAND";
        private static void ExecuteCommandEvent(QueueEvent eevent, SimpleDBConnection dBConnection)
        {
            QueueConfig.Log.Info("QUEUE EVENT COMMAND");

            bool eventSaved = false;
            string errorDescription = "";
            int tryNumber = 0;
            while (!eventSaved)
            {
                tryNumber += 1;
                try
                {
                    M2MProcedureManager.SaveEvent(eevent, dBConnection);
                    eventSaved = true;
                }
                catch (Exception e)
                {
                    if (errorDescription != e.Message)
                    {
                        errorDescription = e.Message;
                        QueueConfig.Log.Info(SaveEvent_EventMessage, e);
                    }
                    else
                    {
                        QueueConfig.Log.Info($"{SaveEvent_EventMessage},\n\t TRY: {tryNumber}\n{e.Message}");
                    }
                }

                if(!eventSaved)
                {
                    Thread.Sleep(Configurator.RetryDbConnectionDelaySec * 1000);
                }
            }
        }

        private static void PrintLogHeader()
        {
            QueueConfig.Log.Info("message received");
            QueueConfig.Log.Info(DateTime.Now);
        }

        private static void PrintMapToLog(IPrimitiveMap map)
        {
            QueueConfig.Log.Info("\n\nMap begin");
            foreach (String key in map.Keys)
            {
                QueueConfig.Log.Info("key: " + key);
                QueueConfig.Log.Info("value: " + map.GetString(key));
            }
            QueueConfig.Log.Info("Map end\n");
            QueueConfig.Log.Info(M2MSerializer.Serialize(EventToDict(map)));
        }

        private static QueueEvent TryProcessMessage(String eventType, IPrimitiveMap map, EventMeta meta)
        {
            QueueConfig.Log.Info("generic event type: " + eventType);
            return new GenericEvent(meta, EventToDict(map));
        }

        private static Dictionary<String, String> EventToDict(IPrimitiveMap map)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            foreach (String key in map.Keys)
            {
                dict.Add(key, map.GetString(key));
            }
            return dict;
        }
    }
}
