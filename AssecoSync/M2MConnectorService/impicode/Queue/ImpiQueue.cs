﻿using Apache.NMS;
using M2MConnectorService.impicode.Main;
using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;

namespace M2MConnectorService.impicode
{
    class ImpiQueue
    {
        private IConnection connection;
        private ISession session;

        private string ConnectionString { get; set; }
        private string UserName { get; set; }
        private string Password { get; set; }
        private QueueConfig.TType ChannelType { get; set; }
        private string Topic { get; set; }
        private string ConnectionId { get; set; }
        private string Selection { get; set; }
        private Boolean IsConnected { get; set; }
        private long KeepAliveInterval { get; set; }

        private SimpleDBConnection dBConnection;
        private object connectionLock = new object();

        public ImpiQueue(string connectionString, string userName, string password, QueueConfig.TType channelType, string topic, string connectionId,
            IList<string> supportedEventTypes, string dbConnectionString, long keepAliveInterval)
        {
            lock (connectionLock)
            {
                ConnectionString = connectionString;
                UserName = userName;
                Password = password;
                ChannelType = channelType;
                Topic = topic;
                ConnectionId = connectionId;
                Selection = PrepareSelection(supportedEventTypes);
                dBConnection = new SimpleDBConnection(dbConnectionString);
                IsConnected = false;
                KeepAliveInterval = keepAliveInterval;
            }
            SendKeepAliveSignal(true);
        }

        private string PrepareSelection(IList<string> supportedEventTypes)
        {
            if (supportedEventTypes.Count == 0)
            {
                return "true";
            }
            StringBuilder ret = new StringBuilder();
            ret.Append("false ");
            foreach (string eventType in supportedEventTypes)
            {
                string tmp = eventType;
                tmp = tmp.Replace("\\", "\\\\");
                tmp = tmp.Replace("'", "\\'");
                ret.Append(" OR EVENT_TYPE = '" + tmp + "' ");
            }
            
            return ret.ToString();
        }

        public void Start()
        {
            lock (connectionLock)
            {
                try
                {
                    QueueConfig.Log.Info("About to connect to " + ConnectionString);

                    NMSConnectionFactory factory = new NMSConnectionFactory(ConnectionString, ConnectionId);
                    connection = factory.CreateConnection(UserName, Password);
                    session = connection.CreateSession(AcknowledgementMode.AutoAcknowledge);

                    connection.ExceptionListener += (Exception eee) =>
                    {
                        lock (dBConnection)
                        {
                            IsConnected = false;
                            QueueConfig.Log.Info("Queue connection interrupted");
                            try
                            {
                                session.Close();
                                connection.Close();
                            }
                            catch (Exception e)
                            {
                                QueueConfig.Log.Info("close failed", e);
                            }
                        }
                        Start();
                    };
                    connection.Start();
                    ListenTopic();
                    IsConnected = true;
                    QueueConfig.Log.Info("Successfully connected " + ConnectionString);
                }
                catch (Exception e)
                {
                    try
                    {
                        session.Close();
                        connection.Close();
                    }
                    catch (Exception ee)
                    {
                    }
                    QueueConfig.Log.Info("queue connection start failed", e);
                    QueueConfig.Log.Info("Retry in 60 seconds");
                    new MainTimer().StartTimer(this);
                }
            }
        }

        public void StartKeepAlive()
        {
            lock (connectionLock)
            {
                new KeepAliveTimer().StartTimer(this);
            }
        }

        private void SendKeepAliveSignal(bool isRestart = false)
        {
            lock (connectionLock)
            {
                try
                {
                    M2MDBSerializer.SendKeepAlive(IsConnected, isRestart, Configurator.ServiceTag, dBConnection);
                    QueueConfig.Log.Info("KEEP ALIVE SENT" + IsConnected);
                } catch (Exception e)
                {
                    QueueConfig.Log.Info("ERROR: KEEP ALIVE NOT SENT", e);
                }
            }
        }

        class KeepAliveTimer
        {
            private ImpiQueue impiQueue;
            
            public void StartTimer(ImpiQueue impiQueue)
            {
                this.impiQueue = impiQueue;
                System.Timers.Timer aTimer = new System.Timers.Timer();
                aTimer.Elapsed += OnTimedEvent;
                aTimer.Interval = impiQueue.KeepAliveInterval * 1000;
                aTimer.AutoReset = true;
                aTimer.Enabled = true;
            }

            public void OnTimedEvent(Object source, ElapsedEventArgs e)
            {
                impiQueue.SendKeepAliveSignal();
            }
        }

        class MainTimer
        {

            private ImpiQueue impiQueue;
            public void StartTimer(ImpiQueue impiQueue)
            {
                this.impiQueue = impiQueue;
                System.Timers.Timer aTimer = new System.Timers.Timer();
                aTimer.Elapsed += OnTimedEvent;
                aTimer.Interval = 60 * 1000;
                aTimer.AutoReset = false;
                aTimer.Enabled = true;
            }
            
            public void OnTimedEvent(Object source, ElapsedEventArgs e)
            {
                impiQueue.Start();
            }
        }

        private void ListenTopic()
        {
            IMessageConsumer consumer;
            if (ChannelType == QueueConfig.TType.QUEUE)
            {
                IQueue topic = session.GetQueue(Topic);
                consumer = session.CreateConsumer(topic);
            } else if (ChannelType == QueueConfig.TType.TOPIC)
            {
                ITopic topic = session.GetTopic(Topic);
                consumer = session.CreateDurableConsumer(topic, ConnectionId, Selection, false);
            } else
            {
                consumer = null;
            }
            
            consumer.Listener += delegate (IMessage innerMessage)
            {
                lock (connectionLock)
                {
                    M2MDBSerializer.RefreshConnection(dBConnection);
                    QueueHandler.ConsumeMessage(dBConnection, innerMessage);
                }
            };
        }
    }
}
