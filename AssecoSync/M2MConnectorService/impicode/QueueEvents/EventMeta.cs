﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2MConnectorService.impicode.QueueEvents
{
    public class EventMeta
    {
        public String EventDate { get; set; }
        public String EventType { get; set; }
        public String OriginSystem { get; set; }

        public EventMeta(string eventDate, string eventType, string originSystem)
        {
            EventDate = eventDate;
            EventType = eventType;
            OriginSystem = originSystem;
        }
    }
}
