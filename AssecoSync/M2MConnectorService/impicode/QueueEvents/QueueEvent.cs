﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2MConnectorService.impicode.QueueEvents
{
    public abstract class QueueEvent
    {
        public const string VISIT_EVENT = "VISIT_EVENT";

        public string EventType { get; set; }

        public EventMeta Meta { get; set; }

        protected QueueEvent(string eventType, EventMeta meta)
        {
            EventType = eventType;
            Meta = meta;
        }

        public abstract void Accept(IQueueEventVisitor visitor);

    }
}
