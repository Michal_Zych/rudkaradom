﻿namespace M2MConnectorService.impicode.QueueEvents
{
    public interface IQueueEventVisitor
    {
        void Visit(GenericEvent e);
    }
}
