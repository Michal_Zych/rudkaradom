﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2MConnectorService.impicode.QueueEvents
{
    public class GenericEvent : QueueEvent
    {
        public Dictionary<string, string> Value { get; private set; }

        public GenericEvent(EventMeta meta, Dictionary<string, string> value) : base("GENERIC_EVENT", meta)
        {
            this.Value = value;
        }

        public override void Accept(IQueueEventVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
