﻿using System;

namespace M2MConnectorService.impicode
{
    class QueueConfig
    {
        public static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public String UserName { get; set; }
        public String Password { get; set; }
        public String ConnectionString { get; set; }
        public String TopicName { get; set; }
        public enum TType { QUEUE, TOPIC };
        public TType ChannelType { get; set; }
        public String EscapeValue { get; set; }
        public String EscapeValueLine { get; set; }
        public long CheckCmdsIntervalMilis { get; set; }
        public long NewQueryLimit { get; set; }
        public string WebServiceUserName { get; set; }
        public string WebServicePassword { get; set; }
        public string WebServiceURL { get; set; }
        public string ActiveMQConnectionId { get; set; }
        public long KeepAliveIntervalS { get; set; }

        private static QueueConfig instance;

        private QueueConfig() {
            ChannelType = QueueConfig.TType.TOPIC;
        }

        public static QueueConfig Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new QueueConfig();
                }
                return instance;
            }
        }
    }
}
