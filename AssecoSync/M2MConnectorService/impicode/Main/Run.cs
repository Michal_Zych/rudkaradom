﻿using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading;
using System.Timers;

namespace M2MConnectorService.impicode.Main
{
    public class Run
    {
        

        public static void Start()
        {
            XmlConfigurator.Configure();
            QueueConfig.Log.Info("M2MCONNECTOR START");


            bool configurationReaded = Configurator.ReadConfiguration();

            if (configurationReaded)
            {
                Thread mainThread = new Thread(() =>
                {
                    try
                    {
                        Console.WriteLine(Configurator.DbConnectionString);
                        AssecoWebService webService = new AssecoWebService(QueueConfig.Instance.UserName, QueueConfig.Instance.WebServicePassword, QueueConfig.Instance.WebServiceURL);
                        webService.Connect();
                        Worker worker = new Worker(webService, Configurator.DbConnectionString);
                        StartQueues(Configurator.SupportedEventTypes, Configurator.DbConnectionString);
                        new MainTimer().StartTimer(worker);
                        Thread workerThread = new Thread(() =>
                        {
                            QueueConfig.Log.Info("worker start");
                            worker.Work();
                        });
                        workerThread.Start();

                        QueueConfig.Log.Info("start listening");
                    }
                    catch (Exception e)
                    {
                        QueueConfig.Log.Fatal("exception during initalization", e);
                        QueueConfig.Log.Fatal("SERVICE QUIT");
                    }
                });
                mainThread.Start();
            }
        }

        public static void StartQueues(IList<string> supportedEventTypes, string dbConnectionString)
        {
            ImpiQueue impiQueue = new ImpiQueue(QueueConfig.Instance.ConnectionString, QueueConfig.Instance.UserName, QueueConfig.Instance.Password,
                QueueConfig.Instance.ChannelType, QueueConfig.Instance.TopicName, QueueConfig.Instance.ActiveMQConnectionId, supportedEventTypes, dbConnectionString,
                QueueConfig.Instance.KeepAliveIntervalS);
            impiQueue.Start();
            impiQueue.StartKeepAlive();
        }

        class MainTimer
        {

            private Worker worker;
            public void StartTimer(Worker worker)
            {
                this.worker = worker;
                System.Timers.Timer aTimer = new System.Timers.Timer();
                aTimer.Elapsed += OnTimedEvent;
                aTimer.Interval = QueueConfig.Instance.CheckCmdsIntervalMilis;
                aTimer.AutoReset = true;
                aTimer.Enabled = true;
            }

            public void OnTimedEvent(Object source, ElapsedEventArgs e)
            {
                worker.AddTimerCommand();
            }
        }


    }
}
