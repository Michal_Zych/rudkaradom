﻿using System.Data.SqlClient;

namespace M2MConnectorService.impicode.Main
{
    public class SimpleDBConnection
    {
        public SqlConnection SqlConnection { get; set; }
        public string DbConnectionString { get; set; }

        public SimpleDBConnection(string dbConnectionString)
        {
            this.DbConnectionString = dbConnectionString;
            SqlConnection = new SqlConnection(dbConnectionString);
        }
    }
}
