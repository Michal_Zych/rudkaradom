﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2MConnectorService.impicode.Main
{
    public static class QueryType
    {
        public const string Tag = "TAG";
        public const string User = "USER";
        public const string Users = "USRS";
        public const string PatientLocalzation = "PALC";
        public const string PatientDemographics = "PADM";
        public const string Visit = "VISI";
    }
}
