﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace M2MConnectorService.impicode.Main
{
    class M2MDBSerializer
    {
       



        public static void SaveEvent(string tag, string value, SimpleDBConnection dBConnection)
        {
            if (tag != "PATIENT_MODIFIED" && tag != "PATIENT_REGISTERED" && tag != "VISIT_END" && tag != "String PATIENT_VISIT_START")
            {
                ;
            }


            RefreshConnection(dBConnection);
            using (SqlCommand insertCommand = new SqlCommand(Configurator.SqlSaveEventData, dBConnection.SqlConnection))
            {
                insertCommand.CommandType = CommandType.StoredProcedure;
                insertCommand.Parameters.Add("@Tag", SqlDbType.NVarChar).Value = tag;
                insertCommand.Parameters.Add("@Record", SqlDbType.NVarChar).Value = value;
                insertCommand.ExecuteNonQuery();
            }
        }

        public static void WriteNewQueryResponse(string requestId, String serializedRecord, String statusCode, SimpleDBConnection dBConnection)
        {
            M2MDBSerializer.RefreshConnection(dBConnection);
            QueueConfig.Log.Info("rekord zwrocony po zapytaniu: " + requestId + " " + serializedRecord);

            using (SqlCommand insertCommand = new SqlCommand(Configurator.SqlSaveQueryResponse, dBConnection.SqlConnection))
            {
                insertCommand.CommandType = CommandType.StoredProcedure;
                insertCommand.Parameters.Add("@RequestId", SqlDbType.NVarChar).Value = requestId;
                insertCommand.Parameters.Add("@Status", SqlDbType.NVarChar).Value = statusCode;
                insertCommand.Parameters.Add("@Record", SqlDbType.NVarChar).Value = serializedRecord;
                insertCommand.ExecuteNonQuery();
            }
        }

        public static List<NewQuery> GetNewQueries(long lastRecordId, SimpleDBConnection dBConnection)
        {
            RefreshConnection(dBConnection);
            List<NewQuery> records = new List<NewQuery>();

            using (SqlCommand cmd = new SqlCommand(Configurator.SqlGetNewQueries, dBConnection.SqlConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                if (lastRecordId == -1)
                {
                    cmd.Parameters.Add("@LastRequestId", SqlDbType.BigInt).Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("@LastRequestId", SqlDbType.BigInt).Value = lastRecordId;
                }
                cmd.Parameters.Add("@Limit", SqlDbType.BigInt).Value = QueueConfig.Instance.NewQueryLimit;
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    records.Add(new NewQuery(reader[0].ToString(), (string)reader[1], (string)reader[2]));
                }
                reader.Close();
            }
             return records;
        }


        public static void SendKeepAlive(bool status, bool isRestrart, string serviceTag, SimpleDBConnection dBConnection)
        {
            RefreshConnection(dBConnection);
            using (SqlCommand cmd = new SqlCommand(Configurator.SqlSendKeepAlive, dBConnection.SqlConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@Status", SqlDbType.NVarChar).Value = status ? "1" : "0";
                cmd.Parameters.Add("@IsRestart", SqlDbType.Bit).Value = isRestrart;
                cmd.Parameters.Add("@ServiceTag", SqlDbType.NVarChar).Value = serviceTag;
                cmd.ExecuteNonQuery();
            }
        }

        public static void RefreshConnection(SimpleDBConnection dbConnection)
        {
            if (dbConnection.SqlConnection == null || dbConnection.SqlConnection.State != System.Data.ConnectionState.Open)
            {
                try
                {
                    dbConnection.SqlConnection.Close();
                }
                catch (Exception e)
                {

                }
                dbConnection.SqlConnection = new SqlConnection(dbConnection.DbConnectionString);
                dbConnection.SqlConnection.Open();
            }
        }
    }
}
