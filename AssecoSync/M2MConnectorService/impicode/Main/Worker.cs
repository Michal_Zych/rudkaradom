﻿using M2MConnectorService.impicode.Main.WorkerCommand;
using M2MConnectorService.impicode.QueueEvents;
using System;
using System.Collections.Concurrent;

namespace M2MConnectorService.impicode.Main
{
    public class Worker : ICmdVisitor
    {
        private BlockingCollection<WorkerCmd> cmds;
        private M2MProcedureManager manager;
        private WebServiceCaller wsCaller;
        private bool isEnd;
        private long lastRecordId;

        public void AddEventCommand(QueueEvent e)
        {
            cmds.Add(new CmdQueue(e));
        }

        public void AddTimerCommand()
        {
            cmds.Add(new CmdTimer());
        }

        public void AddEndCommand()
        {
            cmds.Add(new CmdEnd());
        }

        public Worker(AssecoWebService webService, String dbConnectionString)
        {
            isEnd = false;
            wsCaller = new WebServiceCaller(webService);
            manager = new M2MProcedureManager(wsCaller, new SimpleDBConnection(dbConnectionString));
            cmds = new BlockingCollection<WorkerCmd>();
            lastRecordId = -1;
        }

        public void Work()
        {
            while (!isEnd)
            {
                WorkerCmd cmd = cmds.Take();
                cmd.Accept(this);
            }
        }

        public void Visit(CmdQueue cmd)
        {
            
        }

        public void Visit(CmdTimer cmd)
        {
            QueueConfig.Log.Info("QUEUE TIMER COMMAND");
            try {

                lastRecordId = manager.GetNewQueries(lastRecordId);
            } catch (Exception e)
            {
                QueueConfig.Log.Info("ERROR DURING QUEUE TIMER COMMAND", e);
            }
        }

        public void Visit(CmdEnd cmd)
        {
            isEnd = true;
        }
    }
}
