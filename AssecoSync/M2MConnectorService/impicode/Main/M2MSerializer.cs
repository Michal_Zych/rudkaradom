﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M2MConnectorService.impicode.Main
{
    class M2MSerializer
    {
        public static string Serialize(IDictionary<string, string> dict)
        {
            String escapeValue = QueueConfig.Instance.EscapeValue;
            StringBuilder builder = new StringBuilder();
            foreach (KeyValuePair<String, String> entry in dict)
            {
                builder.Append(entry.Key + "=" + ((entry.Value == null) ? "null" : ("'" + entry.Value.Replace("'", escapeValue) + "'")) + ";");
            }
            return builder.ToString();
        }

        public static IDictionary<string, string> Deserialize(string recordData)
        {
            IDictionary<string, string> dict = new Dictionary<string, string>();

            int pos = 0;
            while (pos < recordData.Length)
            {
                EatWs(recordData, ref pos);
                string name = GetToken(recordData, ref pos);
                EatWs(recordData, ref pos);
                string value = GetToken(recordData, ref pos);
                EatWs(recordData, ref pos);
                if (value.Equals("null"))
                {
                    value = null;
                } else
                {
                    value = value.Replace(QueueConfig.Instance.EscapeValue, "'");
                }
                dict[name] = value;
            }
            return dict;
        }

        private static void EatWs(string str, ref int pos)
        {
           while (true)
            {
                
                if (pos >= str.Length)
                {
                    return;
                }
                String c = str.Substring(pos, 1);
                if (c.Equals("="))
                {
                    ++pos;
                }
                else if (c.Equals(";"))
                {
                    ++pos;
                } else
                {
                    return;
                }
            }
        }

        private static string GetToken(string str, ref int pos)
        {

            EatWs(str, ref pos);
            if (str.Substring(pos, 1).Equals("'"))
            {
                ++pos;
                int posStart = pos;
                int length = 0;
                StringBuilder ret = new StringBuilder();
                while (!IsEnd(str, pos))
                {
                    ++length;
                    ++pos;
                }
                ++pos;
                EatWs(str, ref pos);
                return str.Substring(posStart, length);
            }
            else
            {
                int posStart = pos;
                int length = 0;
                StringBuilder ret = new StringBuilder();
                while (!IsEnd(str, pos))
                {
                    ++length;
                    ++pos;
                }
                EatWs(str, ref pos);
                return str.Substring(posStart, length);
            }
        }

        private static bool IsEnd(string str, int pos)
        {
            if (pos >= str.Length)
            {
                return true;
            }
            String c = str.Substring(pos, 1);
            return c.Equals("'") || c.Equals("=") || c.Equals(";");
        }
    }
}
