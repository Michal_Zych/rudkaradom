﻿using M2MConnectorService.impicode.Main;
using M2MConnectorService.impicode.QueueEvents;
using System;
using System.Collections.Generic;
using System.Text;

namespace M2MConnectorService.impicode
{
    class M2MProcedureManager
    {
        private WebServiceCaller wsCaller;
        private SimpleDBConnection dbConnection;

        
        private Dictionary<string, Func<string, string>> queryProcessor;




        public M2MProcedureManager(WebServiceCaller wsCaller, SimpleDBConnection dbConnection)
        {
            this.wsCaller = wsCaller;
            this.dbConnection = dbConnection;

            queryProcessor = new Dictionary<string, Func<string, string>>()
            {
                {QueryType.PatientLocalzation, AskPatientLocalization}
                ,{QueryType.PatientDemographics, AskPatientDemographics}
                ,{ QueryType.Visit, AskVisit}
                ,{QueryType.User, AskUser}
                ,{ QueryType.Users, AskUsers}
                ,{QueryType.Tag, AskTag }
                
            };

            

            M2MDBSerializer.RefreshConnection(dbConnection);
        }

        public static void SaveEvent(QueueEvent e, SimpleDBConnection dBConnection)
        {
            M2MDBSerializer.RefreshConnection(dBConnection);
            SerializerVisitor visitor = new SerializerVisitor();
            e.Accept(visitor);
            QueueConfig.Log.Info("rekord do bazy SAVE_EVENT: " + visitor.Name + " " + visitor.Value);
            M2MDBSerializer.SaveEvent(visitor.Name, visitor.Value, dBConnection);
        }

        private class SerializerVisitor : IQueueEventVisitor
        {
            public SerializerVisitor()
            {
            }
            public string Name { get; set; }
            public string Value { get; set; }

            public void Visit(GenericEvent e)
            {
                Dictionary<string, string> dict = new Dictionary<string, string>();
                foreach (var el in e.Value)
                {
                    dict.Add(el.Key, el.Value);
                }

                Name = e.Meta.EventType;
                AddMeta(dict, e.Meta);
                Value = M2MSerializer.Serialize(dict);
            }

            private void AddMeta(Dictionary<string, string> ret, EventMeta meta)
            {
                ret["EVENT_DATE"] = meta.EventDate;
                ret["EVENT_TYPE"] = meta.EventType;
                ret["ORIGIN_SYSTEM"] = meta.OriginSystem;
            }
        }

        public long GetNewQueries(long lastRecordId)
        {
            List<NewQuery> records = M2MDBSerializer.GetNewQueries(lastRecordId, dbConnection);
            long newLastRecordId = lastRecordId;
            foreach (NewQuery newQuery in records)
            {
                try
                {
                    ProcessNewQuery(newQuery.RequestId, newQuery.QueryType, newQuery.RecordData);
                }
                catch (Exception e)
                {
                    try
                    {
                        M2MDBSerializer.WriteNewQueryResponse(newQuery.RequestId, e.Message + "\n\n" + e.StackTrace, "0", dbConnection);
                    }
                    catch (Exception ee)
                    {
                        QueueConfig.Log.Info("Process new query - fatal error", ee);
                        QueueConfig.Log.Info("Process new query - fatal error - PREVIOUS", e);
                    }
                }
                newLastRecordId = long.Parse(newQuery.RequestId);
            }

            return newLastRecordId;
        }

        private void ProcessNewQuery(string requestId, string queryType, string recordData)
        {
            queryType = queryType.ToUpper();
            if(queryProcessor.ContainsKey(queryType))
            {
                M2MDBSerializer.WriteNewQueryResponse(requestId, queryProcessor[queryType].Invoke(recordData), "1", dbConnection);
            }
            else
            {
                QueueConfig.Log.Info("unrecognized input newQuery command: " + queryType);
                throw new Exception("Nieznany typ rekordu: " + queryType);
            }
        }

        private string AskVisit(string recordData)
        {
            IDictionary<string, string> request = M2MSerializer.Deserialize(recordData);
            return wsCaller.AskVisit(request["VISIT_ID"]).ToString(Configurator.GetMapper(QueryType.Visit));
        }

        private string AskPatientLocalization(string recordData)
        {
            IDictionary<string, string> request = M2MSerializer.Deserialize(recordData);
            return wsCaller.GetPatientLocalization(request).ToString(Configurator.GetMapper(QueryType.PatientLocalzation));
        }

        private string AskPatientDemographics(string recordData)
        {
            IDictionary<string, string> request = M2MSerializer.Deserialize(recordData);
            return wsCaller.GetPatientDemographics(request).ToString(Configurator.GetMapper(QueryType.PatientDemographics));
        }

        private string AskUser(string recordData)
        {
            IDictionary<string, string> request = M2MSerializer.Deserialize(recordData);
            return wsCaller.AskUser(request["USER_ID"]).ToString(Configurator.GetMapper(QueryType.User));
        }

        private String AskUsers(string recordData)
        {
            IDictionary<string, string> request = M2MSerializer.Deserialize(recordData);
            IDictionary<string, string>[] users = wsCaller.GetUsers(GetValueOrNull(request, "LOGIN"), GetValueOrNull(request, "NAME"),
                GetValueOrNull(request, "SURENAME"), GetValueOrNull(request, "ACTIVE"));

            var map = Configurator.GetMapper(QueryType.User);
            StringBuilder ret = new StringBuilder();
            foreach (var user in users)
            {
                ret.AppendLine(user.ToString(map));
            }
            return ret.ToString();
        }

        private string GetValueOrNull(IDictionary<string, string> dict, string key)
        {
            return dict.TryGetValue(key, out string value) ? value : null;
        }

        private string AskTag(string recordData)
        {
            IDictionary<string, string> request = M2MSerializer.Deserialize(recordData);
            return wsCaller.ModifyTagId(request).ToString(Configurator.GetMapper(QueryType.Tag));
        }

    }
}
