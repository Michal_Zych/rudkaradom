﻿using M2MConnectorService.impicode.QueueEvents;

namespace M2MConnectorService.impicode.Main.WorkerCommand
{
    public class CmdQueue : WorkerCmd
    {
        public QueueEvent Event { get; private set; }
        public CmdQueue(QueueEvent eevent) {
            Event = eevent;
        }

        public override void Accept(ICmdVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
