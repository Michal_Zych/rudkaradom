﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2MConnectorService.impicode.Main.WorkerCommand
{
    public class CmdEnd : WorkerCmd
    {
        public override void Accept(ICmdVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
