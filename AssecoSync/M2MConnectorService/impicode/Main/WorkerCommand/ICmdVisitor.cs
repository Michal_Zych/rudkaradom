﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2MConnectorService.impicode.Main.WorkerCommand
{
    public interface ICmdVisitor
    {
        void Visit(CmdQueue cmd);
        void Visit(CmdTimer cmd);
        void Visit(CmdEnd cmd);
    }
}
