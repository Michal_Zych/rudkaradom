﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2MConnectorService.impicode.Main.WorkerCommand
{
    public abstract class WorkerCmd
    {
        public abstract void Accept(ICmdVisitor visitor);
    }
}
