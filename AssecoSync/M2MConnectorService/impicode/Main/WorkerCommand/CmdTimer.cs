﻿using M2MConnectorService.impicode.QueueEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2MConnectorService.impicode.Main.WorkerCommand
{
    public class CmdTimer : WorkerCmd
    {
        public override void Accept(ICmdVisitor visitor)
        {
            visitor.Visit(this);
        }
    }
}
