﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace M2MConnectorService.impicode.Main
{
    class Configurator
    {
        public const int RetryDbConnectionDelaySec = 10;
        private const int MaxDbConnectionTries = 10000;
        private const string ConfigurationReadingError = "Exception during reading configuration";


        private const string Key_ServiceTag = "ServiceTag";
        private const string Key_ActiveConnectionString = "ActiveConnectionString";
        private const string Key_GetSettingsSqlProc = "SqlProcedure_GetSettings";
        private const string Key_GetSupportedEventsSqlProc = "SqlProcedure_GetSupportedEventTypes";
        private const string Key_SaveEventDataProc = "SqlProcedure_SaveEventData";
        private const string Key_SaveQueryResponse = "SqlProcedure_SaveQueryResponse";


        private const string Key_GetNewQueries = "SqlProcedure_GetNewQueries";
        private const string Key_SendKeepAlive = "SqlProcedure_SendKeepAlive";


        public static string ServiceTag { get; private set; }
        public static string DbConnectionString { get; private set; }
        public static string SqlGetSettings { get; private set; }
        public static string SqlGetSupportedEventsTypes { get; private set; }
        public static string SqlSaveEventData { get; private set; }
        public static string SqlSaveQueryResponse { get; private set; }
        public static string SqlGetNewQueries { get; private set; }
        public static string SqlSendKeepAlive { get; private set; }

        public static IList<string> SupportedEventTypes { get; private set; }

        
        public static bool ReadConfiguration()
        {
            bool configReaded = false;
            string errorInfo = "";
            int errorRepetition = 1;
            int connectionTryNumber = 0;

            while (!configReaded && (connectionTryNumber++ < MaxDbConnectionTries))
            {
                try
                {
                    Initialize();
                    QueueConfig.Log.Info("Configuration readed from database.");
                    configReaded = true;
                }
                catch (Exception e)
                {
                    if (errorInfo != e.Message)
                    {
                        errorRepetition = 1;
                        errorInfo = e.Message;
                        QueueConfig.Log.Fatal(ConfigurationReadingError, e);
                    }
                    else
                    {
                        errorRepetition += 1;
                        QueueConfig.Log.Fatal($"{ConfigurationReadingError}\n\tConnection try: {errorRepetition}\n\tMessage {errorInfo}");
                    }
                }
                if (!configReaded)
                {
                    Thread.Sleep(RetryDbConnectionDelaySec * 1000);
                }
            }

            return configReaded;
        }


        public static void Initialize()
        {
            ServiceTag = ConfigurationManager.AppSettings[Key_ServiceTag];

            String activeConnectionString = ConfigurationManager.AppSettings[Key_ActiveConnectionString];
            ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;
            DbConnectionString = settings[activeConnectionString].ConnectionString;

            SqlGetSettings = ConfigurationManager.AppSettings[Key_GetSettingsSqlProc];
            SqlGetSupportedEventsTypes = ConfigurationManager.AppSettings[Key_GetSupportedEventsSqlProc];
            SqlSaveEventData = ConfigurationManager.AppSettings[Key_SaveEventDataProc];
            SqlSaveQueryResponse = ConfigurationManager.AppSettings[Key_SaveQueryResponse];
            SqlGetNewQueries = ConfigurationManager.AppSettings[Key_GetNewQueries];
            SqlSendKeepAlive = ConfigurationManager.AppSettings[Key_SendKeepAlive];

            GetConfigurationFromDatabase();
        }

        private const string MapperSuffix = "_Mapper";
        public const string AllKeyMap = "*";
        private static IDictionary<string, IDictionary<string, string>> mappers = new Dictionary<string, IDictionary<string, string>>();

        public static IDictionary<string, string> GetMapper(string recordType)
        {
            recordType = recordType.ToUpper();
            if(mappers.ContainsKey(recordType))
            {
                return mappers[recordType];
            }
            var map = new Dictionary<string, string>();

            var stringMap = ConfigurationManager.AppSettings[recordType + MapperSuffix];
            if(stringMap == null || stringMap == AllKeyMap)
            {
                map[AllKeyMap] = AllKeyMap;
            }
            else
            {
                foreach(var item in stringMap.Split(';'))
                {
                    var pair = item.Split('=');
                    string key = pair[0].Trim().ToUpper();
                    string value = AllKeyMap;

                    if (pair.Count() > 1)
                    {
                        value = pair[1].Trim();
                        if (String.IsNullOrWhiteSpace(value))
                        {
                            value = AllKeyMap;
                        }
                    }
                    map[key] = value;
                }
            }

            mappers[recordType] = map;
            return map;
        }

        private static void GetConfigurationFromDatabase()
        {
            using (SqlConnection sqlConnection = new SqlConnection(DbConnectionString))
            {
                sqlConnection.Open();
                using (SqlCommand cmd = new SqlCommand(Configurator.SqlGetSettings, sqlConnection))
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    QueueConfig.Instance.UserName = (string)reader["UserName"];
                    QueueConfig.Instance.Password = (string)reader["Password"];
                    QueueConfig.Instance.ConnectionString = (string)reader["ConnectionString"];
                    QueueConfig.Instance.TopicName = (string)reader["TopicName"];
                    QueueConfig.Instance.EscapeValue = (string)reader["EscapeValue"];
                    QueueConfig.Instance.EscapeValueLine = (string)reader["EscapeValueLine"];
                    QueueConfig.Instance.CheckCmdsIntervalMilis = long.Parse((string)reader["CheckCmdsIntervalMilis"]);
                    QueueConfig.Instance.NewQueryLimit = long.Parse((string)reader["NewQueryLimit"]);
                    QueueConfig.Instance.WebServiceUserName = (string)reader["WebServiceUserName"];
                    QueueConfig.Instance.WebServicePassword = (string)reader["WebServicePassword"];
                    QueueConfig.Instance.WebServiceURL = (string)reader["WebServiceURL"];
                    QueueConfig.Instance.ActiveMQConnectionId = (string)reader["ActiveMQConnectionId"];
                    QueueConfig.Instance.KeepAliveIntervalS = long.Parse((string)reader["KeepAliveIntervalS"]);
                }

                using (SqlCommand cmd = new SqlCommand(Configurator.SqlGetSupportedEventsTypes, sqlConnection))
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    SupportedEventTypes = new List<String>();
                    while (reader.Read())
                    {
                        SupportedEventTypes.Add((string)reader[0]);
                    }
                }
            }
        }
    }
}
