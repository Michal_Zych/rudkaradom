﻿namespace M2MConnectorService.impicode.Main
{
    public struct NewQuery
    {
        public string RequestId;
        public string QueryType;
        public string RecordData;

        public NewQuery(string requestId, string queryType, string recordData)
        {
            RequestId = requestId;
            QueryType = queryType;
            RecordData = recordData;
        }
    }
}
