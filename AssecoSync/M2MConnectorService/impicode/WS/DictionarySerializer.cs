﻿using M2MConnectorService.impicode.Main;
using M2MConnectorService.PatientService2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace M2MConnectorService.impicode
{
    public static class DictionarySerializer
    {
        private static string NullableSufix = "Specified";
        private static string BoolValueYes = "1";
        private static string BoolValueNo = "0";
        private static string ValueSpecifiedIndicator = BoolValueYes;
        private static string NullValue = "null";

        public static IDictionary<string, string> ToDictionary(this object param, string keyIfNull = "", string prefix = "")
        {
            IDictionary<string, string> result = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            if (param == null)
            {
                result.Add(keyIfNull, NullValue);
            }
            else
            {
                var properties = param.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

                foreach (var property in properties)
                {
                    var value = property.GetValue(param, null);
                    if (value == null)
                    {
                        result.Add(prefix + property.Name, NullValue);
                    }
                    else if (value.GetType() == typeof(bool))
                    {
                        result.Add(prefix + property.Name, SerializeBool((bool)value));
                    }
                    else  if(property.PropertyType.IsArray)
                    {
                       Array a = (Array)property.GetValue(param);
                        for(int i = 0; i< a.Length; i++)
                        {
                            object o = a.GetValue(i);
                            var d = ToDictionary(o, property.Name, prefix + property.Name + "[" + i.ToString() + "].");
                            foreach (var pair in d)
                            {
                                result.Add(pair.Key, pair.Value);
                            }
                        }
                    }
                    else if (value.ToString() == value.GetType().ToString())
                    {
                        var d = ToDictionary(value, property.Name, prefix + property.Name + ".");
                        foreach (var pair in d)
                        {
                            result.Add(pair.Key, pair.Value);
                        }
                    }
                    else
                    {
                        result.Add(prefix + property.Name, value.ToString());
                    }
                }
            }
            return result.ConvertNullable();
        }

        public static IDictionary<string, string> ConvertNullable(this IDictionary<string, string> dic)
        {
            var nullables = dic.Keys.Where(key => dic.ContainsKey(key + NullableSufix)).ToList();
            foreach (var key in nullables)
            {
                var nullKey = key + NullableSufix;
                if (dic[nullKey] != ValueSpecifiedIndicator)
                {
                    dic[key] = NullValue;
                }
                dic.Remove(nullKey);
            }
            return dic;
        }

        public static Dictionary<string, string> MapDictionary(this IDictionary<string, string> dic, IDictionary<string, string> keyMap)
        {
            Dictionary<string, string> result = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            foreach (var pair in keyMap)
            {
                if (pair.Key == Configurator.AllKeyMap)
                {
                    foreach (var item in dic)
                    {
                        result[item.Key] = item.Value;
                    }
                }
                else
                {
                    if (dic.ContainsKey(pair.Key))
                    {
                        var key = pair.Key;
                        if (!String.IsNullOrWhiteSpace(pair.Value) && pair.Value != Configurator.AllKeyMap)
                        {
                            key = pair.Value;
                        }
                        result[key] = dic[pair.Key];
                    }
                    else
                    {
                        if(pair.Value == Configurator.AllKeyMap)
                        {
                            result[pair.Key] = NullValue;
                        }
                        else
                        {
                            result[pair.Value] = NullValue;
                        }
                    }
                }
            }
            return result;
        }

        public static string ToString(this IDictionary<string, string> dic, IDictionary<string, string> keyMap)
        {
            return M2MSerializer.Serialize(dic.MapDictionary(keyMap));
        }

        private static string ToStringValue(object value)
        {
            if (value == null)
            {
                return NullValue;
            }

            if (value.GetType() == typeof(bool))
            {
                return SerializeBool((bool)value);
            }

            return value.ToString();
        }


        private static String SerializeBool(bool b)
        {
            return b ? BoolValueYes : BoolValueNo;
        }

        private static bool DeserializeBool(String b)
        {
            return b.Equals(BoolValueYes);
        }
    }
}
