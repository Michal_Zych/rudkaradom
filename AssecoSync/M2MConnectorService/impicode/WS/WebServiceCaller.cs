﻿using System;
using System.Collections.Generic;
using M2MConnectorService.VisitService;
using System.Reflection;
using System.Linq;

namespace M2MConnectorService.impicode
{
    class WebServiceCaller
    {
        private AssecoWebService webService;

        public WebServiceCaller(AssecoWebService webService)
        {
            this.webService = webService;
        }

        public IDictionary<string, string> GetPatientLocalization(IDictionary<string, string> pparams)
        {
            return GetPatientLocalizationFromId(pparams["PATIENT_ID"]);
        }

        public IDictionary<string, string> GetPatientLocalizationFromId(String patientId)
        {
            PatientService2.GetPatientLocalization request = new PatientService2.GetPatientLocalization
            {
                PatientId = patientId
            };
            PatientService2.GetPatientLocalizationResponse response = webService.MainClient.GetPatientLocalization(request);
            PatientService2.patientLocalization loc = response.PatientLocalization;

            return loc.ToDictionary("PatientLocalization");
        }

        public IDictionary<string, string> GetPatientDemographics(IDictionary<string, string> pparams)
        {
            PatientService2.patient[] response = GetPatient(pparams["PATIENT_ID"]);

            if(response.Length > 0)
            {
                return response[0].ToDictionary();
                            //.Add(response[0].identity.patientIdentity.ToDictionary(), DictionarySerializer.PatientIdentityKey)
                            //.SelectKeys(DictionarySerializer.PatientDemographicsKeysMap);
            }
            else
            {
                return ((PatientService2.patient)null).ToDictionary("Patient");
            };
        }

        public IDictionary<string, string> GetPatientForTagEvent(String patientId)
        {
            PatientService2.patient[] response = GetPatient(patientId);
            Dictionary<String, String> ret = new Dictionary<string, string>();
            if (response.Length > 0)
            {
                PatientService2.patient patient = response[0];
                ret["TAG_ID"] = patient.tagId;
            }
            else
            {
                ret["TAG_ID"] = null;
            }
            return ret;
        }

        public PatientService2.patient[] GetPatient(string patientId)
        {
            PatientService2.GetPatients request = new PatientService2.GetPatients
            {
                PatientParams = new PatientService2.patientParams()
            };
            request.PatientParams.patientIdSpecified = true;
            long o;
            long.TryParse(patientId, out o);
            request.PatientParams.patientId = o;
            PatientService2.patient[] response = webService.MainClient.GetPatients(request);
            return response;
        }

        public IDictionary<string, string> ModifyTagId(IDictionary<string, string> pparams)
        {
            PatientService2.ModifyTagId request = new PatientService2.ModifyTagId
            {
                ModificationDate = DateTime.Parse(pparams["MODIFICATION_DATE"]),
                PatientId = pparams["PATIENT_ID"],
                TagId = pparams["TAG_ID"],
                UserId = pparams["USER_ID"]
            };
            PatientService2.ModifyTagIdResponse result = webService.MainClient.ModifyTagId(request);
            bool res = result.Result;
            Dictionary<String, String> ret = new Dictionary<string, string>
            {
                ["RESULT"] = SerializeBool(res)
            };
            return ret;
        }

        public IDictionary<string, string> AskUser(String userId)
        {
            UserService.GetByTechnicalIdentifier request = new UserService.GetByTechnicalIdentifier
            {
                UserParam = userId
            };
            UserService.GetByTechnicalIdentifierResponse response = webService.UserClient.GetByTechnicalIdentifier(request);

            return response.user.ToDictionary();
        }


       internal IDictionary<string, string> AskVisit(string visitId)
        {
            VisitService.GetById request = new VisitService.GetById
            {
                VisitId = long.Parse(visitId),
                VisitIdSpecified = true
            };
            VisitService.GetByIdResponse response = webService.VisitClient.GetById(request);
            return response.@return.ToDictionary("VisitId");
        }

                



        public IDictionary<string, string>[] GetUsers(String login, String name, String surename, String active)
        {
            UserService.GetUsers request = new UserService.GetUsers
            {
                UserParam = new UserService.userParam
                {
                    user = new UserService.user
                    {
                        login = login,
                        name = name,
                        surename = surename
                    }
                }
            };

            if (active != null && !active.Equals(""))
            {
                request.UserParam.user.active = DeserializeBool(active);
                request.UserParam.user.activeSpecified = true;
            }
            UserService.user[] response = webService.UserClient.GetUsers(request);
            IDictionary<string, string>[] retArray = new Dictionary<string, string>[response.Length];
            for (int i = 0; i < response.Length; ++i)
            {
                retArray[i] = response[i].ToDictionary("User");
            }
            return retArray;
        }

        public IDictionary<string, string> GetVisit(string visitId)
        {
            VisitService.GetById request = new VisitService.GetById();
            request.VisitIdSpecified = true;
            long o;
            long.TryParse(visitId, out o);
            request.VisitId = o;

            var response = webService.VisitClient.GetById(request);

            return response.@return.ToDictionary();
        }


        private string GetNullableValue(string value, bool isSpecified)
        {
            if (!isSpecified) return "null";
            return value;
        }

        public String SerializeBool(bool b)
        {
            return b ? "1" : "0";
        }

        public bool DeserializeBool(String b)
        {
            return b.Equals("1");
        }
    }
}
