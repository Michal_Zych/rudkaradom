﻿using M2MConnectorService.impicode;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.Text;
using System.Threading.Tasks;

namespace M2MConnectorService
{
    public class AssecoWebService
    {
        public PatientService2.PatientClient MainClient { get; private set; }
        public UserService.User1Client UserClient { get; private set; }
        public VisitService.VisitClient VisitClient { get; private set; }

        private string UserName { get; set; }
        private string Password { get; set; }
        private string WebServiceURL { get; set; }

        public AssecoWebService(string userName, string password, string webServiceURL)
        {
            UserName = userName;
            Password = password;
            WebServiceURL = webServiceURL;
        }

        public bool Connect()
        {

            CustomBinding mainBinding = GetBinding();
            EndpointAddress mainEndpoint = new EndpointAddress(WebServiceURL + "PatientService2/");
            PatientService2.PatientClient client = new PatientService2.PatientClient(mainBinding, mainEndpoint);
            ConfigureClient(client);
            MainClient = client;

            CustomBinding userBinding = GetBinding();
            EndpointAddress userEndpoint = new EndpointAddress(WebServiceURL + "UserService/");
            UserService.User1Client userClient = new UserService.User1Client(userBinding, userEndpoint);
            ConfigureClient(userClient);
            UserClient = userClient;

            CustomBinding visitBinding = GetBinding();
            EndpointAddress visitEndpoint = new EndpointAddress(WebServiceURL + "VisitService/");
            VisitService.VisitClient visitClient = new VisitService.VisitClient(visitBinding, visitEndpoint);
            ConfigureClient(visitClient);
            VisitClient = visitClient;

            return TestConnection();
        }

        private void ConfigureClient<T>(ClientBase<T> client) where T : class
        {
            client.ChannelFactory.Endpoint.Behaviors.Remove<System.ServiceModel.Description.ClientCredentials>();
            client.ChannelFactory.Endpoint.Behaviors.Add(new CustomCredentials());
            client.ClientCredentials.UserName.UserName = UserName;
            client.ClientCredentials.UserName.Password = Password;
            client.ClientCredentials.SupportInteractive = false;
        }

        private CustomBinding GetBinding()
        {

            var security = TransportSecurityBindingElement.CreateUserNameOverTransportBindingElement();
            security.AllowInsecureTransport = true;
            security.IncludeTimestamp = false;
            security.DefaultAlgorithmSuite = SecurityAlgorithmSuite.Basic256;
            security.MessageSecurityVersion = MessageSecurityVersion.WSSecurity10WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10;


            var encoding = new TextMessageEncodingBindingElement
            {
                MessageVersion = MessageVersion.Soap11
            };

            var transport = new HttpTransportBindingElement
            {
                MaxReceivedMessageSize = 200000000 // 200 megs
            };
            CustomBinding binding = new CustomBinding();
            binding.Elements.Add(security);
            binding.Elements.Add(encoding);
            binding.Elements.Add(transport);
            return binding;
        }


        private bool TestConnection()
        {
            bool result = true;
            QueueConfig.Log.Info("Test connection: PatientService2.");
            try
            {
                WebServiceCaller wsCaller = new WebServiceCaller(this);
                PatientService2.patient[] response = wsCaller.GetPatient("22");
                QueueConfig.Log.Info("\tTest response count: " + response.Count());
                QueueConfig.Log.Info("WebService PatientService2 connection ok.");
            }
            catch (Exception e)
            {
                QueueConfig.Log.Info("WebService PatientService2 connection failed.");
                QueueConfig.Log.Info("\t" + e.Message);

                result = false;
            }

            QueueConfig.Log.Info("Test connection: UserService.");
            try
            {
                WebServiceCaller wsCaller = new WebServiceCaller(this);
                var response = wsCaller.GetUsers("ADMIN", null, null, null);
                QueueConfig.Log.Info("\tTest response count: " + response.Count());
                QueueConfig.Log.Info("WebService UserService connection ok.");
            }
            catch (Exception e)
            {
                QueueConfig.Log.Info("WebService UserService connection failed.");
                QueueConfig.Log.Info("\t" + e.Message);
                result = false;
            }

            QueueConfig.Log.Info("Test connection: VisitService.");
            try
            {
                WebServiceCaller wsCaller = new WebServiceCaller(this);
                var response = wsCaller.GetVisit("1");
                QueueConfig.Log.Info("\tTest response count: " + response.Count());
                QueueConfig.Log.Info("WebService VisitService connection ok.");
            }
            catch (Exception e)
            {
                QueueConfig.Log.Info("WebService VisitService connection failed.");
                QueueConfig.Log.Info("\t" + e.Message);
                result = false;
            }

            return result;
        }
    }

    public class CustomCredentials : ClientCredentials
    {
        public CustomCredentials()
        { }

        protected CustomCredentials(CustomCredentials cc)
            : base(cc)
        { }

        public override System.IdentityModel.Selectors.SecurityTokenManager CreateSecurityTokenManager()
        {
            return new CustomSecurityTokenManager(this);
        }

        protected override ClientCredentials CloneCore()
        {
            return new CustomCredentials(this);
        }
    }

    public class CustomSecurityTokenManager : ClientCredentialsSecurityTokenManager
    {
        public CustomSecurityTokenManager(CustomCredentials cred)
            : base(cred)
        { }

        public override System.IdentityModel.Selectors.SecurityTokenSerializer CreateSecurityTokenSerializer(System.IdentityModel.Selectors.SecurityTokenVersion version)
        {
            return new CustomTokenSerializer(System.ServiceModel.Security.SecurityVersion.WSSecurity11);
        }
    }

    public class CustomTokenSerializer : WSSecurityTokenSerializer
    {
        public CustomTokenSerializer(SecurityVersion sv)
            : base(sv)
        { }

        protected override void WriteTokenCore(System.Xml.XmlWriter writer,
                                                System.IdentityModel.Tokens.SecurityToken token)
        {
            UserNameSecurityToken userToken = token as UserNameSecurityToken;

            string tokennamespace = "o";
            DateTime created = DateTime.UtcNow;

            //string createdStr = created.ToString("yyyy-MM-ddThh:mm:ss.fffZ");
            string createdStr = created.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");

            //yyyy - MM - ddTHH:mm: ss.fffZ
            //createdStr = "2018-04-23T08:00:00.000Z";

            // unique Nonce value - encode with SHA-1 for 'randomness'
            // in theory the nonce could just be the GUID by itself
            string phrase = Guid.NewGuid().ToString();
            var nonce = GetSHA1String(phrase);

            // in this case password is plain text
            // for digest mode password needs to be encoded as:
            // PasswordAsDigest = Base64(SHA-1(Nonce + Created + Password))
            // and profile needs to change to
            //string password = GetSHA1String(nonce + createdStr + userToken.Password);
            string password = userToken.Password;

            writer.WriteRaw(string.Format(
            "<{0}:UsernameToken u:Id=\"" + token.Id +
            "\" xmlns:u=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" +
            "<{0}:Username>" + userToken.UserName + "</{0}:Username>" +
            //"<{0}:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">" +
            "<{0}:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">" +
            CreatePasswordDigest(Convert.FromBase64String(nonce), createdStr, password) + "</{0}:Password>" +
            //password + "</{0}:Password>" +
            "<{0}:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">" +
            nonce + "</{0}:Nonce>" +
            "<u:Created>" + createdStr + "</u:Created></{0}:UsernameToken>", tokennamespace));
        }

        protected string GetSHA1String(string phrase)
        {
            SHA1CryptoServiceProvider sha1Hasher = new SHA1CryptoServiceProvider();
            byte[] hashedDataBytes = sha1Hasher.ComputeHash(Encoding.UTF8.GetBytes(phrase));
            return Convert.ToBase64String(hashedDataBytes);
        }
        public string CreatePasswordDigest(byte[] nonce, string createdTime, string password)
        {
            // combine three byte arrays into one
            byte[] time = Encoding.UTF8.GetBytes(createdTime);
            byte[] pwd = Encoding.UTF8.GetBytes(password);
            byte[] operand = new byte[nonce.Length + time.Length + pwd.Length];
            Array.Copy(nonce, operand, nonce.Length);
            Array.Copy(time, 0, operand, nonce.Length, time.Length);
            Array.Copy(pwd, 0, operand, nonce.Length + time.Length, pwd.Length);

            // create the hash
            var sha1Hasher = new SHA1CryptoServiceProvider();
            byte[] hashedDataBytes = sha1Hasher.ComputeHash(operand);
            return Convert.ToBase64String(hashedDataBytes);
        }

    }
}