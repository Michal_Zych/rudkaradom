﻿using Apache.NMS;
using Apache.NMS.ActiveMQ.Commands;
using System;

namespace M2MConnectorService.impicode
{
    class QueueHandler
    {
        public static void ConsumeMessage(IMessage innerMessage)
        {
            try
            {
                PrintLogHeader();
                if (innerMessage is ActiveMQMapMessage mapMessage)
                {
                    QueueConfig.Log.Info("message id: " + mapMessage.NMSMessageId);
                    IPrimitiveMap headerMap = mapMessage.Properties;
                    String eventDate = headerMap.GetString("EVENT_DATE");
                    String eventType = headerMap.GetString("EVENT_TYPE");
                    String originSystem = headerMap.GetString("ORIGIN_SYSTEM");

                    QueueConfig.Log.Info("EVENT DATE: " + eventDate);
                    QueueConfig.Log.Info("EVENT TYPE: " + eventType);
                    QueueConfig.Log.Info("ORIGIN SYSTEM" + originSystem);
                    IPrimitiveMap messageMap = mapMessage.Body;
                    PrintMapToLog(messageMap);
                } else if (innerMessage is ITextMessage textMessage)
                {
                    QueueConfig.Log.Info("debug message");
                    QueueConfig.Log.Info(textMessage.Text);
                } else
                {
                    QueueConfig.Log.Info("unrecognized message type");
                }
                QueueConfig.Log.Info("message raw");
                QueueConfig.Log.Info(innerMessage.ToString());
                QueueConfig.Log.Info("End message processing...");
            }
            catch (Exception e)
            {
                QueueConfig.Log.Info(e.Message);
                QueueConfig.Log.Info("queuehandler stack", e);
            }
        }

        private static void PrintLogHeader()
        {
            QueueConfig.Log.Info("message received");
            QueueConfig.Log.Info(DateTime.Now);
        }

        private static void PrintMapToLog(IPrimitiveMap map)
        {
            QueueConfig.Log.Info("MAP");
            QueueConfig.Log.Info(map);
            QueueConfig.Log.Info("\n\nMap begin");
            foreach (String key in map.Keys)
            {
                QueueConfig.Log.Info("key: " + key);
                QueueConfig.Log.Info("value: " + map.GetString(key));
                QueueConfig.Log.Info("value: " + map.GetLong(key));
                QueueConfig.Log.Info("value: " + map.GetInt(key));
                QueueConfig.Log.Info("value: " + map.GetDictionary(key));

            }
            QueueConfig.Log.Info("Map end\n");
        }
    }
}
