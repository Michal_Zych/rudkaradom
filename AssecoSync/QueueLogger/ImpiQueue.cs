﻿using Apache.NMS;
using System;
using System.Timers;

namespace M2MConnectorService.impicode
{
    class ImpiQueue
    {
        private IConnection connection;
        private ISession session;

        public ImpiQueue()
        {
        }

        public void Start()
        {
            try
            {
                QueueConfig.Log.Info("About to connect to " + QueueConfig.Instance.ConnectionString);

                IConnectionFactory factory = new NMSConnectionFactory(QueueConfig.Instance.ConnectionString);

                connection = factory.CreateConnection(QueueConfig.Instance.UserName, QueueConfig.Instance.Password);
                session = connection.CreateSession(AcknowledgementMode.AutoAcknowledge);
                connection.ExceptionListener += (Exception eee) =>
                {
                    QueueConfig.Log.Info("Queue connection interrupted");
                    try
                    {
                        session.Close();
                        connection.Close();
                    }
                    catch (Exception e)
                    {
                        QueueConfig.Log.Info("close failed", e);
                    }
                    Start();
                };
                connection.Start();
                ListenTopic(QueueConfig.Instance.TopicName);
                QueueConfig.Log.Info("Successfully connected " + QueueConfig.Instance.ConnectionString);
            }
            catch (Exception e)
            {
                try
                {
                    session.Close();
                    connection.Close();
                }
                catch (Exception ee)
                {
                }
                QueueConfig.Log.Info("queue connection start failed", e);
                QueueConfig.Log.Info("Retry in 60 seconds");
                new MainTimer().StartTimer(this);
            }
        }

        class MainTimer
        {

            private ImpiQueue impiQueue;
            public void StartTimer(ImpiQueue impiQueue)
            {
                this.impiQueue = impiQueue;
                System.Timers.Timer aTimer = new System.Timers.Timer();
                aTimer.Elapsed += OnTimedEvent;
                aTimer.Interval = 60 * 1000;
                aTimer.AutoReset = false;
                aTimer.Enabled = true;
            }

            public void OnTimedEvent(Object source, ElapsedEventArgs e)
            {
                impiQueue.Start();
            }
        }

        private void ListenTopic(String topicName)
        {
            IMessageConsumer consumer;
            if (QueueConfig.Instance.ChannelType == QueueConfig.TType.QUEUE)
            {
                IQueue topic = session.GetQueue(topicName);
                consumer = session.CreateConsumer(topic);
            } else if (QueueConfig.Instance.ChannelType == QueueConfig.TType.TOPIC)
            {
                ITopic topic = session.GetTopic(topicName);
                consumer = session.CreateConsumer(topic);
            } else
            {
                consumer = null;
            }
            
            consumer.Listener += delegate (IMessage innerMessage)
            {
                QueueHandler.ConsumeMessage(innerMessage);
            };
        }
    }
}
