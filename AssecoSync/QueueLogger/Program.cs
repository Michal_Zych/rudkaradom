﻿using log4net.Config;
using M2MConnectorService.impicode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QueueLogger
{
    class Program
    {
        public static void Main(string[] args)
        {
            XmlConfigurator.Configure();
            Console.WriteLine("QUEUE HANDLER START");
            Console.WriteLine("QUEUE HANDLER START");
            Console.WriteLine("QUEUE HANDLER START");
            GetProductionConfig();
            //GetLocalConfig();
            
            StartQueues();
            QueueConfig.Log.Info("start listening");
            Console.WriteLine("type quit to quit.");
            String line = "";
            while (!line.StartsWith("quit"))
            {
                line = Console.ReadLine();
            }
            Console.WriteLine("Program quit...");
        }

        public static void StartQueues()
        {
            ImpiQueue impiQueue = new ImpiQueue();
            impiQueue.Start();
        }

        public static void GetProductionConfig()
        {
            QueueConfig.SetConfig("M2M", "Int3Grm@Mt34m", "activemq:tcp://192.168.3.89:61616", "pl.asseco.poz.integration.events", QueueConfig.TType.TOPIC, "&quot;", 10000);
        }

        public static void GetProductionQueueConfig()
        {
            QueueConfig.SetConfig("M2M", "Int3Grm@Mt34m", "activemq:tcp://192.168.3.89:61616", "pl.asseco.mpi.messages", QueueConfig.TType.QUEUE, "&quot;", 10000);
        }

        public static void GetLocalConfig()
        {
            QueueConfig.SetConfig("", "", "tcp://localhost:61616", "TEST.FOO", QueueConfig.TType.TOPIC, "&quot;", 10000);
        }
    }
}
