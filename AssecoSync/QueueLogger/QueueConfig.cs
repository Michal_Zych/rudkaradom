﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2MConnectorService.impicode
{
    class QueueConfig
    {
        public static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string UserName { get; private set; }
        public string Password { get; private set; }
        public string ConnectionString { get; private set; }
        public string TopicName { get; private set; }
        public enum TType { QUEUE, TOPIC };
        public TType ChannelType { get; private set; }
        public string EscapeValue { get; private set; }
        public long CheckCmdsIntervalMilis { get; private set; }

        private static QueueConfig instance;

        private QueueConfig() { }

        public static QueueConfig Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new QueueConfig();
                }
                return instance;
            }
        }

        public static void SetConfig(string userName, string password, string connectionString, string topicName, TType channelType, string escapeValue, long timerMilis)
        {
            instance = new QueueConfig
            {
                UserName = userName,
                Password = password,
                ConnectionString = connectionString,
                TopicName = topicName,
                ChannelType = channelType,
                EscapeValue = escapeValue,
                CheckCmdsIntervalMilis = timerMilis,
            };
        }
    }
}
