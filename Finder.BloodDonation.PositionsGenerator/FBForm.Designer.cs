﻿namespace Finder.BloodDonation.PositionsGenerator
{
    partial class FBForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.T1H = new System.Windows.Forms.TextBox();
            this.Device = new System.Windows.Forms.TextBox();
            this.Interval = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.positionTimer = new System.Windows.Forms.Timer(this.components);
            this.T1L = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.T2H = new System.Windows.Forms.TextBox();
            this.T2L = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.listGenerated = new System.Windows.Forms.ListBox();
            this.txtPoints = new System.Windows.Forms.TextBox();
            this.txtSamplesCount = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // T1H
            // 
            this.T1H.Location = new System.Drawing.Point(59, 38);
            this.T1H.Name = "T1H";
            this.T1H.Size = new System.Drawing.Size(73, 20);
            this.T1H.TabIndex = 1;
            // 
            // Device
            // 
            this.Device.Location = new System.Drawing.Point(59, 64);
            this.Device.Name = "Device";
            this.Device.Size = new System.Drawing.Size(73, 20);
            this.Device.TabIndex = 2;
            // 
            // Interval
            // 
            this.Interval.Location = new System.Drawing.Point(183, 64);
            this.Interval.Name = "Interval";
            this.Interval.Size = new System.Drawing.Size(73, 20);
            this.Interval.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "T1H";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Device";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(135, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Interval";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(19, 116);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(113, 23);
            this.btnStart.TabIndex = 6;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(138, 116);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(118, 23);
            this.btnStop.TabIndex = 7;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // positionTimer
            // 
            this.positionTimer.Tick += new System.EventHandler(this.positionTimer_Tick);
            // 
            // T1L
            // 
            this.T1L.Location = new System.Drawing.Point(59, 12);
            this.T1L.Name = "T1L";
            this.T1L.Size = new System.Drawing.Size(73, 20);
            this.T1L.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "T1L";
            // 
            // T2H
            // 
            this.T2H.Location = new System.Drawing.Point(183, 38);
            this.T2H.Name = "T2H";
            this.T2H.Size = new System.Drawing.Size(73, 20);
            this.T2H.TabIndex = 4;
            // 
            // T2L
            // 
            this.T2L.Location = new System.Drawing.Point(183, 12);
            this.T2L.Name = "T2L";
            this.T2L.Size = new System.Drawing.Size(73, 20);
            this.T2L.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(149, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "T1H";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(151, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "T1L";
            // 
            // listGenerated
            // 
            this.listGenerated.FormattingEnabled = true;
            this.listGenerated.Location = new System.Drawing.Point(12, 145);
            this.listGenerated.Name = "listGenerated";
            this.listGenerated.Size = new System.Drawing.Size(244, 186);
            this.listGenerated.TabIndex = 5;
            // 
            // txtPoints
            // 
            this.txtPoints.Location = new System.Drawing.Point(59, 90);
            this.txtPoints.Name = "txtPoints";
            this.txtPoints.Size = new System.Drawing.Size(73, 20);
            this.txtPoints.TabIndex = 0;
            // 
            // txtSamplesCount
            // 
            this.txtSamplesCount.Location = new System.Drawing.Point(183, 90);
            this.txtSamplesCount.Name = "txtSamplesCount";
            this.txtSamplesCount.Size = new System.Drawing.Size(73, 20);
            this.txtSamplesCount.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(132, 93);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Samples";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Points";
            // 
            // FBForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(272, 346);
            this.Controls.Add(this.listGenerated);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Interval);
            this.Controls.Add(this.Device);
            this.Controls.Add(this.T2L);
            this.Controls.Add(this.txtSamplesCount);
            this.Controls.Add(this.txtPoints);
            this.Controls.Add(this.T1L);
            this.Controls.Add(this.T2H);
            this.Controls.Add(this.T1H);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FBForm";
            this.Text = "FB";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox T1H;
        private System.Windows.Forms.TextBox Device;
        private System.Windows.Forms.TextBox Interval;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Timer positionTimer;
        private System.Windows.Forms.TextBox T1L;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox T2H;
        private System.Windows.Forms.TextBox T2L;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox listGenerated;
        private System.Windows.Forms.TextBox txtPoints;
        private System.Windows.Forms.TextBox txtSamplesCount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}

