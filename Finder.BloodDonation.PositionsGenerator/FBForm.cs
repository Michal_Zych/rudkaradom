﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Finder.BloodDonation.PositionsGenerator
{
    public partial class FBForm : Form
    {
        Random rand = new Random();
        List<double> Points = new List<double>();
        int current_point_index = 0;
        int points_per_index = 0;
        bool points_mode = false;
        bool back_mode = false;

        public FBForm()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStop_Click(sender, e);
            listGenerated.Items.Clear();
            Points.Clear();
            current_point_index = 0;
            points_mode = false;
            points_per_index = 0;
            back_mode = false;
            if (txtPoints.Text.Length > 0)
            {
                Points = (from p in txtPoints.Text.Split(';') select Convert.ToDouble(p)).ToList();
                if (Points.Count > 0)
                {
                    points_mode = true;
                }
            }
            positionTimer.Interval = Convert.ToInt32(Interval.Text) * 1000;
            positionTimer.Start();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            positionTimer.Stop();
        }

        private void positionTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                //string connectionString = "Data Source=192.168.125.164;Initial Catalog=finderblood;User ID=finder;Password=finder1230-=;application name=FinderBlood";
                string connectionString = "data source=GRZESIEK-DELL\\SQLEXPRESS;initial catalog=FinderBlood;Integrated Security=true;application name=FinderBlood";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = connection.CreateCommand();
                    double t1 = 0.0;
                    double t2 = 0.0;
                    if (points_mode)
                    {
                        if (current_point_index < Points.Count)
                        {
                            if (current_point_index < 0)
                            {
                                btnStop_Click(sender, e);
                                return;
                            }

                            int pn = rand.Next(-1, 1);
                            if (pn == 0)
                            {
                                pn = 1;
                            }
                            t1 = Points[current_point_index] + (rand.NextDouble() * pn);
                            t2 = Points[current_point_index] + (rand.NextDouble() * pn);
                            points_per_index++;

                            if (points_per_index > Convert.ToInt32(txtSamplesCount.Text))
                            {
                                if (!back_mode)
                                {
                                    current_point_index++;
                                    points_per_index = 0;
                                }
                                else
                                {
                                    current_point_index--;
                                    points_per_index = 0;
                                }
                            }
                        }
                        else
                        {
                            back_mode = true;
                            current_point_index = Points.Count - 1;
                            points_per_index = 0;
                        }
                    }
                    else
                    {
                        t1 = rand.Next(Convert.ToInt32(T1L.Text), Convert.ToInt32(T1H.Text)) + rand.NextDouble();
                        t2 = rand.Next(Convert.ToInt32(T2L.Text), Convert.ToInt32(T2H.Text)) + rand.NextDouble();
                    }
                    command.CommandText = string.Format("EXEC Comm_StoreData @deviceId = {0}, @timestamp = '{1}', @t1 = {2}, @t2 = {3}, @alarming = 0", Device.Text, DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString(), t1.ToString().Replace(',', '.'), t2.ToString().Replace(',', '.'));
                    listGenerated.Items.Insert(0, string.Format("T1:{0} T2:{1}", t1, t2));
                    connection.Open();
                    command.ExecuteScalar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
