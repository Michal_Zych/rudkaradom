﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Finder.BloodDonation.Integration.Service
{
	public partial class MainService : ServiceBase
	{
		private Controller _controller;

		public MainService()
		{
			log4net.Config.XmlConfigurator.Configure(new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "logging.config"));

			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			_controller = new Controller(new ControllerConfiguration());

			_controller.Start();
		}

		protected override void OnStop()
		{
			_controller.Stop();
		}
	}
}
