﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FinderFX.Web.Core.Configuration;
using Finder.BloodDonation.Model.Services;
using Finder.BloodDonation.Model.Domain;
using Finder.BloodDonation.Model.Dto;
using Finder.BloodDonation.Model.Authentication;
using FinderFX.Web.Core.Authentication;
using NHibernate;
using FinderFX.Web.Core.Communication;
using Finder.BloodDonation.Model.Activities;

namespace Finder.BloodDonation.Model.Test
{
	/// <summary>
	/// Summary description for UnitTest1
	/// </summary>
	[TestClass]
	public class UnitTest1
	{
		public UnitTest1()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		//
		// You can use the following additional attributes as you write your tests:
		//
		 //Use ClassInitialize to run code before running the first test in the class
		 [ClassInitialize()]
		 public static void MyClassInitialize(TestContext testContext) 
		 {
			 GlobalConfiguration.Initialize(new BloodConfiguration());
		 }
		//
		// Use ClassCleanup to run code after all tests in a class have run
		// [ClassCleanup()]
		// public static void MyClassCleanup() { }
		//
		// Use TestInitialize to run code before running each test 
		// [TestInitialize()]
		// public void MyTestInitialize() { }
		//
		// Use TestCleanup to run code after each test has run
		// [TestCleanup()]
		// public void MyTestCleanup() { }
		//
		#endregion

		[TestMethod]
		public void TestMethod1()
		{
			//
			try
			{
				AuthenticationManager authManager = GlobalConfiguration.Current.AuthManager as AuthenticationManager;
				BloodyUser user = authManager.LoadUser(1) as BloodyUser;
				//ActivityHelper.SaveActivity<DBUser,DBUserDTO>(1, ActivityTypes.UnitEditing);

				//DBUserDTO dto = ActivityHelper.GetActivityObject<DBUserDTO>(5);
				//ActivityHelper.Restore<DBUserDTO, DBUser>(5, 1);
				
				PermissionsService proxy = new PermissionsService();
				


				IList<DBUserDTO> users = proxy.GetUsers(1, 4,user);
				int i = 234;

			}
			catch (Exception exc)
			{

			}
		}
	}
}
