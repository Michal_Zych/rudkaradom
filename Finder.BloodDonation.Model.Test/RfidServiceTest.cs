﻿using Finder.BloodDonation.Model.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Finder.BloodDonation.Model.Dto;
using System.Collections.Generic;
using FinderFX.Web.Core.Configuration;
using Finder.BloodDonation.Model.Domain;
using Moq;
using Finder.BloodDonation.Model.Authentication;
using FinderFX.Web.Core.Authentication;

namespace Finder.BloodDonation.Model.Test
{
    
    
    /// <summary>
    ///This is a test class for RfidServiceTest and is intended
    ///to contain all RfidServiceTest Unit Tests
    ///</summary>
    [TestClass]
    public class RfidServiceTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        //[TestMethod]
        //public void GetPrepCountByUnitMockedTest()
        //{
        //    Mock<BloodConfiguration> mockedConfig = new Mock<BloodConfiguration>();

        //    BloodAuthenticationManager bam = new BloodAuthenticationManager();
        //    mockedConfig.Setup(m => m.GetAuthenticationManager()).Returns(bam);

        //    GlobalConfiguration.Initialize(mockedConfig.Object);

        //    RfidService target = new RfidService(); // TODO: Initialize to an appropriate value
        //    int unitId = 2; // TODO: Initialize to an appropriate value
        //    IList<PrepTypeDto> expected = null; // TODO: Initialize to an appropriate value
        //    IList<PrepTypeDto> actual;
        //    actual = target.GetPrepCountByUnit(unitId);

        //    Assert.AreEqual(expected, actual);
        //}

		/// <summary>
		///A test for GetPrepHistory
		///</summary>
		[TestMethod]
		public void GetPrepHistoryTest()
		{
			var bc = new BloodConfiguration();
			GlobalConfiguration.Initialize(bc);

			RfidService target = new RfidService(); // TODO: Initialize to an appropriate value
			int unitId = 220; // TODO: Initialize to an appropriate value
			DateTime startDate = new DateTime(2011, 12, 1, 0, 0, 0); // TODO: Initialize to an appropriate value
			DateTime endDate = new DateTime(2011, 12, 3, 0, 0, 0); // TODO: Initialize to an appropriate value
			IList<PrepHistoryDto> expected = new List<PrepHistoryDto>();
			expected.Add(new PrepHistoryDto() { UnitId = unitId });

			IList<PrepHistoryDto> actual;
			actual = target.GetPrepHistoryByUnit(unitId, startDate, endDate);
			
			Assert.AreEqual(expected.Count, actual.Count);
			Assert.IsTrue(expected.Count > 0);
			Assert.AreEqual(expected[0].UnitId, actual[0].UnitId);
		}
    }
}
