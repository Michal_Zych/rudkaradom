﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finder.BloodDonation.CLR.Logic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace Finder.BloodDonation.CLR.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            TestMeasureArray();
            //TestChartGeneralization();

            Console.ReadLine();
        }

        static void TestChartGeneralization()
        {
            try
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                DataTable dtData = new DataTable();

                using (SqlConnection connection = new SqlConnection("Data Source=192.168.125.164;Initial Catalog=finderblood_prod;User ID=finder;Password=finder1230-=;application name=FinderBloodCLR"))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("Chart_ChartsDeviceData", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("device_id", 175));
                    command.Parameters.Add(new SqlParameter("date_from", new DateTime(2010, 12, 5)));
                    command.Parameters.Add(new SqlParameter("date_to", new DateTime(2010, 12, 17)));

                    using (command)
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            dtData.Load(reader);
                        }
                    }
                }

                ChartGeneralizationLogic logic = new ChartGeneralizationLogic();
                DataTable result = logic.Execute(dtData);
                sw.Stop();
            }
            catch (Exception exc)
            {

            }
        }

        static void TestMeasureArray()
        {
            try
            {

                DataTable dtData = new DataTable();

                using (SqlConnection connection = new SqlConnection("Data Source=192.168.125.164;Initial Catalog=finderblood_prod;User ID=finder;Password=finder1230-=;application name=FinderBloodCLR"))
                {
                    connection.Open();

                    SqlCommand command = new SqlCommand("Reports_GetMeasureArrayData", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("UserReportID", 36));

                    using (command)
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            dtData.Load(reader);
                        }
                    }
                }

                MeasureArrayLogic logic = new MeasureArrayLogic();
                DataTable result = logic.Execute(1, dtData);
                //DataTable result = logic.ExecuteByMnutes(5, dtData);
            }
            catch (Exception exc)
            {

            }
        }
    }
}
