﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Finder.BloodDonation.CLR.Logic
{
    public class DetailsTemperatures
    {
        public int UserReportId { get; protected set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DataTable TempData { get; protected set; }
        public DataTable Data { get; protected set; }
        public DataTable RawData { get; protected set; }
        public DataTable Parameters { get; protected set; }
        public DataTable UserReportsParameters { get; private set; }

        private const string UnitId = "UnitId";
        private const string UnitName = "Name";
        private const string DateFrom = "DateFrom";
        private const string DateTo = "DateTo";
        private const string PrepId = "PrepId";
        private const string AvgTemp = "AvgTemp";
        private const string InDate = "InDate";
        private const string OutDate = "OutDate";
        private const string Status = "StatusId";

        private const string BloodType = "BloodType";
        private const string BloodCode = "BloodCode";
        private const string Description = "Description";
        private const string DNCode = "DNCode";

        private const string DeviceID = "DeviceID";

        private const string DN = "DN";
        private const string GR = "GR";
        private const string GR2 = "GR2";
        private const string TP = "TP";
        private const string VU = "VU";
        private const string CP = "CP";


        private const string MONTH = "Month";
        private const string DAY = "Day";
        private const string TIMESTAMP = "Timestamp";
        private const string CHART_URL = "Chart";

        private const string T00h = "T00h";
        private const string T10h = "T10h";
        private const string T01h = "T01h";
        private const string T11h = "T11h";
        private const string T02h = "T02h";
        private const string T12h = "T12h";
        private const string T03h = "T03h";
        private const string T13h = "T13h";
        private const string T04h = "T04h";
        private const string T14h = "T14h";
        private const string T05h = "T05h";
        private const string T15h = "T15h";
        private const string T06h = "T06h";
        private const string T16h = "T16h";
        private const string T07h = "T07h";
        private const string T17h = "T17h";
        private const string T08h = "T08h";
        private const string T18h = "T18h";
        private const string T09h = "T09h";
        private const string T19h = "T19h";
        private const string T010h = "T010h";
        private const string T110h = "T110h";
        private const string T011h = "T011h";
        private const string T111h = "T111h";
        private const string T012h = "T012h";
        private const string T112h = "T112h";
        private const string T013h = "T013h";
        private const string T113h = "T113h";
        private const string T014h = "T014h";
        private const string T114h = "T114h";
        private const string T015h = "T015h";
        private const string T115h = "T115h";
        private const string T016h = "T016h";
        private const string T116h = "T116h";
        private const string T017h = "T017h";
        private const string T117h = "T117h";
        private const string T018h = "T018h";
        private const string T118h = "T118h";
        private const string T019h = "T019h";
        private const string T119h = "T119h";
        private const string T020h = "T020h";
        private const string T120h = "T120h";
        private const string T021h = "T021h";
        private const string T121h = "T121h";
        private const string T022h = "T022h";
        private const string T122h = "T122h";
        private const string T023h = "T023h";
        private const string T123h = "T123h";
        
        Dictionary<string, string> ggW0 = new Dictionary<string, string>();

        public DetailsTemperatures(int userReportId)
        {
            UserReportId = userReportId;
            
            ggW0.Add("06", "A-");
            ggW0.Add("62", "A+");
            ggW0.Add("17", "B-");
            ggW0.Add("73", "B+");
            ggW0.Add("28", "AB-");
            ggW0.Add("84", "AB+");
            ggW0.Add("95", "0-");
            ggW0.Add("51", "0+");
            ggW0.Add("66", "A");
            ggW0.Add("77", "A");
            ggW0.Add("88", "AB");
            ggW0.Add("55", "0");
        }

        public DataTable Execute()
        {
            GetParameters();
            GetRawData();
            GetData();
            PreExecuteMain();
            ExecuteMain();
            PostExecuteMain();

            return Data;
        }

        private void GetRawData()
        {
            const string QueryGetRawData = "EXEC dbo.GetRfidPrepHistoryByRfid @Rfid = {0}, @DataFrom='{1}', @DataTo='{2}'";

            if (UserReportsParameters == null
                || UserReportsParameters.Rows.Count == 0)
                return;

            int rfID = 0;
            DateTime dateFrom = DateTime.MinValue;
            DateTime dateTo = DateTime.MinValue;

            RawData = new DataTable();

            if (!Parameters.Columns.Contains(DateFrom))
                throw new Exception(String.Format("Kolumny DateFrom nie ma w parametrach.ReportUserId:{0}", UserReportId));

            if (!Parameters.Columns.Contains(DateTo))
                throw new Exception(String.Format("Kolumny DateTo nie ma w parametrach.ReportUserId:{0}", UserReportId));

            if (!Int32.TryParse(UserReportsParameters.Rows[0][DeviceID].ToString(), out rfID))
                throw new Exception(String.Format("Parametr rfID nie jest wartoscia liczbowa.ReportUserId:{0}", UserReportId));

            if (!DateTime.TryParse(Parameters.Rows[0][DateFrom].ToString(), out dateFrom))
                throw new Exception(String.Format("Parametr DateFrom nie jest wartoscia DateTime.ReportUserId:{0}", UserReportId));

            if (!DateTime.TryParse(Parameters.Rows[0][DateTo].ToString(), out dateTo))
                throw new Exception(String.Format("Parametr DateTo nie jest wartoscia DateTime.ReportUserId:{0}", UserReportId));

            FromDate = dateFrom;
            ToDate = dateTo;

            using (var connection = new SqlConnection("context connection=true;"))
            {
                connection.Open();

                using (var command = new SqlCommand(String.Format(QueryGetRawData, rfID, dateFrom, dateTo), connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        RawData.Load(reader);
                    }
                }
            }
        }

        private void PostExecuteMain()
        {
            Data = GetSchemaTable();
            foreach (DataRow dataRow in TempData.Rows)
            {
                DataTable result = GetMeasureArrayData(dataRow);

                AddRowToFinalTable(dataRow, result);
            }
        }

        private void AddRowToFinalTable(DataRow dataRow, DataTable result)
        {
            if(result != null)
                foreach (DataRow dr in result.Rows)
                {
                    if (!CheckRowExists(dr, dataRow))
                        InsertRow(dr, dataRow);
                    
                }
        }

        private bool CheckRowExists(DataRow dr, DataRow dataRow)
        {
            bool isExists = false;

            int deviceID = Convert.ToInt32(dataRow[DeviceID]);
            int year = Convert.ToDateTime(dataRow[InDate]).Year;
            int month = Convert.ToInt32(dr[MONTH]);
            int day = Convert.ToInt32(dr[DAY]);

            for (int i = 0; i < Data.Rows.Count; i++)
            {
                int deviceID_DT = Convert.ToInt32(Data.Rows[i][DeviceID]);
                int year_DT = Convert.ToDateTime(Data.Rows[i][InDate]).Year;
                int month_DT = Convert.ToInt32(Data.Rows[i][MONTH]);
                int day_DT = Convert.ToInt32(Data.Rows[i][DAY]);

                if (deviceID == deviceID_DT
                    && year == year_DT
                    && month == month_DT
                    && day == day_DT)
                {
                    if (Data.Rows[i][T00h] == DBNull.Value) Data.Rows[i][T00h] = dr[T00h];
                    if (Data.Rows[i][T10h] == DBNull.Value) Data.Rows[i][T10h] = dr[T10h];
                    if (Data.Rows[i][T01h] == DBNull.Value) Data.Rows[i][T01h] = dr[T01h];
                    if (Data.Rows[i][T11h] == DBNull.Value) Data.Rows[i][T11h] = dr[T11h];
                    if (Data.Rows[i][T02h] == DBNull.Value) Data.Rows[i][T02h] = dr[T02h];
                    if (Data.Rows[i][T12h] == DBNull.Value) Data.Rows[i][T12h] = dr[T12h];
                    if (Data.Rows[i][T03h] == DBNull.Value) Data.Rows[i][T03h] = dr[T03h];
                    if (Data.Rows[i][T13h] == DBNull.Value) Data.Rows[i][T13h] = dr[T13h];
                    if (Data.Rows[i][T04h] == DBNull.Value) Data.Rows[i][T04h] = dr[T04h];
                    if (Data.Rows[i][T14h] == DBNull.Value) Data.Rows[i][T14h] = dr[T14h];
                    if (Data.Rows[i][T05h] == DBNull.Value) Data.Rows[i][T05h] = dr[T05h];
                    if (Data.Rows[i][T15h] == DBNull.Value) Data.Rows[i][T15h] = dr[T15h];
                    if (Data.Rows[i][T06h] == DBNull.Value) Data.Rows[i][T06h] = dr[T06h];
                    if (Data.Rows[i][T16h] == DBNull.Value) Data.Rows[i][T16h] = dr[T16h];
                    if (Data.Rows[i][T07h] == DBNull.Value) Data.Rows[i][T07h] = dr[T07h];
                    if (Data.Rows[i][T17h] == DBNull.Value) Data.Rows[i][T17h] = dr[T17h];
                    if (Data.Rows[i][T08h] == DBNull.Value) Data.Rows[i][T08h] = dr[T08h];
                    if (Data.Rows[i][T18h] == DBNull.Value) Data.Rows[i][T18h] = dr[T18h];
                    if (Data.Rows[i][T09h] == DBNull.Value) Data.Rows[i][T09h] = dr[T09h];
                    if (Data.Rows[i][T19h] == DBNull.Value) Data.Rows[i][T19h] = dr[T19h];
                    if (Data.Rows[i][T010h] == DBNull.Value) Data.Rows[i][T010h] = dr[T010h];
                    if (Data.Rows[i][T110h] == DBNull.Value) Data.Rows[i][T110h] = dr[T110h];
                    if (Data.Rows[i][T011h] == DBNull.Value) Data.Rows[i][T011h] = dr[T011h];
                    if (Data.Rows[i][T111h] == DBNull.Value) Data.Rows[i][T111h] = dr[T111h];
                    if (Data.Rows[i][T012h] == DBNull.Value) Data.Rows[i][T012h] = dr[T012h];
                    if (Data.Rows[i][T112h] == DBNull.Value) Data.Rows[i][T112h] = dr[T112h];
                    if (Data.Rows[i][T013h] == DBNull.Value) Data.Rows[i][T013h] = dr[T013h];
                    if (Data.Rows[i][T113h] == DBNull.Value) Data.Rows[i][T113h] = dr[T113h];
                    if (Data.Rows[i][T014h] == DBNull.Value) Data.Rows[i][T014h] = dr[T014h];
                    if (Data.Rows[i][T114h] == DBNull.Value) Data.Rows[i][T114h] = dr[T114h];
                    if (Data.Rows[i][T015h] == DBNull.Value) Data.Rows[i][T015h] = dr[T015h];
                    if (Data.Rows[i][T115h] == DBNull.Value) Data.Rows[i][T115h] = dr[T115h]; 
                    if (Data.Rows[i][T016h] == DBNull.Value) Data.Rows[i][T016h] = dr[T016h];
                    if (Data.Rows[i][T116h] == DBNull.Value) Data.Rows[i][T116h] = dr[T116h]; 
                    if (Data.Rows[i][T017h] == DBNull.Value) Data.Rows[i][T017h] = dr[T017h];
                    if (Data.Rows[i][T117h] == DBNull.Value) Data.Rows[i][T117h] = dr[T117h];
                    if (Data.Rows[i][T018h] == DBNull.Value) Data.Rows[i][T018h] = dr[T018h]; 
                    if (Data.Rows[i][T118h] == DBNull.Value) Data.Rows[i][T118h] = dr[T118h];
                    if (Data.Rows[i][T019h] == DBNull.Value) Data.Rows[i][T019h] = dr[T019h];
                    if (Data.Rows[i][T119h] == DBNull.Value) Data.Rows[i][T119h] = dr[T119h];
                    if (Data.Rows[i][T020h] == DBNull.Value) Data.Rows[i][T020h] = dr[T020h];
                    if (Data.Rows[i][T120h] == DBNull.Value) Data.Rows[i][T120h] = dr[T120h];
                    if (Data.Rows[i][T021h] == DBNull.Value) Data.Rows[i][T021h] = dr[T021h];
                    if (Data.Rows[i][T121h] == DBNull.Value) Data.Rows[i][T121h] = dr[T121h];
                    if (Data.Rows[i][T022h] == DBNull.Value) Data.Rows[i][T022h] = dr[T022h];
                    if (Data.Rows[i][T122h] == DBNull.Value) Data.Rows[i][T122h] = dr[T122h];
                    if (Data.Rows[i][T023h] == DBNull.Value) Data.Rows[i][T023h] = dr[T023h];
                    if (Data.Rows[i][T123h] == DBNull.Value) Data.Rows[i][T123h] = dr[T123h];
                    isExists = true;
                }
            }

            return isExists;
        }

        private void InsertRow(DataRow dr, DataRow dataRow)
        {
            DataRow newDataRow = Data.NewRow();
            newDataRow[PrepId] = dataRow[PrepId];
            newDataRow[DN] = dataRow[DN];
            newDataRow[BloodCode] = dataRow[BloodCode];
            newDataRow[BloodType] = dataRow[BloodType];
            newDataRow[DNCode] = dataRow[DNCode];

            newDataRow[Description] = dataRow[Description];
            newDataRow[InDate] = dataRow[InDate];
            newDataRow[OutDate] = dataRow[OutDate];
            newDataRow[AvgTemp] = dataRow[AvgTemp];
            newDataRow[Status] = dataRow[Status];

            newDataRow[UnitId] = dataRow[UnitId];
            newDataRow[UnitName] = dataRow[UnitName];

            newDataRow[DeviceID] = dataRow[DeviceID];

            newDataRow[MONTH] = dr[MONTH];
            newDataRow[DAY] = dr[DAY];
            newDataRow[CHART_URL] = dr[CHART_URL];

            newDataRow[T00h] = dr[T00h];
            newDataRow[T10h] = dr[T10h];
            newDataRow[T01h] = dr[T01h];
            newDataRow[T11h] = dr[T11h];
            newDataRow[T02h] = dr[T02h];
            newDataRow[T12h] = dr[T12h];
            newDataRow[T03h] = dr[T03h];
            newDataRow[T13h] = dr[T13h];
            newDataRow[T04h] = dr[T04h];
            newDataRow[T14h] = dr[T14h];
            newDataRow[T05h] = dr[T05h];
            newDataRow[T15h] = dr[T15h];
            newDataRow[T06h] = dr[T06h];
            newDataRow[T16h] = dr[T16h];
            newDataRow[T07h] = dr[T07h];
            newDataRow[T17h] = dr[T17h];
            newDataRow[T08h] = dr[T08h];
            newDataRow[T18h] = dr[T18h];
            newDataRow[T09h] = dr[T09h];
            newDataRow[T19h] = dr[T19h];
            newDataRow[T010h] = dr[T010h];
            newDataRow[T110h] = dr[T110h];
            newDataRow[T011h] = dr[T011h];
            newDataRow[T111h] = dr[T111h];
            newDataRow[T012h] = dr[T012h];
            newDataRow[T112h] = dr[T112h];
            newDataRow[T013h] = dr[T013h];
            newDataRow[T113h] = dr[T113h];
            newDataRow[T014h] = dr[T014h];
            newDataRow[T114h] = dr[T114h];
            newDataRow[T015h] = dr[T015h];
            newDataRow[T115h] = dr[T115h];
            newDataRow[T016h] = dr[T016h];
            newDataRow[T116h] = dr[T116h];
            newDataRow[T017h] = dr[T017h];
            newDataRow[T117h] = dr[T117h];
            newDataRow[T018h] = dr[T018h];
            newDataRow[T118h] = dr[T118h];
            newDataRow[T019h] = dr[T019h];
            newDataRow[T119h] = dr[T119h];
            newDataRow[T020h] = dr[T020h];
            newDataRow[T120h] = dr[T120h];
            newDataRow[T021h] = dr[T021h];
            newDataRow[T121h] = dr[T121h];
            newDataRow[T022h] = dr[T022h];
            newDataRow[T122h] = dr[T122h];
            newDataRow[T023h] = dr[T023h];
            newDataRow[T123h] = dr[T123h];

            Data.Rows.Add(newDataRow);
        }

        private DataTable GetMeasureArrayData(DataRow dataRow)
        {
            int deviceId = Convert.ToInt32(dataRow[DeviceID]);
            DateTime dateStart = Convert.ToDateTime(dataRow[InDate]);
            DateTime dateEnd = DateTime.MinValue;

            if (dataRow[OutDate] == DBNull.Value)
                dateEnd = FromDate;
            else
                dateEnd = Convert.ToDateTime(dataRow[OutDate]);

            DataTable dtData = new DataTable();

            using (SqlConnection connection = new SqlConnection("context connection=true;"))
            {
                connection.Open();

                SqlCommand command = new SqlCommand("dbo.Reports_GetMeasureArrayDataByDeviceId", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("DateFrom", dateStart));
                command.Parameters.Add(new SqlParameter("DateTo", dateEnd));
                command.Parameters.Add(new SqlParameter("DeviceID", deviceId));

                using (command)
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        dtData.Load(reader);
                    }
                }
            }

            MeasureArrayLogic logic = new MeasureArrayLogic();
            DataTable result = logic.Execute(1, dtData);

            return result;
        }

        private void AddRowToTempTable(DataRow dataRow)
        {
            DataRow newDataRow = TempData.NewRow();

            newDataRow[PrepId] = dataRow[PrepId];
            newDataRow[DN] = dataRow[DN];
            newDataRow[BloodCode] = dataRow[GR];
            if (dataRow[GR] != DBNull.Value)
            {
                string str = dataRow[GR].ToString();
                if (str.Length > 2)
                    newDataRow[BloodType] = ggW0[dataRow[GR].ToString().Substring(0, 2)];
            }
            if (dataRow[TP] != DBNull.Value)
            {
                string dnCode = dataRow[TP].ToString();
                if (dnCode.Length > 5)
                    newDataRow[DNCode] = dnCode.Substring(0, 5);

                newDataRow[Description] = BloodIngredientDescritpion.GetBloodDescription(dnCode);
            }
            newDataRow[InDate] = dataRow[InDate];
            newDataRow[OutDate] = dataRow[OutDate];
            newDataRow[AvgTemp] = dataRow[AvgTemp];
            newDataRow[Status] = dataRow[Status];

            newDataRow[UnitId] = dataRow[UnitId];
            newDataRow[UnitName] = dataRow[UnitName];

            newDataRow[DeviceID] = dataRow[DeviceID];

            TempData.Rows.Add(newDataRow);
        }

        private DataTable GetSchemaTable()
        {
            Data = new DataTable();
            Data.Columns.Add(PrepId, typeof(int));
            Data.Columns.Add(AvgTemp, typeof(decimal));
            Data.Columns.Add(InDate, typeof(DateTime));
            Data.Columns.Add(OutDate, typeof(DateTime));
            Data.Columns.Add(Status, typeof(int));

            Data.Columns.Add(DN, typeof(string));
            Data.Columns.Add(DNCode, typeof(string));
            Data.Columns.Add(BloodType, typeof(string));
            Data.Columns.Add(BloodCode, typeof(string));
            Data.Columns.Add(Description, typeof(string));

            Data.Columns.Add(UnitId, typeof(int));
            Data.Columns.Add(UnitName, typeof(string));

            Data.Columns.Add(DeviceID, typeof(int));

            Data.Columns.Add(MONTH, typeof(int));
            Data.Columns.Add(DAY, typeof(int));
            Data.Columns.Add(CHART_URL, typeof(string));

            Data.Columns.Add(T00h, typeof(decimal));
            Data.Columns.Add(T10h, typeof(decimal));
            Data.Columns.Add(T01h, typeof(decimal));
            Data.Columns.Add(T11h, typeof(decimal));
            Data.Columns.Add(T02h, typeof(decimal));
            Data.Columns.Add(T12h, typeof(decimal));
            Data.Columns.Add(T03h, typeof(decimal));
            Data.Columns.Add(T13h, typeof(decimal));
            Data.Columns.Add(T04h, typeof(decimal));
            Data.Columns.Add(T14h, typeof(decimal));
            Data.Columns.Add(T05h, typeof(decimal));
            Data.Columns.Add(T15h, typeof(decimal));
            Data.Columns.Add(T06h, typeof(decimal));
            Data.Columns.Add(T16h, typeof(decimal));
            Data.Columns.Add(T07h, typeof(decimal));
            Data.Columns.Add(T17h, typeof(decimal));
            Data.Columns.Add(T08h, typeof(decimal));
            Data.Columns.Add(T18h, typeof(decimal));
            Data.Columns.Add(T09h, typeof(decimal));
            Data.Columns.Add(T19h, typeof(decimal));
            Data.Columns.Add(T010h, typeof(decimal));
            Data.Columns.Add(T110h, typeof(decimal));
            Data.Columns.Add(T011h, typeof(decimal));
            Data.Columns.Add(T111h, typeof(decimal));
            Data.Columns.Add(T012h, typeof(decimal));
            Data.Columns.Add(T112h, typeof(decimal));
            Data.Columns.Add(T013h, typeof(decimal));
            Data.Columns.Add(T113h, typeof(decimal));
            Data.Columns.Add(T014h, typeof(decimal));
            Data.Columns.Add(T114h, typeof(decimal));
            Data.Columns.Add(T015h, typeof(decimal));
            Data.Columns.Add(T115h, typeof(decimal));
            Data.Columns.Add(T016h, typeof(decimal));
            Data.Columns.Add(T116h, typeof(decimal));
            Data.Columns.Add(T017h, typeof(decimal));
            Data.Columns.Add(T117h, typeof(decimal));
            Data.Columns.Add(T018h, typeof(decimal));
            Data.Columns.Add(T118h, typeof(decimal));
            Data.Columns.Add(T019h, typeof(decimal));
            Data.Columns.Add(T119h, typeof(decimal));
            Data.Columns.Add(T020h, typeof(decimal));
            Data.Columns.Add(T120h, typeof(decimal));
            Data.Columns.Add(T021h, typeof(decimal));
            Data.Columns.Add(T121h, typeof(decimal));
            Data.Columns.Add(T022h, typeof(decimal));
            Data.Columns.Add(T122h, typeof(decimal));
            Data.Columns.Add(T023h, typeof(decimal));
            Data.Columns.Add(T123h, typeof(decimal));

            return Data;
        }

        private DataTable GetSchemaTempTable()
        {
            TempData = new DataTable();

            TempData.Columns.Add(PrepId, typeof(int));
            TempData.Columns.Add(AvgTemp, typeof(decimal));
            TempData.Columns.Add(InDate, typeof(DateTime));
            TempData.Columns.Add(OutDate, typeof(DateTime));
            TempData.Columns.Add(Status, typeof(int));

            TempData.Columns.Add(DN, typeof(string));
            TempData.Columns.Add(DNCode, typeof(string));
            TempData.Columns.Add(BloodType, typeof(string));
            TempData.Columns.Add(BloodCode, typeof(string));
            TempData.Columns.Add(Description, typeof(string));

            TempData.Columns.Add(UnitId, typeof(int));
            TempData.Columns.Add(UnitName, typeof(string));

            TempData.Columns.Add(DeviceID, typeof(int));
            return TempData;
        }

        private void ExecuteMain()
        {
            TempData = GetSchemaTempTable();

            foreach (DataRow dataRow in RawData.Rows)
            {
                AddRowToTempTable(dataRow);
            }
        }

        private void PreExecuteMain()
        {
            
        }

        private void GetData()
        {
            
        }

        private void GetParameters()
        {
            const string QueryGetParametersReports = "SELECT * FROM dbo.UsersReports WHERE ID = {0}";
            const string QueryGetRfidId = "SELECT TOP 1 * FROM dbo.UserReportDevices WHERE UserReportId = {0}";

            Parameters = new DataTable();
            UserReportsParameters = new DataTable();

            using (var connection = new SqlConnection("context connection=true;"))
            {
                connection.Open();

                using (var command = new SqlCommand(String.Format(QueryGetParametersReports, UserReportId), connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        Parameters.Load(reader);
                    }
                }
                using (var command = new SqlCommand(String.Format(QueryGetRfidId, UserReportId), connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        UserReportsParameters.Load(reader);
                    }
                }
            }
        }
    }
}
