﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Finder.BloodDonation.CLR.Logic
{
    public class BloodIngredientDescritpion
    {
        static readonly public Dictionary<string, string> DescrptionDict = new Dictionary<string, string>();

        static BloodIngredientDescritpion()
        {
            DescrptionDict.Add("E3844", "KONCENTRAT KRWINEK CZERWONYCH|SAGM/450ml/2-6C|liczba leuk:<1,2log9");
        }

        public static string GetBloodDescription(string code)
        {
            if (code == null || code.Length < 5)
                return string.Empty;
            string shortCode = code.Substring(0, 5);

            if (!DescrptionDict.ContainsKey(shortCode))
                return string.Empty;

            string resultStr = DescrptionDict[shortCode];
            if (resultStr.Length > 400)
                resultStr = resultStr.Substring(0, 399);

            return resultStr;
        }
    }
}
