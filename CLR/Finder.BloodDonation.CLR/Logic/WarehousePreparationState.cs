﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace Finder.BloodDonation.CLR.Logic
{
    public class WarehousePreparationState
    {
        public int UserReportId { get; protected set; }
        public DataTable Data { get; protected set; }
        public DataTable RawData { get; protected set; }
        public DataTable Parameters { get; protected set;}

        private const string UnitId = "UnitId";
        private const string UnitName = "Name";
        private const string DateFrom = "DateFrom";
        private const string DateTo = "DateTo";
        private const string PrepId = "PrepId";
        private const string AvgTemp = "AvgTemp";
        private const string InDate = "InDate";
        private const string OutDate = "OutDate";
        private const string Status = "StatusId";

        private const string BloodType = "BloodType";
        private const string BloodCode = "BloodCode";
        private const string Description = "Description";
        private const string DNCode = "DNCode";

        private const string DeviceID = "DeviceID";

        private const string DN = "DN";
        private const string GR = "GR";
        private const string GR2 = "GR2";
        private const string TP = "TP";
        private const string VU = "VU";
        private const string CP = "CP";

        Dictionary<string, string> ggW0 = new Dictionary<string, string>();

        public WarehousePreparationState(int userReportId)
        {
            UserReportId = userReportId;
            
            ggW0.Add("06", "A-");
            ggW0.Add("62", "A+");
            ggW0.Add("17", "B-");
            ggW0.Add("73", "B+");
            ggW0.Add("28", "AB-");
            ggW0.Add("84", "AB+");
            ggW0.Add("95", "0-");
            ggW0.Add("51", "0+");
            ggW0.Add("66", "A");
            ggW0.Add("77", "A");
            ggW0.Add("88", "AB");
            ggW0.Add("55", "0");
        }

        public DataTable Execute()
        {
            GetParameters();
            GetData();
            PreExecuteMain();
            ExecuteMain();
            PostExecuteMain();

            return Data;
        }

        private void GetParameters()
        {
            const string QueryGetParametersReports = "SELECT * FROM dbo.UsersReports WHERE ID = {0}";

            Parameters = new DataTable();
            using (var connection = new SqlConnection("context connection=true;"))
            {
                connection.Open();

                using (var command = new SqlCommand(String.Format(QueryGetParametersReports, UserReportId), connection))
                {
                    //command.Parameters.Add("@UserReportId", SqlDbType.Int, UserReportId);
                    using (var reader = command.ExecuteReader())
                    {
                        Parameters.Load(reader);
                    }
                }
            }
        }


        private void PostExecuteMain()
        {
            
        }

        private void PreExecuteMain()
        {
            
        }

        private void ExecuteMain()
        {
            Data = GetSchemaTable();

            foreach (DataRow dataRow in RawData.Rows)
            {
                AddRow(dataRow);
            }
        }

        private void AddRow(DataRow dataRow)
        {
            DataRow newDataRow = Data.NewRow();

            newDataRow[PrepId] = dataRow[PrepId];
            newDataRow[DN] = dataRow[DN];
            newDataRow[BloodCode] = dataRow[GR];
            if (dataRow[GR] != DBNull.Value)
            {
                string str = dataRow[GR].ToString();
                if (str.Length > 2)
                    newDataRow[BloodType] = ggW0[dataRow[GR].ToString().Substring(0, 2)];
            }
            if (dataRow[TP] != DBNull.Value)
            {
                string dnCode = dataRow[TP].ToString();
                if (dnCode.Length > 5)
                    newDataRow[DNCode] = dnCode.Substring(0, 5);

                newDataRow[Description] = BloodIngredientDescritpion.GetBloodDescription(dnCode);
            }
            
            newDataRow[InDate] = dataRow[InDate];
            newDataRow[OutDate] = dataRow[OutDate];
            newDataRow[AvgTemp] = dataRow[AvgTemp];
            newDataRow[Status] = dataRow[Status];

            newDataRow[UnitId] = dataRow[UnitId];
            newDataRow[UnitName] = dataRow[UnitName];

            newDataRow[DeviceID] = dataRow[DeviceID];

            Data.Rows.Add(newDataRow);
        }

        private DataTable GetSchemaTable()
        {
            Data = new DataTable();

            Data.Columns.Add(PrepId, typeof(int));
            Data.Columns.Add(AvgTemp, typeof(decimal));
            Data.Columns.Add(InDate, typeof(DateTime));
            Data.Columns.Add(OutDate, typeof(DateTime));
            Data.Columns.Add(Status, typeof(int));

            Data.Columns.Add(DN, typeof(string));
            Data.Columns.Add(DNCode, typeof(string));
            Data.Columns.Add(BloodType, typeof(string));
            Data.Columns.Add(BloodCode, typeof(string));
            Data.Columns.Add(Description, typeof(string));

            Data.Columns.Add(UnitId, typeof(int));
            Data.Columns.Add(UnitName, typeof(string));
            Data.Columns.Add(DeviceID, typeof(int));


            return Data;
        }


        private void GetData()
        {
            const string QueryGetRawData = "EXEC dbo.GetRfidPrepHistory @UserReportId = {0}, @DataFrom='{1}', @DataTo='{2}'";

            int unitId = 0;
            DateTime dateFrom = DateTime.MinValue;
            DateTime dateTo = DateTime.MinValue;

            RawData = new DataTable();
           
            if (!Parameters.Columns.Contains(DateFrom))
                throw new Exception(String.Format("Kolumny DateFrom nie ma w parametrach.ReportUserId:{0}", UserReportId));

            if (!Parameters.Columns.Contains(DateTo))
                throw new Exception(String.Format("Kolumny DateTo nie ma w parametrach.ReportUserId:{0}", UserReportId));

            if (!Int32.TryParse(Parameters.Rows[0][UnitId].ToString(), out unitId))
                throw new Exception(String.Format("Parametr UnitId nie jest wartoscia liczbowa.ReportUserId:{0}", UserReportId));

            if(!DateTime.TryParse(Parameters.Rows[0][DateFrom].ToString(), out dateFrom))
                throw new Exception(String.Format("Parametr DateFrom nie jest wartoscia DateTime.ReportUserId:{0}", UserReportId));

            if (!DateTime.TryParse(Parameters.Rows[0][DateTo].ToString(), out dateTo))
                throw new Exception(String.Format("Parametr DateTo nie jest wartoscia DateTime.ReportUserId:{0}", UserReportId));

            using (var connection = new SqlConnection("context connection=true;"))
            {
                connection.Open();

                using (var command = new SqlCommand(String.Format(QueryGetRawData, UserReportId, dateFrom, dateTo), connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        RawData.Load(reader);
                    }
                }
            }
        }

    }
}
