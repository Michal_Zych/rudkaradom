﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Finder.BloodDonation.CLR.Logic
{
    public class ChartGeneralizationLogic
    {
        private const string TIMESTAMP = "Timestamp";
        private const string T0 = "T1";
        private const string T1 = "T2";

        public DataTable Execute(DataTable dt_data)
        {
            DataTable dt_result = new DataTable();
            dt_result.Columns.Add(TIMESTAMP, typeof(DateTime));
            dt_result.Columns.Add(T0, typeof(decimal));
            dt_result.Columns.Add(T1, typeof(decimal));
            try
            {
                if (dt_data != null)
                {
                    int count = dt_data.Rows.Count;

                    int destinationCount = 1000;

                    if (count <= destinationCount)
                        return dt_data;

                    int maxAggregateFactor = count / destinationCount;

                    //niegeneralizujemy malej ilosci danych
                    if (maxAggregateFactor <= 1)
                        return dt_data;

                    //generalizuj paczkami
                    for (int i = 0; i < dt_data.Rows.Count; i = i + maxAggregateFactor)
                    {
                        int end = Math.Min(i + maxAggregateFactor, dt_data.Rows.Count);

                        dt_result.Rows.Add(GetValue(i, end, dt_data, dt_result));
                    }

                    return dt_result;
                }
            }
            catch (Exception)
            {

            }

            return dt_result;
        }

        private DataRow GetValue(int start, int end, DataTable dt_data, DataTable dt_result)
        {
            double sum_t0 = 0;
            double sum_t1 = 0;

            for (int i = start; i < end; i++)
            {
                sum_t0 += Convert.ToDouble(dt_data.Rows[i][T0]);
                sum_t1 += Convert.ToDouble(dt_data.Rows[i][T1]);
            }

            //srednia wartosc
            double avg_t0 = sum_t0 / (end - start);
            double avg_t1 = sum_t1 / (end - start);

            double maxDeviation_t0 = double.MinValue;
            double maxDeviation_t1 = double.MinValue;
            int maxDeviationIndex_t0 = 0;
            int maxDeviationIndex_t1 = 0;
            for (int i = start; i < end; i++)
            {
                //odchylenie od sredniej
                double deviation_t0 = Math.Abs(Convert.ToDouble(dt_data.Rows[i][T0]) - avg_t0);
                double deviation_t1 = Math.Abs(Convert.ToDouble(dt_data.Rows[i][T1]) - avg_t1);

                //znajdz indeks najwiekszego odchylenia
                if (deviation_t0 > maxDeviation_t0)
                {
                    maxDeviation_t0 = deviation_t0;
                    maxDeviationIndex_t0 = i;
                }

                if (deviation_t1 > maxDeviation_t1)
                {
                    maxDeviation_t1 = deviation_t1;
                    maxDeviationIndex_t1 = i;
                }
            }

            DataRow dr = dt_result.NewRow();
            dr[TIMESTAMP] = dt_data.Rows[maxDeviationIndex_t0][TIMESTAMP];
            dr[T0] = dt_data.Rows[maxDeviationIndex_t0][T0];
            dr[T1] = dt_data.Rows[maxDeviationIndex_t1][T1];
            return dr;
        }
    }
}
