﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Finder.BloodDonation.CLR.Logic
{
    public class AlarmPreparates
    {
        public int DeviceId { get; private set; }
        public DateTime DateFrom { get; private set; }
        public DateTime DateTo { get; private set; }

        public DataTable Data { get; private set; }
        public DataTable RawData { get; private set; }
        public DataTable Parameters { get; private set; }

        private const string UnitId = "UnitId";
        private const string UnitName = "Name";
        private const string DateFromCol = "DateFrom";
        private const string DateToCol = "DateTo";
        private const string PrepId = "PrepId";
        private const string AvgTemp = "AvgTemp";
        private const string InDate = "InDate";
        private const string OutDate = "OutDate";
        private const string Status = "StatusId";

        private const string BloodType = "BloodType";
        private const string BloodCode = "BloodCode";
        private const string Description = "Description";
        private const string DNCode = "DNCode";

        private const string DeviceIDCol = "DeviceID";

        private const string DN = "DN";
        private const string GR = "GR";
        private const string GR2 = "GR2";
        private const string TP = "TP";
        private const string VU = "VU";
        private const string CP = "CP";

        Dictionary<string, string> ggW0 = new Dictionary<string, string>();

        public AlarmPreparates(int deviceId, DateTime dateFrom, DateTime dateTo)
        {
            DeviceId = deviceId;
            DateFrom = dateFrom;
            DateTo = dateTo;
            
            ggW0.Add("06", "A-");
            ggW0.Add("62", "A+");
            ggW0.Add("17", "B-");
            ggW0.Add("73", "B+");
            ggW0.Add("28", "AB-");
            ggW0.Add("84", "AB+");
            ggW0.Add("95", "0-");
            ggW0.Add("51", "0+");
            ggW0.Add("66", "A");
            ggW0.Add("77", "A");
            ggW0.Add("88", "AB");
            ggW0.Add("55", "0");
        }

        public DataTable Execute()
        {
            GetData();
            ExecuteMain();
            return Data;
        }

        private void PostExecuteMain()
        {

        }

        private void ExecuteMain()
        {
            Data = GetSchemaTable();

            foreach (DataRow dataRow in RawData.Rows)
            {
                AddRow(dataRow);
            }
        }
        private void AddRow(DataRow dataRow)
        {
            DataRow newDataRow = Data.NewRow();

            newDataRow[PrepId] = dataRow[PrepId];
            newDataRow[DN] = dataRow[DN];
            newDataRow[BloodCode] = dataRow[GR];
            if (dataRow[GR] != DBNull.Value)
            {
                string str = dataRow[GR].ToString();
                if (str.Length > 2)
                    newDataRow[BloodType] = ggW0[dataRow[GR].ToString().Substring(0, 2)];
            }
            if (dataRow[TP] != DBNull.Value)
            {
                string dnCode = dataRow[TP].ToString();
                if (dnCode.Length > 5)
                    newDataRow[DNCode] = dnCode.Substring(0, 5);

                newDataRow[Description] = BloodIngredientDescritpion.GetBloodDescription(dnCode);
            }
            newDataRow[InDate] = dataRow[InDate];
            newDataRow[OutDate] = dataRow[OutDate];
            newDataRow[AvgTemp] = dataRow[AvgTemp];
            newDataRow[Status] = dataRow[Status];

            Data.Rows.Add(newDataRow);
        }

        private DataTable GetSchemaTable()
        {
            Data = new DataTable();

            Data.Columns.Add(PrepId, typeof(int));
            Data.Columns.Add(AvgTemp, typeof(decimal));
            Data.Columns.Add(InDate, typeof(DateTime));
            Data.Columns.Add(OutDate, typeof(DateTime));
            Data.Columns.Add(Status, typeof(int));
            Data.Columns.Add(DN, typeof(string));
            Data.Columns.Add(DNCode, typeof(string));
            Data.Columns.Add(BloodType, typeof(string));
            Data.Columns.Add(BloodCode, typeof(string));
            Data.Columns.Add(Description, typeof(string));

            return Data;
        }

        private void PreExecuteMain()
        {

        }

        private void GetData()
        {
            const string QueryGetRawData = "EXEC dbo.GetRfidPrepHistoryByDeviceId @DeviceId = {0}, @DataFrom='{1}', @DataTo='{2}'";
            RawData = new DataTable();

            using (var connection = new SqlConnection("context connection=true;"))
            {
                connection.Open();

                using (var command = new SqlCommand(String.Format(QueryGetRawData, DeviceId, DateFrom, DateTo), connection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        RawData.Load(reader);
                    }
                }
            }
        }
    }
}
