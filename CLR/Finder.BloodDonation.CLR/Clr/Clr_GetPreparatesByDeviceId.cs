﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using Finder.BloodDonation.CLR.Logic;


public partial class StoredProcedures
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void Clr_GetPreparatesByDeviceId(SqlInt32 DeviceId, SqlDateTime DateFrom, SqlDateTime DateTo)
    {
        AlarmPreparates dt = new AlarmPreparates(DeviceId.Value, DateFrom.Value, DateTo.Value);
        DataTable dataTable = dt.Execute();
        SendDataTableOverPipe(dataTable);
    }
};
