﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Finder.BloodDonation.CLR.Logic
{
    public class MeasureArrayLogic
    {
        private const string DEVICE_ID = "DeviceID";
        private const string MONTH = "Month";
        private const string DAY = "Day";
        private const string HOUR = "Hour";

        private const string TIMESTAMP = "Timestamp";
        private const string CHART_URL = "Chart";

        private const string T0 = "T1";
        private const string T1 = "T2";

        public DataTable ExecuteByMnutes(int range, DataTable dt_data)
        {
            DataTable dt_result = new DataTable();
            dt_result.Columns.Add(DEVICE_ID, typeof(int));
            dt_result.Columns.Add(MONTH, typeof(int));
            dt_result.Columns.Add(DAY, typeof(int));
            dt_result.Columns.Add(HOUR, typeof(int));

            int i = 0;
            do
            {
                dt_result.Columns.Add("T0" + i + "h", typeof(decimal));
                dt_result.Columns.Add("T1" + i + "h", typeof(decimal));
                i = i + 5;
            } while (i < 60);
            dt_result.Columns.Add(CHART_URL, typeof(string));

            try
            {
                if (dt_data != null && dt_data.Rows.Count > 0)
                {
                    int current_device_id = 0;
                    int current_year = 0;
                    int current_month = 0;
                    int current_day = 0;
                    int current_minutes = 0;
                    

                    double current_sum_t0 = 0;
                    double current_sum_t1 = 0;
                    int current_count_t0 = 0;
                    int current_count_t1 = 0;
                    string current_chart_url = string.Empty;
                    DataRow current_row = null;
                    int current_hour = 0;
                    DateTime date_from = (DateTime)dt_data.Rows[0]["DateFrom"];
                    DateTime date_to = (DateTime)dt_data.Rows[0]["DateTo"];

                    for (i = 0; i < dt_data.Rows.Count; i++)
                    {
                        int device_id = (int)dt_data.Rows[i][DEVICE_ID];
                        DateTime timestamp = (DateTime)dt_data.Rows[i][TIMESTAMP];
                        int minutes = (int)(Math.Floor((double)timestamp.Minute / (double)range) * range);
                        int hour = ((DateTime)dt_data.Rows[i][TIMESTAMP]).Hour;

                        if (device_id != current_device_id) //zmieniamy device
                        {
                            //4chart hack
                            if (current_device_id > 0)
                            {
                                //missing days for month and last device
                                AddMissingDays(dt_result, current_day, current_device_id, current_month, 32, current_chart_url);
                            }

                            current_device_id = device_id;
                            current_chart_url = GetChartByMinutesURL(current_device_id, timestamp);
                            current_year = timestamp.Year;
                            current_month = timestamp.Month;
                            current_day = timestamp.Day;
                            current_minutes = minutes;
                            current_hour = hour;
                            current_count_t0 = 0;
                            current_count_t1 = 0;
                            current_sum_t0 = 0;
                            current_sum_t1 = 0;

                            AddMissingDays(dt_result, 0, current_device_id, current_month, current_day, current_chart_url);

                            current_row = AddDeviceDayRow(dt_result, current_device_id, current_month, current_day, current_hour, current_chart_url);

                            UpdateRowMinutes(current_row, current_minutes, Convert.ToDouble(dt_data.Rows[i][T0]), Convert.ToDouble(dt_data.Rows[i][T1]), ref current_sum_t0, ref current_sum_t1, ref current_count_t0, ref current_count_t1);
                        }
                        else
                        {
                            //analizujemy ten sam dzień
                            if (current_year == timestamp.Year && current_month == timestamp.Month && current_day == timestamp.Day && current_hour == timestamp.Hour && current_minutes != minutes)
                            {
                                current_minutes = minutes;
                                current_count_t0 = 0;
                                current_count_t1 = 0;
                                current_sum_t0 = 0;
                                current_sum_t1 = 0;
                                UpdateRowMinutes(current_row, current_minutes, Convert.ToDouble(dt_data.Rows[i][T0]), Convert.ToDouble(dt_data.Rows[i][T1]), ref current_sum_t0, ref current_sum_t1, ref current_count_t0, ref current_count_t1);
                            }
                            else if (current_year == timestamp.Year && current_month == timestamp.Month && current_day == timestamp.Day && current_hour == timestamp.Hour && current_minutes == minutes)
                            {
                                UpdateRowMinutes(current_row, current_minutes, Convert.ToDouble(dt_data.Rows[i][T0]), Convert.ToDouble(dt_data.Rows[i][T1]), ref current_sum_t0, ref current_sum_t1, ref current_count_t0, ref current_count_t1);
                            }
                            // analizujemy inna godzine
                            else if (current_year == timestamp.Year && current_month == timestamp.Month && current_day == timestamp.Day && current_hour != timestamp.Hour)
                            {
                                //missing days for current month
                                AddMissingDays(dt_result, current_day, current_device_id, current_month, timestamp.Day, current_chart_url);

                                current_minutes = minutes;
                                current_hour = hour;
                                current_count_t0 = 0;
                                current_count_t1 = 0;
                                current_sum_t0 = 0;
                                current_sum_t1 = 0;

                                //new day
                                current_chart_url = GetChartByMinutesURL(current_device_id, timestamp);
                                current_row = AddDeviceDayRow(dt_result, current_device_id, current_month, current_day, current_hour, current_chart_url);
                                UpdateRowMinutes(current_row, current_minutes, Convert.ToDouble(dt_data.Rows[i][T0]), Convert.ToDouble(dt_data.Rows[i][T1]), ref current_sum_t0, ref current_sum_t1, ref current_count_t0, ref current_count_t1);
                            }
                            //analizujemy inny dzień
                            else if (current_year == timestamp.Year && current_month == timestamp.Month && current_day != timestamp.Day)
                            {
                                //missing days for current month
                                AddMissingDays(dt_result, current_day, current_device_id, current_month, timestamp.Day, current_chart_url);

                                current_minutes = minutes;
                                current_day = timestamp.Day;
                                current_hour = timestamp.Hour;
                                current_count_t0 = 0;
                                current_count_t1 = 0;
                                current_sum_t0 = 0;
                                current_sum_t1 = 0;

                                //new day
                                current_chart_url = GetChartByMinutesURL(current_device_id, timestamp);
                                current_row = AddDeviceDayRow(dt_result, current_device_id, current_month, current_day, current_hour, current_chart_url);
                                UpdateRowMinutes(current_row, current_minutes, Convert.ToDouble(dt_data.Rows[i][T0]), Convert.ToDouble(dt_data.Rows[i][T1]), ref current_sum_t0, ref current_sum_t1, ref current_count_t0, ref current_count_t1);
                            }
                            //przeskakujemy do innego miesiąca/roku
                            else
                            {
                                if (current_year != timestamp.Year || current_month != timestamp.Month)
                                {
                                    AddMissingDays(dt_result, current_day, current_device_id, current_month, 32, current_chart_url);

                                    current_year = timestamp.Year;
                                    current_month = timestamp.Month;
                                    current_day = timestamp.Day;
                                    current_hour = hour;
                                    current_count_t0 = 0;
                                    current_count_t1 = 0;
                                    current_sum_t0 = 0;
                                    current_sum_t1 = 0;

                                    current_chart_url = GetChartByMinutesURL(current_device_id, timestamp);

                                    //new day
                                    current_row = AddDeviceDayRow(dt_result, current_device_id, current_month, current_day, current_hour, current_chart_url);
                                    UpdateRowMinutes(current_row, current_minutes, Convert.ToDouble(dt_data.Rows[i][T0]), Convert.ToDouble(dt_data.Rows[i][T1]), ref current_sum_t0, ref current_sum_t1, ref current_count_t0, ref current_count_t1);
                                }
                            }
                        }
                    }

                    AddMissingDays(dt_result, current_day, current_device_id, current_month, 32, current_chart_url);
                }
            }
            catch (Exception exc)
            {

            }

            return dt_result;
        }

        public DataTable Execute(int range, DataTable dt_data)
        {
            DataTable dt_result = new DataTable();
            dt_result.Columns.Add(DEVICE_ID, typeof(int));
            dt_result.Columns.Add(MONTH, typeof(int));
            dt_result.Columns.Add(DAY, typeof(int));

            int i = 0;
            do
            {
                dt_result.Columns.Add("T0" + i + "h", typeof(decimal));
                dt_result.Columns.Add("T1" + i + "h", typeof(decimal));
                i = i + range;
            } while (i < 24);
            dt_result.Columns.Add(CHART_URL, typeof(string));
            try
            {
                if (dt_data != null && dt_data.Rows.Count > 0)
                {
                    int current_device_id = 0;
                    int current_year = 0;
                    int current_month = 0;
                    int current_day = 0;
                    double current_sum_t0 = 0;
                    double current_sum_t1 = 0;
                    int current_count_t0 = 0;
                    int current_count_t1 = 0;
                    string current_chart_url = string.Empty;
                    DataRow current_row = null;
                    int current_hour = 0;
                    DateTime date_from = (DateTime)dt_data.Rows[0]["DateFrom"];
                    DateTime date_to = (DateTime)dt_data.Rows[0]["DateTo"];

                    for (i = 0; i < dt_data.Rows.Count; i++)
                    {
                        int device_id = (int)dt_data.Rows[i][DEVICE_ID];
                        DateTime timestamp = (DateTime)dt_data.Rows[i][TIMESTAMP];
                        int hour = (int)(Math.Floor((double)timestamp.Hour / (double)range) * range);

                        if (device_id != current_device_id) //zmieniamy device
                        {
                            //4chart hack
                            if (current_device_id > 0)
                            {
                                //missing days for month and last device
                                AddMissingDays(dt_result, current_day, current_device_id, current_month, 32, current_chart_url);
                            }

                            current_device_id = device_id;
                            current_chart_url = GetChartURL(current_device_id, timestamp, date_from, date_to);
                            current_year = timestamp.Year;
                            current_month = timestamp.Month;
                            current_day = timestamp.Day;
                            current_hour = hour;
                            current_count_t0 = 0;
                            current_count_t1 = 0;
                            current_sum_t0 = 0;
                            current_sum_t1 = 0;

                            AddMissingDays(dt_result, 0, current_device_id, current_month, current_day, current_chart_url);

                            current_row = AddDeviceDayRow(dt_result, current_device_id, current_month, current_day, current_chart_url);

                            UpdateRowHour(current_row, current_hour, Convert.ToDouble(dt_data.Rows[i][T0]), Convert.ToDouble(dt_data.Rows[i][T1]), ref current_sum_t0, ref current_sum_t1, ref current_count_t0, ref current_count_t1);
                        }
                        else
                        {
                            //analizujemy ten sam dzień
                            if (current_year == timestamp.Year && current_month == timestamp.Month && current_day == timestamp.Day && current_hour != hour)
                            {
                                current_hour = hour;
                                current_count_t0 = 0;
                                current_count_t1 = 0;
                                current_sum_t0 = 0;
                                current_sum_t1 = 0;
                                UpdateRowHour(current_row, current_hour, Convert.ToDouble(dt_data.Rows[i][T0]), Convert.ToDouble(dt_data.Rows[i][T1]), ref current_sum_t0, ref current_sum_t1, ref current_count_t0, ref current_count_t1);
                            }
                            else if (current_year == timestamp.Year && current_month == timestamp.Month && current_day == timestamp.Day && current_hour == hour)
                            {
                                UpdateRowHour(current_row, current_hour, Convert.ToDouble(dt_data.Rows[i][T0]), Convert.ToDouble(dt_data.Rows[i][T1]), ref current_sum_t0, ref current_sum_t1, ref current_count_t0, ref current_count_t1);
                            }
                            //analizujemy inny dzień
                            else if (current_year == timestamp.Year && current_month == timestamp.Month && current_day != timestamp.Day)
                            {
                                //missing days for current month
                                AddMissingDays(dt_result, current_day, current_device_id, current_month, timestamp.Day, current_chart_url);

                                current_day = timestamp.Day;
                                current_hour = hour;
                                current_count_t0 = 0;
                                current_count_t1 = 0;
                                current_sum_t0 = 0;
                                current_sum_t1 = 0;

                                //new day
                                current_chart_url = GetChartURL(current_device_id, timestamp, date_from, date_to);
                                current_row = AddDeviceDayRow(dt_result, current_device_id, current_month, current_day, current_chart_url);
                                UpdateRowHour(current_row, current_hour, Convert.ToDouble(dt_data.Rows[i][T0]), Convert.ToDouble(dt_data.Rows[i][T1]), ref current_sum_t0, ref current_sum_t1, ref current_count_t0, ref current_count_t1);
                            }
                            //przeskakujemy do innego miesiąca/roku
                            else
                            {
                                if (current_year != timestamp.Year || current_month != timestamp.Month)
                                {
                                    AddMissingDays(dt_result, current_day, current_device_id, current_month, 32, current_chart_url);

                                    current_year = timestamp.Year;
                                    current_month = timestamp.Month;
                                    current_day = timestamp.Day;
                                    current_hour = hour;
                                    current_count_t0 = 0;
                                    current_count_t1 = 0;
                                    current_sum_t0 = 0;
                                    current_sum_t1 = 0;

                                    current_chart_url = GetChartURL(current_device_id, timestamp, date_from, date_to);

                                    //new day
                                    current_row = AddDeviceDayRow(dt_result, current_device_id, current_month, current_day, current_chart_url);
                                    UpdateRowHour(current_row, current_hour, Convert.ToDouble(dt_data.Rows[i][T0]), Convert.ToDouble(dt_data.Rows[i][T1]), ref current_sum_t0, ref current_sum_t1, ref current_count_t0, ref current_count_t1);
                                }
                            }
                        }
                    }

                    AddMissingDays(dt_result, current_day, current_device_id, current_month, 32, current_chart_url);
                }
            }
            catch (Exception exc)
            {

            }

            return dt_result;
        }

        private string GetChartByMinutesURL(int current_device_id, DateTime timestamp)
        {
            DateTime startDate = new DateTime(timestamp.Year, timestamp.Month, timestamp.Day, 0, 0, 0);
            DateTime endDate = new DateTime(timestamp.Year, timestamp.Month, timestamp.Day, 23, 59, 59, 59);
            return string.Format(
                //"http://192.168.125.40:2017/DeviceChart.ashx?DeviceID={0}&From={1}&To={2}"
                "DeviceChart.ashx?DeviceID={0}&From={1}&To={2}"
                , current_device_id
                , startDate
                , endDate
                );
        }

        private string GetChartURL(int current_device_id, DateTime timestamp, DateTime date_from, DateTime date_to)
        {
            DateTime last_month_date = GetLastDayMonthDate(timestamp);
            if (last_month_date > date_to)
            {
                last_month_date = date_to;
            }
            return string.Format(
                //"http://192.168.125.40:2017/DeviceChart.ashx?DeviceID={0}&From={1}&To={2}"
                "DeviceChart.ashx?DeviceID={0}&From={1}&To={2}"
                , current_device_id
                , timestamp.ToShortDateString()
                , last_month_date
                );
        }

        private DateTime GetLastDayMonthDate(DateTime dt)
        {
            DateTime d = dt;
            d = d.AddMonths(1);
            d = d.AddDays(-(d.Day));
            return d;
        }

        private void UpdateRowMinutes(DataRow current_row, int current_minutes, double t0, double t1, ref  double sum_t0, ref double sum_t1, ref int count_t0, ref int count_t1)
        {
            count_t0++;
            count_t1++;
            sum_t0 += t0;
            sum_t1 += t1;
            current_row["T0" + current_minutes + "h"] = sum_t0 / count_t0;
            current_row["T1" + current_minutes + "h"] = sum_t1 / count_t1;
        }

        private void UpdateRowHour(DataRow current_row, int current_hour, double t0, double t1, ref  double sum_t0, ref double sum_t1, ref int count_t0, ref int count_t1)
        {
            count_t0++;
            count_t1++;
            sum_t0 += t0;
            sum_t1 += t1;
            current_row["T0" + current_hour + "h"] = sum_t0 / count_t0;
            current_row["T1" + current_hour + "h"] = sum_t1 / count_t1;
        }

        private void AddMissingDays(DataTable dt_result, int current_day, int current_device_id, int current_month, int to_days, string chart)
        {
            if (current_month == 0)
            {
                current_month = 1;
            }

            if (current_device_id > 0)
            {
                //for (int m = current_day + 1; m < to_days; m++)
                {
                    //missing days in current month
                    //AddDeviceDayRow(dt_result, current_device_id, current_month, m, chart);
                }
            }
        }

        private DataRow AddDeviceDayRow(DataTable dt, int device, int month, int day, string chart)
        {
            DataRow row = dt.NewRow();

            row[DEVICE_ID] = device;
            row[MONTH] = month;
            row[DAY] = day;
            row[CHART_URL] = chart;

            dt.Rows.Add(row);

            return row;
        }

        private DataRow AddDeviceDayRow(DataTable dt, int device, int month, int day, int hour, string chart)
        {
            DataRow row = dt.NewRow();

            row[DEVICE_ID] = device;
            row[MONTH] = month;
            row[DAY] = day;
            row[CHART_URL] = chart;
            row[HOUR] = hour;

            dt.Rows.Add(row);

            return row;
        }
    }
}
