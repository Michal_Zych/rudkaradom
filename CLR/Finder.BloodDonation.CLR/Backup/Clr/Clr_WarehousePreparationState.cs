﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using Finder.BloodDonation.CLR.Logic;


public partial class StoredProcedures
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void Clr_WarehousePreparationState(SqlInt32 UserReportID)
    {
        WarehousePreparationState wps = new WarehousePreparationState(UserReportID.Value);
        DataTable result = wps.Execute();
        SendDataTableOverPipe(result);
    }
};
