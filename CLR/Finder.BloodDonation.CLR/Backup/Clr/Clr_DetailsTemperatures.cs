﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using Finder.BloodDonation.CLR.Logic;


public partial class StoredProcedures
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void Clr_DetailsTemperatures(SqlInt32 UserReportID)
    {
        DetailsTemperatures dt = new DetailsTemperatures(UserReportID.Value);
        DataTable dataTable = dt.Execute();
        SendDataTableOverPipe(dataTable);
    }
};
