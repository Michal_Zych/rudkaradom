﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Collections.Generic;
using Finder.BloodDonation.CLR.Logic;


public partial class StoredProcedures
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void Reports_MeasureArray(SqlInt32 UserReportID, SqlInt32 range)
    {
        DataTable dtData = new DataTable();

        using (SqlConnection connection = new SqlConnection("context connection=true;"))
        {
            connection.Open();

            SqlCommand command = new SqlCommand("Reports_GetMeasureArrayData", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("UserReportID", UserReportID.Value));

            using (command)
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    dtData.Load(reader);
                }
            }
        }

        MeasureArrayLogic logic = new MeasureArrayLogic();
        DataTable result = logic.Execute(range.Value, dtData);

        SendDataTableOverPipe(result);
    }

    private static void SendDataTableOverPipe(DataTable tbl)
    {
        List<SqlMetaData> OutputColumns = new List<SqlMetaData>(tbl.Columns.Count);
        foreach (DataColumn col in tbl.Columns)
        {
            SqlMetaData OutputColumn = ToSqlDataType(col.ColumnName, col.DataType);
            OutputColumns.Add(OutputColumn);
        }
        SqlDataRecord record = new SqlDataRecord(OutputColumns.ToArray());
        if (SqlContext.Pipe.IsSendingResults)
        {
            SqlContext.Pipe.SendResultsEnd();
        }

        SqlContext.Pipe.SendResultsStart(record);
        foreach (DataRow row in tbl.Rows)
        {
            for (int col = 0; col < tbl.Columns.Count; col++)
            {
                record.SetValue(col, row.ItemArray[col]);
            }
            SqlContext.Pipe.SendResultsRow(record);
        }
        SqlContext.Pipe.SendResultsEnd();
    }

    private static SqlMetaData ToSqlDataType(string column, Type typ)
    {
        if (typ == typeof(int))
        {
            return new SqlMetaData(column, SqlDbType.Int);
        }
        else if (typ == typeof(double) || typ == typeof(decimal))
        {
            return new SqlMetaData(column, SqlDbType.Decimal, 10, 3);
        }
        else if (typ == typeof(DateTime))
        {
            return new SqlMetaData(column, SqlDbType.DateTime);
        }
        else if (typ == typeof(short))
        {
            return new SqlMetaData(column, SqlDbType.SmallInt);
        }
        else if (typ == typeof(byte))
        {
            return new SqlMetaData(column, SqlDbType.TinyInt);
        }
        else if (typ == typeof(bool))
        {
            return new SqlMetaData(column, SqlDbType.Bit);
        }

        return new SqlMetaData(column, SqlDbType.VarChar, 1024);
    }
};
