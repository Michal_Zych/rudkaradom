﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Collections.Generic;
using Finder.BloodDonation.CLR.Logic;


public partial class StoredProcedures
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void Chart_GetDeviceData(SqlInt32 DeviceID, SqlDateTime date_from, SqlDateTime date_to)
    {
        DataTable dtData = new DataTable();

        using (SqlConnection connection = new SqlConnection("context connection=true;"))
        {
            connection.Open();

            SqlCommand command = new SqlCommand("Chart_ChartsDeviceData", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("device_id", DeviceID.Value));
            command.Parameters.Add(new SqlParameter("date_from", date_from.Value));
            command.Parameters.Add(new SqlParameter("date_to", date_to.Value));

            using (command)
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    dtData.Load(reader);
                }
            }
        }

        ChartGeneralizationLogic logic = new ChartGeneralizationLogic();
        DataTable result = logic.Execute(dtData);

        SendDataTableOverPipe(result);
    }
};
