﻿CREATE TABLE [dbo].[sync_EventsIN] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [StoreDateUtc]  DATETIME       NOT NULL,
    [EventDateUtc]  DATETIME       NULL,
    [EventType]     NVARCHAR (100) NOT NULL,
    [EventRecordId] NVARCHAR (100) NULL,
    [Record]        NVARCHAR (MAX) NOT NULL,
    [Error]         NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);



