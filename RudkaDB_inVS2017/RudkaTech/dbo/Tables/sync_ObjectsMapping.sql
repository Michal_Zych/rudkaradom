﻿CREATE TABLE [dbo].[sync_ObjectsMapping] (
    [Id]             INT            IDENTITY (1, 1) NOT NULL,
    [ObjectType]     TINYINT        NOT NULL,
    [ExtId]          NVARCHAR (100) NOT NULL,
    [M2mId]          INT            NULL,
    [ExtDescription] NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_sync_ObjectsMapping] PRIMARY KEY CLUSTERED ([Id] ASC)
);

