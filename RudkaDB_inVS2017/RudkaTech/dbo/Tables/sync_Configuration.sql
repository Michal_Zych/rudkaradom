﻿CREATE TABLE [dbo].[sync_Configuration] (
    [Key]                    NVARCHAR (50)  NOT NULL,
    [UserName]               NVARCHAR (100) NULL,
    [Password]               NVARCHAR (100) NULL,
    [ConnectionString]       NVARCHAR (512) NULL,
    [TopicName]              NVARCHAR (512) NULL,
    [EscapeValue]            NVARCHAR (20)  NULL,
    [EscapeValueLine]        NVARCHAR (20)  NULL,
    [CheckCmdsIntervalMilis] NVARCHAR (20)  NULL,
    [NewQueryLimit]          NVARCHAR (10)  NULL,
    [WebServiceUserName]     NVARCHAR (100) NULL,
    [WebServicePassword]     NVARCHAR (100) NULL,
    [WebServiceURL]          NVARCHAR (512) NULL,
    [ActiveMQConnectionId]   NVARCHAR (20)  NULL,
    [KeepAliveIntervalS]     NVARCHAR (10)  NULL,
    PRIMARY KEY CLUSTERED ([Key] ASC)
);

