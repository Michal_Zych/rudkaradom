﻿CREATE TABLE [dbo].[sync_RecordsIdentifiers] (
    [RecType] NVARCHAR (100) NOT NULL,
    [IdKey]   NVARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([RecType] ASC)
);

