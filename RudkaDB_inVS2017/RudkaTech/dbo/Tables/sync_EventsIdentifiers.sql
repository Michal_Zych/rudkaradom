﻿CREATE TABLE [dbo].[sync_EventsIdentifiers] (
    [EventType]   NVARCHAR (100) NOT NULL,
    [IdKey]       NVARCHAR (100) NOT NULL,
    [RecIds]      NVARCHAR (100) NOT NULL,
    [RecQueryKey] NVARCHAR (100) NOT NULL,
    PRIMARY KEY CLUSTERED ([EventType] ASC)
);

