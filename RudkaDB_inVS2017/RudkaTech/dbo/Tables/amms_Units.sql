﻿CREATE TABLE [dbo].[amms_Units] (
    [UnitCode]    NVARCHAR (100) NOT NULL,
    [UnitName]    NVARCHAR (512) NULL,
    [UnitType]    NVARCHAR (10)  NULL,
    [UnitSubType] NVARCHAR (10)  NULL,
    PRIMARY KEY CLUSTERED ([UnitCode] ASC)
);

