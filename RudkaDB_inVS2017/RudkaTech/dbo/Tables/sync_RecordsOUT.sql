﻿CREATE TABLE [dbo].[sync_RecordsOUT] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [EventId]     INT            NULL,
    [RecType]     CHAR (4)       NOT NULL,
    [DateUtc]     DATETIME       NOT NULL,
    [Record]      NVARCHAR (MAX) NOT NULL,
    [ParentID]    INT            NULL,
    [Try]         INT            NULL,
    [TryAgain]    BIT            NULL,
    [SentDateUtc] DATETIME       NULL,
    [Error]       NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

