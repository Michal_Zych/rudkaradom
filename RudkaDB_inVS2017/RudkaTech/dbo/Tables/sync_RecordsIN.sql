﻿CREATE TABLE [dbo].[sync_RecordsIN] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [OutRecordId]      INT            NOT NULL,
    [RecType]          CHAR (4)       NOT NULL,
    [RecId]            NVARCHAR (100) NULL,
    [Record]           NVARCHAR (MAX) NOT NULL,
    [StoreDateUtc]     DATETIME       NULL,
    [Processed]        BIT            NOT NULL,
    [Try]              INT            NULL,
    [ProcessedDateUtc] DATETIME       NULL,
    [Error]            NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

