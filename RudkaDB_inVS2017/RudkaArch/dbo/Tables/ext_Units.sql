﻿CREATE TABLE [dbo].[ext_Units] (
    [ExtId]          NVARCHAR (100) NOT NULL,
    [M2mId]          INT            NULL,
    [ExtDescription] NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ext_Units] PRIMARY KEY CLUSTERED ([ExtId] ASC)
);

