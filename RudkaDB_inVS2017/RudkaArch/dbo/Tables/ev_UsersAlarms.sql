﻿CREATE TABLE [dbo].[ev_UsersAlarms] (
    [EventId]       INT            NOT NULL,
    [UserMessageId] INT            NOT NULL,
    [UserId]        INT            NOT NULL,
    [Message]       NVARCHAR (500) NOT NULL,
    PRIMARY KEY CLUSTERED ([EventId] ASC)
);

