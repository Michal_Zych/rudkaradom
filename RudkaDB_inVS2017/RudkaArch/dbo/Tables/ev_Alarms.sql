﻿CREATE TABLE [dbo].[ev_Alarms] (
    [EventId]        INT            NOT NULL,
    [RaisingEventId] INT            NOT NULL,
    [IsClosed]       BIT            DEFAULT ((0)) NOT NULL,
    [ClosingDateUtc] DATETIME       NULL,
    [ClosingUserId]  INT            NULL,
    [ClosingAlarmId] INT            NULL,
    [ClosingComment] NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([EventId] ASC)
);

