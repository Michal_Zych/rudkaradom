﻿CREATE TABLE [dbo].[ev_Events] (
    [Id]            INT            NOT NULL,
    [DateUtc]       DATETIME       NOT NULL,
    [Type]          TINYINT        NOT NULL,
    [Severity]      TINYINT        NOT NULL,
    [UnitId]        INT            NULL,
    [ObjectId]      INT            NOT NULL,
    [ObjectType]    TINYINT        NOT NULL,
    [ObjectUnitId]  INT            NULL,
    [StoreDateUtc]  DATETIME       NOT NULL,
    [EndDateUtc]    DATETIME       NULL,
    [EndingEventId] INT            NULL,
    [OperatorId]    INT            NULL,
    [EventData]     NVARCHAR (MAX) NULL,
    [Reason]        NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_ev_Events] PRIMARY KEY CLUSTERED ([Id] ASC)
);

