﻿CREATE TABLE [dbo].[Patients] (
    [Id]       INT            NOT NULL,
    [Name]     NVARCHAR (100) NOT NULL,
    [LastName] NVARCHAR (100) NOT NULL,
    [Pesel]    CHAR (11)      NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

