﻿CREATE TABLE [dbo].[ext_Patients] (
    [ExtId]               NVARCHAR (100) NOT NULL,
    [M2mId]               INT            NULL,
    [VisitId]             NVARCHAR (10)  NULL,
    [LastVisitEndDateUtc] DATETIME       NULL,
    CONSTRAINT [PK_ext_Patients] PRIMARY KEY CLUSTERED ([ExtId] ASC)
);





