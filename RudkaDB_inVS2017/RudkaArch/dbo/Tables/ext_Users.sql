﻿CREATE TABLE [dbo].[ext_Users] (
    [ExtId]          NVARCHAR (100) NOT NULL,
    [M2mId]          INT            NULL,
    [ExtDescription] NVARCHAR (MAX) NOT NULL,
    CONSTRAINT [PK_ext_Users] PRIMARY KEY CLUSTERED ([ExtId] ASC)
);

