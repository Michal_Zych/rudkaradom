﻿/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
TRUNCATE TABLE [RudkaArch].dbo.[Patients]
TRUNCATE TABLE [RudkaArch].dbo.[ext_Patients]
TRUNCATE TABLE [RudkaArch].dbo.[ev_Events]
TRUNCATE TABLE [RudkaArch].dbo.[ev_Alarms]
TRUNCATE TABLE [RudkaArch].dbo.[ev_UsersAlarms]

TRUNCATE TABLE dev_SenseTypes

TRUNCATE TABLE dev_ModelSenses


TRUNCATE TABLE dev_Senses

TRUNCATE TABLE dev_sensors

TRUNCATE TABLE sockets

TRUNCATE TABLE sensors




TRUNCATE TABLE ArchivingOrders;

TRUNCATE TABLE enum_EventSeverityDictionaryPl;

TRUNCATE TABLE dbo.enum_EventTypeDictionaryPl;

TRUNCATE TABLE dbo.enum_ObjectTypeDictionaryPl;

TRUNCATE TABLE UnitTypesV2

TRUNCATE TABLE ev_SettingsHistory

TRUNCATE TABLE ev_BandStatus

TRUNCATE TABLE ev_Events

TRUNCATE TABLE BatteryTypes;

TRUNCATE TABLE Bands;

TRUNCATE TABLE BandSettings;

TRUNCATE TABLE al_ConfigUnits;

TRUNCATE TABLE al_CallingGroups;

TRUNCATE TABLE al_NoGoZones

TRUNCATE TABLE Units;


TRUNCATE TABLE dev_Transmitters;
TRUNCATE TABLE ev_TransmitterStatus
TRUNCATE TABLE dev_UnitTransmitter


TRUNCATE TABLE Patients;

TRUNCATE TABLE dbo.ev_SystemEventTypes;


TRUNCATE TABLE Reports;

TRUNCATE TABLE sync_Configuration;