﻿CREATE PROCEDURE [arch].[PatientToArchive]
	@PatientId int
AS
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
		INSERT dbo.ArchivingOrders(PatientId, MaxEventId)
		SELECT @PatientId, (SELECT MAX(Id) FROM dbo.ev_Events)

		-- alarmy użytkownika
		DELETE dbo.ev_UsersAlarms
		OUTPUT	 deleted.EventId
				,deleted.UserMessageId
				,deleted.UserId
				,deleted.[Message]
		INTO [$(RudkaArch)].dbo.ev_UsersAlarms([EventId], [UserMessageId], [UserId], [Message])
		FROM dbo.ev_UsersAlarms ua
		JOIN dbo.ev_Events e ON ua.EventId = e.Id
		JOIN enum.ObjectType objectType ON e.ObjectType = objectType.Patient AND e.ObjectId = @PatientId
		
		-- alarmy
		DELETE dbo.ev_Alarms
		OUTPUT   deleted.EventId
				,deleted.RaisingEventId
				,deleted.IsClosed
				,deleted.ClosingDateUtc
				,deleted.ClosingUserId
				,deleted.ClosingAlarmId
				,deleted.ClosingComment
		INTO [$(RudkaArch)].dbo.ev_Alarms([EventId] ,[RaisingEventId] ,[IsClosed] ,[ClosingDateUtc] ,[ClosingUserId] ,[ClosingAlarmId], [ClosingComment])
		FROM dbo.ev_Alarms a
		JOIN dbo.ev_Events e ON a.EventId = e.Id
		JOIN enum.ObjectType objectType ON e.ObjectType = objectType.Patient AND e.ObjectId = @PatientId

		-- zdarzenia
		DELETE dbo.ev_Events
		OUTPUT	 deleted.Id
				,deleted.DateUtc
				,deleted.Type
				,deleted.Severity
				,deleted.UnitId
				,deleted.ObjectId
				,deleted.ObjectType
				,deleted.ObjectUnitId
				,deleted.StoreDateUtc
				,deleted.EndDateUtc
				,deleted.EndingEventId
				,deleted.OperatorId
				,deleted.EventData
				,deleted.Reason
		INTO [$(RudkaArch)].dbo.ev_Events([Id] ,[DateUtc] ,[Type] ,[Severity] ,[UnitId] ,[ObjectId] ,[ObjectType] ,[ObjectUnitId] 
										,[StoreDateUtc] ,[EndDateUtc] ,[EndingEventId] ,[OperatorId] ,[EventData] ,[Reason])
		FROM dbo.ev_Events e
		JOIN enum.ObjectType objectType ON e.ObjectType = objectType.Patient
		WHERE e.ObjectId = @PatientId

		-- pacjent
		DELETE arch.Patients WHERE Id = @PatientId

		DELETE dbo.Patients
		OUTPUT	 deleted.[Id]
				,deleted.[Name]
				,deleted.[LastName]
				,deleted.[Pesel]
		INTO [$(RudkaArch)].dbo.Patients( [Id], [Name], [LastName], [Pesel])
		WHERE Id = @PatientId

		-- KASOWANIE
		DELETE er
		FROM dbo.ev_EventsToRaise er
		JOIN enum.ObjectType objectType ON er.ObjectType = objectType.Patient
		WHERE er.ObjectId = @PatientId

		DELETE al_ConfigPatients WHERE PatientId = @PatientId

		DELETE c
		FROM dbo.al_CallingGroups c
		JOIN enum.ObjectType objectType ON c.ObjectType = objectType.Patient
		WHERE c.Id = @PatientId

		DELETE n
		FROM dbo.al_NoGoZones n
		JOIN enum.ObjectType objectType ON n.ObjectType = objectType.Patient
		WHERE n.Id = @PatientId

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		RAISERROR('Błąd przenoszenia pacjenta do archiwum', 16, 1)
	END CATCH
	
END

