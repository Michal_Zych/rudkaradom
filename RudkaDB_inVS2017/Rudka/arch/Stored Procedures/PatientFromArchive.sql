﻿CREATE PROCEDURE arch.PatientFromArchive
	@Name nvarchar(100)
	,@LastName nvarchar(100)
	,@Pesel char(11)
	,@PatientId int OUT
AS
BEGIN
	SELECT @PatientId = Id
	FROM [arch].[Patients] p
	WHERE COALESCE(@Pesel, '') = COALESCE(Pesel, '')
		AND @Name = Name
		AND @LastName = LastName
END

