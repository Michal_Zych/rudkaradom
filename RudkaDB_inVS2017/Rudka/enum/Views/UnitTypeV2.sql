﻿


create view [enum].[UnitTypeV2] AS
SELECT
	CAST(1 AS tinyint) AS [Zone]
	,CAST(2 AS tinyint) AS [Hospital]
	,CAST(3 AS tinyint) AS [Department]
	,CAST(4 AS tinyint) AS [Registration]
	,CAST(5 AS tinyint) AS [UnassignedPatients]
	,CAST(6 AS tinyint) AS [Undefined]