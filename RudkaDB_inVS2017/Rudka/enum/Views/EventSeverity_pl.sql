﻿

CREATE VIEW [enum].[EventSeverity_pl] AS
SELECT
	 [Event]	AS [Zdarzenie]
	,[Warning]	AS [Ostrzeżenie]
	,[Alarm]	AS [Alarm]
FROM enum.EventSeverity