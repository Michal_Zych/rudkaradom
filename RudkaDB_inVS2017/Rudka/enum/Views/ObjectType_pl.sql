﻿

CREATE view [enum].[ObjectType_pl] AS
SELECT
	[Unit]							AS [Jednostka organizacyjna]
	,[User]							AS [Użytkownik]
	,[Band]							AS [Opaska]
	,[Transmitter]					AS [Transmiter]
	,[Patient]						AS [Pacjent]
	,[UnitAlarmConfiguration]		AS [Alarmowanie w JO]
	,[PatientAlarmConfiguration]	AS [Alarmowanie dla Pacjenta]
	,[UnitMapping]					AS [Mapowanie JO]
	,[TransmitterGroup]				AS [Grupa Transmiterów]
	,[AmmsImport]					AS [Import z Amms]
	FROM enum.ObjectType