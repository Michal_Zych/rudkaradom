﻿CREATE VIEW [enum].[EventType]
AS
SELECT 
  CAST(00 AS [tinyint]) AS [Delete]
, CAST(01 AS [tinyint]) AS [Create]
, CAST(02 AS [tinyint]) AS [Update]
, CAST(03 AS [tinyint]) AS RegisterInHostpital
, CAST(04 AS [tinyint]) AS RegisterOutHostpital
, CAST(05 AS [tinyint]) AS RegisterInDepartment
, CAST(06 AS [tinyint]) AS RegisterOutDepartment
, CAST(07 AS [tinyint]) AS RegisterInRoom
, CAST(08 AS [tinyint]) AS RegisterOutRoom
, CAST(09 AS [tinyint]) AS BandAssignment
, CAST(10 AS [tinyint]) AS BatteryOk
, CAST(11 AS [tinyint]) AS Connected
, CAST(12 AS [tinyint]) AS ZoneIn
, CAST(13 AS [tinyint]) AS NoGoZoneOut
, CAST(14 AS [tinyint]) AS UserMessage
, CAST(15 AS [tinyint]) AS LowBattery
, CAST(16 AS [tinyint]) AS Moving
, CAST(17 AS [tinyint]) AS NoMoving
, CAST(18 AS [tinyint]) AS ZoneOut
, CAST(19 AS [tinyint]) AS ACC
, CAST(20 AS [tinyint]) AS Disconnected
, CAST(21 AS [tinyint]) AS NoGoZoneIn
, CAST(22 AS [tinyint]) AS SOS

, CAST(23 AS [tinyint]) AS TransmitterAssignment
, CAST(24 AS [tinyint]) AS LogOn
, CAST(25 AS [tinyint]) AS LogOff

, CAST(26 AS [tinyint]) AS BatteryChange

, CAST(250 AS [tinyint]) AS AmmsImport
, CAST(255 AS [tinyint]) AS DbError

