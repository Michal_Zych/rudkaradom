﻿CREATE view [enum].[ObjectType] AS
SELECT
	 CAST(0 AS tinyint) AS [Unit]
	,CAST(1 AS tinyint) AS [User]
	,CAST(2 AS tinyint) AS [Band]
	,CAST(3 AS tinyint) AS [Transmitter]
	,CAST(4 AS tinyint) AS [Patient]
	,CAST(5 AS tinyint) AS [UnitAlarmConfiguration]
	,CAST(6 AS tinyint) AS [PatientAlarmConfiguration]
	,CAST(7 AS tinyint) AS [UnitMapping]
	,CAST(8 AS tinyint) AS [TransmitterGroup]
	,CAST(9 AS tinyint) AS [AmmsImport]