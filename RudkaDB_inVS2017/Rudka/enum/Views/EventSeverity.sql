﻿CREATE VIEW [enum].[EventSeverity] AS
SELECT
	 CAST(00 AS tinyint) AS [Event]
	,CAST(01 AS tinyint) AS [Warning]
	,CAST(02 AS tinyint) AS [Alarm]
