﻿


CREATE VIEW [dbo].[SyncRecordsIN]
AS
	SELECT Type AS Type
		,Id AS Id
		,RequestId AS RequestId
		,StoreDateUtc AS StoreDateUtc
		,RecordType AS RecordType
		,RecordId AS RecordId
		,Record AS Record
		,Processed AS Processed
		,Try AS Try
		,ProcessedDateUtc AS ProcessedDateUtc
		,Error AS Error
	FROM 
	(SELECT 'Z'  AS Type
		,Id				AS Id
		,NULL			AS RequestId
		,StoreDateUtc	AS StoreDateUtc
		,EventType		AS RecordType
		,EventRecordId	AS RecordId
		,Record			AS Record
		,1				AS Processed
		,1				AS Try
		,StoreDateUtc	AS ProcessedDateUtc
		,Error			AS Error
	FROM tech.sync_EventsIN
	UNION ALL
	SELECT 'O'
		,Id
		,OutRecordId
		,StoreDateUtc
		,RecType
		,RecId
		,Record
		,Processed
		,Try
		,ProcessedDateUtc
		,Error
	FROM tech.sync_RecordsIN) A