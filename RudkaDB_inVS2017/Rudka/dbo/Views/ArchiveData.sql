﻿
CREATE VIEW [dbo].[ArchiveData] AS
	SELECT e.Id AS EventId
		,patients.Id AS PatientId
		,patients.Name AS Name
		,patients.LastName AS LastName
		,dbo.GetDisplayName(patients.Id, objectType.Patient) as DisplayName
		,patients.Pesel AS Pesel
		,e.DateUtc AS DateUtc
		,e.Type AS [Type]
		,typeEnum.Description AS TypeDescription
		,e.Severity AS Severity
		, severityEnum.Description AS SeverityDescription
		, e.UnitId, u.Name AS UnitName
		, dbo.GetUnitLocation(e.UnitId, DEFAULT, DEFAULT, DEFAULT) AS UnitLocation
		, e.ObjectId AS ObjectId
		, e.ObjectType AS ObjectType
		, objectTypeEnum.Description AS ObjectTypeDescription
		, dbo.GetDisplayName(e.ObjectId, e.ObjectType) AS ObjectName
		, e.ObjectUnitId AS ObjectUnitId
		, uo.Name AS ObjectUnitName
		, dbo.GetUnitLocation(e.ObjectUnitId, DEFAULT, DEFAULT, DEFAULT) AS ObjectUnitLocation
		, e.EndDateUtc AS EndDateUtc
		, e.EndingEventId AS EndingEventId
		, e.OperatorId AS OperatorId
		, usr.Name AS OperatorName
		, usr.LastName AS OperatorLastName
		, e.EventData AS [EventData]
		, e.Reason AS Reason
		, a.RaisingEventId AS RaisingEventId
		, a.IsClosed AS IsClosed
		, a.ClosingDateUtc AS ClosingDateUtc
		, a.ClosingUserId AS ClosingUserId
		, ausr.Name AS ClosingUserName
		, ausr.LastName AS ClosingUserLastName
		, a.ClosingAlarmId AS ClosingAlarmId
		, a.ClosingComment AS ClosingComment
		, ua.UserMessageId AS UserMessageId
		, ua.UserId AS SenderId
		, ausr.Name AS SenderName
		, ausr.LastName AS SenderLastName
		, ua.Message AS [Message]
	FROM [$(RudkaArch)].dbo.ev_Events AS e 
	JOIN enum.ObjectType objectType ON 1 = 1
	LEFT JOIN dbo.ev_Alarms AS a ON a.EventId = e.Id 
	LEFT JOIN dbo.ev_UsersAlarms AS ua ON a.EventId = ua.EventId 
	LEFT JOIN dbo.Units AS u ON e.UnitId = u.ID 
	LEFT JOIN dbo.Units AS uo ON e.ObjectUnitId = uo.ID 
	LEFT JOIN dbo.Users AS usr ON e.OperatorId = usr.ID 
	LEFT JOIN dbo.Users AS ausr ON a.ClosingUserId = ausr.ID 
	LEFT JOIN dbo.Users AS musr ON ua.UserId = musr.ID 
	LEFT JOIN dbo.enum_EventTypeDictionaryPl AS typeEnum ON typeEnum.Value = e.Type 
	LEFT JOIN dbo.enum_EventSeverityDictionaryPl AS severityEnum ON severityEnum.Value = e.Severity 
	LEFT JOIN dbo.enum_ObjectTypeDictionaryPl AS objectTypeEnum ON objectTypeEnum.Value = e.ObjectType
	LEFT JOIN [$(RudkaArch)].dbo.Patients AS patients ON e.ObjectId = patients.Id
	WHERE e.ObjectType = objectType.Patient;