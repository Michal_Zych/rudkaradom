﻿






CREATE VIEW [dbo].[BandsStatuses] AS
SELECT 
	bs.BandId AS BandId
	,bs.ObjectType AS ObjectType
	,ot.Description AS ObjectTypeName
	,bs.ObjectId AS ObjectId
	,dbo.GetDisplayName(bs.ObjectId, bs.ObjectType) AS ObjectDisplayName
	,bs.AssignDateUtc AS AssignDateUtc
	,bs.ObjectUnitId AS ObjectUnitId
	,obu.Name AS ObjectUnitName
	,dbo.GetUnitLocation(bs.ObjectUnitId, default, default, default) AS ObjectUnitLocation
	,bs.CurrentUnitId AS CurrentUnitId
	,cru.Name AS CurrentUnitName
	,dbo.GetUnitLocation(bs.CurrentUnitId, default, default, default) AS CurrentUnitLocation
	,bs.InCurrentUnitUtc AS InCUrrentUnitUtc
	,bs.PreviousUnitId AS PreviousUnitId
	,pru.Name AS PreviousUnitName
	,dbo.GetUnitLocation(bs.PreviousUnitId, default, default, default) AS PreviousUnitLocation
	,bs.InPreviousUnitUtc AS InPreviousUnitUtc
	,bs.TopAlarmId AS TopAlarmId
	,bs.TopAlarmType AS TopAlarmType
	,et.Description AS TopAlarmTypeName
	,bs.TopAlarmSeverity AS TopAlarmSeverity
	,es.Description AS TopAlarmSevertityName
	,ua.Message AS TopAlarmMessage
	,bs.TopAlarmDateUtc AS TopAlarmDateUtc
	,bs.TopEventDateUtc AS TopEventDateUtc
	,bs.Severity1Count AS Severity1Count
	,bs.Severity2Count AS Severtity2Count
	
	,CONVERT(bit, COALESCE(CONVERT(bit, bs.OngNoGoZoneInAlarmId), 0) +  (CONVERT(bit, bs.OngNoGoZoneInEventId) - 1)) as AlarmNoGoZoneOngoing
	,CONVERT(bit, COALESCE(CONVERT(bit, bs.OngDisconnectedAlarmId), 0) +  (CONVERT(bit, bs.OngDisconnectedEventId) - 1)) as AlarmDisconnectedOngoing
	,CONVERT(bit, COALESCE(CONVERT(bit, bs.OngZoneOutAlarmId), 0) +  (CONVERT(bit, bs.OngZoneOutEventId) - 1)) as AlarmZoneOutOngoing
	,CONVERT(bit, COALESCE(CONVERT(bit, bs.OngNoMovingAlarmId), 0) +  (CONVERT(bit, bs.OngNoMovingEventId) - 1)) as AlarmNoMovingOngoing
	,CONVERT(bit, COALESCE(CONVERT(bit, bs.OngMovingAlarmId), 0) +  (CONVERT(bit, bs.OngMovingEventId) - 1)) as AlarmMovingOngoing
	,CONVERT(bit, COALESCE(CONVERT(bit, bs.OngLowBatteryAlarmId), 0) +  (CONVERT(bit, bs.OngLowBatteryEventId) - 1)) as AlarmLowBatteryOngoing
	,CONVERT(bigint, bs.SynchroToken) as SynchroToken
	
--,*
FROM dbo.ev_BandStatus bs
	JOIN dbo.enum_ObjectTypeDictionaryPl ot ON bs.ObjectType = ot.Value
	LEFT JOIN dbo.Units obu ON obu.ID = bs.ObjectUnitId
	LEFT JOIN dbo.Units cru ON cru.ID = bs.CurrentUnitId
	LEFT JOIN dbo.Units pru ON pru.ID = bs.PreviousUnitId
	LEFT JOIN dbo.enum_EventTypeDictionaryPl et ON et.Value = bs.TopAlarmType
	LEFT JOIN dbo.enum_EventSeverityDictionaryPl es ON es.Value = bs.TopAlarmSeverity
	LEFT JOIN ev_UsersAlarms ua ON bs.TopAlarmId = ua.EventId