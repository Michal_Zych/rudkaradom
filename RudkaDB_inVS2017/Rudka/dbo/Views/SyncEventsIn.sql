﻿
CREATE VIEW [dbo].[SyncEventsIn] AS
	SELECT e.Id as EventId
      ,e.StoreDateUtc as StoreDateUtc
      ,e.EventDateUtc as EventDateUtc
      ,e.EventType as EventType
      ,e.EventRecordId as EventRecordId
      ,e.Record	as Record
      ,e.Error as Error
  FROM tech.sync_EventsIN e
  WHERE 1 = 1