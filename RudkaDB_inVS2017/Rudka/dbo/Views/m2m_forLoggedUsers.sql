﻿CREATE VIEW dbo.[m2m_forLoggedUsers]
AS
SELECT     TOP (100) PERCENT ul.Id, u.ClientID, dbo.Units.Name AS Unit, ul.UserID, u.Login, u.Name, u.LastName, ul.LoggingDate, ul.LastPingDate, ul.Monitoring, u.Phone
FROM         dbo.UsersLogs AS ul INNER JOIN
                      dbo.Users AS u ON u.ID = ul.UserID INNER JOIN
                      dbo.UsersUnitsRoles ON u.ID = dbo.UsersUnitsRoles.UserID INNER JOIN
                      dbo.Units ON dbo.UsersUnitsRoles.UnitID = dbo.Units.ID
WHERE     (ul.LastPingDate > dbo.GetCurrentDate() - '00:01:00')
ORDER BY ul.LastPingDate

GO
