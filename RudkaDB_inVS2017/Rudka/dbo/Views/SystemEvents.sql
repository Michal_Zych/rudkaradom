﻿

CREATE VIEW [dbo].[SystemEvents]
AS
	SELECT e.Id AS Id
		, e.DateUtc AS DateUtc
		, e.Type AS Type
		, eventType.Description AS TypeDescription
		, e.UnitId AS UnitId
		, dbo.GetUnitLocation(e.UnitId, 1, 1, default) AS UnitLocation
		, e.ObjectId AS ObjectId
		, e.ObjectType AS ObjectType
		, objectType.Description AS ObjectTypeDescription
		, dbo.GetObjectName(e.ObjectId, e.ObjectType) AS ObjectName
		, e.OperatorId AS OperatorId
		, COALESCE(usr.LastName, '') + COALESCE(' ' + usr.Name, '') AS OperatorName
		, CONVERT(nvarchar(200), e.[EventData]) as [EventData]
		, CONVERT(nvarchar(200), e.Reason) AS Reason
	FROM dbo.ev_Events e
	JOIN dbo.enum_EventTypeDictionaryPl eventType ON e.Type = eventType.Value
	JOIN dbo.enum_ObjectTypeDictionaryPl objectType ON e.ObjectType = objectType.Value
	LEFT JOIN dbo.Users AS usr ON e.OperatorId = usr.ID
	JOIN dbo.ev_SystemEventTypes sev ON sev.EventTypeId = e.Type