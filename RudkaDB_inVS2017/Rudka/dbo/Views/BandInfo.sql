﻿


CREATE view [dbo].[BandInfo] as
	SELECT b.Id as Id
		,b.BarCode as BarCode
		,b.MacString as MacString
		,b.Uid as Uid
		,b.SerialNumber as SerialNumber
		,bt.Id as BatteryTypeId
		,bt.Name as BatteryTypeName
		,COALESCE(e.Severity, CASE WHEN bs.OngLowBatteryEventId IS NOT NULL THEN 1 ELSE NULL END) as BatteryAlarmSeverityId
		,es.Description as BatteryAlarmSeverityName
		,b.BatteryInstallationDateUtc as BatteryInstallationDateUtc
		,DATEDIFF(DAY, dbo.GetCurrentUtcDate(), DATEADD(MONTH, bt.LifeMonths, b.BatteryInstallationDateUtc)) as BatteryDaysToExchange
		,b.Description as Description
		,bs.ObjectType as ObjectTypeId
		,ot.Description as ObjectTypeName
		,dbo.GetDisplayName(bs.ObjectId, bs.ObjectType) as ObjectDisplayName
		,b.UnitId as UnitId
		,bu.Name AS UnitName
		,dbo.GetUnitLocation(bu.Id, 1, 0, default) as UnitLocation
		,cu.ID as CurrentUnitId
		,cu.Name as CurrentUnitName
		,dbo.GetUnitLocation(cu.Id, 1, 0, default) as CurrentUnitLocation
	FROM dbo.Bands b
	LEFT JOIN dbo.BatteryTypes bt ON b.BatteryTypeId = bt.Id
	LEFT JOIN dbo.ev_BandStatus bs ON b.Id = bs.BandId
	LEFT JOIN dbo.ev_Events e ON bs.OngLowBatteryAlarmId = e.Id
	LEFT JOIN dbo.Units bu ON bu.Id = b.UnitId
	LEFT JOIN dbo.Units cu ON cu.Id = bs.CurrentUnitId
	LEFT JOIN dbo.enum_EventSeverityDictionaryPl es ON COALESCE(e.Severity, CASE WHEN bs.OngLowBatteryEventId IS NOT NULL THEN 1 ELSE NULL END) = es.Value
	LEFT JOIN dbo.enum_ObjectTypeDictionaryPl ot ON bs.ObjectType = ot.Value
	WHERE b.Id > 0