﻿

CREATE VIEW [dbo].[m2m_LoggedUsers]
AS
SELECT DISTINCT lu.ID, lu.ClientID, 
	SUBSTRING(
		(
			SELECT ', ' + lu1.Unit
			FROM dbo.m2m_forLoggedUsers lu1
			 Where lu.ID = lu1.ID
            ORDER BY lu.ID
            For XML PATH ('')

		), 2, 1000) Unit
		,lu.UserID, lu.Login, lu.Name, lu.LastName, lu.LoggingDate, lu.LastPingDate, lu.Monitoring, lu.Phone
FROM dbo.m2m_forLoggedUsers lu

