﻿CREATE VIEW ExternalUnitMap
	AS 
	SELECT eu.ExtId AS ExtId
		,eu.ExtDescription AS ExtDescription
		,eu.M2mId AS M2mId
		,dbo.GetUnitLocation(eu.M2mId, 1, 1, default) AS M2mDescription
	FROM [arch].[ext_Units] eu
	WHERE 1 = 1