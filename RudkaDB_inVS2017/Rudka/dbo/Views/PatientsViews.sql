﻿






CREATE VIEW [dbo].[PatientsViews] AS
	SELECT p.Id as Id
		,p.Name as Name
		,p.LastName as LastName
		,dbo.GetDisplayName(p.Id, objectType.Patient) as DisplayName
		,p.Pesel as Pesel
		,p.WardId as WardId
		,pu.Name as WardUnitName
		,dbo.GetUnitLocation(p.WardId, default, default, default) as WardUnitLocation
		,p.WardDateUtc as WardAdmissionDateUtc
		,p.RoomId as RoomId
		,pr.Name as RoomUnitName
		,dbo.GetUnitLocation(p.RoomId, default, default, default) as RoomUnitLocation
		,p.RoomDateUtc as RoomAdmissionDateUtc
		,b.Id as BandId
		,b.BarCode as BandBarCode
		,b.SerialNumber as BandSerialNumber
		,cu.Name as CurrentUnitName
		,dbo.GetUnitLocation(cu.ID, default, default, default) as CurrentUnitLocation
	FROM dbo.Patients p
	JOIN enum.ObjectType objectType ON 1 = 1
	LEFT JOIN dbo.Units pu ON p.WardId = pu.ID
	LEFT JOIN dbo.Units pr ON p.RoomId = pr.ID
	LEFT JOIN dbo.ev_BandStatus bs ON bs.ObjectId = p.Id AND bs.ObjectType = objectType.Patient
	LEFT JOIN dbo.Bands b ON b.Id = bs.BandId
	LEFT JOIN dbo.Units cu ON cu.ID = bs.CurrentUnitId