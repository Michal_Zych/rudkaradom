﻿

CREATE VIEW [dbo].[ServiceStatus] AS
	SELECT ServiceName AS ServiceName
      ,Description AS Description
      ,StartTimeUtc AS StartTimeUtc
      ,PingTimeUtc AS PingTimeUtc
      ,PreviousPingTimeUtc AS PreviousPingTimeUtc
      ,HasPartner AS HasPartner
      ,PartnerPingTimeUtc AS PartnerPingTimeUtc
      ,InfoTxt AS InfoTxt
	FROM dbo.Services s
	WHERE 1 = 1