﻿


CREATE VIEW [dbo].[sync_RecordsOUT_new]
AS
	SELECT Id AS Id
		,EventId		AS EventId
		,DateUtc		AS DateUtc
		,RecType		AS RecordType
		,Record			AS Record
		,ParentId		AS ParentId
		,Try			AS Try
		,TryAgain		AS TryAgain
		,SentDateUtc	AS SentDateUtc
		,Error			AS Error
	FROM tech.sync_RecordsOUT