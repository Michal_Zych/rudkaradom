﻿






CREATE VIEW [dbo].[TransmittersDetails] AS
SELECT bs.Id AS Id
	,bs.Mac AS Mac
	,bs.MacString AS MacString
	,bs.ClientId AS ClientId
	,u.ID AS InstallationUnitId
	,u.Name AS InstallationUnitName
	,bs.Name AS Name
	,bs.BarCode AS BarCode
	,bs.TypeId AS TypeId
	,bs.ReportIntervalSec AS ReportIntervalSec
	,bs.Sensitivity AS Sensitivity
	,bs.AvgCalcTimeS AS AvgCalcTimeS
	,bs.Msisdn AS Msisdn
	,bs.IsBandUpdater AS IsBandUpdater
	,bs.[Description] AS [Description]
	,bs.RssiTreshold AS RssiTreshold
	,bs.[Group] AS [GroupId]
	,tg.Description AS GroupName
	,bs.IpAddress AS IpAddress
	,CONVERT(bit, COALESCE(ts.OngConnectedEventId, 0)) AS IsConnected
	,CONVERT(bit, COALESCE(ts.OngDisconnectedAlarmId,0)) AS AlarmOngoing
FROM dbo.dev_Transmitters bs
LEFT JOIN dev_UnitTransmitter AS ut on bs.Id = ut.TransmitterId
LEFT JOIN Units AS u on ut.UnitID = u.ID
LEFT JOIN dbo.dev_TransmitterGroups tg ON tg.[Group] = bs.[Group]
LEFT JOIN dbo.ev_TransmitterStatus ts ON ts.TransmitterId = bs.Id