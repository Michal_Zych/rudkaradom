﻿



CREATE  view [dbo].[BandTest] as
	SELECT b.Id as Id
		,b.BarCode as BarCode
		,b.MacString as MacString
		,b.Uid as Uid
		,bt.Name as BatteryTypeStr
		,COALESCE(e.Severity, CASE WHEN bs.OngLowBatteryEventId IS NOT NULL THEN 1 ELSE NULL END) as BatteryAlarmSeverityIds
		,es.Description as BatteryAlarmSeverity
		,b.BatteryInstallationDateUtc as BatteryInstallationDate
		,DATEDIFF(DAY, dbo.GetCurrentUtcDate(), DATEADD(MONTH, bt.LifeMonths, b.BatteryInstallationDateUtc)) as BatteryDaysToExchange
		,b.Description as Description
		,bs.ObjectType as ObjectTypeIds
		,ot.Description as ObjectType
		,dbo.GetDisplayName(bs.ObjectId, bs.ObjectType) as ObjectDisplayName
		,ou.ID as ObjectUnitIds
		,ou.Name as ObjectUnitName
		,dbo.GetUnitLocation(ou.Id, default, default, default) as ObjectUnitLocation
		,cu.ID as CurrentUnitIds
		,cu.Name as CurrentUnitName
		,dbo.GetUnitLocation(cu.Id, default, default, default) as CurrentUnitLocation
	FROM dbo.Bands b
	LEFT JOIN dbo.BatteryTypes bt ON b.BatteryTypeId = bt.Id
	LEFT JOIN dbo.ev_BandStatus bs ON b.Id = bs.BandId
	LEFT JOIN dbo.ev_Events e ON bs.OngLowBatteryAlarmId = e.Id
	LEFT JOIN dbo.Units ou ON ou.Id = bs.ObjectUnitId
	LEFT JOIN dbo.Units cu ON cu.Id = bs.CurrentUnitId
	LEFT JOIN dbo.enum_EventSeverityDictionaryPl es ON COALESCE(e.Severity, CASE WHEN bs.OngLowBatteryEventId IS NOT NULL THEN 1 ELSE NULL END) = es.Value
	LEFT JOIN dbo.enum_ObjectTypeDictionaryPl ot ON bs.ObjectType = ot.Value
	WHERE b.Id > 0