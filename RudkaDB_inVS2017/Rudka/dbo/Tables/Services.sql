﻿CREATE TABLE [dbo].[Services] (
    [ServiceName]         NVARCHAR (200) NOT NULL,
    [Description]         NVARCHAR (512) NULL,
    [StartTimeUtc]        DATETIME       NULL,
    [PingTimeUtc]         DATETIME       NULL,
    [HasPartner]          BIT            NULL,
    [PartnerPingTimeUtc]  DATETIME       NULL,
    [InfoTxt]             NVARCHAR (MAX) NULL,
    [PreviousPingTimeUtc] DATETIME       NULL,
    [PreviousInfoTxt]     NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([ServiceName] ASC)
);



