﻿CREATE TABLE [dbo].[ev_SystemEventTypes] (
    [EventTypeId] TINYINT NOT NULL,
    CONSTRAINT [PK_ev_SystemEventTypes] PRIMARY KEY CLUSTERED ([EventTypeId] ASC)
);

