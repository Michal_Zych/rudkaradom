﻿CREATE TABLE [dbo].[Alarms] (
    [ID]            INT             IDENTITY (1, 1) NOT NULL,
    [SensorID]      SMALLINT        NOT NULL,
    [Timestamp]     DATETIME        NOT NULL,
    [EventType]     TINYINT         NOT NULL,
    [Status]        BIT             CONSTRAINT [DF_Alarms_Status] DEFAULT ((0)) NOT NULL,
    [UserStatus]    INT             NULL,
    [DateStatus]    DATETIME        NULL,
    [CommentStatus] NVARCHAR (1024) NULL,
    [LoRange]       SMALLINT        NOT NULL,
    [UpRange]       SMALLINT        NOT NULL,
    [DateEnd]       DATETIME        NULL,
    CONSTRAINT [PK_Alarms] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_SensorID_Inc_IDTimestamp_status]
    ON [dbo].[Alarms]([SensorID] ASC)
    INCLUDE([ID], [Timestamp], [Status]);

