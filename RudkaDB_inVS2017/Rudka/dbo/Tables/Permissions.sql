﻿CREATE TABLE [dbo].[Permissions] (
    [ID]            INT           NOT NULL,
    [Name]          NVARCHAR (64) NOT NULL,
    [ApplicationID] INT           NOT NULL,
    CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Permissions_Applications] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Applications] ([ID])
);

