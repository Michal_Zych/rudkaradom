﻿CREATE TABLE [dbo].[ActivityTypes] (
    [Id]   INT            IDENTITY (1, 1) NOT NULL,
    [Name] NVARCHAR (400) NOT NULL,
    CONSTRAINT [PK_ActivityTypes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

