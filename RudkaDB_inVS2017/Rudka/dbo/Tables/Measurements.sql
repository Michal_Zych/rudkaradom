﻿CREATE TABLE [dbo].[Measurements] (
    [SensorID]        SMALLINT NOT NULL,
    [MeasurementTime] DATETIME NOT NULL,
    [Value]           SMALLINT NULL,
    [StoreDelay]      INT      NOT NULL,
    [StoreTime]       AS       (dateadd(second,[StoreDelay],dateadd(millisecond, -datepart(millisecond,[MeasurementTime]),[MeasurementTime]))),
    CONSTRAINT [PK_Measurements] PRIMARY KEY CLUSTERED ([SensorID] ASC, [MeasurementTime] ASC)
);

