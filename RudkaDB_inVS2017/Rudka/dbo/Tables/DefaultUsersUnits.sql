﻿CREATE TABLE [dbo].[DefaultUsersUnits] (
    [UserID] INT NOT NULL,
    [UnitID] INT NOT NULL,
    CONSTRAINT [PK_DefaultUsersUnits] PRIMARY KEY CLUSTERED ([UserID] ASC, [UnitID] ASC)
);

