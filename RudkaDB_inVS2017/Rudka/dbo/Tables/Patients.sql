﻿CREATE TABLE [dbo].[Patients] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [Name]            NVARCHAR (100) NOT NULL,
    [LastName]        NVARCHAR (100) NOT NULL,
    [Pesel]           CHAR (11)      NULL,
    [HospitalDateUtc] DATETIME       NOT NULL,
    [WardId]          INT            NULL,
    [WardDateUtc]     DATETIME       NULL,
    [RoomId]          INT            NULL,
    [RoomDateUtc]     DATETIME       NULL,
    [BandId]          SMALLINT       NULL,
    [BandDateUtc]     DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);






