﻿CREATE TABLE [dbo].[Roles] (
    [ID]            INT           NOT NULL,
    [Name]          NVARCHAR (64) NOT NULL,
    [ApplicationID] INT           NOT NULL,
    [ParentRoleID]  INT           NULL,
    CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Roles_Applications] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Applications] ([ID])
);

