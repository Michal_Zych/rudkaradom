﻿CREATE TABLE [dbo].[dev_TransmitterGroups] (
    [Group]       TINYINT         NOT NULL,
    [Description] NVARCHAR (1024) NULL,
    CONSTRAINT [PK_dev_TransmitterGroups] PRIMARY KEY CLUSTERED ([Group] ASC)
);

