﻿CREATE TABLE [dbo].[AlarmsOmittedRanges] (
    [SensorID]  SMALLINT NOT NULL,
    [DateStart] DATETIME NOT NULL,
    [DateEnd]   DATETIME NOT NULL,
    CONSTRAINT [PK_AlarmsOmittedRanges_1] PRIMARY KEY CLUSTERED ([SensorID] ASC, [DateStart] ASC)
);

