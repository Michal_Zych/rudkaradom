﻿CREATE TABLE [dbo].[_Log] (
    [Id]            BIGINT         IDENTITY (1, 1) NOT NULL,
    [LogDate]       DATETIME       CONSTRAINT [DF__Log_LogDate] DEFAULT (getdate()) NOT NULL,
    [ProcedureName] NVARCHAR (400) NULL,
    [Info]          NVARCHAR (MAX) NULL,
    [ErrorLine]     INT            NULL,
    [ErrorMessage]  NVARCHAR (MAX) NULL,
    CONSTRAINT [PK__Log] PRIMARY KEY CLUSTERED ([Id] ASC)
);

