﻿CREATE TABLE [dbo].[SensorTests] (
    [Id]          INT      IDENTITY (1, 1) NOT NULL,
    [SensorId]    INT      NOT NULL,
    [Value]       SMALLINT NULL,
    [EventType]   SMALLINT CONSTRAINT [DF_SensorTests_EventType] DEFAULT ((0)) NOT NULL,
    [Active]      BIT      DEFAULT ((0)) NOT NULL,
    [DateStarted] DATETIME NOT NULL,
    [DateEnded]   DATETIME NULL,
    CONSTRAINT [PK_SensorTests] PRIMARY KEY CLUSTERED ([Id] ASC)
);

