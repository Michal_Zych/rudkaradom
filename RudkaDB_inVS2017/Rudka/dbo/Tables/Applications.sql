﻿CREATE TABLE [dbo].[Applications] (
    [ID]     INT            NOT NULL,
    [Name]   VARCHAR (64)   NOT NULL,
    [Charts] NVARCHAR (128) NULL,
    CONSTRAINT [PK_Applications] PRIMARY KEY CLUSTERED ([ID] ASC)
);

