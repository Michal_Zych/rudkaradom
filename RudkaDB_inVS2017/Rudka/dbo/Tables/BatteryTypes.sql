﻿CREATE TABLE [dbo].[BatteryTypes]
(
	[Id] TINYINT identity (1,1) NOT NULL PRIMARY KEY, 
    [Name] NVARCHAR(100) NOT NULL, 
    [LifeMonths] SMALLINT NOT NULL, 
    [Capacity] INT NOT NULL, 
    [Voltage] FLOAT NOT NULL, 
    [Description] NVARCHAR(MAX) NULL

)
