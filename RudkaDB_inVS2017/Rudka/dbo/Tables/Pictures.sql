﻿CREATE TABLE [dbo].[Pictures] (
    [ID]       INT             IDENTITY (1, 1) NOT NULL,
    [Name]     NVARCHAR (128)  NULL,
    [Path]     NVARCHAR (1024) NOT NULL,
    [ClientID] INT             NULL,
    CONSTRAINT [PK_Pictures] PRIMARY KEY CLUSTERED ([ID] ASC)
);

