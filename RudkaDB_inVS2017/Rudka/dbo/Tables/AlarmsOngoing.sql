﻿CREATE TABLE [dbo].[AlarmsOngoing] (
    [SensorID]               SMALLINT NOT NULL,
    [MeasureAlID]            INT      NULL,
    [MeasureAlTime]          DATETIME NULL,
    [MeasureAlType]          TINYINT  NULL,
    [CommAlID]               INT      NULL,
    [CommAlTime]             DATETIME NULL,
    [MeasureAlLastSmsTime]   DATETIME NULL,
    [MeasureAlLastEmailTime] DATETIME NULL,
    [CommAlLastSmsTime]      DATETIME NULL,
    [CommAlLastEmailTime]    DATETIME NULL,
    CONSTRAINT [PK_AlarmsOngoing] PRIMARY KEY CLUSTERED ([SensorID] ASC)
);

