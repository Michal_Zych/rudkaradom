﻿CREATE TABLE [dbo].[PreviewsUnits] (
    [PreviewedUnitID] INT  NOT NULL,
    [UnitID]          INT  NOT NULL,
    [TLX]             REAL NOT NULL,
    [TLY]             REAL NOT NULL,
    [BRX]             REAL NOT NULL,
    [BRY]             REAL NOT NULL,
    CONSTRAINT [PK_PreviewsUnits] PRIMARY KEY CLUSTERED ([PreviewedUnitID] ASC, [UnitID] ASC)
);

