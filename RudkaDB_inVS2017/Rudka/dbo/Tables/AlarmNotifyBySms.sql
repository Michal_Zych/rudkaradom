﻿CREATE TABLE [dbo].[AlarmNotifyBySms] (
    [SensorId] SMALLINT NOT NULL,
    [UserId]   INT      NOT NULL,
    CONSTRAINT [PK_AlarmNotifyBySms] PRIMARY KEY CLUSTERED ([SensorId] ASC, [UserId] ASC)
);

