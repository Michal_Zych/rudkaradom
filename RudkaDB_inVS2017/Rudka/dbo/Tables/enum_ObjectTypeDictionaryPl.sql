﻿CREATE TABLE [dbo].[enum_ObjectTypeDictionaryPl] (
    [Value]       TINYINT        NOT NULL,
    [Description] NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_enum_ObjectTypeDictionaryPl] PRIMARY KEY CLUSTERED ([Value] ASC)
);

