﻿CREATE TABLE [dbo].[dev_SenseTypes] (
    [Id]          INT             IDENTITY (1, 1) NOT NULL,
    [SubCode]     NVARCHAR (50)   NOT NULL,
    [Description] NVARCHAR (1024) NULL,
    CONSTRAINT [PK_dev_SenseTypes] PRIMARY KEY CLUSTERED ([Id] ASC)
);







