﻿CREATE TABLE [dbo].[ev_CallOrders] (
    [EventId] INT             NOT NULL,
    [DateUtc] DATETIME        NOT NULL,
    [Message] NVARCHAR (1024) NOT NULL,
    PRIMARY KEY CLUSTERED ([EventId] ASC)
);

