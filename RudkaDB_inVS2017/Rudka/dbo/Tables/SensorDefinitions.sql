﻿CREATE TABLE [dbo].[SensorDefinitions] (
    [UnitTypeIdv2]   INT          NOT NULL,
    [Interval]       INT          NULL,
    [IntervalMin]    INT          NULL,
    [IntervalMax]    INT          NULL,
    [MU]             NVARCHAR (5) NULL,
    [LineColor]      VARCHAR (15) NULL,
    [LineWidth]      TINYINT      NULL,
    [RangeLineWidth] TINYINT      NULL,
    [LineStyle]      VARCHAR (50) NULL,
    [RangeLineStyle] VARCHAR (50) NULL,
    [AlarmColor]     VARCHAR (15) NULL,
    [AlarmCommColor] VARCHAR (15) NULL,
    [GuiRangeDelay]  SMALLINT     NULL,
    [GuiCommDelay]   SMALLINT     NULL,
    [SmsEnabled]     BIT          NULL,
    [SmsRangeDelay]  SMALLINT     NULL,
    [SmsCommDelay]   SMALLINT     NULL,
    [MailEnabled]    BIT          NULL,
    [MailRangeDelay] SMALLINT     NULL,
    [MailCommDelay]  SMALLINT     NULL,
    CONSTRAINT [PK_SensorDefinitions] PRIMARY KEY CLUSTERED ([UnitTypeIdv2] ASC)
);

