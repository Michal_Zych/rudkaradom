﻿CREATE TABLE [dbo].[MessagesLog] (
    [MessageID]  INT      NOT NULL,
    [ReceiverID] INT      NOT NULL,
    [Date]       DATETIME CONSTRAINT [DF_MessagesLog_Date] DEFAULT (getdate()) NOT NULL,
    [Count]      INT      CONSTRAINT [DF_MessagesLog_Count] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_MessagesLog] PRIMARY KEY CLUSTERED ([MessageID] ASC, [ReceiverID] ASC)
);

