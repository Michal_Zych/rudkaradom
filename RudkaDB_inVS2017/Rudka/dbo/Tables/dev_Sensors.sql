﻿CREATE TABLE [dbo].[dev_Sensors] (
    [Id]             INT      IDENTITY (1, 1) NOT NULL,
    [SenseId]        BIGINT   NOT NULL,
    [SensorNr]       SMALLINT NOT NULL,
    [IntervalS]      INT      NOT NULL,
    [DefCorrection]  REAL     CONSTRAINT [DF_dev_Sensors_DefCorrection] DEFAULT ((0)) NOT NULL,
    [IsHwCalibrated] BIT      CONSTRAINT [DF_dev_Sensors_IsHwCalibrated] DEFAULT ((0)) NOT NULL,
    [IsEvent]        BIT      CONSTRAINT [DF_dev_Sensors_IsEvent] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_dev_Sensors] PRIMARY KEY CLUSTERED ([Id] ASC)
);

