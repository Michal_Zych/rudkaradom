﻿CREATE TABLE [dbo].[Bands] (
    [Id]                         [dbo].[OBJECT_ID]       IDENTITY (1, 1) NOT NULL,
    [Mac]                        BIGINT         NOT NULL,
    [MacString]                  AS             ([dbo].[formatMAC](right(upper(substring([sys].[fn_varbintohexstr]([Mac]),(3),(16))),(12)))),
    [Uid]                        BIGINT         NULL,
    [BarCode]                   NVARCHAR (100) NULL,
	[SerialNumber]				 NVARCHAR(50)	NULL,
    [TransmissionPower]          SMALLINT       NOT NULL,
    [AccPercent]                 TINYINT        NOT NULL,
    [SensitivityPercent]         TINYINT        NOT NULL,
    [BroadcastingIntervalMs]     INT            NOT NULL,
    [BatteryTypeId]              TINYINT        NOT NULL,
    [BatteryInstallationDateUtc] DATETIME       DEFAULT (getutcdate()) NOT NULL,
    [UnitId]                     INT            NULL,
    [Description]                NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Bands] PRIMARY KEY CLUSTERED ([Id] ASC)
);












GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Bands_Mac]
    ON [dbo].[Bands]([Mac] ASC);

