﻿CREATE TABLE [dbo].[AlarmsEditedBySystem] (
    [ID]                  INT                   IDENTITY (1, 1) NOT NULL,
    [Timestamp]           DATETIME              NULL,
    [AlarmID]             INT                   NULL,
    [DateEndBefore]       DATETIME              NULL,
    [DateEndAfter]        DATETIME              NULL,
    [StatusBefore]        INT                   NULL,
    [StatusAfter]         INT                   NULL,
    [DateStatusBefore]    DATETIME              NULL,
    [DateStatusAfter]     DATETIME              NULL,
    [UserStatusBefore]    INT                   NULL,
    [UserStatusAfter]     INT                   NULL,
    [CommentStatusBefore] [dbo].[ALARM_COMMENT] NULL,
    [CommentStatusAfter]  [dbo].[ALARM_COMMENT] NULL,
    CONSTRAINT [PK_AlarmsEditedBySystem] PRIMARY KEY CLUSTERED ([ID] ASC)
);

