﻿CREATE TABLE [dbo].[dev_Transmitters] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [Mac]               BIGINT          NULL,
    [MacString]         AS              ([dbo].[formatMAC](right(upper(substring([sys].[fn_varbintohexstr]([Mac]),(3),(16))),(12)))),
    [ClientId]          INT             CONSTRAINT [DF_dev_Transmitters_ClientId] DEFAULT ((0)) NOT NULL,
    [Name]              NVARCHAR (100)  CONSTRAINT [DF_dev_Transmitters_Name] DEFAULT ('') NULL,
    [BarCode]           NVARCHAR (50)   NULL,
    [TypeId]            INT             CONSTRAINT [DF_dev_Transmitters_TypeId] DEFAULT ((1)) NOT NULL,
    [ReportIntervalSec] INT             NOT NULL,
    [Sensitivity]       INT             CONSTRAINT [DF_dev_Transmitters_Sensitivity] DEFAULT ((0)) NOT NULL,
    [AvgCalcTimeS]      INT             CONSTRAINT [DF_dev_Transmitters_AvgCalcTimeS] DEFAULT ((11)) NOT NULL,
    [SwitchDelayS]      INT             CONSTRAINT [DF_dev_Transmitters_SwitchDelayS] DEFAULT ((6)) NOT NULL,
    [Msisdn]            NVARCHAR (20)   NULL,
    [IsBandUpdater]     BIT             NULL,
    [Description]       NVARCHAR (2048) NULL,
    [RssiTreshold]      SMALLINT        NULL,
    [Group]             TINYINT         NULL,
    [IpAddress]         NVARCHAR (20)   NULL,
    [IsActive]          BIT             DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_dev_Transmitters] PRIMARY KEY CLUSTERED ([Id] ASC)
);













