﻿CREATE TABLE [dbo].[al_NoGoZones] (
    [Id]         INT     NOT NULL,
    [ObjectType] TINYINT NOT NULL,
    [UnitId]     INT     NOT NULL,
    CONSTRAINT [PK_al_NoGoZones] PRIMARY KEY CLUSTERED ([Id] ASC, [ObjectType] ASC, [UnitId] ASC)
);

