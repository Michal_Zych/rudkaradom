﻿CREATE TABLE [dbo].[_test_cases] (
    [Id]            INT             IDENTITY (1, 1) NOT NULL,
    [Scenario]      NVARCHAR (2048) NOT NULL,
    [RunDate]       DATETIME        NULL,
    [RunResult]     NVARCHAR (MAX)  NOT NULL,
    [LastRunDate]   DATETIME        NULL,
    [LastRunResult] NVARCHAR (MAX)  NULL,
    [IsLastRunOK]   BIT             NULL,
    CONSTRAINT [PK__test_cases] PRIMARY KEY CLUSTERED ([Id] ASC)
);

