﻿CREATE TABLE [dbo].[Units_old] (
    [ID]               INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (256) NOT NULL,
    [ParentUnitID]     INT            NULL,
    [UnitTypeID]       INT            CONSTRAINT [DF_Units_UnitTypeID_old] DEFAULT ((1)) NOT NULL,
    [Active]           BIT            CONSTRAINT [DF_Units_Active_old] DEFAULT ((1)) NULL,
    [ClientID]         INT            NOT NULL,
    [UnitTypeIdv2]     INT            NOT NULL,
    [Pictureid]        INT            NULL,
    [Description]      NVARCHAR (200) NULL,
    [UserID]           INT            NULL,
    [PreviewPictureID] INT            NULL,
    CONSTRAINT [PK_Units_old] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Units_ParentUnitId_Included_Id_old]
    ON [dbo].[Units_old]([ParentUnitID] ASC)
    INCLUDE([ID]);


GO
CREATE NONCLUSTERED INDEX [IX_ParentUnitID_old]
    ON [dbo].[Units_old]([ParentUnitID] ASC)
    INCLUDE([ID], [UnitTypeID]);

