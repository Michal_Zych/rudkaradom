﻿CREATE TABLE [dbo].[Scans] (
    [Id]       INT            IDENTITY (1, 1) NOT NULL,
    [FileName] NVARCHAR (256) NOT NULL,
    [CenterID] INT            NOT NULL,
    CONSTRAINT [PK_DevicesScans] PRIMARY KEY CLUSTERED ([Id] ASC)
);

