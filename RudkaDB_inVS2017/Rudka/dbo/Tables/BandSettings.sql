﻿CREATE TABLE [dbo].[BandSettings]
(
	[BandId] [dbo].[OBJECT_ID] NOT NULL , 
    [Key] NVARCHAR(50) NOT NULL, 
    [Description] NVARCHAR(1024) NOT NULL, 
    [Value] NVARCHAR(1024) NULL, 
    PRIMARY KEY ([BandId], [Key])
)
