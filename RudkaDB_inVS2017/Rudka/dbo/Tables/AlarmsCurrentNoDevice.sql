﻿CREATE TABLE [dbo].[AlarmsCurrentNoDevice] (
    [DeviceID]  INT      NOT NULL,
    [AlarmID]   INT      NULL,
    [Timestamp] DATETIME NULL
);

