﻿CREATE TABLE [dbo].[Devices] (
    [Id]              SMALLINT IDENTITY (1, 1) NOT NULL,
    [Address]         SMALLINT NOT NULL,
    [Timeout]         SMALLINT DEFAULT ((1000)) NOT NULL,
    [MaxChannelCount] SMALLINT NOT NULL,
    [DeviceModel]     SMALLINT NOT NULL,
    [HubId]           INT      NOT NULL,
    [Active]          BIT      DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Devices] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Devices_Hubs] FOREIGN KEY ([HubId]) REFERENCES [dbo].[Hubs] ([Id])
);

