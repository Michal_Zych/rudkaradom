﻿CREATE TABLE [dbo].[UsersReports] (
    [ID]              INT      IDENTITY (1, 1) NOT NULL,
    [UserID]          INT      NOT NULL,
    [ReportID]        INT      NOT NULL,
    [DateFrom]        DATETIME NULL,
    [DateTo]          DATETIME NULL,
    [UnitID]          INT      NOT NULL,
    [HideCharts]      BIT      NULL,
    [Mono]            BIT      NULL,
    [HyperPermission] BIT      NULL,
    CONSTRAINT [PK_UsersReports] PRIMARY KEY CLUSTERED ([ID] ASC)
);



