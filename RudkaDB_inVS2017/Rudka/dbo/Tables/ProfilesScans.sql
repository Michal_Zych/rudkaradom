﻿CREATE TABLE [dbo].[ProfilesScans] (
    [Id]       INT            IDENTITY (1, 1) NOT NULL,
    [FileName] NVARCHAR (256) NOT NULL,
    [ClientID] INT            NULL,
    CONSTRAINT [PK_ProfilesScans] PRIMARY KEY CLUSTERED ([Id] ASC)
);

