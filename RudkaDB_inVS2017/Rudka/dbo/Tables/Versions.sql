﻿CREATE TABLE [dbo].[Versions] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [Version]          NVARCHAR (10)  NOT NULL,
    [IssueDate]        DATETIME       NOT NULL,
    [InstallationDate] DATETIME       NOT NULL,
    [Description]      NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Versions] PRIMARY KEY CLUSTERED ([Id] ASC)
);

