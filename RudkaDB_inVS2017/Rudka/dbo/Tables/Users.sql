﻿CREATE TABLE [dbo].[Users] (
    [ID]                 INT             IDENTITY (1, 1) NOT NULL,
    [Login]              NVARCHAR (64)   NOT NULL,
    [Password]           NVARCHAR (64)   NOT NULL,
    [ClientID]           INT             NULL,
    [UnitID]             INT             NULL,
    [Name]               NVARCHAR (128)  NULL,
    [LastName]           NVARCHAR (128)  NULL,
    [Phone]              NVARCHAR (128)  NULL,
    [LastPingID]         INT             NULL,
    [Settings]           NVARCHAR (1024) NULL,
    [RfidUid]            BIGINT          NULL,
    [Active]             BIT             CONSTRAINT [DF__Users__Active__795DFB40] DEFAULT ((1)) NOT NULL,
    [PasswordChangeDate] DATETIME        NULL,
    [eMail]              NVARCHAR (100)  NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Users_Clients] FOREIGN KEY ([ClientID]) REFERENCES [dbo].[Clients] ([ID])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [PK_Users_LoginClient_Unique]
    ON [dbo].[Users]([Login] ASC, [ClientID] ASC) WITH (ALLOW_PAGE_LOCKS = OFF);

