﻿CREATE TABLE [dbo].[Hubs] (
    [Id]        INT           IDENTITY (1, 1) NOT NULL,
    [IPAddress] NVARCHAR (50) NOT NULL,
    [Port]      INT           NOT NULL,
    [Active]    BIT           DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Hubs] PRIMARY KEY CLUSTERED ([Id] ASC)
);

