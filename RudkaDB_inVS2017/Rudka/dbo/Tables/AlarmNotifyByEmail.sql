﻿CREATE TABLE [dbo].[AlarmNotifyByEmail] (
    [SensorId] SMALLINT NOT NULL,
    [UserId]   INT      NOT NULL,
    CONSTRAINT [PK_AlarmNotifyByEmail] PRIMARY KEY CLUSTERED ([SensorId] ASC, [UserId] ASC)
);

