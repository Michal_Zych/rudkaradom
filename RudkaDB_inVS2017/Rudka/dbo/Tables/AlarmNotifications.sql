﻿CREATE TABLE [dbo].[AlarmNotifications] (
    [ID]            INT             IDENTITY (1, 1) NOT NULL,
    [SensorID]      SMALLINT        NOT NULL,
    [AlarmID]       INT             NOT NULL,
    [SendAfterDate] DATETIME        NOT NULL,
    [SentDate]      DATETIME        NULL,
    [Type]          TINYINT         NOT NULL,
    [Message]       NVARCHAR (2000) NULL,
    [Addressees]    NVARCHAR (MAX)  NULL,
    [DeleteDate]    DATETIME        NULL,
    [Info]          NVARCHAR (MAX)  NULL,
    CONSTRAINT [PK_AlarmNotifications] PRIMARY KEY CLUSTERED ([ID] ASC)
);

