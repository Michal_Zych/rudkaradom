﻿CREATE TABLE [dbo].[ev_BandStatus] (
    [BandId]                 [dbo].[OBJECT_ID]                    NOT NULL,
    [ObjectType]             [dbo].[OBJECT_TYPE]    NOT NULL,
    [ObjectId]               [dbo].[OBJECT_ID]                    NOT NULL,
    [AssignDateUtc]          DATETIME               NOT NULL,
    [ObjectUnitId]           INT                    NULL,
    [CurrentUnitId]          INT                    NULL,
    [InCurrentUnitUtc]       DATETIME               NOT NULL,
    [PreviousUnitId]         INT                    NULL,
    [InPreviousUnitUtc]      DATETIME               NULL,
    [TopAlarmId]             INT                    NULL,
    [TopAlarmType]           [dbo].[EVENT_TYPE]     NULL,
    [TopAlarmSeverity]       [dbo].[EVENT_SEVERITY] NULL,
    [TopAlarmDateUtc]        DATETIME               NULL,
    [TopEventDateUtc]        DATETIME               NULL,
    [Severity1Count]         INT                    DEFAULT ((0)) NOT NULL,
    [Severity2Count]         INT                    DEFAULT ((0)) NOT NULL,
    [OngNoGoZoneInEventId]   INT                    NULL,
    [OngNoGoZoneInAlarmId]   INT                    NULL,
    [OngDisconnectedEventId] INT                    NULL,
    [OngDisconnectedAlarmId] INT                    NULL,
    [OngZoneOutEventId]      INT                    NULL,
    [OngZoneOutAlarmId]      INT                    NULL,
    [OngNoMovingEventId]     INT                    NULL,
    [OngNoMovingAlarmId]     INT                    NULL,
    [OngMovingEventId]       INT                    NULL,
    [OngMovingAlarmId]       INT                    NULL,
    [OngLowBatteryEventId]   INT                    NULL,
    [OngLowBatteryAlarmId]   INT                    NULL,
    [SynchroToken]           ROWVERSION             NULL,
    PRIMARY KEY CLUSTERED ([BandId] ASC)
);






