﻿CREATE TABLE [dbo].[Settings] (
    [ID]       INT             IDENTITY (1, 1) NOT NULL,
    [ClientID] INT             NOT NULL,
    [Name]     NVARCHAR (50)   NOT NULL,
    [Value]    NVARCHAR (1024) NULL,
    CONSTRAINT [PK_Settings] PRIMARY KEY CLUSTERED ([ID] ASC)
);

