﻿CREATE TABLE [dbo].[UnitTypesV2] (
    [ID]          INT            IDENTITY (0, 1) NOT NULL,
    [Description] NVARCHAR (100) NOT NULL,
    [Picture]     NVARCHAR (50)  NOT NULL,
    [IsSensor]    BIT            NOT NULL,
    CONSTRAINT [PK_UnitTypesV2] PRIMARY KEY CLUSTERED ([ID] ASC)
);

