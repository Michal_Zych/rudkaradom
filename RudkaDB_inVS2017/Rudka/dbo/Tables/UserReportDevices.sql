﻿CREATE TABLE [dbo].[UserReportDevices] (
    [UserReportID] INT NOT NULL,
    [DeviceID]     INT NOT NULL,
    CONSTRAINT [PK_UserReportDevices] PRIMARY KEY CLUSTERED ([UserReportID] ASC, [DeviceID] ASC)
);

