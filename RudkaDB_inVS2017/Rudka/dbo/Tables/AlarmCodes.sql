﻿CREATE TABLE [dbo].[AlarmCodes] (
    [Id]          TINYINT        NOT NULL,
    [Description] NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_AlarmCodes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

