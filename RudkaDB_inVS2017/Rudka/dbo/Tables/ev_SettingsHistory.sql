﻿CREATE TABLE [dbo].[ev_SettingsHistory] (
    [Id]                            INT            IDENTITY (1, 1) NOT NULL,
    [StartDateUtc]                  DATETIME       DEFAULT (getutcdate()) NOT NULL,
    [UserId]                        INT            DEFAULT ((0)) NOT NULL,
    [NoConnectionFinishingAlarms]   BIT            DEFAULT ((1)) NOT NULL,
    [FullMonitoringUnassignedBands] BIT            DEFAULT ((0)) NOT NULL,
    [WarningAutoCloseMessage]       NVARCHAR (200) DEFAULT ('Zamknięte przez alarm.') NOT NULL,
    [LoBatteryWrActive]             BIT            DEFAULT ((1)) NOT NULL,
    [LoBatteryWrMins]               SMALLINT       DEFAULT ((0)) NOT NULL,
    [LoBatteryAlActive]             BIT            DEFAULT ((1)) NOT NULL,
    [LoBatteryAlMins]               SMALLINT       DEFAULT ((1440)) NOT NULL,
    [BatteryTermWrDaysBefore]       SMALLINT       DEFAULT ((14)) NULL,
    [BatteryTermWrMessage]          NVARCHAR (200) DEFAULT ('Za dwa tygodnie upływa termin wymiany baterii.') NOT NULL,
    [BatteryTermAlDaysBefore]       SMALLINT       DEFAULT ((-1)) NULL,
    [BatteryTermAlMessage]          NVARCHAR (200) DEFAULT ('Upłynął termin wymiany baterii') NOT NULL,
    [DayStartHour]                  DATETIME       DEFAULT ('9:00') NOT NULL,
    [DayEndHour]                    DATETIME       DEFAULT ('22:00') NOT NULL,
    [NoTransmitterWrActive]         BIT            DEFAULT ((0)) NOT NULL,
    [NoTransmitterWrMins]           SMALLINT       DEFAULT ((10)) NOT NULL,
    [NoTransmitterAlActive]         BIT            DEFAULT ((0)) NOT NULL,
    [NoTransmitterAlMins]           SMALLINT       DEFAULT ((20)) NOT NULL,
    [FirstBandAssignmentReason]     NVARCHAR (200) DEFAULT ('Nowa opaska.') NOT NULL,
    [EventToArchiveAfterDays]       TINYINT        DEFAULT ((31)) NOT NULL,
    [_LocalAdminPhone]              NVARCHAR (50)  DEFAULT ('00000000') NOT NULL,
    [MaxPasswordAgeDays]            INT            DEFAULT ((30)) NOT NULL,
    [ShowReasonField]               BIT            DEFAULT ((1)) NOT NULL,
    [PingSessionIntervalSecs]       INT            DEFAULT ((20)) NOT NULL,
    [MonitoringSoundIntervalSecs]   INT            DEFAULT ((30)) NOT NULL,
    [RangeColorIntensityPercent]    INT            DEFAULT ((50)) NOT NULL,
    [MaxRwosLogs]                   INT            DEFAULT ((500)) NOT NULL,
    [MaxRwosMessages]               INT            DEFAULT ((200)) NOT NULL,
    [DefMessageActivityMinutes]     INT            DEFAULT ((30)) NOT NULL,
    [MaxRowsAlarmsPopup]            INT            DEFAULT ((200)) NOT NULL,
    [MaxRowsAlarms]                 INT            DEFAULT ((200)) NOT NULL,
    [MaxRowsRawData]                INT            DEFAULT ((1000)) NOT NULL,
    [RoleDefForNewUser]             INT            DEFAULT ((3)) NOT NULL,
    [UnitsLocationSeparator]        NVARCHAR (50)  DEFAULT (' > ') NOT NULL,
    [MaxDaysReports]                INT            DEFAULT ((33)) NOT NULL,
    [MaxDaysRawData]                INT            DEFAULT ((5)) NOT NULL,
    [SearchMessagesDaysBefore]      INT            DEFAULT ((2)) NOT NULL,
    [SearchMessagesDaysAfter]       INT            DEFAULT ((1)) NOT NULL,
    [SearchLogsDaysBefore]          INT            DEFAULT ((2)) NOT NULL,
    [SearchLogsDaysAfter]           INT            DEFAULT ((1)) NOT NULL,
    [SearchChartDataHoursBefore]    INT            DEFAULT ((24)) NOT NULL,
    [SearchChartDataHoursAfter]     INT            DEFAULT ((24)) NOT NULL,
    [ChartImageWidth]               INT            DEFAULT ((800)) NOT NULL,
    [ChartImageHeight]              INT            DEFAULT ((600)) NOT NULL,
    [SearchRawDataDaysBefore]       INT            DEFAULT ((1)) NOT NULL,
    [SearchRawDataDaysAfter]        INT            DEFAULT ((1)) NOT NULL,
    [SearchAlarmsDaysBefore]        INT            DEFAULT ((2)) NOT NULL,
    [SearchAlarmsDaysAfter]         INT            DEFAULT ((1)) NOT NULL,
    [ShowUnitId]                    BIT            DEFAULT ((1)) NOT NULL,
    [EnableMonitoringStatusButton]  BIT            DEFAULT ((1)) NOT NULL,
    [OldDataTimeMinutes]            INT            DEFAULT ((10)) NOT NULL,
    [OldDataColor]                  NVARCHAR (50)  DEFAULT ('255,255,150,35') NOT NULL,
    [SqlLoggingIsOn]                BIT            NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);










