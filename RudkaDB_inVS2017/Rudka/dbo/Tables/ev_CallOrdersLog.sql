﻿CREATE TABLE [dbo].[ev_CallOrdersLog] (
    [EventId] INT          NOT NULL,
    [Number]  VARCHAR (50) NULL,
    [Type]    INT          NOT NULL,
    [Status]  INT          NOT NULL,
    [Time]    DATETIME     NOT NULL
);

