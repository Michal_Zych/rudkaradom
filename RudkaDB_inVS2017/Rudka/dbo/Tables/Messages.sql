﻿CREATE TABLE [dbo].[Messages] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [ClientId]   INT            NOT NULL,
    [Message]    NVARCHAR (MAX) NOT NULL,
    [ActiveFrom] DATETIME       NOT NULL,
    [ActiveTo]   DATETIME       NOT NULL,
    [Mode]       TINYINT        CONSTRAINT [DF_Messages_Mode] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Messages] PRIMARY KEY CLUSTERED ([Id] ASC)
);

