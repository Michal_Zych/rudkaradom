﻿CREATE TABLE [dbo].[Sockets] (
    [Id]                  INT           IDENTITY (1, 1) NOT NULL,
    [Name]                VARCHAR (100) NOT NULL,
    [LoRange]             SMALLINT      NOT NULL,
    [HiRange]             SMALLINT      NOT NULL,
    [LoRangeH]            SMALLINT      NOT NULL,
    [HiRangeH]            SMALLINT      NOT NULL,
    [DeviceId]            INT           NULL,
    [DeviceChannelNumber] SMALLINT      NULL,
    [Broken]              BIT           DEFAULT ((0)) NOT NULL,
    [SenseSensorId]       INT           NULL,
    CONSTRAINT [PK_Sockets] PRIMARY KEY CLUSTERED ([Id] ASC)
);



