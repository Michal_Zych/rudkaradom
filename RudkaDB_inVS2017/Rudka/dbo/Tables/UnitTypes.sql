﻿CREATE TABLE [dbo].[UnitTypes] (
    [ID]   INT            NOT NULL,
    [Name] NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_UnitTypes] PRIMARY KEY CLUSTERED ([ID] ASC)
);

