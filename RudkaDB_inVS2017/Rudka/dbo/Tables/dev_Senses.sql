﻿CREATE TABLE [dbo].[dev_Senses] (
    [Id]        BIGINT        NOT NULL,
    [MacString] AS            ([dbo].[formatMAC](right(upper(substring([sys].[fn_varbintohexstr]([Id]),(3),(16))),(12)))),
    [ClientId]  INT           CONSTRAINT [DF_dev_Senses_ClientId] DEFAULT ((0)) NOT NULL,
    [Name]      NVARCHAR (16) CONSTRAINT [DF_dev_Senses_Name] DEFAULT ('') NULL,
    [BarCode]   NVARCHAR (50) NULL,
    [TypeId]    INT           CONSTRAINT [DF_dev_Senses_TypeId] DEFAULT ((1)) NOT NULL,
    [LayerName] NVARCHAR (50) NULL,
    CONSTRAINT [PK_dev_Senses] PRIMARY KEY CLUSTERED ([Id] ASC)
);



