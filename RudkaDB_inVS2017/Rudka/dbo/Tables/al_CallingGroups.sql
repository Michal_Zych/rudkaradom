﻿CREATE TABLE [dbo].[al_CallingGroups] (
    [Id]          INT            NOT NULL,
    [ObjectType]  TINYINT        NOT NULL,
    [SequenceNr] TINYINT NOT NULL, 
	[Phone]       VARCHAR(20)  NOT NULL,
    [Description] NVARCHAR (200) NULL
);

