﻿CREATE TABLE [dbo].[ReportTypeEvents] (
    [TypeId]      TINYINT NOT NULL,
    [EventTypeId] TINYINT NOT NULL,
    CONSTRAINT [PK_ReportTypeEvents] PRIMARY KEY CLUSTERED ([TypeId] ASC, [EventTypeId] ASC)
);

