﻿CREATE TABLE [dbo].[ev_EventsToRaise] (
    [Id]                  INT                 IDENTITY (1, 1) NOT NULL,
    [RaisingObjectType]   [dbo].[OBJECT_TYPE] NOT NULL,
    [RaisingObjectId]     SMALLINT            NULL,
    [RaisingEventId]      INT                 NOT NULL,
    [RaisingEventDateUtc] DATETIME            NOT NULL,
    [RaiseDateUtc]        DATETIME            NOT NULL,
    [Type]                TINYINT             NOT NULL,
    [Severity]            TINYINT             NOT NULL,
    [UnitId]              INT                 NULL,
    [ObjectId]            INT                 NOT NULL,
    [ObjectType]          TINYINT             NOT NULL,
    [ObjectUnitId]        INT                 NULL,
    CONSTRAINT [PK_ev_EventsToRaise] PRIMARY KEY CLUSTERED ([Id] ASC)
);

