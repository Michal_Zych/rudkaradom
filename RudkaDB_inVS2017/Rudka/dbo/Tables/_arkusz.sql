﻿CREATE TABLE [dbo].[_arkusz] (
    [Id]          FLOAT (53)     NULL,
    [Nazwa]       NVARCHAR (255) NULL,
    [Location]    NVARCHAR (255) NULL,
    [Scale]       FLOAT (53)     NULL,
    [Calibration] FLOAT (53)     NULL,
    [DxE]         FLOAT (53)     NULL,
    [Korekta 0]   FLOAT (53)     NULL
);

