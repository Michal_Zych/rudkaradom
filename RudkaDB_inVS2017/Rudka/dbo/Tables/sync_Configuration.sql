﻿CREATE TABLE [dbo].[sync_Configuration]
(
	[Key] nvarchar(50) NOT NULL PRIMARY KEY,
	UserName nvarchar(100) NOT NULL,
	Password nvarchar(100) NOT NULL,
	ConnectionString nvarchar(512) NOT NULL,
	TopicName nvarchar(512) NOT NULL,
	EscapeValue nvarchar(20) NOT NULL,
	EscapeValueLine nvarchar(20) NOT NULL,
	CheckCmdsIntervalMilis nvarchar(20) NOT NULL,
	NewQueryLimit nvarchar(10) NOT NULL,
	WebServiceUserName nvarchar(100) NOT NULL,
	WebServicePassword nvarchar(100) NOT NULL,
	WebServiceURL nvarchar(512) NOT NULL,
	ActiveMQConnectionId nvarchar(20) NOT NULL,
	KeepAliveIntervalS nvarchar(10) NOT NULL
)
