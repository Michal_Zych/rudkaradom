﻿CREATE TABLE [dbo].[Clients] (
    [ID]            INT            NOT NULL,
    [Name]          VARCHAR (64)   NOT NULL,
    [ApplicationID] INT            NOT NULL,
    [HostName]      NVARCHAR (128) NULL,
    [DefaultUserID] INT            NULL,
    [RootUnitID]    INT            NULL,
	[RegistrationWardId] INT		NULL,
	[UnassignedPatientsUnitId] INT NULL,
    CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_Clients_Applications] FOREIGN KEY ([ApplicationID]) REFERENCES [dbo].[Applications] ([ID]),
    CONSTRAINT [FK_Clients_Users] FOREIGN KEY ([DefaultUserID]) REFERENCES [dbo].[Users] ([ID]) ON DELETE SET NULL ON UPDATE CASCADE
);

