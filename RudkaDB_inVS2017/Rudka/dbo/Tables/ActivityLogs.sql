﻿CREATE TABLE [dbo].[ActivityLogs] (
    [ID]            INT             IDENTITY (1, 1) NOT NULL,
    [UserID]        INT             NOT NULL,
    [ActivityType]  INT             NOT NULL,
    [Timestamp]     DATETIME        CONSTRAINT [DF_ActivityLogs_Timestamp] DEFAULT (getdate()) NOT NULL,
    [PreviousState] NVARCHAR (1024) NOT NULL,
    CONSTRAINT [PK_ActivityLogs] PRIMARY KEY CLUSTERED ([ID] ASC)
);

