﻿CREATE TABLE [dbo].[dev_TransmitterTransitions] (
    [TransmitterId] INT     NOT NULL,
    [Group]         TINYINT NOT NULL,
    CONSTRAINT [PK_dev_TransmitterTransitions] PRIMARY KEY CLUSTERED ([TransmitterId] ASC, [Group] ASC)
);

