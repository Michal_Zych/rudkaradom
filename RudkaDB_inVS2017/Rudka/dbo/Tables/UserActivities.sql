﻿CREATE TABLE [dbo].[UserActivities] (
    [Id]         INT      IDENTITY (1, 1) NOT NULL,
    [ActivityId] INT      NOT NULL,
    [UserId]     INT      NOT NULL,
    [Timestamp]  DATETIME NOT NULL,
    CONSTRAINT [PK_UserActivities] PRIMARY KEY CLUSTERED ([Id] DESC),
    CONSTRAINT [FK_UserActivities_ActivityTypes] FOREIGN KEY ([ActivityId]) REFERENCES [dbo].[ActivityTypes] ([Id]),
    CONSTRAINT [FK_UserActivities_Users] FOREIGN KEY ([UserId]) REFERENCES [dbo].[Users] ([ID])
);

