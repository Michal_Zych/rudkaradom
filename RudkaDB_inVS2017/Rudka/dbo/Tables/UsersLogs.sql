﻿CREATE TABLE [dbo].[UsersLogs] (
    [Id]           INT          IDENTITY (1, 1) NOT NULL,
    [UserID]       INT          NOT NULL,
    [LoggingDate]  DATETIME     CONSTRAINT [DF_UsersLogs_LoggingDate] DEFAULT (getdate()) NOT NULL,
    [LastPingDate] DATETIME     CONSTRAINT [DF_UsersLogs_LastPingDate] DEFAULT (getdate()) NOT NULL,
    [Monitoring]   BIT          NULL,
    [Phone]        VARCHAR (32) NULL,
    CONSTRAINT [PK_UsersLogs] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UsersLogs_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
);

