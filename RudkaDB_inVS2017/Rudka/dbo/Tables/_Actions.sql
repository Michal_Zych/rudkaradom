﻿CREATE TABLE [dbo].[_Actions] (
    [id]     INT            IDENTITY (1, 1) NOT NULL,
    [Date]   DATETIME       CONSTRAINT [DF__Actions_Date] DEFAULT (getdate()) NULL,
    [Action] NVARCHAR (MAX) NULL
);

