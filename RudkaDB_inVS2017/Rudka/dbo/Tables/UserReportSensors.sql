﻿CREATE TABLE [dbo].[UserReportSensors] (
    [UserReportID] INT      NOT NULL,
    [SensorID]     SMALLINT NOT NULL,
    [AlarmsCount]  INT      NOT NULL,
    CONSTRAINT [PK_UserReportSensors] PRIMARY KEY CLUSTERED ([UserReportID] ASC, [SensorID] ASC)
);

