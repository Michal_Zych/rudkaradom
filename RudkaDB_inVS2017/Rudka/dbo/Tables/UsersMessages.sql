﻿CREATE TABLE [dbo].[UsersMessages]
(
	[Id] INT  IDENTITY (1,1) NOT NULL PRIMARY KEY, 
    [UserId] INT NOT NULL, 
    [Message] NVARCHAR(500) NOT NULL
)
