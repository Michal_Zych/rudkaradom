﻿CREATE TABLE [dbo].[UnitHistories] (
    [UnitId]       INT        NOT NULL,
    [ParentUnitId] NCHAR (10) NOT NULL,
    [InParentFrom] DATETIME   NOT NULL,
    CONSTRAINT [PK_UnitHistories] PRIMARY KEY CLUSTERED ([UnitId] ASC, [ParentUnitId] ASC, [InParentFrom] ASC)
);

