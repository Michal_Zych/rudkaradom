﻿CREATE TABLE [dbo].[Reports] (
    [ID]          INT             NOT NULL,
    [Name]        NVARCHAR (128)  NOT NULL,
    [Description] NVARCHAR (1024) NULL,
    [Rdl]         NVARCHAR (64)   NOT NULL,
    [Type]        INT             CONSTRAINT [DF_Reports_Type] DEFAULT ((1)) NOT NULL,
    [Picture]     NVARCHAR (50)   NULL,
    [Title]       NVARCHAR (200)  NULL,
    CONSTRAINT [PK_Reports] PRIMARY KEY CLUSTERED ([ID] ASC)
);

