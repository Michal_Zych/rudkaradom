﻿CREATE TABLE [dbo].[MeasurementRanges] (
    [Id]        INT      IDENTITY (1, 1) NOT NULL,
    [SensorID]  SMALLINT NOT NULL,
    [DateStart] DATETIME NOT NULL,
    [DateEnd]   DATETIME NULL,
    [LoRange]   SMALLINT NOT NULL,
    [UpRange]   SMALLINT NOT NULL,
    CONSTRAINT [PK_MeasurementRanges] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MeasurementRanges]
    ON [dbo].[MeasurementRanges]([SensorID] ASC, [DateStart] ASC);

