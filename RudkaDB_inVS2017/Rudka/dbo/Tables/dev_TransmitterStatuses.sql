﻿CREATE TABLE [dbo].[dev_TransmitterStatuses] (
    [Id]           INT           IDENTITY (1, 1) NOT NULL,
    [TransmiterId] INT           NOT NULL,
    [MeasureDate]  DATETIME      NOT NULL,
    [RSSI]         INT           NULL,
    [IpAddress]    NVARCHAR (15) NULL,
    [Version]      NVARCHAR (50) NULL,
    [Status]       NVARCHAR (50) NULL,
    CONSTRAINT [PK_dev_TransmitterRSSI] PRIMARY KEY CLUSTERED ([Id] ASC)
);

