﻿CREATE TABLE [dbo].[ArchivingOrders] (
    [Id]         INT IDENTITY (1, 1) NOT NULL,
    [PatientId]  INT NOT NULL,
    [MaxEventId] INT NOT NULL,
    CONSTRAINT [PK_ArchivingOrders] PRIMARY KEY CLUSTERED ([Id] ASC)
);

