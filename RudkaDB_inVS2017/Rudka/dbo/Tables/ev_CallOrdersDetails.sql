﻿CREATE TABLE [dbo].[ev_CallOrdersDetails] (
    [EventId]           INT          NOT NULL,
    [IsNrFromEventUnit] BIT          NOT NULL,
    [SequenceNr]        TINYINT      NOT NULL,
    [PhoneNr]           VARCHAR (20) NOT NULL,
    CONSTRAINT [PK__ev_Phone__7944C81071CB237D] PRIMARY KEY CLUSTERED ([EventId] ASC, [IsNrFromEventUnit] ASC, [SequenceNr] ASC)
);

