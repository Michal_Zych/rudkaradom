﻿CREATE TABLE [dbo].[dev_HistoryRanges] (
    [Id]      BIGINT   IDENTITY (1, 1) NOT NULL,
    [SenseId] INT      NOT NULL,
    [Start]   DATETIME NOT NULL,
    [End]     DATETIME NOT NULL
);

