﻿CREATE TABLE [dbo].[dev_ModelSenses] (
    [TypeId]            INT          NOT NULL,
    [SensorNr]          SMALLINT     NOT NULL,
    [PhysicalUnit]      NVARCHAR (5) CONSTRAINT [DF_dev_ModelSenses_PhysicalUnit] DEFAULT ('T') NOT NULL,
    [Scale]             REAL         CONSTRAINT [DF_dev_ModelSenses_Scale] DEFAULT ((1)) NOT NULL,
    [Offset]            REAL         CONSTRAINT [DFDF_dev_ModelSenses_Offset] DEFAULT ((0)) NOT NULL,
    [LoRange]           REAL         NOT NULL,
    [HiRange]           REAL         NOT NULL,
    [ShortestIntervalS] INT          CONSTRAINT [DFDF_dev_ModelSenses_ShortestIntervalS] DEFAULT ((60)) NOT NULL,
    [Resolution]        REAL         CONSTRAINT [DFDF_dev_ModelSenses_Resolution] DEFAULT ((0.1)) NOT NULL,
    [IsEvent]           BIT          NULL,
    CONSTRAINT [PK_dev_ModelSenses] PRIMARY KEY CLUSTERED ([TypeId] ASC, [SensorNr] ASC)
);



