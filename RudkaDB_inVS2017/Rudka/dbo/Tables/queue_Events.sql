﻿CREATE TABLE [dbo].[queue_Events] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [ObjectType]    TINYINT        NOT NULL,
    [ObjectID]      INT            NOT NULL,
    [OperationType] CHAR (1)       NOT NULL,
    [Details]       NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_queue_Events] PRIMARY KEY CLUSTERED ([Id] ASC)
);

