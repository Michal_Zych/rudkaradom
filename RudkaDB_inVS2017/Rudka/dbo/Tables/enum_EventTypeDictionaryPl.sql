﻿CREATE TABLE [dbo].[enum_EventTypeDictionaryPl] (
    [Value]       TINYINT        NOT NULL,
    [Description] NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_enum_EventTypeDictionaryPl] PRIMARY KEY CLUSTERED ([Value] ASC)
);

