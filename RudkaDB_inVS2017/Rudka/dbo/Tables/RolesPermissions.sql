﻿CREATE TABLE [dbo].[RolesPermissions] (
    [RoleID]       INT NOT NULL,
    [PermissionID] INT NOT NULL,
    CONSTRAINT [PK_RolesPermissions] PRIMARY KEY CLUSTERED ([RoleID] ASC, [PermissionID] ASC),
    CONSTRAINT [FK_RolesPermissions_Permissions] FOREIGN KEY ([PermissionID]) REFERENCES [dbo].[Permissions] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_RolesPermissions_Roles] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Roles] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
);

