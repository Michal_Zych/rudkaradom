﻿CREATE TABLE [dbo].[dev_TransmitterSenses] (
    [Id]           INT            IDENTITY (1, 1) NOT NULL,
    [TransmiterId] INT            NOT NULL,
    [SenseId]      BIGINT         NOT NULL,
    [MeasureDate]  DATETIME       NOT NULL,
    [RSSI]         INT            NOT NULL,
    [Status]       NVARCHAR (200) CONSTRAINT [DF_dev_TransmitterSenses_Status] DEFAULT (N'n/a') NOT NULL,
    CONSTRAINT [PK_dev_TransmitterSenses] PRIMARY KEY CLUSTERED ([Id] ASC)
);

