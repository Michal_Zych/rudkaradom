﻿CREATE TABLE [dbo].[PreviewsObjects] (
    [PreviewedObjectID] INT  NOT NULL,
    [ObjectID]          INT  NOT NULL,
    [ObjectTypeID]      INT  NOT NULL,
    [TLX]               REAL NOT NULL,
    [TLY]               REAL NOT NULL,
    [BRX]               REAL NOT NULL,
    [BRY]               REAL NOT NULL,
    CONSTRAINT [PK_PreviewsObjects] PRIMARY KEY CLUSTERED ([PreviewedObjectID] ASC, [ObjectID] ASC)
);

