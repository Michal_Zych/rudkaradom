﻿CREATE TABLE [dbo].[ReportTypes] (
    [Id]          TINYINT        NOT NULL,
    [Description] NVARCHAR (200) NOT NULL,
    CONSTRAINT [PK_ReportTypes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

