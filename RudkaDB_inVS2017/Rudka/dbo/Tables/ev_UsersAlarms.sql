﻿CREATE TABLE [dbo].[ev_UsersAlarms]
(
	[EventId] INT NOT NULL PRIMARY KEY, 
    [UserMessageId] INT NOT NULL,
	[UserId] INT NOT NULL, 
    [Message] NVARCHAR(500) NOT NULL
    
)
