﻿CREATE TABLE [dbo].[ev_TransmitterStatus] (
    [TransmitterId]                SMALLINT               NOT NULL,
    [OngDisconnectedEventId]       INT                    NULL,
    [OngDisconnectedAlarmId]       INT                    NULL,
    [OngDisconnectedAlarmSeverity] [dbo].[EVENT_SEVERITY] NULL,
    [OngConnectedEventId]          INT                    NULL,
    [SynchroToken]                 ROWVERSION             NULL,
    PRIMARY KEY CLUSTERED ([TransmitterId] ASC)
);

