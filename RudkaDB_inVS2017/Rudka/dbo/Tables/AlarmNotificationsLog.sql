﻿CREATE TABLE [dbo].[AlarmNotificationsLog] (
    [NotificationID] INT            NOT NULL,
    [ReceiverID]     INT            NOT NULL,
    [Address]        NVARCHAR (128) NULL,
    [SentDate]       DATETIME       NULL,
    [DeleteDate]     DATETIME       NULL,
    CONSTRAINT [PK_AlarmNotificationsLog] PRIMARY KEY CLUSTERED ([NotificationID] ASC, [ReceiverID] ASC)
);

