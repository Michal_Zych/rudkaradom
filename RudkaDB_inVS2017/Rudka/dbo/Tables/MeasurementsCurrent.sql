﻿CREATE TABLE [dbo].[MeasurementsCurrent] (
    [SensorID]             SMALLINT NOT NULL,
    [MeasurementTime]      DATETIME NOT NULL,
    [Value]                SMALLINT NULL,
    [HyperMesurementTime]  DATETIME NULL,
    [HyperValue]           SMALLINT NULL,
    [MeasurementStoreTime] DATETIME NULL,
    CONSTRAINT [PK_MeasurementsCurrent] PRIMARY KEY CLUSTERED ([SensorID] ASC)
);

