﻿CREATE TABLE [dbo].[Units] (
    [ID]               INT            IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (256) NOT NULL,
    [ParentUnitID]     INT            NULL,
    [UnitTypeID]       INT            CONSTRAINT [DF_Units_UnitTypeID] DEFAULT ((1)) NOT NULL,
    [Active]           BIT            CONSTRAINT [DF_Units_Active] DEFAULT ((1)) NULL,
    [ClientID]         INT            NOT NULL,
    [UnitTypeIdv2]     INT            NOT NULL,
    [Pictureid]        INT            NULL,
    [Description]      NVARCHAR (200) NULL,
    [UserID]           INT            NULL,
    [PreviewPictureID] INT            NULL,
    CONSTRAINT [PK_Units] PRIMARY KEY CLUSTERED ([ID] ASC)
);

