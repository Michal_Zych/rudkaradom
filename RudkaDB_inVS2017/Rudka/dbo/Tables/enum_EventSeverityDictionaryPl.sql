﻿CREATE TABLE [dbo].[enum_EventSeverityDictionaryPl] (
    [Value]       TINYINT        NOT NULL,
    [Description] NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_enum_EventSeverityDictionaryPl] PRIMARY KEY CLUSTERED ([Value] ASC)
);

