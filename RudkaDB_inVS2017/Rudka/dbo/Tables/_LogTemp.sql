﻿CREATE TABLE [dbo].[_LogTemp] (
    [Id]      INT            IDENTITY (1, 1) NOT NULL,
    [LogDate] DATETIME       CONSTRAINT [DF__LogTemp_LogDate] DEFAULT (getdate()) NOT NULL,
    [Info]    NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

