﻿CREATE TABLE [dbo].[Logs] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [OperationDate] DATETIME       NOT NULL,
    [OperatorId]    INT            NOT NULL,
    [OperationType] TINYINT        NOT NULL,
    [ObjectType]    TINYINT        NOT NULL,
    [ObjectID]      INT            NOT NULL,
    [Info]          NVARCHAR (MAX) NULL,
    [Reason]        NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_Logs] PRIMARY KEY CLUSTERED ([Id] ASC)
);

