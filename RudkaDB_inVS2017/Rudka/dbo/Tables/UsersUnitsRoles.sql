﻿CREATE TABLE [dbo].[UsersUnitsRoles] (
    [UserID] INT NULL,
    [UnitID] INT NULL,
    [RoleID] INT NULL,
    CONSTRAINT [FK_UsersUnitsRoles_Roles] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Roles] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_UsersUnitsRoles_Units] FOREIGN KEY ([UnitID]) REFERENCES [dbo].[Units_old] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_UsersUnitsRoles_Users] FOREIGN KEY ([UserID]) REFERENCES [dbo].[Users] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
);


GO
CREATE NONCLUSTERED INDEX [IX_UsersUnitsRoles_UserID]
    ON [dbo].[UsersUnitsRoles]([UserID] ASC)
    INCLUDE([UnitID], [RoleID]) WITH (ALLOW_PAGE_LOCKS = OFF);


GO
CREATE NONCLUSTERED INDEX [IX_UserID]
    ON [dbo].[UsersUnitsRoles]([UserID] ASC)
    INCLUDE([UnitID]);

