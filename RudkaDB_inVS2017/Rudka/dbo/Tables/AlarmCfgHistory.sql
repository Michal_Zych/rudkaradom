﻿CREATE TABLE [dbo].[AlarmCfgHistory] (
    [ID]                  INT            IDENTITY (1, 1) NOT NULL,
    [DeviceID]            INT            NOT NULL,		-- Id z Devices
    [UserId]              INT            NOT NULL,		-- Id z Users
    [Timestamp]           DATETIME       NOT NULL,		-- Czas wystąpienia Alarmu
    [A1L]                 DECIMAL (4, 1) NOT NULL,
    [A1H]                 DECIMAL (4, 1) NOT NULL,
    [A2L]                 DECIMAL (4, 1) NOT NULL,
    [A2H]                 DECIMAL (4, 1) NOT NULL,
    [AlDisabled]          BIT            NOT NULL,
    [AlDisabledFrom]      DATETIME       NULL,
    [AlDisabledForRanges] BIT            NOT NULL,
    [AlDisabledRanges]    VARCHAR (MAX)  NULL,
    [AlDisabledForHours]  BIT            NOT NULL,
    [AlDisableHoursFrom1] TIME (7)       NULL,
    [AlDisableHoursTo1]   TIME (7)       NULL,
    [AlDisableHoursFrom2] TIME (7)       NULL,
    [AlDisableHoursTo2]   TIME (7)       NULL,
    [SmsEnabled]          BIT            NOT NULL,
    [ServiceEnabled]      BIT            NOT NULL,
    [SmsT1RangeDelay]     SMALLINT       NULL,
    [SmsT2RangeDelay]     SMALLINT       NULL,
    [SmsT1TechDelay]      SMALLINT       NULL,
    [SmsT2TechDelay]      SMALLINT       NULL,
    [SmsCommDelay]        SMALLINT       NULL,
    [ServiceT1RangeDelay] SMALLINT       NULL,
    [ServiceT2RangeDelay] SMALLINT       NULL,
    [ServiceT1TechDelay]  SMALLINT       NULL,
    [ServiceT2TechDelay]  SMALLINT       NULL,
    [ServiceCommDelay]    SMALLINT       NULL,
    [GuiT1RangeDelay]     SMALLINT       NOT NULL,
    [GuiT2RangeDelay]     SMALLINT       NOT NULL,
    [GuiT1TechDelay]      SMALLINT       NOT NULL,
    [GuiT2TechDelay]      SMALLINT       NOT NULL,
    [GuiCommDelay]        SMALLINT       NOT NULL,
    CONSTRAINT [PK_AlarmCfgHistory] PRIMARY KEY CLUSTERED ([ID] ASC)
);

