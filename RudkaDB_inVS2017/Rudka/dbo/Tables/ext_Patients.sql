﻿CREATE TABLE [dbo].[ext_Patients] (
    [ExtKey]      NVARCHAR (50)  NOT NULL,
    [ExtSystemId] TINYINT        NOT NULL,
    [PatientCode] NVARCHAR (250) NOT NULL,
    [PatientId]   [dbo].[OBJECT_ID]            NULL,
    CONSTRAINT [PK_ext_Patients] PRIMARY KEY CLUSTERED ([ExtKey] ASC, [ExtSystemId] ASC)
);

