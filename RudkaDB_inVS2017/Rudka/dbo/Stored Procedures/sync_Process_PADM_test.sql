﻿CREATE PROCEDURE [dbo].[sync_Process_PADM_test]
	@RecordId int
	,@PrintMode bit = 0
AS 
BEGIN
	SET NOCOUNT ON;

	DECLARE 
		  @KEY_VISIT_ID			nvarchar(100) = 'currentVisit.visitId'
		, @KEY_VISIT_DATE_START	nvarchar(100) = 'currentVisit.dateStart'
		, @KEY_VISIT_DATE_END	nvarchar(100) = 'currentVisit.dateEnd'
		, @KEY_VISIT_UNIT_ID	nvarchar(100) = 'currentVisit.organizationUnitId'
		, @KEY_VISIT_UNIT_NAME	nvarchar(500) = 'currentVisit.organizationUnitName'
		, @KEY_PATIENT_ID		nvarchar(100) = 'identity.patientIdentity.id'
		, @KEY_PATIENT_PESEL	nvarchar(100) = 'identity.patientIdentity.nationalId'
		, @KEY_PATIENT_NAME		nvarchar(100) = 'name'
		, @KEY_PATIENT_LASTNAME nvarchar(100) = 'lastName' 
		, @KEY_PATIENT_TAG_ID	nvarchar(100) = 'tagId'
	
	DECLARE 
		 @AUTO_CLOSE_IMPORT_ERROR bit = 1
		,@ASSECO_OPERATOR_ID int = 0
		,@REASON nvarchar(100) = 'Źródło AMMS INRecordId=' + CONVERT(nvarchar(10), @RecordId)
		,@OperationDateUtc dateTime = dbo.GetCurrentUtcDate()
		,@NULL_VALUE nvarchar(10) = ''
		,@NEW_LINE nvarchar(5) = CHAR(13) + CHAR(10)

	DECLARE
		@errorInfo nvarchar(max)
		,@rec nvarchar(max)
		,@recPatientId nvarchar(100)

		,@currentVisitId nvarchar(10)
		,@eventId int
		,@eventDateUtc dateTime
		,@m2mPatientId int
		,@lastVisitEndDateUtc dateTime

		,@currentPatientId int
		,@archPatientId int

	SELECT @rec = r.Record
		, @recPatientId = r.RecId
		, @eventDateUtc = e.EventDateUtc

		, @m2mPatientId = ep.M2mId
		, @currentVisitId = ep.VisitId
		, @lastVisitEndDateUtc = ep.LastVisitEndDateUtc

		,@currentPatientId = p.Id
		,@archPatientId = ap.Id

	FROM tech.sync_RecordsIN r
	JOIN tech.sync_RecordsOUT ro ON r.OutRecordId = ro.Id
	JOIN tech.sync_EventsIN e ON ro.EventId = e.Id
	LEFT JOIN arch.ext_Patients ep ON ep.ExtId = r.RecId 
	LEFT JOIN dbo.Patients p ON ep.M2mId = p.Id
	LEFT JOIN arch.Patients ap ON ep.M2mId = ap.Id
	WHERE r.Id = @RecordId

	
	DECLARE
		@visitId nvarchar(200)
		,@visitDateStart nvarchar(200)
		,@visitDateEnd nvarchar(200)
		,@visitUnitId nvarchar(200)
		,@visitUnitName nvarchar(500)
		,@patientId nvarchar(200)
		,@pesel char(11)
		,@name nvarchar(200)
		,@lastName nvarchar(200)
		,@tagId nvarchar(200)
		,@newBandId int
		,@visitDateStartUtc dateTime
		,@UnitId int

	SELECT @visitId = NULLIF(LEFT(dbo.srv_KeyValue_CI_GetValue(@rec, @KEY_VISIT_ID), 200), @NULL_VALUE)
		, @visitDateStart = NULLIF(LEFT(dbo.srv_KeyValue_CI_GetValue(@rec, @KEY_VISIT_DATE_START), 200), @NULL_VALUE)
		, @visitDateEnd = NULLIF(LEFT(dbo.srv_KeyValue_CI_GetValue(@rec, @KEY_VISIT_DATE_END), 200), @NULL_VALUE)
		, @visitUnitId = NULLIF(LEFT(dbo.srv_KeyValue_CI_GetValue(@rec, @KEY_VISIT_UNIT_ID), 200), @NULL_VALUE)
		, @patientId = LEFT(dbo.srv_KeyValue_CI_GetValue(@rec, @KEY_PATIENT_ID), 200)
		, @pesel = LEFT(dbo.srv_KeyValue_CI_GetValue(@rec, @KEY_PATIENT_PESEL), 200)
		, @name  = LEFT(dbo.srv_KeyValue_CI_GetValue(@rec, @KEY_PATIENT_NAME), 200)
		, @lastName = LEFT(dbo.srv_KeyValue_CI_GetValue(@rec, @KEY_PATIENT_LASTNAME), 200)
		, @tagId = NULLIF(LEFT(dbo.srv_KeyValue_CI_GetValue(@rec, @KEY_PATIENT_TAG_ID), 200), @NULL_VALUE)
		, @visitUnitName = LEFT(dbo.srv_KeyValue_CI_GetValue(@rec, @KEY_VISIT_UNIT_NAME), 500)

/********************************/
SELECT @newBandId = Id FROM dbo.Bands WHERE BarCode = @tagId
/*******************************/

	IF @PrintMode = 1
	BEGIN
		SELECT @visitId as VisitId
			, @visitDateStart as DateStart
			, @visitDateEnd as DateEnd
			, @visitUnitId as VisitUnitID
			, @visitUnitName AS VisitUnitDescription
			, @patientId as patientid
			, @pesel as pesel
			, @name as name 
			, @lastName as lastName
			, @tagId as TagId
			, @eventDateUtc as EventDateUtc
			, @recPatientId AS RecId
			, @m2mPatientId AS m2mPatientID
			, @currentPatientId AS IdPacjentaWSzpitalu
			, @archPatientId AS IdPacjentaWArchiwum
			, @newBandId AS NewBandId
			, @visitDateStartUtc AS VisitDateStartUtc
			, @UnitId AS UnitId
			
	END

	IF @recPatientId <> @patientId
	BEGIN
		SET @errorInfo = 'Niespójność w identyfikacji pacjenta. Id rekordu importowanego: ' + COALESCE(@recPatientId, 'null') + ' Id pacjenta: ' + COALESCE(@patientId, 'null')
	END
	
	-- Edycja danych pacjenta
	IF @currentPatientId IS NOT NULL 
	BEGIN
		DECLARE 
			@currentName nvarchar(100)
			,@currentLastName nvarchar(100)
			,@currentPesel nvarchar(11)
		SELECT @currentName = Name, @currentLastName = LastName, @currentPesel = Pesel FROM dbo.Patients WHERE Id = @currentPatientId
		IF @currentName <> @name OR @currentLastName = @lastName OR @currentPesel <> @pesel
		BEGIN
			SET @pesel = COALESCE(@pesel, @currentPesel)
			EXEC dbo.ev_PatientEdit @currentPatientId, @name, @lastName, @pesel, @ASSECO_OPERATOR_ID, @REASON, null
		END
		
	END
	IF @archPatientId IS NOT NULL
	BEGIN
		UPDATE arch.Patients SET Name = @name, LastName = @lastName, Pesel = @pesel WHERE Id = @currentPatientId
	END

	IF @tagId IS NOT NULL
	BEGIN
		SELECT @newBandId = Id FROM dbo.Bands WHERE BarCode = @tagId
		IF @newBandId IS NULL
		BEGIN
			SET @errorInfo = COALESCE(@errorInfo + @NEW_LINE, '') + 'Nie znalezionno opaski o kodzie ' + COALESCE(@tagId, 'null')
		END
	END

	IF @visitDateStart IS NOT NULL
	BEGIN
		IF ISDATE(@visitDateStart) = 0
			SET @errorInfo = COALESCE(@errorInfo + @NEW_LINE, '') + 'Błędny format daty rozpoczęcia wizyty ' + COALESCE(@visitDateStart, 'null')
		ELSE
			SELECT @visitDateStartUtc = dbo.ToUtcDate(@visitDateStart)


		IF @visitUnitId IS NOT NULL
			SELECT @UnitId = M2mId FROM arch.ext_Units WHERE ExtId = @visitUnitId
		IF @UnitId IS NULL
		BEGIN
			IF NOT EXISTS (SELECT * FROM arch.ext_Units WHERE ExtId = @visitUnitId)
				INSERT arch.ext_Units(ExtId, ExtDescription) 
				VALUES(@visitUnitId, COALESCE(@visitUnitName, @NULL_VALUE))
			DECLARE @errorUnitName nvarchar(max) = 'Niezamapowana jednostka organizacyjna Id=' + @visitUnitId + '; opis=' + COALESCE(@visitUnitName, 'brak') + ' - użyto Rejestracji'

			EXEC dbo.sync_StoreImportError @RecordId, @errorUnitName, @AUTO_CLOSE_IMPORT_ERROR

			SELECT @UnitId = RegistrationWardId FROM dbo.Clients WHERE Id = 1
		END
	END

	
	DECLARE @result TABLE(Result int, ErrorCode int, ErrorMessage nvarchar(max))


	-- przetestować przyjęcie do szpitala pacjenta, który keidyś był w szpitalu
	IF @errorInfo IS NULL AND  @currentPatientId IS NULL
		AND @visitId IS NOT NULL AND @newBandId IS NOT NULL 
		AND @visitDateStartUtc > COALESCE(@lastVisitEndDateUtc, CONVERT(dateTime, 1))
	BEGIN  -- 
		INSERT @result
		EXEC dbo.ev_PatientToHospital @name, @lastName, @pesel, @UnitId, null, @newBandId, null, @ASSECO_OPERATOR_ID, @REASON, 1, @visitDateStartUtc

		SELECT @m2mPatientId = Result FROM @result
		IF @m2mPatientId IS NOT  NULL
			INSERT arch.ext_Patients(ExtId, M2mId, VisitId) VALUES(@patientId, @m2mPatientId, @visitId)
	END

	/*


	



	IF @m2mPatientId IS NOT NULL
	SELECT @currentVisitId = ep.VisitId
		,@lastVisitEndDateUtc = ep.LastVisitEndDateUtc

	FROM arch.ext_Patients ep
	WHERE ep.M2mId = @m2mPatientId




	SELECT @m2mPatientId = p.Id
		, @RoomWardDateUtc = COALESCE(p.RoomDateUtc, p.WardDateUtc)
		, @currentTagBarCode = b.BarCode
		, @currentBandDateUtc = p.BandDateUtc
		, @lastVisitEndDate  = ep.LastVisitEndDateUtc
		, @currentBandId = p.BandId
	FROM dbo.Patients p
	JOIN arch.ext_Patients ep ON p.Id = ep.M2mId AND ep.ExtId = @patientId
	LEFT JOIN dbo.Bands b ON b.Id = p.BandId




	
	
	IF @PrintMode = 1 RETURN

	


----------------------
	declare
	@processed bit = 1
,@oldRecord	nvarchar(max)
	DECLARE 
		 @error nvarchar(max)
		,@m2mPatientId int
		,@RoomWardDateUtc dateTime
		,@currentTagBarCode nvarchar(100)
		,@currentBandId int

		,@currentBandDateUtc dateTime
		,@lastVisitEndDate dateTime
		,@visitDateStartUtc dateTime = dbo.ToUtcDate(@visitDateStart)

	
	

	IF @visitId IS NULL
		AND @m2mPatientId IS NOT NULL  -- i pacjent w szpitalu
		AND @eventDateUtc > @RoomWardDateUtc -- i data zdarzenia późniejsza niż przyjęcie pacjęta na oodział/salę
	BEGIN
		-- wypisz pacjenta ze szpitala
		EXEC dbo.ev_PatientFromHospital @m2mPatientId, @ASSECO_OPERATOR_ID, @REASON, @eventDateUtc
	END

	IF @visitId IS NOT NULL AND @tagId IS NULL 
		AND @m2mPatientId IS NOT NULL 
		AND @currentBandId IS NOT NULL AND @currentBandDateUtc < @eventDateUtc
	BEGIN
		-- wypisz ze szpitala
		EXEC dbo.ev_PatientFromHospital @m2mPatientId, @ASSECO_OPERATOR_ID, @REASON, @eventDateUtc
	END


	

	IF @visitId IS NOT NULL AND @tagId IS NOT NULL AND @m2mPatientId IS NOT NULL -- pacjent w szpitalu
	BEGIN
		IF @currentVisitId<> @visitId AND @visitDateStartUtc > @RoomWardDateUtc 
			UPDATE arch.ext_Patients SET VisitId = @visitId WHERE ExtId = @patientId

		IF @tagId <> @currentTagBarCode AND COALESCE(@currentBandDateUtc, '01-01-01') < @eventDateUtc
		BEGIN
			--### PRzypisz tag
			print 'przypisz tag'
		END
	END





	DECLARE @result TABLE(Id int, ErrorCode int, ErrorMessage nvarchar(max))

	BEGIN TRANSACTION
		IF COALESCE(@record, '') <> COALESCE(@oldRecord, '')
		BEGIN
			BEGIN--pobranie danych przesłanych z serwisu
				SET @name = LEFT(dbo.srv_KeyValue_GetValue(@Record, 'NAME'), 100) 
				SET @lastName = LEFT(dbo.srv_KeyValue_GetValue(@Record, 'LAST_NAME'), 100)
				SET @pesel = LEFT(dbo.srv_KeyValue_GetValue(@Record, 'PESEL'), 11)
			END

			IF @m2mId IS NULL
			BEGIN  --nowy pacjent
					INSERT dbo.Patients (Name, LastName, Pesel, HospitalDateUtc) VALUES (@name, @lastName, @PESEL, @OperationDateUtc)
					SELECT @m2mId = SCOPE_IDENTITY()

					DECLARE @json nvarchar(max) = 'SELECT Id, Name, LastName, Pesel FROM dbo.Patients WHERE Id = ' + CONVERT(nvarchar(10), @m2mId)
					EXEC @json = dbo.SerializeJSON @json

					INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason)
					SELECT @OperationDateUtc, eventType.[Create], eventSeverity.[Event], null, @m2mId, objectType.Patient, null, @OperationDateUtc, @OperationDateUtc, @ASSECO_OPERATOR_ID, @json, @REASON 
					FROM enum.EventType eventType				
					JOIN enum.EventSeverity eventSeverity ON 1 = 1
					JOIN enum.ObjectType objectType ON 1 = 1
				
					SELECT @eventId = SCOPE_IDENTITY()
					UPDATE ev_Events SET EndingEventId = @eventId WHERE Id = @eventId


					INSERT arch.Patients (Id, Name, LastName, Pesel) VALUES (@m2mId, @name, @lastName, @pesel)

					DELETE dbo.Patients WHERE Id = @m2mId
					
					INSERT arch.ext_Patients(ExtId, M2mId, Visitid) VALUES (@recId, @m2mId, @visitId)
			END		
			ELSE -- edycja pacjenta
			BEGIN
				INSERT @result
				EXEC dbo.ev_PatientEdit @m2mId, @name, @lastName, @pesel, @ASSECO_OPERATOR_ID, @REASON

				SELECT @error = ErrorMessage FROM @result
				IF @error IS NOT NULL 
					SELECT @processed = 0
				ELSE
					UPDATE arch.ext_Patients SET 
						--ExtDescription = @record
						VisitId = @visitId
					WHERE ExtId = @recId
			END
		END
		UPDATE tech.sync_RecordsIN SET
			Processed = @processed
			,ProcessedDateUtc = @OperationDateUtc
			,Error = @error
			,Try = COALESCE([Try], 0) + 1
		WHERE Id = @RecordId
	COMMIT TRANSACTION
*/
	IF @errorInfo IS NOT NULL
		SELECT @errorInfo
	--	EXEC dbo.sync_StoreImportError @RecordId, @errorInfo, @AUTO_CLOSE_IMPORT_ERROR


END