﻿

CREATE procedure [dbo].[srv_ServicePing]
	@ServiceName nvarchar(200)
	,@PartnerResponded bit = null
	,@IsRestart bit = null
	,@Status nvarchar(max) = null
AS
BEGIN
	DECLARE 
		@currentDate datetime = dbo.GetCurrentUtcDate()
		
	UPDATE dbo.[Services] SET
		StartTimeUtc = CASE WHEN @IsRestart = 1 THEN @currentDate ELSE StartTimeUtc END
		,PingTimeUtc = @currentDate
		,PreviousPingTimeUtc = PingTimeUtc
		,InfoTxt = @Status
		,PreviousInfoTxt = InfoTxt
		,PartnerPingTimeUtc = CASE WHEN HasPartner = 1 THEN
								CASE WHEN @PartnerResponded = 1 THEN @currentDate ELSE PartnerPingTimeUtc END
							ELSE NULL END
	WHERE @ServiceName = ServiceName
	
	IF @@ROWCOUNT = 0
		INSERT dbo.[Services]  (ServiceName, StartTimeUtc, PingTimeUtc, PreviousPingTimeUtc, HasPartner, PartnerPingTimeUtc,InfoTxt, PreviousInfoTxt)
			VALUES (
				@ServiceName
				,@currentDate
				,@currentDate
				,null
				,CASE WHEN @PartnerResponded IS NULL THEN 0 ELSE 1 END
				,CASE WHEN @PartnerResponded = 1 THEN @currentDate ELSE NULL END
				,@Status
				,null
				)
END




