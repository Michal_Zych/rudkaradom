﻿-- =============================================
-- Author:		pi
-- Create date: 23-10-2012
-- Description:	Get UsersUnitsRoles (only acitve users)
-- =============================================
CREATE PROCEDURE [dbo].[GetActiveUsersUnitsRoles]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

	select r.UserID, r.UnitId, r.RoleId 
	from UsersUnitsRoles r inner join Users u
	on r.UserId = u.Id 
	where u.Active = 1

END

