﻿CREATE PROCEDURE [dbo].[ev_GenerateBandStatus](
	 @BandId int									
    ,@CurrentUnitId int
    ,@InCurrentUnitUtc dateTime
    ,@PreviousUnitId int
    ,@InPreviousUnitUtc dateTime
    ,@TopAlarmId int
    ,@TopAlarmType dbo.EVENT_TYPE  
    ,@TopAlarmSeverity dbo.EVENT_SEVERITY 
    ,@TopAlarmDateUtc dateTime
	,@TopEventDateUtc dateTime
    ,@Severity1Count int
    ,@Severity2Count int
    ,@OngNoGoZoneInEventId int
    ,@OngNoGoZoneInAlarmId int 
    ,@OngDisconnectedEventId int 
    ,@OngDisconnectedAlarmId int 
    ,@OngZoneOutEventId int 
    ,@OngZoneOutAlarmId int 
    ,@OngNoMovingEventId int 
    ,@OngNoMovingAlarmId int 
    ,@OngMovingEventId int 
    ,@OngMovingAlarmId int 
    ,@OngLowBatteryEventId int 
    ,@OngLowBatteryAlarmId int
	,@CurrentEventId int
	,@CurrentEventType dbo.EVENT_TYPE
	,@CurrentEventDateUtc dateTime
	,@GeneratedAlarmId int
	,@GeneratedAlarmSeverity dbo.EVENT_SEVERITY
	,@GeneratedWarningDateUtc dateTime
	,@GeneratedAlarmDateUtc dateTime
)
AS
BEGIN
	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
		BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'BandId=' + COALESCE(CONVERT(nvarchar(100), @BandId), 'NULL') 
		EXEC Log_Action @@PROCID, @info
	END
	/****************************************************/

	IF @GeneratedAlarmId IS NOT NULL
		IF  @TopAlarmSeverity IS NULL
			OR (@topAlarmSeverity < @GeneratedAlarmSeverity)
			OR ((@topAlarmSeverity = @GeneratedAlarmSeverity) AND (dbo.ev_GetEventPriority(@topAlarmType) < dbo.ev_GetEventPriority(@currentEventType)))
		BEGIN 
			SELECT @topAlarmId = COALESCE(@GeneratedAlarmId, @CurrentEventId)
			, @topAlarmType = @currentEventType
			, @topAlarmSeverity = @GeneratedAlarmSeverity
			, @topAlarmDateUtc = COALESCE(@GeneratedAlarmDateUtc, @GeneratedWarningDateUtc, @CurrentEventDateUtc)
			, @topEventDateUtc = @CurrentEventDateUtc
		END
	-- zapisz status
	UPDATE ev_BandStatus SET
		CurrentUnitId			= @currentUnitId				
		,InCurrentUnitUtc		= @inCurrentUnitUtc			
		,PreviousUnitId			= @previousUnitId			
		,InPreviousUnitUtc		= @inPreviousUnitUtc			
		,TopAlarmId				= @topAlarmId				
		,TopAlarmType			= @topAlarmType				
		,TopAlarmSeverity		= @topAlarmSeverity			  
		,TopAlarmDateUtc		= @topAlarmDateUtc			
		,TopEventDateUtc		= @topEventDateUtc			
		,Severity1Count			= @severity1Count			
		,Severity2Count			= @severity2Count			
		,OngNoGoZoneInEventId	= @ongNoGoZoneInEventId		
		,OngNoGoZoneInAlarmId	= @ongNoGoZoneInAlarmId		
		,OngDisconnectedEventId	= @ongDisconnectedEventId	
		,OngDisconnectedAlarmId	= @ongDisconnectedAlarmId	
		,OngZoneOutEventId		= @ongZoneOutEventId			
		,OngZoneOutAlarmId		= @ongZoneOutAlarmId			
		,OngNoMovingEventId		= @ongNoMovingEventId		
		,OngNoMovingAlarmId		= @ongNoMovingAlarmId		
		,OngMovingEventId		= @ongMovingEventId			
		,OngMovingAlarmId		= @ongMovingAlarmId			
		,OngLowBatteryEventId	= @ongLowBatteryEventId		
		,OngLowBatteryAlarmId	= @ongLowBatteryAlarmId		
	WHERE BandId = @BandId

END