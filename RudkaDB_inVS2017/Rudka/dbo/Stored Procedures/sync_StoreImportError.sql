﻿
CREATE PROCEDURE dbo.sync_StoreImportError
	@RecordId int						= 999
	,@ErrorInfo nvarchar(max)			= 'błąd zapisu'
	,@AutoClose bit = 1
AS
BEGIN
	DECLARE @UPDATE_ENDING_EVENT_ID bit = 1
		,@AUTO_CLOSE_MESSAGE nvarchar(200) = 'Alarm zamknięty automatycznie'

	DECLARE
		@currentDateUtc dateTime = dbo.GetCurrentUtcDate()
		,@eventId int

	INSERT dbo.ev_Events(DateUtc, Type, Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, EndingEventId, OperatorId, EventData, Reason)
	SELECT
		@currentDateUtc
		,eventType.AmmsImport
		,eventSeverity.Alarm
		,null
		,@RecordId
		,objectType.AmmsImport
		, null
		, @currentDateUtc
		, @currentDateUtc
		, null
		, null
		, @ErrorInfo
		, null
	FROM enum.EventType eventType 
	JOIN enum.EventSeverity eventSeverity ON 1 = 1
	JOIN enum.ObjectType objectType ON 1 = 1
	SET @eventId = SCOPE_IDENTITY()
	IF @UPDATE_ENDING_EVENT_ID = 1
	BEGIN
		UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
	END

	INSERT ev_Alarms(EventId, RaisingEventId, IsClosed, ClosingDateUtc, ClosingUserId, ClosingAlarmId, ClosingComment)
	SELECT
		@eventId
		,@eventId
		,@AutoClose
		,CASE WHEN @AutoClose = 1 THEN @currentDateUtc ELSE NULL END
		,NULL
		,CASE WHEN @AutoClose = 1 THEN @eventId ELSE NULL END
		,CASE WHEN @AutoClose = 1 THEN @AUTO_CLOSE_MESSAGE ELSE NULL END
END