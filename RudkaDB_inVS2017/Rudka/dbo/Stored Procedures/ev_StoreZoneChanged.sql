﻿CREATE PROCEDURE [dbo].[ev_StoreZoneChanged]
	@BandId smallint
	,@EventDateUtc dateTime = NULL
	,@NewUnitId int
AS
BEGIN
	SET NOCOUNT ON;
/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
	BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'BandId=' + COALESCE(CONVERT(nvarchar(100), @BandId), 'NULL') 
					+ ';EventDateUtc=' + COALESCE(CONVERT(nvarchar(50), @EventDateUtc, 121), 'NULL')
					+ ';NewUnitId=' + COALESCE(CONVERT(nvarchar(100), @NewUnitId), 'NULL')
		EXEC Log_Action @@PROCID, @info
	END
/****************************************************/


	DECLARE @UPDATE_ENDING_EVENT_ID bit = 1
		,@SYSTEM_USER_ID int = 0
		,@OBJECT_TYPE_BAND dbo.OBJECT_TYPE

	DECLARE
		@currentDateUtc datetime = dbo.GetCurrentUtcDate()
		,@bandStatusChanged bit = 0
		,@ongoingZoneAlarmSeverity dbo.EVENT_SEVERITY
		,@ongoingNoGoAlarmSeverity dbo.EVENT_SEVERITY

		,@warningOutZoneDateUtc dateTime
		,@alarmOutZoneDateUtc dateTime
		,@warningInNoGoZoneDateUtc dateTime
		,@alarmInNoGoZoneDateUtc dateTime
	IF @EventDateUtc IS NULL SET @EventDateUtc = @currentDateUtc

BEGIN -- zmienne konfiguracji
	DECLARE
		 @objectType				dbo.OBJECT_TYPE   
		,@objectId					dbo.OBJECT_ID  
		,@objectUnitId				int  
		,@currentUnitId				int  
		,@inCurrentUnitUtc			dateTime  
		,@previousUnitId			int  
		,@inPreviousUnitUtc			dateTime  
		,@topAlarmId				int  
		,@topAlarmType				dbo.EVENT_TYPE   
		,@topAlarmSeverity			dbo.EVENT_SEVERITY   
		,@topAlarmDateUtc			dateTime 
		,@topEventDateUtc			dateTime  
		,@severity1Count			int  
		,@severity2Count			int  
		,@ongNoGoZoneInEventId		int  
		,@ongNoGoZoneInAlarmId		int  
		,@ongDisconnectedEventId	int  
		,@ongDisconnectedAlarmId	int  
		,@ongZoneOutEventId			int  
		,@ongZoneOutAlarmId			int  
		,@ongNoMovingEventId		int  
		,@ongNoMovingAlarmId		int  
		,@ongMovingEventId			int  
		,@ongMovingAlarmId			int  
		,@ongLowBatteryEventId		int  
		,@ongLowBatteryAlarmId		int 

		,@zoneOutWarningActive		bit     
		,@zoneOutWarningDelayMins	smallint
		,@zoneOutAlarmActive		bit     
		,@zoneOutAlarmDelayMins		smallint
		,@noGoInWarningActive		bit     
		,@noGoInWarningDelayMins	smallint
		,@noGoInAlarmActive			bit     
		,@noGoInAlarmDelayMins		smallint

		,@isCurrentUnitNoGoZone		int
		,@isNewUnitNoGoZone			int

		,@warningAutoCloseMessage	nvarchar(max)
		,@fullMonitoringUnassignedBands bit
		,@zoneInEventType			dbo.EVENT_TYPE
		,@zoneOutEventType			dbo.EVENT_TYPE
		,@noGoZoneInEventType		dbo.EVENT_TYPE
		,@noGoZoneOutEventType		dbo.EVENT_TYPE

		,@currentOut bit = 0
		,@homeOut bit = 0
		,@noGoOut bit = 0
		,@newIn bit = 0
		,@homeIn bit = 0
		,@noGoIn bit = 0
		,@zoneOutEventId int
		,@zoneInEventId int
		,@finishedEventId int
		,@finishedAlarmId int
END

BEGIN	-- pobranie konfiguracji
	SELECT
		 @objectType					= bs.ObjectType			
		,@objectId						= bs.ObjectId				
		,@objectUnitId					= bs.ObjectUnitId			
		,@currentUnitId					= bs.CurrentUnitId			
		,@inCurrentUnitUtc				= bs.InCurrentUnitUtc		
		,@previousUnitId				= bs.PreviousUnitId		
		,@inPreviousUnitUtc				= bs.InPreviousUnitUtc		
		,@topAlarmId					= bs.TopAlarmId			
		,@topAlarmType					= bs.TopAlarmType			
		,@topAlarmSeverity				= bs.TopAlarmSeverity		  
		,@topAlarmDateUtc				= bs.TopAlarmDateUtc		
		,@topEventDateUtc				= bs.TopEventDateUtc		
		,@severity1Count				= bs.Severity1Count		
		,@severity2Count				= bs.Severity2Count		
		,@ongNoGoZoneInEventId			= bs.OngNoGoZoneInEventId  
		,@ongNoGoZoneInAlarmId			= bs.OngNoGoZoneInAlarmId  
		,@ongDisconnectedEventId		= bs.OngDisconnectedEventId
		,@ongDisconnectedAlarmId		= bs.OngDisconnectedAlarmId
		,@ongZoneOutEventId				= bs.OngZoneOutEventId		
		,@ongZoneOutAlarmId				= bs.OngZoneOutAlarmId		
		,@ongNoMovingEventId			= bs.OngNoMovingEventId	
		,@ongNoMovingAlarmId			= bs.OngNoMovingAlarmId	
		,@ongMovingEventId				= bs.OngMovingEventId		
		,@ongMovingAlarmId				= bs.OngMovingAlarmId		
		,@ongLowBatteryEventId			= bs.OngLowBatteryEventId  
		,@ongLowBatteryAlarmId			= bs.OngLowBatteryAlarmId  
		
		,@zoneOutWarningActive			= COALESCE(cp.OutOfZoneWrActive	, cu.OutOfZoneWrActive)
		,@zoneOutWarningDelayMins		= COALESCE(cp.OutOfZoneWrMins	, cu.OutOfZoneWrMins  )
		,@zoneOutAlarmActive			= COALESCE(cp.OutOfZoneAlActive	, cu.OutOfZoneAlActive)
		,@zoneOutAlarmDelayMins			= COALESCE(cp.OutOfZoneAlMins	, cu.OutOfZoneAlMins  )
		,@noGoInWarningActive			= 0
		,@noGoInWarningDelayMins		= 0
		,@noGoInAlarmActive				= 1
		,@noGoInAlarmDelayMins			= COALESCE(cp.InNoGoZoneMins, cu.InNoGoZoneMins)

		,@isCurrentUnitNoGoZone			= cu.IsNoGoZone + COALESCE(nc.UnitId - nc.UnitId + 1, 0)
		,@isNewUnitNoGoZone				= nu.IsNoGoZone + COALESCE(nn.UnitId - nn.UnitId + 1, 0)

		,@warningAutoCloseMessage		= st.WarningAutoCloseMessage
		,@fullMonitoringUnassignedBands = st.FullMonitoringUnassignedBands
		,@zoneInEventType				= eventType.ZoneIn
		,@zoneOutEventType				= eventType.ZoneOut
		,@noGoZoneInEventType			= eventType.NoGoZoneIn
		,@noGoZoneOutEventType			= eventType.NoGoZoneOut

		,@OBJECT_TYPE_BAND				= objectType.Band
	FROM dbo.ev_Settings st 
		JOIN enum.EventType eventType  ON 1 = 1
		JOIN enum.ObjectType objectType ON 1 = 1
		LEFT JOIN dbo.ev_BandStatus bs ON bs.BandId = @BandId
		LEFT JOIN dbo.al_NoGoZones nc ON ((nc.Id = bs.ObjectId AND nc.ObjectType = bs.ObjectType) -- dla monitorowanego obiektu
											OR (bs.ObjectType = objectType.Band AND nc.id = bs.ObjectUnitId AND nc.ObjectType = objectType.Unit )) -- dla unita do którego przypisano opaskę
										AND nc.UnitId = bs.CurrentUnitId
		LEFT JOIN dbo.al_NoGoZones nn ON ((nn.Id = bs.ObjectId AND nn.ObjectType = bs.ObjectType) -- dla monitorowanego obiektu
											OR (bs.ObjectType = objectType.Band AND nn.id = bs.ObjectUnitId AND nn.ObjectType = objectType.Unit )) -- dla unita do którego przypisano opaskę
										AND nn.UnitId = @NewUnitId
		LEFT JOIN dbo.al_ConfigPatients cp ON cp.PatientId = bs.ObjectId AND bs.ObjectType = objectType.Patient
		LEFT JOIN dbo.al_ConfigUnits cu ON cu.UnitId = bs.ObjectUnitId AND bs.ObjectType = objectType.Band
		LEFT JOIN dbo.al_ConfigUnits nu ON nu.UnitId = @NewUnitId
END	


BEGIN	--zakończ brak łączności
	DECLARE
		 @connectedNewUnitId int = @NewUnitId		
		,@connectedEventDateUtc	dateTime = @EventDateUtc		
		,@connectedCurrentUnitId int = @CurrentUnitId	
		,@connectedInCurrentUnitUtc	dateTime = @InCurrentUnitUtc	

	IF @ongDisconnectedEventId IS NOT NULL
	BEGIN
		EXEC [dbo].[ev_SetBandConnected]
			@BandId						= @BandId
			,@currentDateUtc			= @currentDateUtc
			,@CurrentEventDateUtc		= @EventDateUtc
			,@CurrentEventUnitId		= @currentUnitId
			,@ObjectId					= @objectId				
			,@ObjectType				= @objectType
			,@ObjectUnitId				= @objectUnitId
			,@CurrentUnitId				= @connectedNewUnitId			OUTPUT
			,@InCurrentUnitUtc			= @connectedEventDateUtc		OUTPUT
			,@PreviousUnitId			= @connectedCurrentUnitId		OUTPUT
			,@InPreviousUnitUtc			= @connectedInCurrentUnitUtc	OUTPUT
			,@OngDisconnectedEventId	= @ongDisconnectedEventId		OUTPUT
			,@OngDisconnectedAlarmId	= @ongDisconnectedAlarmId		OUTPUT
		SET @bandStatusChanged = 1
	END
END

	-- zarejestrowanie rekordu statusu dla opaski
	IF @objectId IS NULL
	BEGIN
		EXEC dbo.ev_AssignBandToItself
			@EventDateUtc		= @EventDateUtc
			,@BandId			= @BandId
			,@ObjectId			= @objectId			OUTPUT
			,@ObjectType		= @objectType		OUTPUT
			,@InCurrentUnitUtc	= @inCurrentUnitUtc	OUTPUT
			,@severity1Count	= @severity1Count	OUTPUT
			,@severity2Count	= @severity2Count	OUTPUT
	END

	-- przetwarzaj jeśli zmiana unita
	IF COALESCE(@NewUnitId, -1) <> COALESCE(@currentUnitId, -1)
	BEGIN
		-- Dla niewpełni monitorowanych opasek zarejestruj tylko zmianę CurrentUnit - bez zdarzeń
		IF @objectType = @OBJECT_TYPE_BAND AND @fullMonitoringUnassignedBands = 0
		BEGIN
			UPDATE dbo.ev_BandStatus SET
				CurrentUnitId = @NewUnitId
				,InCurrentUnitUtc = @EventDateUtc
				,PreviousUnitId = CurrentUnitId
				,InPreviousUnitUtc = InCurrentUnitUtc
			WHERE BandId = @BandId
			RETURN
		END

	
		SET @bandStatusChanged = 1
		BEGIN	
			-- obliczenia przejść
			
			IF COALESCE(@currentUnitId, -1)  <> @objectUnitId SELECT @currentOut = 1
			--IF @currentUnitId = @objectUnitId OR (@currentUnitId IS NULL AND @NewUnitId <> @objectUnitId) SELECT @homeOut = 1
			IF @currentUnitId = @objectUnitId OR (@currentUnitId IS NULL AND @NewUnitId <> @objectUnitId AND @previousUnitId = @objectUnitId) SELECT @homeOut = 1 --JZ
			IF @isCurrentUnitNoGoZone = 1 SELECT @noGoOut = 1, @currentOut = 0
			IF @NewUnitId = @objectUnitId SELECT @homeIn = 1 ELSE SELECT @newIn = 1
			IF @isNewUnitNoGoZone = 1 SELECT @noGoIn = 1, @newIn = 0
		
			--IF @currentOut = 1 -- wyjście z bierzącego
			IF @currentOut = 1 AND @currentUnitId IS NOT NULL -- wyjście z bierzącego
			BEGIN
				SELECT @finishedEventId = NULL, @finishedAlarmId = null
				EXEC dbo.ev_GenerateEvent
					@currentDateUtc			= @currentDateUtc
					,@EventDateUtc			= @EventDateUtc
					,@EventType				= @zoneOutEventType
					,@EventUnitId			= @currentUnitId
					,@EventObjectId			= @objectId
					,@EventObjectType		= @objectType
					,@EventObjectUnitId		= @objectUnitId
					,@EventData				= null
					,@IsLongEvent			= 0
					,@FinishedEventId		= @finishedEventId		OUTPUT
					,@FinishedAlarmId		= @finishedAlarmId		OUTPUT
					,@EventId				= @zoneOutEventID		OUTPUT
			END

			IF @homeOut = 1 -- wyjście z macierzystego
			BEGIN
				SELECT @finishedEventId = NULL, @finishedAlarmId = null
				EXEC dbo.ev_GenerateEvent
					@currentDateUtc			= @currentDateUtc
					,@EventDateUtc			= @EventDateUtc
					,@EventType				= @zoneOutEventType
					,@EventUnitId			= @objectUnitId
					,@EventObjectId			= @objectId
					,@EventObjectType		= @objectType
					,@EventObjectUnitId		= @objectUnitId
					,@EventData				= null
					,@IsLongEvent			= 1
					,@FinishedEventId		= @finishedEventId		OUTPUT
					,@FinishedAlarmId		= @finishedAlarmId		OUTPUT
					,@EventId				= @ongZoneOutEventId	OUTPUT
				-- wygeneruj ostrzeżenia/alarmy
				EXEC dbo.ev_GenerateAlarmsForEvent 
					 @EventObjectType			= @OBJECT_TYPE_BAND
					,@EventObjectid				= @BandId
					,@currentDateUtc			= @currentDateUtc
					,@CurrentEventDateUtc		= @EventDateUtc
					,@currentEventId			= @ongZoneOutEventId
					,@currentEventType			= @zoneOutEventType
					,@currentUnitId				= @objectUnitId --@currentUnitId
					,@objectId					= @objectId
					,@objectType				= @objectType
					,@objectUnitId				= @objectUnitId
					,@warningCloseMessage		= @warningAutoCloseMessage
					,@WarningActive				= @zoneOutWarningActive
					,@WarningDelayMins			= @zoneOutWarningDelayMins
					,@AlarmActive				= @zoneOutAlarmActive
					,@AlarmDelayMins			= @zoneOutAlarmDelayMins	
					,@CurrrentAlarmSeverity		= @ongoingZoneAlarmSeverity OUTPUT
					,@CurrrentAlarmId			= @ongZoneOutAlarmId OUTPUT
					,@warningDateUtc			= @warningOutZoneDateUtc OUTPUT
					,@alarmDateUtc				= @alarmOutZoneDateUtc	OUTPUT
					,@severity1Count			= @severity1Count OUTPUT
					,@severity2Count			= @severity2Count OUTPUT
			END

			IF @noGoOut = 1 -- wyjście ze strefy zakazanej
			BEGIN
				DECLARE @noGoZoneId int
				EXEC dbo.ev_GenerateEvent
					@currentDateUtc				= @currentDateUtc
					,@EventDateUtc				= @EventDateUtc
					,@EventType					= @noGoZoneOutEventType
					,@EventUnitId				= @currentUnitId
					,@EventObjectId				= @objectId
					,@EventObjectType			= @objectType
					,@EventObjectUnitId			= @objectUnitId
					,@EventData					= null
					,@IsLongEvent				= 0
					,@FinishedEventId			= @ongNoGoZoneInEventId	OUTPUT
					,@FinishedAlarmId			= @ongNoGoZoneInAlarmId	OUTPUT
					,@EventId					= @noGoZoneId			OUTPUT
			END

			IF @newIn = 1 -- wejście do nowego
			BEGIN
				SELECT @finishedEventId = NULL, @finishedAlarmId = null
				EXEC dbo.ev_GenerateEvent
					@currentDateUtc			= @currentDateUtc
					,@EventDateUtc			= @EventDateUtc
					,@EventType				= @zoneInEventType
					,@EventUnitId			= @NewUnitId
					,@EventObjectId			= @objectId
					,@EventObjectType		= @objectType
					,@EventObjectUnitId		= @objectUnitId
					,@EventData				= null
					,@IsLongEvent			= 1
					,@FinishedEventId		= @finishedEventId		OUTPUT
					,@FinishedAlarmId		= @finishedAlarmId		OUTPUT
					,@EventId				= @zoneInEventId	OUTPUT
			END

			IF @homeIn = 1 -- wejście do macierzystego
			BEGIN
				EXEC dbo.ev_GenerateEvent
					@currentDateUtc				= @currentDateUtc
					,@EventDateUtc				= @EventDateUtc
					,@EventType					= @zoneInEventType
					,@EventUnitId				= @NewUnitId
					,@EventObjectId				= @objectId
					,@EventObjectType			= @objectType
					,@EventObjectUnitId			= @objectUnitId
					,@EventData					= null
					,@IsLongEvent				= 0
					,@FinishedEventId			= @OngZoneOutEventId	OUTPUT
					,@FinishedAlarmId			= @OngZoneOutAlarmId	OUTPUT
					,@EventId					= @zoneInEventId		OUTPUT
			END

			IF @noGoIn = 1 -- wejście do strefy zakazanej
			BEGIN
				SELECT @finishedEventId = NULL, @finishedAlarmId = null
				EXEC dbo.ev_GenerateEvent
					@currentDateUtc			= @currentDateUtc
					,@EventDateUtc			= @EventDateUtc
					,@EventType				= @noGoZoneInEventType
					,@EventUnitId			= @NewUnitId
					,@EventObjectId			= @objectId
					,@EventObjectType		= @objectType
					,@EventObjectUnitId		= @objectUnitId
					,@EventData				= null
					,@IsLongEvent			= 1
					,@FinishedEventId		= @finishedEventId		OUTPUT
					,@FinishedAlarmId		= @finishedAlarmId		OUTPUT
					,@EventId				= @ongNoGoZoneInEventId	OUTPUT
				-- wygeneruj ostrzeżenia/alarmy
				EXEC dbo.ev_GenerateAlarmsForEvent 
					 @EventObjectType			= @OBJECT_TYPE_BAND
					,@EventObjectid				= @BandId
					,@currentDateUtc			= @currentDateUtc
					,@CurrentEventDateUtc		= @EventDateUtc
					,@currentEventId			= @ongNoGoZoneInEventId
					,@currentEventType			= @noGoZoneInEventType
					,@currentUnitId				= @NewUnitId
					,@objectId					= @objectId
					,@objectType				= @objectType
					,@objectUnitId				= @objectUnitId
					,@warningCloseMessage		= @warningAutoCloseMessage
					,@WarningActive				= @noGoInWarningActive		
					,@WarningDelayMins			= @noGoInWarningDelayMins	
					,@AlarmActive				= @noGoInAlarmActive			
					,@AlarmDelayMins			= @noGoInAlarmDelayMins		
					,@CurrrentAlarmSeverity		= @ongoingNoGoAlarmSeverity OUTPUT
					,@CurrrentAlarmId			= @ongNoGoZoneInAlarmId OUTPUT
					,@warningDateUtc			= @warningInNoGoZoneDateUtc OUTPUT 
					,@alarmDateUtc				= @alarmInNoGoZoneDateUtc	OUTPUT
					,@severity1Count			= @severity1Count OUTPUT
					,@severity2Count			= @severity2Count OUTPUT
			END

		END
		

		--- obliczenia
		SELECT
			@previousUnitId =  @currentUnitId, @inPreviousUnitUtc = @inCurrentUnitUtc,
			@currentUnitId = @NewUnitId, @inCurrentUnitUtc = @EventDateUtc
		
		DECLARE 
			@zoneOutImportance int
			,@noGoZoneInImportance int
			,@currentEventType dbo.EVENT_TYPE
			,@ongoingAlarmSeverity dbo.EVENT_SEVERITY
			,@warningDateUtc dateTime
			,@alarmDateUtc dateTime

		SELECT @zoneOutImportance = @ongoingZoneAlarmSeverity, @noGoZoneInImportance = @ongoingNoGoAlarmSeverity
		SELECT	@zoneOutImportance = @zoneOutImportance + 100 * dbo.ev_GetEventPriority(@zoneOutEventType)
			,@noGoZoneInImportance = @noGoZoneInImportance + 100 * dbo.ev_GetEventPriority(@noGoZoneInEventType)


		IF @zoneOutImportance > @noGoZoneInImportance
			SELECT @CurrentEventType = @zoneOutEventType, @ongoingAlarmSeverity = @ongoingZoneAlarmSeverity, @warningDateUtc = @warningOutZoneDateUtc,  @alarmDateUtc = @alarmOutZoneDateUtc
		ELSE 
			SELECT @CurrentEventType = @noGoZoneInEventType, @ongoingAlarmSeverity = @ongoingNoGoAlarmSeverity, @warningDateUtc = @warningInNoGoZoneDateUtc, @alarmDateUtc = @alarmInNoGoZoneDateUtc
		
	END
	

	DECLARE @currentEventId int
	IF @bandStatusChanged = 1
		EXEC dbo.ev_GenerateBandStatus
			 @BandId					= @BandId 
			,@CurrentUnitId 			= @CurrentUnitId 
			,@InCurrentUnitUtc			= @InCurrentUnitUtc
			,@PreviousUnitId 			= @PreviousUnitId 
			,@InPreviousUnitUtc 		= @InPreviousUnitUtc 
			,@TopAlarmId 				= @TopAlarmId 
			,@TopAlarmType  			= @TopAlarmType  
			,@TopAlarmSeverity 			= @TopAlarmSeverity 
			,@TopAlarmDateUtc 			= @TopAlarmDateUtc 
			,@TopEventDateUtc 			= @TopEventDateUtc 
			,@Severity1Count 			= @Severity1Count 
			,@Severity2Count 			= @Severity2Count 
			,@OngNoGoZoneInEventId 		= @OngNoGoZoneInEventId 
			,@OngNoGoZoneInAlarmId  	= @OngNoGoZoneInAlarmId  
			,@OngDisconnectedEventId  	= @OngDisconnectedEventId 
			,@OngDisconnectedAlarmId  	= @OngDisconnectedAlarmId 
			,@OngZoneOutEventId  		= @OngZoneOutEventId  
			,@OngZoneOutAlarmId  		= @OngZoneOutAlarmId  
			,@OngNoMovingEventId  		= @OngNoMovingEventId  
			,@OngNoMovingAlarmId  		= @OngNoMovingAlarmId  
			,@OngMovingEventId  		= @OngMovingEventId  
			,@OngMovingAlarmId  		= @OngMovingAlarmId  
			,@OngLowBatteryEventId  	= @OngLowBatteryEventId  
			,@OngLowBatteryAlarmId 		= @OngLowBatteryAlarmId 
			,@currentEventId			= @currentEventId
			,@CurrentEventType 			= @CurrentEventType 
			,@CurrentEventDateUtc 		= @EventDateUtc 
			,@GeneratedAlarmId			= @currentEventId
			,@GeneratedAlarmSeverity 	= @ongoingAlarmSeverity 
			,@GeneratedWarningDateUtc 	= @warningDateUtc
			,@GeneratedAlarmDateUtc 	= @alarmDateUtc 

END