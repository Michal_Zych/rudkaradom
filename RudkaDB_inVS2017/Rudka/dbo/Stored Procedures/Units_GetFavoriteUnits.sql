﻿CREATE  PROCEDURE [dbo].[Units_GetFavoriteUnits]
	@user_id int
AS
BEGIN
	SET NOCOUNT ON;
  
	create table #t ( id int )
	create table #r ( id int )
	
	insert into #t (id)
	select UnitID
	from FavoriteUnits
	where UserID = @user_id
		
	declare @f int
	
	while exists (select 1 from #t)
	begin
		select top(1) @f = id from #t
		
		insert into #r (id)
		select Id
		from dbo.GetParentsWithUnit(@f)
		where ID not in (select id from #r)
		
		delete from #t where id = @f
	end

    select u.Id, Name, UnitTypeId, ParentUnitId, ClientId, UnitTypeIDv2, PictureId, Description, dbo.unt_IsMonitored(u.ID) as IsMonitored
    from units u
		inner join #r on u.ID = #r.ID
		where active=1
END

