﻿

-- =============================================
-- Author:		
-- Create date: 
-- Description:	zwraca magazyny danego klienta
-- =============================================
CREATE PROCEDURE [dbo].[exp_GetMeasurements]
		@ClientID int		
		,@MaxRows int
		,@UnitID int
		,@DateFrom datetime
		,@DateTo datetime
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @Measurements TABLE(
		[Date] datetime
		,Value real)		

	INSERT @Measurements
	SELECT TOP(@MaxRows) m.MeasurementTime, m.Value * s.Scale AS Value
	FROM dbo.Measurements m WITH(NOLOCK)
	JOIN dbo.Sensors s WITH(NOLOCK) ON s.Id = m.SensorID
	JOIN dbo.Units u WITH(NOLOCK) ON s.Unitid = u.Id AND u.ClientID = @ClientID AND u.ID = @UnitID
	WHERE m.MeasurementTime BETWEEN @DateFrom AND @DateTo
	ORDER BY m.MeasurementTime

	DECLARE @rows int

	SELECT @rows = COUNT(1)
	FROM dbo.Measurements m WITH(NOLOCK)
	JOIN dbo.Sensors s WITH(NOLOCK) ON s.Id = m.SensorID
	JOIN dbo.Units u WITH(NOLOCK) ON s.Unitid = u.Id AND u.ClientID = @ClientID
	WHERE m.MeasurementTime BETWEEN @DateFrom AND @DateTo
	
	IF @MaxRows < @rows
		INSERT @Measurements VALUES(null, null)

	SELECT * FROM @Measurements
END


