﻿

-- =============================================
-- Author:		
-- Create date: 
-- Description:	zwraca magazyny danego klienta
-- =============================================
CREATE PROCEDURE [dbo].[exp_GetSensors]
		@ClientID int		
AS
BEGIN
	SET NOCOUNT ON;
		
	SELECT u.ID, s.MU, s.Calibration * s.Scale AS Calibration, s.Enabled, so.Name AS Socket,
		CASE WHEN AlDisabled = 1 
			OR (AlDisabledForHours = 1 AND (CONVERT(time, dbo.GetCurrentDate()) BETWEEN AlDisableHoursFrom1 AND AlDisableHoursTo1
											OR CONVERT(time, dbo.GetCurrentDate()) BETWEEN AlDisableHoursFrom2 AND AlDisableHoursTo2)
				)
			OR (AlDisabledForRanges = 1 AND (EXISTS (SELECT 1 FROM dbo.AlarmsOmittedRanges WHERE SensorId = s.Id AND dbo.GetCurrentDate() BETWEEN DateStart AND DateEnd)))
			THEN 0
			ELSE 1 END As AlarmingEnabled
			, s.Lorange * s.Scale AS LoRange, s.UpRange * s.Scale AS UpRange
	FROM dbo.Sensors s WITH(NOLOCK)
	JOIN dbo.Units u WITH(NOLOCK) ON u.Id = s.UnitId AND u.ClientID = @ClientID AND u.Active = 1 
	LEFT JOIN dbo.Sockets so WITH(NOLOCK) ON so.Id = s.SocketID
	ORDER BY u.ID
END


