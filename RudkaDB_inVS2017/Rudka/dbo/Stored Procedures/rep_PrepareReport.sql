﻿
CREATE  PROCEDURE [dbo].[rep_PrepareReport]
	@UserId int,
	@ReportId int,
	@ObjectId dbo.OBJECT_ID,
	@DateFrom datetime = null,
	@DateTo datetime = null,
	@HideCharts bit = null,
	@IsMono bit = 0,
	@HyperPermission bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @userReportId int
	
	SELECT @userReportId = ID from dbo.UsersReports where UserID = @UserId and ReportID = @ReportId
	
	IF COALESCE(@userReportId, 0) = 0
	BEGIN
		INSERT INTO [dbo].[UsersReports] (UserID, ReportID, DateFrom, DateTo, UnitId, HideCharts, Mono, HyperPermission)
		VALUES (@UserId, @ReportId, @DateFrom, @DateTo, @ObjectId, @HideCharts, @IsMono, 0)
        SET @userReportId = SCOPE_IDENTITY()
	END
	ELSE
		UPDATE dbo.UsersReports SET
			DateFrom = @DateFrom
			, DateTo = @DateTo
			, HideCharts = @HideCharts
			, Mono = @IsMono
			, HyperPermission = @HyperPermission
			, UnitID = @ObjectId
		WHERE ID = @userReportId


END

