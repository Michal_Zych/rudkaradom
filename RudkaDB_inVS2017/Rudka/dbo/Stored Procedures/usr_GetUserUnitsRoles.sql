﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usr_GetUserUnitsRoles]
	@userId int
AS
BEGIN
	SET NOCOUNT ON;
	

	SELECT UserID, UnitId, RoleId 
	FROM dbo.UsersUnitsRoles 
	WHERE UserID = @userId

END

