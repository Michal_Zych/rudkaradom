﻿
-- =============================================
-- Author:		
-- Create date: 
-- Description:	Odbiorcy powiadomień alarmowych dla danego unita sensorowego
-- =============================================
CREATE PROCEDURE [dbo].[unt_GetNotificationReceiversForUnit]
	@unitId int
AS
BEGIN
	SET NOCOUNT ON;


	DECLARE @sensorId smallint
	SELECT @sensorId = Id FROM dbo.Sensors WHERE UnitId = @unitId

	SELECT COALESCE(s.UserId, m.UserId) AS UserId
		, CAST((CASE WHEN s.UserId IS NULL THEN 0 ELSE 1 END) AS bit) AS Sms
		, CAST((CASE WHEN m.UserId IS NULL THEN 0 ELSE 1 END) AS bit) as Email
	FROM (
			SELECT UserId FROM dbo.AlarmNotifyBySms WHERE SensorID = @sensorId
		 ) s
		FULL OUTER JOIN (
			SELECT UserId FROM dbo.AlarmNotifyByEmail WHERE SensorId = @sensorId
		) m ON s.UserId = m.UserId
END
