﻿CREATE PROCEDURE [dbo].[ev_GenerateAlarmsForEvent](
	@EventObjectType dbo.OBJECT_TYPE
	,@EventObjectId smallint
	,@currentDateUtc dateTime
	,@CurrentEventDateUtc datetime
	,@currentEventId int
	,@currentEventType dbo.EVENT_TYPE
	,@currentUnitId int
	,@objectId dbo.OBJECT_ID
	,@objectType dbo.OBJECT_TYPE
	,@objectUnitId int

	,@warningCloseMessage nvarchar(max)

	,@WarningActive bit
	,@WarningDelayMins smallint
	,@AlarmActive bit
	,@AlarmDelayMins smallint

	,@CurrrentAlarmSeverity dbo.EVENT_SEVERITY OUTPUT
	,@CurrrentAlarmId int OUTPUT
	,@warningDateUtc dateTime OUTPUT
	,@alarmDateUtc dateTime OUTPUT
	,@severity1Count int OUTPUT
	,@severity2Count int OUTPUT
)
AS
BEGIN
	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
		BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'EventObjectId=' + COALESCE(CONVERT(nvarchar(100), @EventObjectId), 'NULL') 
		EXEC Log_Action @@PROCID, @info
	END
	/****************************************************/

	DECLARE @OBJECT_TYPE_BAND		dbo.OBJECT_TYPE
		,@OBJECT_TYPE_TRANSMITTER	dbo.OBJECT_TYPE
	SELECT @CurrrentAlarmSeverity = (SELECT severity.Event FROM enum.EventSeverity severity)
	
	SELECT @OBJECT_TYPE_BAND = Band, @OBJECT_TYPE_TRANSMITTER = Transmitter FROM enum.ObjectType


	--czas ostrzeżenia i alarmu
	SELECT @WarningActive = COALESCE(@WarningActive, 0), @AlarmActive = COALESCE(@AlarmActive, 0)
	IF @WarningActive = 0 OR @WarningDelayMins < 0 
		OR (@AlarmActive = 1 AND @WarningDelayMins >= @AlarmDelayMins) SET @WarningDelayMins = NULL
	IF @AlarmActive = 0  OR @AlarmDelayMins < 0 SET @AlarmDelayMins	= NULL

	SELECT @warningDateUtc = DATEADD(MINUTE, @WarningDelayMins, @CurrentEventDateUtc)
		,@alarmDateUtc = DATEADD(MINUTE, @AlarmDelayMins, @CurrentEventDateUtc)


	-- ostrzeżenie
	IF @warningDateUtc <= @currentDateUtc
	BEGIN -- zapisz ostrzeżenie
		INSERT ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc)
		SELECT @warningDateUtc, @currentEventType, eventSeverity.Warning, @currentUnitId, @objectId, @objectType, @objectUnitId, @currentDateUtc
		FROM enum.EventSeverity eventSeverity
		SET @CurrrentAlarmId = SCOPE_IDENTITY()

		INSERT ev_Alarms(EventId, RaisingEventId) VALUES(@CurrrentAlarmId, @currentEventId)
		SELECT @severity1Count = @severity1Count + 1
			,@CurrrentAlarmSeverity = severtity.Warning
		FROM enum.EventSeverity severtity
	END
	IF @warningDateUtc > @currentDateUtc
	BEGIN -- zapisz ostrzeżenie do wyzwolenia w przyszłości
		INSERT ev_EventsToRaise(RaisingObjectType, RaisingObjectId, RaisingEventId, RaisingEventDateUtc, RaiseDateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId)
		SELECT  @EventObjectType, @EventObjectId, @currentEventId, @CurrentEventDateUtc, @warningDateUtc, @currentEventType, eventSeverity.Warning, @currentUnitId, @objectId, @objectType, @objectUnitId
		FROM enum.EventSeverity eventSeverity
		-- ostrzeżenie przełożone do generowania w przyszłości
		SET @warningDateUtc = NULL
	END
	-- alarm
	IF @alarmDateUtc <= @currentDateUtc
	BEGIN -- zapisz alarm
		DECLARE @tempId int = @CurrrentAlarmId
		INSERT ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc)
		SELECT @alarmDateUtc, @currentEventType, eventSeverity.Alarm, @currentUnitId, @objectId, @objectType, @objectUnitId, @currentDateUtc
		FROM enum.EventSeverity eventSeverity
		SET @CurrrentAlarmId = SCOPE_IDENTITY()

		INSERT ev_Alarms(EventId, RaisingEventId) VALUES(@CurrrentAlarmId, @currentEventId)
		UPDATE ev_Events SET
			EndDateUtc = @alarmDateUtc
			,EndingEventId = @CurrrentAlarmId
		WHERE Id = @tempId
		SELECT @severity2Count = @severity2Count + 1	
			,@CurrrentAlarmSeverity = severtity.Alarm
		FROM enum.EventSeverity severtity

		UPDATE ev_Alarms SET
			ClosingAlarmId = @CurrrentAlarmId
			,ClosingComment = @WarningCloseMessage
			,ClosingDateUtc = @alarmDateUtc
			,IsClosed = 1
		WHERE EventId = @tempId AND IsClosed = 0
		SELECT @Severity1Count = @Severity1Count - @@ROWCOUNT

	END
	IF @alarmDateUtc > @currentDateUtc
	BEGIN -- zapisz alarm do wyzwolenia w przyszłości
		INSERT ev_EventsToRaise(RaisingObjectType, RaisingObjectId, RaisingEventId, RaisingEventDateUtc, RaiseDateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId)
		SELECT @EventObjectType, @EventObjectId, @currentEventId, @CurrentEventDateUtc, @alarmDateUtc, @currentEventType, eventSeverity.Alarm, @currentUnitId, @objectId, @objectType, @objectUnitId
		FROM enum.EventSeverity eventSeverity
		--alarm przełożony do wygenerowania w przyszłości
		SET @alarmDateUtc = null
	END

END
