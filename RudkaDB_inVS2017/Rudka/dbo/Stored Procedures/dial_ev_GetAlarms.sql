﻿
/****** Object:  StoredProcedure [dbo].[mb_ev_GetPatientList]    Script Date: 2018-02-08 14:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		kt
-- Create date: 2018-02-08
-- Description:	gets alarms for dial service
-- =============================================
CREATE PROCEDURE [dbo].[dial_ev_GetAlarms]
	@from datetime
AS
BEGIN

	SET NOCOUNT ON;

    SELECT
		a.EventId,
		o.DateUtc,
		o.Message,
		od.SequenceNr,
		od.PhoneNr
	FROM
		dbo.ev_Alarms a 
		INNER JOIN dbo.ev_CallOrders o ON a.EventId = o.EventId
		INNER JOIN dbo.ev_CallOrdersDetails od ON o.EventId = od.EventId
	WHERE
		a.isClosed = 0
		AND o.DateUtc > @from
	
END