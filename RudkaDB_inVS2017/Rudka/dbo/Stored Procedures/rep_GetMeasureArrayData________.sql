﻿CREATE PROCEDURE [dbo].[rep_GetMeasureArrayData________]
 @UserReportID int
AS
BEGIN
 SET NOCOUNT ON;
 
	DECLARE @dateFrom datetime
    DECLARE @dateTo datetime

    SELECT @dateFrom = DateFrom, @dateTo = DateTo FROM UsersReports WHERE ID = @UserReportID 

    SELECT m.SensorID, m.MeasurementTime, m.Value 
    FROM dbo.Measurements m WITH(NOLOCK)
    JOIN dbo.UserReportSensors urs WITH(NOLOCK) ON urs.SensorID = m.SensorID
    WHERE urs.UserReportID = @UserReportID AND MeasurementTime BETWEEN @dateFrom AND @dateTo
		AND Value IS NOT NULL
    ORDER BY m.SensorID, m.MeasurementTime ASC
END

