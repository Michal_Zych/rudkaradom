﻿CREATE PROCEDURE dbo.GetUnitAlarmConfig
(
	@UnitId int = 1
)
AS
BEGIN	
	DECLARE
		@RECORD_PATIENT tinyint = 4
		,@RECORD_UNIT tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_UPDATE tinyint = 2

	SELECT UnitId
      ,IsSOScalling
      ,IsACCactive
      ,DMoveWrActive
      ,DMoveWrMins
      ,DMoveAlActive
      ,DMoveAlMins
      ,DNoMoveWrActive
      ,DNoMoveWrMins
      ,DNoMoveAlActive
      ,DNoMoveAlMins
      ,NMoveWrActive
      ,NMoveWrMins
      ,NMoveAlActive
      ,NMoveAlMins
      ,NNoMoveWrActive
      ,NNoMoveWrMins
      ,NNoMoveAlActive
      ,NNoMoveAlMins
      ,OutOfZoneWrActive
      ,OutOfZoneWrMins
      ,OutOfZoneAlActive
      ,OutOfZoneAlMins
      ,InNoGoZoneMins
      ,NoConnectWrActive
      ,NoConnectWrMins
      ,NoConnectAlActive
      ,NoConnectAlMins
	  ,NoTransmitterWrActive 
	  ,NoTransmitterWrMins 
      ,NoTransmitterAlActive 
      ,NoTransmitterAlMins 
	FROM dbo.al_ConfigUnits
	WHERE UnitId = @UnitId

	SELECT Phone, Description
	FROM dbo.al_CallingGroups
	WHERE Id = @UnitId AND ObjectType = @RECORD_UNIT

	SELECT UnitId, Name, dbo.GetUnitLocationReversed(UnitId, 1, 1, default) AS Location
	FROM dbo.al_NoGoZones z
	JOIN dbo.Units u ON z.UnitId = u.ID
	WHERE z.Id = @UnitId AND z.ObjectType = @RECORD_UNIT
END