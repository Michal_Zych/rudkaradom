﻿-- =============================================
-- Author:		kt
-- Create date: 2018-02-08
-- Description:	gets alarms for dial service
-- =============================================
CREATE PROCEDURE [dbo].[dial_ev_GetAlarms]
AS
BEGIN

	SET NOCOUNT ON;

    SELECT
		a.EventId as Id,
		o.DateUtc as Time,
		o.Message,
		d.SequenceNr as SequenceNo,
		d.PhoneNr as Number,
		CASE WHEN EXISTS(SELECT TOP 1 EventId 
						 FROM dbo.ev_CallOrdersLog 
						 WHERE EventId = a.EventId)
			 THEN 1
			 ELSE 0
		END as Status
	FROM
		dbo.ev_Alarms a 
		INNER JOIN dbo.ev_CallOrders o ON a.EventId = o.EventId
		INNER JOIN dbo.ev_CallOrdersDetails d ON a.EventId = d.EventId
		LEFT JOIN dbo.ev_CallOrdersLog od ON o.EventId = od.EventId
	WHERE
		a.isClosed = 0
	
END