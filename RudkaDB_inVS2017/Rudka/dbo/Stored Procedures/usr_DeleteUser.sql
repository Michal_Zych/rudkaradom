﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	Dezaktywacja użytkownika
-- =============================================
CREATE PROCEDURE [dbo].[usr_DeleteUser]
	@userId int
	, @operatorId int
	, @reason nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @OPERATION_DELETE tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_EDIT tinyint = 2

		,@RECORD_UNIT tinyint = 0
		,@RECORD_USER tinyint = 1

		--kody błędów
	DECLARE
		@ERROR int = -1
		,@operationDateUtc dateTime = dbo.GetCurrentUtcDate()
		,@eventId int

	BEGIN TRANSACTION
		BEGIN TRY
			UPDATE dbo.Users 
			SET
				Active = 0
			WHERE ID = @userId

			DELETE dbo.AlarmNotifyByEmail WHERE UserId = @userId
			DELETE dbo.AlarmNotifyBySms WHERE UserId = @userId

			INSERT dbo.Logs(OperationDate, OperatorId, OperationType, ObjectType, ObjectId, Reason, Info)
					VALUES(dbo.GetCurrentDate(), @operatorId, @OPERATION_DELETE, @RECORD_USER, @userId, @reason, null)
			
			DECLARE @json nvarchar(max) = 'SELECT Id, Login, Name, LastName, Active FROM dbo.Users WHERE Id = ' + CONVERT(nvarchar(10), @userId)
			EXEC @json = dbo.SerializeJSON @json

			INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason)
			SELECT @operationDateUtc, eventType.[Delete], eventSeverity.[Event], null, @userId, objectType.[User], null, @OperationDateUtc, @OperationDateUtc, @operatorId, @json, @reason 
			FROM enum.EventType eventType				
			JOIN enum.EventSeverity eventSeverity ON 1 = 1
			JOIN enum.ObjectType objectType ON 1 = 1
			SELECT @eventId = SCOPE_IDENTITY()
			
			UPDATE ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
		
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			SELECT @ERROR
			RETURN
		END CATCH
	COMMIT TRANSACTION

	SELECT @userId
END


