﻿CREATE PROCEDURE [dbo].[test_AddTest]
	@sensorId int
	,@value smallint = null
	,@eventType smallint = 0
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @DEFAULT_SCALE real;
	SET @DEFAULT_SCALE = 0.1;

	DECLARE @testActive bit;

	SELECT @testActive = COUNT(1) 
	FROM dbo.SensorTests 
	WHERE SensorId = @sensorId AND Active = 1

	DECLARE @scale real;
	DECLARE @calibration smallint;

	SELECT @scale = Scale, @calibration = Calibration
	FROM dbo.Sensors
	WHERE Id = @sensorId;

	IF (@value IS NOT NULL)
		SET @value = round(@value / COALESCE(@scale, @DEFAULT_SCALE), 1) - @calibration; 

	IF (@testActive = 1)
	BEGIN
		exec dbo.test_RemoveTest @sensorId;
		SELECT 0;
	END

	INSERT INTO dbo.SensorTests (SensorId, Value, EventType, Active, DateStarted)
	VALUES (@sensorId, @value, @eventType, 1, dbo.GetCUrrentDate())
	SELECT 1;
	
END

