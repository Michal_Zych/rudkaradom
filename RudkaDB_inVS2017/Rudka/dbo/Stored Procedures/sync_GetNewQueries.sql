﻿CREATE PROCEDURE [dbo].[sync_GetNewQueries]
	@LastRequestId bigint,
	@Limit bigint
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @MAX_RECORDS_COUNT int = 100
		,@REPLY_SENDING_AFTER_SEC int =1800		-- ponawiaj próbę wysyłki co pół godziny

	SET @Limit = COALESCE(@Limit, @MAX_RECORDS_COUNT)
	IF @Limit > @MAX_RECORDS_COUNT SET @Limit = @MAX_RECORDS_COUNT

	DECLARE
		@currentTimeUtc dateTime = dbo.GetCurrentUtcDate();

	DECLARE @toReply TABLE (
		Id int
		,EventId int
		,RecType char(4)
		,DateUtc dateTime
		,Record nvarchar(max)
		,ParentId int
		,Try int)


BEGIN -- przygotuj rekordy do ponownego wysyłania
	INSERT @toReply
	SELECT Id, EventId, RecType, DateUtc, Record, COALESCE(ParentId, Id), Try
	FROM tech.sync_RecordsOUT
	WHERE Error IS NOT NULL
		AND DATEDIFF(SECOND, SentDateUtc, @currentTimeUtc) > @REPLY_SENDING_AFTER_SEC
		AND TryAgain = 1

	BEGIN TRANSACTION
		UPDATE tech.sync_RecordsOUT SET TryAgain = 0 WHERE Id IN (SELECT Id FROM @toReply)

		INSERT tech.sync_RecordsOUT(EventId, RecType, DateUtc, Record, ParentID, Try, TryAgain)
		SELECT EventId, RecType, DateUtc, Record, ParentId, COALESCE(Try, 0), 0
		FROM @toReply

	COMMIT TRANSACTION


END
	SELECT TOP (@Limit)
		Id, RTRIM(RecType) AS RecType, Record
	FROM tech.sync_RecordsOUT
	WHERE SentDateUtc IS NULL
	ORDER BY DateUtc
END