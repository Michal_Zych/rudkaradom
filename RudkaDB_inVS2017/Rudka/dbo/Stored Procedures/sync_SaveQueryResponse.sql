﻿CREATE PROCEDURE [dbo].[sync_SaveQueryResponse]
	@RequestId nvarchar(max),
	@Status nvarchar(max),
	@Record nvarchar(max)
AS
BEGIN
	INSERT _log(LogDate, ProcedureName, Info)
	values (dbo.GetCurrentDate(), 'sync_SaveQueryResponse', @RequestId)


	SET NOCOUNT ON;

	DECLARE @OK_STATUS nvarchar(10) = '1'


	UPDATE tech.sync_RecordsOUT SET
		Try = 1
		,TryAgain = CASE WHEN @Status = @OK_STATUS THEN 0 ELSE 1 END
		,SentDateUtc = dbo.GetCurrentUTCDate()
		,Error = CASE WHEN @Status = @OK_STATUS THEN NULL ELSE @Record END
	WHERE Id = @RequestId

	IF @status = @OK_STATUS
	BEGIN
	DECLARE 
		@requestType char(4)
		,@requestRecordId nvarchar(100)
		,@recordId int

		SELECT @requestType = r.RecType, @requestRecordId = e.EventRecordId
		FROM tech.sync_RecordsOUT r
		LEFT JOIN tech.sync_EventsIN e ON r.EventId = e.Id
		WHERE r.Id = @RequestId

		
		INSERT tech.sync_RecordsIN (OutRecordId, RecType, RecId, Record, Processed, StoreDateUtc) VALUES
		(@RequestId, @requestType,  @requestRecordId, @Record, 0, dbo.GetCurrentUTCDate())
		SELECT @recordId = SCOPE_IDENTITY()

		EXEC dbo.sync_ProcessRecord @recordId, @requestType
	END
END