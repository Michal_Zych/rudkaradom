﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	Edycja telefonu użytkownika o podanym ID
-- Założenie - telefon nie podlega synhronizacji
-- jeśli to się zmieni przerobić procedurę aby wywoływała usr_EditUser
-- =============================================
CREATE PROCEDURE [dbo].[usr_EditUserPhone]
(
	  @id int
	, @phone USER_PHONE
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @OPERATION_DELETE tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_EDIT tinyint = 2

		,@RECORD_UNIT tinyint = 0
		,@RECORD_USER tinyint = 1

		--kody błędów
	DECLARE
		@ERROR int = -1
		,@oldPhone nvarchar(128)

	BEGIN TRANSACTION
		BEGIN TRY
			SELECT @oldPhone = Phone FROM dbo.Users WHERE Id = @id

			UPDATE dbo.Users 
			SET Phone = @phone
			WHERE ID = @id

			INSERT dbo.Logs(OperationDate, OperatorId, OperationType, ObjectType, ObjectId, Reason, Info)
					VALUES(dbo.GetCurrentDate(), @id, @OPERATION_EDIT, @RECORD_USER, @id, '', 'Telefon z:' + COALESCE(@oldPhone, '') + ' na: ' + COALESCE(@phone, ''))

		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			SELECT @ERROR
			RETURN
		END CATCH
	COMMIT TRANSACTION

	SELECT @Id
END
