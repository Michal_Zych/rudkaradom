﻿CREATE PROCEDURE fda_GetServiceStatusFiltered
@PageNum int
,@PageSize int

,@TreeUnitIds nvarchar(max),
@ServiceName nvarchar(200) = null
,@Description nvarchar(200) = null
,@StartTimeUtcFrom datetime = null
,@StartTimeUtcTo datetime = null
,@PingTimeUtcFrom datetime = null
,@PingTimeUtcTo datetime = null
,@PreviousPingTimeUtcFrom datetime = null
,@PreviousPingTimeUtcTo datetime = null
,@HasPartnerFrom bit = null
,@HasPartnerTo bit = null
,@PartnerPingTimeUtcFrom datetime = null
,@PartnerPingTimeUtcTo datetime = null
,@InfoTxt nvarchar(200) = null
,@SortOrder nvarchar(100) = null
AS
BEGIN

 DECLARE @cmd nvarchar(max)
 SET @cmd = 'WITH orn AS ( SELECT ROW_NUMBER() OVER ('
		+ dbo.dsql_GetServiceStatus_OrderBY(@SortOrder) + ') AS RowNum, '
		+ dbo.dsql_GetServiceStatus_Select()
		+ dbo.dsql_GetServiceStatus_From()
		+ dbo.dsql_GetServiceStatus_Where(@TreeUnitIds,
			@ServiceName, @Description, @StartTimeUtcFrom, @StartTimeUtcTo, @PingTimeUtcFrom, @PingTimeUtcTo
			, @PreviousPingTimeUtcFrom, @PreviousPingTimeUtcTo, @HasPartnerFrom, @HasPartnerTo, @PartnerPingTimeUtcFrom, @PartnerPingTimeUtcTo
			, @InfoTxt)
 + ')
		SELECT *
		FROM orn
		WHERE RowNum BETWEEN ' + CONVERT(nvarchar(20), @PageNum * @PageSize +1) + ' AND '
		+ CONVERT(nvarchar(20), (@PageNum + 1) * @PageSize) + ' ORDER BY rowNum'

 EXEC(@cmd)
-- print @cmd
END