﻿CREATE PROCEDURE [dbo].[User_GetDescendantsAndSiblingsProc] 
	@userId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT *
	FROM User_GetDescendantsAndSiblings(@userId)
END

