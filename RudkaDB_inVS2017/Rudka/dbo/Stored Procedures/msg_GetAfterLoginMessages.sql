﻿


CREATE PROCEDURE [dbo].[msg_GetAfterLoginMessages] @userId INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE
		  @INSTANT_MESSAGE tinyint	= 0
		, @LOGIN_MESSAGE tinyint	= 1

	DECLARE @msg TABLE(ID int, Message nvarchar(MAX));

	DECLARE
		@currentDate dateTime = dbo.GetCurrentDate()


	INSERT @msg
	SELECT Id, Message 
	FROM dbo.Messages 
	WHERE ClientId = (SELECT TOP(1) ClientID FROM dbo.Users WHERE ID = @userId)
		AND Mode = @LOGIN_MESSAGE
		AND @currentDate BETWEEN ActiveFrom AND ActiveTo

	UPDATE dbo.MessagesLog
	SET Date = @currentDate, Count = Count + 1
	WHERE MessageID IN (SELECT Id FROM @msg)
		AND ReceiverID = @userId

	INSERT dbo.MessagesLog 
	SELECT ID, @userId, @currentDate, 1
	FROM @msg
	WHERE NOT EXISTS  (SELECT 1 FROM dbo.MessagesLog WHERE MessageID = ID AND ReceiverID = @userId)

	SELECT Message FROM @msg
END
