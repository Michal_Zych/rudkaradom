﻿

CREATE PROCEDURE [dbo].[unt_GetPreviewsForUnit__old]
	@unitId int
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @rows int

	DECLARE @previews TABLE (
		  ID int
		, Name nvarchar(256)
		, ParentUnitID int
		, UnitType int
		
		, PreviewTLX real
		, PreviewTLY real
		, PreviewBRX real
		, PreviewBRY real
		
		, Location nvarchar(1024)
		, UnitTypeV2 int
		, PictureId int
		, MeasureUnit nvarchar(5)
		, Value float
		, LoRange float
		, UpRange float
		, MeasureTime datetime)

	INSERT @previews
	SELECT
		u.ID,
		u.Name,
		u.ParentUnitID,
		u.UnitTypeID,
		
		pu.TLX,
		pu.TLY,
		pu.BRX,
		pu.BRY,
		
		dbo.GetUnitLocation(u.id,1, 0, DEFAULT),
		u.UnitTypeIDv2,
		u.PictureId,
		s.MU,
		mc.Value * s.Scale,
		s.LoRange * s.Scale,
		s.UpRange * s.Scale,
		mc.MeasurementTime
	FROM
		dbo.PreviewsUnits pu
		JOIN dbo.Units u WITH(NOLOCK) ON u.id = pu.UnitID
		LEFT JOIN dbo.Sensors s WITH(NOLOCK) ON s.UnitId = u.Id
		LEFT JOIN dbo.MeasurementsCurrent mc ON mc.SensorId = s.Id
	WHERE pu.PreviewedUnitId = @unitID

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT @previews
		SELECT
			u.ID,
			u.Name,  
			u.ParentUnitID,
			u.UnitTypeID,
			0, 0, 0, 0,
			dbo.GetUnitLocation(u.id,1, 0, DEFAULT),
			u.UnitTypeIDv2,
			u.PictureId,
			s.MU,
			mc.Value * s.Scale,
			s.LoRange * s.Scale,
			s.UpRange * s.Scale,
			mc.MeasurementTime
		FROM dbo.Units u
		LEFT JOIN dbo.Sensors s ON s.UnitId = u.Id
		LEFT JOIN dbo.MeasurementsCurrent mc ON mc.SensorId = s.Id
		WHERE u.ID IN (
						SELECT UnitId
						FROM dbo.Sensors 
						WHERE UnitId IN (SELECT ID FROM Units WHERE ID = @unitId OR ParentUnitID = @unitId)
					  )
		SELECT @rows = @@ROWCOUNT

		DECLARE @s real = 0.2	--stosunek przerwy do wysokoćci
			,@H real			--wysokość podglądu wskaźników
			SET @H = 1 / (@rows * (1 + @s) + @s)


		UPDATE @previews
			SET PreviewTLX = @H * @s * Number + @H * (Number - 1), PreviewTLY = 0.1, 
				PreviewBRX = @H * (1 + @s) * Number, PreviewBRY = 0.9
		FROM @previews P
		INNER JOIN 
		(SELECT ID, ROW_NUMBER() OVER(ORDER BY ID) AS Number
			FROM @previews) A ON P.ID = A.ID
	END

	SELECT * FROM @previews
END

