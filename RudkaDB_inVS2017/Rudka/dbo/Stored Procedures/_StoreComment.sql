﻿
CREATE PROCEDURE [dbo].[_StoreComment]
	@comment nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @eventDateUtc datetime = dbo.GetCurrentUtcDate()

/****************************************************/
	DECLARE @info nvarchar(max)
	SET @info = 'comment=' + COALESCE(@comment, 'NULL') 
				+ ';EventDateUtc=' + COALESCE(CONVERT(nvarchar(50), @EventDateUtc, 121), 'NULL')
	EXEC Log_Action @@PROCID, @info
/****************************************************/
END