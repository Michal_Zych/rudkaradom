﻿-- =============================================
-- Author:		kt
-- Create date: 2017-12-21
-- Description:	gets count of new events
-- =============================================
CREATE PROCEDURE [dbo].[mb_ev_GetNewEventsCount]
	@userId int,
	@lastId int
AS
BEGIN
	SET NOCOUNT ON;

DECLARE @userUnits TABLE(id int);

INSERT INTO @userUnits SELECT id FROM Units_GetUserUnits_Func(@userId)

DECLARE @events TABLE(id int);

INSERT INTO
	@events
SELECT
	e.id
FROM
	dbo.ev_events e 
	INNER JOIN enum.ObjectType ot ON 1 = 1
	inner join dbo.ev_BandStatus bs on e.ObjectId = bs.ObjectId and bs.ObjectType = ot.Patient
	inner join dbo.Patients p on p.id = bs.ObjectId
	inner join dbo.units u on p.RoomId = u.id 
	inner join @userUnits uu on u.id = uu.id
where
	e.id > @lastId

DECLARE @alarmCount int;
DECLARE @userAlarmCount int;

SELECT @alarmCount = count(id) 
FROM @events e 
INNER JOIN ev_Alarms ea ON e.id = ea.EventId

SELECT @userAlarmCount = count(id)
FROM @events e 
INNER JOIN ev_UsersAlarms eua ON e.id = eua.EventId

SELECT @alarmCount + @userAlarmCount AS [count]

END