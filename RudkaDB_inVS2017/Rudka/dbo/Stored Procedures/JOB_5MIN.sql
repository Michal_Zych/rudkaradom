﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[JOB_5MIN] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE 
	FROM dbo.dev_TransmitterSenses
	WHERE MeasureDate < DATEADD(DAY, -14, dbo.GetCurrentDate())

	DELETE 
	FROM dbo.dev_TransmitterStatuses
	WHERE MeasureDate < DATEADD(MONTH, -1, dbo.GetCurrentDate())

END