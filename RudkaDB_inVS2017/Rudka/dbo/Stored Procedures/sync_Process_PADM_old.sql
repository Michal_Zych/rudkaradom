﻿CREATE PROCEDURE [dbo].[sync_Process_PADM_old]
	@RecordId int
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @ASSECO_OPERATOR_ID int = 0
		,@REASON nvarchar(100) = 'Źródło AMMS INRecordId=' + CONVERT(nvarchar(10), @RecordId)
		,@OperationDateUtc dateTime = dbo.GetCurrentUtcDate()

	DECLARE
		@record nvarchar(max)
		,@recId nvarchar(100)
		,@oldRecord nvarchar(max)
		,@m2mId int
		,@eventId int

	SELECT @recId = RecId, @record = Record FROM tech.sync_RecordsIN 
	WHERE Id = @RecordId

	/*
	SELECT @recId = r.RecId, @record = r.Record, @oldRecord = ep.ExtDescription, @m2mId = ep.M2mId
	FROM tech.sync_RecordsIN r
	LEFT JOIN arch.ext_Patients ep ON ep.ExtId = r.RecId 
	WHERE r.Id = @RecordId
	*/
	DECLARE
		@name nvarchar(100)
		,@lastName nvarchar(100)
		,@pesel char(11)
		,@processed bit = 1
		,@error nvarchar(max) 
	

	DECLARE @result TABLE(Id int, ErrorCode int, ErrorMessage nvarchar(max))

	BEGIN TRANSACTION
		IF COALESCE(@record, '') <> COALESCE(@oldRecord, '')
		BEGIN
			BEGIN--pobranie danych przesłanych z serwisu
				SET @name = LEFT(dbo.srv_KeyValue_GetValue(@Record, 'NAME'), 100) 
				SET @lastName = LEFT(dbo.srv_KeyValue_GetValue(@Record, 'LAST_NAME'), 100)
				SET @pesel = LEFT(dbo.srv_KeyValue_GetValue(@Record, 'PESEL'), 11)
			END

			IF @m2mId IS NULL
			BEGIN  --nowy pacjent
					INSERT dbo.Patients (Name, LastName, Pesel, HospitalDateUtc) VALUES (@name, @lastName, @PESEL, @OperationDateUtc)
					SELECT @m2mId = SCOPE_IDENTITY()

					DECLARE @json nvarchar(max) = 'SELECT Id, Name, LastName, Pesel FROM dbo.Patients WHERE Id = ' + CONVERT(nvarchar(10), @m2mId)
					EXEC @json = dbo.SerializeJSON @json

					INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason)
					SELECT @OperationDateUtc, eventType.[Create], eventSeverity.[Event], null, @m2mId, objectType.Patient, null, @OperationDateUtc, @OperationDateUtc, @ASSECO_OPERATOR_ID, @json, @REASON 
					FROM enum.EventType eventType				
					JOIN enum.EventSeverity eventSeverity ON 1 = 1
					JOIN enum.ObjectType objectType ON 1 = 1
				
					SELECT @eventId = SCOPE_IDENTITY()
					UPDATE ev_Events SET EndingEventId = @eventId WHERE Id = @eventId


					INSERT arch.Patients (Id, Name, LastName, Pesel) VALUES (@m2mId, @name, @lastName, @pesel)

					DELETE dbo.Patients WHERE Id = @m2mId
					
					--INSERT arch.ext_Patients(ExtId, M2mId, ExtDescription) VALUES (@recId, @m2mId, @record)
			END		
			ELSE -- edycja pacjenta
			BEGIN
				INSERT @result
				EXEC dbo.ev_PatientEdit @m2mId, @name, @lastName, @pesel, @ASSECO_OPERATOR_ID, @REASON

				SELECT @error = ErrorMessage FROM @result
				IF @error IS NOT NULL 
					SELECT @processed = 0
				/*ELSE
					UPDATE arch.ext_Patients SET 
						ExtDescription = @record
					WHERE ExtId = @recId*/
			END
		END
		UPDATE tech.sync_RecordsIN SET
			Processed = @processed
			,ProcessedDateUtc = @OperationDateUtc
			,Error = @error
			,Try = COALESCE([Try], 0) + 1
		WHERE Id = @RecordId
	COMMIT TRANSACTION

END