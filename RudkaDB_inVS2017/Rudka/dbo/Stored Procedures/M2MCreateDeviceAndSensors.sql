﻿CREATE PROCEDURE [dbo].[M2MCreateDeviceAndSensors]
 @hubId smallint,
 @slaveAddress smallint,
 @deviceModel smallint,
 @socketNames nvarchar(100),
 @parentUnitId int

AS
BEGIN
BEGIN TRANSACTION
  BEGIN TRY
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 declare @maxChannels int=(select top(1) count(value) from fn_Split(@socketNames,',') )

 INSERT INTO [dbo].[Devices]
           ([Address]
           ,[MaxChannelCount]
           ,[DeviceModel]
           ,[HubId])
     VALUES(@slaveAddress,@maxChannels,@deviceModel,@hubId)

 Declare @deviceid int =scope_identity()
    declare @channel int=0

 DECLARE @socketName varchar (50)
 Declare @socketId int 
 declare @tab table(unitid int)
 Declare @UnitId int

declare kur SCROLL cursor for 

select value as name from fn_Split(@socketNames,',')

OPEN kur;
FETCH NEXT FROM kur INTO @socketName;
WHILE @@FETCH_STATUS=0
   BEGIN

   delete from @tab
   insert into @tab exec [unt_UnitCreate] @socketName,@parentUnitId,1,20,null,null,1,null,null

   set @UnitId =(select top(1) unitid from @tab)

 if(@unitid<0)
 begin
  select 'Nie można utworzyć unita'+@socketName
  return 
 end

   INSERT INTO [dbo].[Sockets]
           ([Name]
           ,[LoRange]
           ,[HiRange]
           ,[LoRangeH]
           ,[HiRangeH]
           ,[DeviceId]
           ,[DeviceChannelNumber]
     ,[Broken]
           )
     VALUES
  (
  'GN '+convert(nvarchar(50),@deviceid)+' '+ @socketName,-2000,500,-2000,500,@deviceid,@channel,0
  )

  set @socketid=scope_identity()

  update sensors set SocketID=@socketid,[Enabled]=1,scale=0.1 where UnitId=@UnitId

  set @channel+=1

       FETCH NEXT FROM kur INTO @socketName;
    END
CLOSE kur  
DEALLOCATE kur

 END TRY
  BEGIN CATCH
   ROLLBACK TRANSACTION
   SELECT
    ERROR_NUMBER() AS ErrorNumber
    ,ERROR_SEVERITY() AS ErrorSeverity
    ,ERROR_STATE() AS ErrorState
    ,ERROR_PROCEDURE() AS ErrorProcedure
    ,ERROR_LINE() AS ErrorLine
    ,ERROR_MESSAGE() AS ErrorMessage;
   RETURN
  END CATCH

 COMMIT TRANSACTION
END