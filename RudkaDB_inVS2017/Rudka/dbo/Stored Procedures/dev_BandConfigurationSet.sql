﻿CREATE PROCEDURE [dbo].[dev_BandConfigurationSet]
	@BandId dbo.OBJECT_ID = null -- insert
	,@Mac bigint
	,@Uid bigint
	,@BarCode nvarchar(100)
	,@SerialNumber nvarchar(50)
	,@TransmissionPower smallint
	,@AccPercent tinyint
	,@SensitivityPercent tinyint
	,@BroadcastingIntervalMs int
	,@BatteryTypeId tinyint
	,@BatteryInstallationDateUtc dateTime
	,@Description nvarchar(max)
	,@Parameters nvarchar(max)
	,@UnitId int

	,@OperatorId int
	,@Reason nvarchar(max) = null
AS
BEGIN
	SET NOCOUNT OFF;
	DECLARE @UPDATE_ENDING_EVENT_ID bit = 1
		,@DEF_CLIENT_ID int = 1
		,@BAND_DEVICE_TYPEID int = 1
		,@BANDS_LAYER_NAME nvarchar(50) = 'test'

	DECLARE
		 @SEPARATOR char(1) = ','
		,@SEPARATOR_ESCAPE nvarchar(10) = '&comma;'
		,@MAX_LEN int = 1024
		,@NEW_LINE nvarchar(2) = CHAR(10)

	DECLARE @params TABLE(Position int identity(0,1), Text nvarchar(1024))

	DECLARE
		@EVENT_UPDATE dbo.EVENT_TYPE
		,@EVENT_CREATE dbo.EVENT_TYPE
		,@OBJECT_TYPE_BAND dbo.OBJECT_TYPE
		,@SEVERITY_EVENT dbo.EVENT_SEVERITY
		,@eventType dbo.EVENT_TYPE
		,@currentDate datetime
		,@MacStr nvarchar(20)
		



	SET @currentDate = dbo.GetCurrentUtcDate()
	IF @BatteryInstallationDateUtc IS NULL SET @BatteryInstallationDateUtc = @currentDate

	SELECT @EVENT_CREATE = [Create], @EVENT_UPDATE = [Update] FROM enum.EventType
	SELECT @OBJECT_TYPE_BAND = Band FROM enum.ObjectType
	SELECT @SEVERITY_EVENT = [Event] FROM enum.EventSeverity

	IF @BandId IS NULL SET @eventType = @EVENT_CREATE
	ELSE SET @eventType = @EVENT_UPDATE

	
	BEGIN --żeby nie zajmować autonumerów, sprawdzenie duplikatów przed transakcją
	DECLARE 
		@errors TABLE(Id smallint, Mac bigint, MacStr nvarchar(20), Uid bigint, BarCode nvarchar(100))
	DECLARE @errorStr nvarchar(max) = ''
	INSERT @errors
	SELECT Id, Mac, MacString, Uid, BarCode
	FROM dbo.Bands 
	WHERE (Mac = @Mac OR Uid = @Uid OR BarCode = @BarCode)
		AND (Id <> @BandId OR @BandId IS NULL)
	SELECT @errorStr = @errorStr + 
		CASE 
			WHEN Mac = @Mac THEN 'MAC ' + MacStr + ' jest już przypisany do opaski Id = ' + CONVERT(nvarchar(10), Id)
			WHEN Uid = @Uid THEN 'UID ' + CONVERT(nvarchar(20), Uid) + ' jest już przypisany do opaski Id = ' + CONVERT(nvarchar(10), Id)
			WHEN BarCode = @BarCode THEN 'Kod kreskowy ' + BarCode + ' jest już przypisany do opaski Id = ' + CONVERT(nvarchar(10), Id)
			ELSE 'Nieznany błąd'
		END + @NEW_LINE
	FROM @errors

	IF @errorStr <>''
	BEGIN
		SELECT CONVERT(int, null) AS Result, -1 AS ErrorCode, @errorStr AS ErrorMessage
		RETURN
	END
	END

	--parsowanie parametrów
	INSERT @params(Text)
	SELECT LEFT(REPLACE(Value, @SEPARATOR_ESCAPE, @SEPARATOR), @MAX_LEN)  FROM dbo.fn_SplitWithEmpty(@parameters, @SEPARATOR)

	BEGIN TRY
		BEGIN TRANSACTION
			IF @eventType = @EVENT_CREATE
			BEGIN
				INSERT dbo.Bands(Mac, Uid, BarCode, SerialNumber, TransmissionPower, AccPercent, SensitivityPercent, BroadcastingIntervalMs, BatteryTypeId, BatteryInstallationDateUtc, Description, UnitId) VALUES
					(@Mac, @Uid, @BarCode, @SerialNumber, @TransmissionPower, @AccPercent, @SensitivityPercent, @BroadcastingIntervalMs, @BatteryTypeId, @BatteryInstallationDateUtc, @Description, @UnitId)
				SELECT @BandId = SCOPE_IDENTITY()
				SELECT @MacStr = MacString FROM dbo.Bands WHERE Id = @BandId
				
				EXEC dbo.dev_BandDeviceCreate @Mac, @DEF_CLIENT_ID, @MacStr, @BarCode, @BAND_DEVICE_TYPEID, @BANDS_LAYER_NAME


			END
			ELSE -- update
			BEGIN
				UPDATE dbo.Bands SET
					Mac = @Mac
					,Uid = @Uid
					,BarCode = @BarCode
					,SerialNumber = @SerialNumber
					,TransmissionPower = @TransmissionPower
					,AccPercent = @AccPercent
					,SensitivityPercent = @SensitivityPercent
					,BroadcastingIntervalMs = @BroadcastingIntervalMs
					,BatteryTypeId = @BatteryTypeId
					,BatteryInstallationDateUtc = @BatteryInstallationDateUtc
					,Description = @Description
					,UnitId = @UnitId
				WHERE Id = @BandId

				UPDATE dbo.ev_BandStatus SET ObjectUnitId = @UnitId, AssignDateUtc = @currentDate
				WHERE BandId = @BandId
					AND ObjectType = @OBJECT_TYPE_BAND

				DELETE dbo.BandSettings WHERE BandId = @BandId
			END

			--parametry opaski
			INSERT dbo.BandSettings
			SELECT @BandId
				, REPLACE(k.Text, @SEPARATOR_ESCAPE, @SEPARATOR)
				, REPLACE(d.Text, @SEPARATOR_ESCAPE, @SEPARATOR)
				, REPLACE(v.Text, @SEPARATOR_ESCAPE, @SEPARATOR)
			FROM @params k
			JOIN @params d ON d.Position = k.Position + 1
			JOIN @params v ON v.Position = k.Position + 2
			WHERE k.Position % 3 = 0

			DECLARE @JSON nvarchar(max)
			EXEC @JSON = dbo.SerializeJSON_Band @BandId

			INSERT dbo.ev_Events(DateUtc, Type, Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason) VALUES
				(@currentDate, @eventType, @SEVERITY_EVENT, @UnitId, @BandId, @OBJECT_TYPE_BAND, @UnitId, @currentDate, @currentDate, @OperatorId, @JSON, @Reason)
			IF @UPDATE_ENDING_EVENT_ID = 1
			BEGIN
				DECLARE @lastInsertedEventId int
				SET @lastInsertedEventId = SCOPE_IDENTITY()
				UPDATE dbo.ev_Events SET EndingEventId = @lastInsertedEventId WHERE Id = @lastInsertedEventId
			END
			IF @eventType = @EVENT_CREATE
			BEGIN
				DECLARE @creationReason nvarchar(200)
				SELECT @creationReason = FirstBandAssignmentReason FROM dbo.ev_Settings
				EXEC [dbo].[ev_AssignBandToObject_core] @operatorId, @currentDate, @creationReason, @BandId, null, @BandId, @OBJECT_TYPE_BAND
			END

		COMMIT TRANSACTION
		SELECT @BandId AS Result, NULL as ErrorCode, NULL as ErrorMessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		SELECT CONVERT(int, null) AS Result, ERROR_NUMBER() AS ErrorCode, ERROR_MESSAGE() AS ErrorMessage
	END CATCH
END