﻿

CREATE PROCEDURE [dbo].[ev_UserLoggedOn]
	@UserId int
AS
BEGIN
	SET NOCOUNT ON;

	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
		BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'UserId=' + COALESCE(CONVERT(nvarchar(100), @UserId), 'NULL') 
		EXEC Log_Action @@PROCID, @info
	END
	/****************************************************/
	
	DECLARE
		@UPDATE_ENDING_EVENT_ID bit = 1
	DECLARE 
		@eventDateUtc datetime =  dbo.GetCurrentUtcDate() 
		,@currentEventId int
		,@json nvarchar(max)

	SET  @json = 'SELECT Id AS UserId, Login, Name, LastName FROM dbo.Users WHERE Id = ' + CONVERT(nvarchar(10), @UserId)
	EXEC @json = dbo.SerializeJSON @json

	INSERT ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, EndingEventId, [EventData], OperatorId)
	SELECT @EventDateUtc, eventType.LogOn, eventSeverity.[Event], null, @UserId, objectType.[User], null, @eventDateUtc, @eventDateUtc, null, @json, @UserId
	FROM enum.EventType AS eventType 
	JOIN enum.EventSeverity AS eventSeverity ON 1 = 1
	JOIN enum.ObjectType AS objectType ON 1 = 1
	SET @currentEventId = SCOPE_IDENTITY()
	IF @UPDATE_ENDING_EVENT_ID = 1
	BEGIN
		UPDATE dbo.ev_Events SET EndingEventId = @currentEventId WHERE Id = @currentEventId
	END
	
END