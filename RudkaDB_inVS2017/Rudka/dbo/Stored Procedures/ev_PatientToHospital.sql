﻿CREATE PROCEDURE [dbo].[ev_PatientToHospital]
	@Name nvarchar(100)
	,@LastName nvarchar(100)
	,@Pesel char(11)

	,@NewWardId int
	,@NewRoomId int

	,@BandId smallint
	,@BandCode nvarchar(100)

	,@OperatorId int
	,@Reason nvarchar(max) = null
	,@ForceAction bit = 0 -- 0 nie przyjmie pacjenta będącego w szpitalu, 1 - wypisze i przyjmie na nowo

	,@OperationDateUtc dateTime = null
AS
BEGIN
	SET NOCOUNT ON;

	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
		BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'Name=' + COALESCE(CONVERT(nvarchar(100), @Name), 'NULL') 
					+ ';BandId=' + COALESCE(CONVERT(nvarchar(50), @BandId, 121), 'NULL')
					+ ';NewRoomId=' + COALESCE(CONVERT(nvarchar(100), @NewRoomId), 'NULL')
		EXEC Log_Action @@PROCID, @info
	END
	/****************************************************/

	DECLARE
		@UPDATE_ENDING_EVENT_ID bit = 1
		,@clientId int = 1	
	DECLARE
		@REGISTRATION_WARD int
		,@SEVERITY_EVENT dbo.EVENT_SEVERITY
		,@OBJECT_TYPE_PATIENT dbo.OBJECT_TYPE
		,@eventId int
		,@inRoomDateUtc datetime

	IF @OperationDateUtc IS NULL SELECT @OperationDateUtc = dbo.GetCurrentUTCDate()
	

	SELECT @REGISTRATION_WARD = RegistrationWardId
	FROM dbo.Clients WHERE ID = @ClientId

	IF @NewWardId IS NULL SELECT @NewWardId = @REGISTRATION_WARD, @NewRoomId = NULL
	IF @NewRoomId IS NULL SELECT @inRoomDateUtc = NULL ELSE SELECT @inRoomDateUtc = @OperationDateUtc

	SELECT
		@SEVERITY_EVENT = eventSeverity.Event
		,@OBJECT_TYPE_PATIENT = objectType.Patient
	FROM enum.EventSeverity eventSeverity
	JOIN enum.ObjectType objectType ON 1 = 1


	DECLARE
		@PatientId int

	SELECT @PatientId = Id 
	FROM dbo.Patients 
	WHERE COALESCE(@Pesel, '') = COALESCE(Pesel, '') AND @Name = Name AND @LastName = LastName

	IF @PatientId IS NOT NULL
		IF @forceAction = 1
		BEGIN
			EXEC dbo.ev_PatientFromHospital @PatientId, @OperatorId
			SELECT @PatientId = NULL
		END
		ELSE
		BEGIN
			SELECT CONVERT(int, null) AS Result, -1 AS ErrorCode, 'Pacjent został już przyjęty do szpitala.' AS ErrorMessage
			RETURN
		END	



	BEGIN TRY
		BEGIN TRANSACTION
			EXEC arch.PatientFromArchive @Name, @LastName, @Pesel, @PatientId OUT

			BEGIN --Tworzenie rekordu pacjenta
				IF @PatientId IS NOT NULL
				BEGIN
					SET IDENTITY_INSERT dbo.Patients ON;
					INSERT dbo.Patients(Id, Name, LastName, Pesel, HospitalDateUtc, WardId, WardDateUtc, RoomId, RoomDateUtc) VALUES
						--(@PatientId, @Name, @LastName, @Pesel,@OperationDateUtc, @NewWardId, @OperationDateUtc, @NewRoomId, @inRoomDateUtc)
						(@PatientId, @Name, @LastName, @Pesel,@OperationDateUtc, null, null, null, null)
					SET IDENTITY_INSERT dbo.Patients OFF;
				END
				ELSE
				BEGIN
					INSERT dbo.Patients(Name, LastName, Pesel, HospitalDateUtc, WardId, WardDateUtc, RoomId, RoomDateUtc) VALUES
						--(@Name, @LastName, @Pesel,@OperationDateUtc, @NewWardId, @OperationDateUtc, @NewRoomId, @inRoomDateUtc)
						(@Name, @LastName, @Pesel,@OperationDateUtc, null, null, null, null)
					SELECT @PatientId = SCOPE_IDENTITY()
				END
			END


			BEGIN --Rejestracja
				DECLARE @json nvarchar(max)
				EXEC @json = dbo.SerializeJSON_Patient @PatientId

				INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason)
				SELECT @OperationDateUtc, eventType.RegisterInHostpital, @SEVERITY_EVENT, @REGISTRATION_WARD, @PatientId, @OBJECT_TYPE_PATIENT, null, @OperationDateUtc, @OperationDateUtc, @OperatorId, @json, @Reason
				FROM enum.EventType eventType
				IF @UPDATE_ENDING_EVENT_ID = 1
				BEGIN
					SELECT @eventId = SCOPE_IDENTITY()
					UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
				END
			END

			EXEC dbo.ev_AssignPatientToWardRoom_core @PatientId,null, null, @NewWardId, @NewRoomId, @OperatorId, @Reason, 0, @OperationDateUtc
			
			IF @BandId IS NULL SELECT @BandId = dbo.GetBandByCode(@BandCode)
			IF @BandId IS NOT NULL
				EXEC dbo.ev_AssignBandToObject_core @OperatorId, @OperationDateUtc, NULL, @BandId, NULL, @PatientId, @OBJECT_TYPE_PATIENT
			ELSE RAISERROR('Nieznany kod opaski', 16, 1)

		SELECT @PatientId AS Result, NULL as ErrorCode, NULL as ErrorMessage
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		SELECT CONVERT(int, null) AS Result, ERROR_NUMBER() AS ErrorCode, ERROR_MESSAGE() AS ErrorMessage
	END CATCH	


END