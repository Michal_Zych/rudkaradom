﻿
CREATE PROCEDURE [dbo].[dev_TransmitterGroupsGet]
AS 
BEGIN
	SET NOCOUNT OFF;
	
	SELECT [Group] AS Id, [Description] AS Name
	FROM dbo.dev_TransmitterGroups
	ORDER BY [Description]
END