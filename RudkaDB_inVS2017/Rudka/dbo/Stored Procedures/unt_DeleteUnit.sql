﻿-- =============================================
-- Author:		
-- Create date: 
-- Description: Deaktywacja Unit'a
-- R.Sz. Nie sprawdza czy w unicie są urządzenie, koncentrator
-- =============================================
CREATE PROCEDURE [dbo].[unt_DeleteUnit]
	@unitId int
	,@operator int
	,@reason nvarchar(max)
AS
BEGIN


	DECLARE @OPERATION_DELETE tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_EDIT tinyint = 2

		,@RECORD_UNIT tinyint = 0
		,@RECORD_USER tinyint = 1

	DECLARE
		@sensorID smallint

	BEGIN TRANSACTION
		BEGIN TRY
			IF (EXISTS(SELECT 1 FROM dbo.Units WHERE ParentUnitId = @unitId)) SELECT 1/0 --nie usuwamy niepustych

			UPDATE dbo.Units
			SET Active = 0
				,ParentUnitID = null
			WHERE ID = @unitId;

			SELECT @sensorID = ID FROM dbo.Sensors WHERE Unitid = @unitId

			IF @sensorID IS NOT NULL
			BEGIN
				UPDATE dbo.Sensors
					SET Active = 0
						,SocketID = null
				WHERE ID = @sensorID

				DELETE dbo.AlarmsOmittedRanges
				WHERE SensorID = @sensorID

				DELETE dbo.AlarmNotifyByEmail
				WHERE SensorID = @sensorID

				DELETE dbo.AlarmNotifyBySms
				WHERE SensorId = @sensorID
			END

			INSERT dbo.Logs(OperationDate, OperatorId, OperationType, ObjectType, ObjectId, Reason)
				VALUES(dbo.GetCurrentDate(), @operator, @OPERATION_DELETE, @RECORD_UNIT, @unitId, @reason)
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			SELECT -1
			RETURN
		END CATCH

	COMMIT TRANSACTION
	
	SELECT @unitId	

END

