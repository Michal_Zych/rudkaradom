﻿CREATE PROCEDURE fda_GetExternalUnitMapFilteredTotalCount
@TreeUnitIds nvarchar(max),
@ExtId nvarchar(200) = null
,@ExtDescription nvarchar(200) = null
,@M2mIdFrom int = null
,@M2mIdTo int = null
,@M2mDescription nvarchar(200) = null
AS
BEGIN

 DECLARE @cmd nvarchar(max)
 SET @cmd = 'SELECT COUNT(1) '
		+ dbo.dsql_GetExternalUnitMap_From()
		+ dbo.dsql_GetExternalUnitMap_Where(@TreeUnitIds,
			@ExtId, @ExtDescription, @M2mIdFrom, @M2mIdTo, @M2mDescription)

 EXEC(@cmd)
-- print @cmd
END