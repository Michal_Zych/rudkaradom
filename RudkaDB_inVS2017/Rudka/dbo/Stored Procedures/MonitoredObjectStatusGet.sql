﻿
CREATE PROCEDURE [dbo].[MonitoredObjectStatusGet]
	@ObjectType dbo.OBJECT_TYPE
	,@ObjectId int
AS
BEGIN
	DECLARE
		@alarmsCountSeverity1 int
	   ,@alarmsCountSeverity2 int

	--JZ 2019-01-17 12:09 zamieniono COUNT na SUM
	SELECT @alarmsCountSeverity1 = SUM(CASE WHEN e.Severity = 1 THEN 1 ELSE 0 END), @alarmsCountSeverity2 = SUM(CASE WHEN e.Severity = 2 THEN 1 ELSE 0 END)
	FROM ev_Alarms a
	JOIN ev_Events e ON a.EventId = e.Id
	WHERE a.IsClosed = 0
		AND e.ObjectId = @ObjectId AND e.ObjectType = @ObjectType


	SELECT 
		bs.ObjectId AS ObjectId
		,bs.ObjectType AS ObjectTypeId
		,objectTypeName.Description AS ObjectTypeDescription
		,bs.CurrentUnitId AS CurrentUnitId
		,cu.Name AS CurrentUnitName
		,dbo.GetUnitLocation(bs.CurrentUnitId, default, default, default) AS CurrentUnitLocation
		,bs.InCurrentUnitUtc AS InCurrentUnitUtc
		,bs.PreviousUnitId AS PreviousUnitId
		,pu.Name AS PreviousUnitName
		,dbo.GetUnitLocation(bs.PreviousUnitId, default, default, default) as PreviousUnitLocation
		,bs.InPreviousUnitUtc AS InPreviousUnitUtc
		,COALESCE(p.LastName + COALESCE(' ' + p.Name, ''), b.MacString) AS Name
		,bs.TopAlarmType AS TopAlarmTypeId
		,eventTypeName.Description AS TopAlarmTypeName
		,bs.TopAlarmSeverity AS TopAlarmSeverityId
		,eventSeverityName.Description AS TopAlarmTypeSeverityName
		,bs.TopAlarmDateUtc AS TopAlarmDateUtc
		,eu.Id AS TopAlarmUnitId
		,eu.Name AS TopAlarmUnitName
		,dbo.GetUnitLocation(eu.Id, default, default, default) AS TopAlarmUnitLocation
		,CONVERT(bit, COALESCE(OngMovingEventId,0)) As IsMoving
		,COALESCE(p.Pesel, b.MacString) AS DispalyId
		,ar.Id AS AssignmentRoomId
		,ar.Name AS AssignmentRoomName
		,aw.Id AS AssignmentWardId
		,aw.Name AS AssignmentWardName
		,au.Id AS AssignmentUnitId
		,au.Name AS AssignmentUnitName
		,bs.Severity1Count AS OngoingAlarmsSeverity1
		,@alarmsCountSeverity1 AS OpenAlarmsSeverity1
		,bs.Severity2Count AS OngoingAlarmsSeverity2
		,@alarmsCountSeverity2 AS OpenAlarmsSeverity2
	FROM dbo.ev_BandStatus bs
	JOIN enum.ObjectType objectType ON 1 = 1
	JOIN dbo.enum_ObjectTypeDictionaryPl objectTypeName ON objectTypeName.Value = bs.ObjectType
	-- JOIN dbo.Units cu ON cu.Id = bs.CurrentUnitId --JZ
	LEFT JOIN dbo.Units cu ON cu.Id = bs.CurrentUnitId
	LEFT JOIN dbo.Units pu ON pu.Id = bs.PreviousUnitId
	LEFT JOIN dbo.Patients p ON p.Id = bs.ObjectId AND bs.ObjectType = objectType.Patient
	LEFT JOIN dbo.Bands b ON b.Id = bs.ObjectId AND bs.ObjectType = objectType.Band
	LEFT JOIN dbo.enum_EventTypeDictionaryPl eventTypeName ON eventTypeName.Value = bs.TopAlarmType
	LEFT JOIN dbo.enum_EventSeverityDictionaryPl eventSeverityName ON eventSeverityName.Value = TopAlarmSeverity
	LEFT JOIN dbo.Units ar ON ar.Id = p.RoomId AND bs.ObjectType = objectType.Patient
	LEFT JOIN dbo.Units aw ON aw.Id = p.WardId AND bs.ObjectType = objectType.Patient
	LEFT JOIN dbo.Units au ON au.Id = bs.ObjectUnitId AND bs.ObjectType = objectType.Band
	LEFT JOIN dbo.ev_Events e ON bs.TopAlarmId = e.Id
	LEFT JOIN dbo.Units eu ON eu.Id = e.UnitId
	WHERE bs.ObjectId = @ObjectId AND bs.ObjectType = @ObjectType
END