﻿CREATE PROCEDURE [dbo].[ev_CloseAlarms]
	@Ids IDs_LIST
	,@Comment nvarchar(max)
	,@Userid int
AS
BEGIN
	SET NOCOUNT ON;

	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
		BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'Comment=' + COALESCE(CONVERT(nvarchar(100), @Comment), 'NULL') 
		EXEC Log_Action @@PROCID, @info
	END
	/****************************************************/
	
	DECLARE
		@currentDate dateTime = dbo.GetCurrentUtcDate()

	DECLARE @alarmsIds TABLE (Id int)
	INSERT @alarmsIds --tabela identyfikatorów zamykanych alarmów
	SELECT Value FROM dbo.SplitWordsToInt(@Ids)


	UPDATE a SET
		IsClosed = 1
		,ClosingDateUtc  = COALESCE(ClosingDateUtc, @currentDate)
		,ClosingUserId	 = COALESCE(ClosingUserId, @UserId)
		,ClosingComment = CASE 
							WHEN COALESCE(IsClosed, 0) = 0 
							THEN COALESCE(ClosingComment + CHAR(13), '')
								+ CONVERT(nvarchar(25), DATEADD(MINUTE, DATEDIFF(MINUTE, dbo.GetCurrentUTCDate(), dbo.GetCurrentDate()),@currentDate), 120) + ' Zamknięcie' 
								+ CHAR(13) + COALESCE(u.Name, '') + ' ' + COALESCE(u.LastName, '')
								+ COALESCE(CHAR(13) + @Comment, '')
							ELSE COALESCE(ClosingComment, '')
								+ CHAR(13) + CONVERT(nvarchar(25), DATEADD(MINUTE, DATEDIFF(MINUTE, dbo.GetCurrentUTCDate(), dbo.GetCurrentDate()),@currentDate), 120) + ' Ponowne zamknięcie'
								+ CHAR(13) + COALESCE(u.Name, '') + ' ' + COALESCE(u.LastName, '')
								+ COALESCE(CHAR(13) + @comment, '')
							END
	FROM dbo.ev_Alarms a
	JOIN @alarmsIds ai ON a.EventId = ai.Id
	JOIN dbo.Users u ON u.Id = @Userid
	
	DELETE dbo.ev_CallOrdersDetails WHERE EventID IN (SELECT Id FROM @alarmsIds)
	DELETE dbo.ev_CallOrders WHERE EventID IN (SELECT Id FROM @alarmsIds)

	DECLARE @changedObjects TABLE(ObjectId int, ObjectType dbo.OBJECT_TYPE)
	INSERT @changedObjects
	SELECT DISTINCT e.ObjectId, e.ObjectType
	FROM dbo.ev_Events e
	JOIN @alarmsIds a ON e.Id = a.Id
	
	--oblicz statusy opasek/transmiterów
	DECLARE @objectId dbo.OBJECT_ID
		,@objectType dbo.OBJECT_TYPE
		,@OBJECT_TYPE_TRANSMITTER dbo.OBJECT_TYPE = (SELECT Transmitter FROM enum.ObjectType)
	
	SELECT TOP 1 @objectType = ObjectType, @objectId = ObjectId FROM @changedObjects
	WHILE @objectId IS NOT NULL
	BEGIN
		IF @objectType = @OBJECT_TYPE_TRANSMITTER 
		BEGIN-- status transmitera
			UPDATE s SET
				s.OngDisconnectedAlarmId = NULL
				,s.OngDisconnectedAlarmSeverity = CASE WHEN s.OngDisconnectedEventId IS NULL THEN NULL ELSE eventSeverity.Event END
			FROM dbo.ev_TransmitterStatus s
			JOIN enum.EventSeverity eventSeverity ON 1 = 1
			WHERE s.TransmitterId = @objectId
		END
		ELSE	
			EXEC dbo.ev_CalculateMonitoredObjectStatus  @objectId, @objectType
		
		DELETE @changedObjects
		WHERE ObjectId = @ObjectId AND ObjectType = @objectType

		SELECT @objectId = null, @objectType = null
		SELECT TOP 1 @objectType = ObjectType, @objectId = ObjectId FROM @changedObjects
	END

	SELECT @Ids
END