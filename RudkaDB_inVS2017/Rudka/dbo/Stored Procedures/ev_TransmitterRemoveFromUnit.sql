﻿
CREATE PROCEDURE [dbo].[ev_TransmitterRemoveFromUnit]
	@UnitId int
	,@OperatorId int
	,@Reason nvarchar(max) = null
	,@OperationDateUtc dateTime = null
AS
BEGIN
	SET NOCOUNT ON;

	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
		BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'UnitId=' + COALESCE(CONVERT(nvarchar(100), @UnitId), 'NULL') 
		EXEC Log_Action @@PROCID, @info
	END
	/****************************************************/

	DECLARE
		@UPDATE_ENDING_EVENT_ID bit = 1

	DECLARE 
		@errorString nvarchar(1024) = ''
		,@transmitterId int
		,@currentDate dateTime 
		,@eventId int

	SELECT @transmitterId = TransmitterId FROM dbo.dev_UnitTransmitter WHERE UnitId = @UnitId
	IF @transmitterId IS NULL SET @errorString = 'Jednostka organizacyjna nie ma przypisanego transmitera'

	IF @errorString <> ''
	BEGIN
		SELECT CONVERT(int, null) AS Result, 1 AS ErrorCode, @errorString AS ErrorMessage
		RETURN
	END
	
	SELECT @currentDate = COALESCE(@OperationDateUtc, dbo.GetCurrentUtcDate())

	BEGIN TRY
		BEGIN TRANSACTION
			DELETE dbo.dev_UnitTransmitter WHERE UnitID = @UnitId

			INSERT dbo.ev_Events(DateUtc, Type, Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason) 
			SELECT @currentDate, eventType.TransmitterAssignment, eventSeverity.Event, @UnitId, @transmitterId, objectType.Transmitter, null, @currentDate, @currentDate, @OperatorId, null, @Reason
			FROM enum.EventType eventType
			JOIN enum.EventSeverity eventSeverity ON 1 = 1
			JOIN enum.ObjectType objectType ON 1 = 1
			SELECT @eventId = SCOPE_IDENTITY()
			IF @UPDATE_ENDING_EVENT_ID = 1
			BEGIN
				UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
			END


		COMMIT TRANSACTION
		SELECT @transmitterId AS Result, NULL as ErrorCode, NULL as ErrorMessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		SELECT CONVERT(int, null) AS Result, ERROR_NUMBER() AS ErrorCode, ERROR_MESSAGE() AS ErrorMessage
	END CATCH



END