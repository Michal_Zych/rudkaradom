﻿CREATE PROCEDURE [JOBdaily_GenerateBatteryTermAlarms]
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE
		@UPDATE_ENDING_EVENT_ID bit = 1
		,@SYSTEM_OPERATOR int = 0		
	DECLARE
		@currentDate datetime = dbo.GetCurrentUtcDate()
		,@warningDaysBefore int
		,@alarmDaysBefore int
		,@warningMessage nvarchar(max)
		,@alarmMessage nvarchar(max)
		,@currentMessage nvarchar(max)
		,@EVENT_TYPE_USERMESSAGE dbo.EVENT_TYPE
		,@EVENT_SEVERITY_WARNING dbo.EVENT_SEVERITY
		,@EVENT_SEVERITY_ALARM dbo.EVENT_SEVERITY

	SELECT @warningDaysBefore = st.BatteryTermWrDaysBefore, @warningMessage = BatteryTermWrMessage
		,@alarmDaysBefore = st.BatteryTermAlDaysBefore, @alarmMessage = BatteryTermAlMessage
		,@EVENT_TYPE_USERMESSAGE = eventType.UserMessage
		,@EVENT_SEVERITY_WARNING = eventSeverity.Warning
		,@EVENT_SEVERITY_ALARM = eventSeverity.Alarm
	FROM dbo.ev_Settings st
	JOIN enum.EventType eventType ON 1 = 1
	JOIN enum.EventSeverity eventSeverity ON 1 = 1


	DECLARE
		@currentSeverity dbo.EVENT_SEVERITY
	BEGIN -- zmienne konfiguracji
		DECLARE
		 @objectType				dbo.OBJECT_TYPE   
		,@objectId					dbo.OBJECT_ID  
		,@objectUnitId				int  
		,@currentUnitId				int  
		,@inCurrentUnitUtc			dateTime  
		,@previousUnitId			int  
		,@inPreviousUnitUtc			dateTime  
		,@topAlarmId				int  
		,@topAlarmType				dbo.EVENT_TYPE   
		,@topAlarmSeverity			dbo.EVENT_SEVERITY   
		,@topAlarmDateUtc			dateTime 
		,@topEventDateUtc			dateTime  
		,@severity1Count			int  
		,@severity2Count			int  
		,@ongNoGoZoneInEventId		int  
		,@ongNoGoZoneInAlarmId		int  
		,@ongDisconnectedEventId	int  
		,@ongDisconnectedAlarmId	int  
		,@ongZoneOutEventId			int  
		,@ongZoneOutAlarmId			int  
		,@ongNoMovingEventId		int  
		,@ongNoMovingAlarmId		int  
		,@ongMovingEventId			int  
		,@ongMovingAlarmId			int  
		,@ongLowBatteryEventId		int  
		,@ongLowBatteryAlarmId		int 
	END




	DECLARE @BandsToInfo TABLE(BandId smallint, Warning bit, Alarm bit)
	INSERT @BandsToInfo
	SELECT b.Id, CASE WHEN DATEDIFF(DAY, @currentDate,  DATEADD(MONTH, bt.LifeMonths, BatteryInstallationDateUtc)) = @warningDaysBefore THEN 1 ELSE 0 END AS GenerateWarning
		, CASE WHEN DATEDIFF(DAY, @currentDate,  DATEADD(MONTH, bt.LifeMonths, BatteryInstallationDateUtc)) <= @alarmDaysBefore THEN 1 ELSE 0 END AS GenerateAlarm
	FROM dbo.Bands b
	JOIN dbo.BatteryTypes bt on b.BatteryTypeId = bt.Id
	WHERE b.Id > 0 
		AND ((DATEDIFF(DAY, @currentDate,  DATEADD(MONTH, bt.LifeMonths, BatteryInstallationDateUtc)) = @warningDaysBefore)	
			OR (DATEDIFF(DAY, @currentDate,  DATEADD(MONTH, bt.LifeMonths, BatteryInstallationDateUtc)) <= @alarmDaysBefore))
--select * from @BandsToInfo;return

	DECLARE
		@bandId smallint
		,@generateWarning bit
		,@generateAlarm bit
	SELECT TOP 1 @bandId = BandId, @generateWarning = Warning, @generateAlarm = Alarm FROM @BandsToInfo
	WHILE @bandId IS NOT NULL
	BEGIN
		IF @generateAlarm = 1 SET @generateWarning = 0
		IF @generateWarning = 1 
			SELECT @currentSeverity = @EVENT_SEVERITY_WARNING, @currentMessage = @warningMessage
		ELSE 
			SELECT @currentSeverity = @EVENT_SEVERITY_ALARM, @currentMessage = @alarmMessage


		BEGIN	-- pobranie konfiguracji
			SELECT
				 @objectType					= bs.ObjectType			
				,@objectId						= bs.ObjectId				
				,@objectUnitId					= bs.ObjectUnitId			
				,@currentUnitId					= bs.CurrentUnitId			
				,@inCurrentUnitUtc				= bs.InCurrentUnitUtc		
				,@previousUnitId				= bs.PreviousUnitId		
				,@inPreviousUnitUtc				= bs.InPreviousUnitUtc		
				,@topAlarmId					= bs.TopAlarmId			
				,@topAlarmType					= bs.TopAlarmType			
				,@topAlarmSeverity				= bs.TopAlarmSeverity		  
				,@topAlarmDateUtc				= bs.TopAlarmDateUtc		
				,@topEventDateUtc				= bs.TopEventDateUtc		
				,@severity1Count				= bs.Severity1Count		
				,@severity2Count				= bs.Severity2Count		
				,@ongNoGoZoneInEventId			= bs.OngNoGoZoneInEventId  
				,@ongNoGoZoneInAlarmId			= bs.OngNoGoZoneInAlarmId  
				,@ongDisconnectedEventId		= bs.OngDisconnectedEventId
				,@ongDisconnectedAlarmId		= bs.OngDisconnectedAlarmId
				,@ongZoneOutEventId				= bs.OngZoneOutEventId		
				,@ongZoneOutAlarmId				= bs.OngZoneOutAlarmId		
				,@ongNoMovingEventId			= bs.OngNoMovingEventId	
				,@ongNoMovingAlarmId			= bs.OngNoMovingAlarmId	
				,@ongMovingEventId				= bs.OngMovingEventId		
				,@ongMovingAlarmId				= bs.OngMovingAlarmId		
				,@ongLowBatteryEventId			= bs.OngLowBatteryEventId  
				,@ongLowBatteryAlarmId			= bs.OngLowBatteryAlarmId  
			FROM dbo.ev_BandStatus bs 
			WHERE bs.BandId = @bandId
		END	


		-- zarejestrowanie rekordu statusu dla opaski
		IF @objectId IS NULL
		BEGIN
			EXEC dbo.ev_AssignBandToItself
				@EventDateUtc		= @currentDate
				,@BandId			= @BandId
				,@ObjectId			= @objectId			OUTPUT
				,@ObjectType		= @objectType		OUTPUT
				,@InCurrentUnitUtc	= @inCurrentUnitUtc	OUTPUT
				,@severity1Count	= @severity1Count	OUTPUT
				,@severity2Count	= @severity2Count	OUTPUT
		END


		-- generuj alarm
		DECLARE
			@currentEventId int
		
		INSERT ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, EndingEventId, [EventData], OperatorId)
		SELECT @currentDate, @EVENT_TYPE_USERMESSAGE, @currentSeverity, @currentUnitId, @objectId, @objectType, @objectUnitId, @currentDate, @currentDate, null, @currentMessage, @SYSTEM_OPERATOR
		SET @currentEventId = SCOPE_IDENTITY()
		IF @UPDATE_ENDING_EVENT_ID = 1
		BEGIN
			UPDATE dbo.ev_Events SET EndingEventId = @currentEventId WHERE Id = @currentEventId
		END

		INSERT dbo.ev_Alarms(EventId, RaisingEventId) VALUES (@currentEventId, @currentEventId)

		IF @currentSeverity = @EVENT_SEVERITY_WARNING SET @Severity1Count = @Severity1Count + 1
		ELSE SET @Severity2Count = @Severity2Count + 1

		

		-- uaktualnij status
		DECLARE 
			@warnigDate datetime
			,@alarmDate datetime
		SELECT @warnigDate = CASE WHEN @currentSeverity = @EVENT_SEVERITY_WARNING THEN @currentDate ELSE NULL END
			, @alarmDate = CASE WHEN @currentSeverity = @EVENT_SEVERITY_ALARM THEN @currentDate ELSE NULL END
		EXEC dbo.ev_GenerateBandStatus
			 @BandId					= @BandId 
			,@CurrentUnitId 			= @CurrentUnitId 
			,@InCurrentUnitUtc			= @InCurrentUnitUtc
			,@PreviousUnitId 			= @PreviousUnitId 
			,@InPreviousUnitUtc 		= @InPreviousUnitUtc 
			,@TopAlarmId 				= @TopAlarmId 
			,@TopAlarmType  			= @TopAlarmType  
			,@TopAlarmSeverity 			= @TopAlarmSeverity 
			,@TopAlarmDateUtc 			= @TopAlarmDateUtc 
			,@TopEventDateUtc 			= @TopEventDateUtc 
			,@Severity1Count 			= @Severity1Count 
			,@Severity2Count 			= @Severity2Count 
			,@OngNoGoZoneInEventId 		= @OngNoGoZoneInEventId 
			,@OngNoGoZoneInAlarmId  	= @OngNoGoZoneInAlarmId  
			,@OngDisconnectedEventId  	= @OngDisconnectedEventId 
			,@OngDisconnectedAlarmId  	= @OngDisconnectedAlarmId 
			,@OngZoneOutEventId  		= @OngZoneOutEventId  
			,@OngZoneOutAlarmId  		= @OngZoneOutAlarmId  
			,@OngNoMovingEventId  		= @OngNoMovingEventId  
			,@OngNoMovingAlarmId  		= @OngNoMovingAlarmId  
			,@OngMovingEventId  		= @OngMovingEventId  
			,@OngMovingAlarmId  		= @OngMovingAlarmId  
			,@OngLowBatteryEventId  	= @OngLowBatteryEventId  
			,@OngLowBatteryAlarmId 		= @OngLowBatteryAlarmId 
			,@CurrentEventId			= @currentEventId
			,@CurrentEventType 			= @EVENT_TYPE_USERMESSAGE 
			,@CurrentEventDateUtc 		= @currentDate 
			,@GeneratedAlarmId			= @currentEventId
			,@GeneratedAlarmSeverity 	= @currentSeverity 
			,@GeneratedWarningDateUtc 	= @warnigDate
			,@GeneratedAlarmDateUtc 	= @alarmDate


		DELETE @BandsToInfo WHERE BandId = @bandId
		SELECT @bandId = null
		SELECT TOP 1 @bandId = BandId, @generateWarning = Warning, @generateAlarm = Alarm FROM @BandsToInfo
	END
END