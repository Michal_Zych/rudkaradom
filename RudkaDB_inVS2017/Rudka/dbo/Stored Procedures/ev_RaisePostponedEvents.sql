﻿CREATE PROCEDURE [dbo].[ev_RaisePostponedEvents]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE
			 @EVENT_NOGOZONEIN			dbo.EVENT_TYPE
			,@EVENT_DISCONNECTED		dbo.EVENT_TYPE
			,@EVENT_ZONEIN				dbo.EVENT_TYPE
			,@EVENT_ZONEOUT				dbo.EVENT_TYPE
			,@EVENT_NOMOVING			dbo.EVENT_TYPE
			,@EVENT_MOVING				dbo.EVENT_TYPE
			,@EVENT_LOWBATTERY			dbo.EVENT_TYPE
			
			,@OBJECT_TYPE_BAND			dbo.OBJECT_TYPE
			,@OBJECT_TYPE_TRANSMITTER	dbo.OBJECT_TYPE
			
			
			,@warningAutoCloseMessage	nvarchar(max)

		SELECT @EVENT_NOGOZONEIN			= et.NoGoZoneIn
			   ,@EVENT_DISCONNECTED			= et.Disconnected
			   ,@EVENT_ZONEIN				= et.ZoneIn
			   ,@EVENT_ZONEOUT				= et.ZoneOut
			   ,@EVENT_NOMOVING				= et.NoMoving
			   ,@EVENT_MOVING				= et.Moving
			   ,@EVENT_LOWBATTERY			= et.LowBattery
			   ,@warningAutoCloseMessage	= st.WarningAutoCloseMessage
			   ,@OBJECT_TYPE_BAND			= ot.Band
			   ,@OBJECT_TYPE_TRANSMITTER	= ot.Transmitter
		FROM enum.EventType et
		JOIN enum.ObjectType ot ON 1 = 1
		JOIN dbo.ev_Settings st ON  1 = 1
		
	
	
	DECLARE
		@BandId int									
		,@CurrentUnitId int
		,@InCurrentUnitUtc dateTime
		,@PreviousUnitId int
		,@InPreviousUnitUtc dateTime
		,@TopAlarmId int
		,@TopAlarmType dbo.EVENT_TYPE  
		,@TopAlarmSeverity dbo.EVENT_SEVERITY 
		,@TopAlarmDateUtc dateTime
		,@TopEventDateUtc dateTime
		,@Severity1Count int
		,@Severity2Count int
		,@OngNoGoZoneInEventId int
		,@OngNoGoZoneInAlarmId int 
		,@OngDisconnectedEventId int 
		,@OngDisconnectedAlarmId int 
		,@OngZoneOutEventId int 
		,@OngZoneOutAlarmId int 
		,@OngNoMovingEventId int 
		,@OngNoMovingAlarmId int 
		,@OngMovingEventId int 
		,@OngMovingAlarmId int 
		,@OngLowBatteryEventId int 
		,@OngLowBatteryAlarmId int
		,@CurrentEventId int
		,@CurrentEventType dbo.EVENT_TYPE
		,@CurrentEventDateUtc dateTime
		,@GeneratedAlarmId int
		,@GeneratedAlarmSeverity dbo.EVENT_SEVERITY
		,@GeneratedWarningDateUtc dateTime
		,@GeneratedAlarmDateUtc dateTime

		,@ToRaiseId int
		,@RaisingEventId int
		,@RaisingEventDateUtc dateTime
		,@RaiseDateUtc dateTime
		,@AlarmType dbo.EVENT_TYPE
		,@AlarmSeverity dbo.EVENT_SEVERITY
		,@UnitId int
		,@ObjectId dbo.OBJECT_ID
		,@ObjectType dbo.OBJECT_TYPE
		,@ObjectUnitId int

		,@TransmitterId int
		,@trOngDisconnectedEventId int
		,@trOngDisconnectedAlarmId int
		,@trOngDisconnectedAlarmSeverity dbo.EVENT_SEVERITY
		,@trOngConnectedEventId	int

		,@currentDateUtc dateTime = dbo.GetCurrentUtcDate()
		,@CurrentAlarmId int
		,@currentWarningId int
		,@errorInfo nvarchar(1024)

	WHILE 1 = 1 
	BEGIN
		SELECT @RaisingEventId = NULL
		SELECT TOP 1 
			 @BandId					= bs.BandId 
			,@CurrentUnitId				= bs.CurrentUnitId			
			,@InCurrentUnitUtc			= bs.InCurrentUnitUtc		
			,@PreviousUnitId			= bs.PreviousUnitId			
			,@InPreviousUnitUtc			= bs.InPreviousUnitUtc		
			,@TopAlarmId				= bs.TopAlarmId				
			,@TopAlarmType				= bs.TopAlarmType			
			,@TopAlarmSeverity			= bs.TopAlarmSeverity		
			,@TopAlarmDateUtc			= bs.TopAlarmDateUtc		
			,@TopEventDateUtc			= bs.TopEventDateUtc		
			,@Severity1Count			= bs.Severity1Count			
			,@Severity2Count			= bs.Severity2Count			
			,@OngNoGoZoneInEventId		= bs.OngNoGoZoneInEventId	
			,@OngNoGoZoneInAlarmId		= bs.OngNoGoZoneInAlarmId	
			,@OngDisconnectedEventId	= bs.OngDisconnectedEventId	
			,@OngDisconnectedAlarmId	= bs.OngDisconnectedAlarmId	
			,@OngZoneOutEventId			= bs.OngZoneOutEventId		
			,@OngZoneOutAlarmId			= bs.OngZoneOutAlarmId		
			,@OngNoMovingEventId		= bs.OngNoMovingEventId		
			,@OngNoMovingAlarmId		= bs.OngNoMovingAlarmId		
			,@OngMovingEventId  		= bs.OngMovingEventId  
			,@OngMovingAlarmId  		= bs.OngMovingAlarmId  
			,@OngLowBatteryEventId 		= bs.OngLowBatteryEventId 
			,@OngLowBatteryAlarmId 		= bs.OngLowBatteryAlarmId 

			,@ToRaiseId					= er.Id
			,@RaisingEventId			= er.RaisingEventId
			,@RaisingEventDateUtc		= er.RaisingEventDateUtc
			,@RaiseDateUtc 				= er.RaiseDateUtc 
			,@AlarmType 				= er.[Type]
			,@AlarmSeverity 			= er.Severity 
			,@UnitId	  				= er.UnitId	  
			,@ObjectId					= er.ObjectId		
			,@ObjectType				= er.ObjectType	
			,@ObjectUnitId				= er.ObjectUnitId

			,@TransmitterId				= ts.TransmitterId
			,@trOngDisconnectedEventId	= ts.OngDisconnectedEventId
			,@trOngDisconnectedAlarmId	= ts.OngDisconnectedAlarmId
			,@trOngDisconnectedAlarmSeverity = ts.OngDisconnectedAlarmSeverity
			,@trOngConnectedEventId		= ts.OngConnectedEventId
		FROM dbo.ev_EventsToRaise er
		LEFT JOIN dbo.ev_BandStatus bs ON er.RaisingObjectType = @OBJECT_TYPE_BAND AND er.RaisingObjectId = bs.BandId
		LEFT JOIN dbo.ev_TransmitterStatus ts ON er.RaisingObjectType = @OBJECT_TYPE_TRANSMITTER AND er.RaisingObjectId = ts.TransmitterId
		WHERE er.RaiseDateUtc <= dbo.GetCurrentUTCDate()
		ORDER BY er.Id

		IF @RaisingEventId IS NULL RETURN

		DECLARE @warningId int     --- id warningu
		SELECT @warningId = 
			CASE 
				WHEN @AlarmType = @EVENT_NOGOZONEIN		THEN @ongNoGoZoneInAlarmId
				WHEN @AlarmType = @EVENT_DISCONNECTED	THEN CASE 
																WHEN @BandId IS NOT NULL THEN @ongDisconnectedAlarmId
																WHEN @TransmitterId IS NOT NULL THEN @trOngDisconnectedAlarmId
																ELSE -1
															END
				--WHEN @AlarmType = @EVENT_ZONEIN		THEN @ong
				WHEN @AlarmType = @EVENT_ZONEOUT		THEN @ongZoneOutAlarmId
				WHEN @AlarmType = @EVENT_NOMOVING		THEN @ongNoMovingAlarmId
				WHEN @AlarmType = @EVENT_MOVING			THEN @ongMovingAlarmId
				WHEN @AlarmType = @EVENT_LOWBATTERY		THEN @ongLowBatteryAlarmId
				ELSE -1
			END
		IF @warningId = -1
		BEGIN
			DECLARE @JSON nvarchar(max)
			SET @JSON = 'SELECT ''ev_EventsToRaise - Nieodpowiedni typ alarmu'' AS Description, * FROM dbo.ev_EventsToRaise	WHERE Id = ' + CONVERT(nvarchar(10), @ToRaiseId)
			EXEC @JSON = dbo.SerializeJSON @JSON
			EXEC dbo.ev_StoreDbError @JSON, @ObjectId, @ObjectType


			DELETE dbo.ev_EventsToRaise WHERE Id = @ToRaiseId
			CONTINUE
		END

		IF @AlarmSeverity = (SELECT Warning FROM enum.EventSeverity)
		BEGIN -- zapisz ostrzeżenie
			INSERT ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc)
				VALUES(@RaiseDateUtc, @AlarmType, @AlarmSeverity, @UnitId, @objectId, @objectType, @objectUnitId, @currentDateUtc)
			SET @CurrentWarningId = SCOPE_IDENTITY()

			INSERT ev_Alarms(EventId, RaisingEventId) VALUES(@CurrentWarningId, @RaisingEventId)
			SELECT @severity1Count = @severity1Count + 1
		END
		ELSE
		BEGIN -- zapisz alarm
			INSERT ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc)
				VALUES (@RaiseDateUtc, @AlarmType, @AlarmSeverity, @UnitId, @objectId, @objectType, @objectUnitId, @currentDateUtc)
			SET @CurrentAlarmId = SCOPE_IDENTITY()

			INSERT ev_Alarms(EventId, RaisingEventId) VALUES(@CurrentAlarmId, @RaisingEventId)
			UPDATE ev_Events SET
				EndDateUtc = @RaiseDateUtc
				,EndingEventId = @CurrentAlarmId
			WHERE Id = @warningId
			SELECT @Severity2Count = @Severity2Count + 1


			UPDATE ev_Alarms SET
				ClosingAlarmId = @CurrentAlarmId
				,ClosingComment = @warningAutoCloseMessage
				,ClosingDateUtc = @RaiseDateUtc
				,IsClosed = 1
			WHERE EventId = @warningId AND IsClosed = 0
			SELECT @Severity1Count = @Severity1Count - @@ROWCOUNT
		END

		DECLARE
			@currentId int = COALESCE(@CurrentWarningId, @CurrentAlarmId)
		IF @BandId IS NOT NULL
		BEGIN
			IF @AlarmType = @EVENT_NOGOZONEIN			SET @OngNoGoZoneInAlarmId = @currentId
			ELSE IF @AlarmType = @EVENT_DISCONNECTED	SET @OngDisconnectedAlarmId = @currentId
			ELSE IF @AlarmType = @EVENT_ZONEOUT			SET @ongZoneOutAlarmId = @currentId
			ELSE IF @AlarmType = @EVENT_NOMOVING		SET @ongNoMovingAlarmId = @currentId
			ELSE IF @AlarmType = @EVENT_MOVING			SET @ongMovingAlarmId = @currentId
			ELSE IF @AlarmType = @EVENT_LOWBATTERY		SET @ongLowBatteryAlarmId = @currentId

			EXEC dbo.ev_GenerateBandStatus
				 @BandId					= @BandId 
				,@CurrentUnitId 			= @CurrentUnitId 
				,@InCurrentUnitUtc			= @InCurrentUnitUtc
				,@PreviousUnitId 			= @PreviousUnitId 
				,@InPreviousUnitUtc 		= @InPreviousUnitUtc 
				,@TopAlarmId 				= @TopAlarmId 
				,@TopAlarmType  			= @TopAlarmType  
				,@TopAlarmSeverity 			= @TopAlarmSeverity 
				,@TopAlarmDateUtc 			= @TopAlarmDateUtc 
				,@TopEventDateUtc 			= @TopEventDateUtc 
				,@Severity1Count 			= @Severity1Count 
				,@Severity2Count 			= @Severity2Count 
				
				,@OngNoGoZoneInEventId 		= @OngNoGoZoneInEventId 
				,@OngNoGoZoneInAlarmId  	= @OngNoGoZoneInAlarmId  
				,@OngDisconnectedEventId  	= @OngDisconnectedEventId 
				,@OngDisconnectedAlarmId  	= @OngDisconnectedAlarmId 
				,@OngZoneOutEventId  		= @OngZoneOutEventId  
				,@OngZoneOutAlarmId  		= @OngZoneOutAlarmId  
				,@OngNoMovingEventId  		= @OngNoMovingEventId  
				,@OngNoMovingAlarmId  		= @OngNoMovingAlarmId  
				,@OngMovingEventId  		= @OngMovingEventId  
				,@OngMovingAlarmId  		= @OngMovingAlarmId  
				,@OngLowBatteryEventId  	= @OngLowBatteryEventId  
				,@OngLowBatteryAlarmId 		= @OngLowBatteryAlarmId 
				
				,@currentEventId			= @RaisingEventId				--ok
				,@CurrentEventType 			= @AlarmType					--ok
				,@CurrentEventDateUtc 		= @RaisingEventDateUtc			--ok 
				,@GeneratedAlarmId			= @currentId					--ok 
				,@GeneratedAlarmSeverity 	= @AlarmSeverity				--ok 
				,@GeneratedWarningDateUtc 	= @CurrentWarningId				--ok
				,@GeneratedAlarmDateUtc 	= @RaiseDateUtc					--ok
		END
		IF @TransmitterId IS NOT NULL
		BEGIN
			UPDATE ev_TransmitterStatus SET
				OngDisconnectedAlarmId = @currentId
				,OngDisconnectedAlarmSeverity = @AlarmSeverity
			WHERE TransmitterId = @TransmitterId
		END
		DELETE dbo.ev_EventsToRaise WHERE Id = @ToRaiseId
		SELECT @CurrentWarningId = NULL, @CurrentAlarmId = NULL
	END
END