﻿
create procedure tmp_GetPatients
as 
begin
	SELECT 1 AS Id, 'Kowalski' AS LastName, 'Jan' AS Name, 4 AS DepartmentId, 8 AS RoomId, '2017-01-01 12:12:12' AS InDate
	UNION
	SELECT 2, 'Nowak', 'Janina', 2, 9, '2017-03-03 11:11:11'
end