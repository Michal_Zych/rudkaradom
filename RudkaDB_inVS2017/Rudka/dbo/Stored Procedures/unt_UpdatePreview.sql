﻿

CREATE PROCEDURE [dbo].[unt_UpdatePreview]
	@unitId int,
	@pictureId int
as
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @PREVIEW_WITHOUT_BACKGROUND int = -1

	UPDATE dbo.Units
	SET PreviewPictureID = NULLIF(@pictureId, @PREVIEW_WITHOUT_BACKGROUND)
	WHERE ID = @UnitId

	DELETE dbo.PreviewsUnits
	WHERE PreviewedUnitID = @unitId
END

