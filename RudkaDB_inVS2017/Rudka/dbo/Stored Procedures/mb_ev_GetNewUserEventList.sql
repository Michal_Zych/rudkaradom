﻿-- =============================================
-- Author:		kt
-- Create date: 2017-11-29
-- Description:	gets events for user (staff)
-- =============================================
CREATE PROCEDURE [dbo].[mb_ev_GetNewUserEventList]
	@userId int,
	@lastId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		e.Id,
		Type,
		Severity,
		DateUtc AS StartTime,
		EndDateUtc AS EndTime,
		p.Name + ' ' + p.LastName AS PatientDisplayName,
		p.Id AS PatientId
	FROM dbo.ev_Events e
	INNER JOIN enum.ObjectType ot ON 1 = 1
	INNER JOIN dbo.Patients p on e.ObjectId = p.id
	WHERE
		e.ObjectType = ot.Patient
		and e.Id >= @lastId
	ORDER BY
		e.Id DESC

END