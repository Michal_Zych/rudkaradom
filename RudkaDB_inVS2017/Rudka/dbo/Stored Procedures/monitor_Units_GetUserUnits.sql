﻿-- =============================================
-- Author:		pi
-- Create date: 10,10,2012
-- Description: Returns Units For User (not filtered)
-- =============================================
CREATE PROCEDURE [dbo].[monitor_Units_GetUserUnits] @userId INT
AS
BEGIN
	SET NOCOUNT ON;

	select *
	from Units_GetUserUnits_Func(@userId)

END