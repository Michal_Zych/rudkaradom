﻿CREATE PROCEDURE [dbo].[ev_AssignBandToObject_core]
	@OperatorId int
	,@OperationDateUtc datetime = null
	,@Reason nvarchar(max) = null

	,@BandId smallint = null
	,@BarCode nvarchar(100)
	,@ObjectId dbo.OBJECT_ID 
	,@ObjectType dbo.OBJECT_TYPE
	
	,@IsExternalEvent bit = 0

	,@ForceAssign bit = 0 --JZ
AS
BEGIN
	SET NOCOUNT OFF;

	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
		BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'BandId=' + COALESCE(CONVERT(nvarchar(100), @BandId), 'NULL') 
		EXEC Log_Action @@PROCID, @info
	END
	/****************************************************/


	DECLARE @UPDATE_ENDING_EVENT_ID bit = 1
		,@OBJECT_TYPE_BAND dbo.OBJECT_TYPE
		,@OBJECT_TYPE_PATIENT dbo.OBJECT_TYPE

	DECLARE
		 @currentDateUtc dateTime = dbo.GetCurrentUtcDate()
		,@eventId int
		,@objectBandId smallint
		,@objectAssignDateUtc dateTime
		,@ObjectUnitId int
		,@ObjectCurrentUnitId int
		,@ObjectInCurrentUnitUtc dateTime
		,@objectBandUnitId int

		,@bandObjectId int
		,@bandUnitId int
		,@bandCurrentUnitId int
		,@bandObjectType dbo.OBJECT_TYPE
		,@bandObjectUnitId int

		,@objectAssigned bit
		,@bandAssigned bit

		,@json nvarchar(max)
		,@stateWasSet bit = 0
		,@errorMessage nvarchar(max)

	IF @OperationDateUtc IS NULL SET @OperationDateUtc = @currentDateUtc 
	SELECT @OBJECT_TYPE_BAND = Band, @OBJECT_TYPE_PATIENT = Patient
	FROM enum.ObjectType

	BEGIN  -- pobranie Id opaski na podstawie kodu
		IF @BandId IS NULL AND @BarCode IS NOT NULL
		BEGIN
			SELECT @BandId = dbo.GetBandByCode(@BarCode)
			IF @BandId IS NULL
			BEGIN
				SELECT @errorMessage = 'Nie znaleziono opaski o kodzie: ' + @BarCode 
				RAISERROR(@errorMessage, 16, 1)
			END
		END
	END

	BEGIN -- sprawdzenie czy to nie odebranie opaski
		IF @objectId IS NULL AND @objectType IS NULL
			SELECT @objectId = @BandId, @ObjectType = @OBJECT_TYPE_BAND
	END

	BEGIN -- pobranie danych objektu 
		IF @ObjectId IS NULL
			RAISERROR('Brak identyfikatora obiektu', 16, 1)
		
		IF (@ObjectType  IS NULL) OR (@ObjectType <> @OBJECT_TYPE_BAND AND @ObjectType <> @OBJECT_TYPE_PATIENT)
		BEGIN
			SELECT @errorMessage = 'Niewłaściwy typ obiektu ObjectType = ' + COALESCE(CONVERT(nvarchar(10), @ObjectType), 'NULL')
			
		END

		SELECT @ObjectUnitId = dbo.GetObjectUnitId(@ObjectType, @ObjectId)
		SELECT @objectBandId = bs.BandId, @ObjectInCurrentUnitUtc = bs.InCurrentUnitUtc, @ObjectCurrentUnitId= bs.CurrentUnitId, @objectBandUnitId = b.UnitId
		FROM dbo.ev_BandStatus bs
		JOIN dbo.Bands b on bs.BandId = b.Id 
		WHERE ObjectId = @ObjectId AND ObjectType = @ObjectType

		IF COALESCE(@objectBandId, -1) = COALESCE(@BandId, -1)
		BEGIN
			SELECT @errorMessage = 'Opaska ID=' + COALESCE(CONVERT(nvarchar(10), @BandId), 'NULL') + ', Kod kreskowy=' + COALESCE(@BarCode, 'NULL') + ' została już wcześniej przypisana do tego obiektu.' 
			RAISERROR(@errorMessage, 16, 1)
		END
	END


	BEGIN -- pobranie danych opaski
		

		IF @BandId IS NOT NULL
			SELECT @bandObjectId = bs.ObjectId, @bandObjectType = bs.ObjectType, @bandObjectUnitId = bs.ObjectUnitId, @bandUnitId = b.UnitId, @bandCurrentUnitId = bs.CurrentUnitId
			FROM dbo.ev_BandStatus bs
			JOIN dbo.Bands b on bs.BandId = b.Id
			WHERE bs.BandId = @BandId

		IF @ForceAssign = 0 -- sprawdzenie czy nie jest przypisana a nie wolno zmienić --JZ
		BEGIN 
			IF @bandObjectType = @OBJECT_TYPE_PATIENT 
			BEGIN
				SELECT @errorMessage = 'Opaska ID=' + COALESCE(CONVERT(nvarchar(10), @BandId), 'NULL') + ', Kod kreskowy=' + COALESCE(@BarCode, 'NULL') + ' jest przypisana do pacjenta: ' +  COALESCE(CONVERT(nvarchar(max),(SELECT Name + ' ' + SUBSTRING(LastName, 1, 1) + '.' FROM Patients WHERE Id = @bandObjectId)), 'NULL')
				RAISERROR(@errorMessage, 16, 1)
			END
		END

	END

	IF @objectAssignDateUtc > @OperationDateUtc  -- system posiada bardziej aktualne przypisanie
	BEGIN
		IF @IsExternalEvent = 1 EXEC dbo.sync_BandAssigned @ObjectId, @ObjectType, @BandId, @objectAssignDateUtc, @OperatorId
		SELECT CONVERT(int, null) AS Result, -1 AS ErrorCode
			, 'System posiada świeższe dane o przypisaniu opaski Id=' + CONVERT(nvarchar(10), @BandId) AS ErrorMessage
		RETURN
	END
	
	IF @objectBandId IS NULL SET @objectAssigned = 0 ELSE SET @objectAssigned = 1
	IF @BandId IS NOT NULL
	BEGIN
		IF @bandObjectType IS NULL OR @bandObjectType = @OBJECT_TYPE_BAND 
			SET @bandAssigned = 0
		ELSE 
			SET @bandAssigned = 1
	END

	/*
	IF @bandAssigned IS NULL 
		IF @objectAssigned = 1 SELECT '1 Odbierz obiektowi opaskę'
		ELSE SELECT '2 BEZ ZMIAN'
	ELSE IF @bandAssigned = 1
		IF @objectAssigned = 1 SELECT '3 odepnij staryObiekt od opaski, odepnij starąOpaskę od obiektu, przypisz opaskę obiektowi'
		ELSE SELECT '4 odepnij staruObiekt od opaski, przpisz opaskę obiektowi'
	ELSE
		IF @objectAssigned = 1 SELECT '5 odepnij starąOpaskę od obiektu, przypisz opaskę obiektowi'
		ELSE SELECT '6 przypisz opaskę obiektowi'
	*/

	-- bez zmian
	IF @bandAssigned IS NULL AND @objectAssigned = 0 
		RETURN

	IF @bandAssigned = 1
	BEGIN
		IF @IsExternalEvent = 0
			EXEC dbo.sync_BandAssigned @bandObjectId, @bandObjectType, NULL, @OperationDateUtc, @OperatorId
		-- event przypiszOpaskę BandObject <- null
		IF @bandObjectType = @OBJECT_TYPE_PATIENT UPDATE dbo.Patients SET BandId = null, BandDateUtc = @OperationDateUtc WHERE Id = @bandObjectId

		SELECT @json = 'SELECT null AS Band'
		EXEC @json = dbo.SerializeJSON @json
		INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason)
		SELECT @OperationDateUtc, eventType.BandAssignment, eventSeverity.Event, @bandObjectUnitId, @bandObjectId, @bandObjectType, @bandObjectUnitId, @currentDateUtc
			, @OperationDateUtc, @OperatorId, @json, null
		FROM enum.EventType eventType
		JOIN enum.EventSeverity eventSeverity ON 1 = 1
		IF @UPDATE_ENDING_EVENT_ID = 1
		BEGIN
			SELECT @eventId = SCOPE_IDENTITY()
			UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
		END
	END

	IF @objectAssigned = 1
	BEGIN
		IF @IsExternalEvent = 0
			EXEC dbo.sync_BandAssigned NULL, NULL, @objectBandId, @operationDateUtc, @OperatorId
		-- poprzedni status do nowej opaski
		IF @BandId IS NOT NULL
		BEGIN
			DELETE dbo.ev_BandStatus WHERE Bandid = @BandId
			UPDATE dbo.ev_BandStatus SET
				BandId = @BandId
				,AssignDateUtc = @OperationDateUtc
			WHERE BandId = @objectBandId
			SET @stateWasSet = 1
			
			IF @objectBandId IS NOT NULL  -- przypisz objectBand samą sobie
			BEGIN
				IF @IsExternalEvent =  0
					Exec dbo.sync_BandAssigned @objectBandId, @OBJECT_TYPE_BAND, @objectBandId, @operationDateUtc, @OperatorId
				
				INSERT dbo.ev_BandStatus(BandId, ObjectType, ObjectId, AssignDateUtc, ObjectUnitId, CurrentUnitId, InCurrentUnitUtc)
					SELECT @objectBandId, objectType.Band, @objectBandId, @OperationDateUtc, @objectBandUnitId, @ObjectCurrentUnitId, @ObjectInCurrentUnitUtc
					FROM enum.ObjectType objectType
				SET  @json = 'SELECT Id AS BandId, MacString AS MAC, Uid, BarCode FROM dbo.Bands WHERE Id = ' + CONVERT(nvarchar(10), @objectBandId)
				EXEC @json = dbo.SerializeJSON @json
				INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason)
				SELECT @OperationDateUtc, eventType.BandAssignment, eventSeverity.Event, @ObjectUnitId, @objectBandId, @OBJECT_TYPE_BAND, @objectBandUnitId, @currentDateUtc, @OperationDateUtc, @OperatorId, @json, 'Obiektowi przypisano inną opaskę.'
				FROM enum.EventType eventType
				JOIN enum.EventSeverity eventSeverity ON 1 = 1
				IF @UPDATE_ENDING_EVENT_ID = 1
				BEGIN
					SELECT @eventId = SCOPE_IDENTITY()
					UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
				END
			END
		END
	END

	BEGIN  -- przypisz nową opaskę do obiektu
		IF @IsExternalEvent =  0
			Exec dbo.sync_BandAssigned @Objectid, @ObjectType, @BandId, @operationDateUtc, @OperatorId
		
		IF @ObjectType = @OBJECT_TYPE_PATIENT UPDATE dbo.Patients SET BandId = @BandId, BandDateUtc = @OperationDateUtc WHERE Id = @ObjectId

		IF @BandId IS NULL 
			SET @json = 'SELECT null AS Band'
		ELSE 
			SET  @json = 'SELECT Id AS BandId, MacString AS MAC, Uid, BarCode FROM dbo.Bands WHERE Id = ' + CONVERT(nvarchar(10), @BandId)
		EXEC @json = dbo.SerializeJSON @json
		--zdarzenie przypisania opaski
		INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason)
		SELECT @OperationDateUtc, eventType.BandAssignment, eventSeverity.Event, @ObjectUnitId, @ObjectId, @ObjectType, @ObjectUnitId, @currentDateUtc, @OperationDateUtc, @OperatorId, @json, @Reason
		FROM enum.EventType eventType
		JOIN enum.EventSeverity eventSeverity ON 1 = 1
		IF @UPDATE_ENDING_EVENT_ID = 1
		BEGIN
			SELECT @eventId = SCOPE_IDENTITY()
			UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
		END

		IF @stateWasSet = 0
		BEGIN
			DELETE dbo.ev_BandStatus WHERE BandId = @BandId
			IF @bandAssigned IS NULL   -- status opaski wskazuje na siebie
				BEGIN
					DELETE dbo.ev_BandStatus WHERE BandId = @objectBandId
					INSERT dbo.ev_BandStatus(BandId, ObjectType, ObjectId, AssignDateUtc, ObjectUnitId, CurrentUnitId, InCurrentUnitUtc)
					SELECT @objectBandId, objectType.Band, @objectBandId, @OperationDateUtc, @objectBandUnitId, @ObjectCurrentUnitId, @ObjectInCurrentUnitUtc
					FROM enum.ObjectType objectType
				END
			ELSE
				INSERT dbo.ev_BandStatus(BandId, ObjectType, ObjectId, AssignDateUtc, ObjectUnitId, CurrentUnitId, InCurrentUnitUtc) VALUES
						(@BandId, @ObjectType, @ObjectId, @OperationDateUtc, @objectUnitId, @bandCurrentUnitId, @OperationDateUtc)
		END
	END
	
END