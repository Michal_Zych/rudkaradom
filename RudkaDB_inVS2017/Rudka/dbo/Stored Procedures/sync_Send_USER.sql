﻿CREATE PROCEDURE [dbo].[sync_Send_USER]
	@ExtUserId nvarchar(100)
	,@UserId int = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE 
		@RECORD_TYPE char(4) = 'USER'

	DECLARE 
		@record nvarchar(1024) = ''

	IF @ExtUserId IS NULL
		SELECT @ExtUserId = ExtId FROM arch.ext_Users WHERE  M2mId = @UserId

	IF @ExtUserId IS NOT NULL
	BEGIN
		SELECT @record = 'USER_ID=''' + @ExtUserId + ''''
		EXEC dbo.sync_SendRecord @RECORD_TYPE, @record
	END
END