﻿-- =============================================
-- Author:		kt
-- Create date: 2018-02-15
-- Description:	gets calls for incomplete alarms
-- =============================================
CREATE PROCEDURE [dbo].[dial_ev_GetCalls]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		o.EventId AS AlarmId,
		l.Number,
		o.Message,
		l.Time,
		l.Status
	FROM
		dbo.ev_CallOrders o 
		INNER JOIN dbo.ev_CallOrdersLog l on o.EventId = l.EventId
	WHERE
		l.Type = 0; -- actual calls

END