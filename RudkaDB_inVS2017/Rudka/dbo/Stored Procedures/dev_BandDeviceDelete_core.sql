﻿CREATE PROCEDURE [dbo].[dev_BandDeviceDelete_core] 
	@BandId int
AS  
BEGIN

	DELETE dbo.dev_Sensors
	FROM dbo.dev_Sensors s
	JOIN dbo.Bands b ON s.SenseId = b.Mac
	WHERE b.id = @BandId
	
	DELETE dbo.Sensors
	FROM dbo.Sensors se
	JOIN dbo.Sockets s ON s.Id = se.SocketId
	JOIN dbo.dev_Sensors dsr ON s.SenseSensorId = dsr.Id
	JOIN dbo.dev_Senses ds ON dsr.senseid = ds.Id
	JOIN dbo.Bands b ON ds.Id = b.Mac
	WHERE b.id = @bandId
	
	DELETE dbo.Sockets
	FROM dbo.Sockets s
	JOIN dbo.dev_Sensors dsr ON s.SenseSensorId = dsr.Id
	JOIN dbo.dev_Senses ds ON dsr.senseid = ds.Id
	JOIN dbo.Bands b ON ds.Id = b.Mac
	WHERE b.id = @bandId
	
	DELETE dbo.dev_Sensors
	FROM dbo.dev_Sensors dsr
	JOIN dbo.dev_Senses ds ON dsr.senseid = ds.Id
	JOIN dbo.Bands b ON ds.Id = b.Mac
	WHERE b.id = @bandId
	
	
	DELETE dbo.dev_Senses
	FROM dbo.dev_Senses ds
	JOIN dbo.Bands b ON ds.Id = b.Mac
	WHERE b.id = @bandId
END