﻿-- =============================================
-- Author:		pi
-- Create date: 23-10-2012
-- Description:	DeacivateUser
-- =============================================
CREATE PROCEDURE [dbo].[Users_Deactivate]
	@userId int
AS
BEGIN
	SET NOCOUNT ON;

	--deactivate
	UPDATE Users
	SET Active = 0
	WHERE ID = @userId

	--deactivate basket (basket should be empty)
	UPDATE Units
	Set Active = 0
	WHERE UserId = @userId


END

