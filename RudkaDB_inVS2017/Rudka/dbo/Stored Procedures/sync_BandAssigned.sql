﻿
CREATE PROCEDURE [dbo].[sync_BandAssigned] 
	@ObjectId dbo.OBJECT_ID
	,@ObjectType dbo.OBJECT_TYPE
	,@BandId smallint
	,@OperationDateUtc datetime
	,@operatorId int
AS 
BEGIN
	SET NOCOUNT ON;
	
	IF @OperationDateUtc IS NULL SET @OperationDateUtc = dbo.GetCurrentUTCDate()

	DECLARE
		@bandOwner int
		,@bandDateUtc dateTime
		,@assignPatientId int
		,@unAssignPatientId int

	SELECT @bandOwner = Id, @bandDateUtc = BandDateUtc FROM dbo.Patients WHERE BandId = @BandId

	IF @ObjectType = (SELECT Patient FROM enum.ObjectType)
	BEGIN
		IF @ObjectId <> @bandOwner
		BEGIN
			IF @bandDateUtc < @OperationDateUtc
				SELECT @unAssignPatientId = @bandOwner, @assignPatientId = @ObjectId
			ELSE
				EXEC dbo.sync_Send_TAG @bandOwner, @operatorId, @bandDateUtc
		END
	END
	ELSE
	BEGIN
		IF @bandOwner IS NOT NULL AND COALESCE(@bandDateUtc, '01-01-01') < @OperationDateUtc
			SELECT @unAssignPatientId = @bandOwner
	END


	UPDATE dbo.Patients SET
		@BandId = CASE WHEN Id = @unAssignPatientId THEN NULL ELSE @BandId END
		,BandDateUtc = @OperationDateUtc
	WHERE Id IN (@unAssignPatientId, @assignPatientId)

	IF @unAssignPatientId IS NOT NULL
		EXEC dbo.sync_Send_TAG @unAssignPatientId, @operatorId, @OperationDateUtc

	IF @assignPatientId IS NOT NULL
		EXEC dbo.sync_Send_TAG @assignPatientId, @operatorId, @OperationDateUtc
END