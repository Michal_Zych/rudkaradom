﻿



CREATE PROCEDURE [dbo].[usr_GetLoggedUsers]
	@clientId int

AS
BEGIN
	SET NOCOUNT ON;

	SELECT ClientId, Unit, UserId, Login, COALESCE(LastName, '') + ', ' + COALESCE(Name, '') AS [User]
		,LoggingDate, LastPingDate, Phone
	FROM [dbo].[m2m_LoggedUsers]
	WHERE (ClientID = @clientId ) OR (@clientId IS NULL)
	ORDER BY LastName, Name
END