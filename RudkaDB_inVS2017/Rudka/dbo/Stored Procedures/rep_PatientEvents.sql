﻿-- zdarzenia pacjenta
CREATE PROCEDURE [dbo].[rep_PatientEvents]
	@UserReportId int 
AS
BEGIN
	DECLARE @REPORT_TYPE tinyint = 2 -- zdarzenia pacjenta

	DECLARE
		@dateStart datetime
		,@dateEnd datetime
		,@patientId int		
	
		,@currentDate dateTime = dbo.GetCurrentDate()
		,@currentUtcDate dateTime = dbo.GetCurrentUtcDate()

	SELECT @dateStart = DATEADD(MI, DATEDIFF(MI, @currentDate, @currentUtcDate), DateFrom) 
		,@dateEnd = DATEADD(MI, DATEDIFF(MI, @currentDate, @currentUtcDate), DateTo)
		,@patientId = UnitId
	FROM dbo.UsersReports WHERE Id = @UserReportId


	IF @dateStart IS NULL SET @dateStart = CONVERT(datetime, 1)
	IF @dateEnd IS NULL SET @dateEnd = DATEADD(YEAR, 1, @currentDate)

	SELECT * FROM 
		(SELECT DateUtc 
			,DATEADD(MI, DATEDIFF(MI, @currentUtcDate, @currentDate), e.DateUtc) AS Date
			,et.Description as EventType
			,es.Description as EventSeverity
			,COALESCE(dbo.GetUnitLocation(e.UnitId, 1, 0, default), 'nieznana') AS Location
		FROM dbo.ev_Events e
		JOIN enum.ObjectType objectType ON e.ObjectType = objectType.Patient
		JOIN dbo.ReportTypeEvents rte ON rte.TypeId = @REPORT_TYPE AND e.Type = rte.EventTypeId
		JOIN dbo.enum_EventTypeDictionaryPl et ON et.Value = e.Type
		JOIN dbo.enum_EventSeverityDictionaryPl es ON es.Value = e.Severity
		WHERE e.ObjectId = @patientId
			AND DateUtc BETWEEN @dateStart AND @dateEnd
		UNION ALL
		SELECT DateUtc 
			,DATEADD(MI, DATEDIFF(MI, @currentUtcDate, @currentDate), e.DateUtc) AS Date
			,et.Description as EventType
			,es.Description as EventSeverity
			,COALESCE(dbo.GetUnitLocation(e.UnitId, 1, 0, default), 'nieznana') AS Location
		FROM arch.ev_Events e
		JOIN enum.ObjectType objectType ON e.ObjectType = objectType.Patient
		JOIN dbo.ReportTypeEvents rte ON rte.TypeId = @REPORT_TYPE AND e.Type = rte.EventTypeId
		JOIN dbo.enum_EventTypeDictionaryPl et ON et.Value = e.Type
		JOIN dbo.enum_EventSeverityDictionaryPl es ON es.Value = e.Severity
		WHERE e.ObjectId = @patientId
			AND DateUtc BETWEEN @dateStart AND @dateEnd
		) a
	ORDER BY DateUtc
END