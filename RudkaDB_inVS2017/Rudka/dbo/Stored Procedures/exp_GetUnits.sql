﻿

-- =============================================
-- Author:		
-- Create date: 
-- Description:	zwraca magazyny danego klienta
-- =============================================
CREATE PROCEDURE dbo.[exp_GetUnits]
		@ClientID int		
AS
BEGIN
	SET NOCOUNT ON;
		
	SELECT ID, Name, Description, ParentUnitID
	FROM dbo.Units
	WHERE ClientID = @ClientID 
		AND Active = 1 
	ORDER BY ID
END


