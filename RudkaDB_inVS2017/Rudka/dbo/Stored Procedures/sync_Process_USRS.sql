﻿CREATE PROCEDURE [dbo].[sync_Process_USRS]
	@RecordId int 
AS
BEGIN
	INSERT _log(LogDate, ProcedureName, Info)
	values (dbo.GetCurrentDate(), 'sync_Process_USRS', @RecordId)
	
	SET NOCOUNT ON;
	DECLARE @KEY_ID nvarchar(20)	= 'USERID'
		,@KEY_LOGIN nvarchar(20)	= 'LOGIN'
		,@KEY_NAME nvarchar(20)		= 'NAME'
		,@KEY_LASTNAME nvarchar(20) = 'SURENAME'
		,@KEY_ACTIVE nvarchar(20)	= 'ACTIVE'



	DECLARE 
		@RECORD_SEPARATOR char(1) = CHAR(10)
		,@ASSECO_OPERATOR_ID int = 0
		,@REASON nvarchar(100) = 'Źródło AMMS INRecordId=' + CONVERT(nvarchar(10), @RecordId)
		,@OperationDateUtc dateTime = dbo.GetCurrentUtcDate()

	DECLARE @extUsers TABLE(ExtId nvarchar(10), Login nvarchar(200), Name nvarchar(200), LastName nvarchar(200), Active bit, Record nvarchar(max))
	DECLARE @toProcess TABLE(Login nvarchar(100))
	DECLARE @result TABLE(Id int)
	
	
	DECLARE @record nvarchar(max)
		,@login nvarchar(100)
		,@id int
		,@password USER_PASSWORD
		,@name USER_NAME
		,@lastName USER_NAME
		,@phone USER_PHONE
		,@eMail USER_EMAIL
		,@extId nvarchar(100)

	SELECT @record = Record FROM tech.sync_RecordsIN WHERE Id = @RecordId
	
	INSERT @extUsers(ExtId, Login, Name, LastName, Active, Record)
	SELECT LEFT(dbo.srv_KeyValue_CI_GetValue(Value, @KEY_ID), 200) 
			,LEFT(dbo.srv_KeyValue_CI_GetValue(Value, @KEY_LOGIN), 200) 
			,LEFT(dbo.srv_KeyValue_CI_GetValue(Value, @KEY_NAME), 200) 
			,LEFT(dbo.srv_KeyValue_CI_GetValue(Value, @KEY_LASTNAME), 200) 
			,CONVERT(bit, LEFT(dbo.srv_KeyValue_CI_GetValue(Value, @KEY_ACTIVE), 200))
			,Value
	FROM dbo.fn_Split(@record, @RECORD_SEPARATOR) 

	--EDYCJA
	INSERT @toProcess
	SELECT u.Login
	FROM dbo.Users u
	JOIN @extUsers ex ON u.Login = ex.Login
	WHERE u.Name <> ex.Name
		OR u.LastName <> ex.LastName

	SELECT TOP 1 @login = Login FROM @toProcess
	WHILE @login IS NOT NULL
	BEGIN
		SELECT @id = u.Id, @password = u.Password, @name = ex.Name, @lastName = ex.LastName, @phone = u.Phone, @eMail = u.eMail, @extId = ex.ExtId, @record = ex.Record
		FROM dbo.Users u
		JOIN @extUsers ex ON u.Login = ex.Login
		WHERE u.Login = @login

		EXEC dbo.usr_EditUser @id, @login, @password, @name, @lastName, @phone, @email, @ASSECO_OPERATOR_ID, @REASON, @REASON

		DELETE arch.ext_Users WHERE ExtId = @extId
		INSERT arch.ext_Users (ExtId, M2mId, ExtDescription) VALUES (@extId, @id, @record)

		DELETE @toProcess WHERE Login = @login
		SET @login = NULL
		SELECT TOP 1 @login = Login FROM @toProcess
	END

	-- CREATE
	DELETE @toProcess
	INSERT @toProcess
	SELECT Login
	FROM @extUsers
	WHERE Login NOT IN (SELECT Login FROM dbo.Users)

	SELECT TOP 1 @login = Login FROM @toProcess
	WHILE @login IS NOT NULL
	BEGIN
		SELECT @name = Name, @lastName = LastName, @extId = ExtId, @record = Record
		FROM @extUsers
		WHERE Login = @login

		DELETE @result
		INSERT @result
		EXEC dbo.usr_CreateUser @login, 'nie ustawiono', 1, @name, @lastName, null, null, 1,  @ASSECO_OPERATOR_ID, @REASON, @REASON
		SELECT @id = Id FROM @result

		DELETE arch.ext_users WHERE ExtId = @extId
		INSERT arch.ext_users(ExtId, m2mId, ExtDescription) VALUES(@extId, @id, @record)

		DELETE @toProcess WHERE Login = @login
		SET @login  = NULL
		SELECT TOP 1 @login = Login FROM @toProcess
	END

	--DELETE
	DELETE @toProcess
	INSERT @toProcess
	SELECT Login
	FROM @extUsers
	WHERE Active = 0

	SELECT TOP 1 @login = Login FROM @toProcess
	WHILE @login IS NOT NULL
	BEGIN
		SELECT @extId = ex.ExtId, @record = ex.Record, @id = u.Id
		FROM @extUsers ex
		JOIN dbo.Users u ON u.Login = ex.Login
		WHERE ex.Login = @login

		EXEC dbo.usr_DeleteUser @id, @ASSECO_OPERATOR_ID, @REASON

		DELETE arch.ext_users WHERE ExtId = @extId
		INSERT arch.ext_users(ExtId, m2mId, ExtDescription) VALUES(@extId, @id, @record)

		DELETE @toProcess WHERE Login = @login
		SET @login  = NULL
		SELECT TOP 1 @login = Login FROM @toProcess
	END

	UPDATE tech.sync_RecordsIN SET
		Processed = 1
		,Try = COALESCE(Try, 0) + 1
		,ProcessedDateUtc = @OperationDateUtc
	WHERE Id = @RecordId
END