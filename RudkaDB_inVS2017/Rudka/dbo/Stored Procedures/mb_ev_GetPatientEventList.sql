﻿-- =============================================
-- Author:		kt
-- Create date: 2017-11-23
-- Description:	gets events of patient
-- =============================================
CREATE PROCEDURE [dbo].[mb_ev_GetPatientEventList]
	@bandId int,
	@daysBack int = 20
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 100
		e.Id,
		Type,
		Severity,
		DateUtc AS StartTime,
		EndDateUtc AS EndTime,
		p.Id AS PatientId,
		u.[Name] AS UnitName,
		pu.[Name] AS ParentUnitName,
		COALESCE(a.EventId, 0) AS isAlarm,
		COALESCE(a.IsClosed, 0) AS isClosed,
		a.ClosingDateUtc AS ClosingDate
	FROM dbo.ev_Events e
	INNER JOIN enum.ObjectType ot ON 1 = 1
	INNER JOIN dbo.Patients p on e.ObjectId = p.id
	LEFT JOIN dbo.Units u ON e.UnitId = u.id
	LEFT JOIN dbo.Units pu ON u.ParentUnitID = pu.ID
	LEFT JOIN dbo.ev_Alarms a ON a.EventId = e.Id
	WHERE
		e.ObjectType = ot.Patient
		AND e.ObjectId = @bandId
		AND Type <> 18 -- wyjscie ze strefy
		AND DateUtc > DATEADD(DD, -1 * @daysBack, dbo.GetCurrentDate())
		
	UNION 

	SELECT 
		e.Id,
		Type,
		Severity,
		DateUtc AS StartTime,
		EndDateUtc AS EndTime,
		p.Id AS PatientId,
		u.[Name] AS UnitName,
		pu.[Name] AS ParentUnitName,
		e.Id AS isAlarm,
		COALESCE(a.IsClosed, 0) AS isClosed,
		a.ClosingDateUtc AS ClosingDate
	FROM dbo.ev_Events e
	INNER JOIN enum.ObjectType ot ON 1 = 1
	INNER JOIN dbo.Patients p on e.ObjectId = p.id
	INNER JOIN dbo.ev_BandStatus bs ON e.Id = bs.TopAlarmId
	LEFT JOIN dbo.Units u ON e.UnitId = u.id
	LEFT JOIN dbo.Units pu ON u.ParentUnitID = pu.ID
	LEFT JOIN dbo.ev_Alarms a ON a.EventId = e.Id
	WHERE
		e.ObjectType = ot.Patient
		AND e.ObjectId = @bandId
	
	ORDER BY
		e.Id DESC


END