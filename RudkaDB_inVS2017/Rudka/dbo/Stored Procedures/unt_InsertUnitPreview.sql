﻿CREATE PROCEDURE dbo.unt_InsertUnitPreview
	@PreviewedUnitId int
	,@InsertedUnitId int
AS
BEGIN
	DELETE dbo.PreviewsUnits
	WHERE PreviewedUnitID = @PreviewedUnitId
		AND UnitID IN (SELECT UnitId FROM dbo.PreviewsUnits WHERE PreviewedUnitID = @InsertedUnitId)

	DECLARE
		@tlx real, @tly real, @brx real, @bry real
		,@sx real, @sy real

	SELECT @tlx = TLX, @tly = TLY, @brx = BRX, @bry = BRY
	FROM dbo.PreviewsUnits 
	WHERE PreviewedUnitID = @PreviewedUnitId AND UnitID = @InsertedUnitId

	SELECT @sx = @brx - @tlx, @sy = @bry - @tly


	INSERT dbo.PreviewsUnits
	SELECT @PreviewedUnitId, UnitID, @tlx + @sx * TLX, @tly + @sy * TLY, @tlx + @sx * brx, @tly + @sy * BRY
	FROM dbo.PreviewsUnits
	WHERE PreviewedUnitID = @InsertedUnitId
END