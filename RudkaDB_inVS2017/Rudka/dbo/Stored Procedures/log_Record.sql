﻿CREATE PROCEDURE [dbo].[log_Record](
	@ObjectID int
	,@table varchar(50)
	,@id bigint
)
AS
BEGIN
	DECLARE @schema varchar(10)
			,@tableName varchar(50)
			,@keyColumn varchar(50)
			,@i integer
	SET @i = PATINDEX('%.%', @table)
	SET @schema = CASE WHEN @i = 0 THEN 'dbo' ELSE SUBSTRING(@table, 1, @i - 1) END
	SET @tableName = SUBSTRING(@table, @i + 1, 100)
	
	SELECT @keyColumn = ccu.COLUMN_NAME
    FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
        JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON tc.CONSTRAINT_NAME = ccu.Constraint_name
    WHERE tc.CONSTRAINT_TYPE = 'Primary Key'
		AND tc.TABLE_NAME = @tableName
		AND tc.TABLE_SCHEMA = @schema
	
	IF @tableName = 'UnidataView' SET @keyColumn = 'ID'


	DECLARE @xml_var XML  
	DECLARE @command varchar(max)
	
	SET @command =  'SELECT * FROM ' + @table + ' WHERE ' + @keyColumn + '  = ' 
					+ CONVERT(varchar(20),@Id) + ' FOR XML AUTO, TYPE'
	
	DECLARE @t TABLE (Value XML)
	INSERT INTO @t (Value)
	EXEC(@command)
	
	SELECT @xml_var = Value FROM @t
	
	SET @command = CONVERT(nvarchar(max), @xml_var)
	SET @command = REPLACE(@command, '&quot;', '"')
	SET @command = REPLACE(@command, '&lt;', '<')
	SET @command = REPLACE(@command, '&gt;', '>')
	
	
	
	
	EXEC log_Action @ObjectID, @command
END

