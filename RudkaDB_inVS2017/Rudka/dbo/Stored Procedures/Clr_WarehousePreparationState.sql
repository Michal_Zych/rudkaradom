﻿CREATE PROCEDURE [dbo].[Clr_WarehousePreparationState]
@UserReportID INT
AS EXTERNAL NAME [Finder.BloodDonation.CLR].[StoredProcedures].[Clr_WarehousePreparationState]


GO
EXECUTE sp_addextendedproperty @name = N'AutoDeployed', @value = N'yes', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'PROCEDURE', @level1name = N'Clr_WarehousePreparationState';


GO
EXECUTE sp_addextendedproperty @name = N'SqlAssemblyFile', @value = N'Clr\Clr_WarehousePreparationState.cs', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'PROCEDURE', @level1name = N'Clr_WarehousePreparationState';


GO
EXECUTE sp_addextendedproperty @name = N'SqlAssemblyFileLine', @value = 11, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'PROCEDURE', @level1name = N'Clr_WarehousePreparationState';

