﻿-- =============================================
-- Author:  <Author,,Name>
-- Create date: <Create Date,,>
-- Description: <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[M2MCreateHubAllSensors]
 @name nvarchar(50),
 @ipAddress nvarchar(50),
 @parentunitid int,
 @startAddress int,
 @numDevices int,
 @devtype int
AS
BEGIN
 BEGIN TRANSACTION
  BEGIN TRY
  declare @tab table( value int)
  declare @hubid int
  declare @Unitpid int
  declare @Unitid int

  if exists(select 1 from hubs where IPAddress=@ipAddress and active=1)
  begin
   set @hubid=(select top(1) id from hubs where IPAddress=@ipAddress and active=1)
  end else begin
   insert into hubs values(@ipaddress,1001,0)

   set @hubid=SCOPE_IDENTITY()

  end

insert into @tab exec unt_UnitCreate @name,@parentunitid,1,2,null,null,0,null,null

set @unitpid=(select top(1) value from @tab)
declare @I int=@startAddress
declare @devname nvarchar(10)
while(@numDevices>0)
begin
 delete from @tab
 set @devname='D'+convert(nvarchar(10) ,@I)
 insert into @tab exec unt_UnitCreate @devname,@unitpid,1,2,null,null,0,null,null

 set @unitid=(select top(1) value from @tab)
 if (@devtype=1)
 begin
  exec M2MCreateDeviceAndSensors @hubid, @I,@devtype,'ch1,ch2,ch3,ch4,ch5,ch6,ch7,ch8',@unitid
 end
 else
  if (@devtype=4)
 begin
  exec M2MCreateDeviceAndSensors @hubid, @I,@devtype,'Time,TEMP,pH,pO2,O2_T,N2_T,CO2_T',@unitid
 end
 else
 begin
  exec M2MCreateDeviceAndSensors @hubid, @I,@devtype,'wilg,temp',@unitid
 end

 set @numDevices-=1
 set @i+=1
end


 END TRY
  BEGIN CATCH
   ROLLBACK TRANSACTION
   SELECT
    ERROR_NUMBER() AS ErrorNumber
    ,ERROR_SEVERITY() AS ErrorSeverity
    ,ERROR_STATE() AS ErrorState
    ,ERROR_PROCEDURE() AS ErrorProcedure
    ,ERROR_LINE() AS ErrorLine
    ,ERROR_MESSAGE() AS ErrorMessage;
   RETURN
  END CATCH

 COMMIT TRANSACTION

END