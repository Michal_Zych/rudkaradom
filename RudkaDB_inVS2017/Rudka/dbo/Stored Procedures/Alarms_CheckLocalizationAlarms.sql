﻿
CREATE PROCEDURE [dbo].[Alarms_CheckLocalizationAlarms]
	@UserId int,
	@Units IDs_LIST = null,
	@LowestSeverity int = null  -- zwraca alarmy o severity większym lub równym LowestSeverity
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @AllAlarmsSeverity int = 0

	DECLARE @clientId int = 1


	DECLARE @maxRows int 
	SELECT @maxRows = MaxRowsAlarmsPopup 
		,@LowestSeverity = COALESCE(@LowestSeverity, @AllAlarmsSeverity)
	FROM dbo.ev_Settings
	

	IF @Units IS NULL
		SELECT @Units = CONVERT(nvarchar(10), Id) + ',' + COALESCE(@Units, '') FROM dbo.Units WHERE Active = 1
	ELSE
		SELECT @Units =  CONVERT(nvarchar(10), UnassignedPatientsUnitId) + ',' + @Units from Clients WHERE Id = @clientId


	SELECT DISTINCT TOP (@maxRows)
			a.EventId AS AlarmId
			, e.Severity 
			--, eventSeverity.Description AS [Priority]
			, e.ObjectType 
			, objectType.Description AS ObjectTypeName
			, COALESCE(dbo.GetObjectName(ua.UserId, 1),  dbo.GetObjectName(e.ObjectId, e.ObjectType)) as ObjectName
			, e.Type AS AlarmType
			, COALESCE(ua.Message, eventType.Description)  as AlarmDescription
			, dbo.GetUnitLocation(e.UnitId, 1, 0, default) as AlarmLocation
			, e.DateUtc AS AlarmDateUtc
			, e.EndDateUtc AS AlarmEndDateUtc
		FROM dbo.ev_Alarms a
		JOIN dbo.ev_Events e ON a.EventId = e.Id
		LEFT JOIN dbo.ev_UsersAlarms ua ON ua.EventId = a.EventId
		JOIN dbo.enum_ObjectTypeDictionaryPl objectType ON e.ObjectType = objectType.Value
		JOIN dbo.enum_EventSeverityDictionaryPl eventSeverity ON e.Severity = eventSeverity.Value
		JOIN dbo.enum_EventTypeDictionaryPl eventType ON e.Type = eventType.Value
		JOIN dbo.fn_Split(@Units, ',') u ON e.UnitId = u.value OR e.ObjectUnitId = u.value OR (e.UnitId IS NULL AND e.ObjectUnitId IS NULL)
		WHERE a.IsClosed = 0 
			AND e.Severity >= @LowestSeverity
		ORDER BY e.DateUtc
END