﻿
-- =============================================
-- Author:		
-- Create date: 
-- Description:	zwraca dane teleadresowe użytkowników
-- =============================================
CREATE PROCEDURE [dbo].[usr_GetUsersContactsForUnit]
	@unitId int
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @clientId int

	IF @unitId < 0
		SELECT @clientId = ABS(@unitId)
	ELSE SELECT @clientId =  ClientId FROM dbo.Units WHERE ID = @unitId
		
	
	SELECT ID, Name, LastName, Phone, eMail
	FROM dbo.Users
	WHERE Active = 1
		AND ClientID = @clientId
	ORDER BY LastName, Name
END
