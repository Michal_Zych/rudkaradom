﻿CREATE PROCEDURE [dbo].[dev_TransmitterGroupSet]
	@Id tinyint
	,@Name nvarchar(1024)
	,@OperatorId int
	,@Reason nvarchar(max) = null
AS
BEGIN
	SET NOCOUNT OFF;

	DECLARE @UPDATE_ENDING_EVENT_ID bit = 1
	DECLARE @EVENT_CREATE dbo.EVENT_TYPE
		,@EVENT_UPDATE dbo.EVENT_TYPE


	DECLARE @eventType dbo.EVENT_TYPE
		,@currentDate datetime
		,@eventId int

	SELECT @EVENT_CREATE = [Create]
		, @EVENT_UPDATE = [Update] 
		, @currentDate = dbo.GetCurrentUtcDate()
	FROM enum.EventType

	SELECT @eventType = @EVENT_CREATE

	BEGIN TRY
		BEGIN TRANSACTION
			IF @Id IS NOT NULL
			BEGIN
				UPDATE dbo.dev_TransmitterGroups SET Description = @Name
				WHERE [Group] = @Id
				IF @@ROWCOUNT > 0 SET @eventType = @EVENT_UPDATE
			END

			IF @eventType = @EVENT_CREATE
			BEGIN
				IF @Id IS NULL SELECT @Id = COALESCE(MAX([Group]), 0) + 1 FROM dbo.dev_TransmitterGroups
				INSERT dbo.dev_TransmitterGroups([Group], Description) VALUES(@Id, @Name)
			END
				
			-- event	
			DECLARE @JSON nvarchar(max) = 'SELECT [Group] AS Id, Description AS Name FROM dbo.dev_TransmitterGroups WHERE [Group] = ' + CONVERT(nvarchar(10), @Id)
			EXEC @JSON = dbo.SerializeJSON @JSON
			
			INSERT dbo.ev_Events(DateUtc, Type, Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason) 
			SELECT @currentDate, @eventType, eventSeverity.Event, null, @Id, objectType.TransmitterGroup, null, @currentDate, @currentDate, @OperatorId, @JSON, @Reason
			FROM enum.EventSeverity eventSeverity
			JOIN enum.ObjectType objectType ON 1 = 1
			SELECT @eventId = SCOPE_IDENTITY()
			IF @UPDATE_ENDING_EVENT_ID = 1
			BEGIN
				UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
			END

		COMMIT TRANSACTION
		SELECT CONVERT(int, @Id) AS Result, NULL as ErrorCode, NULL as ErrorMessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		SELECT CONVERT(int, null) AS Result, ERROR_NUMBER() AS ErrorCode, ERROR_MESSAGE() AS ErrorMessage
	END CATCH

END