﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Log_Action]
	@ObjectID int,
	@Info nvarchar(max) = null
AS
BEGIN
	SET NOCOUNT ON;

	--RETURN

	DECLARE 
		@ProcedureName NVARCHAR(400);
  
	SELECT
	@ProcedureName = COALESCE
			(QUOTENAME(OBJECT_NAME(@ObjectID, DB_ID())),   ERROR_PROCEDURE());
 
	INSERT dbo._Log(ProcedureName, ErrorLine,    ErrorMessage,	  Info)
	SELECT		   @ProcedureName, ERROR_LINE(), ERROR_MESSAGE(), @Info;

    
END

