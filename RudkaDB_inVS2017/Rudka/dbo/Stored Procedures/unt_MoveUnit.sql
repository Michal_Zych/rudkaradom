﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	Przeniesienie unita do innego rodzica
-- =============================================
CREATE PROCEDURE [dbo].[unt_MoveUnit]
	@unitId int
	,@ParentUnitId int
	,@OperatorId int
	,@LogEntry nvarchar(max)
    ,@Reason nvarchar(max)

AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @OPERATION_DELETE tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_EDIT tinyint = 2

		,@RECORD_UNIT tinyint = 0
		,@RECORD_USER tinyint = 1

	BEGIN TRANSACTION
		BEGIN TRY
			UPDATE dbo.Units
				SET ParentUnitID = @ParentUnitId
			WHERE ID = @unitId

			INSERT dbo.UnitHistories
				VALUES(@unitId, @ParentUnitId, dbo.GetCurrentDate())

			INSERT dbo.Logs(OperationDate, OperatorId, OperationType, ObjectType, ObjectId, Reason, Info)
				VALUES(dbo.GetCurrentDate(), @operatorId, @OPERATION_EDIT, @RECORD_UNIT, @unitId, @reason, @logEntry)
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			SELECT -1
			RETURN
		END CATCH

	COMMIT TRANSACTION
	
	SELECT @unitId	
END

