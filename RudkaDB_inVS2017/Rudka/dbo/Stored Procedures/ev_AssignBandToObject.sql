﻿CREATE PROCEDURE [dbo].[ev_AssignBandToObject]
	@OperatorId int
	,@OperationDateUtc datetime = null
	,@Reason nvarchar(max) = null

	,@BandId smallint = null
	,@BarCode nvarchar(100)
	,@ObjectId dbo.OBJECT_ID 
	,@ObjectType dbo.OBJECT_TYPE
	
	,@IsExternalEvent bit = 0

	,@ForceAssign bit = 0 --JZ
AS
BEGIN
	SET NOCOUNT OFF;

	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
		BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'BandId=' + COALESCE(CONVERT(nvarchar(100), @BandId), 'NULL') 
		EXEC Log_Action @@PROCID, @info
	END
	/****************************************************/

	BEGIN TRY
		BEGIN TRANSACTION
			--EXEC dbo.ev_AssignBandToObject_core @OperatorId, @OperationDateUtc, @Reason, @BandId, @BarCode, @ObjectId, @ObjectType, @IsExternalEvent
			EXEC dbo.ev_AssignBandToObject_core @OperatorId, @OperationDateUtc, @Reason, @BandId, @BarCode, @ObjectId, @ObjectType, @IsExternalEvent, @ForceAssign --JZ
			SELECT @ObjectId AS Result, NULL as ErrorCode, NULL as ErrorMessage
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		SELECT CONVERT(int, null) AS Result, ERROR_NUMBER() AS ErrorCode, ERROR_MESSAGE() AS ErrorMessage
	END CATCH
END