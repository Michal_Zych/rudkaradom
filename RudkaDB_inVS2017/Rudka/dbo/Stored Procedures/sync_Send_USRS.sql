﻿

CREATE PROCEDURE [dbo].[sync_Send_USRS]
	@Active bit = null
	,@Login nvarchar(100) = null
	,@Name nvarchar(100) = null
	,@Surename nvarchar(100) = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE 
		@RECORD_TYPE nvarchar(10) = 'USRS'

	DECLARE 
		@record nvarchar(1024) = ''

	IF @Active IS NULL 
		AND @Login IS NULL
		AND @Name IS NULL
		AND @Surename IS NULL
	BEGIN
		SET @record = 'ACTIVE_SPECIFIED=''1'';ACTIVE=''0'''
		EXEC dbo.sync_SendRecord @RECORD_TYPE, @record
		SET @record = 'ACTIVE_SPECIFIED=''1'';ACTIVE=''1'''
		EXEC dbo.sync_SendRecord @RECORD_TYPE, @record
	END
	ELSE
	BEGIN
		SELECT @record = 'ACTIVE_SPECIFIED='''
			+ CASE WHEN @Active IS NULL THEN '0' ELSE '1' END
			+ ''';ACTIVE=''' + CONVERT(char(1), COALESCE(@Active, 1))
		
		IF @Login IS NOT NULL SELECT @record = @record
			+ ';LOGIN=''' + @Login + ''''

		IF @Name IS NOT NULL SELECT @record = @record
			+ ';NAME=''' + @Name + ''''
		
		IF @Surename IS NOT NULL SELECT @record = @record
			+ ';SURENAME=''' + @Surename + ''''
		EXEC dbo.sync_SendRecord @RECORD_TYPE, @record
	END
END