﻿CREATE PROCEDURE [dbo].[cfg_ApplicationSettingsGet]
AS
BEGIN
	SELECT sh.*,  c.RootUnitID, c.RegistrationWardId, c.UnassignedPatientsUnitId
	FROM dbo.ev_SettingsHistory sh
	JOIN dbo.Clients c ON c.ID = 1
	WHERE sh.Id = 1
	UNION ALL
	SELECT s.*,  c.RootUnitID, c.RegistrationWardId, c.UnassignedPatientsUnitId
	FROM  dbo.ev_Settings s
	JOIN dbo.Clients c ON c.ID = 1
END