﻿
CREATE PROCEDURE [dbo].[dev_BandBatteryChange]
	@BandId dbo.OBJECT_ID = null -- insert
	,@BatteryTypeId tinyint

	,@OperatorId int
	,@Reason nvarchar(max) = null
AS
BEGIN
	SET NOCOUNT OFF;
	DECLARE @UPDATE_ENDING_EVENT_ID bit = 1

	DECLARE
		@currentDate datetime

	SET @currentDate = dbo.GetCurrentUtcDate()

	DECLARE @JSON nvarchar(max) = 'SELECT bt.Id, bt.Name, bt.LifeMonths, bt.Capacity, CONVERT(nvarchar(10), bt.Voltage) as Voltage, bt.Description FROM BatteryTypes bt JOIN Bands b ON bt.Id = b.BatteryTypeId WHERE b.Id =' + CONVERT(nvarchar(10), @BandId)
	EXEC @JSON = dbo.SerializeJSON @JSON
	
	INSERT dbo.ev_Events(DateUtc, Type, Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason)
	SELECT @currentDate, eventType.BatteryChange, eventSeverity.Event, b.UnitId, b.Id, objectType.Band, b.UnitId, @currentDate, @currentDate, @OperatorId, @JSON, @Reason
	FROM dbo.Bands b
	JOIN enum.EventType eventType ON 1 = 1
	JOIN enum.EventSeverity eventSeverity ON 1 = 1
	JOIN enum.ObjectType objectType ON 1 = 1
	WHERE b.Id = @BandId -- JZ

	--JZ
	UPDATE dbo.Bands SET BatteryInstallationDateUtc = @currentDate WHERE Id = @BandId

	IF @UPDATE_ENDING_EVENT_ID = 1
	BEGIN
		DECLARE @eventId int
		SET @eventId = SCOPE_IDENTITY()
		UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
	END
	
	SELECT @BandId AS Result, NULL as ErrorCode, NULL as ErrorMessage

END