﻿-- =============================================
-- Author:		kt
-- Create date: 2017-10-30
-- Description:	gets sensors
-- =============================================
CREATE PROCEDURE [dbo].[dev_GetSensors]
 @LayerName nvarchar(50)
AS
BEGIN
 SET NOCOUNT ON;

 SELECT 
  sc.Id AS DeviceId, --SocketId
  sr.Id AS SensorId, --dbo.Sensor Id
  s.Id as SenseSensorId, --dbo.dev_Sensor Id
  s.SenseId as SenseId,
  s.SensorNr as SensorNumber,
  s.IntervalS,
  m.PhysicalUnit,
  m.LoRange,
  m.HiRange,
  0.0 AS Calibration1,
  0.0 AS Calibration2,
  0 AS AlarmRangeID,
  2500 AS ReadInterval,
  t.SubCode,
  s.IsEvent,
  b.Id AS BandId
 FROM dbo.dev_Sensors s
 INNER JOIN dbo.dev_Senses ss ON s.SenseId = ss.id
 INNER JOIN dbo.dev_ModelSenses m ON ss.TypeId = m.TypeId AND m.SensorNr = s.SensorNr
 INNER JOIN dbo.dev_SenseTypes t ON m.TypeId = t.Id
 INNER JOIN dbo.Sockets sc ON sc.SenseSensorId = s.Id
 INNER JOIN dbo.Sensors sr ON sr.SocketID = sc.Id
 LEFT JOIN dbo.Bands b ON b.Mac = ss.Id
 ORDER BY s.SenseId, s.SensorNr

END