﻿CREATE PROCEDURE [dbo].[ev_StoreBandOutOfRange]
	@BandId smallint,
	@EventDateUtc datetime = NULL
AS
BEGIN
	SET NOCOUNT ON;

/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
	BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'BandId=' + COALESCE(CONVERT(nvarchar(100), @BandId), 'NULL') 
					+ ';EventDateUtc=' + COALESCE(CONVERT(nvarchar(50), @EventDateUtc, 121), 'NULL')
		EXEC Log_Action @@PROCID, @info
	END
/****************************************************/

	DECLARE @SYSTEM_USER_ID int = 0
		,@OBJECT_TYPE_BAND dbo.OBJECT_TYPE

	DECLARE
		@currentDateUtc datetime = dbo.GetCurrentUtcDate()
		,@warningDateUtc dateTime
		,@alarmDateUtc dateTime
		
	IF @EventDateUtc IS NULL SET @EventDateUtc = @currentDateUtc

BEGIN -- zmienne konfiguracji
	DECLARE
		 @objectType				dbo.OBJECT_TYPE   
		,@objectId					dbo.OBJECT_ID  
		,@objectUnitId				int  
		,@currentUnitId				int  
		,@inCurrentUnitUtc			dateTime  
		,@previousUnitId			int  
		,@inPreviousUnitUtc			dateTime  
		,@topAlarmId				int  
		,@topAlarmType				dbo.EVENT_TYPE   
		,@topAlarmSeverity			dbo.EVENT_SEVERITY   
		,@topAlarmDateUtc			dateTime 
		,@topEventDateUtc			dateTime  
		,@severity1Count			int  
		,@severity2Count			int  
		,@ongNoGoZoneInEventId		int  
		,@ongNoGoZoneInAlarmId		int  
		,@ongDisconnectedEventId	int  
		,@ongDisconnectedAlarmId	int  
		,@ongZoneOutEventId			int  
		,@ongZoneOutAlarmId			int  
		,@ongNoMovingEventId		int  
		,@ongNoMovingAlarmId		int  
		,@ongMovingEventId			int  
		,@ongMovingAlarmId			int  
		,@ongLowBatteryEventId		int  
		,@ongLowBatteryAlarmId		int 

		,@noConnectWrActive			bit     
		,@noConnectWrMins			smallint
		,@noConnectAlActive			bit     
		,@noConnectAlMins			smallint
		,@warningAutoCloseMessage	nvarchar(max)
		,@noConnectionFinishingAlarms bit
		,@fullMonitoringUnassignedBands bit
		,@currentEventType			dbo.EVENT_TYPE
		,@json nvarchar(max)
END

BEGIN	-- pobranie konfiguracji
	SELECT
		 @objectType					= bs.ObjectType			
		,@objectId						= bs.ObjectId				
		,@objectUnitId					= bs.ObjectUnitId			
		,@currentUnitId					= bs.CurrentUnitId			
		,@inCurrentUnitUtc				= bs.InCurrentUnitUtc		
		,@previousUnitId				= bs.PreviousUnitId		
		,@inPreviousUnitUtc				= bs.InPreviousUnitUtc		
		,@topAlarmId					= bs.TopAlarmId			
		,@topAlarmType					= bs.TopAlarmType			
		,@topAlarmSeverity				= bs.TopAlarmSeverity		  
		,@topAlarmDateUtc				= bs.TopAlarmDateUtc		
		,@topEventDateUtc				= bs.TopEventDateUtc		
		,@severity1Count				= bs.Severity1Count		
		,@severity2Count				= bs.Severity2Count		
		,@ongNoGoZoneInEventId			= bs.OngNoGoZoneInEventId  
		,@ongNoGoZoneInAlarmId			= bs.OngNoGoZoneInAlarmId  
		,@ongDisconnectedEventId		= bs.OngDisconnectedEventId
		,@ongDisconnectedAlarmId		= bs.OngDisconnectedAlarmId
		,@ongZoneOutEventId				= bs.OngZoneOutEventId		
		,@ongZoneOutAlarmId				= bs.OngZoneOutAlarmId		
		,@ongNoMovingEventId			= bs.OngNoMovingEventId	
		,@ongNoMovingAlarmId			= bs.OngNoMovingAlarmId	
		,@ongMovingEventId				= bs.OngMovingEventId		
		,@ongMovingAlarmId				= bs.OngMovingAlarmId		
		,@ongLowBatteryEventId			= bs.OngLowBatteryEventId  
		,@ongLowBatteryAlarmId			= bs.OngLowBatteryAlarmId  
		
		,@noConnectionFinishingAlarms	= st.NoConnectionFinishingAlarms
		,@fullMonitoringUnassignedBands = st.FullMonitoringUnassignedBands
		,@warningAutoCloseMessage		= st.WarningAutoCloseMessage

		,@noConnectWrActive				= COALESCE(cp.NoConnectWrActive, cu.NoConnectWrActive)
		,@noConnectWrMins				= COALESCE(cp.NoConnectWrMins, cu.NoConnectWrMins)
		,@noConnectAlActive				= COALESCE(cp.NoConnectAlActive, cu.NoConnectAlActive)
		,@noConnectAlMins				= COALESCE(cp.NoConnectAlMins, cu.NoConnectAlMins)

		,@currentEventType				= eventType.Disconnected
		,@OBJECT_TYPE_BAND				= objectType.Band
	FROM dbo.ev_Settings st 
		JOIN enum.ObjectType objectType ON 1 = 1  
		JOIN enum.EventType eventType ON 1 = 1
		LEFT JOIN dbo.ev_BandStatus bs ON bs.BandId = @BandId
		LEFT JOIN dbo.al_ConfigPatients cp ON cp.PatientId = bs.ObjectId AND bs.ObjectType = objectType.Patient
		LEFT JOIN dbo.al_ConfigUnits cu ON cu.UnitId = bs.CurrentUnitId
END

	-- jeśli trwa brak łączności lub nie monitorujemy nieprzypisanych opasek to koniec
	IF @ongDisconnectedEventId IS NOT NULL 
		OR (@objectType = (SELECT Band FROM enum.ObjectType) AND @fullMonitoringUnassignedBands = 0)
	BEGIN
		RETURN
	END

	-- zarejestrowanie rekordu statusu dla opaski
	IF @objectId IS NULL
	BEGIN
		EXEC dbo.ev_AssignBandToItself
			@EventDateUtc		= @EventDateUtc
			,@BandId			= @BandId
			,@ObjectId			= @objectId			OUTPUT
			,@ObjectType		= @objectType		OUTPUT
			,@InCurrentUnitUtc	= @inCurrentUnitUtc	OUTPUT
			,@severity1Count	= @severity1Count	OUTPUT
			,@severity2Count	= @severity2Count	OUTPUT
	END


	-- obliczenia
		SELECT @previousUnitId = @currentUnitId, @currentUnitId = null,
			@inPreviousUnitUtc = @inCurrentUnitUtc, @inCurrentUnitUtc = @EventDateUtc

	-- wstaw zdarzenie brak łączności
	SELECT @json = 'SELECT ' + COALESCE(CONVERT(nvarchar(10), @previousUnitId), 'NULL') + ' AS PreviousUnitId, ' 
		+ COALESCE('''' + CONVERT(nvarchar(25), @inPreviousUnitUtc, 121) + '''', 'NULL') + ' AS InPreviousUnitUtc'
	EXEC @json = dbo.SerializeJSON @json
	DECLARE @finishedEventId int
			, @finishedAlarmId int
	EXEC dbo.ev_GenerateEvent
					@currentDateUtc				= @currentDateUtc
					,@EventDateUtc				= @EventDateUtc
					,@EventType					= @currentEventType
					,@EventUnitId				= @currentUnitId
					,@EventObjectId				= @objectId
					,@EventObjectType			= @objectType
					,@EventObjectUnitId			= @objectUnitId
					,@EventData					= @json
					,@IsLongEvent				= 1
					,@FinishedEventId			= @finishedEventId			OUTPUT
					,@FinishedAlarmId			= @finishedAlarmId			OUTPUT
					,@EventId					= @ongDisconnectedEventId	OUTPUT

	/*
	INSERT ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, EndingEventId, [EventData])
	SELECT @EventDateUtc, @currentEventType, eventSeverity.Event, @currentUnitId, @objectId, @objectType, @objectUnitId, @currentDateUtc, null, null, @json
	FROM enum.EventSeverity eventSeverity
	SET @ongDisconnectedEventId = SCOPE_IDENTITY()
	*/

	-- Brak łączności kończy inne alarmy
	IF @noConnectionFinishingAlarms = 1
	BEGIN
		-- nie będzie alarmów bo zakończone
		DELETE dbo.ev_EventsToRaise WHERE RaisingObjectType = @OBJECT_TYPE_BAND AND RaisingObjectId = @BandId AND RaiseDateUtc > @currentDateUtc
		
		UPDATE dbo.ev_Events SET
			EndDateUtc = @EventDateUtc
			,EndingEventId = @ongDisconnectedEventId
		WHERE Id IN (@ongNoGoZoneInEventId, @ongNoGoZoneInAlarmId, @ongDisconnectedAlarmId, @ongZoneOutEventId, @ongZoneOutAlarmId			
					 ,@ongNoMovingEventId, @ongNoMovingAlarmId, @ongMovingEventId, @ongMovingAlarmId, @ongLowBatteryEventId, @ongLowBatteryAlarmId)

		SELECT @ongNoGoZoneInEventId = NULL, @ongNoGoZoneInAlarmId = NULL, @ongDisconnectedAlarmId = NULL, @ongZoneOutEventId = NULL, @ongZoneOutAlarmId = NULL	
			  ,@ongNoMovingEventId = NULL, @ongNoMovingAlarmId = NULL, @ongMovingEventId = NULL, @ongMovingAlarmId = NULL, @ongLowBatteryEventId = NULL, @ongLowBatteryAlarmId = NULL
	END

	-- wygeneruj ostrzeżenia/alarmy
	DECLARE 
		@ongoingAlarmSeverity dbo.EVENT_SEVERITY

	EXEC dbo.ev_GenerateAlarmsForEvent 
			 @EventObjectType			= @OBJECT_TYPE_BAND
			,@EventObjectid				= @BandId
			,@currentDateUtc			= @currentDateUtc
			,@CurrentEventDateUtc		= @EventDateUtc
			,@currentEventId			= @ongDisconnectedEventId
			,@currentEventType			= @currentEventType
			,@currentUnitId				= @currentUnitId
			,@objectId					= @objectId
			,@objectType				= @objectType
			,@objectUnitId				= @objectUnitId
			,@warningCloseMessage		= @warningAutoCloseMessage
			,@WarningActive				= @noConnectWrActive	
			,@WarningDelayMins			= @noConnectWrMins	
			,@AlarmActive				= @noConnectAlActive	
			,@AlarmDelayMins			= @noConnectAlMins	
			,@CurrrentAlarmSeverity		= @ongoingAlarmSeverity OUTPUT
			,@CurrrentAlarmId			= @ongDisconnectedAlarmId OUTPUT
			,@warningDateUtc			= @warningDateUtc OUTPUT
			,@alarmDateUtc				= @alarmDateUtc	OUTPUT
			,@severity1Count			= @severity1Count OUTPUT
			,@severity2Count			= @severity2Count OUTPUT

	-- uaktualnij status
	EXEC dbo.ev_GenerateBandStatus
			 @BandId					= @BandId 
			,@CurrentUnitId 			= @CurrentUnitId 
			,@InCurrentUnitUtc			= @InCurrentUnitUtc
			,@PreviousUnitId 			= @PreviousUnitId 
			,@InPreviousUnitUtc 		= @InPreviousUnitUtc 
			,@TopAlarmId 				= @TopAlarmId 
			,@TopAlarmType  			= @TopAlarmType  
			,@TopAlarmSeverity 			= @TopAlarmSeverity 
			,@TopAlarmDateUtc 			= @TopAlarmDateUtc 
			,@TopEventDateUtc 			= @TopEventDateUtc 
			,@Severity1Count 			= @Severity1Count 
			,@Severity2Count 			= @Severity2Count 
			,@OngNoGoZoneInEventId 		= @OngNoGoZoneInEventId 
			,@OngNoGoZoneInAlarmId  	= @OngNoGoZoneInAlarmId  
			,@OngDisconnectedEventId  	= @OngDisconnectedEventId 
			,@OngDisconnectedAlarmId  	= @OngDisconnectedAlarmId 
			,@OngZoneOutEventId  		= @OngZoneOutEventId  
			,@OngZoneOutAlarmId  		= @OngZoneOutAlarmId  
			,@OngNoMovingEventId  		= @OngNoMovingEventId  
			,@OngNoMovingAlarmId  		= @OngNoMovingAlarmId  
			,@OngMovingEventId  		= @OngMovingEventId  
			,@OngMovingAlarmId  		= @OngMovingAlarmId  
			,@OngLowBatteryEventId  	= @OngLowBatteryEventId  
			,@OngLowBatteryAlarmId 		= @OngLowBatteryAlarmId 
			,@currentEventId			= @ongDisconnectedEventId
			,@CurrentEventType 			= @CurrentEventType 
			,@CurrentEventDateUtc 		= @EventDateUtc 
			,@GeneratedAlarmId			= @OngDisconnectedAlarmId 
			,@GeneratedAlarmSeverity 	= @ongoingAlarmSeverity 
			,@GeneratedWarningDateUtc 	= @warningDateUtc
			,@GeneratedAlarmDateUtc 	= @alarmDateUtc 

END