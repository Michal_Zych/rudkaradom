﻿
CREATE PROCEDURE [dbo].[ev_GenerateEvent](
	@currentDateUtc dateTime
	,@EventDateUtc dateTime
	,@EventType dbo.EVENT_TYPE
	,@EventUnitId int
	,@EventObjectId int
	,@EventObjectType int
	,@EventObjectUnitId int
	,@EventData nvarchar(max)
	,@IsLongEvent bit

	,@FinishedEventId int	OUTPUT
	,@FinishedAlarmId int	OUTPUT
	,@EventId int			OUTPUT
)
AS
BEGIN
	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
		BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'EventObjectId=' + COALESCE(CONVERT(nvarchar(100), @EventObjectId), 'NULL') 
					+ ';currentDateUtc=' + COALESCE(CONVERT(nvarchar(50), @currentDateUtc, 121), 'NULL')
					+ ';EventDateUtc=' + COALESCE(CONVERT(nvarchar(100), @EventDateUtc), 'NULL')
		EXEC Log_Action @@PROCID, @info
	END
	/****************************************************/

	DECLARE @UPDATE_ENDING_EVENT_ID bit = 1

	IF @FinishedEventId IS NOT NULL  --jeśli zdarzenie kończy inne to skasuj alarmy do podniesienia
		DELETE dbo.ev_EventsToRaise WHERE RaisingEventId = @FinishedEventId AND RaiseDateUtc >= @EventDateUtc

	-- wygeneruj zdarzenie
	INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, [EventData])
	SELECT @EventDateUtc, @EventType, eventSeverity.Event, @EventUnitId, @EventObjectId, @EventObjectType, @EventObjectUnitId, @currentDateUtc, @EventData
	FROM enum.EventSeverity eventSeverity
	SET @EventId = SCOPE_IDENTITY()
	IF @IsLongEvent = 0 AND @UPDATE_ENDING_EVENT_ID = 1
	BEGIN
		UPDATE dbo.ev_Events SET EndingEventId = @EventId, EndDateUtc = @EventDateUtc WHERE Id = @EventId
	END

	-- jeśli zdarzenie kończy inne to je zakończ
	IF @FinishedEventId IS NOT NULL
	BEGIN
		UPDATE dbo.ev_Events SET
			EndDateUtc = @EventDateUtc
			,EndingEventId = @EventId
		WHERE Id IN(@FinishedEventId, @FinishedAlarmId)
		SELECT @FinishedEventId = null, @FinishedAlarmId = null
	END
END