﻿

CREATE PROCEDURE [dbo].[Alarms_SearchAlarms]
	@date_from datetime,
	@date_to datetime,
	@sensor_name DEVICE_NAME,
	@user int,
	@units IDs_LIST
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @clientId int = 1

	DECLARE @maxRows int
	SELECT @maxRows = MaxRowsAlarms FROM dbo.ev_Settings
	
	SELECT top(@maxRows)
			a.ID as [ID],
			
			a.EventType as [EventType],
			a.[Timestamp] as [Timestamp],
			
			a.SensorID as [SensorID],
			u.Name as [SensorName],
			u.Id as [UnitId],
			dbo.GetUnitLocation(u.id, default, default, default) as Location,
			s.MU AS MU,

			a.LoRange * s.Scale AS LoRange,
			a.UpRange * s.Scale AS UpRange,
			dat.Value * s.Scale AS Value,
			
			a.DateEnd as DateEnd,
			a.DateStatus as AlarmCloseDate,
			a.UserStatus as AlarmCloseUserID,
			isnull(us.Name,'') + ' ' + isnull(us.LAstName,'') as AlarmCloseUserName,
			a.CommentStatus as AlarmCloseComment,
			s.IsBitSensor as IsBitSensor,
			s.LoStateDesc as LoStateDesc,
			s.HiStateDesc as HiStateDesc

	FROM
			Alarms a
			left join Measurements dat on dat.SensorID = a.SensorId and dat.MeasurementTime = a.Timestamp
			inner join Sensors s on s.id = a.SensorId
			inner join Units u on u.Id = s.UnitId
			left join Users us on us.ID = a.UserStatus
			join dbo.SplitWordsToInt(@units) SU on SU.value = u.ID
	where (u.Name like '%' + @sensor_name + '%' or @sensor_name is null)
			and (a.Timestamp >= @date_from or @date_from is null)
			and (a.Timestamp <= @date_to or @date_to is null)
	ORDER BY
		a.[Timestamp] desc
    
END

