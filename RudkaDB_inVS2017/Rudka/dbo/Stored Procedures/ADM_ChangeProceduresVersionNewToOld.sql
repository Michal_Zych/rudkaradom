﻿CREATE PROCEDURE [dbo].[ADM_ChangeProceduresVersionNewToOld]
AS BEGIN
-- procedura podmienia wersje procedur:
-- jeśli jest procedura z przyrostkiemj __new to procedura bez przyrostka uzyskuje przyrostek __old
-- a procedura __new staje się procedurą bieżącą
-- jeśli jest procedura z przyrostkiemj __old to procedura bez przyrostka uzyskuje przyrostek __new
-- a procedura __old staje się procedurą bieżącą

-- ****************************************************************************
--		NIE ZMIENIAJ PIERWSZYCH LINII KODU (NIE KASUJ/NIE KOMENTUJ)
-- ****************************************************************************	


SELECT 'Zabezpieczenie przed nieświadomym użyciem - nie wykonano żadnej operacji, przeczytaj komentarz w ciele procedury'
RETURN

--stałe
	DECLARE
		@SUFFIX_TAG nvarchar(10) = '__'
		,@ESC_CHAR nchar(1) = '\'
		,@SUFFIX_NEW nvarchar(10) = 'new' 
		,@SUFFIX_OLD nvarchar(10) = 'old'
		
	DECLARE @procs TABLE(
		ID int identity(1,1)
		,Name nvarchar(200)
		)
	

	INSERT @procs (Name)
	SELECT ROUTINE_NAME From Information_Schema.Routines
	WHERE ROUTINE_NAME LIKE '%' + REPLACE(@SUFFIX_TAG, '_', @ESC_CHAR + '_') + '%' ESCAPE @ESC_CHAR

	SELECT * FROM @procs
	WHERE SUBSTRING(Name, 1, CHARINDEX(@SUFFIX_TAG, Name) - 1 ) 
		NOT IN 	(SELECT ROUTINE_NAME FROM Information_Schema.Routines)
	IF @@ROWCOUNT > 0 
	BEGIN
		SELECT 'Brak procedury pierwotnej dla powyższych procedur - nie wykonano żadnej operacji'
		RETURN
	END	

	SELECT * FROM @procs
	WHERE SUBSTRING(Name,  CHARINDEX(@SUFFIX_TAG, Name) + LEN(@SUFFIX_TAG), 999) NOT IN (@SUFFIX_NEW, @SUFFIX_OLD)
	IF @@ROWCOUNT > 0 
	BEGIN
		SELECT 'Błędny przyrostek (dopuszczalne new/old) - nie wykonano żadnej operacji'
		RETURN
	END

	
	DECLARE @id int
		,@lastId int
		,@current_name nvarchar(200)
		,@current_suffix nvarchar(3)
		,@procName nvarchar(200)
		,@oryginalTo nvarchar(200)	

	SELECT TOP (1) @id = ID, @current_name = Name FROM @procs

	WHILE @id IS NOT NULL
	BEGIN
	
		SELECT @current_suffix = SUBSTRING(@current_name, CHARINDEX(@SUFFIX_TAG, @current_name) + LEN(@SUFFIX_TAG), 999)
		SELECT @procName = SUBSTRING(@current_name, 1, CHARINDEX(@SUFFIX_TAG, @current_name) - 1)
	
		IF @current_suffix = @SUFFIX_NEW SELECT @oryginalTo = @procName + @SUFFIX_TAG + @SUFFIX_OLD
		ELSE SELECT @oryginalTo = @procName + @SUFFIX_TAG + @SUFFIX_NEW

		exec sp_rename @procName, @oryginalTo
		exec sp_rename @current_name, @procName
	
		SELECT @lastId = @id, @id = NULL
		SELECT TOP (1) @id = ID, @current_name = name FROM @procs	WHERE ID > @lastId
	END

	SELECT SUBSTRING(Name, 1, CHARINDEX(@SUFFIX_TAG, Name) - 1 ) + ' = ' + Name AS [Podmieniono] from @procs
	
END
