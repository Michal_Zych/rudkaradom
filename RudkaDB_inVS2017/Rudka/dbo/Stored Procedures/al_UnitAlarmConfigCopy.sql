﻿

CREATE PROCEDURE [dbo].[al_UnitAlarmConfigCopy]
(
	@SourceUnitId int,
	@DestinationUnitId int,
	@OperatorId int,
	@Reason nvarchar(max) = null
)
 AS  
 BEGIN
	SET NOCOUNT ON; 

	DECLARE @UPDATE_ENDING_EVENT_ID bit = 1

	DECLARE
		@EVENT_UPDATE dbo.EVENT_TYPE
		,@EVENT_CREATE dbo.EVENT_TYPE
		,@OBJECT_TYPE_UNIT dbo.OBJECT_TYPE
		,@OBJECT_TYPE_UNITALARMCONFIG dbo.OBJECT_TYPE
		,@SEVERITY_EVENT dbo.EVENT_SEVERITY
		,@eventType dbo.EVENT_TYPE
		,@OperationDate datetime
		,@eventId int

	SELECT @EVENT_CREATE = [Create], @EVENT_UPDATE = [Update] FROM enum.EventType
	SELECT @OBJECT_TYPE_UNITALARMCONFIG = UnitAlarmConfiguration, @OBJECT_TYPE_UNIT = Unit FROM enum.ObjectType
	SELECT @SEVERITY_EVENT = [Event] FROM enum.EventSeverity

	SET @OperationDate = dbo.GetCurrentUtcDate()

	
	IF EXISTS(SELECT 1 FROM dbo.al_ConfigUnits WHERE UnitId = @DestinationUnitId)
		SET @eventType = @EVENT_UPDATE
	ELSE SET @eventType = @EVENT_CREATE

	BEGIN TRY
		BEGIN TRANSACTION
			IF @eventType = @EVENT_UPDATE
			BEGIN
				DELETE dbo.al_CallingGroups WHERE Id = @DestinationUnitId AND ObjectType = @OBJECT_TYPE_UNIT
				DELETE dbo.al_NoGoZones WHERE Id = @DestinationUnitId AND ObjectType = @OBJECT_TYPE_UNIT
				DELETE dbo.al_ConfigUnits WHERE UnitId = @DestinationUnitId
			END

			INSERT dbo.al_ConfigUnits
			SELECT @DestinationUnitId, 
				IsSOScalling, IsACCactive, DMoveWrActive, DMoveWrMins, DMoveAlActive, DMoveAlMins, DNoMoveWrActive,
				DNoMoveWrMins, DNoMoveAlActive, DNoMoveAlMins, NMoveWrActive, NMoveWrMins, NMoveAlActive,
				NMoveAlMins, NNoMoveWrActive, NNoMoveWrMins, NNoMoveAlActive, NNoMoveAlMins, OutOfZoneWrActive,
				OutOfZoneWrMins, OutOfZoneAlActive, OutOfZoneAlMins, IsNoGoZone, InNoGoZoneMins, NoConnectWrActive, NoConnectWrMins,
				NoConnectAlActive, NoConnectAlMins, NoTransmitterWrActive, NoTransmitterWrMins, NoTransmitterAlActive, NoTransmitterAlMins,
				MoveSettingsPriority
			FROM dbo.al_ConfigUnits
			WHERE UnitId = @SourceUnitId
	
			-- calling groups
			INSERT dbo.al_CallingGroups
			SELECT @DestinationUnitId, ObjectType, SequenceNr, Phone, Description
			FROM dbo.al_CallingGroups
			WHERE Id = @SourceUnitId AND ObjectType = @OBJECT_TYPE_UNIT
	
			--no go zones
			INSERT dbo.al_NoGoZones
			SELECT @DestinationUnitId, ObjectType, UnitId
			FROM dbo.al_NoGoZones
			WHERE Id = @SourceUnitId AND ObjectType = @OBJECT_TYPE_UNIT
			 
			-- event	
			DECLARE @JSON nvarchar(max)
			EXEC @JSON = dbo.SerializeJSON_UnitAlarmConfiguration @DestinationUnitId

			INSERT dbo.ev_Events(DateUtc, Type, Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason) VALUES
				(@OperationDate, @eventType, @SEVERITY_EVENT, @DestinationUnitId, @DestinationUnitId, @OBJECT_TYPE_UNITALARMCONFIG, @DestinationUnitId, @OperationDate, @OperationDate, @OperatorId, @JSON, @Reason)
			SELECT @eventId = SCOPE_IDENTITY()
			IF @UPDATE_ENDING_EVENT_ID = 1
			BEGIN
				UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
			END

		COMMIT TRANSACTION
		SELECT @DestinationUnitId AS Result, NULL as ErrorCode, NULL as ErrorMessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		SELECT CONVERT(int, null) AS Result, ERROR_NUMBER() AS ErrorCode, ERROR_MESSAGE() AS ErrorMessage
	END CATCH
END