﻿
	
CREATE PROCEDURE [dbo].[al_UnitAlarmConfigGet]
	@UnitId int
AS
BEGIN
	IF (NOT EXISTS (SELECT UnitId FROM dbo.al_ConfigUnits WHERE UnitId = @UnitId)) SET @UnitId = 0

	SELECT c.*, ut.TransmitterId
		,dbo.GetCallingGroupString(objectType.Unit, c.UnitId) AS CallingGroup
		,dbo.GetNoGoZonesString(objectType.Unit, c.UnitId) AS NoGoZones
	FROM dbo.al_ConfigUnits c
	LEFT JOIN dbo.dev_UnitTransmitter ut ON c.UnitId = ut.UnitId
	JOIN enum.ObjectType objectType ON 1 = 1 
	WHERE c.UnitId = @UnitId
END