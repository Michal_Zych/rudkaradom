﻿

CREATE PROCEDURE [dbo].[dev_GetPatientId]
	@Pesel char(11)
	AS 
	BEGIN
	SET NOCOUNT ON;

	DECLARE
	@Result int

	IF LEN(@Pesel) < 11
	BEGIN
		SELECT CONVERT(int, null) AS Result, -1 AS ErrorCode, 'Pesel nie może być krótszy niż 11 znaków' AS ErrorMessage
		RETURN
	END

	SELECT @Result = p.Id FROM Patients p WHERE p.Pesel = @Pesel;

	IF @result IS NULL SELECT @Result = p.Id FROM arch.Patients p WHERE p.Pesel = @Pesel;

	IF @Result IS NULL
	BEGIN
		SELECT CONVERT(int, null) AS Result, -1 AS ErrorCode, 'Nie odnaleziono pacjenta o peselu ' + @Pesel AS ErrorMessage;
		RETURN
	END

	SELECT @Result AS Result, NULL AS ErrorCode, NULL AS ErrorMessage;
	END