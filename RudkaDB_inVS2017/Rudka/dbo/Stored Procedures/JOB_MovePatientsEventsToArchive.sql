﻿
CREATE PROCEDURE [dbo].[JOB_MovePatientsEventsToArchive]
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @SECONDS_TO_RUN int = 5
	DECLARE @MAX_ROWS_COUNT int = 1000

	DECLARE @OBJECT_TYPE_PATIENT dbo.OBJECT_TYPE
		,@OBJECT_TYPE_PATIENT_SETTINGS dbo.OBJECT_TYPE

	DECLARE @patientId int
		,@maxEventId int
		,@startTime datetime
		,@deletedCount int

	SELECT @startTime = dbo.GetCurrentUTCDate()
		,@OBJECT_TYPE_PATIENT = objectType.Patient
		,@OBJECT_TYPE_PATIENT_SETTINGS = objectType.PatientAlarmConfiguration
	FROM enum.ObjectType objectType

	SELECT TOP 1 @patientId = PatientId, @maxEventId = MaxEventId
	FROM dbo.ArchivingOrders
	ORDER BY Id
	
	WHILE @patientId IS NOT NULL AND DATEDIFF(SECOND, @startTime, dbo.GetCurrentUTCDate()) <= @SECONDS_TO_RUN
	BEGIN
		SELECT @deletedCount = 1
		WHILE @deletedCount > 0 AND DATEDIFF(SECOND, @startTime, dbo.GetCurrentUTCDate()) <= @SECONDS_TO_RUN
		BEGIN
			DELETE TOP (@MAX_ROWS_COUNT) dbo.ev_UsersAlarms
				OUTPUT deleted.EventId, deleted.UserMessageId, deleted.UserId, deleted.Message
				INTO [$(RudkaArch)].dbo.ev_UsersAlarms(EventId, UserMessageId, UserId, Message)
			FROM dbo.ev_UsersAlarms ua
			JOIN dbo.ev_Events e ON ua.EventId = e.Id
			WHERE e.Id <= @maxEventId
				AND e.ObjectId = @patientId
				AND e.ObjectType = @OBJECT_TYPE_PATIENT

			SELECT @deletedCount = @@ROWCOUNT
		END

		IF @deletedCount = 0
		BEGIN
			SELECT @deletedCount = 1
			WHILE @deletedCount > 0 AND DATEDIFF(SECOND, @startTime, dbo.GetCurrentUTCDate()) <= @SECONDS_TO_RUN
			BEGIN

				DELETE TOP (@MAX_ROWS_COUNT) dbo.ev_Alarms
					OUTPUT deleted.EventId, deleted.RaisingEventId, deleted.IsClosed, deleted.ClosingDateUtc, deleted.ClosingUserId, deleted.ClosingAlarmId, deleted.ClosingComment
					INTO [$(RudkaArch)].dbo.ev_Alarms(EventId, RaisingEventId, IsClosed, ClosingDateUtc, ClosingUserId, ClosingAlarmId, ClosingComment)
				FROM dbo.ev_Alarms a
				JOIN dbo.ev_Events e ON a.EventId = e.Id
				WHERE e.Id <= @maxEventId
					AND e.ObjectId = @patientId
					AND e.ObjectType = @OBJECT_TYPE_PATIENT
				
				SELECT @deletedCount = @@ROWCOUNT
			END

			IF @deletedCount = 0
			BEGIN
				SELECT @deletedCount = 1
				WHILE @deletedCount > 0 AND DATEDIFF(SECOND, @startTime, dbo.GetCurrentUTCDate()) <= @SECONDS_TO_RUN
				BEGIN

					DELETE TOP (@MAX_ROWS_COUNT) 
					FROM dbo.ev_Events
						OUTPUT deleted.Id, deleted.DateUtc, deleted.[Type], deleted.Severity, deleted.UnitId, deleted.ObjectId, deleted.ObjectType, deleted.ObjectUnitId
							,deleted.StoreDateUtc, deleted.EndDateUtc, deleted.EndingEventId, deleted.OperatorId, deleted.[EventData], deleted.Reason
						INTO [$(RudkaArch)].dbo.ev_Events(Id, DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId
							,StoreDateUtc, EndDateUtc, EndingEventId, OperatorId, [EventData], Reason)
					WHERE Id <= @maxEventId
						AND ObjectId = @patientId
						AND (ObjectType = @OBJECT_TYPE_PATIENT OR ObjectType = @OBJECT_TYPE_PATIENT_SETTINGS)
					
					SELECT @deletedCount = @@ROWCOUNT
				END
			END
		END
		
		IF @deletedCount = 0
		BEGIN
			DELETE dbo.ArchivingOrders WHERE PatientId = @patientId
			SELECT @patientId = NULL
			SELECT TOP 1 @patientId = PatientId, @maxEventId = MaxEventId
			FROM dbo.ArchivingOrders
			ORDER BY Id
		END
	END


	
		
END