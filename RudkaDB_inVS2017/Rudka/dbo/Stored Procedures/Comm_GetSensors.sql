﻿
CREATE PROCEDURE [dbo].[Comm_GetSensors]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT s.Id, so.deviceid, so.DeviceChannelNumber, s.Interval, so.LoRange, so.HiRange, so.LoRangeH, so.HiRangeH, s.Scale  
	FROM dbo.Sensors s
	INNER JOIN dbo.Sockets so ON so.Id = s.SocketID 
	WHERE s.Active = 1 AND s.[Enabled] = 1
			AND so.DeviceId IS NOT NULL 
			AND so.DeviceChannelNumber IS NOT NULL 
			AND so.Broken = 0
END
