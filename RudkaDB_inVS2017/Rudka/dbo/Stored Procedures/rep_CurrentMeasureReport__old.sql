﻿CREATE  PROCEDURE [dbo].[rep_CurrentMeasureReport__old]
	@UserReportID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @precision int = 1
	DECLARE
		@date datetime
		,@hyperPermission bit

	SELECT @date = DateFrom
		, @hyperPermission = COALESCE(HyperPermission, 0)
	FROM dbo.UsersReports ur WITH(NOLOCK)
	WHERE ur.Id = @UserReportID


	SELECT s.Id AS SensorId
		, CASE WHEN @hyperPermission = 1 THEN m.HyperMesurementTime ELSE m.MeasurementTime END AS MeasurementTime
		, CASE WHEN @hyperPermission = 1 THEN ROUND(m.HyperValue * s.Scale, @precision) ELSE ROUND(m.Value * s.scale, @precision) END AS Value
		, u.Name AS Name
		, dbo.GetUnitLocation(u.ID, default, 1, default ) AS Location
		, s.MU AS MU
	FROM dbo.MeasurementsCurrent m WITH(NOLOCK)
	JOIN dbo.UserReportSensors r WITH(NOLOCK) ON m.SensorID = r.SensorID
	JOIN dbo.Sensors s WITH(NOLOCK) ON s.Id = r.SensorID
	JOIN dbo.Units u WITH(NOLOCK) ON u.ID = s.UnitId
	WHERE r.UserReportID = @UserReportID
	
END

