﻿CREATE PROCEDURE [dbo].[unt_GetSensorConfiguration]
	@unitId int
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @DEFAULT_SCALE real = 0.1
	DECLARE @DEFAULT_HI_STATE nvarchar(10) = '1'
	DECLARE @DEFAULT_LO_STATE nvarchar(10) = '0'

	SELECT s.ID, SocketID, Interval, (Calibration * COALESCE(Scale, @DEFAULT_SCALE)) AS Calibration, [Enabled], MU, 
		(LoRange * COALESCE(Scale, @DEFAULT_SCALE)) AS LoRange,
		(UpRange * COALESCE(Scale, @DEFAULT_SCALE)) AS UpRange,
		LegendDescription, LegendShortcut, LegendRangeDescription,
		LineColor, LineWidth, LineStyle,
		RangeLineWidth, RangeLineStyle,
		AlarmColor, AlarmCommColor,
		AlDisabled, AlDisabledForRanges, AlDisabledForHours,
		CONVERT(datetime, AlDisableHoursFrom1) AS AlDisableHoursFrom1, 
		CONVERT(datetime, AlDisableHoursTo1) AS AlDisableHoursTo1, 
		CONVERT(datetime, AlDisableHoursFrom2) AS AlDisableHoursFrom2, 
		CONVERT(datetime, AlDisableHoursTo2) AS AlDisableHoursTo2,
		GuiRangeDelay, GuiCommDelay, 
		SmsEnabled, SmsRangeDelay, SmsCommDelay,  
		MailEnabled, MailRangeDelay, MailCommDelay, 
		IsBitSensor, CONVERT(bit, COALESCE(IsHighAlarming, 0)) AS IsHighAlarming,
		COALESCE(HiStateDesc, @DEFAULT_HI_STATE) AS HiStateDesc,
		COALESCE(LoStateDesc, @DEFAULT_LO_STATE) AS LoStateDesc,

		GuiRangeRepeat			,
		GuiRangeRepeatMins 	,
		GuiRangeRepeatBreakable, 
		GuiCommRepeat 			,
		GuiCommRepeatMins 		,
		GuiCommRepeatBreakable ,
		SmsRangeEnabled			,
		SmsRangeAfterFinish		,
		SmsRangeAfterClose		,
		SmsRangeRepeat			,
		SmsRangeRepeatMins		,
		SmsRangeRepeatUntil		,
		SmsRangeRepeatBreakable ,
		SmsCommEnabled,
		SmsCommAfterFinish		,
		SmsCommAfterClose		,
		SmsCommRepeat			,
		SmsCommRepeatMins		,
		SmsCommRepeatUntil		,
		SmsCommRepeatBreakable	,
		MailRangeEnabled,
		MailRangeAfterFinish	,
		MailRangeAfterClose		,
		MailRangeRepeat			,
		MailRangeRepeatMins		,
		MailRangeRepeatUntil	,
		MailRangeRepeatBreakable,
		MailCommEnabled,
		MailCommAfterFinish		,
		MailCommAfterClose		,
		MailCommRepeat			,
		MailCommRepeatMins		,
		MailCommRepeatUntil		,
		MailCommRepeatBreakable	,
		st.EventType AS TestAlarmEventType,
		CONVERT(smallint, st.Value * Scale + Calibration) AS TestAlarmValue

	FROM dbo.Sensors s
	LEFT JOIN dbo.SensorTests st on s.Id = st.SensorId AND st.Active = 1
	WHERE Unitid =@unitId

END

