﻿CREATE PROCEDURE fda_GetPatientDetailsFilteredTotalCount
@TreeUnitIds nvarchar(max),
@IdFrom int = null
,@IdTo int = null
,@Name nvarchar(200) = null
,@LastName nvarchar(200) = null
,@DisplayName nvarchar(200) = null
,@Pesel char(200) = null
,@WardIdFrom int = null
,@WardIdTo int = null
,@WardUnitName nvarchar(200) = null
,@WardUnitLocation nvarchar(200) = null
,@WardAdmissionDateUtcFrom datetime = null
,@WardAdmissionDateUtcTo datetime = null
,@RoomIdFrom int = null
,@RoomIdTo int = null
,@RoomUnitName nvarchar(200) = null
,@RoomUnitLocation nvarchar(200) = null
,@RoomAdmissionDateUtcFrom datetime = null
,@RoomAdmissionDateUtcTo datetime = null
,@BandIdFrom int = null
,@BandIdTo int = null
,@BandBarCode nvarchar(200) = null
,@BandSerialNumber nvarchar(200) = null
,@CurrentUnitName nvarchar(200) = null
,@CurrentUnitLocation nvarchar(200) = null
AS
BEGIN

 DECLARE @cmd nvarchar(max)
 SET @cmd = 'SELECT COUNT(1) '
		+ dbo.dsql_GetPatientDetails_From()
		+ dbo.dsql_GetPatientDetails_Where(@TreeUnitIds,
			@IdFrom, @IdTo, @Name, @LastName, @DisplayName, @Pesel
			, @WardIdFrom, @WardIdTo, @WardUnitName, @WardUnitLocation, @WardAdmissionDateUtcFrom, @WardAdmissionDateUtcTo
			, @RoomIdFrom, @RoomIdTo, @RoomUnitName, @RoomUnitLocation, @RoomAdmissionDateUtcFrom, @RoomAdmissionDateUtcTo
			, @BandIdFrom, @BandIdTo, @BandBarCode, @BandSerialNumber, @CurrentUnitName, @CurrentUnitLocation
			)

 EXEC(@cmd)
-- print @cmd
END