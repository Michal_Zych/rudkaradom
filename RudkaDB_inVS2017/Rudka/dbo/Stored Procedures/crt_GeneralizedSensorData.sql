﻿CREATE PROCEDURE [dbo].[crt_GeneralizedSensorData]
@sensor SMALLINT, @dateFrom DATETIME, @dateTo DATETIME, @mode INT, @rowCount INT
AS EXTERNAL NAME [m2mTeam.Monitoring.CLR].[StoredProcedures].[crt_GeneralizedSensorData]

