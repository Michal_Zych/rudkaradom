﻿
CREATE PROCEDURE [dbo].[sync_Process_PALC]
	@RecordId int 
AS
BEGIN
	INSERT _log(LogDate, ProcedureName, Info)
	values (dbo.GetCurrentDate(), 'sync_Process_PALC', @RecordId)
	RETURN
	SET NOCOUNT ON;

	DECLARE @ASSECO_OPERATOR_ID int = 0
		,@REASON nvarchar(100) = 'Źródło AMMS INRecordId=' + CONVERT(nvarchar(10), @RecordId)
		,@NULL_VALUE nvarchar(100) = 'null'
		,@OperationDateUtc dateTime = dbo.GetCurrentUtcDate()

	
	DECLARE
		@record nvarchar(max)
		,@recId nvarchar(100)
		,@changeDateUtc dateTime
		,@patientId int
		,@room nvarchar(100)
		,@unitCode nvarchar(100)
		,@roomId int
		,@wardid int
		,@error nvarchar(max) 
		,@processed bit = 1

	DECLARE 
		@currentRoomId int
		,@currentWardId int
		,@inRoomDate dateTime
		,@inWardDate dateTime
		,@inHospitalDate dateTime
		
	SELECT @recId = RecId, @record = Record, @changeDateUtc = StoreDateUtc FROM tech.sync_RecordsIN WHERE Id = @RecordId
	SELECT @patientId = M2mId FROM arch.ext_Patients WHERE ExtId = @recId


	IF @patientId IS NULL SELECT @error = 'Brak mapowania pacjenta ' + @recId
	ELSE 
	BEGIN
		SELECT @currentRoomId = RoomId, @currentWardId = WardId, @inRoomDate = RoomDateUtc, @inWardDate = WardDateUtc, @inHospitalDate = HospitalDateUtc
		FROM dbo.Patients 
		WHERE Id = @patientId

		IF @inHospitalDate IS NULL SELECT @error = 'Pacjent nie hospitalizowany Id=' + CONVERT(nvarchar(10), @patientId)
	END
	
		
	IF @error IS NULL
	BEGIN
		SET @room = LEFT(dbo.srv_KeyValue_GetValue(@Record, 'ROOM'), 100) 
		SET @unitCode = LEFT(dbo.srv_KeyValue_GetValue(@Record, 'UNIT_CODE'), 100)

		IF @room = @NULL_VALUE SELECT @roomId = NULL
		ELSE
		BEGIN
			SELECT @roomId = M2mId FROM arch.ext_Units WHERE ExtId = @room
			IF @roomId IS NULL SELECT @error = 'Niezamapowana sala: ' + @room
		END

		IF @unitCode = @NULL_VALUE SELECT @wardId = NULL
		ELSE
		BEGIN
			SELECT @wardId = M2mId FROM arch.ext_Units WHERE ExtId = @unitCode
			IF @wardId IS NULL SELECT @error = COALESCE(@error, '') + '; Niezamapowany oddział: ' + @unitCode
		END

		IF @roomId IS NOT NULL AND @wardId IS NULL SELECT @wardId = dbo.GetWardIdForUnit(@roomId)
	END

	IF @error IS NULL
		AND @changeDateUtc > COALESCE(@inRoomDate, @inWardDate, @inHospitalDate)
		AND (COALESCE(@currentWardId, -1) <> COALESCE(@wardId, -1)   OR  COALESCE(@currentRoomId, -1) <> COALESCE(@roomId, -1))
	BEGIN
		DECLARE @result TABLE(Id int, ErrorCode int, ErrorMessage nvarchar(max))
		
		INSERT @result
		EXEC dbo.ev_AssignPatientToWardRoom @patientId, null, null, @wardId, @roomId, @ASSECO_OPERATOR_ID, @Reason, 1, @changeDateUtc

		SELECT @error = ErrorMessage FROM @result
	END

	IF @error IS NOT NULL SELECT @processed = 0
	
	UPDATE tech.sync_RecordsIN SET
		Processed = @processed
		,ProcessedDateUtc = @OperationDateUtc
		,Error = @error
		,Try = COALESCE([Try], 0) + 1
	WHERE Id = @RecordId
END