﻿CREATE PROCEDURE [dbo].[sync_Process_USER]
	@RecordId int 
AS
BEGIN
	INSERT _log(LogDate, ProcedureName, Info)
	values (dbo.GetCurrentDate(), 'sync_Process_USER', @RecordId)

	SET NOCOUNT ON;
	EXEC dbo.sync_Process_USRS @RecordId
END