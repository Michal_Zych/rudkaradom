﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	Edycja danych Unit'a o podanym ID
-- =============================================
CREATE PROCEDURE [dbo].[unt_EditUnit]
(
	  @id int
	, @name  UNIT_NAME
	, @unitTypeIdV2 int
	, @pictureId int
	, @description nvarchar(200)
	, @operatorId int
	, @logEntry nvarchar(max)
	, @reason nvarchar(max)
)
AS
BEGIN
	DECLARE @OPERATION_DELETE tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_EDIT tinyint = 2

		,@RECORD_UNIT tinyint = 0
		,@RECORD_USER tinyint = 1

	BEGIN TRANSACTION
		BEGIN TRY
			Update dbo.Units
				SET Name = @name
				, UnitTypeIdv2 = @unitTypeIdV2
				, Pictureid = @pictureId
				, [Description] = @description
			WHERE ID = @id

			INSERT dbo.Logs(OperationDate, OperatorId, OperationType, ObjectType, ObjectId, Reason, Info)
				VALUES(dbo.GetCurrentDate(), @operatorId, @OPERATION_EDIT, @RECORD_UNIT, @Id, @reason, @logEntry)
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			SELECT -1
			RETURN
		END CATCH

	COMMIT TRANSACTION
	
	SELECT @Id	
END

