﻿

CREATE PROCEDURE [dbo].[usr_GetLogs]
	@ClientId int
	,@DateStart datetime
	,@DateEnd datetime
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @OPERATION_DELETE tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_EDIT tinyint = 2

		,@RECORD_UNIT tinyint = 0
		,@RECORD_USER tinyint = 1
		,@RECORD_SYSTEMSETTING tinyint = 255

	DECLARE @maxRows int = [dbo].[settings_GetInt](@ClientId, 'MaxRwosLogs')

	SELECT TOP(@maxRows)
		OperationDate
		,usr.LastName + ' ' + usr.Name AS Operator
		,OperationType
		,ObjectType
		, CASE 
			WHEN ObjectType = @RECORD_UNIT THEN (SELECT TOP 1 Name FROM dbo.Units u WHERE u.ID = ObjectID)
			WHEN ObjectType = @RECORD_USER THEN (SELECT TOP 1 LastName + ' ' + Name FROM dbo.Users u WHERE u.ID = ObjectID)
			WHEN ObjectType = @RECORD_SYSTEMSETTING THEN (SELECT TOP 1 Name From dbo.Settings WHERE Id = ObjectID)
			ELSE NULL
		  END AS Record
		, Reason
		, Info
	FROM dbo.Logs l
	JOIN dbo.Users usr ON l.OperatorId = usr.ID
	WHERE OperationDate BETWEEN @DateStart AND @DateEnd
END
