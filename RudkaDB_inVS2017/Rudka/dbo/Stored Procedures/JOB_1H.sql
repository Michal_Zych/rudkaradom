﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[JOB_1H] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	exec [dbo].[JOB_MovePatientsEventsToArchive]
    exec [dbo].[JOBdaily_GenerateBatteryTermAlarms]
	exec [dbo].[JOBdaily_MoveEventsToArchive]
END