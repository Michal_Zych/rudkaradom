﻿-- =============================================
-- Author:  
-- Create date: 
-- Description: 
-- =============================================
CREATE PROCEDURE [dbo].[usr_GetVersions]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT Version,IssueDate,InstallationDate,Description
	FROM dbo.Versions
	ORDER BY Id

END

