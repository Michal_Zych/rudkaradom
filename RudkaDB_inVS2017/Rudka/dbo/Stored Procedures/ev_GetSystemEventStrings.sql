﻿


CREATE PROCEDURE [dbo].[ev_GetSystemEventStrings]
	@EventId int
AS
BEGIN
	SET NOCOUNT ON;
	

	IF EXISTS (SELECT 1 FROM dbo.ev_Events WHERE Id = @EventId)
		SELECT Id, [EventData], Reason
		FROM dbo.ev_Events e
		WHERE Id = @EventId
	ELSE
		SELECT Id, [EventData], Reason
		FROM arch.ev_Events
		WHERE Id = @EventId
END