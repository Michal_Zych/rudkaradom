﻿

CREATE  PROCEDURE [dbo].[unt_GetDisableAlarmUnits]
 @units IDs_LIST
AS
BEGIN
 SET NOCOUNT ON;
        
	DECLARE @now datetime = [dbo].[GetCurrentDate]()
	DECLARE @time time(7)
	SET @time = CONVERT(time(7), @now)


	SELECT u.ID AS UnitID
	FROM dbo.Sensors s
	JOIN dbo.Units u ON u.ID = s.UnitId
	JOIN dbo.SplitWordsToInt(@units) ul on ul.value = u.ID 
	LEFT JOIN dbo.AlarmsOmittedRanges aor on aor.SensorID = s.ID
	WHERE 
		(s.Enabled = 0
		 OR s.AlDisabled = 1 
		 OR s.AlDisabledForHours = 1
			AND ( (@time BETWEEN s.AlDisableHoursFrom1 AND s.AlDisableHoursTo1)
				  OR
				  (@time BETWEEN s.AlDisableHoursFrom2 AND s.AlDisableHoursTo2)
				)
		 OR s.AlDisabledForRanges = 1
			AND (@now BETWEEN aor.DateStart AND aor.DateEnd)
		)

END



