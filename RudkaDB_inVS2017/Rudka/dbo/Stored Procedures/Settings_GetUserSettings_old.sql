﻿

CREATE PROCEDURE [dbo].[Settings_GetUserSettings_old]
	@clientId int,
	@userId int
AS
BEGIN
	SELECT Name, 
		COALESCE(CONVERT (nvarchar(1024), [dbo].[settings_GetInt_old](ClientId, Name))
			, [dbo].[settings_GetString_old](ClientId, Name)) AS Value
	FROM dbo.Settings_old
	WHERE ClientId = @ClientId

END
