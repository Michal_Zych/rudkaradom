﻿

CREATE PROCEDURE [dbo].[Alarms_CheckAlarmsWithoutShorts__old]
	@date_from datetime,
	@user int,
	@units IDs_LIST
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE --stałe typów alarmów EventType
		 @VALUE_TO_LOW tinyint = 1
		,@VALUE_TO_HIGH tinyint = 2
		,@NO_COMMUNICATION tinyint = 3
		,@NO_SENSOR tinyint = 3
		,@NO_DEVICE tinyint = 4
		,@NO_HUB tinyint = 5

	DECLARE @clientId int = 1
		
	DECLARE @maxRows int = [dbo].[settings_GetInt_old](@clientId, 'MaxRowsAlarmsPopup')
	
	if	exists (select 1 from Users u inner join UsersLogs ul on u.LastPingID = ul.ID where u.ID = @user) --ul.Monitoring = 1 and 
	begin

		SELECT top(@maxRows)
			a.ID as [ID],
			
			a.EventType as [EventType],
			a.[Timestamp] as [Timestamp],
			
			a.SensorID as [SensorID],
			u.Name as [SensorName],
			u.Id as [UnitId],
			dbo.GetUnitLocation(u.id, default, default, default) as Location,
			s.MU AS MU,

			a.LoRange * s.Scale AS LoRange,
			a.UpRange * s.Scale AS UpRange,
			dat.Value * s.Scale AS Value,
			
			a.DateEnd as DateEnd,
			a.DateStatus as AlarmCloseDate,
			a.UserStatus as AlarmCloseUserID,
			isnull(us.Name,'') + ' ' + isnull(us.LAstName,'') as AlarmCloseUserName,
			a.CommentStatus as AlarmCloseComment
		FROM
			Alarms a
			left join Measurements dat on dat.SensorID = a.SensorId and dat.MeasurementTime = a.Timestamp
			join Sensors s on s.id =  a.SensorId
			inner join Units u on u.Id = s.UnitId
			left join Users us on us.ID = a.UserStatus
			join dbo.SplitWordsToInt(@units) SU on SU.value = u.ID
		where (a.status != 1 or a.status is null)
			and ( DATEDIFF(SECOND, a.Timestamp, Coalesce(a.DateEnd, dbo.GetCurrentDate())) 
				  > (CASE 
						WHEN a.EventType < @NO_COMMUNICATION		THEN s.GuiRangeDelay
						ELSE s.GuiCommDelay
				     END))
		ORDER BY
		a.[Timestamp] desc
	
	end    
END
