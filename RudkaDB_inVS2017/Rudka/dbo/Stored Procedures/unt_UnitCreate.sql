﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	Zwraca ID utworzonego Unit'a  -1 oznacza błąd
-- =============================================
CREATE PROCEDURE [dbo].[unt_UnitCreate]
(
	  @name  UNIT_NAME
	, @parentUnitId int
	, @unitTypeIdV2 int
	, @pictureId int
	, @description nvarchar(200)
	, @operatorId int
	, @reason nvarchar(max)
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @TEST bit = 0

IF @TEST = 1 
BEGIN -- sekcja dla testów
	DECLARE @info nvarchar(max) = 'EXEC unt_UnitCreate '''
		+ @name + ''', ' 
		+ COALESCE(CONVERT(nvarchar(10), @parentUnitId), 'NULL') + ', '
		+ COALESCE(CONVERT(nvarchar(10), @unitTypeIdV2), 'NULL') + ', '
		+ COALESCE(CONVERT(nvarchar(10), @pictureId), 'NULL') + ', '
		+ COALESCE('''' + @description + '''', 'NULL') + ', 0'
		+ ', NULL'
	INSERT _Actions(Action) VALUES(@info)
END	

	DECLARE @CLIENT_ID int = 1
	DECLARE @UPDATE_ENDING_EVENT_ID bit = 1

	DECLARE @currentDateUtc dateTime = dbo.GetCurrentUTCDate()
		,@json nvarchar(max)

	DECLARE @OPERATION_DELETE tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_EDIT tinyint = 2

		,@RECORD_UNIT tinyint = 0
		,@RECORD_USER tinyint = 1
		,@DEFAULT_LO_RANGE smallint = 0
		,@DEFAULT_HI_RANGE smallint = 0

	DECLARE @unitId int = -1
		,@isSensor bit
		,@sensorID smallint
		,@eventId int
	
	BEGIN TRANSACTION
		BEGIN TRY
			INSERT dbo.Units(Name, ParentUnitID, UnitTypeID, Active, ClientID, UnitTypeIdv2, Pictureid, [Description])
				VALUES(@name, @parentUnitId, 3, 1, @CLIENT_ID, @unitTypeIdV2, @pictureId, @description)
			SELECT @unitId = SCOPE_IDENTITY()

			EXEC @json = dbo.SerializeJSON_Unit @unitId
			
			INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, OperatorId, EndDateUtc, StoreDateUtc, [EventData], Reason)
			SELECT @currentDateUtc, eventType.[Create], eventSeverity.Event, @parentUnitId, @unitId, objectType.Unit, @parentUnitId, @operatorId, @currentDateUtc, @currentDateUtc, @json, @reason
			FROM enum.EventType eventType
			JOIN enum.EventSeverity eventSeverity ON 1 = 1
			JOIN enum.ObjectType objectType ON 1 = 1
			SELECT @eventId = SCOPE_IDENTITY()
			IF @UPDATE_ENDING_EVENT_ID = 1
			BEGIN
				UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
			END


			IF @parentUnitId IS NULL SET @parentUnitId = 0
			EXEC dbo.al_UnitAlarmConfigCopy @parentUnitId, @unitId, @operatorId, 'Nowa jednostka organizacyjna'

			COMMIT TRANSACTION
			SELECT @UnitId AS Result, NULL as ErrorCode, NULL as ErrorMessage
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			SELECT CONVERT(int, null) AS Result, ERROR_NUMBER() AS ErrorCode, ERROR_MESSAGE() AS ErrorMessage
		END CATCH
END