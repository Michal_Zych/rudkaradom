﻿
CREATE PROCEDURE [dbo].[msg_DeleteMessages] 
	@messages IDs_LIST
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @IDs TABLE (ID int not null)
	INSERT @IDs SELECT Value FROM  dbo.SplitWordsToInt(@messages)

	DECLARE @result bit = 0

	BEGIN TRANSACTION
		BEGIN TRY
			DELETE dbo.MessagesLog WHERE MessageID IN (SELECT ID FROM @IDs)
			DELETE dbo.Messages WHERE Id IN (SELECT ID FROM @IDs)
			SELECT @result = 1
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
		END CATCH
	SELECT @result AS Result
END
