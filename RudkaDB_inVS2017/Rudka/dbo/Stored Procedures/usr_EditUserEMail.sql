﻿
-- =============================================
-- Author:		
-- Create date: 
-- Description:	Edycja eMaila użytkownika o podanym ID
-- =============================================
CREATE PROCEDURE [dbo].[usr_EditUserEMail]
(
	  @id int
	, @eMail nvarchar(100)
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @OPERATION_DELETE tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_EDIT tinyint = 2

		,@RECORD_UNIT tinyint = 0
		,@RECORD_USER tinyint = 1

		--kody błędów
	DECLARE
		@ERROR int = -1
		,@oldEmail nvarchar(100)

	BEGIN TRANSACTION
		BEGIN TRY
			SELECT @oldEmail = eMail FROM dbo.Users WHERE Id = @id

			UPDATE dbo.Users 
			SET eMail = @eMail
			WHERE ID = @id

			INSERT dbo.Logs(OperationDate, OperatorId, OperationType, ObjectType, ObjectId, Reason, Info)
					VALUES(dbo.GetCurrentDate(), @id, @OPERATION_EDIT, @RECORD_USER, @id, '', 'e-mail z:' + COALESCE(@oldeMail, '') + ' na: ' + COALESCE(@eMail, ''))

		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			SELECT @ERROR
			RETURN
		END CATCH
	COMMIT TRANSACTION

	SELECT @Id
END


