﻿
-- =============================================
-- Author:		
-- Create date: 
-- Description:	zwraca czujniki skojarzone z unitem (lub jego bezpośrednimi podunitami)
-- =============================================
CREATE PROCEDURE [dbo].[unt_GetSensorsInUnit]
	@unitId int
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT s.Id, u.Name, dbo.GetUnitLocation(u.Id, 0, 0, DEFAULT) AS Location
	FROM dbo.Sensors s
	JOIN dbo.Units u ON u.ID = s.UnitId
	WHERE u.Id = @unitId
	UNION
	SELECT s.Id, u.Name, dbo.GetUnitLocation(u.Id, 0, 0, DEFAULT)
	FROM dbo.Sensors s
	JOIN dbo.Units u ON u.Id = s.UnitId
	WHERE u.ParentUnitID = @unitId

END
