﻿


CREATE PROCEDURE [dbo].[usr_CreateUser]
(
	  @login  USER_LOGIN
	, @password USER_PASSWORD
	, @clientId int
	, @name USER_NAME
	, @lastName USER_NAME
	, @phone USER_PHONE = NULL
	, @eMail nvarchar(100) = null
	, @unit int					--unit, w którym konto zostanie dodane do roli users
	, @operatorId int
	, @logEntry nvarchar(max)
	, @reason nvarchar(max)
)
AS
BEGIN
	DECLARE @OPERATION_DELETE tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_EDIT tinyint = 2

		,@RECORD_UNIT tinyint = 0
		,@RECORD_USER tinyint = 1

		--kody błędów
	DECLARE
		@ERROR int = -1
		,@LOGIN_EXISTS int = -2
		,@operationDateUtc dateTime = dbo.GetCurrentUtcDate()

	DECLARE @userId integer
		,@eventId int
	
	
	SET @userId = dbo.Usr_GetIdForLogin(@login, @clientId)
	IF @userId IS NOT NULL 
	BEGIN
		SELECT @LOGIN_EXISTS
		RETURN
	END
	
	SET @userid = @ERROR
	BEGIN TRANSACTION 
		BEGIN TRY
			INSERT INTO dbo.Users (Login,  Password,  ClientID,  Name,  LastName,  Phone, eMail, Active, PasswordChangeDate)
			VALUES   	      (@login, @password, @clientId, @name, @lastName, @phone, @eMail,1, dbo.GetCurrentDate())
			SELECT @userId = SCOPE_IDENTITY()
	
			-- rola Użytkownik w podanym unicie
			DECLARE @defRole int
			SELECT @defRole = RoleDefForNewUser FROM dbo.ev_Settings

			INSERT INTO dbo.UsersUnitsRoles
			SELECT @userId, @unit, @defRole

			INSERT dbo.Logs(OperationDate, OperatorId, OperationType, ObjectType, ObjectId, Reason, Info)
					VALUES(dbo.GetCurrentDate(), @operatorId, @OPERATION_CREATE, @RECORD_USER, @userId, @reason, @logEntry)
			
			DECLARE @json nvarchar(max) = 'SELECT Id, Login, Name, LastName, Active FROM dbo.Users WHERE Id = ' + CONVERT(nvarchar(10), @userId)
			EXEC @json = dbo.SerializeJSON @json

			INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason)
			SELECT @operationDateUtc, eventType.[Create], eventSeverity.[Event], @unit, @userId, objectType.[User], @unit, @OperationDateUtc, @OperationDateUtc, @operatorId, @json, @reason 
			FROM enum.EventType eventType				
			JOIN enum.EventSeverity eventSeverity ON 1 = 1
			JOIN enum.ObjectType objectType ON 1 = 1
			SELECT @eventId = SCOPE_IDENTITY()
			
			UPDATE ev_Events SET EndingEventId = @eventId WHERE Id = @eventId

		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			SELECT @ERROR
			RETURN
		END CATCH

	COMMIT TRANSACTION
	SELECT @userId	
END
