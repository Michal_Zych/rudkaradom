﻿CREATE PROCEDURE [dbo].[sync_Process_VISI]
	@RecordId int 
AS
BEGIN
	INSERT _log(LogDate, ProcedureName, Info)
	values (dbo.GetCurrentDate(), 'sync_Process_VISI', @RecordId)
	RETURN
	SET NOCOUNT ON;
END