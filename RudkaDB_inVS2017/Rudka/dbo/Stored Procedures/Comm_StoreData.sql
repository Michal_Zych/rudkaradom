﻿

CREATE PROCEDURE [dbo].[Comm_StoreData]
	@sensorid smallint,
	@timestamp datetime,
	@value smallint,
	@eventType int --- 0 = wszystko ok
AS
BEGIN
	SET NOCOUNT ON;
	
	exec [dbo].[Com_StoreData] @sensorid, @timestamp, @value, @eventType

	select 1 as data_id;
	return
	------------------------------------


	if(@eventType > 0)
	begin

		INSERT INTO [dbo].[Alarms]
			   ([SensorID]
			   ,[Timestamp]
			   ,[EventType]
			   ,[Status]
			   ,[UserStatus]
			   ,[DateStatus]
			   ,[CommentStatus]
			   ,[LoRange]
			   ,[UpRange]
			   ,[DateEnd])
		 VALUES
			   (@sensorid,@timestamp,@eventType,0,NULL,NULL,NULL,1,1,NULL)
	end
	else
	update alarms set dateend=timestamp where sensorid=@sensorid and dateend is null

	if not exists (select 1 from [dbo].[Measurements] where [MeasurementTime]=@timestamp and SensorId=@sensorid)
		insert into [dbo].[Measurements](SensorId,[MeasurementTime],Value,StoreDelay) values(@sensorid,@timestamp,@value,DATEDIFF(ss,dbo.GetCurrentDate(),@timestamp))

	--current
	if not exists (select 1 from [dbo].[MeasurementsCurrent] where SensorId=@sensorid)
		insert into [dbo].[MeasurementsCurrent](SensorId,[MeasurementTime],Value,[MeasurementStoreTime],[HyperMesurementTime],[HyperValue]) values(@sensorid,@timestamp,@value,dbo.GetCurrentDate(),@timestamp,@value)
	else
		update [dbo].[MeasurementsCurrent] set value=@value,[MeasurementTime]=@timestamp,[HyperValue]=@value,[HyperMesurementTime]=@timestamp,[MeasurementStoreTime]=dbo.GetCurrentDate() where [SensorID]=@sensorid

	select 1 as data_id;

END

