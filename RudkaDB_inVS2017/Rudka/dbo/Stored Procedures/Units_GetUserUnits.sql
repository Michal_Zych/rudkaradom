﻿-- =============================================
-- Author:		pi
-- Create date: 10,10,2012
-- Description: Returns Units For User (not filtered)
-- =============================================
CREATE PROCEDURE [dbo].[Units_GetUserUnits] @userId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ROOTUNIT int = 1
	DECLARE @SERVICEACCOUNT int = 1
	DECLARE @HYPERADMIN int = 4

	DECLARE @role int

	IF @userId = @SERVICEACCOUNT
	BEGIN
		SELECT @role = RoleID FROM dbo.UsersUnitsRoles WHERE UserID = @SERVICEACCOUNT AND UnitID = @ROOTUNIT
		IF COALESCE(@Role, -1) <> @HYPERADMIN
			INSERT dbo.UsersUnitsRoles VALUES(@SERVICEACCOUNT, @ROOTUNIT, @HYPERADMIN)
	END


	select *
	from Units_GetUserUnits_Func(@userId)

END

