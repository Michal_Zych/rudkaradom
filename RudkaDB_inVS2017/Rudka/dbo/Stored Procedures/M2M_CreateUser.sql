﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	Zwraca ID utworzonego użytkownika -1 - błąd; -2 login jst używany
-- =============================================
Create PROCEDURE [dbo].[M2M_CreateUser]
(
	  @login  USER_LOGIN
	, @pwd nvarchar(100)
	, @clientId int
	, @role int
	, @name USER_NAME
	, @lastName USER_NAME
	, @phone USER_PHONE = NULL
	, @eMail nvarchar(100) = null
	, @unit int					--unit, w którym konto zostanie dodane do roli users
	, @operatorId int
	, @logEntry nvarchar(max)
	, @reason nvarchar(max)
)
AS
BEGIN
	DECLARE @OPERATION_DELETE tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_EDIT tinyint = 2

		,@RECORD_UNIT tinyint = 0
		,@RECORD_USER tinyint = 1
		,@currentDate dateTime = dbo.GetCurrentDate()
		--kody błędów
	DECLARE
		@ERROR int = -1
		,@LOGIN_EXISTS int = -2


	DECLARE @userId integer,
	@password USER_PASSWORD= (select dbo.usr_GetEncodedPassword(@pwd))
	
	
	SET @userId = dbo.Usr_GetIdForLogin(@login, @clientId)
	IF @userId IS NOT NULL 
	BEGIN
		SELECT @LOGIN_EXISTS
		RETURN
	END
	
	SET @userid = @ERROR
	BEGIN TRANSACTION 
		BEGIN TRY
			INSERT INTO dbo.Users (Login,  Password,  ClientID,  Name,  LastName,  Phone, eMail, Active, PasswordChangeDate)
			VALUES   	      (@login, @password, @clientId, @name, @lastName, @phone, @eMail,1, @currentDate)
			SELECT @userId = SCOPE_IDENTITY()
	
			-- rola Użytkownik w podanym unicie
			INSERT INTO dbo.UsersUnitsRoles 
			SELECT @userId, @unit, @role

			INSERT dbo.Logs(OperationDate, OperatorId, OperationType, ObjectType, ObjectId, Reason, Info)
					VALUES(@currentDate, @operatorId, @OPERATION_CREATE, @RECORD_USER, @userId, @reason, @logEntry)
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			SELECT @ERROR
			RETURN
		END CATCH

	COMMIT TRANSACTION
	SELECT @userId	
END

