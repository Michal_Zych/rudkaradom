﻿-- Create date: 2015-12-08
-- Description:	aktualizuje wpis temperatury do testowania
-- =============================================
CREATE PROCEDURE [dbo].[test_SetTestValue]
	@sensorId int
	,@value smallint
AS
BEGIN

	SET NOCOUNT ON;

	IF (EXISTS(
		SELECT 1 FROM dbo.SensorTests WHERE SensorId = @SensorId AND Value IS NULL AND Active = 1))
		BEGIN
			SELECT 0;
			return;
		END

	DECLARE @DEFAULT_SCALE real;
	SET @DEFAULT_SCALE = 0.1;

	DECLARE @testActive bit;
	DECLARE @scale real;
	DECLARE @calibration smallint;

	SELECT @scale = Scale, @calibration = Calibration
	FROM dbo.Sensors
	WHERE Id = @sensorId;

	SET @value = round(@value / COALESCE(@scale, @DEFAULT_SCALE), 1) - @calibration; 

	UPDATE dbo.SensorTests
	SET Value = @value
	WHERE SensorId = @sensorId
		AND Active = 1

	SELECT @@ROWCOUNT;
	
END