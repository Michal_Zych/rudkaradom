﻿
CREATE PROCEDURE [dbo].[rep_GetReportDescription]
	@UserReportId int
AS
BEGIN
	DECLARE
		@dateFrom dateTime
		,@dateTo dateTime
		,@title nvarchar(max)
		,@unitId int
		,@reportId int
		,@name nvarchar(200)
		,@userName nvarchar(200)

	SELECT @reportId = ur.ReportId, @dateFrom = ur.DateFrom, @dateTo = ur.DateTo, @unitId = ur.UnitId, @title = r.Title
		,@userName = COALESCE(u.LastName, '') + COALESCE(' ' + u.Name, '')
	FROM dbo.UsersReports ur
	JOIN dbo.Reports r ON r.ID = ur.ReportID
	JOIN dbo.Users u ON u.Id = ur.UserID
	WHERE ur.Id = @UserReportId

	IF @reportId IN (5, 6)
	BEGIN
		SELECT @name = COALESCE(LastName, '') + COALESCE(' ' + Name, '')
		FROM arch.Patients
		WHERE Id = @unitId
		
		IF COALESCE(@name, '') = ''
		SELECT @name = COALESCE(LastName, '') + COALESCE(' ' + Name, '')
		FROM dbo.Patients 
		WHERE Id = @unitId
	END
	ELSE
	BEGIN
		SELECT @name = dbo.GetUnitLocation(@unitId, 0, 1, DEFAULT)
	END
	SET @title = REPLACE(@title, '\n', CHAR(10))
	SET @title = REPLACE(REPLACE(@title, '<Patient>', @name), '<UnitName>', @name)
	SET @title = REPLACE(@title, '<DateFrom>', COALESCE(CONVERT(nvarchar(16), @dateFrom, 120), 'początku'))
	SET @title = REPLACE(@title, '<DateTo>', COALESCE(CONVERT(nvarchar(16), @dateTo, 120), 'końca'))

	SELECT @title AS Title
		,@userName AS PreparedBy
		,dbo.GetCurrentDate() as PreparedDate
END