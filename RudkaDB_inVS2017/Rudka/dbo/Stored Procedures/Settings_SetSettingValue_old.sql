﻿
CREATE PROCEDURE [dbo].[Settings_SetSettingValue_old]
	@ClientId int
	,@Name nvarchar(50)
	,@Value nvarchar(1024)
	, @operatorId int
	, @logEntry nvarchar(max)
	, @reason nvarchar(max)
AS
BEGIN
	DECLARE @OPERATION_DELETE tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_EDIT tinyint = 2

		,@RECORD_SYSTEMSETTING tinyint = 255

	DECLARE @newValue nvarchar(1024)
		,@oldValue nvarchar(1024)
		,@settingId int
	DECLARE @result bit = 0

	BEGIN TRANSACTION
		BEGIN TRY
			UPDATE dbo.Settings_old
				SET Value = @Value
			WHERE ClientId = @ClientId AND Name = @Name

			SELECT @newValue = COALESCE(CONVERT (nvarchar(1024), [dbo].[settings_GetInt_old](ClientId, Name))
											, [dbo].[settings_GetString_old](ClientId, Name))
				,@settingId = ID
			FROM dbo.Settings_old 
			WHERE ClientID = @ClientId AND Name  = @Name	

			IF @newValue = @Value SET @result = 1
			ELSE SET @result = 1 / 0 --rollback

			INSERT dbo.Logs(OperationDate, OperatorId, OperationType, ObjectType, ObjectId, Reason, Info)
					VALUES(dbo.GetCurrentDate(), @operatorId, @OPERATION_EDIT, @RECORD_SYSTEMSETTING, @settingId, @reason, @logEntry)
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
		END CATCH

	SELECT @result AS Result
END
