﻿
CREATE PROCEDURE [dbo].[_tech_GenerateFDAbasedOnView]
-- Format Widoku
--	SELECT a.a as a
--		,b.b as b
--		,dbo.Function(a) as Func
--	FROM a
--	LEFT JOIN b on a.a=b.b
--	WHERE a.a=x
	@ViewName nvarchar(100) 
	,@RecordsName nvarchar(100) 
	,@ProcPrefix nvarchar(5) = 'fda'
	
	,@SingleUnitIdForProc bit = 0
	,@useIsMatchedForNvarchar bit = 1
	,@nvarcharParameterLength int = 200
	,@printStatementAsOutput bit = 0
AS
BEGIN
	DECLARE @NEW_LINE nvarchar(5) = CHAR(13) + CHAR(10)

	DECLARE @definition nvarchar(max)
		,@defSelect nvarchar(max)
		,@defFrom nvarchar(max)
		,@defWhere nvarchar(max)

	SELECT @definition = Definition
	FROM sys.objects o
	JOIN sys.sql_modules m on m.object_id = o.object_id
	WHERE o.object_id = object_id( @ViewName)
		AND o.Type      = 'V'

	--print @definition return


	SET @defSelect = SUBSTRING(@definition, CHARINDEX('SELECT ', @definition) + 7, CHARINDEX('FROM ', @definition) - (CHARINDEX('SELECT ', @definition) + 7))
	SET @defSelect = REPLACE(REPLACE(REPLACE(@defSelect, CHAR(13), ''), CHAR(10), ''), CHAR(9), '')
	WHILE CHARINDEX('  ', @defSelect) > 0 SET @defSelect = REPLACE(@defSelect, '  ', ' ')
	SET @defSelect = REPLACE(@defSelect, '''', '''''')

	--print @defSelect return


	SET @defFrom = SUBSTRING(@definition, CHARINDEX('FROM ', @definition) + 5, COALESCE(NULLIF(CHARINDEX('WHERE ', @definition), 0), 999999)  - (CHARINDEX('FROM ', @definition) + 5))
	--SET @defFrom = REPLACE(REPLACE(REPLACE(@defFrom, CHAR(13), ''), CHAR(10), ''), CHAR(9), '')
	WHILE CHARINDEX('  ', @defFrom) > 0 SET @defFrom = REPLACE(@defFrom, '  ', ' ')
	
	--print @deffrom return

	IF CHARINDEX('WHERE', @definition) > 1
	BEGIN
			SET @defWhere = SUBSTRING(@definition, CHARINDEX('WHERE ', @definition) + 6, COALESCE(NULLIF(CHARINDEX('GROUP BY ', @definition), 0), 999999)  - (CHARINDEX('WHERE ', @definition) + 6))
			SET @defWhere = REPLACE(REPLACE(REPLACE(@defWhere, CHAR(13), ''), CHAR(10), ''), CHAR(9), '')
			WHILE CHARINDEX('  ', @defWhere) > 0 SET @defWhere = REPLACE(@defWhere, '  ', ' ')
	END
	-- print @defWhere return


	DECLARE @columns TABLE(Nr int identity(1, 1), Name nvarchar(1000), Alias nvarchar(100), DataType nvarchar(1000))
	SET @defSelect = SUBSTRING(@definition, CHARINDEX('SELECT ', @definition) + 7, CHARINDEX('FROM ', @definition) - (CHARINDEX('SELECT ', @definition) + 7))
	SET @defSelect = ','+ REPLACE(@defSelect, CHAR(9), '')
	SET @defSelect = REPLACE(@defSelect, '''', '''''')

	INSERT @columns(Name, Alias)
	SELECT SUBSTRING(SUBSTRING(Value, 1, CHARINDEX(' as ', Value)), CHARINDEX(',',SUBSTRING(Value, 1, CHARINDEX(' as ', Value))) + 1, 9999)
		, SUBSTRING(Value, CHARINDEX(' as ', Value) + 4, 9999)
	FROM dbo.fn_Split(@defSelect, CHAR(13))
	WHERE LTRIM(RTRIM(REPLACE(Value, CHAR(10), ''))) <> '' 

	UPDATE c	
		SET DataType = i.DATA_TYPE
			,Alias = REPLACE(REPLACE(Alias, '[', ''), ']', '')
	FROM @columns c
	JOIN INFORMATION_SCHEMA.COLUMNS  i ON c.Nr = i.ORDINAL_POSITION
	WHERE i.TABLE_NAME = @ViewName

	UPDATE @columns SET DataType = 'nvarchar(max)'
	WHERE CHARINDEX('sdi', REVERSE(Alias)) = 1

update @columns SET Name = REPLACE(Name, '''', '''''') /******************************************************************************************************************************/
	--select * from @columns return

	DECLARE @drop nvarchar(max)
	SET @drop = 'IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(''%'') AND Type IN (''FN'', ''IF'', ''TF'', ''FS'', ''FT'')) DROP FUNCTION %'
	DECLARE @functionName nvarchar(100)


BEGIN--	FROM
	DECLARE @_from nvarchar(max)
	SET @functionName = 'dsql_Get' + @RecordsName + '_From'
	SET @_from = REPLACE(@drop, '%', @functionName)
	EXEC (@_from)

	SET @_from = 'CREATE FUNCTION ' + @functionName + @NEW_LINE
		+ '()' + @NEW_LINE
		+ 'RETURNS nvarchar(max)' + @NEW_LINE
		+ 'AS' + @NEW_LINE
		+ 'BEGIN' + @NEW_LINE
		+ ' RETURN' + @NEW_LINE
		+ ' '' FROM ' + REPLACE(@defFrom, '''', '''''' )+ ' ''' + @NEW_LINE
		+ 'END' + @NEW_LINE

	SELECT @_from AS [FROM]
	EXEC (@_from)
END

BEGIN-- ORDER BY
	DECLARE @_orderBy nvarchar(max)
	SET @functionName = 'dsql_Get' + @RecordsName + '_OrderBy'
	SET @_orderBy = REPLACE(@drop, '%', @functionName)
	EXEC (@_orderBy)

	SET @_orderBy = 'CREATE FUNCTION ' + @functionName + @NEW_LINE
		+ '(' + @NEW_LINE
		+ ' @SortOrder nvarchar(100)' + @NEW_LINE
		+ ')' + @NEW_LINE
		+ 'RETURNS nvarchar(max)' + @NEW_LINE
		+ 'AS' + @NEW_LINE
		+ 'BEGIN' + @NEW_LINE
		+ ' DECLARE @result nvarchar(max)' + @NEW_LINE
		+ ' SET @result = '' ORDER BY '' +' + @NEW_LINE
		+ '  CASE @SortOrder' + @NEW_LINE
	DECLARE @sortPrefix TABLE(Prefix nvarchar(5), Sufix nvarchar(5))
	INSERT @sortPrefix (Prefix, Sufix) VALUES ('+', 'ASC'), ('-', 'DESC')

	SELECT @_orderBy = @_orderBy
		+ '   WHEN ''' + s.Prefix + c.Alias + ''' THEN ''' + c.Name + ' ' + s.Sufix + '''' + @NEW_LINE
	FROM @columns c
		,@sortPrefix s
	ORDER BY c.Nr, s.Prefix DESC

	SELECT @_orderBy = @_orderBy
		+ '   ELSE ''' + Name + '''' + @NEW_LINE
	FROM @columns WHERE Nr = 1

	SET @_orderBy = @_orderBy 
		+ '  END' + @NEW_LINE
		+ ' RETURN @result + '' ''' + @NEW_LINE
		+ 'END' + @NEW_LINE
	SELECT  @_orderBy as [ORDER BY]

	EXEC (@_orderBy)
END

BEGIN--	SELECT
	DECLARE @_select nvarchar(max)
	SET @functionName = 'dsql_Get' + @RecordsName + '_Select'
	SET @_select = REPLACE(@drop, '%', @functionName)
	EXEC (@_select)

	SET @defSelect = SUBSTRING(LTRIM(@defSelect), 2, 999999)
	--print @defSelect return

	SET @_select = 'CREATE FUNCTION ' + @functionName + @NEW_LINE
		+ '()' + @NEW_LINE
		+ 'RETURNS nvarchar(max)' + @NEW_LINE
		+ 'AS' + @NEW_LINE
		+ 'BEGIN' + @NEW_LINE
		+ ' RETURN' + @NEW_LINE
		+ '''' + @defSelect + '''' + @NEW_LINE
		+ 'END' + @NEW_LINE
	SELECT @_select as [SELECT]

	EXEC (@_select)
END

BEGIN-- WHERE
	DECLARE @_where nvarchar(max)
	SET @functionName = 'dsql_Get' + @RecordsName + '_Where'
	SET @_where = REPLACE(@drop, '%', @functionName)
	EXEC (@_where)

	SET @_where = 'CREATE FUNCTION ' + @functionName + @NEW_LINE
		+ '(' + @NEW_LINE

	DECLARE @params nvarchar(max) = ''
	SET @params = @params + '@TreeUnitIds nvarchar(max),' + @NEW_LINE

	DECLARE @whereSufix TABLE(Sufix nvarchar(5))
	INSERT @whereSufix VALUES(''), ('From'), ('To')


	SELECT @params = @params + '@' + c.Alias + p.Sufix + ' ' + c.DataType + 
		CASE WHEN CHARINDEX('char', c.DataType) > 0 AND CHARINDEX('max', c.DataType) = 0  THEN '(' + CONVERT(nvarchar(10), @nvarcharParameterLength) +')' ELSE '' END + ' = null' + @NEW_LINE + ','
	FROM @columns c
	JOIN @whereSufix p ON (CHARINDEX('char', c.DataType) > 0 AND p.Sufix = '') OR (CHARINDEX('char', c.DataType) = 0 AND (p.Sufix = 'From' OR p.Sufix = 'To'))
	ORDER BY c.Nr, p.Sufix
	SET @params = SUBSTRING(@params, 1, LEN(@params) - 1)

	--print @params return
	

	SET @_where = @_where
		+ @params
		+ ')' + @NEW_LINE
		+ 'RETURNS nvarchar(max)' + @NEW_LINE
		+ 'AS' + @NEW_LINE
		+ 'BEGIN' + @NEW_LINE
	/*
	IF @UnitsParamType IS NOT NULL AND @UnitsParamType <> 'nvarchar(max)' SET @_where = @_where
		+ ' --DECLARE @units nvarchar(max)' + @NEW_LINE
		+ ' --SELECT @units = @units + CONVERT(nvarchar(20), Id) + '','' FROM dbo.GetChildUnits(@UnitId)' + @NEW_LINE
		+ ' --SET @units = LEFT(@units, LEN(@units) - 1)' + @NEW_LINE
	*/
	SET @_where = @_where
		+ ' DECLARE @useMatchFunction bit = ' + CASE WHEN @useIsMatchedForNvarchar = 1 THEN '1' ELSE '0' END + @NEW_LINE

	SET @_where = @_where
		+ ' DECLARE @r nvarchar(max) = '' WHERE '''
	IF COALESCE(@defWhere, '') <> '' SET @_where = @_where + ' + ''' + @defWhere + ' AND ''' + @NEW_LINE

	SET @_where = @_where + @NEW_LINE
		+ '-- IF COALESCE(@TreeUnitIds, '''') <> ''''' + @NEW_LINE
		+ '--   SET @r = @r + ''UnitId IN('''''' + @TreeUnitIds + '''''') AND ''' + @NEW_LINE + @NEW_LINE

	DECLARE @name nvarchar(100)
		,@alias nvarchar(100)
		,@dataType nvarchar(100)
		,@rowNr int = 1
	
	SELECT @name = Name, @alias = Alias, @dataType = DataType FROM @columns WHERE Nr = @rowNr
	WHILE @name IS NOT NULL
	BEGIN
		IF CHARINDEX('(max)', @dataType) > 0
			SET @_where = @_where
				+ ' IF ' + '@' + @alias + ' IS NOT NULL AND @' + @alias + ' <> ''''' + @NEW_LINE
				+ '   SET @r = @r + ''' + @name + ' IN('' + @' + @alias + ' + '') AND ''' + @NEW_LINE
		ELSE IF CHARINDEX('char', @dataType) > 0 AND CHARINDEX('max', @dataType) = 0
		BEGIN
			SET @_where = @_where
				+ ' IF ' + '@' + @alias + ' IS NOT NULL' + @NEW_LINE
				+ '  IF ' + '@' + @alias + ' = ''?''' + @NEW_LINE
				+ '   SET @r = @r + ''COALESCE(' + @name + ', '''''''') = '''''''' AND ''' + @NEW_LINE
				+ '  ELSE' + @NEW_LINE
			SET @_where = @_where
				+ '  BEGIN' + @NEW_LINE
				+ '    IF @useMatchFunction = 1 ' + @NEW_LINE
				+ '      SET @r = @r + ''dbo.IsStringMatched(' + @name +', '''''' + @' + @alias + ' + '''''') > 0 AND ''' + @NEW_LINE
				+ '    ELSE' + @NEW_LINE
				+ '     SET @r = @r + ''' + @name + ' LIKE '''''' + @' + @alias + ' + '''''' AND ''' + @NEW_LINE
				+ '  END' + @NEW_LINE
				/*
				IF @useIsMatchedForNvarchar = 1 SET @_where = @_where + '--'
				SET @_where = @_where + '   SET @r = @r + ''' + @name + ' LIKE '''''' + @' + @alias + ' + '''''' AND ''' + @NEW_LINE
				IF @useIsMatchedForNvarchar = 0 SET @_where = @_where + '--'
				SET @_where = @_where + '   SET @r = @r + ''dbo.IsStringMatched(' + @name +', '''''' + @' + @alias + ' + '''''') > 0 AND ''' + @NEW_LINE
				*/
		END
		ELSE IF (CHARINDEX('date', @dataType) > 0) OR (CHARINDEX('time', @dataType) > 0)
			SET @_where = @_where
				+ ' IF @' + @alias + 'From IS NOT NULL' + @NEW_LINE
				+ '  SET @r = @r + ''' + @name + ' >='''''' + CONVERT(nvarchar(30), @' + @alias + 'From, 121) + '''''' AND ''' + @NEW_LINE
				+ ' IF @' + @alias + 'To IS NOT NULL' + @NEW_LINE
				+ '  SET @r = @r + ''' + @name + ' <='''''' + CONVERT(nvarchar(30), @' + @alias + 'To, 121) + '''''' AND ''' + @NEW_LINE
		ELSE
			SET @_where = @_where
				+ ' IF @' + @alias + 'From IS NOT NULL AND @' + @alias + 'To IS NOT NULL' + @NEW_LINE
				+ '  SET @r = @r + ''(' + @name + ' BETWEEN '' + CONVERT(nvarchar(20), @' + @alias + 'From) + '' AND '' + CONVERT(nvarchar(20), @' + @alias + 'To) + '') AND ''' + @NEW_LINE
				+ ' ELSE IF @' + @alias + 'From IS NOT NULL AND @' + @alias + 'To IS NULL' + @NEW_LINE
				+ '  SET @r = @r + ''' + @name + ' = '' + CONVERT(nvarchar(20), @' + @alias + 'From) + '' AND ''' + @NEW_LINE
				+ ' ELSE IF @' + @alias + 'From IS NULL AND @' + @alias + 'To IS NOT NULL' + @NEW_LINE
				+ '  SET @r = @r + ''' + @name + ' IS NULL AND ''' + @NEW_LINE

		SET @_where = @_where + @NEW_LINE
		SELECT @name = null, @rowNr = @rowNr + 1
		SELECT @name = Name, @alias = Alias, @dataType = DataType FROM @columns WHERE Nr = @rowNr
	END
	SET @_where = @_where
		+ ' SET @r = @r + '' 1=1''' + @NEW_LINE
		+ ' RETURN @r' +  @NEW_LINE
		+ 'END' + @NEW_LINE
	
	SELECT @_where as [Where]
	EXEC (@_where)
END

	SET @drop = 'IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(''%'') AND Type IN (''P'', ''PC'')) DROP PROCEDURE %'

BEGIN --GetFiltered
	DECLARE @_GetFiltered nvarchar(max)
	SET @functionName = @ProcPrefix + '_Get' + @RecordsName + 'Filtered'
	SET @_GetFiltered = REPLACE(@drop, '%', @functionName)
	EXEC (@_GetFiltered)
	
	SET @_GetFiltered = 'CREATE PROCEDURE ' + @functionName + @NEW_LINE
		+ '@PageNum int' + @NEW_LINE
		+ ',@PageSize int' + @NEW_LINE + @NEW_LINE + ','


	SET @_GetFiltered = @_GetFiltered + @params 
		+ ',@SortOrder nvarchar(100) = null' + @NEW_LINE
		+ 'AS' + @NEW_LINE
		+ 'BEGIN' + @NEW_LINE
	
	IF @SingleUnitIdForProc = 1 SET @_GetFiltered = REPLACE(@_GetFiltered, '@TreeUnitIds nvarchar(max)', '@UnitId int')

	IF @SingleUnitIdForProc = 1
		SET @_GetFiltered = @_GetFiltered+ ' DECLARE @TreeUnitIds nvarchar(max) = ''''' + @NEW_LINE
			+ ' IF @UnitId IS NOT NULL' + @NEW_LINE
			+ ' BEGIN' + @NEW_LINE
			+ '   SELECT @TreeUnitIds = @TreeUnitIds + CONVERT(nvarchar(10), Id) + '','' FROM dbo.GetChildUnits(@UnitId)' + @NEW_LINE
			+ '   SET @TreeUnitIds = LEFT(@TreeUnitIds, LEN(@TreeUnitIds) - 1)' + @NEW_LINE
			+ ' END' + @NEW_LINE

	SET @_GetFiltered = @_GetFiltered + @NEW_LINE
		+ ' DECLARE @cmd nvarchar(max)' + @NEW_LINE
		+ ' SET @cmd = ''WITH orn AS ( SELECT ROW_NUMBER() OVER (''' + @NEW_LINE
		+ '		+ dbo.dsql_Get' + @RecordsName + '_OrderBY(@SortOrder) + '') AS RowNum, ''' + @NEW_LINE
		+ '		+ dbo.dsql_Get' + @RecordsName + '_Select()' + @NEW_LINE
		+ '		+ dbo.dsql_Get' + @RecordsName + '_From()' + @NEW_LINE
		+ '		+ dbo.dsql_Get' + @RecordsName + '_Where(@TreeUnitIds,' + @NEW_LINE

	DECLARE @innerParams nvarchar(max) = '			'
	DECLARE @maxInRow int = 6
		,@inRow int = 1
	SELECT @innerParams =@innerParams + '@' + c.Alias + p.Sufix 
		+  CASE WHEN @inRow = @maxInRow THEN @NEW_LINE + '			' ELSE '' END
		+ ', ', @inRow = CASE WHEN @inRow = @maxInRow THEN 1 ELSE @inRow + 1 END
	FROM @columns c
	JOIN @whereSufix p ON (CHARINDEX('char', c.DataType) > 0 AND p.Sufix = '') OR (CHARINDEX('char', c.DataType) = 0 AND (p.Sufix = 'From' OR p.Sufix = 'To'))
	ORDER BY c.Nr, p.Sufix
	SET @innerParams = SUBSTRING(@innerParams, 1, LEN(@innerParams) - 1)

	SET @_GetFiltered = @_GetFiltered + @innerParams + ')' + @NEW_LINE
		+ ' + '')' + @NEW_LINE
		+ '		SELECT *' + @NEW_LINE
		+ '		FROM orn' + @NEW_LINE
		+ '		WHERE RowNum BETWEEN '' + CONVERT(nvarchar(20), @PageNum * @PageSize +1) + '' AND ''' + @NEW_LINE
		+ '		+ CONVERT(nvarchar(20), (@PageNum + 1) * @PageSize) + '' ORDER BY rowNum''' + @NEW_LINE + @NEW_LINE
		+ CASE WHEN @printStatementAsOutput = 1 THEN '--' ELSE '' END
		+ ' EXEC(@cmd)' + @NEW_LINE
		+ CASE WHEN @printStatementAsOutput = 0 THEN '--' ELSE '' END
		+ ' print @cmd' + @NEW_LINE
		+ 'END' + @NEW_LINE
	SELECT @_GetFiltered as GetFiltered
	EXEC(@_GetFiltered)
END

BEGIN --GetTotalCount
	DECLARE @_GetTotalCount nvarchar(max)
	SET @functionName = @ProcPrefix + '_Get' + @RecordsName + 'FilteredTotalCount'
	SET @_GetTotalCount = REPLACE(@drop, '%', @functionName)
	EXEC (@_GetTotalCount)
	
	SET @_GetTotalCount = 'CREATE PROCEDURE ' + @functionName + @NEW_LINE

	SET @_GetTotalCount = @_GetTotalCount + @params 
		+ 'AS' + @NEW_LINE
		+ 'BEGIN' + @NEW_LINE
	IF @SingleUnitIdForProc = 1 SET @_GetTotalCount = REPLACE(@_GetTotalCount, '@TreeUnitIds nvarchar(max)', '@UnitId int')

	IF @SingleUnitIdForProc = 1
		SET @_GetTotalCount = @_GetTotalCount
			+ ' DECLARE @TreeUnitIds nvarchar(max) = ''''' + @NEW_LINE
			+ ' IF @UnitId IS NOT NULL' + @NEW_LINE
			+ ' BEGIN' + @NEW_LINE
			+ '   SELECT @TreeUnitIds = @TreeUnitIds + CONVERT(nvarchar(10), Id) + '','' FROM dbo.GetChildUnits(@UnitId)' + @NEW_LINE
			+ '   SET @TreeUnitIds = LEFT(@TreeUnitIds, LEN(@TreeUnitIds) - 1)' + @NEW_LINE
			+ ' END' + @NEW_LINE

	SET @_GetTotalCount = @_GetTotalCount + @NEW_LINE
		+ ' DECLARE @cmd nvarchar(max)' + @NEW_LINE
		+ ' SET @cmd = ''SELECT COUNT(1) ''' + @NEW_LINE
		+ '		+ dbo.dsql_Get' + @RecordsName + '_From()' + @NEW_LINE
		+ '		+ dbo.dsql_Get' + @RecordsName + '_Where(@TreeUnitIds,' + @NEW_LINE

	SET @_GetTotalCount = @_GetTotalCount + @innerParams + ')' + @NEW_LINE + @NEW_LINE
		+ CASE WHEN @printStatementAsOutput = 1 THEN '--' ELSE '' END
		+ ' EXEC(@cmd)' + @NEW_LINE
		+ CASE WHEN @printStatementAsOutput = 0 THEN '--' ELSE '' END
		+ ' print @cmd' + @NEW_LINE
		+ 'END' + @NEW_LINE
	SELECT @_GetTotalCount as GetTotalCount
	EXEC (@_GetTotalCount)
END
END