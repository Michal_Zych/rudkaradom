﻿

CREATE PROCEDURE [dbo].[ev_TransmitterDelete]
	@TransmitterId int
	,@OperatorId int
	,@Reason nvarchar(max) = null
	,@OperationDateUtc dateTime = null
AS
BEGIN
	SET NOCOUNT ON;

	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
		BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'TransmitterId=' + COALESCE(CONVERT(nvarchar(100), @TransmitterId), 'NULL') 
		EXEC Log_Action @@PROCID, @info
	END
	/****************************************************/

	DECLARE
		@UPDATE_ENDING_EVENT_ID bit = 1
		,@NEW_LINE nvarchar(5) = CHAR(13) + CHAR(10)
	DECLARE 
		@errorString nvarchar(1024) = ''
		,@unitId int
		,@unitLocation nvarchar(max)
		,@currentDate dateTime 
		,@eventId int

	SELECT @unitId = UnitID, @unitLocation = dbo.GetUnitLocation(UnitId, 1, default, default)
	FROM dbo.dev_UnitTransmitter 
	WHERE TransmitterId = @TransmitterId
	IF @unitId IS NOT NULL SET @errorString = 'Nie można usunąć transmitera.' + @NEW_LINE + 'Transmiter jest przypisany do ' + @unitLocation

	IF @errorString <> ''
	BEGIN
		SELECT CONVERT(int, null) AS Result, 1 AS ErrorCode, @errorString AS ErrorMessage
		RETURN
	END
	
	SELECT @currentDate = COALESCE(@OperationDateUtc, dbo.GetCurrentUtcDate())

	BEGIN TRY
		BEGIN TRANSACTION
			UPDATE dev_Transmitters SET IsActive = 0 WHERE Id = @TransmitterId

			INSERT dbo.ev_Events(DateUtc, Type, Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason) 
			SELECT @currentDate, eventType.[Delete], eventSeverity.Event, null, @transmitterId, objectType.Transmitter, null, @currentDate, @currentDate, @OperatorId, null, @Reason
			FROM enum.EventType eventType
			JOIN enum.EventSeverity eventSeverity ON 1 = 1
			JOIN enum.ObjectType objectType ON 1 = 1
			SELECT @eventId = SCOPE_IDENTITY()
			IF @UPDATE_ENDING_EVENT_ID = 1
			BEGIN
				UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
			END


		COMMIT TRANSACTION
		SELECT @transmitterId AS Result, NULL as ErrorCode, NULL as ErrorMessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		SELECT CONVERT(int, null) AS Result, ERROR_NUMBER() AS ErrorCode, ERROR_MESSAGE() AS ErrorMessage
	END CATCH



END