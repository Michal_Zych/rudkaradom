﻿CREATE PROCEDURE fda_GetExternalRecordsInFiltered
@PageNum int
,@PageSize int

,@TreeUnitIds nvarchar(max),
@Type varchar(200) = null
,@IdFrom int = null
,@IdTo int = null
,@RequestIdFrom int = null
,@RequestIdTo int = null
,@StoreDateUtcFrom datetime = null
,@StoreDateUtcTo datetime = null
,@RecordType nvarchar(200) = null
,@RecordId nvarchar(200) = null
,@Record nvarchar(200) = null
,@ProcessedFrom int = null
,@ProcessedTo int = null
,@TryFrom int = null
,@TryTo int = null
,@ProcessedDateUtcFrom datetime = null
,@ProcessedDateUtcTo datetime = null
,@Error nvarchar(200) = null
,@SortOrder nvarchar(100) = null
AS
BEGIN

 DECLARE @cmd nvarchar(max)
 SET @cmd = 'WITH orn AS ( SELECT ROW_NUMBER() OVER ('
		+ dbo.dsql_GetExternalRecordsIn_OrderBY(@SortOrder) + ') AS RowNum, '
		+ dbo.dsql_GetExternalRecordsIn_Select()
		+ dbo.dsql_GetExternalRecordsIn_From()
		+ dbo.dsql_GetExternalRecordsIn_Where(@TreeUnitIds,
			@Type, @IdFrom, @IdTo, @RequestIdFrom, @RequestIdTo, @StoreDateUtcFrom
			, @StoreDateUtcTo, @RecordType, @RecordId, @Record, @ProcessedFrom, @ProcessedTo
			, @TryFrom, @TryTo, @ProcessedDateUtcFrom, @ProcessedDateUtcTo, @Error)
 + ')
		SELECT *
		FROM orn
		WHERE RowNum BETWEEN ' + CONVERT(nvarchar(20), @PageNum * @PageSize +1) + ' AND '
		+ CONVERT(nvarchar(20), (@PageNum + 1) * @PageSize) + ' ORDER BY rowNum'

 EXEC(@cmd)
-- print @cmd
END