﻿

CREATE  PROCEDURE [dbo].[rep_AlarmSubReport]
	@UnitId int,
	@DateFrom datetime,
	@DateTo datetime,
	@ShowAllAlarms bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @precision int = 1

	DECLARE @NO_COMMUNICATION tinyint = 3
			,@VALUE_TO_LOW tinyint = 1
			,@VALUE_TO_HIGH tinyint = 2		

	DECLARE @result TABLE(
		EventType tinyint
		,DateFrom datetime
		,DateEnd datetime
		,Name nvarchar(256)
		,Value float
		,LoRange float
		,UpRange float
		,UserStatus nvarchar(256)
		,DateStatus datetime
		,CommentStatus nvarchar(1024)
		,MU nvarchar(10)
		,IsBitSensor bit
		,LoStateDesc nvarchar(10)
		,HiStateDesc nvarchar(10)
		)
	INSERT @result
	SELECT  
	CASE WHEN @ShowAllAlarms = 1 THEN a.EventType
		ELSE CASE WHEN a.EventType > @NO_COMMUNICATION THEN @NO_COMMUNICATION ELSE a.EventType END
	END AS EventType,
	a.Timestamp as DateFrom,
	a.DateEnd,
	u.Name,
	ROUND(m.Value * s.Scale, @precision) AS Value,
	ROUND(a.LoRange * s.Scale, @precision) AS LoRange,
	ROUND(a.UpRange * s.Scale, @precision) AS UpRange,
	COALESCE(usa.LastName + ' ' + usa.Name,'') as UserStatus,
    a.DateStatus,
    a.CommentStatus,
	s.MU,
	s.IsBitSensor,
	s.LoStateDesc,
	s.HiStateDesc
	FROM dbo.Alarms a WITH(NOLOCK)
	LEFT JOIN dbo.Measurements m WITH(NOLOCK) ON a.SensorID = m.SensorID AND a.Timestamp = m.MeasurementTime
	LEFT JOIN dbo.Users usa WITH(NOLOCK) ON usa.Id = a.UserStatus
	JOIN dbo.Sensors s WITH(NOLOCK) ON a.SensorID = s.Id
	JOIN dbo.Units u WITH(NOLOCK) ON u.Id = s.UnitId
	WHERE u.ParentUnitID = @unitid AND a.Timestamp BETWEEN @DateFrom AND @DateTo 
		AND ( @ShowAllAlarms = 1 OR (DATEDIFF(SECOND, a.Timestamp, Coalesce(a.DateEnd, dbo.GetCurrentDate()))
				  > (CASE 
							WHEN a.EventType < @NO_COMMUNICATION THEN s.GuiRangeDelay
							ELSE s.GuiCommDelay
						END))
	   )
   --ORDER BY a.Timestamp 

   SELECT
		CASE WHEN IsBitSensor = 1 AND (EventType = @VALUE_TO_LOW OR EventType = @VALUE_TO_HIGH) THEN -1
			ELSE EventType END AS EventType
		,DateFrom
		,DateEnd
		,Name
		,CASE WHEN IsBitSensor = 1 THEN
					CASE WHEN Value = 0 THEN LoStateDesc ELSE HiStateDesc END		
		 ELSE REPLACE(CONVERT(nvarchar(10), Value), '.', ',') END AS Value
		,CASE WHEN IsBitSensor = 1 THEN LoStateDesc ELSE REPLACE(CONVERT(nvarchar(10), LoRange), '.', ',') END AS LoRange
		,CASE WHEN IsBitSensor = 1 THEN HiStateDesc ELSE REPLACE(CONVERT(nvarchar(10), UpRange), '.', ',') END AS UpRange
		,UserStatus
		,DateStatus
		,CommentStatus
		,CASE WHEN IsBitSensor = 1 THEN '' ELSE MU END AS MU
   FROM @result
   ORDER BY DateFrom
END

