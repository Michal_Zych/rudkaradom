﻿
CREATE PROCEDURE dbo.Object_GetLocation
	@ObjectId dbo.OBJECT_ID				
	,@ObjectType dbo.OBJECT_TYPE		
AS
BEGIN
	DECLARE
		@OBJECT_TYPE_BAND dbo.OBJECT_TYPE		= 2
		,@OBJECT_TYPE_PATIENT dbo.OBJECT_TYPE	= 4

	DECLARE 
		@currentUnitID int
		,@inCurrentUnitUtc dateTime
		,@previousUnitId int
		,@inPreviousUnitUtc dateTime
	
	SELECT @currentUnitID = CurrentUnitId
		,@inCurrentUnitUtc = InCurrentUnitUtc
		,@previousUnitId = PreviousUnitId
		,@inPreviousUnitUtc = InPreviousUnitUtc
	FROM dbo.ev_BandStatus
	WHERE ObjectId = @ObjectId AND ObjectType = @ObjectType

	IF COALESCE(@currentUnitId, @previousUnitId) IS NULL
		SELECT TOP 1 
			@previousUnitId = UnitId
			,@inPreviousUnitUtc = DateUtc 
		FROM dbo.ev_Events e
		JOIN enum.EventType eventType ON e.Type = eventType.ZoneIn OR e.Type = eventType.NoGoZoneIn
		WHERE ObjectId = @ObjectId AND ObjectType = @ObjectType
		ORDER BY DateUtc DESC

	DECLARE @values TABLE (CurrentUnitId int, InCurrentUtc dateTime, PreviousUnitId int, InPreviousUtc dateTime)
	INSERT @values(CurrentUnitId, InCurrentUtc, PreviousUnitId, InPreviousUtc) VALUES
		(@currentUnitID, @inCurrentUnitUtc, @previousUnitId, @inPreviousUnitUtc)

	SELECT 
		CurrentUnitId
		,u.Name AS CurrentUnitName
		,dbo.GetUnitLocation(u.ID, 0, 0, default) AS CurrentUnitLocation
		,InCurrentUtc 
		,PreviousUnitId
		,p.Name AS PreviousUnitName
		,dbo.GetUnitLocation(p.Id, 0, 0, default) AS PreviousUnitLocation
		,InPreviousUtc
	FROM @values v
	LEFT JOIN dbo.Units u ON u.ID = v.CurrentUnitId
	LEFT JOIN dbo.Units p ON p.ID = v.PreviousUnitId
END