﻿-- =============================================
-- Author:		kt
-- Create date: 2017-11-02
-- Description:	stores sensor data
-- =============================================
CREATE PROCEDURE dbo.dev_StoreData
	@sensorId smallint,
	@measurementTime datetime,
	@value smallint
AS
BEGIN
	DECLARE @storeDelay int;
	SET @storeDelay = DATEDIFF(ss, @measurementTime, dbo.GetCurrentDate());


	SET NOCOUNT ON;
	
	INSERT INTO dbo.Measurements(SensorID, MeasurementTime, Value, StoreDelay)
		VALUES (@sensorId, @measurementTime, @value, @storeDelay);

END