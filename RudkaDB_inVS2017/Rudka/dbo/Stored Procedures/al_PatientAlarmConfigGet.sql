﻿
CREATE PROCEDURE [dbo].[al_PatientAlarmConfigGet]
(
	@PatientId int
)
AS  
BEGIN
	DECLARE @unitId int
	IF (NOT EXISTS (SELECT PatientId FROM dbo.al_ConfigPatients WHERE PatientId = @PatientId)) 
		SELECT @unitId = COALESCE(RoomId, WardId) FROM dbo.Patients WHERE Id = @PatientId
	IF @unitId IS NOT NULL
		SELECT -@unitId, c.IsSOScalling, c.IsACCactive, c.DMoveWrActive, c.DMoveWrMins, c.DMoveAlActive, c.DMoveAlMins, c.DNoMoveWrActive 
			,c.DNoMoveWrMins, c.DNoMoveAlActive, c.DNoMoveAlMins, c.NMoveWrActive, c.NMoveWrMins, c.NMoveAlActive, c.NMoveAlMins 
			,c.NNoMoveWrActive, c.NNoMoveWrMins, c.NNoMoveAlActive, c.NNoMoveAlMins, c.OutOfZoneWrActive, c.OutOfZoneWrMins 
			,c.OutOfZoneAlActive, c.OutOfZoneAlMins, c.InNoGoZoneMins, c.NoConnectWrActive, c.NoConnectWrMins, c.NoConnectAlActive, c.NoConnectAlMins
			,dbo.GetCallingGroupString(objectType.Unit, c.UnitId) AS CallingGroup
			,dbo.GetNoGoZonesString(objectType.Unit, c.UnitId) AS NoGoZones
		FROM dbo.al_ConfigUnits c
		JOIN enum.ObjectType objectType ON 1 = 1 
		WHERE c.UnitId = @unitId
	ELSE
		SELECT p.*
			,dbo.GetCallingGroupString(objectType.Patient, p.PatientId) AS CallingGroup
			,dbo.GetNoGoZonesString(objectType.Patient, p.PatientId) AS NoGoZones
		FROM dbo.al_ConfigPatients p
		JOIN enum.ObjectType objectType ON 1 = 1 
		WHERE p.PatientId = @PatientId
END