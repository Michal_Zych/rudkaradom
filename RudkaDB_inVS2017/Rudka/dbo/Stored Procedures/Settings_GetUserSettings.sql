﻿

CREATE PROCEDURE [dbo].[Settings_GetUserSettings]
	@clientId int,
	@userId int
AS
BEGIN
	SELECT Name, 
		COALESCE(CONVERT (nvarchar(1024), [dbo].[settings_GetInt](ClientId, Name))
			, [dbo].[settings_GetString](ClientId, Name)) AS Value
	FROM dbo.Settings
	WHERE ClientId = @ClientId

END
