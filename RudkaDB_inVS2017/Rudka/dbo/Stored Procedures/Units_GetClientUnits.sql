﻿
-- =============================================
-- Author:		rs
-- Create date: 24,08,2013
-- Description: Zwraca aktywne unity danego klienta
-- =============================================
CREATE PROCEDURE [dbo].[Units_GetClientUnits] @clientId INT
AS
BEGIN
	SET NOCOUNT ON;

/*	with Tree(ID, Name, UnitTypeID, ParentUnitID)
	as
	(
		select e.ID, e.Name, UnitTypeID, ParentUnitID
		from Units e join Clients c on e.ID = c.RootUnitID
		where c.ID = @clientId
		union all
		select e.ID, e.Name, e.UnitTypeID, e.ParentUnitID
		from Units e inner join Tree eh on e.ParentUnitID = eh.ID
		where e.Active = 1
	)
	select ID, Name, UnitTypeID, ParentUnitID
	from Tree 
*/

	SELECT u.ID, u.Name, u.UnitTypeID, u.ParentUnitID, u.ClientId, u.UnitTypeIDv2, u.PictureId, u.Description, dbo.unt_IsMonitored(u.ID) AS IsMonitored
	FROM dbo.Units u WITH(NOLOCK)
	WHERE u.ClientID = @clientId
		AND u.Active = 1

END


