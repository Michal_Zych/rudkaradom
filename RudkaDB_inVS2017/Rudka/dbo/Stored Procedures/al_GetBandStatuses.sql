﻿
CREATE PROCEDURE al_GetBandStatuses
(
	@SynchroToken bigint = NULL
)
AS
BEGIN	
	SELECT 
	   BandId
      ,ObjectType
      ,ObjectTypeName
      ,ObjectId
      ,ObjectDisplayName
      ,AssignDateUtc
      ,ObjectUnitId
      ,ObjectUnitName
      ,ObjectUnitLocation
      ,CurrentUnitId
      ,CurrentUnitName
      ,CurrentUnitLocation
      ,InCUrrentUnitUtc
      ,PreviousUnitId
      ,PreviousUnitName
      ,PreviousUnitLocation
      ,InPreviousUnitUtc
      ,TopAlarmId
      ,TopAlarmType
      ,TopAlarmTypeName
      ,TopAlarmSeverity
      ,TopAlarmSevertityName
      ,TopAlarmDateUtc
      ,TopEventDateUtc
      ,Severity1Count
      ,Severtity2Count
      ,AlarmNoGoZoneOngoing
      ,AlarmDisconnectedOngoing
      ,AlarmZoneOutOngoing
      ,AlarmNoMovingOngoing
      ,AlarmMovingOngoing
      ,AlarmLowBatteryOngoing
      ,SynchroToken
	FROM dbo.BandsStatuses
	WHERE SynchroToken > COALESCE(@SynchroToken, -1)
END