﻿
CREATE PROCEDURE [dbo].[crt_GetSensorChartData__old]
	@sensorID smallint,
	@number smallint,
	@dateFrom datetime,
	@dateTo datetime,
	@mode int,
	@showAllAlarms bit = 0,
	@rowCount int = 1000
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE 
		@UnitTypeIDv2 int
		,@GuiRangeDelay smallint
		,@GuiCommDelay smallint
		,@sensorName nvarchar(256)
		,@scale real

BEGIN--POBRANIE KONFIGURACJI
	SELECT @sensorName = Name, @scale = Scale, @UnitTypeIDv2 = UnitTypeIDv2, @GuiRangeDelay= GuiRangeDelay, @GuiCommDelay = GuiCommDelay
	FROM dbo.Sensors s
	JOIN dbo.Units u ON s.UnitId = u.ID
	WHERE s.ID = @sensorID
END	

BEGIN--WYNIKI POMIARÓW
	DECLARE @Data TABLE
    (
		Timestamp datetime,
		Value MEASURE_DT
    );
	INSERT @Data
	EXEC crt_GeneralizedSensorData @sensorID, @dateFrom, @dateTo, @mode/*2*/, @rowCount

	UPDATE @Data SET Value = Value * @scale
END    
	
BEGIN--ZAKRESY ALARMOWANIA
	DECLARE @AlarmRanges TABLE
    (
		[TimeStamp] datetime,
		AL MEASURE_DT,
		AH MEASURE_DT,
		Domkniecie bit
    );
	
	--początek
	INSERT @AlarmRanges
	SELECT TOP(1) @dateFrom, LoRange, UpRange, 0
	FROM dbo.MeasurementRanges
	WHERE SensorID = @sensorID
		AND DateStart <= @dateFrom
	ORDER BY DateStart DESC

	--środek
	INSERT @AlarmRanges
	SELECT DateStart, LoRange, UpRange, 0
	FROM dbo.MeasurementRanges
	WHERE SensorID = @sensorID
		AND DateStart BETWEEN @dateFrom AND @dateTo
	ORDER BY DateStart
	--środek domknięcia
	INSERT @AlarmRanges
	SELECT (SELECT TOP(1) TimeStamp FROM @alarmRanges WHERE TimeStamp > a.TimeStamp ORDER BY TimeStamp), AL, AH, 1
	FROM @alarmRanges a
	where (SELECT TOP(1) TimeStamp FROM @alarmRanges WHERE TimeStamp > a.TimeStamp ORDER BY TimeStamp) is not null


	--koniec
	DECLARE @maxDate datetime
	IF dbo.GetCurrentDate() < @dateTo SET @maxDate = dbo.GetCurrentDate()
	ELSE SET @maxDate = @dateTo

	INSERT @AlarmRanges
	SELECT TOP(1) @maxDate, LoRange, UpRange, 0
	FROM dbo.MeasurementRanges
	WHERE SensorID = @sensorID
		AND DateStart <= @dateTo
	ORDER BY DateStart DESC

	UPDATE @AlarmRanges SET AL = AL * @scale, AH = AH * @scale
END

BEGIN--ALARMY
	DECLARE --stałe
		 @VALUE_TO_LOW tinyint = 1
		,@VALUE_TO_HIGH tinyint = 2
		,@NO_COMMUNICATION tinyint = 3
		,@NO_SENSOR tinyint = 3
		,@NO_DEVICE tinyint = 4
		,@NO_HUB tinyint = 5
		
	DECLARE @Alarms TABLE
    (
		Id int,
		DateStart datetime,
		DateEnd datetime,
		EventType int
    );
    
    INSERT INTO @Alarms(Id,  DateStart, DateEnd, EventType)
	SELECT				id, [TimeStamp], DateEnd, EventType
	FROM dbo.Alarms
	WHERE SensorID = @sensorID
		AND (
				[TimeStamp] BETWEEN @dateFrom AND @dateTo
				OR ([TimeStamp] < @dateFrom and (ISNULL(DateEnd, @dateTo) > @dateFrom))
			)
		
	--jeśli nie pokazujemy wszystkich alarmów to usuń krótkie
	IF @showAllAlarms = 0
	DELETE FROM @Alarms
	WHERE ( DATEDIFF(SECOND, DateStart, COALESCE(DateEnd, dbo.GetCurrentDate())) 
				  <= (CASE 
						WHEN EventType IN (@VALUE_TO_LOW, @VALUE_TO_HIGH) THEN @GuiRangeDelay
						ELSE @GuiCommDelay
				     END))	
	
END

BEGIN-- ZWRÓCENIE WYNIKÓW
	
	-- Wyniki pomiarów
	IF @showAllAlarms = 0
	DELETE FROM @Data
	WHERE Value IS NULL 
		AND [Timestamp] NOT IN (SELECT DateStart FROM @Alarms)
		
	SELECT * FROM @Data
	
	
	-- Zakresy alarmowania
	SELECT [TimeStamp], AL, AH
	FROM @AlarmRanges
	ORDER BY [TimeStamp] ASC, Domkniecie DESC
	
	-- Alarmy
	SELECT DateStart, DateEnd 
	FROM @Alarms 
	ORDER BY ID
		
	-- Konfiguracja
	SELECT 
			LegendDescription, LegendShortcut, LegendRangeDescription,
			LineColor, LineWidth, LineStyle,
			RangeLineWidth, RangeLineStyle,
			AlarmColor, AlarmCommColor
	FROM dbo.Sensors
	WHERE ID = @sensorID
END
	
END