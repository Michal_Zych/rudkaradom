﻿


CREATE PROCEDURE [dbo].[unt_UnitWithTransmitterGet]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		dbo.Units u
		JOIN dev_UnitTransmitter on u.ID=dev_UnitTransmitter.UnitID
END