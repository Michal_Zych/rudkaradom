﻿CREATE PROCEDURE [dbo].[ev_PatientFromHospital]
	@PatientId int
	
	,@OperatorId int
	,@Reason nvarchar(max) = null
	,@OperationDateUtc dateTime = null
AS
BEGIN
	SET NOCOUNT ON;

	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
		BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'PatientId=' + COALESCE(CONVERT(nvarchar(100), @PatientId), 'NULL') 
		EXEC Log_Action @@PROCID, @info
	END
	/****************************************************/

	DECLARE
		@UPDATE_ENDING_EVENT_ID bit = 1
		
	DECLARE
		@SEVERITY_EVENT dbo.EVENT_SEVERITY
		,@OBJECT_TYPE_PATIENT dbo.OBJECT_TYPE
		,@eventId int
		,@bandId int
		,@patientUnitId int

		
	SELECT @operationDateUtc = COALESCE(@operationDateUtc, dbo.GetCurrentUTCDate())
	
	SELECT
		@patientUnitId = COALESCE(p.RoomId, p.WardId)
		,@bandId = bs.BandId
		,@SEVERITY_EVENT = eventSeverity.Event
		,@OBJECT_TYPE_PATIENT = objectType.Patient
	FROM enum.EventSeverity eventSeverity
	JOIN enum.ObjectType objectType ON 1 = 1
	JOIN dbo.Patients p ON p.Id = @PatientId
	LEFT JOIN dbo.ev_BandStatus bs ON bs.ObjectId = p.Id AND bs.ObjectType = objectType.Patient
	
	
	


	BEGIN TRY
		BEGIN TRANSACTION
			-- przenieś do nieprzypisanych
			EXEC dbo.ev_AssignPatientToWardRoom_core @PatientId, NULL, NULL, NULL, NULL, @OperatorId, NULL
			
			-- odepnij opaskę
			IF @bandId IS NOT NULL
				EXEC dbo.ev_AssignBandToObject_core @OperatorId, NULL, @Reason, @bandId, NULL, NULL, NULL
				

			-- wygeneruj zdarzenie wypisania
			INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason)
			SELECT @OperationDateUtc, eventType.RegisterOutHostpital, @SEVERITY_EVENT, @patientUnitId, @PatientId, @OBJECT_TYPE_PATIENT, @patientUnitId, @OperationDateUtc, @OperationDateUtc, @OperatorId, NULL, @Reason
			FROM enum.EventType eventType
			IF @UPDATE_ENDING_EVENT_ID = 1
			BEGIN
				SELECT @eventId = SCOPE_IDENTITY()
				UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
			END

			-- przenieś dane do archiwum
			EXEC arch.PatientToArchive @PatientId

			-- Zakończ wizytę dla zamapowanego pacjenta
			UPDATE arch.ext_Patients SET 
				VisitId = NULL 
				,LastVisitEndDateUtc = @OperationDateUtc
			WHERE M2mId = @PatientId


		SELECT @PatientId AS Result, NULL as ErrorCode, NULL as ErrorMessage
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		SELECT CONVERT(int, null) AS Result, ERROR_NUMBER() AS ErrorCode, ERROR_MESSAGE() AS ErrorMessage
	END CATCH	


END