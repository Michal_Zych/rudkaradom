﻿
CREATE PROCEDURE [dbo].[al_PatientAlarmConfigCopyFromUnit]
(
	@PatientId int,
	@UnitId int,

	@OperatorId int,
	@Reason nvarchar(max) = null
)
 AS  
 BEGIN

	BEGIN TRY
		BEGIN TRANSACTION
			EXEC al_PatientAlarmConfigCopyFromUnit_core @PatientId, @UnitId, @OperatorId, @Reason

		COMMIT TRANSACTION
		SELECT @PatientId AS Result, NULL as ErrorCode, NULL as ErrorMessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		SELECT CONVERT(int, null) AS Result, ERROR_NUMBER() AS ErrorCode, ERROR_MESSAGE() AS ErrorMessage
	END CATCH
END