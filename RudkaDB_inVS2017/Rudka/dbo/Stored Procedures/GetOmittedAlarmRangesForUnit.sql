﻿
-- =============================================
-- Author:		
-- Create date: 
-- Description:	zwraca zakresy wyłączonego alarmowania dla danego unita sensorowego
-- =============================================
CREATE PROCEDURE [dbo].[GetOmittedAlarmRangesForUnit]
	@unitId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	FROM dbo.AlarmsOmittedRanges r
	JOIN dbo.Sensors s ON r.SensorID = s.Id
	WHERE s.UnitId = @unitId
	ORDER BY r.DateStart

END
