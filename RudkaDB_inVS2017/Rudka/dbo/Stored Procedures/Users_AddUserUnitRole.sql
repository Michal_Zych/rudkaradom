﻿-- =============================================
-- Author:		pi
-- Create date: 09-01-2013
-- =============================================
CREATE PROCEDURE [dbo].[Users_AddUserUnitRole] @userId INT
	,@unitId INT
	,@roleId INT
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO UsersUnitsRoles (
		UserID
		,UnitID
		,RoleID
		)
	VALUES (
		@userId
		,@unitId
		,@roleId
		)

	--update Timestamp
	UPDATE Users
	SET NAME = NAME
	WHERE Id = @userId
END

