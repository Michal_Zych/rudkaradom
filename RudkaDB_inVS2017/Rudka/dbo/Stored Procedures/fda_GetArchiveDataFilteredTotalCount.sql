﻿CREATE PROCEDURE fda_GetArchiveDataFilteredTotalCount
@TreeUnitIds nvarchar(max),
@EventIdFrom int = null
,@EventIdTo int = null
,@PatientIdFrom int = null
,@PatientIdTo int = null
,@Name nvarchar(200) = null
,@LastName nvarchar(200) = null
,@DisplayName nvarchar(200) = null
,@Pesel char(200) = null
,@DateUtcFrom datetime = null
,@DateUtcTo datetime = null
,@TypeFrom tinyint = null
,@TypeTo tinyint = null
,@TypeDescription nvarchar(200) = null
,@SeverityFrom tinyint = null
,@SeverityTo tinyint = null
,@SeverityDescription nvarchar(200) = null
,@UnitNameFrom int = null
,@UnitNameTo int = null
,@UnitLocation nvarchar(200) = null
,@ObjectId nvarchar(200) = null
,@ObjectTypeFrom int = null
,@ObjectTypeTo int = null
,@ObjectTypeDescriptionFrom tinyint = null
,@ObjectTypeDescriptionTo tinyint = null
,@ObjectName nvarchar(200) = null
,@ObjectUnitId nvarchar(200) = null
,@ObjectUnitNameFrom int = null
,@ObjectUnitNameTo int = null
,@ObjectUnitLocation nvarchar(200) = null
,@EndDateUtc nvarchar(200) = null
,@EndingEventIdFrom datetime = null
,@EndingEventIdTo datetime = null
,@OperatorIdFrom int = null
,@OperatorIdTo int = null
,@OperatorNameFrom int = null
,@OperatorNameTo int = null
,@OperatorLastName nvarchar(200) = null
,@EventData nvarchar(200) = null
,@Reason nvarchar(200) = null
,@RaisingEventId nvarchar(200) = null
,@IsClosedFrom int = null
,@IsClosedTo int = null
,@ClosingDateUtcFrom bit = null
,@ClosingDateUtcTo bit = null
,@ClosingUserIdFrom datetime = null
,@ClosingUserIdTo datetime = null
,@ClosingUserNameFrom int = null
,@ClosingUserNameTo int = null
,@ClosingUserLastName nvarchar(200) = null
,@ClosingAlarmId nvarchar(200) = null
,@ClosingCommentFrom int = null
,@ClosingCommentTo int = null
,@UserMessageId nvarchar(200) = null
,@SenderIdFrom int = null
,@SenderIdTo int = null
,@SenderNameFrom int = null
,@SenderNameTo int = null
,@SenderLastName nvarchar(200) = null
,@Message nvarchar(200) = null
AS
BEGIN

 DECLARE @cmd nvarchar(max)
 SET @cmd = 'SELECT COUNT(1) '
		+ dbo.dsql_GetArchiveData_From()
		+ dbo.dsql_GetArchiveData_Where(@TreeUnitIds,
			@EventIdFrom, @EventIdTo, @PatientIdFrom, @PatientIdTo, @Name, @LastName
			, @DisplayName, @Pesel, @DateUtcFrom, @DateUtcTo, @TypeFrom, @TypeTo
			, @TypeDescription, @SeverityFrom, @SeverityTo, @SeverityDescription, @UnitNameFrom, @UnitNameTo
			, @UnitLocation, @ObjectId, @ObjectTypeFrom, @ObjectTypeTo, @ObjectTypeDescriptionFrom, @ObjectTypeDescriptionTo
			, @ObjectName, @ObjectUnitId, @ObjectUnitNameFrom, @ObjectUnitNameTo, @ObjectUnitLocation, @EndDateUtc
			, @EndingEventIdFrom, @EndingEventIdTo, @OperatorIdFrom, @OperatorIdTo, @OperatorNameFrom, @OperatorNameTo
			, @OperatorLastName, @EventData, @Reason, @RaisingEventId, @IsClosedFrom, @IsClosedTo
			, @ClosingDateUtcFrom, @ClosingDateUtcTo, @ClosingUserIdFrom, @ClosingUserIdTo, @ClosingUserNameFrom, @ClosingUserNameTo
			, @ClosingUserLastName, @ClosingAlarmId, @ClosingCommentFrom, @ClosingCommentTo, @UserMessageId, @SenderIdFrom
			, @SenderIdTo, @SenderNameFrom, @SenderNameTo, @SenderLastName, @Message)

 EXEC(@cmd)
-- print @cmd
END