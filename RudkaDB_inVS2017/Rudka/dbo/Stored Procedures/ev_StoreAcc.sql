﻿CREATE PROCEDURE [dbo].[ev_StoreAcc]
	@BandId smallint
	,@EventDateUtc dateTime = NULL
AS
BEGIN
	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
	BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'BandId=' + COALESCE(CONVERT(nvarchar(100), @BandId), 'NULL') 
					+ ';EventDateUtc=' + COALESCE(CONVERT(nvarchar(50), @EventDateUtc, 121), 'NULL')
		EXEC Log_Action @@PROCID, @info
	END
	/****************************************************/
	
	DECLARE @UPDATE_ENDING_EVENT_ID bit = 1
		,@SYSTEM_USER_ID int = 0
		,@OBJECT_TYPE_BAND dbo.OBJECT_TYPE

	DECLARE
		@currentDateUtc datetime = dbo.GetCurrentUtcDate()
		,@bandStatusChanged bit = 0

	IF @EventDateUtc IS NULL SET @EventDateUtc = @currentDateUtc


BEGIN -- zmienne konfiguracji
	DECLARE
		 @objectType				dbo.OBJECT_TYPE   
		,@objectId					dbo.OBJECT_ID
		,@objectUnitId				int  
		,@currentUnitId				int  
		,@inCurrentUnitUtc			dateTime  
		,@previousUnitId			int  
		,@inPreviousUnitUtc			dateTime  
		,@topAlarmId				int  
		,@topAlarmType				dbo.EVENT_TYPE   
		,@topAlarmSeverity			dbo.EVENT_SEVERITY   
		,@topAlarmDateUtc			dateTime 
		,@topEventDateUtc			dateTime  
		,@severity1Count			int  
		,@severity2Count			int  
		,@ongNoGoZoneInEventId		int  
		,@ongNoGoZoneInAlarmId		int  
		,@ongDisconnectedEventId	int  
		,@ongDisconnectedAlarmId	int  
		,@ongZoneOutEventId			int  
		,@ongZoneOutAlarmId			int  
		,@ongNoMovingEventId		int  
		,@ongNoMovingAlarmId		int  
		,@ongMovingEventId			int  
		,@ongMovingAlarmId			int  
		,@ongLowBatteryEventId		int  
		,@ongLowBatteryAlarmId		int 

		,@isAccActive				bit

		,@fullMonitoringUnassignedBands bit

		,@currentEventType			dbo.EVENT_TYPE
		,@currentEventSeverity		dbo.EVENT_SEVERITY
END

BEGIN	-- pobranie konfiguracji
	SELECT
		 @objectType					= bs.ObjectType			
		,@objectId						= bs.ObjectId				
		,@objectUnitId					= bs.ObjectUnitId			
		,@currentUnitId					= bs.CurrentUnitId			
		,@inCurrentUnitUtc				= bs.InCurrentUnitUtc		
		,@previousUnitId				= bs.PreviousUnitId		
		,@inPreviousUnitUtc				= bs.InPreviousUnitUtc		
		,@topAlarmId					= bs.TopAlarmId			
		,@topAlarmType					= bs.TopAlarmType			
		,@topAlarmSeverity				= bs.TopAlarmSeverity		  
		,@topAlarmDateUtc				= bs.TopAlarmDateUtc		
		,@topEventDateUtc				= bs.TopEventDateUtc		
		,@severity1Count				= bs.Severity1Count		
		,@severity2Count				= bs.Severity2Count		
		,@ongNoGoZoneInEventId			= bs.OngNoGoZoneInEventId  
		,@ongNoGoZoneInAlarmId			= bs.OngNoGoZoneInAlarmId  
		,@ongDisconnectedEventId		= bs.OngDisconnectedEventId
		,@ongDisconnectedAlarmId		= bs.OngDisconnectedAlarmId
		,@ongZoneOutEventId				= bs.OngZoneOutEventId		
		,@ongZoneOutAlarmId				= bs.OngZoneOutAlarmId		
		,@ongNoMovingEventId			= bs.OngNoMovingEventId	
		,@ongNoMovingAlarmId			= bs.OngNoMovingAlarmId	
		,@ongMovingEventId				= bs.OngMovingEventId		
		,@ongMovingAlarmId				= bs.OngMovingAlarmId		
		,@ongLowBatteryEventId			= bs.OngLowBatteryEventId  
		,@ongLowBatteryAlarmId			= bs.OngLowBatteryAlarmId  

		,@isAccActive					= p.IsACCactive
		
		,@fullMonitoringUnassignedBands = st.FullMonitoringUnassignedBands
						
		,@currentEventType				= eventType.ACC
		,@currentEventSeverity			= CASE WHEN p.IsACCactive = 1 THEN eventSeverity.Alarm ELSE eventSeverity.Event END
		,@OBJECT_TYPE_BAND				= objectType.Band
	FROM dbo.ev_Settings st 
		 JOIN enum.EventType eventType ON 1 = 1
		 JOIN enum.ObjectType objectType ON 1 = 1
		 JOIN enum.EventSeverity eventSeverity ON 1 = 1
		 LEFT JOIN dbo.ev_BandStatus bs ON bs.BandId = @BandId
		 LEFT JOIN dbo.al_ConfigPatients p ON p.PatientId = bs.ObjectId AND bs.ObjectType = objectType.Patient
END	

	--zakończ brak łączności
	IF @ongDisconnectedEventId IS NOT NULL
	BEGIN
		EXEC [dbo].[ev_SetBandConnected]
			@BandId						= @BandId
			,@currentDateUtc			= @currentDateUtc
			,@CurrentEventDateUtc		= @EventDateUtc
			,@CurrentEventUnitId		= @currentUnitId
			,@ObjectId					= @objectId				
			,@ObjectType				= @objectType
			,@ObjectUnitId				= @objectUnitId
			,@CurrentUnitId				= @currentUnitId			OUTPUT
			,@InCurrentUnitUtc			= @inCurrentUnitUtc			OUTPUT
			,@PreviousUnitId			= @previousUnitId			OUTPUT
			,@InPreviousUnitUtc			= @inPreviousUnitUtc		OUTPUT
			,@OngDisconnectedEventId	= @ongDisconnectedEventId	OUTPUT
			,@OngDisconnectedAlarmId	= @ongDisconnectedAlarmId	OUTPUT
		SET @bandStatusChanged = 1
	END

	IF @fullMonitoringUnassignedBands = 0 RETURN


	-- zarejestrowanie rekordu statusu dla opaski
	IF @objectId IS NULL
	BEGIN
		EXEC dbo.ev_AssignBandToItself
			@EventDateUtc		= @EventDateUtc
			,@BandId			= @BandId
			,@ObjectId			= @objectId			OUTPUT
			,@ObjectType		= @objectType		OUTPUT
			,@InCurrentUnitUtc	= @inCurrentUnitUtc	OUTPUT
			,@severity1Count	= @severity1Count	OUTPUT
			,@severity2Count	= @severity2Count	OUTPUT
	END
		
	-- generuj zdarzenie/alarm
	DECLARE
		@currentEventId int
		
	INSERT ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, EndingEventId)
	SELECT @EventDateUtc, @currentEventType, @currentEventSeverity, @currentUnitId, @objectId, @objectType, @objectUnitId, @currentDateUtc, @EventDateUtc, null
	SET @currentEventId = SCOPE_IDENTITY()
	IF @UPDATE_ENDING_EVENT_ID = 1
	BEGIN
		UPDATE dbo.ev_Events SET EndingEventId = @currentEventId WHERE Id = @currentEventId
	END

	IF @isAccActive = 1
	BEGIN
		SET @bandStatusChanged = 1
		INSERT dbo.ev_Alarms(EventId, RaisingEventId) VALUES (@currentEventId, @currentEventId)
		SELECT @Severity2Count = @Severity2Count + 1

	END

	-- uaktualnij status
	IF @bandStatusChanged = 1
		EXEC dbo.ev_GenerateBandStatus
			 @BandId					= @BandId 
			,@CurrentUnitId 			= @CurrentUnitId 
			,@InCurrentUnitUtc			= @InCurrentUnitUtc
			,@PreviousUnitId 			= @PreviousUnitId 
			,@InPreviousUnitUtc 		= @InPreviousUnitUtc 
			,@TopAlarmId 				= @TopAlarmId 
			,@TopAlarmType  			= @TopAlarmType  
			,@TopAlarmSeverity 			= @TopAlarmSeverity 
			,@TopAlarmDateUtc 			= @TopAlarmDateUtc 
			,@TopEventDateUtc 			= @TopEventDateUtc 
			,@Severity1Count 			= @Severity1Count 
			,@Severity2Count 			= @Severity2Count 
			,@OngNoGoZoneInEventId 		= @OngNoGoZoneInEventId 
			,@OngNoGoZoneInAlarmId  	= @OngNoGoZoneInAlarmId  
			,@OngDisconnectedEventId  	= @OngDisconnectedEventId 
			,@OngDisconnectedAlarmId  	= @OngDisconnectedAlarmId 
			,@OngZoneOutEventId  		= @OngZoneOutEventId  
			,@OngZoneOutAlarmId  		= @OngZoneOutAlarmId  
			,@OngNoMovingEventId  		= @OngNoMovingEventId  
			,@OngNoMovingAlarmId  		= @OngNoMovingAlarmId  
			,@OngMovingEventId  		= @OngMovingEventId  
			,@OngMovingAlarmId  		= @OngMovingAlarmId  
			,@OngLowBatteryEventId  	= @OngLowBatteryEventId  
			,@OngLowBatteryAlarmId 		= @OngLowBatteryAlarmId 
			,@currentEventId			= @currentEventId
			,@CurrentEventType 			= @CurrentEventType 
			,@CurrentEventDateUtc 		= @EventDateUtc 
			,@GeneratedAlarmId			= @currentEventId
			,@GeneratedAlarmSeverity 	= @currentEventSeverity 
			,@GeneratedWarningDateUtc 	= null
			,@GeneratedAlarmDateUtc 	= @EventDateUtc 
END