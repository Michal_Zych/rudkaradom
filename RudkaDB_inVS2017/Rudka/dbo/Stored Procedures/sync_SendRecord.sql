﻿CREATE PROCEDURE dbo.sync_SendRecord
	@RecType char(4)
	,@Record nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT tech.sync_RecordsOUT(RecType, DateUtc, Record) 
	VALUES (@RecType, dbo.GetCurrentUTCDate(), @record)
END