﻿

CREATE PROCEDURE [dbo].[Com_StoreData]
	@SensorId smallint
	,@MeasurementTime datetime
	,@Value smallint
	,@EventType tinyint = 0 -- 0 = OK
AS
BEGIN
SET NOCOUNT ON;

-- @currnet_date z parametu do ciała procedury



--początek
	--zwracane wartości
	DECLARE @MEASURE_STORED int = 10
		, @SENSOR_NO_ACIVE int = 20
		, @RESULT_OK int = 1
		, @RESULT_ERROR int = -1

	DECLARE 
		@LOW_STATE_VALUE smallint = 0
		,@HIGH_STATE_VALUE smallint = 1000

	DECLARE --stałe typów alarmów EventType
		 @VALUE_OK tinyint = 0
		,@VALUE_TO_LOW tinyint = 1
		,@VALUE_TO_HIGH tinyint = 2
		,@NO_COMMUNICATION tinyint = 3
		,@NO_SENSOR tinyint = 3
		,@NO_DEVICE tinyint = 4
		,@NO_HUB tinyint = 5
		--typy powiadomień
		, @GUI			tinyint = 0
		, @SMS			tinyint = 1
		, @EMAIL		tinyint = 2
		--czas przetwarzania
		, @CURRENT_DATE datetime = [dbo].[GetCurrentDate]()
		-- powód zamknięcia alarmu
		, @REASON_SHORT_ALARM smallint = 1
		, @REASON_ALARMING_DISABLED smallint = 1
		, @REPEAT_UNTIL_CLOSE nchar(2) = 'C'
		, @REPEAT_UNTIL_FINISH nchar(2) ='F'
		, @REPEAT_UNTIL_FINISHCLOSE nchar(2) = 'FC'

	DECLARE
		@active bit
		,@enabled bit
		,@loRange smallint
		,@upRange smallint
		,@rangeValidFrom datetime
		,@isBitSensor bit
		,@isHighAlarming bit
		,@loStateDesc nvarchar(10)
		,@hiStateDesc nvarchar(10)
		,@scale real

		,@alDisabled bit
		,@alDisabledFrom datetime
		,@alDisabledForHours bit
		,@alDisableHoursFrom1 time
		,@alDisableHoursTo1 time
		,@alDisableHoursFrom2 time
		,@alDisableHoursTo2 time 
		,@alDisabledForRanges bit
		
		,@guiRangeDelay smallint
		,@guiRangeRepeat bit
		,@guiRangeRepeatMins smallint
		,@guiRangeRepeatBroken bit
		
		,@guiCommDelay smallint
		,@guiCommRepeat bit
		,@guiCommRepeatMins smallint
		,@guiCommRepeatBroken bit
		
		,@smsRangeEnabled bit
		,@smsRangeDelay smallint
		,@smsRangeAfterFinish bit
		,@smsRangeAfterClose bit
		,@smsRangeRepeat bit
		,@smsRangeRepeatMins smallint
		,@smsRangeRepeatUntil nchar(2)
		,@smsRangeRepeatBroken bit
		
		,@smsCommEnabled bit
		,@smsCommDelay smallint
		,@smsCommAfterFinish bit
		,@smsCommAfterClose bit
		,@smsCommRepeat bit
		,@smsCommRepeatMins smallint
		,@smsCommRepeatUntil nchar(2)
		,@smsCommRepeatBroken bit
		
		,@mailRangeEnabled bit
		,@mailRangeDelay smallint
		,@mailRangeAfterFinish bit
		,@mailRangeAfterClose bit
		,@mailRangeRepeat bit
		,@mailRangeRepeatMins smallint
		,@mailRangeRepeatUntil nchar(2)
		,@mailRangeRepeatBroken bit
		
		,@mailCommEnabled bit
		,@mailCommDelay smallint
		,@mailCommAfterFinish bit
		,@mailCommAfterClose bit
		,@mailCommRepeat bit
		,@mailCommRepeatMins smallint
		,@mailCommRepeatUntil nchar(2)
		,@mailCommRepeatBroken bit
		
		,@alarmRangeId int
		,@alarmRangeTime datetime	
		,@alarmRangeType tinyint
		,@alarmRangeCloseTime datetime
		,@alarmCommId int
		,@alarmCommTime datetime	
		,@alarmCommCloseTime datetime
		
		,@omittedRangeDateStart datetime
		,@measureAlLastSmsTime datetime
		,@measureAlLastEmailTime datetime
		,@commAlLastSmsTime datetime
		,@commAlLastEmailTime datetime


	DECLARE--zmienne robocze
		  @alarmsToFinischCloseTime datetime-- czas kiedy należy wyłączyć trwające alarmy
		, @alarmingEnabled	bit				-- 0 = całkowite wyłączenie alarmowania  
		, @closeReason smallint				-- powód zamknięcia alarmu
		, @closeMessage ALARM_COMMENT		-- komenntarz automatycznego zmaknięcia
		, @close bit						-- czy kończony alarm należy zamknąć
		, @commToClose int					-- id alarmu braku łączności do zamknięcia/zakończenia
		, @rangeToClose int					-- id alarmu przekroczenia zakresu do zamknięcia/zakończenia
		, @commOpen bit						-- otwórz alarm braku łączności
		, @rangeOpen bit					-- otwórz alarm przekroczenia zakresu
		, @rangeOpened int					-- id otwartego alarmu przekroczenia zakresu
		, @commOpened int					-- id otwartego alarmu braku łączności	
		, @toFinishCloseComm int			-- alarm techniczny będzie zamykany
		, @toFinishCloseRange int			-- alarm zakresu bedzie zamykany
	DECLARE--tabele robocze
		  @alarmsToFinischClose TABLE(		-- tabela zawierająca informacje o alarmach do zakończenia
				  ID int NOT NULL			-- id alarmu
				, CloseAlarm bit NOT NULL	-- = 1 alarm należy zamknąć
				, CloseReason smallint		-- powód zamknięcia
				, CloseComment ALARM_COMMENT-- opis powodu zamknięcia
				, IsRangeAlarm bit NOT NULL)-- czy zamykany alarm to Alarm przekroczenia zakresu
		DECLARE
		  @notifications TABLE(				-- tabela zawierająca powiadomienia które nalezy dodać
			AlarmID int NOT NULL			-- ID alarmu
			, Time DateTime NULL	    	-- czas wysłania powiadomienia
			, Type tinyint NOT NULL			-- typ powiadomienia (stałe - typy powiadomien)
			, Message nvarchar(2000)
		)
	SELECT
        @active								= Active
	  , @enabled							= [Enabled]
      , @loRange							= s.LoRange
      , @upRange							= s.UpRange
      , @Value								= CASE 
												WHEN IsBitSensor = 1 THEN @Value
												ELSE @Value + Calibration
											  END
      , @rangeValidFrom						= RangeValidFrom
	  , @isBitSensor						= IsBitSensor
      , @isHighAlarming						= IsHighAlarming
	  , @loStateDesc						= LoStateDesc
	  , @hiStateDesc						= HiStateDesc    									
	  , @scale								= Scale

      , @alDisabled							= AlDisabled
      , @alDisabledFrom						= AlDisabledFrom
      , @alDisabledForHours					= AlDisabledForHours
      , @alDisableHoursFrom1				= AlDisableHoursFrom1
      , @alDisableHoursTo1					= AlDisableHoursTo1
      , @alDisableHoursFrom2				= AlDisableHoursFrom2
      , @alDisableHoursTo2					= AlDisableHoursTo2
      , @alDisabledForRanges				= AlDisabledForRanges
	    									
      , @guiRangeDelay						= GuiRangeDelay
      , @guiRangeRepeat						= GuiRangeRepeat
      , @guiRangeRepeatMins					= GuiRangeRepeatMins
      , @guiRangeRepeatBroken				= 0 --ar.GuiRepeatBroken
	    									
	  , @guiCommDelay						= GuiCommDelay
      , @guiCommRepeat						= GuiCommRepeat
      , @guiCommRepeatMins					= GuiCommRepeatMins
      , @guiCommRepeatBroken				= 0 --ac.GuiRepeatBroken
        									
      , @smsRangeEnabled					= SmsRangeEnabled
      , @smsRangeDelay						= SmsRangeDelay
      , @smsRangeAfterFinish				= SmsRangeAfterFinish
      , @smsRangeAfterClose					= SmsRangeAfterClose
      , @smsRangeRepeat						= SmsRangeRepeat
      , @smsRangeRepeatMins					= SmsRangeRepeatMins
      , @smsRangeRepeatUntil				= SmsRangeRepeatUntil
      , @smsRangeRepeatBroken				= 0 --ar.SmsRepeatBroken
	    									
      , @smsCommEnabled						= SmsCommEnabled
      , @smsCommDelay						= SmsCommDelay
      , @smsCommAfterFinish					= SmsCommAfterFinish
      , @smsCommAfterClose					= SmsCommAfterClose
      , @smsCommRepeat						= SmsCommRepeat
      , @smsCommRepeatMins					= SmsCommRepeatMins
      , @smsCommRepeatUntil					= SmsCommRepeatUntil
      , @smsCommRepeatBroken				= 0 --ac.SmsRepeatBroken
	    									
      , @mailRangeEnabled					= MailRangeEnabled
      , @mailRangeDelay						= MailRangeDelay
      , @mailRangeAfterFinish				= MailRangeAfterFinish
      , @mailRangeAfterClose				= MailRangeAfterClose
      , @mailRangeRepeat					= MailRangeRepeat
      , @mailRangeRepeatMins				= MailRangeRepeatMins
      , @mailRangeRepeatUntil				= MailRangeRepeatUntil
      , @mailRangeRepeatBroken				= 0 --ar.MailRepeatBroken
	    									
      , @mailCommEnabled					= MailCommEnabled
      , @mailCommDelay						= MailCommDelay
      , @mailCommAfterFinish				= MailCommAfterFinish
      , @mailCommAfterClose					= MailCommAfterClose
      , @mailCommRepeat						= MailCommRepeat
      , @mailCommRepeatMins					= MailCommRepeatMins
      , @mailCommRepeatUntil				= MailCommRepeatUntil
      , @mailCommRepeatBroken				= 0 --ac.MailRepeatBroken
											
	  --informacje o trwających alarmach	
	  , @alarmRangeId						= MeasureAlID		
	  , @alarmRangeTime						= MeasureAlTime		
	  , @alarmRangeType						= MeasureAlType	
	  , @alarmRangeCloseTime				= ar.DateStatus
	  , @alarmCommId						= CommAlID			
	  , @alarmCommTime						= CommAlTime
	  , @alarmCommCloseTime					= ac.DateStatus
											
	  , @omittedRangeDateStart				= o.DateStart
	  , @measureAlLastSmsTime				= ao.MeasureAlLastSmsTime 
	  , @measureAlLastEmailTime 			= ao.MeasureAlLastEmailTime
	  , @commAlLastSmsTime 					= ao.CommAlLastSmsTime 
	  , @commAlLastEmailTime 				= ao.CommAlLastEmailTime 
	FROM dbo.Sensors s
	LEFT JOIN dbo.AlarmsOngoing ao ON s.Id = ao.SensorID
	LEFT JOIN dbo.AlarmsOmittedRanges o ON (s.Id = o.SensorID AND (@MeasurementTime BETWEEN o.DateStart AND o.DateEnd))
	LEFT JOIN dbo.Alarms ar ON ar.Id = ao.MeasureAlID
	LEFT JOIN dbo.Alarms ac ON ac.Id = ao.CommAlID
	WHERE s.Id = @SensorId

BEGIN--jeśli zakres nie jest aktualny dla podanego czasu to pobierz go z tabeli MeasurementRanges
	IF @MeasurementTime < @rangeValidFrom
	BEGIN
		SELECT @loRange = LoRange, @upRange = UpRange
		FROM dbo.MeasurementRanges WITH(NOLOCK) 
		WHERE SensorID = @SensorId
			  AND @MeasurementTime BETWEEN DateStart AND DateEnd
	END
END

BEGIN --dla czujników bitowych 0 stan niski, wszysto inne stan wysoki
	IF @isBitSensor = 1 AND @Value <> 0 SET @Value = @HIGH_STATE_VALUE
END


	IF @active = 0 OR @enabled = 0
	BEGIN
		SELECT @SENSOR_NO_ACIVE
		RETURN
	END

BEGIN--jeśli zakres nie jest aktualny dla podanego czasu to pobierz go z tabeli MeasurementRanges
	IF @MeasurementTime < @rangeValidFrom
	BEGIN
		SELECT @loRange = LoRange, @upRange = UpRange
		FROM dbo.MeasurementRanges WITH(NOLOCK) 
		WHERE SensorID = @SensorId
			  AND @MeasurementTime BETWEEN DateStart AND DateEnd
	END
END

BEGIN --OBLICZ CZAS WYŁĄCZENIA ALARMOWANIA
	SET @alarmsToFinischCloseTime = null
	SET @alarmingEnabled = 1
	
	DECLARE @h time
	DECLARE @d datetime
	-- jeśli całkowicie wyłączone alarmowanie
	IF (@alDisabled = 1)
	BEGIN
		IF(@alDisabledFrom IS NULL) SET @alarmsToFinischCloseTime = @MeasurementTime
		ELSE SET @alarmsToFinischCloseTime = @alDisabledFrom
	END
	-- jeśli wyłączone w godzinach
	IF(@alDisabledForHours = 1) 
	BEGIN
		SET @h = NULL -- godzina rozpoczęcia okresu wyłączenia alarmowania
		IF(CONVERT(time, @MeasurementTime) BETWEEN @alDisableHoursFrom1 AND @alDisableHoursTo1)
			SET @h = @alDisableHoursFrom1
		IF(CONVERT(time, @MeasurementTime) BETWEEN @alDisableHoursFrom2 AND @alDisableHoursTo2)
			SET @h = @alDisableHoursFrom2
		IF(@h IS NOT NULL) -- @h godzina początkowa zakresu wyłączonego alarmowania
		BEGIN
			-- data wyłączenia to dzień pomiaru + godzina początkowa zakresu wyłączenia
			SET @d = CONVERT(datetime, CONVERT(date, @MeasurementTime)) + CONVERT(datetime, @h)
			-- czas wyłączenia alarmowania to miejszy z czau calkowitego wyłączenia i czasu wyłączenia w godzinach
			IF(@alarmsToFinischCloseTime IS NULL) SET @alarmsToFinischCloseTime = @d
			ELSE IF(@d < @alarmsToFinischCloseTime) SET @alarmsToFinischCloseTime = @d
		END
	END
	-- jeśli wyłączone w podanych zakresach
	IF(@alDisabledForRanges =1)
	BEGIN
		IF(@omittedRangeDateStart IS NOT NULL)
		-- czas wyłączenia to mniejszy z dotychczas obliczonego i czasu wyłączenia w okresach
		BEGIN
			IF(@alarmsToFinischCloseTime IS NULL) 
				SET @alarmsToFinischCloseTime = @omittedRangeDateStart
			ELSE IF(@omittedRangeDateStart < @alarmsToFinischCloseTime) 
				SET @alarmsToFinischCloseTime = @omittedRangeDateStart
		END
	END
	
	IF (@alarmsToFinischCloseTime IS NOT NULL)
		SET @alarmingEnabled = 0
END

BEGIN--WYZNACZ TYP ZDARZENIA 
	IF @EventType < @NO_COMMUNICATION 
	BEGIN
		IF @Value < @loRange SET @EventType = @VALUE_TO_LOW
		IF @Value > @upRange SET @EventType = @VALUE_TO_HIGH
	END
END

BEGIN--WYZNACZ ALARMY DO ZAKOŃCZENIA/ZAMKNIĘCIA
	IF @alarmingEnabled = 1 
	BEGIN
		SELECT @closeReason = @REASON_SHORT_ALARM, @closeMessage = 'Krótki alarm zamknięty automatycznie'
		SET @alarmsToFinischCloseTime = @MeasurementTime
		IF @EventType < @NO_COMMUNICATION --jeśli brak łączności to nie kończymy żadnych alarmów
		BEGIN--które alarmy do zakończenia
			--jest odczyt zakończ brak łączności
			SET @commToClose = @alarmCommId
			--pomiary ok - zamknij alarmy
			IF @EventType = @VALUE_OK
				SET @rangeToClose = @alarmRangeId
			--pomiar przekracza górną granicę
			IF @EventType = @VALUE_TO_HIGH
			BEGIN
				IF @alarmRangeType = @VALUE_TO_LOW --jeśli poprzednio była dolna to zamknij
					SET @rangeToClose = @alarmRangeId
			END
			--pomiar przekracza dolną granicę
			IF @EventType = @VALUE_TO_LOW
			BEGIN
				IF @alarmRangeType = @VALUE_TO_HIGH --jeśli poprzednio była górna to zamknij
					SET @rangeToClose = @alarmRangeId
			END
		END	
		BEGIN--czy alarmy do zakończenia to krótkie alarmy i należy zamknąć
			IF @rangeToClose IS NOT NULL
			BEGIN
				SET @close = 0
				IF DATEDIFF(SECOND, @alarmRangeTime, @alarmsToFinischCloseTime) <= @guiRangeDelay
					SET @close = 1
				INSERT @alarmsToFinischClose VALUES(@rangeToClose, @close
										,CASE 
											WHEN @close = 1 THEN @closeReason
											ELSE NULL 
										END
										,CASE 
											WHEN @close = 1 THEN @closeMessage
											ELSE NULL 
										END
										,1
										)
			END
			IF @commToClose IS NOT NULL
			BEGIN
				SET @close = 0
				IF DATEDIFF(SECOND, @alarmCommTime, @CURRENT_DATE) <= @guiCommDelay
					SET @close = 1
				INSERT @alarmsToFinischClose VALUES(@commToClose, @close
												,CASE 
													WHEN @close = 1 THEN @closeReason
													ELSE NULL		
												END
												,CASE 
													WHEN @close = 1 THEN @closeMessage
													ELSE NULL		
												END
												,0
												)
			END	
		END
	END
	ELSE  
	BEGIN--alarmowanie wyłączone
		SELECT @closeReason = @REASON_ALARMING_DISABLED, @closeMessage = 'Alarmowanie wyłączone, alarm zamknięty automatycznie'
		SET @rangeToClose = @alarmRangeId
		SET @commToClose = @alarmCommId
		IF @rangeToClose IS NOT NULL
			INSERT @alarmsToFinischClose VALUES(@rangeToClose, 1, @closeReason, @closeMessage, 1)
		IF @commToClose IS NOT NULL
			INSERT @alarmsToFinischClose VALUES(@commToClose, 1, @closeReason, @closeMessage, 0)
	END
END

BEGIN--WYZNACZ NOWE ALARMY
	IF @alarmingEnabled = 1
	BEGIN
		BEGIN--USTAL KTÓRE ALARMY OTWORZYĆ
			SET @rangeOpen = NULL
			SET @commOpen = NULL
			--jeśli nie ma pomiaru to otwórz brak łączności
			IF @EventType >= @NO_COMMUNICATION
			BEGIN
				IF @alarmCommId IS NULL
					SET @commOpen = 1
			END 
			ELSE
			BEGIN
				--jeśli nie było alarmu a jest przekroczenie to otwórz temperaturowy
				IF (@alarmRangeId IS NULL) AND (@EventType > @VALUE_OK)
					SET @rangeOpen = 1
				--jeśli było przkroczenie jednego progu a jest przekroczenie drugiego otwórz temperaturowy
				IF (@alarmRangeId IS NOT NULL) AND (@EventType > @VALUE_OK) AND (@alarmRangeType <> @EventType)
					SET @rangeOpen = 1
			END
		END
	END
END

	DECLARE @reopenComm bit
		,@reopenRange bit
	
BEGIN --WYZNACZ ALARMY DO ESKALACJI
	IF @alarmingEnabled = 1
	BEGIN
		IF @alarmCommId IS NOT NULL
		BEGIN
			SELECT @toFinishCloseComm = ID FROM @alarmsToFinischClose WHERE ID = @alarmCommId
			IF @toFinishCloseComm IS NULL AND @guiCommRepeat = 1 
				AND DATEADD(MINUTE, @guiCommRepeatMins, @alarmCommCloseTime) < @CURRENT_DATE
				SET @reopenComm = 1
		END

		IF @alarmRangeId IS NOT NULL
		BEGIN
			SELECT @toFinishCloseRange = ID FROM @alarmsToFinischClose WHERE ID = @alarmRangeId
			IF @toFinishCloseRange IS NULL AND @guiRangeRepeat = 1
				AND DATEADD(MINUTE, @guiRangeRepeatMins, @alarmRangeCloseTime) < @CURRENT_DATE
				SET @reopenRange = 1
		END
	END
END


BEGIN--WYZNACZENIE NOWYCH POWIADOMIEŃ
	IF @alarmingEnabled = 1 
	BEGIN
		--smsy
		INSERT @notifications
		SELECT a.Id 
			,@CURRENT_DATE 
			,@SMS 
			,CASE 
				WHEN a.CloseAlarm = 0 THEN dbo.FormatAlarmNotifyText(@SensorId, @SMS, @MeasurementTime, 'F', IsRangeAlarm, null, null, null, null)
				ELSE dbo.FormatAlarmNotifyText(@SensorId, @SMS, @MeasurementTime, 'C', IsRangeAlarm, null, null, null, null)
			END 
		FROM @alarmsToFinischClose a
		WHERE (a.IsRangeAlarm = 1 AND (@MeasureAlLastSmsTime IS NOT NULL) 
				AND (a.CloseAlarm = 0 AND @smsRangeAfterFinish = 1  OR a.CloseAlarm = 1 AND @smsRangeAfterClose = 1  ))
			OR
			(a.IsRangeAlarm = 0 AND (@CommAlLastSmsTime IS NOT NULL) 
				AND (a.CloseAlarm = 0 AND @smsCommAfterFinish = 1  OR a.CloseAlarm = 1 AND @smsCommAfterClose = 1  ))
		--maile
		INSERT @notifications
		SELECT a.Id 
			,@CURRENT_DATE 
			,@EMAIL
			,CASE 
				WHEN a.CloseAlarm = 0 THEN dbo.FormatAlarmNotifyText(@SensorId, @EMAIL, @MeasurementTime, 'F', IsRangeAlarm, null, null, null, null)
				ELSE dbo.FormatAlarmNotifyText(@SensorId, @EMAIL, @MeasurementTime, 'C', IsRangeAlarm, null, null, null, null)
			END 
		FROM @alarmsToFinischClose a
		WHERE (a.IsRangeAlarm = 1 AND (@MeasureAlLastEmailTime IS NOT NULL) 
				AND (a.CloseAlarm = 0 AND @mailRangeAfterFinish = 1  OR a.CloseAlarm = 1 AND @mailRangeAfterClose = 1  ))
			OR
			(a.IsRangeAlarm = 0 AND (@commAlLastEmailTime IS NOT NULL) 
				AND (a.CloseAlarm = 0 AND @mailCommAfterFinish = 1  OR a.CloseAlarm = 1 AND @mailCommAfterClose = 1  ))

	--DECLARE @v XML = (SELECT * FROM @notifications FOR XML AUTO)
	END
END

BEGIN--alarmowanie cykliczne
	DECLARE
		@valueStr nvarchar(10)
		,@loRangeStr nvarchar(10)
		,@upRangeStr nvarchar(10)
		,@rangeToFinist int
		,@commToFinist int
	IF @isBitSensor = 1
	BEGIN
		IF @Value IS NOT NULL
			IF @Value = @LOW_STATE_VALUE SELECT @valueStr = @loStateDesc
			ELSE SELECT @valueStr = @hiStateDesc
		
		IF @loRange = 0 SELECT @loRangeStr = @loStateDesc, @upRangeStr = @loStateDesc
		ELSE SELECT @loRangeStr = @hiStateDesc, @upRangeStr = @hiStateDesc
	END
	ELSE
	BEGIN
		SELECT @valueStr = REPLACE(CONVERT(nvarchar(20), @value * @scale), '.', ',')
			, @loRangeStr  = REPLACE(CONVERT(nvarchar(20), @loRange * @scale), '.', ',')
			, @upRangeStr  = REPLACE(CONVERT(nvarchar(20), @upRange * @scale), '.', ',')
	END

	SELECT @rangeToFinist = ID
	FROM @alarmsToFinischClose WHERE IsRangeAlarm = 1
	SELECT @commToFinist = ID
	FROM @alarmsToFinischClose WHERE IsRangeAlarm = 0



	IF @rangeToFinist IS NULL 
	BEGIN
		--smsy
			--range
		IF DATEADD(MINUTE, @smsRangeRepeatMins, @measureAlLastSmsTime) < @CURRENT_DATE
		BEGIN
			SELECT @measureAlLastSmsTime = @CURRENT_DATE
			IF @alarmingEnabled = 1 AND @smsRangeEnabled = 1 AND @smsRangeRepeat = 1
			BEGIN
				IF ((@smsRangeRepeatUntil = @REPEAT_UNTIL_CLOSE OR @smsRangeRepeatUntil = @REPEAT_UNTIL_FINISHCLOSE)
					AND (@alarmRangeCloseTime IS NULL) AND (@toFinishCloseRange IS NULL)) --alarm niezamknięty
					OR ((@smsRangeRepeatUntil = @REPEAT_UNTIL_FINISH) AND (@toFinishCloseRange IS NULL)) 
				BEGIN
					INSERT @notifications
						SELECT @alarmRangeId, @CURRENT_DATE, @SMS, dbo.FormatAlarmNotifyText(@SensorId, @SMS, @MeasurementTime, '', 1, @valueStr, @loRangeStr, @upRangeStr, null)
				END	
			END
		END
		--email
			--range
		IF DATEADD(MINUTE, @mailRangeRepeatMins, @measureAlLastemailTime) < @CURRENT_DATE
		BEGIN
			SELECT @measureAlLastemailTime = @CURRENT_DATE
			IF @alarmingEnabled = 1 AND @mailRangeEnabled = 1 AND @mailRangeRepeat = 1
			BEGIN
				IF ((@mailRangeRepeatUntil = @REPEAT_UNTIL_CLOSE OR @mailRangeRepeatUntil = @REPEAT_UNTIL_FINISHCLOSE)
					AND (@alarmRangeCloseTime IS NULL) AND (@toFinishCloseRange IS NULL)) --alarm niezamknięty
					OR
					((@mailRangeRepeatUntil = @REPEAT_UNTIL_FINISH) AND (@toFinishCloseRange IS NULL))
				BEGIN
					INSERT @notifications
						SELECT @alarmRangeId, @CURRENT_DATE, @EMAIL, dbo.FormatAlarmNotifyText(@SensorId, @EMAIL, @MeasurementTime, '', 1, @valueStr, @loRangeStr, @upRangeStr, null)
				END	
			END
		END
	END

	IF @commToFinist IS NULL
	BEGIN
		--sms
				--comm
		IF DATEADD(MINUTE, @smsCommRepeatMins, @commAlLastSmsTime) < @CURRENT_DATE
		BEGIN
			SELECT @commAlLastSmsTime = @CURRENT_DATE
			IF @alarmingEnabled = 1 AND @smsCommEnabled = 1 AND @smsCommRepeat = 1
			BEGIN
				IF ((@smsCommRepeatUntil = @REPEAT_UNTIL_CLOSE OR @smsCommRepeatUntil = @REPEAT_UNTIL_FINISHCLOSE)
					AND (@alarmCommCloseTime IS NULL) AND (@toFinishCloseComm IS NULL)) --alarm niezamknięty
					OR
					((@smsCommRepeatUntil = @REPEAT_UNTIL_FINISH) AND (@toFinishCloseComm IS NULL))
				BEGIN
					INSERT @notifications
						SELECT @alarmCommId, @CURRENT_DATE, @SMS, dbo.FormatAlarmNotifyText(@SensorId, @SMS, @CURRENT_DATE, '', 1, @valueStr, @loRangeStr, @upRangeStr, null)
				END	
			END
		END


	
		--email
				--comm
		IF DATEADD(MINUTE, @mailCommRepeatMins, @commAlLastemailTime) < @CURRENT_DATE
		BEGIN
			SELECT @commAlLastemailTime = @CURRENT_DATE
			IF @alarmingEnabled = 1 AND @mailCommEnabled = 1 AND @mailCommRepeat = 1
			BEGIN
				IF ((@mailCommRepeatUntil = @REPEAT_UNTIL_CLOSE OR @mailCommRepeatUntil = @REPEAT_UNTIL_FINISHCLOSE)
					AND (@alarmCommCloseTime IS NULL) AND (@toFinishCloseComm IS NULL)) --alarm niezamknięty
					OR
					((@mailCommRepeatUntil = @REPEAT_UNTIL_FINISH) AND (@toFinishCloseComm IS NULL))
				BEGIN
					INSERT @notifications
						SELECT @alarmCommId, @CURRENT_DATE, @EMAIL, dbo.FormatAlarmNotifyText(@SensorId, @EMAIL, @CURRENT_DATE, '', 1, @valueStr, @loRangeStr, @upRangeStr, null)
				END	
			END
		END
	END

END


	BEGIN TRANSACTION
		BEGIN TRY

			BEGIN--ZAPISZ WYNIKI
				INSERT dbo.Measurements(SensorID, MeasurementTime, Value, StoreDelay)
				VALUES(@SensorId, @MeasurementTime, @value, DATEDIFF(SECOND, @MeasurementTime, @CURRENT_DATE)) 
				
				UPDATE dbo.MeasurementsCurrent
				SET
					MeasurementTime = CASE
									WHEN @Value IS NULL 
										THEN CASE WHEN DATEADD(SECOND, @guiCommDelay, MeasurementTime) < @MeasurementTime THEN @MeasurementTime ELSE MeasurementTime END
									ELSE @MeasurementTime
								  END,
					Value = CASE
							WHEN @Value IS NULL
								THEN CASE WHEN DATEADD(SECOND, @guiCommDelay, MeasurementTime) < @MeasurementTime THEN @Value ELSE Value END
							ELSE @Value
						 END,
					HyperMesurementTime = @MeasurementTime,
					HyperValue = @Value,
					MeasurementStoreTime = CASE WHEN (@Value IS NOT NULL) THEN @CURRENT_DATE
												ELSE MeasurementStoreTime END
				WHERE SensorID = @SensorId
				IF(@@ROWCOUNT = 0)
				BEGIN
					INSERT INTO dbo.MeasurementsCurrent(SensorID, MeasurementTime, Value, HyperMesurementTime, HyperValue, MeasurementStoreTime)
						VALUES(@SensorId, @MeasurementTime, @Value, @MeasurementTime, @Value, 
								CASE WHEN (@Value IS NOT NULL)  THEN @CURRENT_DATE
												ELSE NULL END)
				END
			END
			BEGIN--ZAKOŃCZ/ZAMKNIJ ALARMY
				UPDATE a --zakończ alarmy, zamknij jeśli wyłączone alarmowanie lub alarm krótki
				SET 
					  DateEnd = CASE
									WHEN c.ID = @alarmCommId THEN @CURRENT_DATE
									ELSE @MeasurementTime
								END
					, Status = CASE WHEN a.Status = 1 THEN 1 ELSE c.CloseAlarm END
					, UserStatus = a.UserStatus
					, DateStatus = CASE
										WHEN a.DateStatus IS NOT NULL THEN a.DateStatus
										ELSE CASE
												WHEN c.CloseAlarm = 1 THEN 
													CASE
														WHEN c.ID = @alarmCommId THEN @CURRENT_DATE
														ELSE @alarmsToFinischCloseTime
													END 
												ELSE NULL
											END
								   END
					, CommentStatus = COALESCE(a.CommentStatus, c.CloseComment)
				FROM dbo.Alarms a WITH(NOLOCK) 
				JOIN @alarmsToFinischClose c ON a.ID = c.ID

				IF @rangeToClose IS NOT NULL
					SELECT @alarmRangeId =  NULL, @alarmRangeTime = NULL, @alarmRangeType = NULL, @measureAlLastSmsTime = NULL, @measureAlLastEmailTime = NULL
				IF @commToClose IS NOT NULL
					SELECT @alarmCommId = NULL, @alarmCommTime = NULL, @commAlLastSmsTime = NULL, @commAlLastEmailTime = NULL

				UPDATE dbo.AlarmsOngoing --usuń z alarmów trwających
				SET
					MeasureAlID = @alarmRangeId
					,MeasureAlTime = @alarmRangeTime
					,MeasureAlType = @alarmRangeType
					,CommAlID = @alarmCommId
					,CommAlTime = @alarmCommTime
					,MeasureAlLastSmsTime = @measureAlLastSmsTime
					,MeasureAlLastEmailTime = @measureAlLastEmailTime
					,CommAlLastSmsTime = @commAlLastSmsTime
					,CommAlLastEmailTime = @commAlLastEmailTime
				WHERE SensorID = @SensorId
				
				
				DELETE FROM dbo.AlarmNotifications --usuń powiadomienia które miały być wysłane po czasie zamknięcia
				WHERE AlarmID IN (@rangeToClose, @commToClose)
					AND SendAfterDate >= @alarmsToFinischCloseTime
					
				UPDATE a
					SET a.DeleteDate = @CURRENT_DATE
					,Info = Info + ' | ' + CONVERT(nvarchar(30), @CURRENT_DATE, 121) + ':com_StoreData(Skasowano)'
				FROM dbo.AlarmNotifications a --usuń powiadomienia które miały być wysłane po czasie zamknięcia
				WHERE a.AlarmID IN (@rangeToClose, @commToClose)
					AND a.SendAfterDate >= @alarmsToFinischCloseTime	
					AND a.DeleteDate IS NULL
			END	
			BEGIN--ESKALACJA GUI
				IF @reopenComm = 1
				BEGIN
					UPDATE dbo.Alarms
						SET Status = 0, UserStatus = null, DateStatus = null, 
						CommentStatus = 
							COALESCE(CommentStatus, '')
						+ CHAR(13) + CONVERT(nvarchar(25), @CURRENT_DATE, 120) + ' Ponowne otwarcie' 
					WHERE ID = @alarmCommId
				END
				IF @reopenRange  = 1
				BEGIN
					UPDATE dbo.Alarms
						SET Status = 0, UserStatus = null, DateStatus = null, 
						CommentStatus = 
							COALESCE(CommentStatus, '')
						+ CHAR(13) + CONVERT(nvarchar(25), @CURRENT_DATE, 120) + ' Ponowne otwarcie' 
					WHERE ID = @alarmRangeId
				END
			END
			BEGIN--OTWÓRZ NOWE ALARMY I POWIADOMIENIA
				IF @alarmingEnabled = 1
				BEGIN
					BEGIN--OTWÓRZ ALARMY
						IF @rangeOpen = 1
						BEGIN
							INSERT dbo.Alarms(SensorID, [Timestamp], EventType, LoRange, UpRange)
							VALUES(@SensorId, @MeasurementTime, @EventType, @loRange, @upRange)
							SET @rangeOpened = SCOPE_IDENTITY()
						END
			
						IF @commOpen = 1
						BEGIN
							INSERT dbo.Alarms(SensorID, [Timestamp], EventType, LoRange, UpRange)
							VALUES(@SensorId, @CURRENT_DATE, @EventType, @lorange, @upRange)
							SET @commOpened = SCOPE_IDENTITY()
						END	
			
						--uzupełnij alarmy trwające
						IF @rangeOpened IS NOT NULL
						BEGIN
							SET @alarmRangeId = @rangeOpened
							SET @alarmRangeTime = @MeasurementTime
							SET @alarmRangeType = @EventType
						END
						IF @commOpened IS NOT NULL
						BEGIN 
							SET @alarmCommId = @commOpened
							SET @alarmCommTime = @CURRENT_DATE
						END

						UPDATE dbo.AlarmsOngoing 
						SET
							MeasureAlID = @alarmRangeId
							,MeasureAlTime = @alarmRangeTime
							,MeasureAlType = @alarmRangeType
							,CommalID = @alarmCommId
							,CommAlTime = @alarmCommTime
						WHERE SensorID = @SensorId
						IF (@@ROWCOUNT = 0)
						INSERT dbo.AlarmsOngoing(SensorID, MeasureAlID, MeasureAlTime, MeasureAlType, CommAlID, CommAlTime)
						VALUES(@SensorId, @alarmRangeId, @alarmRangeTime, @alarmRangeType, @alarmCommId, @alarmCommTime)
					END	
			
					
					BEGIN--DODAJ POWIADOMIENIA DLA OTWIERANYCH ALARMÓW
						DECLARE 
							@smsTime datetime
							,@mailTime datetime
						IF @rangeOpened IS NOT NULL --powiadomienia o przekroczeniu zakresu
						BEGIN
							SELECT @smsTime = null, @mailTime = null
							IF @smsRangeEnabled = 1
							BEGIN
								SELECT @smsTime = DATEADD(SECOND, @smsRangeDelay, @MeasurementTime)
								INSERT @notifications VALUES (@rangeOpened, @smsTime, @SMS, null)
							END
							IF @mailRangeEnabled = 1			
							BEGIN
								SELECT @mailTime = DATEADD(SECOND, @mailRangeDelay, @MeasurementTime)
								INSERT @notifications VALUES (@rangeOpened, @mailTime, @EMAIL, null)
							END
							UPDATE AlarmsOngoing SET 
								MeasureAlLastSmsTime = @smsTime
								,MeasureAlLastEmailTime = @mailTime
							WHERE SensorID = @SensorId
						END
						IF @commOpened IS NOT NULL --powiadomienia o alarmie braku łączności
						BEGIN
							SELECT @smsTime = null, @mailTime = null
							IF @smsCommEnabled = 1
							BEGIN
								SELECT @smsTime = DATEADD(SECOND, @smsCommDelay, @CURRENT_DATE)
								INSERT @notifications VALUES (@commOpened, @smsTime, @SMS, null)
							END
							IF @mailCommEnabled = 1
							BEGIN
								SELECT @mailTime = DATEADD(SECOND, @mailCommDelay, @CURRENT_DATE)
								INSERT @notifications VALUES (@commOpened, @mailTime, @EMAIL, null)
							END
							UPDATE AlarmsOngoing SET 
								CommAlLastSmsTime = @smsTime
								,CommAlLastEmailTime = @mailTime
							WHERE SensorID = @SensorId
						END
				
						INSERT dbo.AlarmNotifications (SensorId, AlarmID, SendAfterDate, Type, Message, Info)
						SELECT @SensorId, AlarmID, Time, Type, Message, CONVERT(nvarchar(30), @CURRENT_DATE, 120) + ':com_StoreData(Insert)'
						FROM @notifications 
						WHERE Time IS NOT NULL
					END
					
				END
			END

		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			SELECT @RESULT_ERROR
			select ERROR_MESSAGE()
			RETURN
		END CATCH
	COMMIT TRANSACTION

	SELECT @RESULT_OK
END


