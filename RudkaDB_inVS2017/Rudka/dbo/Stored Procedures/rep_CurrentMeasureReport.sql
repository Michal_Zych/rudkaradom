﻿
CREATE  PROCEDURE [dbo].[rep_CurrentMeasureReport]
	@UserReportID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @precision int = 1
	DECLARE
		@date datetime
		,@hyperPermission bit

	SELECT @date = DateFrom
		, @hyperPermission = COALESCE(HyperPermission, 0)
	FROM dbo.UsersReports ur WITH(NOLOCK)
	WHERE ur.Id = @UserReportID


	DECLARE
		@result TABLE(
			SensorId smallint
			,MeasurementTime datetime
			,Value float
			,Name nvarchar(100)
			,Location nvarchar(500)
			,MU nvarchar(5)
			,IsBitSensor bit
			,LoStateDesc nvarchar(10)
			,HiStateDesc nvarchar(10))

	INSERT @result
	SELECT s.Id AS SensorId
		, CASE WHEN @hyperPermission = 1 THEN m.HyperMesurementTime ELSE m.MeasurementTime END AS MeasurementTime
		, CASE WHEN @hyperPermission = 1 THEN ROUND(m.HyperValue * s.Scale, @precision) ELSE ROUND(m.Value * s.scale, @precision) END AS Value
		, u.Name AS Name
		, dbo.GetUnitLocation(u.ID, default, 1, default ) AS Location
		, s.MU AS MU
		, s.IsBitSensor
		, s.LoStateDesc
		, s.HiStateDesc
	FROM dbo.MeasurementsCurrent m WITH(NOLOCK)
	JOIN dbo.UserReportSensors r WITH(NOLOCK) ON m.SensorID = r.SensorID
	JOIN dbo.Sensors s WITH(NOLOCK) ON s.Id = r.SensorID
	JOIN dbo.Units u WITH(NOLOCK) ON u.ID = s.UnitId
	WHERE r.UserReportID = @UserReportID
	
	SELECT
		SensorId
		,MeasurementTime
		,CASE WHEN IsBitSensor = 1 THEN
					CASE WHEN Value = 0 THEN LoStateDesc ELSE HiStateDesc END		
		 ELSE REPLACE(CONVERT(nvarchar(10), Value), '.', ',') END AS Value
		,Name
		,Location
		,CASE WHEN IsBitSensor = 1 THEN '' ELSE MU END AS MU
	FROM @result


END


