﻿

-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Alarms_GetNotificationsToSend]
	@Type int
	,@MaxRows int = 20
	,@MaxSmsLen int = 80
AS
BEGIN
	SET NOCOUNT ON;
		
	DECLARE			--typy powiadomień
		  @GUI			tinyint = 0
		, @SMS			tinyint = 1
		, @EMAIL		tinyint = 2
	DECLARE @now datetime = [dbo].[GetCurrentDate]()
	DECLARE @hyperAdmin int = 4	
		,@NO_COMMUNICATION tinyint = 3


	IF(@Type NOT IN(@SMS, @EMAIL))
	BEGIN
		SELECT null AS ID, null AS UserID, null AS Message, null AS Address
		RETURN
	END

	DECLARE @notifications TABLE(
		ID int
		,SensorId int
		,MeasuremetTime datetime
		,IsRangeAlarm bit
		,ValueStr nvarchar(10)
		,LoRangeStr nvarchar(10)
		,UpRangeStr nvarchar(10)
		)

	INSERT @notifications
	SELECT an.ID, an.SensorID 
		,mc.HyperMesurementTime
		,CASE WHEN a.EventType < @NO_COMMUNICATION THEN 1 ELSE 0 END
		,CASE WHEN s.IsBitSensor = 1 AND mc.HyperValue = 0 THEN s.LoStateDesc
			  WHEN s.IsBitSensor = 1 AND mc.HyperValue <> 0 THEN s.HiStateDesc
			ELSE REPLACE(CONVERT(nvarchar(20), mc.HyperValue * s.Scale), '.', ',')
		  END
		,CASE WHEN s.IsBitSensor = 1 AND a.LoRange = 0 THEN s.LoStateDesc
			  WHEN s.IsBitSensor = 1 AND a.LoRange <> 0 THEN s.HiStateDesc
			ELSE REPLACE(CONVERT(nvarchar(20), a.LoRange * s.Scale), '.', ',')
		  END
		,CASE WHEN s.IsBitSensor = 1 AND a.UpRange = 0 THEN s.LoStateDesc
			  WHEN s.IsBitSensor = 1 AND a.UpRange <> 0 THEN s.HiStateDesc
			ELSE REPLACE(CONVERT(nvarchar(20), a.UpRange * s.Scale), '.', ',')
		  END
	FROM dbo.AlarmNotifications an
	JOIN dbo.Alarms a WITH(NOLOCK) on an.AlarmId = a.Id
	JOIN dbo.MeasurementsCurrent mc  WITH(NOLOCK) on mc.SensorID = an.SensorID
	JOIN dbo.Sensors s  WITH(NOLOCK) on s.Id = an.SensorID
	WHERE an.SendAfterDate <= @now
		AND an.SentDate IS NULL
		AND an.Type = @Type
		AND an.DeleteDate IS NULL

	
	BEGIN TRANSACTION
		BEGIN TRY	
			--wstaw do powiadomienia treść wiadomości 
			UPDATE a SET
				a.Message = CASE 
								WHEN a.Message IS NULL THEN dbo.FormatAlarmNotifyText(a.SensorId, @Type, n.MeasuremetTime, null, n.IsRangeAlarm, n.ValueStr, n.LoRangeStr, n.UpRangeStr, null)
								ELSE a.Message
							END
				,a.SentDate = @now
				,a.Info = a.Info + ' | ' + CONVERT(nvarchar(30), @now, 121) + ':alarms_GetNotificationsToSend(Ustawiono Message, SentDate)'
			FROM dbo.AlarmNotifications a
			JOIN @notifications n ON a.ID = n.ID
			--wyznacz userów
			IF(@Type = @SMS)
			BEGIN
				INSERT dbo.AlarmNotificationsLog(NotificationID, ReceiverID, Address)
				SELECT n.ID, nbs.UserId, u.Phone
				FROM @notifications n 
					JOIN dbo.AlarmNotifyBySms nbs WITH(NOLOCK) ON nbs.SensorId = n.SensorID
					JOIN dbo.Users u WITH(NOLOCK) ON u.ID = nbs.UserId
			END
			ELSE IF(@Type = @EMAIL)
			BEGIN
				INSERT dbo.AlarmNotificationsLog(NotificationID, ReceiverID, Address)
				SELECT n.ID, nbs.UserId, u.eMail
				FROM @notifications n 
					JOIN dbo.AlarmNotifyByEmail nbs WITH(NOLOCK) ON nbs.SensorId = n.SensorID
					JOIN dbo.Users u WITH(NOLOCK) ON u.ID = nbs.UserId
			END
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
		END CATCH

		DECLARE @messages TABLE(ID int, UserID int, Message nvarchar(max), Address nvarchar(250))
		INSERT @messages
		SELECT TOP(@maxRows)
			nl.NotificationID AS ID
			,nl.ReceiverID AS UserID
			, CASE
				WHEN ur.RoleID = @hyperAdmin THEN n.Message + ' ' + dbo.GetSensorHardwarePath(n.SensorID, @Type)
				ELSE n.Message
			  END AS  Message
			, nl.Address
		FROM dbo.AlarmNotificationsLog nl WITH(NOLOCK)
			JOIN dbo.AlarmNotifications n WITH(NOLOCK) ON n.ID = nl.NotificationID AND n.SendAfterDate <= @now AND n.Type = @Type AND n.DeleteDate IS NULL
			LEFT JOIN dbo.UsersRoles ur WITH(NOLOCK) ON ur.UserID = nl.ReceiverID
		WHERE nl.SentDate IS NULL
			AND nl.DeleteDate IS NULL


	IF @Type = @SMS
	BEGIN
		;
		WITH t AS (
			SELECT Id, UserId, Address, Message
			FROM @messages
			),
			const AS (
				SELECT @MaxSmsLen as lineLen
			),
			splitLines AS (
				SELECT Id, UserId, Address, left(Message, lineLen) as part, SUBSTRING(Message, lineLen + 1, LEN(Message)) as rest, 1 AS PartNr
				FROM t CROSS JOIN const
				UNION ALL
				SELECT Id, UserId, Address, LEFT(rest, linelen) as part, SUBSTRING(rest, lineLen + 1, LEN(rest)), PartNr + 1
				FROM splitLines CROSS JOIN const
				WHERE LEN(rest) > 0
		)
		SELECT Id, UserId, Part, Address
		FROM splitLines
		ORDER BY Id, PartNr;
	END
	ELSE
		SELECT * FROM @messages



END


