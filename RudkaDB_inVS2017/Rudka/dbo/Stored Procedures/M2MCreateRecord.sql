﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

					
CREATE PROCEDURE [dbo].[M2MCreateRecord]
	@dzial nvarchar(100)
	,@budynek nvarchar(100)
	,@pietro nvarchar(100)
	,@pokoj nvarchar(100)
	,@nazwa nvarchar(100)
	,@nazwaCzujnika nvarchar(100)
	,@socketName nvarchar(100)
	,@ipaddress nvarchar(100)
	,@channel int
	,@slaveAddress int
	,@lorange int
	,@uprange int
	,@calibration real=0
	,@rootid int=1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		BEGIN TRY
			declare @tab table( value int)
			declare @dzialId int
			declare @budynekId int
			declare @pietroId int
			declare @pokojId int
			declare @urzadzenieId int
			declare @sensorUnitId int
			declare @sensorId int
			declare @socketId int
			declare @HubId int
			declare @unitType int
			declare @unitType2 int
			declare @deviceId int
			declare @deviceModel int=0
			declare @scale float=0.1

			set @dzialId= (select top(1) id from units where name=@dzial and ParentUnitID=@rootid)
			delete from @tab
			if(@dzialId is null)
			begin
				insert into @tab exec [dbo].[unt_UnitCreate] @dzial,@rootid,1,3,null,null,0,null,null
				set @dzialId=(select top(1) value from @tab)
			end

			set @budynekId= (select top(1) id from units where name=@budynek and ParentUnitID=@dzialId)
			delete from @tab
			if(@budynekId is null)
			begin
				insert into @tab exec [dbo].[unt_UnitCreate] @budynek,@dzialId,1,2,null,null,0,null,null
				set @budynekId=(select top(1) value from @tab)
			end

			set @pietroId= (select top(1) id from units where name=@pietro and ParentUnitID=@budynekId)
			delete from @tab
			if(@pietroId is null)
			begin
				insert into @tab exec [dbo].[unt_UnitCreate] @pietro,@budynekId,1,10,null,null,0,null,null
				set @pietroId=(select top(1) value from @tab)
			end

			set @pokojId= (select top(1) id from units where name=@pokoj and ParentUnitID=@pietroId)
			delete from @tab
			if(@pokojId is null)
			begin
				insert into @tab exec [dbo].[unt_UnitCreate] @pokoj,@pietroId,1,13,null,null,0,null,null
				set @pokojId=(select top(1) value from @tab)
			end


			if(exists(select 1 where @nazwa like '%TH[0-9][0-9][W,P]%')) 
			begin
				if(exists(select 1 where @nazwa like '%TH[0-9][0-9]W%')) 
					set @unitType2=19
				else
				if(exists(select 1 where @nazwa like '%TH[0-9][0-9]P%')) 
					set @unitType2=20
				
					set @nazwa=SUBSTRING(@nazwa,0,Len(@nazwa));
			end
				

			set @urzadzenieId= (select top(1) id from units where name=@nazwa and ParentUnitID=@pokojId)
			delete from @tab
			if(@urzadzenieId is null)
			begin
				if(exists(select 1 where @nazwa like '%LW %')) 
					set @unitType=5
				else
				if(exists(select 1 where @nazwa like '%I[0-9]%')) 
					set @unitType=5
				else
				if(exists(select 1 where @nazwa like '%L[0-9]%')) 
					set @unitType=5
				else
				if(exists(select 1 where @nazwa like '%Z[0-9]%')) 
					set @unitType=21
				else
				if(exists(select 1 where @nazwa like '%TH[0-9][0-9]%')) 
					set @unitType=14
				else
				if(exists(select 1 where @nazwa like '%CH[0-9]%')) 
					set @unitType=7
				else
				if(exists(select 1 where @nazwa like '%M[0-9]%')) 
					set @unitType=7
				else
				if(exists(select 1 where @nazwa like '%R[0-9]%')) 
					set @unitType=13
				else
				if(exists(select 1 where @nazwa like '%C[0-9]%')) 
					set @unitType=15			
				insert into @tab exec [dbo].[unt_UnitCreate] @nazwa,@pokojId,1,@unitType,null,null,0,null,null
				set @urzadzenieId=(select top(1) value from @tab)
			end
				if(exists(select 1 where @nazwaCzujnika like '%LW %')) 
				begin
					set @unitType2=20
					set @deviceModel=5
					set @scale=0.01
				end
				else
				if(exists(select 1 where @nazwaCzujnika like '%I[0-9]%')) 
					set @unitType2=20
				else
				if(exists(select 1 where @nazwaCzujnika like '%L[0-9]%')) 
					set @unitType2=20
				else
				if(exists(select 1 where @nazwaCzujnika like '%Z[0-9]%')) 
					set @unitType2=20
				else
				if(exists(select 1 where @nazwaCzujnika like '%TH[0-9][0-9]W%')) 
					set @unitType2=19
				else
				if(exists(select 1 where @nazwaCzujnika like '%TH[0-9][0-9]P%')) 
					set @unitType2=20
				else
				if(exists(select 1 where @nazwaCzujnika like '%CH[0-9]%')) 
					set @unitType2=20
				else
				if(exists(select 1 where @nazwaCzujnika like '%M[0-9]%')) 
					set @unitType2=20
				else
				if(exists(select 1 where @nazwaCzujnika like '%R[0-9]%')) 
					set @unitType2=20
				else
				if(exists(select 1 where @nazwaCzujnika like '%C[0..9]%')) 
					set @unitType2=20

			set @sensorUnitId= (select top(1) id from units where name=@nazwaCzujnika and ParentUnitID=@urzadzenieId)
			delete from @tab
			if(@sensorUnitId is null)
			begin	
				insert into @tab exec [dbo].[unt_UnitCreate] @nazwaCzujnika,@urzadzenieId,1,@unitType2,null,null,0,null,null
				set @sensorUnitId=(select top(1) value from @tab)
			end	

			set @sensorId=(select top(1) id from sensors where unitid=@sensorUnitId)

			if exists(select 1 from hubs where IPAddress=@ipAddress and active=1)
			begin
				set @hubid=(select top(1) id from hubs where IPAddress=@ipAddress and active=1)
			end else begin
				insert into hubs values(@ipaddress,1001,1)

				set @hubid=SCOPE_IDENTITY()

			end

			if(@deviceModel=0)
			begin
				if(@unitType2=20)
					set @deviceModel=1
				else
				if(@unitType2=19)
					set @deviceModel=2
			end
			
			set @deviceId= (select top(1) id from devices where address=@slaveAddress and active=1 and hubid=@hubid)
			if(@deviceId is null)
			begin
				INSERT INTO [dbo].[Devices]
			   ([Address]
			   ,[MaxChannelCount]
			   ,[DeviceModel]
			   ,[HubId])
				VALUES(@slaveAddress,0,@deviceModel,@hubId)
				set @deviceId=SCOPE_IDENTITY()
			end
			set @socketid= (select top(1) id from Sockets where name=@socketName and broken=0)
			if(@socketid is null)
			begin
				INSERT INTO [dbo].[Sockets]
			   ([Name]
			   ,[LoRange]
			   ,[HiRange]
			   ,[LoRangeH]
			   ,[HiRangeH]
			   ,[DeviceId]
			   ,[DeviceChannelNumber]
			   ,[Broken]
			   )
				 VALUES
				 (
				 @socketName,-2000,500,-2000,500,@deviceid,@channel-1,0
				 )

			set @socketid=scope_identity()
			update devices set MaxChannelCount=MaxChannelCount+1 where id=@deviceid
			
	 end else 
	 begin
		update Sockets set LoRange=-2000,[HiRange]=500,[LoRangeH]=-2000,[HiRangeH]=500,[DeviceId]=@deviceid,[DeviceChannelNumber]=@channel-1,[Broken]=0 where id=@socketid

		update devices set MaxChannelCount=p.maxchannel
		from devices d
		join (select deviceid, max([DeviceChannelNumber])+1 as maxchannel from sockets group by DeviceId ) p on p.DeviceId=d.Id
		
	 end

	 
	 update sensors set SocketID=@socketid,[Enabled]=0,scale=@scale,LoRange=@lorange/@scale,UpRange=@uprange/@scale, Calibration=@calibration/@scale,AlDisabled=1 where id=@sensorId
	 
	 update MeasurementRanges set LoRange=@lorange/@scale,UpRange=@uprange/@scale where sensorid=@sensorId

		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			SELECT
    ERROR_NUMBER() AS ErrorNumber
    ,ERROR_SEVERITY() AS ErrorSeverity
    ,ERROR_STATE() AS ErrorState
    ,ERROR_PROCEDURE() AS ErrorProcedure
    ,ERROR_LINE() AS ErrorLine
    ,ERROR_MESSAGE() AS ErrorMessage;
			RETURN
		END CATCH

	COMMIT TRANSACTION
END

