﻿/************************************************
	procedura pobierająca dane dla crt.GeneralizedSensorData
************************************************/
CREATE PROCEDURE [dbo].[crt_GetSensorData]
	@sensorID smallint,
	@dateFrom datetime,
	@dateTo datetime
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT MeasurementTime, Value
	FROM dbo.Measurements
	WHERE SensorID = @sensorID
		AND MeasurementTime BETWEEN @dateFrom AND @dateTo
	ORDER BY MeasurementTime
	
END
