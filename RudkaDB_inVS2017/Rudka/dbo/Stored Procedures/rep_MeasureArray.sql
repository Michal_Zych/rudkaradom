﻿CREATE PROCEDURE dbo.[rep_MeasureArray]
	@UserReportId int
	,@range int
AS
BEGIN
	DECLARE @dateFrom datetime
    DECLARE @dateTo datetime

    SELECT @dateFrom = DateFrom, @dateTo = DateTo FROM UsersReports WHERE ID = @UserReportID 

	SELECT SensorId, Date, Value0h, Value1h, Value2h, Value3h, Value4h, Value5h, Value6h, Value7h, Value8h, Value9h, Value10h, Value11h, Value12h, Value13h, Value14h, Value15h, Value16h, Value17h, Value18h, Value19h, Value20h, Value21h, Value22h, Value23h
	FROM 
	(
		SELECT m.SensorID, CONVERT(datetime, CONVERT(date, m.MeasurementTime)) as Date, 'Value' + CONVERT(nvarchar(10), DATEPART(HOUR, MeasurementTime)) + 'h' as Hour, AVG(m.Value) as Value
		FROM dbo.Measurements m WITH(NOLOCK)
		JOIN dbo.UserReportSensors rs WITH(NOLOCK) ON m.SensorID = rs.SensorID
		WHERE rs.UserReportID = @UserReportID AND MeasurementTime BETWEEN @dateFrom AND @dateTo
		GROUP BY m.SensorID, CONVERT(date, m.MeasurementTime), DATEPART(HOUR, m.MeasurementTime)
	) d
	PIVOT
	(
		MAX(d.Value)
		FOR d.hour in (Value0h, Value1h, Value2h, Value3h, Value4h, Value5h, Value6h, Value7h, Value8h, Value9h, Value10h, Value11h, Value12h, Value13h, Value14h, Value15h, Value16h, Value17h, Value18h, Value19h, Value20h, Value21h, VAlue22h, Value23h)
	) piv
	WHERE COALESCE(Value0h, Value1h, Value2h, Value3h, Value4h, Value5h, Value6h, Value7h, Value8h, Value9h, Value10h, Value11h, Value12h, Value13h, Value14h, Value15h, Value16h, Value17h, Value18h, Value19h, Value20h, Value21h, VAlue22h, Value23h) IS NOT NULL
	ORDER BY SensorID, Date
END