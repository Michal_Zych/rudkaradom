﻿CREATE PROCEDURE [dbo].[ev_StoreMovingStatus_old]
	@BandId smallint
	,@EventDateUtc dateTime = NULL
	,@IsMoving bit = 1
AS
BEGIN
	DECLARE @OBJECT_TYPE_BAND dbo.OBJECT_TYPE
	
	DECLARE
		@currentDateUtc datetime = dbo.GetCurrentUtcDate()
		,@bandStatusChanged bit = 0
		,@isDay bit = 0

	IF @EventDateUtc IS NULL SET @EventDateUtc = @currentDateUtc

BEGIN -- zmienne konfiguracji
	DECLARE
		 @objectType				dbo.OBJECT_TYPE   
		,@objectId					dbo.OBJECT_ID  
		,@objectUnitId				int  
		,@currentUnitId				int  
		,@inCurrentUnitUtc			dateTime  
		,@previousUnitId			int  
		,@inPreviousUnitUtc			dateTime  
		,@topAlarmId				int  
		,@topAlarmType				dbo.EVENT_TYPE   
		,@topAlarmSeverity			dbo.EVENT_SEVERITY   
		,@topAlarmDateUtc			dateTime 
		,@topEventDateUtc			dateTime  
		,@severity1Count			int  
		,@severity2Count			int  
		,@ongNoGoZoneInEventId		int  
		,@ongNoGoZoneInAlarmId		int  
		,@ongDisconnectedEventId	int  
		,@ongDisconnectedAlarmId	int  
		,@ongZoneOutEventId			int  
		,@ongZoneOutAlarmId			int  
		,@ongNoMovingEventId		int  
		,@ongNoMovingAlarmId		int  
		,@ongMovingEventId			int  
		,@ongMovingAlarmId			int  
		,@ongLowBatteryEventId		int  
		,@ongLowBatteryAlarmId		int 

		,@dMoveWrActive				bit     
		,@dMoveWrMins				smallint
		,@dMoveAlActive				bit     
		,@dMoveAlMins				smallint
		,@dNoMoveWrActive			bit     
		,@dNoMoveWrMins				smallint
		,@dNoMoveAlActive			bit     
		,@dNoMoveAlMins				smallint
		,@nMoveWrActive				bit     
		,@nMoveWrMins				smallint
		,@nMoveAlActive				bit     
		,@nMoveAlMins				smallint
		,@nNoMoveWrActive			bit     
		,@nNoMoveWrMins				smallint
		,@nNoMoveAlActive			bit     
		,@nNoMoveAlMins				smallint

		,@warningAutoCloseMessage	nvarchar(max)
		,@fullMonitoringUnassignedBands bit
		,@dayStartHour				time 
		,@dayEndHour				time
		,@currentEventType			dbo.EVENT_TYPE

END

BEGIN	-- pobranie konfiguracji
	SELECT
		 @objectType					= bs.ObjectType			
		,@objectId						= bs.ObjectId				
		,@objectUnitId					= bs.ObjectUnitId			
		,@currentUnitId					= bs.CurrentUnitId			
		,@inCurrentUnitUtc				= bs.InCurrentUnitUtc		
		,@previousUnitId				= bs.PreviousUnitId		
		,@inPreviousUnitUtc				= bs.InPreviousUnitUtc		
		,@topAlarmId					= bs.TopAlarmId			
		,@topAlarmType					= bs.TopAlarmType			
		,@topAlarmSeverity				= bs.TopAlarmSeverity		  
		,@topAlarmDateUtc				= bs.TopAlarmDateUtc		
		,@topEventDateUtc				= bs.TopEventDateUtc		
		,@severity1Count				= bs.Severity1Count		
		,@severity2Count				= bs.Severity2Count		
		,@ongNoGoZoneInEventId			= bs.OngNoGoZoneInEventId  
		,@ongNoGoZoneInAlarmId			= bs.OngNoGoZoneInAlarmId  
		,@ongDisconnectedEventId		= bs.OngDisconnectedEventId
		,@ongDisconnectedAlarmId		= bs.OngDisconnectedAlarmId
		,@ongZoneOutEventId				= bs.OngZoneOutEventId		
		,@ongZoneOutAlarmId				= bs.OngZoneOutAlarmId		
		,@ongNoMovingEventId			= bs.OngNoMovingEventId	
		,@ongNoMovingAlarmId			= bs.OngNoMovingAlarmId	
		,@ongMovingEventId				= bs.OngMovingEventId		
		,@ongMovingAlarmId				= bs.OngMovingAlarmId		
		,@ongLowBatteryEventId			= bs.OngLowBatteryEventId  
		,@ongLowBatteryAlarmId			= bs.OngLowBatteryAlarmId  
		
		,@dMoveWrActive					= COALESCE(u.DMoveWrActive	, p.DMoveWrActive	, hu.DMoveWrActive	)
		,@dMoveWrMins					= COALESCE(u.DMoveWrMins	, p.DMoveWrMins		, hu.DMoveWrMins	)
		,@dMoveAlActive					= COALESCE(u.DMoveAlActive	, p.DMoveAlActive	, hu.DMoveAlActive	)	
		,@dMoveAlMins					= COALESCE(u.DMoveAlMins	, p.DMoveAlMins		, hu.DMoveAlMins	)
		,@dNoMoveWrActive				= COALESCE(u.DNoMoveWrActive, p.DNoMoveWrActive	, hu.DNoMoveWrActive)
		,@dNoMoveWrMins					= COALESCE(u.DNoMoveWrMins	, p.DNoMoveWrMins	, hu.DNoMoveWrMins	)	
		,@dNoMoveAlActive				= COALESCE(u.DNoMoveAlActive, p.DNoMoveAlActive	, hu.DNoMoveAlActive)
		,@dNoMoveAlMins					= COALESCE(u.DNoMoveAlMins	, p.DNoMoveAlMins	, hu.DNoMoveAlMins	)	
		,@nMoveWrActive					= COALESCE(u.NMoveWrActive	, p.NMoveWrActive	, hu.NMoveWrActive	)	
		,@nMoveWrMins					= COALESCE(u.NMoveWrMins	, p.NMoveWrMins		, hu.NMoveWrMins	)
		,@nMoveAlActive					= COALESCE(u.NMoveAlActive	, p.NMoveAlActive	, hu.NMoveAlActive	)	
		,@nMoveAlMins					= COALESCE(u.NMoveAlMins	, p.NMoveAlMins		, hu.NMoveAlMins	)
		,@nNoMoveWrActive				= COALESCE(u.NNoMoveWrActive, p.NNoMoveWrActive	, hu.NNoMoveWrActive)
		,@nNoMoveWrMins					= COALESCE(u.NNoMoveWrMins	, p.NNoMoveWrMins	, hu.NNoMoveWrMins	)	
		,@nNoMoveAlActive				= COALESCE(u.NNoMoveAlActive, p.NNoMoveAlActive	, hu.NNoMoveAlActive)
		,@nNoMoveAlMins					= COALESCE(u.NNoMoveAlMins	, p.NNoMoveAlMins	, hu.NNoMoveAlMins	)	
		
		,@warningAutoCloseMessage		= st.WarningAutoCloseMessage
		,@fullMonitoringUnassignedBands = st.FullMonitoringUnassignedBands
		,@dayStartHour					= st.DayStartHour
		,@dayEndHour					= st.DayEndHour

		,@currentEventType				= CASE WHEN @IsMoving = 1 THEN eventType.Moving ELSE eventType.NoMoving END
		,@OBJECT_TYPE_BAND				= objectType.Band
	FROM dbo.ev_Settings st 
		 JOIN enum.EventType eventType  ON 1 = 1
		 JOIN enum.ObjectType objectType ON 1 = 1
		 LEFT JOIN dbo.ev_BandStatus bs ON bs.BandId = @BandId
		 LEFT JOIN dbo.al_ConfigPatients p ON p.PatientId = bs.ObjectId AND bs.ObjectType = objectType.Patient
		 LEFT JOIN dbo.al_ConfigUnits u ON u.UnitId = bs.CurrentUnitId AND u.MoveSettingsPriority = 1 AND bs.ObjectType = objectType.Patient
		 LEFT JOIN dbo.al_ConfigUnits hu ON hu.UnitId = bs.ObjectUnitId AND bs.ObjectType <> objectType.Patient
END	

	--zakończ brak łączności
	IF @ongDisconnectedEventId IS NOT NULL
	BEGIN
		EXEC [dbo].[ev_SetBandConnected]
			@BandId						= @BandId
			,@currentDateUtc			= @currentDateUtc
			,@CurrentEventDateUtc		= @EventDateUtc
			,@CurrentEventUnitId		= @currentUnitId
			,@ObjectId					= @objectId				
			,@ObjectType				= @objectType
			,@ObjectUnitId				= @objectUnitId
			,@CurrentUnitId				= @currentUnitId			OUTPUT
			,@InCurrentUnitUtc			= @inCurrentUnitUtc			OUTPUT
			,@PreviousUnitId			= @previousUnitId			OUTPUT
			,@InPreviousUnitUtc			= @inPreviousUnitUtc		OUTPUT
			,@OngDisconnectedEventId	= @ongDisconnectedEventId	OUTPUT
			,@OngDisconnectedAlarmId	= @ongDisconnectedAlarmId	OUTPUT
		SET @bandStatusChanged = 1
		
		-- powrót do poprzedniego unita - ponów odczyt konfiguracji dla unita
		SELECT
			 @dMoveWrActive					= COALESCE(u.DMoveWrActive	, @dMoveWrActive	)
			,@dMoveWrMins					= COALESCE(u.DMoveWrMins	, @dMoveWrMins		)
			,@dMoveAlActive					= COALESCE(u.DMoveAlActive	, @dMoveAlActive	)	
			,@dMoveAlMins					= COALESCE(u.DMoveAlMins	, @dMoveAlMins		)
			,@dNoMoveWrActive				= COALESCE(u.DNoMoveWrActive, @dNoMoveWrActive	)
			,@dNoMoveWrMins					= COALESCE(u.DNoMoveWrMins	, @dNoMoveWrMins	)	
			,@dNoMoveAlActive				= COALESCE(u.DNoMoveAlActive, @dNoMoveAlActive	)
			,@dNoMoveAlMins					= COALESCE(u.DNoMoveAlMins	, @dNoMoveAlMins	)	
			,@nMoveWrActive					= COALESCE(u.NMoveWrActive	, @nMoveWrActive	)	
			,@nMoveWrMins					= COALESCE(u.NMoveWrMins	, @nMoveWrMins		)
			,@nMoveAlActive					= COALESCE(u.NMoveAlActive	, @nMoveAlActive	)	
			,@nMoveAlMins					= COALESCE(u.NMoveAlMins	, @nMoveAlMins		)
			,@nNoMoveWrActive				= COALESCE(u.NNoMoveWrActive, @nNoMoveWrActive	)
			,@nNoMoveWrMins					= COALESCE(u.NNoMoveWrMins	, @nNoMoveWrMins	)	
			,@nNoMoveAlActive				= COALESCE(u.NNoMoveAlActive, @nNoMoveAlActive	)
			,@nNoMoveAlMins					= COALESCE(u.NNoMoveAlMins	, @nNoMoveAlMins	)	
		FROM dbo.al_ConfigUnits u 
			,enum.ObjectType objectType
		WHERE u.UnitId = @currentUnitId AND u.MoveSettingsPriority = 1 AND @objectType = objectType.Patient
	END

	IF @objectType = @OBJECT_TYPE_BAND AND @fullMonitoringUnassignedBands = 0 RETURN

	-- zarejestrowanie rekordu statusu dla opaski
	IF @objectId IS NULL
	BEGIN
		EXEC dbo.ev_AssignBandToItself
			@EventDateUtc		= @EventDateUtc
			,@BandId			= @BandId
			,@ObjectId			= @objectId			OUTPUT
			,@ObjectType		= @objectType		OUTPUT
			,@InCurrentUnitUtc	= @inCurrentUnitUtc	OUTPUT
			,@severity1Count	= @severity1Count	OUTPUT
			,@severity2Count	= @severity2Count	OUTPUT
	END


	-- obliczenia na ustawieniach dziennych
	SET @isDay = dbo.IsUtcDateTimeInDay(@EventDateUtc, @dayStartHour, @dayEndHour)
	-- jeśli noc to do dziennych wpisz nocne
	IF @isDay = 0 
		SELECT 
			 @dMoveWrActive		= @nMoveWrActive		
			,@dMoveWrMins		= @nMoveWrMins		
			,@dMoveAlActive		= @nMoveAlActive		
			,@dMoveAlMins		= @nMoveAlMins		
			,@dNoMoveWrActive	= @nNoMoveWrActive	
			,@dNoMoveWrMins		= @nNoMoveWrMins		
			,@dNoMoveAlActive	= @nNoMoveAlActive	
			,@dNoMoveAlMins		= @nNoMoveAlMins		

	IF (@ongMovingEventId IS NULL AND @IsMoving = 1)
		OR (@ongNoMovingEventId IS NULL AND @IsMoving = 0)
	BEGIN
		SET @bandStatusChanged = 1
		DECLARE @openedEventId int
			,@warningActive bit
			,@warningDelayMins smallint
			,@alarmActive bit
			,@alarmDelayMins smallint	
			,@ongoingAlarmSeverity dbo.EVENT_SEVERITY
			,@ongoingAlarmId int
			,@warningDateUtc dateTime
			,@alarmDateUtc dateTime
			,@toCloseRaisingEvent int
			,@toCloseAlarmId int
		IF @IsMoving = 1
			SELECT  @warningActive = @dMoveWrActive, @warningDelayMins = @dMoveWrMins, @alarmActive = @dMoveAlActive, @alarmDelayMins = @dMoveAlMins,
				@toCloseRaisingEvent = @ongNoMovingEventId, @toCloseAlarmId = @ongNoMovingAlarmId
		ELSE
			SELECT @warningActive = @dNoMoveWrActive, @warningDelayMins = @dNoMoveWrMins, @alarmActive = @dNoMoveAlActive, @alarmDelayMins = @dNoMoveAlMins,
				@toCloseRaisingEvent = @ongMovingEventId, @toCloseAlarmId = @ongMovingAlarmId

		EXEC dbo.ev_GenerateEvent
					@currentDateUtc				= @currentDateUtc
					,@EventDateUtc				= @EventDateUtc
					,@EventType					= @currentEventType
					,@EventUnitId				= @currentUnitId
					,@EventObjectId				= @objectId
					,@EventObjectType			= @objectType
					,@EventObjectUnitId			= @objectUnitId
					,@EventData					= null
					,@IsLongEvent				= 1
					,@FinishedEventId			= @toCloseRaisingEvent	OUTPUT
					,@FinishedAlarmId			= @toCloseAlarmId		OUTPUT
					,@EventId					= @openedEventId		OUTPUT

		/*
		INSERT ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, EndingEventId)
		SELECT @EventDateUtc, @currentEventType, eventSeverity.Event, @currentUnitId, @objectId, @objectType, @objectUnitId, @currentDateUtc, null, null
		FROM enum.EventSeverity eventSeverity
		SET @openedEventId = SCOPE_IDENTITY()
		*/

		-- wygeneruj ostrzeżenia/alarmy
			EXEC dbo.ev_GenerateAlarmsForEvent 
				 @EventObjectType			= @OBJECT_TYPE_BAND
				,@EventObjectid				= @BandId
				,@currentDateUtc			= @currentDateUtc
				,@CurrentEventDateUtc		= @EventDateUtc
				,@currentEventId			= @openedEventId
				,@currentEventType			= @currentEventType
				,@currentUnitId				= @currentUnitId
				,@objectId					= @objectId
				,@objectType				= @objectType
				,@objectUnitId				= @objectUnitId
				,@warningCloseMessage		= @warningAutoCloseMessage
				,@WarningActive				= @warningActive	
				,@WarningDelayMins			= @warningDelayMins	
				,@AlarmActive				= @alarmActive	
				,@AlarmDelayMins			= @alarmDelayMins	
				,@CurrrentAlarmSeverity		= @ongoingAlarmSeverity OUTPUT
				,@CurrrentAlarmId			= @ongoingAlarmId OUTPUT
				,@warningDateUtc			= @warningDateUtc OUTPUT
				,@alarmDateUtc				= @alarmDateUtc	OUTPUT
				,@severity1Count			= @severity1Count OUTPUT
				,@severity2Count			= @severity2Count OUTPUT
		/*
		-- skasuj alarmy do podniesiena
		DELETE dbo.ev_EventsToRaise WHERE BandId = @BandId AND RaisingEventId = @toCloseRaisingEvent
		-- zakończ zdarzenia/alarmy do zakończenia
		UPDATE dbo.ev_Events SET
			EndDateUtc = @EventDateUtc
			,EndingEventId = @openedEventId
		WHERE Id IN(@toCloseRaisingEvent, @toCloseAlarmId)
		*/
		--przypisz zmienione wartości do odpowiednich zdarzeń
		IF @IsMoving = 1
			SELECT @ongMovingEventId = @openedEventId, @ongMovingAlarmId = @ongoingAlarmId, @ongNoMovingEventId = null, @ongNoMovingAlarmId = null
		ELSE
			SELECT @ongNoMovingEventId = @openedEventId, @ongNoMovingAlarmId = @ongoingAlarmId, @ongMovingEventId = null, @ongMovingAlarmId = null
	END

	-- uaktualnij status
	IF @bandStatusChanged = 1
		EXEC dbo.ev_GenerateBandStatus
			 @BandId					= @BandId 
			,@CurrentUnitId 			= @CurrentUnitId 
			,@InCurrentUnitUtc			= @InCurrentUnitUtc
			,@PreviousUnitId 			= @PreviousUnitId 
			,@InPreviousUnitUtc 		= @InPreviousUnitUtc 
			,@TopAlarmId 				= @TopAlarmId 
			,@TopAlarmType  			= @TopAlarmType  
			,@TopAlarmSeverity 			= @TopAlarmSeverity 
			,@TopAlarmDateUtc 			= @TopAlarmDateUtc 
			,@TopEventDateUtc 			= @TopEventDateUtc 
			,@Severity1Count 			= @Severity1Count 
			,@Severity2Count 			= @Severity2Count 
			,@OngNoGoZoneInEventId 		= @OngNoGoZoneInEventId 
			,@OngNoGoZoneInAlarmId  	= @OngNoGoZoneInAlarmId  
			,@OngDisconnectedEventId  	= @OngDisconnectedEventId 
			,@OngDisconnectedAlarmId  	= @OngDisconnectedAlarmId 
			,@OngZoneOutEventId  		= @OngZoneOutEventId  
			,@OngZoneOutAlarmId  		= @OngZoneOutAlarmId  
			,@OngNoMovingEventId  		= @OngNoMovingEventId  
			,@OngNoMovingAlarmId  		= @OngNoMovingAlarmId  
			,@OngMovingEventId  		= @OngMovingEventId  
			,@OngMovingAlarmId  		= @OngMovingAlarmId  
			,@OngLowBatteryEventId  	= @OngLowBatteryEventId  
			,@OngLowBatteryAlarmId 		= @OngLowBatteryAlarmId 
			,@currentEventId			= @openedEventId
			,@CurrentEventType 			= @currentEventType 
			,@CurrentEventDateUtc 		= @EventDateUtc 
			,@GeneratedAlarmId			= @ongoingAlarmId
			,@GeneratedAlarmSeverity 	= @ongoingAlarmSeverity 
			,@GeneratedWarningDateUtc 	= @warningDateUtc
			,@GeneratedAlarmDateUtc 	= @alarmDateUtc 
	

END