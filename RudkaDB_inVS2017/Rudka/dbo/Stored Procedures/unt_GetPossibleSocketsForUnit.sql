﻿
-- =============================================
-- Author:		
-- Create date: 
-- Description:	zwraca sockety do których możliwe jest podłączenie czujnika
--		(niewykorzystane sockety + socket do którego podłączony jest czujnik)
-- =============================================
CREATE PROCEDURE [dbo].[unt_GetPossibleSocketsForUnit]
	@unitId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	FROM
	(
	SELECT ID, Name
	FROM dbo.Sockets
	WHERE Broken = 0
		AND ID NOT IN (SELECT SocketId FROM dbo.Sensors WHERE SocketID IS NOT NULL)

	UNION

	SELECT so.ID, so.Name
	FROM dbo.Sensors s 
		JOIN dbo.Sockets so ON s.SocketID = so.Id
	WHERE s.UnitId = @unitId) A
	ORDER BY Name ASC
	
END
