﻿
CREATE PROCEDURE [dbo].[msg_GetMessageReceivers]
	@MessageId int
AS
BEGIN
	SET NOCOUNT ON;

	
	SELECT 
		r.ReceiverID AS ID
		, u.LastName AS LastName
		, u.Name AS Name
		, r.Date AS Date
		, r.Count AS [Count]
	FROM dbo.MessagesLog r
	LEFT JOIN dbo.Users u ON r.ReceiverId = u.ID
	WHERE r.MessageID = @MessageId
END
