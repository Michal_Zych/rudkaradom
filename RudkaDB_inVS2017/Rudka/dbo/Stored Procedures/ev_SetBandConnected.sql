﻿CREATE PROCEDURE [dbo].[ev_SetBandConnected]
	@BandId int
	,@currentDateUtc dateTime
	,@CurrentEventDateUtc dateTime
	,@CurrentEventUnitId int
	,@ObjectId dbo.OBJECT_ID				
	,@ObjectType dbo.OBJECT_TYPE
	,@ObjectUnitId int

	,@CurrentUnitId int				OUTPUT
	,@InCurrentUnitUtc dateTime		OUTPUT
    ,@PreviousUnitId int			OUTPUT
    ,@InPreviousUnitUtc dateTime	OUTPUT
	,@OngDisconnectedEventId int	OUTPUT
    ,@OngDisconnectedAlarmId int	OUTPUT
AS
BEGIN
	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
		BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'BandId=' + COALESCE(CONVERT(nvarchar(100), @BandId), 'NULL') 
		EXEC Log_Action @@PROCID, @info
	END
	/****************************************************/

	DECLARE 
		@UPDATE_ENDING_EVENT_ID bit = 1
		,@OBJECT_TYPE_BAND dbo.OBJECT_TYPE
		,@connectedEevntId int
		,@tempUnitId int
		,@tempDateUtc dateTime

	IF @OngDisconnectedEventId IS NULL RETURN

	SELECT @OBJECT_TYPE_BAND = Band FROM enum.ObjectType

	IF @CurrentEventUnitId IS NULL
	BEGIN
		SELECT @tempUnitId = @CurrentUnitId, @tempDateUtc = @InCurrentUnitUtc,
			@CurrentUnitId = @PreviousUnitId, @InCurrentUnitUtc = @CurrentEventDateUtc,
			@PreviousUnitId = @tempUnitId, @InPreviousUnitUtc = @tempDateUtc 
	END
	ELSE BEGIN
		SELECT @PreviousUnitId = @CurrentUnitId, @InPreviousUnitUtc = @InCurrentUnitUtc,
			@CurrentUnitId = @CurrentEventUnitId, @InCurrentUnitUtc = @CurrentEventDateUtc
	END

	-- skasuj alarmy do podniesiena
	DELETE dbo.ev_EventsToRaise WHERE RaisingObjectType = @OBJECT_TYPE_BAND AND RaisingObjectId = @BandId AND RaisingEventId = @OngDisconnectedEventId

	-- wygeneruj zdarzenie powrót łączności
	INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc) 
	SELECT @CurrentEventDateUtc, EventType.Connected, EventSeverity.Event, @CurrentUnitId, @ObjectId, @ObjectType, @ObjectUnitId, @currentDateUtc, @CurrentEventDateUtc
	FROM enum.EventType EventType, enum.EventSeverity EventSeverity
	SET @connectedEevntId = SCOPE_IDENTITY()
	IF @UPDATE_ENDING_EVENT_ID = 1
	BEGIN
		UPDATE dbo.ev_Events SET EndingEventId = @connectedEevntId WHERE Id = @connectedEevntId
	END

	-- zakończ zdarzenia/alarmy braku łączności
	UPDATE dbo.ev_Events SET
		EndDateUtc = @CurrentEventDateUtc
		,EndingEventId = @connectedEevntId
	WHERE Id IN(@OngDisconnectedEventId, @OngDisconnectedAlarmId)
	SELECT @OngDisconnectedEventId = NULL, @OngDisconnectedAlarmId = NULL
END
