﻿CREATE PROCEDURE [dbo].[dev_BandDeviceCreate] 
	@Id bigint, 
	@ClientId int,
	@Name nvarchar(16), 
	@BarCode nvarchar(50),
	@TypeId int, 
	@LayerName nvarchar(50)
AS
BEGIN
	DECLARE  @DEFAULT_INTERVAL_S int = 60


	INSERT dbo.dev_Senses(Id, ClientId, Name, BarCode, TypeId, LayerName) VALUES
	(@Id, @ClientId, @Name, @BarCode, @TypeId, @LayerName)

	INSERT dbo.dev_Sensors(SenseId, SensorNr, IntervalS, DefCorrection, IsHwCalibrated, IsEvent)
	SELECT @Id, SensorNr, @DEFAULT_INTERVAL_S, 0, 0, 0
	FROM dbo.dev_ModelSenses
	WHERE TypeId = @TypeId
	ORDER BY SensorNr

	INSERT dbo.Sockets(Name, LoRange, HiRange, LoRangeH, HiRangeH, Broken, SenseSensorId) 
	SELECT se.MacString + '-' + CONVERT(nvarchar(10), ms.SensorNr) + '-'  + ms.PhysicalUnit
		,ms.LoRange, ms.HiRange
		,0, 0
		,0
		,sr.Id
	FROM dbo.dev_ModelSenses ms
	JOIN dbo.dev_Sensors sr ON sr.SenseId = @Id AND sr.SensorNr = ms.SensorNr
	JOIN dbo.dev_Senses se ON se.Id = sr.SenseId
	WHERE ms.TypeId = @TypeId
	ORDER BY ms.SensorNr


	DECLARE @minUnitId int
	SELECT @minUnitId = COALESCE(MIN(UnitId), 0) - 1 FROM dbo.Sensors


	INSERT dbo.Sensors
	SELECT @minUnitId - soc.Id AS UnitId
		,0 AS Active
		,soc.Id AS SocketId
		,sr.IntervalS AS IntervalS
		,1 AS Scale
		,soc.LoRange AS LoRange
		,soc.HiRange AS HiRange
		,0 -- [GuiRangeDelay]
		,0 -- [GuiCommDelay]
		,0 -- [SmsEnabled]
		,0 -- [SmsRangeDelay]
		,0 -- [SmsCommDelay]
		,0 -- [MailEnabled]
		,0 -- [MailRangeDelay]
		,0 -- [MailCommDelay]
		,1 -- [AlDisabled]
		,'2017-01-01' -- [AlDisabledFrom]
		,0 -- [AlDisabledForHours]
		,null -- [AlDisableHoursFrom1]
		,null -- [AlDisableHoursTo1]
		,null -- [AlDisableHoursFrom2]
		,null -- [AlDisableHoursTo2]
		,0 -- [AlDisabledForRanges]
		,'#' -- [MU]
		,'#' -- [LegendDescription]
		,'#' -- [LegendShortcut]
		,'#' -- [LegendRangeDescription]
		,'FFFF0000' -- [LineColor]
		,1 -- [LineWidth]
		,1 -- [RangeLineWidth]
		,'#' -- [LineStyle]
		,'#' -- [RangeLineStyle]
		,'FFFF0000' -- [AlarmColor]
		,'FFFF0000' -- [AlarmCommColor]
		,0 -- [Enabled]
		,0 -- [Calibration]
		,'2017-01-01' -- [RangeValidFrom]
		,0 -- [IsBitSensor]
		,0 -- [IsHighAlarming]
		,'#' -- [HiStateDesc]
		,'#' -- [LoStateDesc]
		,0 -- [GuiRangeRepeat]
		,0 -- [GuiRangeRepeatMins]
		,0 -- [GuiRangeRepeatBreakable]
		,0 -- [GuiCommRepeat]
		,0 -- [GuiCommRepeatMins]
		,0 -- [GuiCommRepeatBreakable]
		,0 -- [SmsRangeEnabled]
		,0 -- [SmsRangeAfterFinish]
		,0 -- [SmsRangeAfterClose]
		,0 -- [SmsRangeRepeat]
		,0 -- [SmsRangeRepeatMins]
		,0 -- [SmsRangeRepeatUntil]
		,0 -- [SmsRangeRepeatBreakable]
		,0 -- [SmsCommEnabled]
		,0 -- [SmsCommAfterFinish]
		,0 -- [SmsCommAfterClose]
		,0 -- [SmsCommRepeat]
		,0 -- [SmsCommRepeatMins]
		,0 -- [SmsCommRepeatUntil]
		,0 -- [SmsCommRepeatBreakable]
		,0 -- [MailRangeEnabled]
		,0 -- [MailRangeAfterFinish]
		,0 -- [MailRangeAfterClose]
		,0 -- [MailRangeRepeat]
		,0 -- [MailRangeRepeatMins]
		,0 -- [MailRangeRepeatUntil]
		,0 -- [MailRangeRepeatBreakable]
		,0 -- [MailCommEnabled]
		,0 -- [MailCommAfterFinish]
		,0 -- [MailCommAfterClose]
		,0 -- [MailCommRepeat]
		,0 -- [MailCommRepeatMins]
		,0 -- [MailCommRepeatUntil]
		,0 -- [MailCommRepeatBreakable]
	FROM dbo.Sockets soc
	JOIN dbo.dev_Sensors sr ON sr.Id = soc.SenseSensorId AND sr.SenseId = @Id

END