﻿
CREATE PROCEDURE [dbo].[sync_ProcessRecord]
	@RecId int
	,@RecType char(4) = null
AS
BEGIN
	INSERT _log(LogDate, ProcedureName, Info)
	values (dbo.GetCurrentDate(), 'sync_ProcessRecord', @RecId)
	
	SET NOCOUNT ON;

	IF @RecType IS NULL
	SELECT @RecType = RecType FROM tech.sync_RecordsIN WHERE Id = @RecId

	DECLARE @SqlStatement nvarchar(200) = 'sync_Process_' + @RecType  
	
	IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = @SqlStatement)
	BEGIN
		SET @SqlStatement = @SqlStatement + ' ' + CONVERT(nvarchar(10), @RecId)
		EXEC sp_executeSql @SqlStatement
	END
	ELSE
	BEGIN
		UPDATE tech.sync_RecordsIN SET
			Processed = 0
			,Try = 1
			,ProcessedDateUtc = dbo.GetCurrentUTCDate()
			,Error = 'Nie znaleziono procedury ' + @SqlStatement
		WHERE Id = @RecId
		
	END
END