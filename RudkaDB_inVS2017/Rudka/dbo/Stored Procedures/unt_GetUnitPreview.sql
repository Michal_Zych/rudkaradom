﻿


CREATE PROCEDURE [dbo].[unt_GetUnitPreview]
	@unit int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		COALESCE(p.ID, -1) AS ID,
		p.Path,
		u.ID as UnitID,
		u.UnitTypeID as UnitType,
		COALESCE(u.PictureID, u.UnitTypeIdV2) AS PictureID
	FROM
		dbo.Units u
		LEFT JOIN dbo.Pictures p on p.ID = u.PreviewPictureID
	where u.ID = @unit
    
END

