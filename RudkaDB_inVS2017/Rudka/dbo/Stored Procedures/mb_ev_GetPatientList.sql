﻿-- =============================================
-- Author:		kt
-- Create date: 2017-11-07
-- Description:	gets patient list
-- =============================================
CREATE PROCEDURE [dbo].[mb_ev_GetPatientList]
  @userId int
AS
  BEGIN

    SET NOCOUNT ON;

    DECLARE @ids varchar(1000)

    DECLARE @patients TABLE(
      RowNum                     bigint,
      Id                         bigint,
      BarCode                    nvarchar(MAX),
      MacString                  nvarchar(MAX),
      Uid                        bigint,
      SerialNumber               nvarchar(MAX),
      BatteryTypeId              int,
      BatteryTypeName            nvarchar(MAX),
      BatteryAlarmSeverityId     int,
      BatteryAlarmSeverityName   nvarchar(MAX),
      BatteryInstallationDateUtc datetime,
      BatteryDaysToExchange      int,
      Description                nvarchar(MAX),
      ObjectTypeId               int,
      ObjectTypeName             nvarchar(MAX),
      ObjectDisplayName          nvarchar(MAX),
      UnitId                     int,
      UnitName                   nvarchar(MAX),
      UnitLocation               nvarchar(MAX),
      CurrentUnitId              int,
      CurrentUnitName            nvarchar(MAX),
      CurrentUnitLocation        nvarchar(MAX)
    );

	SET @ids = ''
	SELECT @ids = CONVERT(nvarchar(10), Id) + ',' + @ids FROM dbo.Units_GetUserUnits_Func(@userId)
    SET @ids = LEFT(@ids, LEN(@ids) - 1)

    INSERT INTO @patients
    EXEC [dbo].[fda_GetBandsInfoFiltered] 0, 100, @ids

    SELECT
      p.Id,
      'first-name'           AS FirstName,
      'last-name'            AS LastName,
      p.Pesel,
      tp.ObjectDisplayName   AS DisplayName,
      tp.UnitLocation        AS BaseLocation,
      tp.Id                  as BandId,
      tp.CurrentUnitLocation AS CurrentLocation,
      'parent-loc'           AS CurrentParentLocation,
      bs.InCurrentUnitUtc    AS inLocationSince,
      bs.TopAlarmId          AS AlarmId,
      bs.TopAlarmType        AS AlarmType,
      bs.TopAlarmSeverity    AS AlarmSeverity,
      bs.TopAlarmDateUtc     AS AlarmSince,
      bs.TopEventDateUtc     AS EventSince,
      tp.BarCode,
      tp.MacString           AS Mac
    FROM
      @patients tp
      LEFT JOIN dbo.ev_BandStatus bs ON bs.BandId = tp.Id
      LEFT JOIN dbo.Patients p ON bs.ObjectId = p.Id
    WHERE
      tp.ObjectTypeId = 4
    ORDER BY
      DisplayName
  END