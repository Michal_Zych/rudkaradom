﻿CREATE PROCEDURE dbo.rep_GetMeasureRangesByDay
	@UserReportID int,
	@DateFrom datetime,
	@dateTo datetime
AS
BEGIN
	DECLARE @DayRanges Table(
		DateFrom datetime,
		DateTo datetime)

	DECLARE @day date	
	SET @day = @DateFrom
	
	INSERT @DayRanges(DateFrom)
	SELECT DATEADD(DAY, N1.i + 10* N2.i + 100*N3.i, @day) as nextdays
	FROM dbo.Nums N1 WITH (NOLOCK) CROSS JOIN Nums N2 WITH (NOLOCK) CROSS JOIN Nums N3 WITH (NOLOCK)
	WHERE DATEADD(DAY, N1.i + 10* N2.i + 100*N3.i, @day) <= CAST(FLOOR(CAST( @DateTo AS FLOAT)) AS DATETIME)
	ORDER BY nextdays

	UPDATE @DayRanges
	SET DateTo = DATEADD(SECOND, -1, DATEADD(HOUR, 24, DateFrom))

	UPDATE @DayRanges 
	SET DateFrom = @DateFrom
	WHERE DateFrom = @day		    

	UPDATE @DayRanges
	SET DateTo = @DateTo
	WHERE DateTo = (SELECT MAX(DateTo) FROM @DayRanges)

-- *** Okresy obowiązywania dopuszczalnych wielkości granicznych dla sensora ***
	DECLARE @Ranges Table(
		SensorID smallint,
		LoRange smallint,
		UpRange smallint,
		DateStart datetime,
		DateEnd datetime)

	INSERT @Ranges
	SELECT r.sensorid, LoRange, UpRange, dateStart, null
	FROM dbo.MeasurementRanges r WITH(NOLOCK)
	JOIN dbo.UserReportSensors s WITH(NOLOCK) ON s.UserReportID = @UserReportId 
			AND r.SensorID = s.SensorID AND r.DateStart BETWEEN @dateFrom AND @dateTo
	--początki
	INSERT @Ranges
	SELECT r.SensorId, LoRange, UpRange, @dateFrom, null
	FROM dbo.MeasurementRanges r WITH(NOLOCK)
	JOIN dbo.UserReportSensors s WITH(NOLOCK) ON s.UserReportID = @UserReportId AND r.SensorID = s.SensorID 
		AND r.DateStart = (SELECT MAX(DateStart) FROM dbo.MeasurementRanges WHERE SensorID = r.SensorID AND DateStart < @dateFrom)
	--domknięcia przedziałów
	UPDATE r
	SET r.DateEnd = (SELECT MIN(DateStart) FROM @ranges WHERE SensorId = r.SensorID AND DateStart > r.DateStart)
	FROM @Ranges r
	--końce
	UPDATE @ranges
	SET DateEnd = @dateTo
	WHERE DateEnd IS NULL



	SELECT r.SensorID, m.DateFrom AS DateDay,
		CASE WHEN r.DateStart < m.dateFrom THEN DATEPART(HOUR, m.dateFrom)
			 ELSE DATEPART(HOUR, r.DateStart) END  [FromHour], 
		CASE WHEN r.DateEnd > m.dateTo THEN DATEPART(HOUR, m.dateTo)
			 ELSE DATEPART(HOUR, r.DateEnd) END [ToHour]
		,r.LoRange, r.UpRange
	FROM @Ranges r, @DayRanges m 
	WHERE r.DateEnd >= m.dateFrom AND r.DateStart <= m.dateTo 
	ORDER BY r.SensorID, m.dateFrom, FromHour
END

