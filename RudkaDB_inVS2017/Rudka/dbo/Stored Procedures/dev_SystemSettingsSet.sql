﻿CREATE PROCEDURE [dbo].[dev_SystemSettingsSet]
	@UserId int-- insert
	,@NoConnectionFinishingAlarms bit
	,@FullMonitoringUnassignedBands bit
	,@WarningAutoCloseMessage nvarchar(max)
	,@LoBatteryWrActive bit
	,@LoBatteryWrMins smallint
	,@LoBatteryAlActive bit
	,@LoBatteryAlMins smallint
	,@BatteryTermWrDaysBefore smallint
	,@BatteryTermWrMessage nvarchar(200)
	,@BatteryTermAlDaysBefore smallint
	,@BatteryTermAlMessage nvarchar(200)
	,@DayStartHour datetime
	,@DayEndHour datetime
	,@NoTransmitterWrActive bit
	,@NoTransmitterWrMins smallint
	,@NoTransmitterAlActive bit
	,@NoTransmitterAlMins smallint
	,@FirstBandAssignmentReason nvarchar(200)
	,@EventToArchiveAfterDays tinyint
	,@_LocalAdminPhone nvarchar(100)
	,@MaxPasswordAgeDays int
	,@ShowReasonField bit
	,@PingSessionIntervalSecs int
	,@MonitoringSoundIntervalSecs int
	,@RangeColorIntensityPercent int
	,@MaxRwosLogs int
	,@MaxRwosMessages int
	,@DefMessageActivityMinutes int
	,@MaxRowsAlarmsPopup int
	,@MaxRowsAlarms int
	,@MaxRowsRawData int
	,@RoleDefForNewUser int
	,@UnitsLocationSeparator nvarchar(50)
	,@MaxDaysReports int
	,@MaxDaysRawData int
	,@SearchMessagesDaysBefore int
	,@SearchMessagesDaysAfter int
	,@SearchLogsDaysBefore int
	,@SearchLogsDaysAfter int
	,@SearchChartDataHoursBefore int
	,@SearchChartDataHoursAfter int
	,@ChartImageWidth int
	,@ChartImageHeight int
	,@SearchRawDataDaysBefore int
	,@SearchRawDataDaysAfter int
	,@SearchAlarmsDaysBefore int
	,@SearchAlarmsDaysAfter int
	,@ShowUnitId bit
	,@EnableMonitoringStatusButton bit
	,@OldDataTimeMinutes int
	,@OldDataColor nvarchar(50)
	,@SqlLoggingIsOn bit
AS
BEGIN
	insert _LogTemp(LogDate, info)
	SELECT GETDATE(), 	'start: ' + convert(nvarchar(30), @DayStartHour, 121) + '     end: ' + convert(nvarchar(30), @DayEndHour ,121)


	DECLARE
		@INSERTED_ITEM_ID int
		,@START_DATE_UTC datetime
		
	DECLARE @utcOffsetMins int

	SELECT @utcOffsetMins = DATEDIFF(MINUTE, GETDATE(), GETUTCDATE())

	SET NOCOUNT OFF;
	BEGIN TRY
		BEGIN TRANSACTION
			EXEC @START_DATE_UTC = dbo.GetCurrentUTCDate;

			INSERT INTO ev_SettingsHistory
			VALUES(
				 @START_DATE_UTC
				,@UserId
			    ,@NoConnectionFinishingAlarms 
			    ,@FullMonitoringUnassignedBands 
			    ,@WarningAutoCloseMessage
			    ,@LoBatteryWrActive 
			    ,@LoBatteryWrMins
			    ,@LoBatteryAlActive 
			    ,@LoBatteryAlMins
			    ,@BatteryTermWrDaysBefore
			    ,@BatteryTermWrMessage
			    ,@BatteryTermAlDaysBefore 
			    ,@BatteryTermAlMessage
			    ,DATEADD(MINUTE, @utcOffsetMins, @DayStartHour)
			    ,DATEADD(MINUTE, @utcOffsetMins, @DayEndHour)
			    ,@NoTransmitterWrActive 
			    ,@NoTransmitterWrMins 
			    ,@NoTransmitterAlActive 
			    ,@NoTransmitterAlMins 
			    ,@FirstBandAssignmentReason 
			    ,@EventToArchiveAfterDays
			    ,@_LocalAdminPhone 
			    ,@MaxPasswordAgeDays
			    ,@ShowReasonField 
			    ,@PingSessionIntervalSecs
			    ,@MonitoringSoundIntervalSecs
			    ,@RangeColorIntensityPercent
			    ,@MaxRwosLogs
			    ,@MaxRwosMessages
			    ,@DefMessageActivityMinutes
			    ,@MaxRowsAlarmsPopup
			    ,@MaxRowsAlarms
			    ,@MaxRowsRawData
			    ,@RoleDefForNewUser
			    ,@UnitsLocationSeparator
			    ,@MaxDaysReports 
			    ,@MaxDaysRawData 
			    ,@SearchMessagesDaysBefore 
			    ,@SearchMessagesDaysAfter 
			    ,@SearchLogsDaysBefore 
			    ,@SearchLogsDaysAfter 
			    ,@SearchChartDataHoursBefore 
			    ,@SearchChartDataHoursAfter 
			    ,@ChartImageWidth 
			    ,@ChartImageHeight
			    ,@SearchRawDataDaysBefore 
			    ,@SearchRawDataDaysAfter 
			    ,@SearchAlarmsDaysBefore 
			    ,@SearchAlarmsDaysAfter 
			    ,@ShowUnitId 
			    ,@EnableMonitoringStatusButton 
			    ,@OldDataTimeMinutes 
			    ,@OldDataColor 
				,@SqlLoggingIsOn
			);
			SELECT @INSERTED_ITEM_ID = SCOPE_IDENTITY()
		COMMIT TRANSACTION
			SELECT @INSERTED_ITEM_ID AS Result, NULL as ErrorCode, NULL as ErrorMessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
			SELECT CONVERT(int, null) AS Result, -1 AS ErrorCode, 'Operacja nie mogła zostać zrealizowana.' AS ErrorMessage
	END CATCH
END