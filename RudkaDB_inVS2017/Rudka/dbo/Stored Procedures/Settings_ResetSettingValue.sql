﻿

CREATE PROCEDURE [dbo].[Settings_ResetSettingValue]
	@ClientId int
	,@Name nvarchar(50)
	, @operatorId int
	, @reason nvarchar(max)
AS
BEGIN
	DECLARE @OPERATION_DELETE tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_EDIT tinyint = 2

		,@RECORD_SYSTEMSETTING tinyint = 255

	DECLARE @newValue nvarchar(1024)
		,@settingId int
		,@logEntry nvarchar(max)
	DECLARE @result bit = 0

	BEGIN TRANSACTION
		BEGIN TRY
			UPDATE dbo.Settings
				SET Value = null
			WHERE ClientId = @ClientId AND Name = @Name

			SELECT @newValue = COALESCE(CONVERT (nvarchar(1024), [dbo].[settings_GetInt](ClientId, Name))
								, [dbo].[settings_GetString](ClientId, Name))
				,@settingId = ID
			FROM dbo.Settings
			WHERE ClientId = @ClientId AND Name = @Name

			UPDATE dbo.Settings
				SET Value = @newValue
			WHERE ClientId = @ClientId AND Name = @Name


			SET @logEntry = 'Wartość domyślna: ' + @newValue
			INSERT dbo.Logs(OperationDate, OperatorId, OperationType, ObjectType, ObjectId, Reason, Info)
						VALUES(GETDATE(), @operatorId, @OPERATION_EDIT, @RECORD_SYSTEMSETTING, @settingId, @reason, @logEntry)

			SET @result = 1
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
		END CATCH

	SELECT @result AS Result
END
