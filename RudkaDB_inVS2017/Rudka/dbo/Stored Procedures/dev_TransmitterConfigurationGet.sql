﻿CREATE PROCEDURE [dbo].[dev_TransmitterConfigurationGet]
	@TransmitterId smallint = NULL
AS 
BEGIN
	SET NOCOUNT OFF;

	DECLARE @groups nvarchar(max) = ''
	SELECT @groups = CONVERT(nvarchar(10), [Group]) + ',' + @groups From dev_TransmitterTransitions where TransmitterId = @TransmitterId

	SELECT t.Id, t.Mac, t.MacString, t.BarCode, t.Name, t.TypeId, t.ReportIntervalSec, t.Sensitivity, t.AvgCalcTimeS, t.SwitchDelayS, t.Msisdn, t.IsBandUpdater, t.Description
		,t.RssiTreshold, t.[Group], t.IpAddress, @groups AS Transitions
		,dbo.GetUnitLocation(u.ID, default, default, default) AS TransmitterUnitLocation, ut.UnitID AS UnitID
	FROM dbo.dev_Transmitters t
	LEFT JOIN dev_UnitTransmitter ut ON t.Id = ut.TransmitterId
	LEFT JOIN Units u ON ut.UnitID = u.ID
	WHERE t.Id = 0 OR t.Id = @TransmitterId
END