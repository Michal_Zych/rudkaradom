﻿


CREATE PROCEDURE [dbo].[msg_GetMessages]
	@ClientId int
	,@DateStart datetime
	,@DateEnd datetime
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @maxRows int
	SELECT @maxRows = MaxRwosMessages FROM dbo.ev_Settings

	SELECT TOP(@maxRows)
		Id
		, Mode AS MessageType
		, ActiveFrom AS StartDate
		, ActiveTo AS EndDate
		, Message
	FROM dbo.Messages
	WHERE  ActiveFrom <= @DateEnd AND ActiveTo >= @DateStart
	ORDER BY ID
END
