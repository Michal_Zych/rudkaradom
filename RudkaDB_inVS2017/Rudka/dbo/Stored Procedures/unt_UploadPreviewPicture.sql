﻿
CREATE  PROCEDURE [dbo].[unt_UploadPreviewPicture]
	@name nvarchar(128),
	@path nvarchar(1024),
	@clientId int
AS
BEGIN
	SET NOCOUNT ON;
        
    declare @id int
	
	SELECT @id = ID
	FROM Pictures
	WHERE upper(Name) = upper(@name)
	  AND upper(Path) = upper(@path)
	  AND ClientID = @clientId
	
	IF @id is null
	BEGIN
		INSERT INTO [dbo].[Pictures]
			(Name, Path, ClientID)
		VALUES
			(@name, @path, @clientId)
	END
END

