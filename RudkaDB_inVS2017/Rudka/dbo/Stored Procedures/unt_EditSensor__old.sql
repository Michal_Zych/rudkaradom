﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	Edycja danych Unit'a o podanym ID
-- =============================================
CREATE PROCEDURE [dbo].[unt_EditSensor__old]
(
		@Id smallint
		, @SocketID int
		, @Interval int
		, @LoRange real
		, @UpRange real
		, @GuiRangeDelay smallint
		, @GuiCommDelay smallint
		, @SmsEnabled bit 
		, @SmsRangeDelay smallint
		, @SmsCommDelay smallint
		, @MailEnabled bit 
		, @MailRangeDelay smallint
		, @MailCommDelay smallint
		, @AlDisabled bit 
		, @AlDisabledForHours bit 
		, @AlDisableHoursFrom1 time(7)
		, @AlDisableHoursTo1 time(7)
		, @AlDisableHoursFrom2 time(7)
		, @AlDisableHoursTo2 time(7)
		, @AlDisabledForRanges bit 
		, @MU nvarchar(5)
		, @LegendDescription nvarchar(50)
		, @LegendShortcut nvarchar(5)
		, @LegendRangeDescription nvarchar(50)
		, @LineColor varchar(15)
		, @LineWidth tinyint
		, @RangeLineWidth tinyint
		, @LineStyle varchar(50)
		, @RangeLineStyle varchar(50)
		, @AlarmColor varchar(15)
		, @Enabled bit 
		, @Calibration real 
		, @operatorId int
		, @logEntry nvarchar(max)
		, @reason nvarchar(max)
		, @omittedRanges nvarchar(max) --format: 'data'do'data','data'do'data'
		, @smsReceivers nvarchar(max)
		, @emailReceivers nvarchar(max)
)
AS
BEGIN
	DECLARE @OPERATION_DELETE tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_EDIT tinyint = 2

		,@RECORD_UNIT tinyint = 0
		,@RECORD_USER tinyint = 1

		,@CURRENT_DATE datetime = dbo.GetCurrentDate()

	DECLARE @DEFAULT_SCALE real = 0.1
		,@socketScale real

	IF @SocketID IS NOT NULL
		SELECT @socketScale = dbo.GetScaleForSocket(@SocketID)

	BEGIN TRANSACTION
		BEGIN TRY
			DECLARE @oldLoRange smallint
				,@oldUpRange smallint
				,@newLoRange smallint
				,@newUpRange smallint

			SELECT @oldLoRange = LoRange, @oldUpRange = UpRange FROM dbo.Sensors WHERE Id = @Id		


			UPDATE dbo.Sensors SET
				  SocketID = @socketId
				, Interval = @Interval
				, LoRange = CONVERT(smallint, ROUND(@Lorange / COALESCE(Scale, COALESCE(@socketScale, @DEFAULT_SCALE)), 0))
				, UpRange = CONVERT(smallint, ROUND(@UpRange / COALESCE(Scale, COALESCE(@socketScale, @DEFAULT_SCALE)), 0))
				, GuiRangeDelay = @GuiRangeDelay
				, GuiCommDelay = @GuiCommDelay
				, SmsEnabled = @SmsEnabled
				, SmsRangeDelay = @SmsRangeDelay
				, SmsCommDelay = @SmsCommDelay
				, MailEnabled = @MailEnabled
				, MailRangeDelay = @MailRangeDelay
				, MailCommDelay = @MailCommDelay
				, AlDisabled = @AlDisabled
				, AlDisabledFrom = CASE WHEN @AlDisabled = 0 THEN NULL 
									ELSE (CASE WHEN @AlDisabled = AlDisabled THEN AlDisabledFrom ELSE @CURRENT_DATE END) END
				, AlDisabledForHours = @AlDisabledForHours
				, AlDisableHoursFrom1 = @AlDisableHoursFrom1
				, AlDisableHoursTo1 = @AlDisableHoursTo1
				, AlDisableHoursFrom2 = @AlDisableHoursFrom2
				, AlDisableHoursTo2 = @AlDisableHoursTo2
				, AlDisabledForRanges = @AlDisabledForRanges
				, MU = @MU
				, LegendDescription = @LegendDescription
				, LegendShortcut = @LegendShortcut
				, LegendRangeDescription = @LegendRangeDescription
				, LineColor = @LineColor
				, LineWidth = @LineWidth
				, RangeLineWidth = @RangeLineWidth
				, LineStyle = @LineStyle
				, RangeLineStyle = @RangeLineStyle
				, AlarmColor = @AlarmColor
				, [Enabled] = @Enabled
				, Calibration = CONVERT(smallint, ROUND(@Calibration / COALESCE(Scale, COALESCE(@socketScale, @DEFAULT_SCALE)), 0))
				, Scale = COALESCE(Scale, @socketScale)
			WHERE Id = @Id

			SELECT @newLoRange = LoRange, @newUpRange = UpRange FROM dbo.Sensors WHERE Id = @Id		
			IF(@newLoRange <> @oldLoRange) OR (@newUpRange <> @oldUpRange)
			BEGIN
				UPDATE dbo.Sensors SET RangeValidFrom = @CURRENT_DATE WHERE Id = @Id

				UPDATE dbo.MeasurementRanges
					SET DateEnd = @CURRENT_DATE
				WHERE SensorID = @Id AND DateEnd IS NULL

				INSERT dbo.MeasurementRanges(SensorID, DateStart, DateEnd, LoRange, UpRange)
				VALUES (@Id, @CURRENT_DATE, null, @newLoRange, @newUpRange)
			END


			DELETE dbo.AlarmsOmittedRanges WHERE SensorID = @Id
			INSERT dbo.AlarmsOmittedRanges
			
			SELECT @Id
					, LEFT(Value, CHARINDEX('do', Value) - 1)
					, SUBSTRING(Value, CHARINDEX('do', Value)  +2, 999) 
			FROM dbo.fn_Split(@omittedRanges, ',')

			DELETE dbo.AlarmNotifyBySms WHERE Sensorid = @Id
			INSERT dbo.AlarmNotifyBySms
			SELECT @Id, Value
			FROM dbo.fn_Split(@smsReceivers, ',')

			DELETE dbo.AlarmNotifyByEmail WHERE Sensorid = @Id
			INSERT dbo.AlarmNotifyByEmail
			SELECT @Id, Value
			FROM dbo.fn_Split(@emailReceivers, ',')
			
			DECLARE @unitId int
			SELECT @unitId = UnitId FROM dbo.Sensors WHERE Id = @Id
			
			INSERT dbo.Logs(OperationDate, OperatorId, OperationType, ObjectType, ObjectId, Reason, Info)
				VALUES(dbo.GetCurrentDate(), @operatorId, @OPERATION_EDIT, @RECORD_UNIt, @unitId, @reason, @logEntry)
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			SELECT -1
			RETURN
		END CATCH

	COMMIT TRANSACTION
	
	SELECT @Id	
END

