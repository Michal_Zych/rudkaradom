﻿-- T1122 przesunięcie czasu o 11 godzin i 22 minuty
-- + create band
-- !01 - ustaw 01 jako aktualny band
-- Np0102 - ustaw no go zone dla p/u 01 na 02
-- P1122 - pacjentowi 11 przypisz band 22 --00 null
-- Exxx1 zdarzenie xxx 1/0 status
	-- LBSx  - LowBatteryStatus
	-- BOOR	 - BandOutOfRange
	-- ACC.	 - sygnał ACC
	-- SOS.	 - sygnał SOS
	-- MOVx	 - MovingStatus
	-- ZCxx	 - ZoneChanged xx - new UnitId
	-- TxxS	 - TransmitterConnectionStatus xx transmiterId, s IsConnected
-- R release pending alarms
-- C close alarms
-- * Przerwanie wykonywania
-- ; nie rób nic
-- - zakomentuj komendę
-- Snazwa=wartość; - ustawienie zmiennej konfiguracyjnej
-- Qtreść; - wykonaj treść SQL
CREATE PROCEDURE [dbo].[_TestScenario]
	@Scenario nvarchar(1000)
	,@ScenarioId int = null
	,@RunWithoutSaving bit = 1
	,@SetResultAsTemplate bit = 0
	,@SilentMode bit = 0
AS
BEGIN
	IF @Scenario IS NULL
		SELECT @Scenario = Scenario FROM _test_cases WHERE Id = @ScenarioId

	
	IF @Scenario IS NULL
	BEGIN
		SELECT 'Nie podano scenariusza' as Result
		RETURN
	END



DECLARE 
	@eventDate datetime = '2017-01-01 11:11:11'
	,@eventStepSeconds int = 61
	,@defaultBandId smallint = 1

DECLARE
	@stepNr int = 1
	,@params nvarchar(100)
	,@step char
	,@currentDate datetime = @eventDate
	,@bandId nvarchar(10)
	,@sql nvarchar(1024)
	,@sqlSettings nvarchar(1024)
	,@showDefaultResults bit 
	,@isCommented bit
	,@remStr nvarchar(50)
	,@transmitterMode bit = 0
	SELECT @bandId = convert(nvarchar(10), @defaultBandId)
	SET @currentDate = @eventDate
	UPDATE _TestCurrentTime SET CurrentTime = @currentDate
	
	DECLARE @ObjectType TABLE(Value int, Description nvarchar(100))
	DECLARE @EventType TABLE(Value int, Description nvarchar(100))
	DECLARE @EventSeverity TABLE(Value int, Description nvarchar(100))

	INSERT @ObjectType
	EXEC dbo.GetEnumDictionary 'ObjectType'
	INSERT @EventType 
	EXEC dbo.GetEnumDictionary 'EventType'
	INSERT @EventSeverity
	EXEC dbo.GetEnumDictionary 'EventSeverity'


	
	BEGIN -- Clear Data
		TRUNCATE TABLE BatteryTypes
		TRUNCATE TABLE Bands
		TRUNCATE TABLE BandSettings
		TRUNCATE TABLE ev_BandStatus
		TRUNCATE TABLE ev_TransmitterStatus
		TRUNCATE TABLE ev_Events
		TRUNCATE TABLE ev_Alarms
		TRUNCATE TABLE ev_UsersAlarms
		TRUNCATE TABLE ev_EventsToRaise
		TRUNCATE TABLE al_NoGoZones
		TRUNCATE TABLE ev_CallOrders
		TRUNCATE TABLE ev_CallOrdersDetails
		TRUNCATE TABLE dbo.ev_SettingsHistory
		TRUNCATE TABLE dbo.al_CallingGroups
		TRUNCATE TABLE al_ConfigPatients
		TRUNCATE TABLE al_ConfigUnits	
		TRUNCATE TABLE al_NoGoZones
		TRUNCATE TABLE dev_senses

		INSERT BatteryTypes(Name, LifeMonths, Capacity, Voltage, Description) VALUES
		('Bateria mała', 12, 150, 3, 'mała bateria'),
		('Bateria duża', 24, 300, 3.4, 'duża bateria')

		SET IDENTITY_INSERT dbo.Bands ON;
		INSERT dbo.Bands(Id, Mac, Uid, BarCode, TransmissionPower, AccPercent, SensitivityPercent, BroadcastingIntervalMs,BatteryTypeId, BatteryInstallationDateUtc)
		SELECT 0, 0, null, null, 4, 80, 100, 2500, 1, '2001-01-01'
		SET IDENTITY_INSERT dbo.Bands OFF;


		--domyślne ustawienia opaski
		INSERT BandSettings(BandId, [Key], [Description], Value) VALUES
		(0, 'Key1', 'klucz 1', 'value 1'),
		(0, 'Key2', 'klucz 2', 'value 2'),
		(0, 'Key3', 'klucz 3', 'value 3')



		INSERT al_CallingGroups VALUES
		 (1,	4,	1,	'p1a',	'p1a')
		,(1,	4,	2,	'p1b',	'p1b')
		,(2,	4,	1,	'p2a',	'p2a')
		,(2,	4,	2,	'p2b',	'p2b')
		,(3,	0,	1,	'u3a',	'u3a')
		,(3,	0,	2,	'u3b',	'u3b')
		,(4,	0,	1,	'u4a',	'u4a')
		,(4,	0,	2,	'u4b',	'u4b')
		,(8,	0,	1,	'u8a',	'u8a')
		,(8,	0,	2,	'u8b',	'u8b')

		INSERT al_NoGoZones values
		 (1, 4, 13)
		 ,(1, 4, 14)
		 ,(2, 4, 13)
		 

		INSERT dbo.ev_SettingsHistory (StartDateUtc) Values('2017-01-01')
	END
	

	INSERT al_ConfigPatients 
	SELECT Id, 1,1,1,10,1,20,1,10,1,20,1,10,1,20,1,10,1,20,1,10,1,20,10,1,10,1,20
	FROM Patients

	INSERT al_ConfigUnits
	SELECT Id, 1,	1,	1,	10,	1,	20,	1,	10,	1,	20,	1,	10,	1,	20,	1,	10,	1	,20,	1,	10,	1,	20,	0, 10,	1	,10,	1,	20,	1,	10,	1,	20,	1
	FROM Units
	-- unit parzyste w dzień, nieparzyste w nocy
	update al_ConfigUnits set
	DMoveWrMins = 12,	DMoveAlMins = 22,
	DNoMoveWrMins = 14, DNoMoveAlMins = 24,
	NMoveWrMins = 11,	NMoveAlMins = 21,
	NNoMoveWrMins = 13, NNoMoveAlMins = 23

	UPDATE Patients set WardId = 3, roomId = null where id = 1
	UPDATE Patients set WardId = 4, roomId = 8 where id = 2

	WHILE @stepNr <= LEN(@scenario)
	BEGIN
		SELECT @step = SUBSTRING(@scenario, @stepNr, 1)
		SELECT @params = '', @showDefaultResults = 1, @isCommented = 0
		
		IF @step = ';'
		BEGIN
			SET @stepNr = @stepNr + 1
			CONTINUE
		END

		IF @step = '-'
		BEGIN
			SELECT @stepNr = @stepNr + 1, @step = SUBSTRING(@scenario, @stepNr, 1), @isCommented = 1
		END

		SET @sql = CASE WHEN @step = 'E' THEN 'Zdarzenie'
						WHEN @step = 'C' THEN 'Zamknięcie alarmu'
						WHEN @step = 'R' THEN 'Wywołanie odłożonych alarmów'
						WHEN @step = '+' THEN 'Dodaj band'
						WHEN @step = 'P' THEN 'Przypisz pacjentowi opaskę'
						WHEN @step = '!' THEN 'Aktualny BandId = '
						When @step = 'N' THEN 'No go zone '
						WHEN @step = 'T' THEN 'Dodanie czasu '
						WHEN @step = '*' THEN 'Przerwanie'
						WHEN @step = 'S' THEN 'Ustawienie konfiguracji'
						WHEN @step = 'Q' THEN 'Wykonaj SQL'
					END
		IF @stepNr = 1 SET @sql = @scenario + '  ' + @sql
		
		IF @step = 'Q'
		BEGIN
			DECLARE @qi int
			SELECT @qi = CHARINDEX(';', @scenario, @stepNr)
			SELECT @params = SUBSTRING(@scenario, @stepNr + 1, @qi - @stepNr -1)
			SELECT @stepNr = @qi --+ 1
		END


		IF @step = 'S'
		BEGIN
			SELECT @sqlSettings = ''

			WHILE @step IN ('S')
			BEGIN
				DECLARE @i int

				SELECT @i = CHARINDEX(';', @scenario, @stepNr)
				SELECT @params = SUBSTRING(@scenario, @stepNr + 1, @i - @stepNr -1)
				SELECT @stepNr = @i + 1

				IF @sqlSettings = '' 
					SET @sqlSettings = 'UPDATE ev_Settings SET ' + @params
				ELSE
					SET @sqlSettings = @sqlSettings + ', ' + @params

				SELECT @step = SUBSTRING(@scenario, @stepNr, 1) 
			END
			SET @stepNr = @stepNr - 1
			IF @step = '' SET @stepNr = @stepNr - 1
			SELECT @step = 'S', @params = @sqlSettings
		END



		BEGIN -- pobranie parametrów
			IF @step IN ('P', 'T', 'E')	-- 4 znakowe parametry
			BEGIN
				SELECT @params = SUBSTRING(@scenario, @stepNr + 1, 4)

				SET @stepNr = @stepNr + 4
			END

			IF @step IN('!')			-- 2 znakowe parametry
			BEGIN
				SELECT @params = SUBSTRING(@scenario, @stepNr + 1, 2)
				SET @stepNr = @stepNr + 2
			END
			
			IF @step IN('N')			-- 5 znakowe parametry
			BEGIN
				SELECT @params = SUBSTRING(@scenario, @stepNr + 1, 5)
				SET @stepNr = @stepNr + 5
			END
		END

		IF @SilentMode = 0
		BEGIN -- Znacznik aktualnej komendy i czasu
			IF @isCommented = 1 SET @remStr = ' REM  ' ELSE SET @remStr = ''
			DECLARE @silentColumn nvarchar(128)
			SET @silentColumn = LEFT(@remStr + @sql +' ' + @step + ' ' + @params, 128)
			SET @sql = 'SELECT ''Data ->'' as [KOMENDA], ''' + CONVERT(nvarchar(25), @currentDate, 121) + ''' AS [' + @silentColumn + ']'
			EXEC sp_executeSql @sql
		END
		
		
		IF @isCommented = 1
		BEGIN
			SET @stepNr = @stepNr + 1
			CONTINUE
		END

		IF @step = 'E'	-- Zdarzenie
		BEGIN
			IF SUBSTRING(@params, 1, 3) = 'LBS'
				SET @sql = 'EXEC ev_StoreLowBatteryStatus ' + @bandId + ', null, '  + SUBSTRING(@params, 4, 1)
			IF SUBSTRING(@params, 1, 4) = 'BOOR'
				SET @sql = 'EXEC ev_StoreBandOutOfRange ' + @bandId + ', null'
			IF SUBSTRING(@params, 1, 3) = 'ACC'
				SET @sql = 'EXEC ev_StoreAcc ' + @bandId + ', null'
			IF SUBSTRING(@params, 1, 3) = 'SOS'
				SET @sql = 'EXEC ev_StoreSos ' + @bandId + ', null'
			IF SUBSTRING(@params, 1, 3) = 'MOV'
				SET @sql = 'EXEC ev_StoreMovingStatus ' + @bandId + ', null, '  + SUBSTRING(@params, 4, 1)
			IF SUBSTRING(@params, 1, 2) = 'ZC'
				SET @sql = 'EXEC ev_StoreZoneChanged ' + @bandId + ', null, '  + SUBSTRING(@params, 3, 2)
			IF SUBSTRING(@params, 1, 1) = 'T'
				SELECT @sql = 'EXEC ev_StoreTransmitterConnectionStatus ' + @bandId + ', null, '  + SUBSTRING(@params, 4, 1)
					,@transmitterMode = 1

			exec sp_executeSql @sql
		END
	
		IF @step = 'R' -- RaisePostponedEvents
		BEGIN
			SET @sql = 'EXEC dbo.ev_RaisePostponedEvents'
			exec sp_executeSql @sql
		END

		IF @step = '+' --Add Band
		BEGIN
			DECLARE @newBandId int
			SELECT @newBandId = MAX(id) + 1 from Bands
			DECLARE @newBandCode nvarchar(50) = CONVERT(nvarchar(50), @newBandId)


			exec [dbo].[dev_BandConfigurationSet] null, @newBandId, null, @newBandCode, @newBandCode, 0, 0, 100, 10 , 2, @currentDate, null, 'klucz,desc,value,klucza,desca,valuea,', @newBandId, 0
			DELETE al_CallingGroups Where id = @newBandId And ObjectType = 0
			INSERT al_CallingGroups values
				(@newBandId, 0, 1, 'u' + Convert(nvarchar(10), @newBandId) + 'a', 'u' + Convert(nvarchar(10), @newBandId) + 'a')
				,(@newBandId, 0, 2, 'u' + Convert(nvarchar(10), @newBandId) + 'b', 'u' + Convert(nvarchar(10), @newBandId) + 'b')
			if @newBandId <> 1
				BEGIN
					DELETE al_ConfigUnits where UnitId = @newBandId
					INSERT al_ConfigUnits
					SELECT @newBandId,[IsSOScalling],[IsACCactive],[DMoveWrActive],[DMoveWrMins],[DMoveAlActive],[DMoveAlMins],[DNoMoveWrActive],[DNoMoveWrMins]
					     ,[DNoMoveAlActive],[DNoMoveAlMins],[NMoveWrActive],[NMoveWrMins],[NMoveAlActive],[NMoveAlMins],[NNoMoveWrActive],[NNoMoveWrMins]
					     ,[NNoMoveAlActive],[NNoMoveAlMins],[OutOfZoneWrActive],[OutOfZoneWrMins],[OutOfZoneAlActive],[OutOfZoneAlMins],[IsNoGoZone],[InNoGoZoneMins],[NoConnectWrActive]
					     ,[NoConnectWrMins],[NoConnectAlActive],[NoConnectAlMins],[NoTransmitterWrActive],[NoTransmitterWrMins],[NoTransmitterAlActive],[NoTransmitterAlMins],[MoveSettingsPriority]
					 FROM [dbo].[al_ConfigUnits] WhERE UnitId = 1
				END
			IF @SilentMode = 0
				SELECT '' AS [Bands], * FROM Bands
		END

		IF @step = 'C' -- CloseAlarms
		BEGIN
			SELECT @sql = ''
			SELECT @sql = @sql + CONVERT(nvarchar(10), EventId) + ',' FROM ev_Alarms WHERE IsClosed = 0
			SELECT @sql = 'EXEC ev_CloseAlarms ''' + @sql + ''', ''zamknięcie'', 1'
			IF @sql IS NOT NULL exec sp_executeSql @sql
			ELSE IF @SilentMode = 0 SELECT '' AS [Brak Alarmów do zamknięcia]
		END
		
		IF @step = 'P'	-- Przypisz pacjentowi opaskę
		BEGIN
			DECLARE @PBandParams nvarchar(10) = SUBSTRING(@params, 1, 2)
			IF @PBandParams = '00' SET @PBandParams = 'NULL'
			SELECT @sql = SUBSTRING(@params, 3, 2)
			IF @sql = '00' SET @sql = 'NULL'
			SELECT @sql = 'EXEC ev_AssignBandToObject 1, null, ''powód'', ' + @sql + ', null, ' + @PBandParams + ',4,0'
			exec sp_executeSql @sql
		END

		IF @step = 'N'	-- Dodaj NoGoZone
		BEGIN
			DECLARE @noGoObjectType tinyint
			IF SUBSTRING(@params, 1, 1) = 'U' SET @noGoOBjectType = 0
			ELSE SET @noGoObjectType = 4
			
			INSERT al_NoGoZones VALUES(CONVERT(int, SUBSTRING(@params, 2, 2)), @noGoObjectType, CONVERT(int, SUBSTRING(@params, 4, 2)))
			IF @SilentMode = 0 SELECT '' AS [al_NoGoZones], * FROM al_NoGoZones
			SET @showDefaultResults = 0 
		END

		IF @step = '!'	-- ustaw aktualny BandId
		BEGIN
			SET @bandId = SUBSTRING(@params, 1, 2)
		END

		IF @step = 'T'	-- Zwiększ Czas
		BEGIN
			SET @currentDate = DATEADD(MINUTE, CONVERT(int, SUBSTRING(@params, 1, 2)), @currentDate)
			SET @currentDate = DATEADD(SECOND, CONVERT(int, SUBSTRING(@params, 3, 2)), @currentDate)
			IF @SilentMode = 0 SELECT @currentDate AS [CurrentDate]
			SET @showDefaultResults = 0
		END
		ELSE
			SET @currentDate = DATEADD(SECOND, @eventStepSeconds, @currentDate)
			UPDATE _TestCurrentTime SET CurrentTime = @currentDate  -- ustawienie czasu

		IF @step = '*'	-- przerwanie wykonywania
		BEGIN
			SET @stepNr = LEN(@scenario)
			SET @showDefaultResults = 0
		END

		IF @step = 'S' -- ustawienie zmiennej konfiguracyjnej
		BEGIN
			EXEC sp_executeSql @params
			SELECT * FROM ev_Settings
			SET @showDefaultResults = 0
		END

		IF @step = 'Q' -- wykonanie dynamicznego SQL
		BEGIN
			EXEC sp_executeSQL @params
			SET @showDefaultResults = 0
		END


		IF @showDefaultResults = 1 AND @SilentMode = 0   -- ShowResults
		BEGIN
			select '' as BandStatus, b.BandId, o.Description AS ObjectType, b.ObjectId, b.AssignDateUtc, b.ObjectUnitId, b.CurrentUnitId, b.InCurrentUnitUtc, b.PreviousUnitId, b.InPreviousUnitUtc,
				b.TopAlarmId, e.Description AS TopAlarmType, s.Description As TopAlarmSeverity, b.TopAlarmDateUtc, b.TopEventDateUtc, b.Severity1Count, b.Severity2Count,
				b.OngNoGoZoneInEventId, b.OngNoGoZoneInAlarmId, b.OngDisconnectedEventId, b.OngDisconnectedAlarmId, b.OngZoneOutEventId,
				b.OngZoneOutAlarmId, b.OngNoMovingEventId, b.OngNoMovingAlarmId, b.OngMovingEventId, b.OngMovingAlarmId, b.OngLowBatteryEventId, b.OngLowBatteryAlarmId, SynchroToken
			from ev_BandStatus b
			LEFT JOIN @ObjectType o on b.ObjectType = o.Value
			LEFT JOIN @EventType e on b.TopAlarmType = e.Value
			LEFT JOIN @EventSeverity s on b.TopAlarmSeverity = s.Value

			IF @transmitterMode = 1
			BEGIN
				SELECT '' as TransmitterStatus, * FROM ev_TransmitterStatus
			END

			select '' as [Events], e.Id, e.DateUtc, et.Description as [Type], s.Description as Severity, e.UnitId, e.ObjectId, o.Description As ObjectType, e.ObjectUnitId
			,e.StoreDateUtc, e.EndDateUtc, e.EndingEventId, e.OperatorId, e.[EventData], e.Reason
			, 'Alarms->' AS Alarms, a.*, 'UserAlarms->' AS UserAlarms, ua.* 
			from ev_events e
			LEFT JOIN ev_Alarms a on a.EventId = e.Id
			LEFT JOIN ev_UsersAlarms ua on ua.EventId = e.Id
			JOIN @ObjectType o on e.ObjectType = o.Value
			JOIN @EventType et on e.Type = et.Value
			JOIN @EventSeverity s on e.Severity = s.Value
			ORDER BY e.Id

			select ''  as EventsToRaise, e.Id, ro.Description as RaisingObjectType, e.RaisingObjectId, e.RaisingEventId, e.RaisingEventDateUtc, e.RaiseDateUtc, et.Description as [Type], s.Description as Severity, e.UnitId,
			e.ObjectId, o.Description as ObjectType, e.ObjectUnitId
			from ev_EventsToRaise e
			JOIN @ObjectType o on e.ObjectType = o.Value
			JOIN @EventType et on e.Type = et.Value
			JOIN @EventSeverity s on e.Severity = s.Value
			JOIN @ObjectType ro on ro.Value = e.RaisingObjectType
			ORDER BY e.Id
		END
		
		SET @stepNr = @stepNr + 1
	END
 
	IF @RunWithoutSaving = 1 RETURN

	BEGIN --rezultaty
		DECLARE @result nvarchar(max)
		
		EXEC @sql = dbo.SerializeJSON 'SELECT * FROM Bands'
		SET @result = @sql

		EXEC @sql = dbo.SerializeJSON 'SELECT [BandId],[ObjectType],[ObjectId],[AssignDateUtc],[ObjectUnitId],[CurrentUnitId],[InCurrentUnitUtc],[PreviousUnitId],[InPreviousUnitUtc]
			,[TopAlarmId],[TopAlarmType],[TopAlarmSeverity],[TopAlarmDateUtc],[TopEventDateUtc],[Severity1Count],[Severity2Count],[OngNoGoZoneInEventId],[OngNoGoZoneInAlarmId],[OngDisconnectedEventId]
			,[OngDisconnectedAlarmId],[OngZoneOutEventId],[OngZoneOutAlarmId],[OngNoMovingEventId],[OngNoMovingAlarmId],[OngMovingEventId],[OngMovingAlarmId],[OngLowBatteryEventId],[OngLowBatteryAlarmId] 
			FROM ev_BandStatus'
		SET @result = @result + @sql

		IF @transmitterMode = 1
		BEGIN
			EXEC @sql = dbo.SerializeJSON 'SELECT TransmitterId, OngDisconnectedEventId, OngDisconnectedAlarmId, OngDisconnectedAlarmSeverity, OngConnectedEventId FROM ev_TransmitterStatus'
			SET @result = @result + @sql
		END

		EXEC @sql = dbo.SerializeJSON 'SELECT * FROM ev_Events'
		SET @result = @result + @sql

		EXEC @sql = dbo.SerializeJSON 'SELECT * FROM ev_Alarms'
		SET @result = @result + @sql

		EXEC @sql = dbo.SerializeJSON 'SELECT * FROM ev_UsersAlarms'
		SET @result = @result + @sql

		EXEC @sql = dbo.SerializeJSON 'SELECT * FROM ev_EventsToRaise'
		SET @result = @result + @sql

		EXEC @sql = dbo.SerializeJSON 'SELECT * FROM ev_CallOrders'
		SET @result = @result + @sql

		EXEC @sql = dbo.SerializeJSON 'SELECT * FROM ev_CallOrdersDetails'
		SET @result = @result + @sql

		EXEC @sql = dbo.SerializeJSON 'SELECT * FROM ev_Settings'
		SET @result = @result + @sql
	END

	SELECT @ScenarioId = NULL
	SELECT @ScenarioId = Id FROM _test_cases WHERE Scenario = @Scenario

	IF @ScenarioId IS NULL
	BEGIN
		INSERT _test_cases(Scenario, RunDate, RunResult) VALUES
			(@Scenario, GETDATE(), @result)
	END
	ELSE
	BEGIN
		IF @SetResultAsTemplate = 1
		BEGIN
			UPDATE _test_cases SET
				RunDate = GETDATE()
				,RunResult = @result
				,LastRunDate = null
				,LastRunResult = null
				,IsLastRunOk = null
			WHERE Id = @ScenarioId
		END
		ELSE
		BEGIN
			UPDATE _test_cases SET
				LastRunDate = GETDATE()
				,LastRunResult = @result
				,IsLastRunOk = CASE WHEN RunResult = @result THEN 1 ELSE 0 END
			WHERE Id = @ScenarioId
		END
	END


END