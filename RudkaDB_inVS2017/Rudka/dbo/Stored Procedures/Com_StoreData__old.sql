﻿

CREATE PROCEDURE [dbo].[Com_StoreData__old]
	@SensorId smallint
	,@MeasurementTime datetime
	,@Value smallint
	,@EventType tinyint = 0 -- 0 = OK
AS
BEGIN
SET NOCOUNT ON;

	--zwracane wartości
	DECLARE @MEASURE_STORED int = 10
		, @SENSOR_NO_ACIVE int = 20
		, @RESULT_OK int = 1
		, @RESULT_ERROR int = -1

	--jeśli istnieje zapis to zakończ z kodem 10
	IF EXISTS (SELECT 1 FROM dbo.Measurements WHERE SensorID = @SensorId AND MeasurementTime = @MeasurementTime)
	BEGIN
		SELECT @MEASURE_STORED
		RETURN
	END


BEGIN --DEKLARACJE ZMIENNYCH
		DECLARE --stałe typów alarmów EventType
		 @VALUE_OK tinyint = 0
		,@VALUE_TO_LOW tinyint = 1
		,@VALUE_TO_HIGH tinyint = 2
		,@NO_COMMUNICATION tinyint = 3
		,@NO_SENSOR tinyint = 3
		,@NO_DEVICE tinyint = 4
		,@NO_HUB tinyint = 5
		--typy powiadomień
		, @GUI			tinyint = 0
		, @SMS			tinyint = 1
		, @EMAIL		tinyint = 2
		--czas przetwarzania
		, @CURRENT_DATE datetime = dbo.GetCurrentDate()

		--konfiguracja sensora
		,@loRange smallint
		,@upRange smallint
		--informacje o konfiguracji alrmów i powiadomień
		, @alDisabled bit					-- całkowicie wyłączone alarmowanie
		, @alDisabledFrom datetime			-- data całkowitego wyłączenia alarmów
		, @alDisabledForRanges bit			-- wyłączone w zakresach dat
		, @alDisabledForHours bit			-- wyłączone w godzinach dnia
		, @alDisableHoursFrom1 time			-- początek pierwszego zakresu godzinowego
		, @alDisableHoursTo1 time			-- koniec pierwszego zakresu godzinowego
		, @alDisableHoursFrom2 time			-- początek drugiego zakresu godzinoweg
		, @alDisableHoursTo2 time			-- koniec drugiego zakresu godzinowego
		, @smsEnabled bit					-- aktywne smsy
		, @mailEnabled bit					-- aktywne maile
		, @smsRangeDelay smallint
		, @smsCommDelay smallint
		, @mailRangeDelay smallint
		, @mailCommDelay smallint
		, @guiRangeDelay smallint
		, @guiCommDelay smallint
		, @rangeValidFrom datetime			-- od kiedy obowiązuje aktualny zakres
		, @active bit
		--informacje o trwających alarmach
		, @alarmRangeId int
		, @alarmRangeTime datetime
		, @alarmRangeType tinyint
		, @alarmCommId int
		, @alarmCommTime datetime
		--początek okresu wyłączonego alarmowania
		, @omittedRangeDateStart datetime	-- data wyłączenia alarmowania dla aktywnego okresu wyłączenia
		DECLARE--zmienne robocze
		  @alarmsToFinischCloseTime datetime-- czas kiedy należy wyłączyć trwające alarmy
		, @alarmingEnabled	bit				-- 0 = całkowite wyłączenie alarmowania  
		, @closeMessage ALARM_COMMENT		-- komentarz zamykający alarm
		, @close bit						-- czy kończony alarm należy zamknąć
		, @commToClose int					-- id alarmu braku łączności do zamknięcia/zakończenia
		, @rangeToClose int					-- id alarmu przekroczenia zakresu do zamknięcia/zakończenia
		, @commOpen bit						-- otwórz alarm braku łączności
		, @rangeOpen bit					-- otwórz alarm przekroczenia zakresu
		, @rangeOpened int					-- id otwartego alarmu przekroczenia zakresu
		, @commOpened int					-- id otwartego alarmu braku łączności
		DECLARE--tabele robocze
		  @alarmsToFinischClose TABLE(		-- tabela zawierająca informacje o alarmach do zakończenia
				  ID int NOT NULL			-- id alarmu
				, Status bit NOT NULL		-- status = 1 alarm należy zamknąć
				, Comment ALARM_COMMENT)	-- komentarz do zamykanego alarmu
		DECLARE
		  @notifications TABLE(				-- tabela zawierająca powiadomienia które nalezy dodać
			AlarmID int NOT NULL			-- ID alarmu
			, Time DateTime NULL	    	-- czas wysłania powiadomienia
			, Type tinyint NOT NULL			-- typ powiadomienia (stałe - typy powiadomien)
		)
END

BEGIN --POBRANIE KONFIGURACJI I INFORMACJI O TRWAJĄCYCH ALARMACH
	SELECT
		  @active = Active
		, @loRange = LoRange
		, @upRange = UpRange
		, @Value = @Value + Calibration			--wynik pomiaru należy skalibrować
		, @rangeValidFrom = RangeValidFrom

		, @alDisabled = AlDisabled
		, @alDisabledFrom = AlDisabledFrom
		, @alDisabledForRanges	= AlDisabledForRanges
		, @alDisabledForHours	= AlDisabledForHours
		, @alDisableHoursFrom1	= AlDisableHoursFrom1 
		, @alDisableHoursTo1	= AlDisableHoursTo1 
		, @alDisableHoursFrom2	= AlDisableHoursFrom2 
		, @alDisableHoursTo2	= AlDisableHoursTo2 

		, @smsEnabled			= SmsEnabled
		, @mailEnabled			= MailEnabled
		, @smsRangeDelay		= SmsRangeDelay
		, @smsCommDelay			= SmsCommDelay
		, @mailRangeDelay		= MailRangeDelay
		, @mailCommDelay		= MailCommDelay
		, @guiRangeDelay		= GuiRangeDelay
		, @guiCommDelay			= GuiCommDelay

		--informacje o trwających alarmach
		, @alarmRangeId			= MeasureAlID
		, @alarmRangeTime		= MeasureAlTime
		, @alarmRangeType		= MeasureAlType
		, @alarmCommId			= CommAlID
		, @alarmCommTime		= CommAlTime
		
		, @omittedRangeDateStart= o.DateStart

	FROM dbo.Sensors s WITH (NOLOCK)
	LEFT JOIN dbo.AlarmsOngoing ao WITH (NOLOCK) ON s.Id = ao.SensorID
	LEFT JOIN dbo.AlarmsOmittedRanges o WITH (NOLOCK) ON (s.Id = o.SensorID AND (@MeasurementTime BETWEEN o.DateStart AND o.DateEnd))
	WHERE s.Id = @SensorId
END
	
	IF @active = 0
	BEGIN
		SELECT @SENSOR_NO_ACIVE
		RETURN
	END

BEGIN--jeśli zakres nie jest aktualny dla podanego czasu to pobierz go z tabeli MeasurementRanges
	IF @MeasurementTime < @rangeValidFrom
	BEGIN
		SELECT @loRange = LoRange, @upRange = UpRange
		FROM dbo.MeasurementRanges WITH(NOLOCK) 
		WHERE SensorID = @SensorId
			  AND @MeasurementTime BETWEEN DateStart AND DateEnd
	END
END

BEGIN --OBLICZ CZAS WYŁĄCZENIA ALARMOWANIA
	SET @alarmsToFinischCloseTime = null
	SET @alarmingEnabled = 1
	
	DECLARE @h time
	DECLARE @d datetime
	-- jeśli całkowicie wyłączone alarmowanie
	IF (@alDisabled = 1)
	BEGIN
		IF(@alDisabledFrom IS NULL) SET @alarmsToFinischCloseTime = @MeasurementTime
		ELSE SET @alarmsToFinischCloseTime = @alDisabledFrom
	END
	-- jeśli wyłączone w godzinach
	IF(@alDisabledForHours = 1) 
	BEGIN
		SET @h = NULL -- godzina rozpoczęcia okresu wyłączenia alarmowania
		IF(CONVERT(time, @MeasurementTime) BETWEEN @alDisableHoursFrom1 AND @alDisableHoursTo1)
			SET @h = @alDisableHoursFrom1
		IF(CONVERT(time, @MeasurementTime) BETWEEN @alDisableHoursFrom2 AND @alDisableHoursTo2)
			SET @h = @alDisableHoursFrom2
		IF(@h IS NOT NULL) -- @h godzina początkowa zakresu wyłączonego alarmowania
		BEGIN
			-- data wyłączenia to dzień pomiaru + godzina początkowa zakresu wyłączenia
			SET @d = CONVERT(datetime, CONVERT(date, @MeasurementTime)) + CONVERT(datetime, @h)
			-- czas wyłączenia alarmowania to miejszy z czau calkowitego wyłączenia i czasu wyłączenia w godzinach
			IF(@alarmsToFinischCloseTime IS NULL) SET @alarmsToFinischCloseTime = @d
			ELSE IF(@d < @alarmsToFinischCloseTime) SET @alarmsToFinischCloseTime = @d
		END
	END
	-- jeśli wyłączone w podanych zakresach
	IF(@alDisabledForRanges =1)
	BEGIN
		IF(@omittedRangeDateStart IS NOT NULL)
		-- czas wyłączenia to mniejszy z dotychczas obliczonego i czasu wyłączenia w okresach
		BEGIN
			IF(@alarmsToFinischCloseTime IS NULL) 
				SET @alarmsToFinischCloseTime = @omittedRangeDateStart
			ELSE IF(@omittedRangeDateStart < @alarmsToFinischCloseTime) 
				SET @alarmsToFinischCloseTime = @omittedRangeDateStart
		END
	END
	
	IF (@alarmsToFinischCloseTime IS NOT NULL)
		SET @alarmingEnabled = 0
END

BEGIN--WYZNACZ TYP ZDARZENIA 
	IF @EventType < @NO_COMMUNICATION 
	BEGIN
		IF @Value < @loRange SET @EventType = @VALUE_TO_LOW
		IF @Value > @upRange SET @EventType = @VALUE_TO_HIGH
	END
END

BEGIN--WYZNACZ ALARMY DO ZAKOŃCZENIA/ZAMKNIĘCIA
	IF @alarmingEnabled = 1 
	BEGIN
		SET @closeMessage = 'Krótki alarm zamknięty automatycznie'
		SET @alarmsToFinischCloseTime = @MeasurementTime
		IF @EventType < @NO_COMMUNICATION --jeśli brak łączności to nie kończymy żadnych alarmów
		BEGIN--które alarmy do zakończenia
			--jest odczyt zakończ brak łączności
			SET @commToClose = @alarmCommId
			--pomiary ok - zamknij alarmy
			IF @EventType = @VALUE_OK
				SET @rangeToClose = @alarmRangeId
			--pomiar przekracza górną granicę
			IF @EventType = @VALUE_TO_HIGH
			BEGIN
				IF @alarmRangeType = @VALUE_TO_LOW --jeśli poprzednio była dolna to zamknij
					SET @rangeToClose = @alarmRangeId
			END
			--pomiar przekracza dolną granicę
			IF @EventType = @VALUE_TO_LOW
			BEGIN
				IF @alarmRangeType = @VALUE_TO_HIGH --jeśli poprzednio była górna to zamknij
					SET @rangeToClose = @alarmRangeId
			END
		END	
		BEGIN--czy alarmy do zakończenia to krótkie alarmy i należy zamknąć
			IF @rangeToClose IS NOT NULL
			BEGIN
				SET @close = 0
				IF DATEDIFF(SECOND, @alarmRangeTime, @alarmsToFinischCloseTime) <= @guiRangeDelay
					SET @close = 1
				INSERT @alarmsToFinischClose VALUES(@rangeToClose, @close
										,CASE 
											WHEN @close = 1 THEN @closeMessage
											ELSE NULL 
										END)
			END
			IF @commToClose IS NOT NULL
			BEGIN
				SET @close = 0
				IF DATEDIFF(SECOND, @alarmCommTime, @CURRENT_DATE) <= @guiCommDelay
					SET @close = 1
				INSERT @alarmsToFinischClose VALUES(@commToClose, @close
												,CASE 
													WHEN @close = 1 THEN @closeMessage
													ELSE NULL		
												END)
			END	
		END
	END
	ELSE  
	BEGIN--alarmowanie wyłączone
		SET @closeMessage = 'Alarmowanie wyłączone, alarm zamknięty automatycznie'
		SET @rangeToClose = @alarmRangeId
		SET @commToClose = @alarmCommId
		IF @rangeToClose IS NOT NULL
			INSERT @alarmsToFinischClose VALUES(@rangeToClose, 1, @closeMessage)
		IF @commToClose IS NOT NULL
			INSERT @alarmsToFinischClose VALUES(@commToClose, 1, @closeMessage)
	END
END

BEGIN--WYZNACZ NOWE ALARMY
	IF @alarmingEnabled = 1
	BEGIN
		BEGIN--USTAL KTÓRE ALARMY OTWORZYĆ
			SET @rangeOpen = NULL
			SET @commOpen = NULL
			--jeśli nie ma pomiaru to otwórz brak łączności
			IF @EventType >= @NO_COMMUNICATION
			BEGIN
				IF @alarmCommId IS NULL
					SET @commOpen = 1
			END 
			ELSE
			BEGIN
				--jeśli nie było alarmu a jest przekroczenie to otwórz temperaturowy
				IF (@alarmRangeId IS NULL) AND (@EventType > @VALUE_OK)
					SET @rangeOpen = 1
				--jeśli było przkroczenie jednego progu a jest przekroczenie drugiego otwórz temperaturowy
				IF (@alarmRangeId IS NOT NULL) AND (@EventType > @VALUE_OK) AND (@alarmRangeType <> @EventType)
					SET @rangeOpen = 1
			END
		END
	END
END

	BEGIN TRANSACTION
		BEGIN TRY

			BEGIN--ZAPISZ WYNIKI
				INSERT dbo.Measurements(SensorID, MeasurementTime, Value, StoreDelay)
				VALUES(@SensorId, @MeasurementTime, @value, DATEDIFF(SECOND, @MeasurementTime, @CURRENT_DATE)) 
				
				UPDATE dbo.MeasurementsCurrent
				SET
					MeasurementTime = CASE
									WHEN @Value IS NULL 
										THEN CASE WHEN DATEADD(SECOND, @guiCommDelay, MeasurementTime) < @MeasurementTime THEN @MeasurementTime ELSE MeasurementTime END
									ELSE @MeasurementTime
								  END,
					Value = CASE
							WHEN @Value IS NULL
								THEN CASE WHEN DATEADD(SECOND, @guiCommDelay, MeasurementTime) < @MeasurementTime THEN @Value ELSE Value END
							ELSE @Value
						 END,
					HyperMesurementTime = @MeasurementTime,
					HyperValue = @Value,
					MeasurementStoreTime = CASE WHEN (@Value IS NOT NULL) THEN @CURRENT_DATE
												ELSE MeasurementStoreTime END
				WHERE SensorID = @SensorId
				IF(@@ROWCOUNT = 0)
				BEGIN
					INSERT INTO dbo.MeasurementsCurrent(SensorID, MeasurementTime, Value, HyperMesurementTime, HyperValue, MeasurementStoreTime)
						VALUES(@SensorId, @MeasurementTime, @Value, @MeasurementTime, @Value, 
								CASE WHEN (@Value IS NOT NULL)  THEN @CURRENT_DATE
												ELSE NULL END)
				END
			END
			BEGIN--ZAKOŃCZ/ZAMKNIJ ALARMY
				UPDATE a --zakończ alarmy, zamknij jeśli wyłączone alarmowanie lub alarm krótki
				SET 
					  DateEnd = CASE
									WHEN c.ID = @alarmCommId THEN @CURRENT_DATE
									ELSE @MeasurementTime
								END
					, Status = CASE WHEN a.Status = 1 THEN 1 ELSE c.Status END
					, UserStatus = a.UserStatus
					, DateStatus = CASE
										WHEN a.DateStatus IS NOT NULL THEN a.DateStatus
										ELSE CASE
												WHEN c.Status = 1 THEN 
													CASE
														WHEN c.ID = @alarmCommId THEN @CURRENT_DATE
														ELSE @alarmsToFinischCloseTime
													END 
												ELSE NULL
											END
								   END
					, CommentStatus = COALESCE(a.CommentStatus, c.Comment)
				FROM dbo.Alarms a WITH(NOLOCK) 
				JOIN @alarmsToFinischClose c ON a.ID = c.ID

				IF @rangeToClose IS NOT NULL
					SELECT @alarmRangeId =  NULL, @alarmRangeTime = NULL, @alarmRangeType = NULL
				IF @commToClose IS NOT NULL
					SELECT @alarmCommId = NULL, @alarmCommTime = NULL

				UPDATE dbo.AlarmsOngoing --usuń z alarmów trwających
				SET
					MeasureAlID = @alarmRangeId
					,MeasureAlTime = @alarmRangeTime
					,MeasureAlType = @alarmRangeType
					,CommAlID = @alarmCommId
					,CommAlTime = @alarmCommTime
				WHERE SensorID = @SensorId
				
				/*
				DELETE FROM dbo.AlarmNotifications --usuń powiadomienia które miały być wysłane po czasie zamknięcia
				WHERE AlarmID IN (@rangeToClose, @commToClose)
					AND SendAfterDate >= @alarmsToFinischCloseTime
				*/
					
				UPDATE a
					SET a.DeleteDate = dbo.GetCurrentDate()
					,Info = Info + ' | ' + CONVERT(nvarchar(30), @CURRENT_DATE, 121) + ':com_StoreData(Skasowano)'
				FROM dbo.AlarmNotifications a --usuń powiadomienia które miały być wysłane po czasie zamknięcia
				WHERE a.AlarmID IN (@rangeToClose, @commToClose)
					AND a.SendAfterDate >= @alarmsToFinischCloseTime	
					AND a.DeleteDate IS NULL
			END	
			BEGIN--OTWÓRZ NOWE ALARMY I POWIADOMIENIA
				IF @alarmingEnabled = 1
				BEGIN
					BEGIN--OTWÓRZ ALARMY
						IF @rangeOpen = 1
						BEGIN
							INSERT dbo.Alarms(SensorID, [Timestamp], EventType, LoRange, UpRange)
							VALUES(@SensorId, @MeasurementTime, @EventType, @loRange, @upRange)
							SET @rangeOpened = SCOPE_IDENTITY()
						END
			
						IF @commOpen = 1
						BEGIN
							INSERT dbo.Alarms(SensorID, [Timestamp], EventType, LoRange, UpRange)
							VALUES(@SensorId, @CURRENT_DATE, @EventType, @lorange, @upRange)
							SET @commOpened = SCOPE_IDENTITY()
						END	
			
						--uzupełnij alarmy trwające
						IF @rangeOpened IS NOT NULL
						BEGIN
							SET @alarmRangeId = @rangeOpened
							SET @alarmRangeTime = @MeasurementTime
							SET @alarmRangeType = @EventType
						END
						IF @commOpened IS NOT NULL
						BEGIN 
							SET @alarmCommId = @commOpened
							SET @alarmCommTime = @CURRENT_DATE
						END

						UPDATE dbo.AlarmsOngoing 
						SET
							MeasureAlID = @alarmRangeId
							,MeasureAlTime = @alarmRangeTime
							,MeasureAlType = @alarmRangeType
							,CommalID = @alarmCommId
							,CommAlTime = @alarmCommTime
						WHERE SensorID = @SensorId
						IF (@@ROWCOUNT = 0)
						INSERT dbo.AlarmsOngoing(SensorID, MeasureAlID, MeasureAlTime, MeasureAlType, CommAlID, CommAlTime)
						VALUES(@SensorId, @alarmRangeId, @alarmRangeTime, @alarmRangeType, @alarmCommId, @alarmCommTime)
					END	
			
					BEGIN--DODAJ POWIADOMIENIA DLA OTWIERANYCH ALARMÓW
						IF @rangeOpened IS NOT NULL --powiadomienia o przekroczeniu zakresu
						BEGIN
							INSERT @notifications VALUES (@rangeOpened, DATEADD(SECOND, @guiRangeDelay, @MeasurementTime), @GUI)
							IF @smsEnabled = 1
								INSERT @notifications VALUES (@rangeOpened, DATEADD(SECOND, @smsRangeDelay, @MeasurementTime), @SMS)
							IF @mailEnabled = 1			
								INSERT @notifications VALUES (@rangeOpened, DATEADD(SECOND, @mailRangeDelay, @MeasurementTime), @EMAIL)
						END
						IF @commOpened IS NOT NULL --powiadomienia o alarmie braku łączności
						BEGIN
							INSERT @notifications VALUES (@commOpened, DATEADD(SECOND, @guiCommDelay, @CURRENT_DATE), @GUI)
							IF @smsEnabled = 1
								INSERT @notifications VALUES (@commOpened, DATEADD(SECOND, @smsCommDelay, @CURRENT_DATE), @SMS)
							IF @mailEnabled = 1
								INSERT @notifications VALUES (@commOpened, DATEADD(SECOND, @mailCommDelay, @CURRENT_DATE), @EMAIL)
						END
				
						INSERT dbo.AlarmNotifications (SensorId, AlarmID, SendAfterDate, Type, Info)
						SELECT @SensorId, AlarmID, Time, Type, CONVERT(nvarchar(30), @CURRENT_DATE, 121) + ':com_StoreData(Insert)'
						FROM @notifications 
						WHERE Time IS NOT NULL
					END
			
				END
			END

		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			SELECT @RESULT_ERROR
			RETURN
		END CATCH
	COMMIT TRANSACTION

	SELECT @RESULT_OK
END

