﻿CREATE PROCEDURE [dbo].[ev_CalculateMonitoredObjectStatus] 
	@objectId dbo.OBJECT_ID
	,@objectType dbo.OBJECT_TYPE
AS
BEGIN
	SET NOCOUNT ON;

	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
		BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'objectId=' + COALESCE(CONVERT(nvarchar(100), @objectId), 'NULL') 
		EXEC Log_Action @@PROCID, @info
	END
	/****************************************************/

	DECLARE
		 @severity1Count int
		,@severity2Count int

		,@topAlarmId int
		,@topAlarmType dbo.EVENT_TYPE
		,@topAlarmSeverity dbo.EVENT_SEVERITY
		,@topAlarmDateUtc dateTime
		,@topEventDateUtc dateTime
		,@raisingEventId int

	SELECT @severity1Count = SUM(CASE WHEN e.Severity = 1 THEN 1 ELSE 0 END)
		, @severity2Count = SUM(CASE WHEN e.Severity = 2 THEN 1 ELSE 0 END)
	FROM dbo.ev_Events e 
	JOIN dbo.ev_Alarms a WITH(NOLOCK) ON e.Id = a.EventId AND IsClosed = 0
	WHERE e.ObjectId = @objectId AND e.ObjectType = @objectType
	SELECT @severity1Count = COALESCE(@severity1Count, 0), @severity2Count = COALESCE(@severity2Count, 0)




	SELECT TOP 1 @topAlarmId = e.Id, @topAlarmType = e.[Type], @topAlarmSeverity = e.Severity, @topAlarmDateUtc = e.DateUtc, @raisingEventId = a.RaisingEventId
	FROM dbo.ev_Events e
	JOIN dbo.ev_Alarms a ON e.Id = a.EventId AND IsClosed = 0
	WHERE e.ObjectId = @objectId AND e.ObjectType = @objectType
	ORDER BY e.Severity DESC, dbo.ev_GetEventPriority(e.Type), e.DateUtc 

	SELECT @topEventDateUtc = DateUtc
	FROM dbo.ev_Events 
	WHERE Id = @raisingEventId

	UPDATE dbo.ev_BandStatus SET
		TopAlarmId = @topAlarmId
		,TopAlarmType = @topAlarmType
		,TopAlarmSeverity = @topAlarmSeverity
		,TopAlarmDateUtc = @topAlarmDateUtc
		,TopEventDateUtc = @topEventDateUtc
		,Severity1Count = @severity1Count
		,Severity2Count = @severity2Count
	WHERE ObjectId = @objectId AND ObjectType = @objectType

END