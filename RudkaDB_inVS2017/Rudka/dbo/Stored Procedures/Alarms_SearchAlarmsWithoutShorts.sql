﻿
CREATE  PROCEDURE [dbo].[Alarms_SearchAlarmsWithoutShorts]
	@date_from datetime,
	@date_to datetime,
	@sensor_name DEVICE_NAME,
	@user int,
	@units IDs_LIST
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE --stałe typów alarmów EventType
		 @VALUE_TO_LOW tinyint = 1
		,@VALUE_TO_HIGH tinyint = 2
		,@NO_COMMUNICATION tinyint = 3
		,@NO_SENSOR tinyint = 3
		,@NO_DEVICE tinyint = 4
		,@NO_HUB tinyint = 5

	DECLARE @clientId int = 1
	
	DECLARE @maxRows int
	SELECT @maxRows = MaxRowsAlarms FROM dbo.ev_Settings
	
	SELECT top(@maxRows)
			a.ID as [ID],
			
			a.EventType as [EventType],
			a.[Timestamp] as [Timestamp],
			
			a.SensorID as [SensorID],
			u.Name as [SensorName],
			u.Id as [UnitId],
			dbo.GetUnitLocation(u.id, default, default, default) as Location,
			s.MU AS MU,

			a.LoRange * s.Scale AS LoRange,
			a.UpRange * s.Scale AS UpRange,
			dat.Value * s.Scale AS Value,
			
			a.DateEnd as DateEnd,
			a.DateStatus as AlarmCloseDate,
			a.UserStatus as AlarmCloseUserID,
			isnull(us.Name,'') + ' ' + isnull(us.LAstName,'') as AlarmCloseUserName,
			a.CommentStatus as AlarmCloseComment,
			s.IsBitSensor as IsBitSensor,
			s.LoStateDesc as LoStateDesc,
			s.HiStateDesc as HiStateDesc
	FROM
			Alarms a
			left join Measurements dat on dat.SensorID = a.SensorId and dat.MeasurementTime = a.Timestamp
			join Sensors s on s.id =  a.SensorId
			inner join Units u on u.Id = s.UnitId
			left join Users us on us.ID = a.UserStatus
			join dbo.SplitWordsToInt(@units) SU on SU.value = u.ID
	where (u.Name like '%' + @sensor_name + '%' or @sensor_name is null)
			and (a.Timestamp >= @date_from or @date_from is null)
			and (a.Timestamp <= @date_to or @date_to is null)
			and ( DATEDIFF(SECOND, a.Timestamp, Coalesce(a.DateEnd, [dbo].[GetCurrentDate]())) 
				  > (CASE 
						WHEN a.EventType < @NO_COMMUNICATION		THEN s.GuiRangeDelay
						ELSE s.GuiCommDelay
				     END))
	ORDER BY
		a.[Timestamp] desc
    
END

