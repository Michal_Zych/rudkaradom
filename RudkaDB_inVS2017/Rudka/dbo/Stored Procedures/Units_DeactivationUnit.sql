﻿-- =============================================
-- Author:		pi
-- Create date: 13-11-2012
-- Description: Deactivation unit
-- =============================================
CREATE PROCEDURE [dbo].[Units_DeactivationUnit]
	-- Add the parameters for the stored procedure here
	@unitId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (EXISTS(select * from Units where ParentUnitId = @unitId))
	BEGIN
		select 0
	END
	ELSE
	BEGIN
		update Units
		set Active = 0
		where id = @unitId
		select 1
	END
END

