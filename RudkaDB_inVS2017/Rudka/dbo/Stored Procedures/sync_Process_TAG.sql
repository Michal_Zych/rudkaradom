﻿CREATE PROCEDURE [dbo].[sync_Process_TAG]
	@RecordId int 
AS
BEGIN
	INSERT _log(LogDate, ProcedureName, Info)
	values (dbo.GetCurrentDate(), 'sync_Process_PALC', @RecordId)
	
	SET NOCOUNT ON;
	UPDATE tech.sync_RecordsIN SET
		Processed = 1
		,Try = 1
		,ProcessedDateUtc = dbo.GetCurrentUTCDate()
	WHERE Id = @RecordId
END