﻿CREATE PROCEDURE [dbo].[ev_StoreDbError]
	@Info nvarchar(max)
	,@ObjectId dbo.OBJECT_ID
	,@ObjectType dbo.OBJECT_TYPE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE
		@currentDateUtc dateTime = dbo.GetCurrentUtcDate()
		,@eventId int

	INSERT ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, [EventData])
	SELECT @currentDateUtc, eventType.DbError, eventSeverity.Event, null, @ObjectId, @ObjectType, null, @currentDateUtc, @currentDateUtc, @Info
	FROM enum.EventType eventType
	JOIN enum.EventSeverity eventSeverity ON 1 = 1
	SET @eventID = SCOPE_IDENTITY()

	UPDATE ev_Events SET EndingEventId = @eventID WHERE Id = @eventID
END