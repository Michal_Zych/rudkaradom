﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

					
create PROCEDURE [dbo].[M2MCreateSocketDevice]
	@socketName nvarchar(100)
	,@ipaddress nvarchar(100)
	,@channel int
	,@slaveAddress int
	,@lorange int
	,@uprange int
	,@calibration real=0
	,@deviceModel int =1
	,@rootid int=1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRANSACTION
		BEGIN TRY
			declare @socketId int
			declare @HubId int
			declare @unitType int
			declare @unitType2 int
			declare @deviceId int
			declare @scale float=0.1


			if exists(select 1 from hubs where IPAddress=@ipAddress and active=1)
			begin
				set @hubid=(select top(1) id from hubs where IPAddress=@ipAddress and active=1)
			end else begin
				insert into hubs values(@ipaddress,1001,1)

				set @hubid=SCOPE_IDENTITY()

			end

			
			set @deviceId= (select top(1) id from devices where address=@slaveAddress and active=1 and hubid=@hubid)
			if(@deviceId is null)
			begin
				INSERT INTO [dbo].[Devices]
			   ([Address]
			   ,[MaxChannelCount]
			   ,[DeviceModel]
			   ,[HubId])
				VALUES(@slaveAddress,0,@deviceModel,@hubId)
				set @deviceId=SCOPE_IDENTITY()
			end
			set @socketid= (select top(1) id from Sockets where name=@socketName and broken=0)
			if(@socketid is null)
			begin
				INSERT INTO [dbo].[Sockets]
			   ([Name]
			   ,[LoRange]
			   ,[HiRange]
			   ,[LoRangeH]
			   ,[HiRangeH]
			   ,[DeviceId]
			   ,[DeviceChannelNumber]
			   ,[Broken]
			   )
				 VALUES
				 (
				 @socketName,-2000,500,-2000,500,@deviceid,@channel-1,0
				 )

			set @socketid=scope_identity()
			update devices set MaxChannelCount=MaxChannelCount+1 where id=@deviceid
			
	 end else 
	 begin
		update Sockets set LoRange=-2000,[HiRange]=500,[LoRangeH]=-2000,[HiRangeH]=500,[DeviceId]=@deviceid,[DeviceChannelNumber]=@channel-1,[Broken]=0 where id=@socketid

		update devices set MaxChannelCount=p.maxchannel
		from devices d
		join (select deviceid, max([DeviceChannelNumber])+1 as maxchannel from sockets group by DeviceId ) p on p.DeviceId=d.Id
		
	 end

		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			SELECT
    ERROR_NUMBER() AS ErrorNumber
    ,ERROR_SEVERITY() AS ErrorSeverity
    ,ERROR_STATE() AS ErrorState
    ,ERROR_PROCEDURE() AS ErrorProcedure
    ,ERROR_LINE() AS ErrorLine
    ,ERROR_MESSAGE() AS ErrorMessage;
			RETURN
		END CATCH

	COMMIT TRANSACTION
END

