﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Alarms_GetNotificationsToSend__old]
	@Type int
	,@MaxRows int = 20
AS
BEGIN
	SET NOCOUNT ON;
		
	DECLARE			--typy powiadomień
		  @GUI			tinyint = 0
		, @SMS			tinyint = 1
		, @EMAIL		tinyint = 2
	DECLARE @now datetime = dbo.GetCurrentDate()
	DECLARE @hyperAdmin int = 4	



	IF(@Type NOT IN(@SMS, @EMAIL))
	BEGIN
		SELECT null AS ID, null AS UserID, null AS Message, null AS Address
		RETURN
	END

	DECLARE @notifications TABLE(
		ID int
		,SensorID smallint)

	INSERT @notifications
	SELECT ID, SensorID
	FROM dbo.AlarmNotifications
	WHERE SendAfterDate <= @now
		AND SentDate IS NULL
		AND Type = @Type
		AND DeleteDate IS NULL

	
	BEGIN TRANSACTION
		BEGIN TRY	
			--wstaw do powiadomienia treść wiadomości 
			UPDATE dbo.AlarmNotifications SET 
					Message = dbo.GetAlarmNotifyText(AlarmID, Type)
					,SentDate = @now
					,Info = Info + ' | ' + CONVERT(nvarchar(30), @now, 121) + ':alarms_GetNotificationsToSend(Ustawiono Message, SentDate)'
			WHERE ID IN (SELECT ID FROM @notifications)
			--wyznacz userów
			IF(@Type = @SMS)
			BEGIN
				INSERT dbo.AlarmNotificationsLog(NotificationID, ReceiverID, Address)
				SELECT n.ID, nbs.UserId, u.Phone
				FROM @notifications n 
					JOIN dbo.AlarmNotifyBySms nbs WITH(NOLOCK) ON nbs.SensorId = n.SensorID
					JOIN dbo.Users u WITH(NOLOCK) ON u.ID = nbs.UserId
			END
			ELSE IF(@Type = @EMAIL)
			BEGIN
				INSERT dbo.AlarmNotificationsLog(NotificationID, ReceiverID, Address)
				SELECT n.ID, nbs.UserId, u.eMail
				FROM @notifications n 
					JOIN dbo.AlarmNotifyByEmail nbs WITH(NOLOCK) ON nbs.SensorId = n.SensorID
					JOIN dbo.Users u WITH(NOLOCK) ON u.ID = nbs.UserId
			END
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
		END CATCH

		SELECT TOP(@maxRows)
			nl.NotificationID AS ID
			,nl.ReceiverID AS UserID
			, CASE
				WHEN ur.RoleID = @hyperAdmin THEN n.Message + ' ' + dbo.GetSensorHardwarePath(n.SensorID, @Type)
				ELSE n.Message
			  END AS  Message
			, nl.Address
		FROM dbo.AlarmNotificationsLog nl WITH(NOLOCK)
			JOIN dbo.AlarmNotifications n WITH(NOLOCK) ON n.ID = nl.NotificationID AND n.SendAfterDate <= @now AND n.Type = @Type AND n.DeleteDate IS NULL
			LEFT JOIN dbo.UsersRoles ur WITH(NOLOCK) ON ur.UserID = nl.ReceiverID
		WHERE nl.SentDate IS NULL
			AND nl.DeleteDate IS NULL
END

