﻿CREATE PROCEDURE fda_GetBandsInfoFilteredTotalCount
@TreeUnitIds nvarchar(max),
@IdFrom smallint = null
,@IdTo smallint = null
,@BarCode nvarchar(200) = null
,@MacString nvarchar(200) = null
,@UidFrom bigint = null
,@UidTo bigint = null
,@SerialNumber nvarchar(200) = null
,@BatteryTypeIdFrom tinyint = null
,@BatteryTypeIdTo tinyint = null
,@BatteryTypeName nvarchar(200) = null
,@BatteryAlarmSeverityIdFrom int = null
,@BatteryAlarmSeverityIdTo int = null
,@BatteryAlarmSeverityName nvarchar(200) = null
,@BatteryInstallationDateUtcFrom datetime = null
,@BatteryInstallationDateUtcTo datetime = null
,@BatteryDaysToExchangeFrom int = null
,@BatteryDaysToExchangeTo int = null
,@Description nvarchar(200) = null
,@ObjectTypeIdFrom tinyint = null
,@ObjectTypeIdTo tinyint = null
,@ObjectTypeName nvarchar(200) = null
,@ObjectDisplayName nvarchar(200) = null
,@UnitIdFrom int = null
,@UnitIdTo int = null
,@UnitName nvarchar(200) = null
,@UnitLocation nvarchar(200) = null
,@CurrentUnitIdFrom int = null
,@CurrentUnitIdTo int = null
,@CurrentUnitName nvarchar(200) = null
,@CurrentUnitLocation nvarchar(200) = null
AS
BEGIN

 DECLARE @cmd nvarchar(max)
 SET @cmd = 'SELECT COUNT(1) '
		+ dbo.dsql_GetBandsInfo_From()
		+ dbo.dsql_GetBandsInfo_Where(@TreeUnitIds,
			@IdFrom, @IdTo, @BarCode, @MacString, @UidFrom, @UidTo
			, @SerialNumber, @BatteryTypeIdFrom, @BatteryTypeIdTo, @BatteryTypeName, @BatteryAlarmSeverityIdFrom, @BatteryAlarmSeverityIdTo
			, @BatteryAlarmSeverityName, @BatteryInstallationDateUtcFrom, @BatteryInstallationDateUtcTo, @BatteryDaysToExchangeFrom, @BatteryDaysToExchangeTo, @Description
			, @ObjectTypeIdFrom, @ObjectTypeIdTo, @ObjectTypeName, @ObjectDisplayName, @UnitIdFrom, @UnitIdTo
			, @UnitName, @UnitLocation, @CurrentUnitIdFrom, @CurrentUnitIdTo, @CurrentUnitName, @CurrentUnitLocation
			)

 EXEC(@cmd)
-- print @cmd
END