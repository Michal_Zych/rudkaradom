﻿

CREATE PROCEDURE [dbo].[GetEnumDictionary] 
(
	@EnumName nvarchar(100)
)
AS
BEGIN
	DECLARE @sql nvarchar(max) = ''

	SELECT @sql = @sql + '[' + COLUMN_NAME + '], '  FROM information_schema.columns 
	WHERE TABLE_SCHEMA = 'enum' 
		AND TABLE_NAME = @enumName
	ORDER BY ORDINAL_POSITION

	SELECT @sql	= LEFT(@sql, LEN(@sql) -1)

	SELECT @sql = 'SELECT * FROM enum.' + @enumName + ' as t UNPIVOT (Value FOR Description IN('
		+ @sql + ') ) as u;'

	EXEC (@sql)
END