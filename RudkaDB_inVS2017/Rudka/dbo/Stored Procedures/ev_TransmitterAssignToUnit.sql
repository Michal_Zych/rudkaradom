﻿
CREATE PROCEDURE [dbo].[ev_TransmitterAssignToUnit]
	@MacStr nvarchar(20)
	,@UnitId int
	,@OperatorId int
	,@Reason nvarchar(max) = null
	,@OperationDateUtc dateTime = null
AS
BEGIN
	SET NOCOUNT ON;

	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
		BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'UnitId=' + COALESCE(CONVERT(nvarchar(100), @UnitId), 'NULL') 
		EXEC Log_Action @@PROCID, @info
	END
	/****************************************************/

	DECLARE
		@UPDATE_ENDING_EVENT_ID bit = 1

	DECLARE 
		@errorString nvarchar(1024) = ''
		,@macForPatternCount int
		,@transmitterId int
		,@currentDate dateTime 
		,@eventId int

	IF EXISTS (SELECT 1 FROM dbo.Units WHERE ParentUnitID = @UnitId)
		SELECT @errorString = 'Strefa nie może posiadać podrzędnych jednostek organizacyjnych'
	ELSE IF COALESCE(@MacStr, '') = ''
		SELECT @errorString = 'Nie podano adresu MAC'
	ELSE BEGIN
		SELECT @macForPatternCount = COUNT(1) FROM dbo.dev_Transmitters WHERE MacString LIKE '%' + @MacStr 
		IF @macForPatternCount = 0
			SELECT @errorString = 'Nie znaleziono transmitera z podanym adresem MAC: ' + @MacStr 
		ELSE IF @macForPatternCount > 1
			SELECT @errorString = 'Więcej niż jeden transmiter pasuje do wzorca MAC: %' + @MacStr 
		ELSE 
			SELECT @transmitterId = Id FROM dbo.dev_Transmitters WHERE MacString LIKE '%' + @MacStr
	END

	IF @errorString <> ''
	BEGIN
		SELECT CONVERT(int, null) AS Result, 1 AS ErrorCode, @errorString AS ErrorMessage
		RETURN
	END
	
	SELECT @currentDate = COALESCE(@OperationDateUtc, dbo.GetCurrentUtcDate())

	BEGIN TRY
		BEGIN TRANSACTION
			DELETE dbo.dev_UnitTransmitter WHERE UnitID = @UnitId
			INSERT dbo.dev_UnitTransmitter (UnitID, TransmitterId) VALUES (@UnitId, @transmitterId)

			DECLARE @JSON nvarchar(max) = 
			'SELECT t.Id AS ID, t.MacString, t.Name AS Name, u.ID AS UnitId, u.Name AS UnitName, dbo.GetUnitLocation(u.Id, default, default, default) AS UnitLocation
			FROM dbo.dev_Transmitters t JOIN dbo.dev_UnitTransmitter ut ON t.Id = ut.TransmitterId JOIN dbo.Units u ON u.Id = ut.UnitID WHERE t.Id = '
			+ CONVERT(nvarchar(10), @transmitterId)
			EXEC @JSON = dbo.SerializeJSON @JSON

			INSERT dbo.ev_Events(DateUtc, Type, Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason) 
			SELECT @currentDate, eventType.TransmitterAssignment, eventSeverity.Event, @UnitId, @transmitterId, objectType.Transmitter, @UnitId, @currentDate, @currentDate, @OperatorId, @JSON, @Reason
			FROM enum.EventType eventType
			JOIN enum.EventSeverity eventSeverity ON 1 = 1
			JOIN enum.ObjectType objectType ON 1 = 1
			SELECT @eventId = SCOPE_IDENTITY()
			IF @UPDATE_ENDING_EVENT_ID = 1
			BEGIN
				UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
			END


		COMMIT TRANSACTION
		SELECT @transmitterId AS Result, NULL as ErrorCode, NULL as ErrorMessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		SELECT CONVERT(int, null) AS Result, ERROR_NUMBER() AS ErrorCode, ERROR_MESSAGE() AS ErrorMessage
	END CATCH



END