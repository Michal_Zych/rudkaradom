﻿CREATE PROCEDURE [dbo].[ev_AssignPatientToWardRoom_core]
	@PatientId int
	,@CodeType nvarchar(10)
	,@Code nvarchar(100)

	,@NewWardId int
	,@NewRoomId int

	,@OperatorId int
	,@Reason nvarchar(max) = null

	,@IsExternalEvent bit = 0  
	,@OperationDateUtc dateTime = null
AS
BEGIN
	SET NOCOUNT ON;

	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
		BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'PatientId=' + COALESCE(CONVERT(nvarchar(100), @PatientId), 'NULL') 
		EXEC Log_Action @@PROCID, @info
	END
	/****************************************************/

	DECLARE
		@UPDATE_ENDING_EVENT_ID bit = 1

	


	DECLARE
		@WARD_FOR_UNASSIGNED int
		,@SEVERITY_EVENT dbo.EVENT_SEVERITY
		,@OBJECT_TYPE_PATIENT dbo.OBJECT_TYPE
		
		,@currentWardId int
		,@currentRoomId int
		,@currentAddDateUtc dateTime

		,@removeFromRoom bit = 0
		,@removeFromWard bit = 0
		,@addToRoom bit = 0
		,@addToWard bit = 0

		,@eventId int
		,@unitId int
		,@errorMessage nvarchar(max)

	IF @PatientId IS NULL  -- identyfikacja pacjenta
	BEGIN
		DECLARE @patientsForCode TABLE(Id int)
		DECLARE @count int

		INSERT @patientsForCode
		SELECT p.Id
		FROM dbo.Patients p
		JOIN enum.ObjectType objectType ON 1 = 1
		LEFT JOIN dbo.ev_BandStatus bs ON bs.ObjectType = objectType.Patient AND bs.ObjectId = p.Id
		LEFT JOIN dbo.Bands b ON bs.BandId = b.Id
		WHERE b.BarCode = @Code 
			OR b.SerialNumber = @Code
			OR p.Pesel = @Code
			OR p.LastName + ',' + p.Name = @Code
	
		SELECT  @count = COUNT(1) FROM @patientsForCode
	
		IF @count = 0 
		BEGIN
			SELECT @errorMessage = 'Nie można zidentyfikować pacjenta po kodzie ' + COALESCE(@Code, 'NULL')
			RAISERROR(@errorMessage, 16, 1)
		END

		IF @count > 1
		BEGIN
			SELECT @errorMessage = 'Kryteria wyszukiwania pasują do więcej niż jednego pacjenta, kod ' + COALESCE(@Code, 'NULL')
			RAISERROR(@errorMessage, 16, 1)
		END
		
		SELECT @PatientId = Id FROM @patientsForCode
	END


	IF @OperationDateUtc IS NULL SELECT @OperationDateUtc = dbo.GetCurrentUTCDate()
	
	SELECT @WARD_FOR_UNASSIGNED = UnassignedPatientsUnitid
	FROM dbo.Clients u
	WHERE Id = 1

	IF @NewWardId IS NULL SELECT @NewWardId = @WARD_FOR_UNASSIGNED, @NewRoomId = NULL

	SELECT 
			@currentAddDateUtc = COALESCE(p.RoomDateUtc, p.WardDateUtc)
			,@currentWardId = p.WardId
			,@currentRoomId = p.RoomId
			
			,@SEVERITY_EVENT = eventSeverity.Event
			,@OBJECT_TYPE_PATIENT = objectType.Patient
	FROM dbo.Patients p
	JOIN enum.EventSeverity eventSeverity ON 1 = 1
	JOIN enum.ObjectType objectType ON 1 = 1

	-- brak zmian
	IF @NewWardId = @currentWardId AND COALESCE(@NewRoomId, -1) = COALESCE(@currentRoomId, -1)
	BEGIN  
		SELECT @PatientId AS Result, NULL as ErrorCode, NULL as ErrorMessage
		RETURN
	END
	
	IF @currentAddDateUtc > @OperationDateUtc
	BEGIN
		IF @IsExternalEvent = 1 EXEC dbo.sync_PatientAssigned @PatientId
		SELECT @errorMessage = 'System posiada świeższe dane o przypisaniu pacjenta id=' + CONVERT(nvarchar(10), @PatientId)
		RAISERROR(@errorMessage, 16, 1)
	END

	IF @currentWardId	IS NOT NULL		AND COALESCE(@NewWardId, -1)		<> @currentWardId	SELECT @removeFromWard = 1
	IF @NewWardId		IS NOT NULL		AND COALESCE(@currentWardId, -1)	<> @NewWardId		SELECT @addToWard = 1
	IF @currentRoomId	IS NOT NULL		AND COALESCE(@NewRoomId, -1)		<> @currentRoomId	SELECT @removeFromRoom = 1
	IF @NewRoomId		IS NOT NULL		AND COALESCE(@currentRoomId, -1)	<> @NewRoomId		SELECT @addToRoom = 1


	IF @removeFromRoom = 1
	BEGIN
		INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason)
		SELECT @OperationDateUtc, eventType.RegisterOutRoom, @SEVERITY_EVENT, @currentRoomId, @PatientId, @OBJECT_TYPE_PATIENT, @currentWardId, @OperationDateUtc, @OperationDateUtc, @OperatorId, NULL, NULL
		FROM enum.EventType eventType
		IF @UPDATE_ENDING_EVENT_ID = 1
		BEGIN
			SELECT @eventId = SCOPE_IDENTITY()
			UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
		END
	END
	
	IF @removeFromWard = 1
	BEGIN
		INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason)
		SELECT @OperationDateUtc, eventType.RegisterOutDepartment, @SEVERITY_EVENT, @currentWardId, @PatientId, @OBJECT_TYPE_PATIENT, @WARD_FOR_UNASSIGNED, @OperationDateUtc, @OperationDateUtc, @OperatorId, NULL, NULL
		FROM enum.EventType eventType
		IF @UPDATE_ENDING_EVENT_ID = 1
		BEGIN
			SELECT @eventId = SCOPE_IDENTITY()
			UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
		END
	END
	
	IF @addToWard = 1
	BEGIN
		INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason)
		SELECT @OperationDateUtc, eventType.RegisterInDepartment, @SEVERITY_EVENT, @NewWardId, @PatientId, @OBJECT_TYPE_PATIENT, @NewWardId, @OperationDateUtc, @OperationDateUtc, @OperatorId, NULL, @Reason
		FROM enum.EventType eventType
		IF @UPDATE_ENDING_EVENT_ID = 1
		BEGIN
			SELECT @eventId = SCOPE_IDENTITY()
			UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
		END
	END
	
	IF @addToRoom = 1 		
	BEGIN
		INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason)
		SELECT @OperationDateUtc, eventType.RegisterInRoom, @SEVERITY_EVENT, @NewRoomId, @PatientId, @OBJECT_TYPE_PATIENT, @NewRoomId, @OperationDateUtc, @OperationDateUtc, @OperatorId, NULL, @Reason
		FROM enum.EventType eventType
		IF @UPDATE_ENDING_EVENT_ID = 1
		BEGIN
			SELECT @eventId = SCOPE_IDENTITY()
			UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
		END
	END

	UPDATE dbo.Patients SET 
		WardId			= @NewWardId
		,WardDateUtc	= CASE WHEN COALESCE(WardId, -1) <> COALESCE(@NewWardId, -1) THEN @OperationDateUtc ELSE WardDateUtc END
		,RoomId			= @NewRoomId
		,RoomDateUtc	= CASE WHEN COALESCE(RoomId, -1) <> COALESCE(@NewRoomId, -1) THEN @OperationDateUtc ELSE RoomDateUtc END
	WHERE Id = @PatientId
	
	UPDATE bs SET
		ObjectUnitId = COALESCE(@NewRoomId, @NewWardId)
	FROM ev_BandStatus bs
	JOIN enum.ObjectType objectType ON bs.ObjectType = objectType.Patient 
	WHERE bs.ObjectId = @PatientId 


	IF @IsExternalEvent = 0 EXEC dbo.sync_PatientAssigned @PatientId

	-- uaktualnienie konfiguracji alarmowania
	SELECT @unitId = COALESCE(@NewRoomId, @NewWardId)
	EXEC al_PatientAlarmConfigCopyFromUnit_core @PatientId, @unitId, @OperatorId, @Reason

END