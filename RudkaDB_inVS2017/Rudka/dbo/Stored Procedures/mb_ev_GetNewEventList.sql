﻿-- =============================================
-- Author:		kt
-- Create date: 2018-01-16
-- Description:	gets all new events for push service
-- =============================================
CREATE PROCEDURE [dbo].[mb_ev_GetNewEventList]
	@lastId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		e.Id,
		Type,
		Severity,
		DateUtc AS StartTime,
		EndDateUtc AS EndTime,
		p.Name + ' ' + p.LastName AS PatientDisplayName,
		p.Id AS PatientId
	FROM dbo.ev_Events e
	INNER JOIN enum.ObjectType ot ON 1 = 1
	INNER JOIN dbo.Patients p on e.ObjectId = p.id
	WHERE
		e.ObjectType = ot.Patient
		AND e.Id > @lastId
		AND e.DateUtc > DATEADD(dd, -10, dbo.GetCurrentDate())
	ORDER BY
		e.Id DESC

END
