﻿CREATE PROCEDURE [dbo].[_tech_GenerateFDAparameters]
	@ProcName nvarchar(200)
AS
BEGIN
	DECLARE @NEW_LINE nvarchar(10) = CHAR(13) + CHAR(10)
	DECLARE @TABS nvarchar(10) = CHAR(9) + CHAR(9) + CHAR(9) +CHAR(9) +CHAR(9)

	SELECT
		Name,  type_name(user_type_id) AS [Type], max_length AS Length
		,case when type_name(system_type_id) = 'uniqueidentifier' 
              then precision  
              else OdbcPrec(system_type_id, max_length, precision) 
			end AS [Precision]
		,OdbcScale(system_type_id, scale) AS Scale
	FROM sys.parameters 
	WHERE object_id = object_id(@ProcName)
	ORDER BY parameter_id

	DECLARE @execStr nvarchar(max) = CHAR(9) + CHAR(9) + CHAR(9) + 'const string sql = "EXEC ' + @ProcName + ' "' 
	DECLARE @i int = 0

	SELECT @execStr = @execStr 
		+ CASE WHEN @i = 0 THEN '' ELSE CASE WHEN @i % 7 = 0 THEN ', "' ELSE ', ' END END
		+ CASE WHEN @i % 7 = 0 THEN @NEW_LINE + @TABS + '+ "' ELSE '' END
		+ Name 
		,@i = @i + 1
	FROM  sys.parameters 
	WHERE object_id = object_id(@ProcName)
	ORDER BY parameter_id

	SELECT REPLACE(LEFT(@execStr, LEN(@execStr) - LEN(@NEW_LINE)), '@', ':') + '";' + @NEW_LINE 
END