﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	Edycja uprawnień użytkownika o podanym ID
-- =============================================
CREATE PROCEDURE [dbo].[usr_EditUserPermissions]
(
	  @userId int
	 ,@roleId int
	 ,@units IDs_LIST
	, @operatorId int
	, @logEntry nvarchar(max)
	, @reason nvarchar(max)
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @OPERATION_DELETE tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_EDIT tinyint = 2

		,@RECORD_UNIT tinyint = 0
		,@RECORD_USER tinyint = 1

		--kody błędów
	DECLARE
		@ERROR int = -1
	
	BEGIN TRANSACTION
		BEGIN TRY
			DELETE FROM dbo.UsersUnitsRoles WHERE UserID = @userId
			DELETE FROM dbo.UsersRoles WHERE UserID = @userId

			INSERT INTO dbo.UsersUnitsRoles 
			SELECT @userId, value, @roleID
			FROM fn_Split(@units, ',')
	
			INSERT INTO dbo.UsersRoles (UserID,  RoleID)
			VALUES                 (@userId, @roleId)

			INSERT dbo.Logs(OperationDate, OperatorId, OperationType, ObjectType, ObjectId, Reason, Info)
					VALUES(dbo.GetCurrentDate(), @operatorId, @OPERATION_EDIT, @RECORD_USER, @userId, @reason, @logEntry)

		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			SELECT @ERROR
			RETURN
		END CATCH
	COMMIT TRANSACTION

	SELECT @userId
END

