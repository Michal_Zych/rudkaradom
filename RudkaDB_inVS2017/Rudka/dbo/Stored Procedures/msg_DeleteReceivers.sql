﻿

CREATE PROCEDURE [dbo].[msg_DeleteReceivers] 
	@message int
	,@receivers IDs_LIST
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @IDs TABLE (ID int not null)
	INSERT @IDs SELECT Value FROM  dbo.SplitWordsToInt(@receivers)

	DECLARE @result bit = 0

	BEGIN TRANSACTION
		BEGIN TRY
			IF (@receivers IS NULL) OR (@receivers = '')
				DELETE dbo.MessagesLog WHERE MessageID = @message
			ELSE
				DELETE dbo.MessagesLog WHERE MessageID = @message
					AND ReceiverID IN (SELECT ID FROM @IDs)
			SELECT @result = 1
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
		END CATCH
	SELECT @result AS Result
END
