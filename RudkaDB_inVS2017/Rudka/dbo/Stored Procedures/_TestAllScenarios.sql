﻿CREATE PROCEDURE [dbo].[_TestAllScenarios]
AS
BEGIN
	UPDATE _test_Cases SET IsLastRunOk = NULL

	DECLARE @id int
		,@RunDate dateTime

	SET @id = NULL
	SELECT TOP 1 @id = Id, @RunDate = RunDate FROM _test_Cases WHERE RunDate IS NULL OR (RunDate IS NOT NULL AND IsLastRunOk IS NULL)
	WHILE @id IS NOT NULL
	BEGIN
		IF @RunDate IS NULL
			EXEC _TestScenario null, @id, 0, 1, 1
		ELSE
			EXEC _TestScenario null, @id, 0, 0, 1
		
		SET @id = NULL
		SELECT TOP 1 @id = Id, @RunDate = RunDate FROM _test_Cases WHERE RunDate IS NULL OR (RunDate IS NOT NULL AND IsLastRunOk IS NULL)
	END

	SELECT * FROM _test_cases WHERE IsLastRunOk = 0

END