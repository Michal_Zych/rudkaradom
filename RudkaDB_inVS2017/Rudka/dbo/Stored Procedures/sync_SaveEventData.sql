﻿CREATE PROCEDURE [dbo].[sync_SaveEventData]
	@Tag nvarchar(max),
	@Record nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	
DECLARE 
		@KEY_EVENT_DATE nvarchar(100) = 'EVENT_DATE'
		,@currentDateUtc dateTime = dbo.GetCurrentUtcDate()

	DECLARE
		@idKey nvarchar(100)
		,@RecIds nvarchar(100)

		,@eventRecordId nvarchar(100)
		,@eventDateStr nvarchar(100)	
		,@eventDate dateTime
		,@queryKey nvarchar(100)
		,@errorMsg nvarchar(max)
		,@eventId int

	SELECT @idKey = IdKey, @RecIds = RecIds, @queryKey = RecQueryKey FROM tech.sync_EventsIdentifiers WHERE EventType = @Tag

	SET @eventDateStr		= dbo.srv_KeyValue_GetValue(@Record, @KEY_EVENT_DATE)
	SET @eventRecordId	= dbo.srv_KeyValue_GetValue(@Record, @idKey)
	
	IF ISDATE(@eventDateStr) = 1 SET @eventDate = DATEADD(SECOND, DATEDIFF(SECOND, dbo.GetCurrentDate(), dbo.GetCurrentUtcDate()), CONVERT(dateTime, @eventDateStr))
	ELSE SET @errorMsg = 'Niepoprawny format daty:' + COALESCE(@eventDateStr, 'NULL')


	INSERT tech.sync_EventsIN(StoreDateUtc, EventDateUtc, EventType, EventRecordId, Record, Error) VALUES 
	(@currentDateUtc, @eventDate, @Tag, @eventRecordId, @Record, @errorMsg)
	SELECT @eventId = SCOPE_IDENTITY()

	INSERT tech.sync_RecordsOUT(EventId, RecType, DateUtc, Record)
	SELECT @eventId, A.value, @currentDateUtc, @queryKey + '=''' + @eventRecordId + ''''
	FROM dbo.fn_Split(@RecIds, ';') A

	RETURN @eventId
END