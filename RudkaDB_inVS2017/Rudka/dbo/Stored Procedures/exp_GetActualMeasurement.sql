﻿

-- =============================================
-- Author:		
-- Create date: 
-- Description:	zwraca magazyny danego klienta
-- =============================================
CREATE PROCEDURE dbo.[exp_GetActualMeasurement]
		@ClientID int		
		,@UnitID int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT m.MeasurementTime AS [Date], m.Value * s.Scale AS Value
	FROM dbo.MeasurementsCurrent m WITH(NOLOCK)
	JOIN dbo.Sensors s WITH(NOLOCK) ON s.Id = m.SensorID
	JOIN dbo.Units u WITH(NOLOCK) ON s.Unitid = u.Id AND u.ClientID = @ClientID AND u.ID = @UnitID
END


