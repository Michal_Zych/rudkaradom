﻿
-- =============================================
-- Author:		
-- Create date: 
-- Description:	Zwraca pominięte zakresy alarmowe dla unita
-- =============================================
CREATE PROCEDURE [dbo].[unt_GetOmittedAlarmRangesForUnit]
	@unitId int
AS
BEGIN
	SET NOCOUNT ON;

	DELETE a
	FROM dbo.AlarmsOmittedRanges a
	JOIN dbo.Sensors s ON a.SensorId = s.Id AND s.UnitId = @unitid
	WHERE DateEnd <= dbo.GetCurrentDate()

	SELECT DateStart, DateEnd
	FROM dbo.AlarmsOmittedRanges a
	JOIN dbo.Sensors s ON a.SensorID = s.Id AND s.UnitId = @unitId
	ORDER BY DateStart
END
