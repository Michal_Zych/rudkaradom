﻿CREATE PROCEDURE [dbo].[ev_StoreLowBatteryStatus]
	@BandId smallint
	,@EventDateUtc dateTime = NULL
	,@IsLowBattery bit = 1
AS
BEGIN
	SET NOCOUNT ON;

/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
		BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'BandId=' + COALESCE(CONVERT(nvarchar(100), @BandId), 'NULL') 
					+ ';EventDateUtc=' + COALESCE(CONVERT(nvarchar(50), @EventDateUtc, 121), 'NULL')
					+ ';IsLowBattery=' + COALESCE(CONVERT(nvarchar(100), @IsLowBattery), 'NULL')
		EXEC Log_Action @@PROCID, @info
	END
/****************************************************/

	DECLARE @UPDATE_ENDING_EVENT_ID bit = 1
		,@SYSTEM_USER_ID int = 0
		,@OBJECT_TYPE_BAND dbo.OBJECT_TYPE



	DECLARE
		@currentDateUtc datetime = dbo.GetCurrentUtcDate()
		,@warningDateUtc dateTime
		,@alarmDateUtc dateTime
		,@bandStatusChanged bit = 0
		,@ongoingAlarmSeverity dbo.EVENT_SEVERITY

	IF @EventDateUtc IS NULL SET @EventDateUtc = @currentDateUtc

BEGIN -- zmienne konfiguracji
	DECLARE
		 @objectType				dbo.OBJECT_TYPE   
		,@objectId					dbo.OBJECT_ID  
		,@objectUnitId				int  
		,@currentUnitId				int  
		,@inCurrentUnitUtc			dateTime  
		,@previousUnitId			int  
		,@inPreviousUnitUtc			dateTime  
		,@topAlarmId				int  
		,@topAlarmType				dbo.EVENT_TYPE   
		,@topAlarmSeverity			dbo.EVENT_SEVERITY   
		,@topAlarmDateUtc			dateTime 
		,@topEventDateUtc			dateTime  
		,@severity1Count			int  
		,@severity2Count			int  
		,@ongNoGoZoneInEventId		int  
		,@ongNoGoZoneInAlarmId		int  
		,@ongDisconnectedEventId	int  
		,@ongDisconnectedAlarmId	int  
		,@ongZoneOutEventId			int  
		,@ongZoneOutAlarmId			int  
		,@ongNoMovingEventId		int  
		,@ongNoMovingAlarmId		int  
		,@ongMovingEventId			int  
		,@ongMovingAlarmId			int  
		,@ongLowBatteryEventId		int  
		,@ongLowBatteryAlarmId		int 

		,@warningActive				bit     
		,@warningDelayMins			smallint
		,@alarmActive				bit     
		,@alarmDelayMins			smallint
		,@warningAutoCloseMessage	nvarchar(max)
		,@currentEventType			dbo.EVENT_TYPE
END

BEGIN	-- pobranie konfiguracji
	SELECT
		 @objectType					= bs.ObjectType			
		,@objectId						= bs.ObjectId				
		,@objectUnitId					= bs.ObjectUnitId			
		,@currentUnitId					= bs.CurrentUnitId			
		,@inCurrentUnitUtc				= bs.InCurrentUnitUtc		
		,@previousUnitId				= bs.PreviousUnitId		
		,@inPreviousUnitUtc				= bs.InPreviousUnitUtc		
		,@topAlarmId					= bs.TopAlarmId			
		,@topAlarmType					= bs.TopAlarmType			
		,@topAlarmSeverity				= bs.TopAlarmSeverity		  
		,@topAlarmDateUtc				= bs.TopAlarmDateUtc		
		,@topEventDateUtc				= bs.TopEventDateUtc		
		,@severity1Count				= bs.Severity1Count		
		,@severity2Count				= bs.Severity2Count		
		,@ongNoGoZoneInEventId			= bs.OngNoGoZoneInEventId  
		,@ongNoGoZoneInAlarmId			= bs.OngNoGoZoneInAlarmId  
		,@ongDisconnectedEventId		= bs.OngDisconnectedEventId
		,@ongDisconnectedAlarmId		= bs.OngDisconnectedAlarmId
		,@ongZoneOutEventId				= bs.OngZoneOutEventId		
		,@ongZoneOutAlarmId				= bs.OngZoneOutAlarmId		
		,@ongNoMovingEventId			= bs.OngNoMovingEventId	
		,@ongNoMovingAlarmId			= bs.OngNoMovingAlarmId	
		,@ongMovingEventId				= bs.OngMovingEventId		
		,@ongMovingAlarmId				= bs.OngMovingAlarmId		
		,@ongLowBatteryEventId			= bs.OngLowBatteryEventId  
		,@ongLowBatteryAlarmId			= bs.OngLowBatteryAlarmId  
		
		,@warningAutoCloseMessage		= st.WarningAutoCloseMessage
		,@warningActive					= st.LoBatteryWrActive
		,@warningDelayMins				= st.LoBatteryWrMins
		,@alarmActive					= st.LoBatteryAlActive
		,@alarmDelayMins				= st.LoBatteryAlMins

		,@currentEventType				= CASE WHEN @IsLowBattery = 1 THEN eventType.LowBattery ELSE eventType.BatteryOk END
		,@OBJECT_TYPE_BAND				= objectType.Band
	FROM dbo.ev_Settings st 
	JOIN enum.EventType eventType ON 1 = 1
	JOIN enum.ObjectType objectType ON 1 = 1
	LEFT JOIN dbo.ev_BandStatus bs ON bs.BandId = @BandId
END	

	--zakończ brak łączności
	IF @ongDisconnectedEventId IS NOT NULL
	BEGIN
		EXEC [dbo].[ev_SetBandConnected]
			@BandId						= @BandId
			,@currentDateUtc			= @currentDateUtc
			,@CurrentEventDateUtc		= @EventDateUtc
			,@CurrentEventUnitId		= @currentUnitId
			,@ObjectId					= @objectId				
			,@ObjectType				= @objectType
			,@ObjectUnitId				= @objectUnitId
			,@CurrentUnitId				= @currentUnitId			OUTPUT
			,@InCurrentUnitUtc			= @inCurrentUnitUtc			OUTPUT
			,@PreviousUnitId			= @previousUnitId			OUTPUT
			,@InPreviousUnitUtc			= @inPreviousUnitUtc		OUTPUT
			,@OngDisconnectedEventId	= @ongDisconnectedEventId	OUTPUT
			,@OngDisconnectedAlarmId	= @ongDisconnectedAlarmId	OUTPUT
		SET @bandStatusChanged = 1
	END

	-- zarejestrowanie rekordu statusu dla opaski
	IF @objectId IS NULL
	BEGIN
		EXEC dbo.ev_AssignBandToItself
			@EventDateUtc		= @EventDateUtc
			,@BandId			= @BandId
			,@ObjectId			= @objectId			OUTPUT
			,@ObjectType		= @objectType		OUTPUT
			,@InCurrentUnitUtc	= @inCurrentUnitUtc	OUTPUT
			,@severity1Count	= @severity1Count	OUTPUT
			,@severity2Count	= @severity2Count	OUTPUT
	END
	
	-- przetwarzaj tylko gdy zmienia sie stan baterii
	IF (@IsLowBattery = 1 AND @ongLowBatteryEventId IS NULL)
		OR (@IsLowBattery = 0 AND @ongLowBatteryEventId IS NOT NULL)
	BEGIN
		SET @bandStatusChanged = 1
		IF @IsLowBattery = 1
		BEGIN -- słaba bateria
			DECLARE @finishedEventId int
			, @finishedAlarmId int
			EXEC dbo.ev_GenerateEvent
					@currentDateUtc				= @currentDateUtc
					,@EventDateUtc				= @EventDateUtc
					,@EventType					= @currentEventType
					,@EventUnitId				= @currentUnitId
					,@EventObjectId				= @objectId
					,@EventObjectType			= @objectType
					,@EventObjectUnitId			= @objectUnitId
					,@EventData					= NULL
					,@IsLongEvent				= 1
					,@FinishedEventId			= @finishedEventId		OUTPUT
					,@FinishedAlarmId			= @finishedAlarmId		OUTPUT
					,@EventId					= @OngLowBatteryEventId	OUTPUT
			/*
			INSERT ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, EndingEventId)
			SELECT @EventDateUtc, @currentEventType, eventSeverity.Event, @currentUnitId, @objectId, @objectType, @objectUnitId, @currentDateUtc, null, null
			FROM enum.EventSeverity eventSeverity
			SET @OngLowBatteryEventId = SCOPE_IDENTITY()
			*/
			-- wygeneruj ostrzeżenia/alarmy
			EXEC dbo.ev_GenerateAlarmsForEvent 
				@EventObjectType			= @OBJECT_TYPE_BAND
				,@EventObjectid				= @BandId
				,@currentDateUtc			= @currentDateUtc
				,@CurrentEventDateUtc		= @EventDateUtc
				,@currentEventId			= @OngLowBatteryEventId
				,@currentEventType			= @currentEventType
				,@currentUnitId				= @currentUnitId
				,@objectId					= @objectId
				,@objectType				= @objectType
				,@objectUnitId				= @objectUnitId
				,@warningCloseMessage		= @warningAutoCloseMessage
				,@WarningActive				= @warningActive	
				,@WarningDelayMins			= @warningDelayMins	
				,@AlarmActive				= @alarmActive	
				,@AlarmDelayMins			= @alarmDelayMins	
				,@CurrrentAlarmSeverity		= @ongoingAlarmSeverity OUTPUT
				,@CurrrentAlarmId			= @ongLowBatteryAlarmId OUTPUT
				,@warningDateUtc			= @warningDateUtc OUTPUT
				,@alarmDateUtc				= @alarmDateUtc	OUTPUT
				,@severity1Count			= @severity1Count OUTPUT
				,@severity2Count			= @severity2Count OUTPUT
		END
		ELSE
		BEGIN -- bateria ok
			DECLARE @batteryOkEevntId int
			EXEC dbo.ev_GenerateEvent
					@currentDateUtc				= @currentDateUtc
					,@EventDateUtc				= @EventDateUtc
					,@EventType					= @currentEventType
					,@EventUnitId				= @currentUnitId
					,@EventObjectId				= @objectId
					,@EventObjectType			= @objectType
					,@EventObjectUnitId			= @objectUnitId
					,@EventData					= NULL
					,@IsLongEvent				= 0
					,@FinishedEventId			= @ongLowBatteryEventId	OUTPUT
					,@FinishedAlarmId			= @OngLowBatteryAlarmId	OUTPUT
					,@EventId					= @batteryOkEevntId		OUTPUT
			/*
			-- skasuj alarmy do podniesiena
			DELETE dbo.ev_EventsToRaise WHERE RaisingObjectType = @OBJECT_TYPE_BAND AND RaisingObjectId = @BandId AND RaisingEventId = @ongLowBatteryEventId

			-- wygeneruj zdarzenie bateria OK
			INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc) 
			SELECT @EventDateUtc, @currentEventType, EventSeverity.Event, @CurrentUnitId, @ObjectId, @ObjectType, @ObjectUnitId, @currentDateUtc, @EventDateUtc
			FROM enum.EventSeverity EventSeverity
			SET @batteryOkEevntId = SCOPE_IDENTITY()
			IF @UPDATE_ENDING_EVENT_ID = 1
			BEGIN
				UPDATE dbo.ev_Events SET EndingEventId = @batteryOkEevntId WHERE Id = @batteryOkEevntId
			END
			
			-- zakończ zdarzenia/alarmy słabej baterii
			UPDATE dbo.ev_Events SET
				EndDateUtc = @EventDateUtc
				,EndingEventId = @batteryOkEevntId
			WHERE Id IN(@ongLowBatteryEventId, @OngLowBatteryAlarmId)
			SELECT @ongLowBatteryEventId = NULL, @OngLowBatteryAlarmId = NULL
			*/
		END
	END

	-- uaktualnij status
	IF @bandStatusChanged = 1
		EXEC dbo.ev_GenerateBandStatus	
			 @BandId					= @BandId 
			,@CurrentUnitId 			= @CurrentUnitId 
			,@InCurrentUnitUtc			= @InCurrentUnitUtc
			,@PreviousUnitId 			= @PreviousUnitId 
			,@InPreviousUnitUtc 		= @InPreviousUnitUtc 
			,@TopAlarmId 				= @TopAlarmId 
			,@TopAlarmType  			= @TopAlarmType  
			,@TopAlarmSeverity 			= @TopAlarmSeverity 
			,@TopAlarmDateUtc 			= @TopAlarmDateUtc 
			,@TopEventDateUtc 			= @TopEventDateUtc 
			,@Severity1Count 			= @Severity1Count 
			,@Severity2Count 			= @Severity2Count 
			,@OngNoGoZoneInEventId 		= @OngNoGoZoneInEventId 
			,@OngNoGoZoneInAlarmId  	= @OngNoGoZoneInAlarmId  
			,@OngDisconnectedEventId  	= @OngDisconnectedEventId 
			,@OngDisconnectedAlarmId  	= @OngDisconnectedAlarmId 
			,@OngZoneOutEventId  		= @OngZoneOutEventId  
			,@OngZoneOutAlarmId  		= @OngZoneOutAlarmId  
			,@OngNoMovingEventId  		= @OngNoMovingEventId  
			,@OngNoMovingAlarmId  		= @OngNoMovingAlarmId  
			,@OngMovingEventId  		= @OngMovingEventId  
			,@OngMovingAlarmId  		= @OngMovingAlarmId  
			,@OngLowBatteryEventId  	= @OngLowBatteryEventId  
			,@OngLowBatteryAlarmId 		= @OngLowBatteryAlarmId 
			,@currentEventId			= @OngLowBatteryEventId
			,@CurrentEventType 			= @CurrentEventType 
			,@CurrentEventDateUtc 		= @EventDateUtc 
			,@GeneratedAlarmId			= @OngLowBatteryAlarmId 
			,@GeneratedAlarmSeverity 	= @ongoingAlarmSeverity 
			,@GeneratedWarningDateUtc 	= @warningDateUtc
			,@GeneratedAlarmDateUtc 	= @alarmDateUtc 
END
