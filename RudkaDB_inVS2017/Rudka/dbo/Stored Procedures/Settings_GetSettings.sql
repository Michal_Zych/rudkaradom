﻿

CREATE PROCEDURE [dbo].[Settings_GetSettings]
	@ClientId int
AS
BEGIN
	SELECT Name, 
		COALESCE(CONVERT (nvarchar(1024), [dbo].[settings_GetInt](ClientId, Name))
			, [dbo].[settings_GetString](ClientId, Name)) AS Value
	FROM dbo.Settings
	WHERE ClientId = @ClientId
	ORDER BY Name
END

