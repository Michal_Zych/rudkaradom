﻿
CREATE PROCEDURE [dbo].[al_PatientAlarmConfigCopyFromUnit_core]
(
	@PatientId int,
	@UnitId int,

	@OperatorId int,
	@Reason nvarchar(max) = null
)
 AS  
 BEGIN
	DECLARE @UPDATE_ENDING_EVENT_ID bit = 1

	DECLARE
		@EVENT_UPDATE dbo.EVENT_TYPE
		,@EVENT_CREATE dbo.EVENT_TYPE
		,@OBJECT_TYPE_PATIENT dbo.OBJECT_TYPE
		,@OBJECT_TYPE_PATIENTALARMCONFIG dbo.OBJECT_TYPE
		,@OBJECT_TYPE_UNIT dbo.OBJECT_TYPE
		,@SEVERITY_EVENT dbo.EVENT_SEVERITY
		,@eventType dbo.EVENT_TYPE
		,@currentDate datetime
		,@eventId int

	SELECT @EVENT_CREATE = [Create], @EVENT_UPDATE = [Update] FROM enum.EventType
	SELECT @OBJECT_TYPE_PATIENTALARMCONFIG = PatientAlarmConfiguration, @OBJECT_TYPE_PATIENT = Patient, @OBJECT_TYPE_UNIT = Unit
	FROM enum.ObjectType
	SELECT @SEVERITY_EVENT = [Event] FROM enum.EventSeverity

	SET @currentDate = dbo.GetCurrentUtcDate()

	IF EXISTS(SELECT 1 FROM dbo.al_ConfigPatients WHERE PatientId = @PatientId)
		SET @eventType = @EVENT_UPDATE
	ELSE SET @eventType = @EVENT_CREATE



	IF @eventType = @EVENT_UPDATE
	BEGIN
		DELETE dbo.al_CallingGroups WHERE Id = @PatientId AND ObjectType = @OBJECT_TYPE_PATIENT
		DELETE dbo.al_NoGoZones WHERE Id = @PatientId AND ObjectType = @OBJECT_TYPE_PATIENT
		DELETE dbo.al_ConfigPatients WHERE PatientId = @PatientId
	END


	INSERT dbo.al_ConfigPatients
	SELECT @PatientId,
		IsSOScalling, IsACCactive, DMoveWrActive, DMoveWrMins, DMoveAlActive, DMoveAlMins, DNoMoveWrActive,
		DNoMoveWrMins, DNoMoveAlActive, DNoMoveAlMins, NMoveWrActive, NMoveWrMins, NMoveAlActive,
		NMoveAlMins, NNoMoveWrActive, NNoMoveWrMins, NNoMoveAlActive, NNoMoveAlMins, OutOfZoneWrActive,
		OutOfZoneWrMins, OutOfZoneAlActive, OutOfZoneAlMins, InNoGoZoneMins, NoConnectWrActive, NoConnectWrMins,
		NoConnectAlActive, NoConnectAlMins
	FROM dbo.al_ConfigUnits
	WHERE UnitId = @UnitId

	-- calling groups
	INSERT dbo.al_CallingGroups
	SELECT @PatientId, @OBJECT_TYPE_PATIENT, SequenceNr, Phone, Description
	FROM dbo.al_CallingGroups
	WHERE Id = @UnitId AND ObjectType = @OBJECT_TYPE_UNIT
	
	--no go zones
	INSERT dbo.al_NoGoZones
	SELECT @PatientId, @OBJECT_TYPE_PATIENT, UnitId
	FROM dbo.al_NoGoZones
	WHERE Id = @UnitId AND ObjectType = @OBJECT_TYPE_UNIT
	 
	-- event	
	DECLARE @JSON nvarchar(max)
	EXEC @JSON = dbo.SerializeJSON_PatientAlarmConfiguration @PatientId

	INSERT dbo.ev_Events(DateUtc, Type, Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason) 
	SELECT @currentDate, @eventType, @SEVERITY_EVENT, COALESCE(RoomId, WardId), Id, @OBJECT_TYPE_PATIENTALARMCONFIG, COALESCE(RoomId, WardId), @currentDate, @currentDate, @OperatorId, @JSON, @Reason
	FROM dbo.Patients
	WHERE Id = @PatientId
	SELECT @eventId = SCOPE_IDENTITY()
	IF @UPDATE_ENDING_EVENT_ID = 1
	BEGIN
		UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
	END

END