﻿
-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usr_GetUsersList]
	@userId int
	,@unitId int
AS
BEGIN
	SET NOCOUNT ON;

	select distinct ID, Name, LastName, Login
	from [dbo].[UsersUnitsRoles] uur
	inner join users u on u.id=uur.userid
	where 
	((@unitId is  null) or (uur.UnitID in (select id from [dbo].[GetParentsWithUnit](@unitId)))) --userzy którzy mają dostęp do unita

	AND uur.userid in (select userid from [dbo].[User_GetDescendantsAndSiblings](@userId)) -- userzy do których dany user mam dostęp
	AND u.Active = 1
	AND u.ClientID = (SELECT ClientID FROM dbo.Users where ID = @userId)

END

