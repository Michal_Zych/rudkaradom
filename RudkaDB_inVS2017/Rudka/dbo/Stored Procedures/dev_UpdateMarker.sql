﻿-- =============================================
-- Author:		kt
-- Create date: 2017-10-31
-- Description:	updates history marker
-- =============================================
CREATE PROCEDURE [dbo].[dev_UpdateMarker]
	@senseId int,
	@fullDataTime datetime
AS
BEGIN
	SET NOCOUNT ON;

	IF (EXISTS(SELECT (1) FROM dbo.dev_HistoryMarkers WHERE SenseId = @senseId))
	BEGIN
		UPDATE
			dbo.dev_HistoryMarkers
		SET
			FullDataTime = @fullDataTime
		WHERE
			SenseId = @senseId
	END
	ELSE
	BEGIN
		INSERT INTO dbo.dev_HistoryMarkers(SenseId, FullDataTime) 
		VALUES(@senseId, @fullDataTime)
	END

END