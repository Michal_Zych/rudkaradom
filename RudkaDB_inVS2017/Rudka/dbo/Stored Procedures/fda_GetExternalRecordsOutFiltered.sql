﻿CREATE PROCEDURE fda_GetExternalRecordsOutFiltered
@PageNum int
,@PageSize int

,@TreeUnitIds nvarchar(max),
@IdFrom int = null
,@IdTo int = null
,@EventIdFrom int = null
,@EventIdTo int = null
,@DateUtcFrom datetime = null
,@DateUtcTo datetime = null
,@RecordType char(200) = null
,@Record nvarchar(200) = null
,@ParentIdFrom int = null
,@ParentIdTo int = null
,@TryFrom int = null
,@TryTo int = null
,@TryAgainFrom bit = null
,@TryAgainTo bit = null
,@SentDateUtcFrom datetime = null
,@SentDateUtcTo datetime = null
,@Error nvarchar(200) = null
,@SortOrder nvarchar(100) = null
AS
BEGIN

 DECLARE @cmd nvarchar(max)
 SET @cmd = 'WITH orn AS ( SELECT ROW_NUMBER() OVER ('
		+ dbo.dsql_GetExternalRecordsOut_OrderBY(@SortOrder) + ') AS RowNum, '
		+ dbo.dsql_GetExternalRecordsOut_Select()
		+ dbo.dsql_GetExternalRecordsOut_From()
		+ dbo.dsql_GetExternalRecordsOut_Where(@TreeUnitIds,
			@IdFrom, @IdTo, @EventIdFrom, @EventIdTo, @DateUtcFrom, @DateUtcTo
			, @RecordType, @Record, @ParentIdFrom, @ParentIdTo, @TryFrom, @TryTo
			, @TryAgainFrom, @TryAgainTo, @SentDateUtcFrom, @SentDateUtcTo, @Error)
 + ')
		SELECT *
		FROM orn
		WHERE RowNum BETWEEN ' + CONVERT(nvarchar(20), @PageNum * @PageSize +1) + ' AND '
		+ CONVERT(nvarchar(20), (@PageNum + 1) * @PageSize) + ' ORDER BY rowNum'

 EXEC(@cmd)
-- print @cmd
END