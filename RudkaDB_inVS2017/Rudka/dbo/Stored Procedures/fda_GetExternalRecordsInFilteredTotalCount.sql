﻿CREATE PROCEDURE fda_GetExternalRecordsInFilteredTotalCount
@TreeUnitIds nvarchar(max),
@Type varchar(200) = null
,@IdFrom int = null
,@IdTo int = null
,@RequestIdFrom int = null
,@RequestIdTo int = null
,@StoreDateUtcFrom datetime = null
,@StoreDateUtcTo datetime = null
,@RecordType nvarchar(200) = null
,@RecordId nvarchar(200) = null
,@Record nvarchar(200) = null
,@ProcessedFrom int = null
,@ProcessedTo int = null
,@TryFrom int = null
,@TryTo int = null
,@ProcessedDateUtcFrom datetime = null
,@ProcessedDateUtcTo datetime = null
,@Error nvarchar(200) = null
AS
BEGIN

 DECLARE @cmd nvarchar(max)
 SET @cmd = 'SELECT COUNT(1) '
		+ dbo.dsql_GetExternalRecordsIn_From()
		+ dbo.dsql_GetExternalRecordsIn_Where(@TreeUnitIds,
			@Type, @IdFrom, @IdTo, @RequestIdFrom, @RequestIdTo, @StoreDateUtcFrom
			, @StoreDateUtcTo, @RecordType, @RecordId, @Record, @ProcessedFrom, @ProcessedTo
			, @TryFrom, @TryTo, @ProcessedDateUtcFrom, @ProcessedDateUtcTo, @Error)

 EXEC(@cmd)
-- print @cmd
END