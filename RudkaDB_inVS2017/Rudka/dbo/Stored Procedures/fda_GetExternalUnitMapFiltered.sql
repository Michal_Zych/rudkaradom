﻿CREATE PROCEDURE fda_GetExternalUnitMapFiltered
@PageNum int
,@PageSize int

,@TreeUnitIds nvarchar(max),
@ExtId nvarchar(200) = null
,@ExtDescription nvarchar(200) = null
,@M2mIdFrom int = null
,@M2mIdTo int = null
,@M2mDescription nvarchar(200) = null
,@SortOrder nvarchar(100) = null
AS
BEGIN

 DECLARE @cmd nvarchar(max)
 SET @cmd = 'WITH orn AS ( SELECT ROW_NUMBER() OVER ('
		+ dbo.dsql_GetExternalUnitMap_OrderBY(@SortOrder) + ') AS RowNum, '
		+ dbo.dsql_GetExternalUnitMap_Select()
		+ dbo.dsql_GetExternalUnitMap_From()
		+ dbo.dsql_GetExternalUnitMap_Where(@TreeUnitIds,
			@ExtId, @ExtDescription, @M2mIdFrom, @M2mIdTo, @M2mDescription)
 + ')
		SELECT *
		FROM orn
		WHERE RowNum BETWEEN ' + CONVERT(nvarchar(20), @PageNum * @PageSize +1) + ' AND '
		+ CONVERT(nvarchar(20), (@PageNum + 1) * @PageSize) + ' ORDER BY rowNum'

 EXEC(@cmd)
-- print @cmd
END