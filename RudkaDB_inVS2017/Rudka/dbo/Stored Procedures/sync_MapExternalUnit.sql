﻿

CREATE PROCEDURE [dbo].[sync_MapExternalUnit]
	@ExtId nvarchar(100)
	,@M2mId int
	,@OperatorId int
	,@Reason nvarchar(max)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE
		@OperationDateUtc dateTime = dbo.GetCurrentUtcDate()
		,@eventId int

	BEGIN TRY
		BEGIN TRANSACTION
			UPDATE arch.ext_Units SET M2mId = @M2mId WHERE ExtId = @ExtId
			
			DECLARE @json nvarchar(max) = 'SELECT ExtId, ExtDescription, M2mId, dbo.GetUnitLocation(M2mId, 1, 1, default) AS M2mDescription FROM arch.ext_Units WHERE ExtId = ''' + @ExtId + ''''
			EXEC @json = dbo.SerializeJSON @json

			INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason)
			SELECT @operationDateUtc, eventType.[Update], eventSeverity.[Event], @M2mId, @M2mId, objectType.UnitMapping, null, @OperationDateUtc, @OperationDateUtc, @operatorId, @json, @reason 
			FROM enum.EventType eventType				
			JOIN enum.EventSeverity eventSeverity ON 1 = 1
			JOIN enum.ObjectType objectType ON 1 = 1
			SELECT @eventId = SCOPE_IDENTITY()
			
			UPDATE ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
		COMMIT TRANSACTION
		SELECT 1 AS Result, NULL AS ErrorCode, NULL as ErrorMessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		SELECT CONVERT(int, null) AS Result, ERROR_NUMBER() AS ErrorCode, ERROR_MESSAGE() AS ErrorMessage
	END CATCH

END