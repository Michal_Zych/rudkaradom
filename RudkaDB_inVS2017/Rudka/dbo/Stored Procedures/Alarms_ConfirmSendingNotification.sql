﻿

-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[Alarms_ConfirmSendingNotification]
	@notificationID int,
	@userID int
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE dbo.AlarmNotificationsLog
	SET SentDate = [dbo].[GetCurrentDate]()
	WHERE NotificationID = @notificationID
		AND (ReceiverID = @userID OR @userID IS NULL)
       
	SELECT @@ROWCOUNT
END

