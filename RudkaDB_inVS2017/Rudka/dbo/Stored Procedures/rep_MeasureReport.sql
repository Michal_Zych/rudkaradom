﻿
CREATE  PROCEDURE [dbo].[rep_MeasureReport]
	@UserReportID INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @precision int = 1

	DECLARE
		@dateFrom datetime
		,@dateTo datetime

	SELECT @dateFrom = DateFrom
		, @dateTo = DateTo
	FROM dbo.UsersReports ur WITH(NOLOCK)
	WHERE ur.Id = @UserReportID


	SELECT s.Id AS SensorId, m.MeasurementTime AS MeasurementTime, ROUND(m.Value * s.scale, @precision) AS Value, u.Name AS Name
		, dbo.GetUnitLocation(u.ID, default, 1, default ) AS Location
		, s.MU AS MU
		, r.AlarmsCount AS Alarms
		, s.IsBitSensor
		, s.LoStateDesc
		, s.HiStateDesc
	FROM dbo.Measurements m WITH(NOLOCK)
	JOIN dbo.UserReportSensors r WITH(NOLOCK) ON m.SensorID = r.SensorID
	JOIN dbo.Sensors s WITH(NOLOCK) ON s.Id = r.SensorID
	JOIN dbo.Units u WITH(NOLOCK) ON u.ID = s.UnitId
	WHERE r.UserReportID = @UserReportID
	AND m.MeasurementTime BETWEEN @dateFrom AND @dateTo
	
END
