﻿CREATE PROCEDURE [dbo].[cfg_ApplicationSettingsSet]
	@UserId int
	,@Params nvarchar(max)
AS 
BEGIN
	DECLARE	@id int

	INSERT dbo.ev_SettingsHistory(UserId, StartDateUtc) values(@UserId, dbo.GetCurrentUTCDate())
	SET @id = SCOPE_IDENTITY()

	
	SET @Params = 'UPDATE dbo.ev_SettingsHistory SET ''' + @params + ' WHERE Id = ' + CONVERT(nvarchar(10), @id)
		+ @Params
	
	EXEC sp_ExecuteSql @Params
END