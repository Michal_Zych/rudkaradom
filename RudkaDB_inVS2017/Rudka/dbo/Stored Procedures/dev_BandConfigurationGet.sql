﻿CREATE PROCEDURE dbo.[dev_BandConfigurationGet]
	@BandId dbo.OBJECT_ID = NULL
AS 
BEGIN
	SET NOCOUNT OFF;
	SELECT b.Id, b.Mac, b.MacString, b.Uid, b.BarCode, b.SerialNumber, b.TransmissionPower, b.AccPercent, b.SensitivityPercent, b.BroadcastingIntervalMs
		,dbo.GetBandConfigurationString(b.Id) AS [Parameters]
		,b.Description
		,bs.ObjectType, bs.ObjectId, dbo.GetDisplayName(bs.ObjectId, bs.ObjectType) AS ObjectDisplayName
		,bs.ObjectUnitId, ou.Name AS ObjectUnitName, dbo.GetUnitLocation(bs.ObjectUnitId, default, default, default) AS ObjectUnitLocation
		,bs.CurrentUnitId, cu.Name AS CurrentUnitName, dbo.GetUnitLocation(bs.CurrentUnitId, default, default, default) AS CurrentUnitLocation
		,b.BatteryTypeId
		,CASE WHEN bs.OngLowBatteryEventId IS NULL THEN 0 ELSE 1 END AS IsBatteryAlarm
		,b.BatteryInstallationDateUtc
	FROM dbo.Bands b
	LEFT JOIN dbo.ev_BandStatus bs ON b.Id = bs.BandId
	LEFT JOIN dbo.Units as ou ON bs.ObjectUnitId = ou.ID
	LEFT JOIN dbo.Units as cu ON bs.CurrentUnitId = cu.ID
	WHERE b.Id = 0 OR b.Id = @BandId
END