﻿

CREATE  PROCEDURE [dbo].[unt_GetOldDataUnits]
	@ClientId int = 1
	,@Hyper bit = 1
	,@units IDs_LIST
AS
BEGIN
 SET NOCOUNT ON;

         
	DECLARE
		@defOldDataMinutes int = 10
		,@minOldDataMinutes int = 2
		,@oldDataMinutes int 
		,@minutesStr nvarchar(10)

	-- ustalenie minut po ilu dane uważane są za stare
	SELECT @minutesStr = OldDataTimeMinutes FROM dbo.ev_Settings
	IF ISNUMERIC(@minutesStr) = 1 SET @oldDataMinutes = CAST(@minutesStr AS INT)
	IF @oldDataMinutes < @minOldDataMinutes SET @oldDataMinutes = null
	IF @oldDataMinutes IS NULL SET @oldDataMinutes = @defOldDataMinutes


	DECLARE 
		@freshDataDate datetime
	SET @freshDataDate = DATEADD(MINUTE, -@oldDataMinutes, dbo.GetCurrentDate()) 

	
	SELECT u.ID AS UnitID
	FROM dbo.Sensors s
	JOIN dbo.Units u ON u.ID = s.UnitId
	JOIN dbo.SplitWordsToInt(@units) ul on ul.value = u.ID 
	LEFT JOIN MeasurementsCurrent mc on mc.SensorID = s.id
	WHERE (@hyper = 1 AND mc.HyperMesurementTime < @freshDataDate OR mc.HyperValue IS NULL)
		OR (@hyper = 0  AND mc.MeasurementTime < @freshDataDate OR mc.Value IS NULL)

END




