﻿CREATE Procedure [dbo].[Role_GetRolesThatCanBeManagedByUser]
	@UserId int
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @userRole int	
	
	SELECT @userRole = RoleID
	FROM dbo.UsersRoles
	WHERE UserID = @UserId
	
	SELECT r.ID, r.Name
	FROM dbo.Roles r
	JOIN dbo.Roles_GetDescendants(@userRole) dr ON dr.Id = r.ID
END
