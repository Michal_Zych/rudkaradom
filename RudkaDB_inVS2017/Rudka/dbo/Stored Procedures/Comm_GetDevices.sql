﻿
CREATE PROCEDURE [dbo].[Comm_GetDevices]	
AS
BEGIN

	SET NOCOUNT ON;
	SELECT id,Address,MaxChannelCount,Timeout,DeviceModel,HubId from dbo.devices where DeviceModel is not null and MaxChannelCount is not null and Address is not null and active=1
	order by hubid,Address
END
