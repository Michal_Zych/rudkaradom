﻿

CREATE PROCEDURE [dbo].[msg_CreateMessage] 
	@clientId int
	, @message nvarchar(MAX)
	, @activeFrom datetime
	, @activeTo datetime
	, @mode tinyint
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @IDs TABLE(ID int not null)

	DECLARE @result int = -1

	BEGIN TRANSACTION
		BEGIN TRY
			IF (@clientID IS NULL)
				INSERT @IDs SELECT ID FROM dbo.Clients
			ELSE
				INSERT @IDs SELECT @clientId

			INSERT dbo.Messages(ClientId, Message, ActiveFrom, ActiveTo, Mode)
			SELECT ID, @message, @activeFrom, @activeTo, @mode
			FROM @IDs

			SELECT @result = SCOPE_IDENTITY()
			COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
		END CATCH
	SELECT @result AS Result
END
