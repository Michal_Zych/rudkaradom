﻿
CREATE  PROCEDURE [dbo].[rep_AlarmSubReport__old]
	@UnitId int,
	@DateFrom datetime,
	@DateTo datetime,
	@ShowAllAlarms bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @precision int = 1

	DECLARE @NO_COMMUNICATION tinyint = 3
		
	SELECT  
	CASE WHEN @ShowAllAlarms = 1 THEN a.EventType
		ELSE CASE WHEN a.EventType > @NO_COMMUNICATION THEN @NO_COMMUNICATION ELSE a.EventType END
	END AS EventType,
	a.Timestamp as DateFrom,
	a.DateEnd,
	u.Name,
	ROUND(m.Value * s.Scale, @precision) AS Value,
	ROUND(a.LoRange * s.Scale, @precision) AS LoRange,
	ROUND(a.UpRange * s.Scale, @precision) AS UpRange,
	COALESCE(usa.LastName + ' ' + usa.Name,'') as UserStatus,
    a.DateStatus,
    a.CommentStatus,
	s.MU
	FROM dbo.Alarms a WITH(NOLOCK)
	LEFT JOIN dbo.Measurements m WITH(NOLOCK) ON a.SensorID = m.SensorID AND a.Timestamp = m.MeasurementTime
	LEFT JOIN dbo.Users usa WITH(NOLOCK) ON usa.Id = a.UserStatus
	JOIN dbo.Sensors s WITH(NOLOCK) ON a.SensorID = s.Id
	JOIN dbo.Units u WITH(NOLOCK) ON u.Id = s.UnitId
	WHERE u.ParentUnitID = @unitid AND a.Timestamp BETWEEN @DateFrom AND @DateTo 
		AND ( @ShowAllAlarms = 1 OR (DATEDIFF(SECOND, a.Timestamp, Coalesce(a.DateEnd, dbo.GetCurrentDate()))
				  > (CASE 
							WHEN a.EventType < @NO_COMMUNICATION THEN s.GuiRangeDelay
							ELSE s.GuiCommDelay
						END))
	   )
   ORDER BY a.Timestamp 
END