﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	Edycja danych użytkownika o podanym ID
-- =============================================
CREATE PROCEDURE [dbo].[usr_EditUser]
(
	  @id int
	, @login  USER_LOGIN
	, @password USER_PASSWORD
	, @name USER_NAME
	, @lastName USER_NAME
	, @phone USER_PHONE
	, @eMail USER_EMAIL
	, @operatorId int
	, @logEntry nvarchar(max)
	, @reason nvarchar(max)
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @OPERATION_DELETE tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_EDIT tinyint = 2

		,@RECORD_UNIT tinyint = 0
		,@RECORD_USER tinyint = 1

		--kody błędów
	DECLARE
		@ERROR int = -1
		,@operationDateUtc dateTime = dbo.GetCurrentUtcDate()
		,@eventId int

	DECLARE --wartości przed zmianą
	 @o_password USER_PASSWORD
	,@o_passwordChangeDate datetime

	BEGIN TRANSACTION
		BEGIN TRY
			SELECT @o_password = Password, @o_passwordChangeDate = PasswordChangeDate FROM dbo.Users WHERE ID = @id
			IF @o_password <> @password SET @o_passwordChangeDate = dbo.GetCurrentDate()

			UPDATE dbo.Users 
			SET
				Login = @login
				,Password = @password
				,Name = @name
				,LastName = @lastName
				,Phone = @phone
				,eMail = @eMail
				,PasswordChangeDate = @o_passwordChangeDate
			WHERE ID = @id

			INSERT dbo.Logs(OperationDate, OperatorId, OperationType, ObjectType, ObjectId, Reason, Info)
					VALUES(dbo.GetCurrentDate(), @operatorId, @OPERATION_EDIT, @RECORD_USER, @Id, @reason, @logEntry)

			DECLARE @json nvarchar(max) = 'SELECT Id, Login, Name, LastName, Active FROM dbo.Users WHERE Id = ' + CONVERT(nvarchar(10), @id)
			EXEC @json = dbo.SerializeJSON @json

			INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason)
			SELECT @operationDateUtc, eventType.[Update], eventSeverity.[Event], null, @id, objectType.[User], null, @OperationDateUtc, @OperationDateUtc, @operatorId, @json, @reason 
			FROM enum.EventType eventType				
			JOIN enum.EventSeverity eventSeverity ON 1 = 1
			JOIN enum.ObjectType objectType ON 1 = 1
			SELECT @eventId = SCOPE_IDENTITY()
			
			UPDATE ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			SELECT @ERROR
			RETURN
		END CATCH
	COMMIT TRANSACTION

	SELECT @id
END


