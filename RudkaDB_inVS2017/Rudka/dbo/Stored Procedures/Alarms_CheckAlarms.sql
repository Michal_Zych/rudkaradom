﻿

CREATE  PROCEDURE [dbo].[Alarms_CheckAlarms]
	@date_from datetime,
	@user int,
	@units IDs_LIST
AS
BEGIN
	insert _Actions 
	select [dbo].[GetCurrentDate](), convert(nvarchar(30), @date_from, 120) + '   '+ convert(nvarchar(10), @user) + '   ' + @units


	SET NOCOUNT ON;

	DECLARE @clientId int = 1

	DECLARE @maxRows int 
	SELECT @maxRows = MaxRowsAlarmsPopup FROM dbo.ev_Settings
	
	if	exists (select 1 from Users u inner join UsersLogs ul on u.LastPingID = ul.ID where u.ID = @user) --ul.Monitoring = 1 and 
	begin

		SELECT top(@maxRows)
			a.ID as [ID],
			
			a.EventType as [EventType],	
			a.[Timestamp] as [Timestamp],
			
			a.SensorID as [SensorID],
			u.Name as [SensorName],
			u.Id as [UnitId],
			dbo.GetUnitLocation(u.id, default, default, default) as Location,
			s.MU AS MU,

			a.LoRange * s.Scale AS LoRange,
			a.UpRange * s.Scale AS UpRange,
			dat.Value * s.Scale AS Value,
			
			a.DateEnd as DateEnd,
			a.DateStatus as AlarmCloseDate,
			a.UserStatus as AlarmCloseUserID,
			isnull(us.Name,'') + ' ' + isnull(us.LAstName,'') as AlarmCloseUserName,
			a.CommentStatus as AlarmCloseComment,
			s.IsBitSensor as IsBitSensor,
			s.LoStateDesc as LoStateDesc,
			s.HiStateDesc as HiStateDesc
		FROM
			Alarms a
			left join Measurements dat on dat.SensorID = a.SensorId and dat.MeasurementTime = a.Timestamp
			inner join Sensors s on s.id = a.SensorId
			inner join Units u on u.Id = s.UnitId
			left join Users us on us.ID = a.UserStatus
			join dbo.SplitWordsToInt(@units) SU on SU.value = u.ID
		where (a.status != 1 or a.status is null)
			--and (a.EventType < 16) --od 16 w górę alarmy nie notują odczytów - alarmy nietemperaturowe
		ORDER BY
		a.[Timestamp] desc
	end    
END

