﻿CREATE  PROCEDURE [dbo].[Alarms_CloseAlarms__old]
	@ids IDs_LIST,
	@comment ALARM_COMMENT,
	@userid int,
	@tEscalation int = 0
AS
BEGIN
	SET NOCOUNT ON;
	
DECLARE	
		 @closeTime datetime = dbo.GetCurrentDate()
			--typy powiadomień
		, @GUI			tinyint = 0
		, @SMS			tinyint = 1
		, @EMAIL		tinyint = 2
	
DECLARE @notifications TABLE(
	  SensorId smallint
	, AlarmID int
	, Type tinyint not null
	, SentAfterDate datetime
)

DECLARE @AlarmIDs TABLE (
	ID int not null
)

INSERT @AlarmIDs --tabela identyfikatorów zamykanych alarmów
SELECT Value FROM dbo.SplitWordsToInt(@ids)


BEGIN--OBSŁUGA POWIADOMIEŃ
	IF @tEscalation > 0--jeśli Eskalacja to wylicz nowe czasy dla powiadomień
	BEGIN
		INSERT @notifications
		SELECT 
			  n.SensorID
			, n.AlarmID
			, n.Type
			, DATEADD(SECOND, (DATEDIFF(SECOND, a.Timestamp, n.sendAfterDate) + @tEscalation), @closeTime)
		FROM dbo.AlarmNotifications n JOIN dbo.Alarms a ON n.AlarmID = a.ID
			JOIN @AlarmIDs Ids ON a.ID = Ids.ID
		WHERE a.DateEnd IS NULL--nie ma eskalacji dla zakończonych alarmów
			AND n.DeleteDate IS NULL
	END

	UPDATE a -- powiadomieniom GUI wpisz czas wysłania = czasowi zamkniecia alarmu
	SET SentDate = @closeTime
		,Info = Info + ' | ' + CONVERT(nvarchar(30), @closeTime, 121) + ':Alarms_CloseAlarms(Ustawiono SentDate=CloseTime)'
	FROM dbo.AlarmNotifications a JOIN @AlarmIDs ids ON a.AlarmID = ids.ID
	WHERE Type = @GUI
		AND SendAfterDate <= @closeTime
		AND a.DeleteDate IS NULL
	
	/* 
	DELETE a -- skasuj niewysłane powiadomienia
	FROM dbo.AlarmNotifications a JOIN @AlarmIDs ids ON a.AlarmID = ids.ID
	WHERE SendAfterDate > @closeTime
	*/
	UPDATE a -- skasuj niewysłane powiadomienia
		SET DeleteDate = @closeTime
		,Info = Info + ' | ' + CONVERT(nvarchar(30), @closeTime, 121) + ':Alarms_CloseAlarms(Skasowano)'
	FROM dbo.AlarmNotifications a JOIN @AlarmIDs ids ON a.AlarmID = ids.ID
	WHERE SendAfterDate > @closeTime
		AND a.DeleteDate IS NULL
		
		

	IF @tEscalation > 0 --wstaw powiadomienia powstałe po eskalacji
	BEGIN
		INSERT dbo.AlarmNotifications(SensorID, AlarmID, SendAfterDate, SentDate, Type, Message, Addressees, Info)
		SELECT n.SensorId, n.AlarmID, n.SentAfterDate, null, n.Type, null, null, CONVERT(nvarchar(30), @closeTime, 121) + ':Alarms_CloseAlarms(Insert po eskalacji)'
		FROM @notifications n
	END	
END

BEGIN--ZAMYKANIE ALARMÓW	
	UPDATE a
	SET
		Status = 1,
		UserStatus = COALESCE(UserStatus, @userid),
		DateStatus = COALESCE(DateStatus, @closetime),
		CommentStatus = CASE 
							WHEN COALESCE(Status, 0) = 0 THEN @comment
							ELSE COALESCE(CommentStatus, '')
								+ CHAR(13) + 'Następne zamknięcie: ' + CONVERT(varchar(25), @closeTime, 120)
								+ CHAR(13) + COALESCE(u.Name, '') + ' ' + COALESCE(u.LastName, '')
								+ COALESCE(CHAR(13) + @comment, '')
						END
	FROM dbo.Alarms a
	LEFT JOIN dbo.Users u ON u.ID = @userid
	JOIN @AlarmIDs ids ON a.ID = ids.ID
END

	SELECT 1
END