﻿CREATE PROCEDURE [dbo].[al_PatientAlarmConfigSet]
(
	@PatientId int,
	@IsSOScalling bit,
	@IsACCactive bit,
	@DMoveWrActive bit,
	@DMoveWrMins smallint,
	@DMoveAlActive bit,
	@DMoveAlMins smallint,
	@DNoMoveWrActive bit,
	@DNoMoveWrMins smallint,
	@DNoMoveAlActive bit,
	@DNoMoveAlMins smallint,
	@NMoveWrActive bit,
	@NMoveWrMins smallint,
	@NMoveAlActive bit,
	@NMoveAlMins smallint,
	@NNoMoveWrActive bit,
	@NNoMoveWrMins smallint,
	@NNoMoveAlActive bit,
	@NNoMoveAlMins smallint,
	@OutOfZoneWrActive bit,
	@OutOfZoneWrMins smallint,
	@OutOfZoneAlActive bit,
	@OutOfZoneAlMins smallint,
	@InNoGoZoneMins smallint,
	@NoConnectWrActive bit,
	@NoConnectWrMins smallint,
	@NoConnectAlActive bit,
	@NoConnectAlMins smallint,

	@CallingList nvarchar(max),
	@NoGoZones nvarchar(max),

	@OperatorId int,
	@Reason nvarchar(max) = null
)
 AS  
 BEGIN
	DECLARE @UPDATE_ENDING_EVENT_ID bit = 1
	DECLARE 
		 @LIST_SEPARATOR char = ','
		,@LIST_SEPARATOR_REPLACMENT nvarchar(10) = '&comma;'

	DECLARE
		@EVENT_UPDATE dbo.EVENT_TYPE
		,@EVENT_CREATE dbo.EVENT_TYPE
		,@OBJECT_TYPE_PATIENT dbo.OBJECT_TYPE
		,@OBJECT_TYPE_PATIENTALARMCONFIG dbo.OBJECT_TYPE
		,@SEVERITY_EVENT dbo.EVENT_SEVERITY
		,@eventType dbo.EVENT_TYPE
		,@currentDate datetime
		,@eventId int

	SELECT @EVENT_CREATE = [Create], @EVENT_UPDATE = [Update] FROM enum.EventType
	SELECT @OBJECT_TYPE_PATIENTALARMCONFIG = PatientAlarmConfiguration, @OBJECT_TYPE_PATIENT = Patient FROM enum.ObjectType
	SELECT @SEVERITY_EVENT = [Event] FROM enum.EventSeverity

	SET @currentDate = dbo.GetCurrentUtcDate()

	DECLARE @callings TABLE(SequenceNr tinyint identity(1,1), Phone nvarchar(20), Description nvarchar(200))

	INSERT @callings(Phone, Description) 
	SELECT CONVERT(nvarchar(20), A), CONVERT(nvarchar(200), B) 
	FROM dbo.SplitTo2colums(@CallingList, @LIST_SEPARATOR, @LIST_SEPARATOR_REPLACMENT)

	IF EXISTS(SELECT 1 FROM dbo.al_ConfigPatients WHERE PatientId = @PatientId)
		SET @eventType = @EVENT_UPDATE
	ELSE SET @eventType = @EVENT_CREATE

	BEGIN TRY
		BEGIN TRANSACTION
			IF @eventType = @EVENT_UPDATE
			BEGIN
				DELETE dbo.al_CallingGroups WHERE Id = @PatientId AND ObjectType = @OBJECT_TYPE_PATIENT
				DELETE dbo.al_NoGoZones WHERE Id = @PatientId AND ObjectType = @OBJECT_TYPE_PATIENT
				DELETE dbo.al_ConfigPatients WHERE PatientId = @PatientId
			END

			INSERT dbo.al_ConfigPatients(PatientId, IsSOScalling, IsACCactive, DMoveWrActive, DMoveWrMins
				,DMoveAlActive, DMoveAlMins, DNoMoveWrActive, DNoMoveWrMins, DNoMoveAlActive, DNoMoveAlMins
				,NMoveWrActive, NMoveWrMins, NMoveAlActive, NMoveAlMins, NNoMoveWrActive, NNoMoveWrMins
				,NNoMoveAlActive, NNoMoveAlMins, OutOfZoneWrActive, OutOfZoneWrMins, OutOfZoneAlActive
				,OutOfZoneAlMins, InNoGoZoneMins, NoConnectWrActive, NoConnectWrMins, NoConnectAlActive, NoConnectAlMins)
			VALUES(@PatientId, @IsSOScalling, @IsACCactive, @DMoveWrActive, @DMoveWrMins			
				,@DMoveAlActive, @DMoveAlMins, @DNoMoveWrActive, @DNoMoveWrMins, @DNoMoveAlActive, @DNoMoveAlMins		
				,@NMoveWrActive, @NMoveWrMins, @NMoveAlActive, @NMoveAlMins, @NNoMoveWrActive, @NNoMoveWrMins		
				,@NNoMoveAlActive, @NNoMoveAlMins, @OutOfZoneWrActive, @OutOfZoneWrMins, @OutOfZoneAlActive	
				,@OutOfZoneAlMins, @InNoGoZoneMins, @NoConnectWrActive, @NoConnectWrMins, @NoConnectAlActive, @NoConnectAlMins)
	
			-- calling groups
			INSERT [dbo].[al_CallingGroups]
			SELECT @PatientId, @OBJECT_TYPE_PATIENT, SequenceNr, Phone, Description
			FROM @callings
	
			--no go zones
			INSERT [dbo].[al_NoGoZones]
			SELECT @PatientId, @OBJECT_TYPE_PATIENT, Value
			FROM [dbo].[SplitWordsToInt](@NoGoZones)
			 
			-- event	
			DECLARE @JSON nvarchar(max)
			EXEC @JSON = dbo.SerializeJSON_PatientAlarmConfiguration @PatientId

			INSERT dbo.ev_Events(DateUtc, Type, Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason) 
			SELECT @currentDate, @eventType, @SEVERITY_EVENT, COALESCE(RoomId, WardId), Id, @OBJECT_TYPE_PATIENTALARMCONFIG, RoomId, @currentDate, @currentDate, @OperatorId, @JSON, @Reason
			FROM dbo.Patients
			WHERE Id = @PatientId
			SELECT @eventId = SCOPE_IDENTITY()
			IF @UPDATE_ENDING_EVENT_ID = 1
			BEGIN
				UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
			END

		COMMIT TRANSACTION
		SELECT @PatientId AS Result, NULL as ErrorCode, NULL as ErrorMessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		SELECT CONVERT(int, null) AS Result, ERROR_NUMBER() AS ErrorCode, ERROR_MESSAGE() AS ErrorMessage
	END CATCH
END