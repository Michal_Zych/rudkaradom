﻿-- =============================================
-- Author:		rs
-- Create date: 24,08,2013
-- Description: Zwraca aktywne unity danego klienta
-- =============================================
CREATE PROCEDURE [dbo].[Units_GetClientUnits_v1] @clientId INT
AS
BEGIN
	SET NOCOUNT ON;

/*	with Tree(ID, Name, UnitTypeID, ParentUnitID)
	as
	(
		select e.ID, e.Name, UnitTypeID, ParentUnitID
		from Units e join Clients c on e.ID = c.RootUnitID
		where c.ID = @clientId
		union all
		select e.ID, e.Name, e.UnitTypeID, e.ParentUnitID
		from Units e inner join Tree eh on e.ParentUnitID = eh.ID
		where e.Active = 1
	)
	select ID, Name, UnitTypeID, ParentUnitID
	from Tree 
*/
	SELECT ID, Name, UnitTypeID, ParentUnitID, ClientId
	FROM Units
	WHERE ClientID = @clientId
		AND Active = 1
END

