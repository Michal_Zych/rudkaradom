﻿
CREATE PROCEDURE [dbo].[JOBdaily_MoveEventsToArchive]
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @MIN_EVENT_LIFE_DAYS int = 30
	DECLARE @MAX_ROWS_COUNT int = 1000
	DECLARE @DELAY_TIME nvarchar(10) = '00:00:02'

	DECLARE
		@eventsLifeDays int
		,@minEventDateUtc dateTime
		,@deletedCount int
		,@maxEventId int

	SELECT @eventsLifeDays = EventToArchiveAfterDays FROM dbo.ev_Settings
	IF @eventsLifeDays < @MIN_EVENT_LIFE_DAYS SELECT @eventsLifeDays = @MIN_EVENT_LIFE_DAYS

	SELECT @minEventDateUtc = DATEADD(DAY, -@eventsLifeDays, dbo.GetCurrentUTCDate())
	
	SELECT @maxEventId = MAX(Id)
	FROM dbo.ev_Events 
	WHERE DateUtc <= @minEventDateUtc


	SELECT @deletedCount = 1
	WHILE @deletedCount > 0
	BEGIN
		DELETE TOP (@MAX_ROWS_COUNT) dbo.ev_UsersAlarms
			OUTPUT deleted.EventId, deleted.UserMessageId, deleted.UserId, deleted.Message
			INTO [$(RudkaArch)].dbo.ev_UsersAlarms(EventId, UserMessageId, UserId, Message)
		FROM dbo.ev_UsersAlarms ua
		JOIN dbo.ev_Events e ON ua.EventId = e.Id
		WHERE e.Id <= @maxEventId

		SELECT @deletedCount = @@ROWCOUNT
		IF @deletedCount > 0 WAITFOR DELAY @DELAY_TIME
	END

	SELECT @deletedCount = 1
	WHILE @deletedCount > 0 
	BEGIN
		DELETE TOP (@MAX_ROWS_COUNT) dbo.ev_Alarms
			OUTPUT deleted.EventId, deleted.RaisingEventId, deleted.IsClosed, deleted.ClosingDateUtc, deleted.ClosingUserId, deleted.ClosingAlarmId, deleted.ClosingComment
			INTO [$(RudkaArch)].dbo.ev_Alarms(EventId, RaisingEventId, IsClosed, ClosingDateUtc, ClosingUserId, ClosingAlarmId, ClosingComment)
		FROM dbo.ev_Alarms a
		JOIN dbo.ev_Events e ON a.EventId = e.Id
		WHERE e.Id <= @maxEventId
		
		SELECT @deletedCount = @@ROWCOUNT
		IF @deletedCount > 0 WAITFOR DELAY @DELAY_TIME
	END

	SELECT @deletedCount = 1
	WHILE @deletedCount > 0 
	BEGIN

		DELETE TOP (@MAX_ROWS_COUNT) 
		FROM dbo.ev_Events
			OUTPUT deleted.Id, deleted.DateUtc, deleted.[Type], deleted.Severity, deleted.UnitId, deleted.ObjectId, deleted.ObjectType, deleted.ObjectUnitId
				,deleted.StoreDateUtc, deleted.EndDateUtc, deleted.EndingEventId, deleted.OperatorId, deleted.[EventData], deleted.Reason
			INTO [$(RudkaArch)].dbo.ev_Events(Id, DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId
				,StoreDateUtc, EndDateUtc, EndingEventId, OperatorId, [EventData], Reason)
		WHERE Id <= @maxEventId
		
		SELECT @deletedCount = @@ROWCOUNT
		IF @deletedCount > 0 WAITFOR DELAY @DELAY_TIME
	END		
		
END