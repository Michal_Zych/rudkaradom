﻿CREATE PROCEDURE fda_GetExternalRecordsOutFilteredTotalCount
@TreeUnitIds nvarchar(max),
@IdFrom int = null
,@IdTo int = null
,@EventIdFrom int = null
,@EventIdTo int = null
,@DateUtcFrom datetime = null
,@DateUtcTo datetime = null
,@RecordType char(200) = null
,@Record nvarchar(200) = null
,@ParentIdFrom int = null
,@ParentIdTo int = null
,@TryFrom int = null
,@TryTo int = null
,@TryAgainFrom bit = null
,@TryAgainTo bit = null
,@SentDateUtcFrom datetime = null
,@SentDateUtcTo datetime = null
,@Error nvarchar(200) = null
AS
BEGIN

 DECLARE @cmd nvarchar(max)
 SET @cmd = 'SELECT COUNT(1) '
		+ dbo.dsql_GetExternalRecordsOut_From()
		+ dbo.dsql_GetExternalRecordsOut_Where(@TreeUnitIds,
			@IdFrom, @IdTo, @EventIdFrom, @EventIdTo, @DateUtcFrom, @DateUtcTo
			, @RecordType, @Record, @ParentIdFrom, @ParentIdTo, @TryFrom, @TryTo
			, @TryAgainFrom, @TryAgainTo, @SentDateUtcFrom, @SentDateUtcTo, @Error)

 EXEC(@cmd)
-- print @cmd
END