﻿
CREATE PROCEDURE [dbo].[al_CheckAlarms]
	@user int
	,@units IDs_LIST = null
AS 
BEGIN
	SET NOCOUNT ON;

	DECLARE @maxRows int = 200

	IF @units IS NULL
	BEGIN
		SELECT @units = ''
		SELECT @units = @units + ',' +	CONVERT(nvarchar(10), Id) 
		FROM dbo.Units
		WHERE Active = 1
	END	


	SELECT TOP (@maxRows)
	e.Id as Id
	,e.DateUtc as DateUtc
	,e.Type as [Type]
	,COALESCE(ua.Message, typeEnum.Description) as TypeDescription
	,e.Severity as Severity
	--,severityEnum.Description as SeverityDescription
	,e.UnitId as UnitId
	--,u.Name as UnitName
	,dbo.GetUnitLocation(e.UnitId, 1, 0, default) as UnitLocation
	,e.ObjectId as ObjectId
	,e.ObjectType as ObjectType
	--,objectTypeEnum.Description as ObjectTypeDescription
	,dbo.GetDisplayName(e.ObjectId, e.ObjectType) as ObjectName
	,e.ObjectUnitId as ObjectUnitId
	--,uo.Name as ObjectUnitName
	,dbo.GetUnitLocation(e.ObjectUnitId, default, default, default) as ObjectUnitLocation
	,e.EndDateUtc as EndDateUtc
	--,e.EndingEventId as EndingEventId
	--,e.OperatorId as OperatorId
	--,usr.Name as OperatorName
	--,usr.LastName as OperatorLastName
	--,e.[EventData] as [EventData]
	--,e.Reason as Reason
	--,a.RaisingEventId as RaisingEventId
	--,a.IsClosed as IsClosed
	--,a.ClosingDateUtc as ClosingDateUtc
	--,a.ClosingUserId as ClosingUserId
	--,ausr.Name as ClosingUserName
	--,ausr.LastName as ClosingUserLastName
	--,a.ClosingAlarmId as ClosingAlarmId
	--,a.ClosingComment as ClosingComment
	--,ua.UserMessageId as UserMessageId
	--,ua.UserId as SenderId
	--,ausr.Name as SenderName
	--,ausr.LastName as SenderLastName
	--,ua.Message as [Message]
	FROM dbo.ev_Events e
	JOIN dbo.ev_Alarms a ON a.EventId = e.Id
	LEFT JOIN dbo.ev_UsersAlarms ua ON a.EventId = ua.EventId
	LEFT JOIN dbo.Units u ON e.UnitId = u.ID
	LEFT JOIN dbo.Units uo ON e.ObjectUnitId = uo.ID
	LEFT JOIN dbo.Users usr ON e.OperatorId = usr.ID
	LEFT JOIN dbo.Users ausr ON a.ClosingUserId = ausr.ID
	LEFT JOIN dbo.Users musr ON ua.UserId = musr.ID
	JOIN dbo.SplitWordsToInt(@units) su ON su.value = e.UnitId OR su.value = ObjectUnitId
	LEFT JOIN dbo.enum_EventTypeDictionaryPl typeEnum ON typeEnum.Value = e.Type
	LEFT JOIN dbo.enum_EventSeverityDictionaryPl severityEnum ON severityEnum.Value = e.Severity
	LEFT JOIN dbo.enum_ObjectTypeDictionaryPl objectTypeEnum ON objectTypeEnum.Value = e.ObjectType
	WHERE a.IsClosed = 0
	ORDER BY e.DateUtc DESC
END