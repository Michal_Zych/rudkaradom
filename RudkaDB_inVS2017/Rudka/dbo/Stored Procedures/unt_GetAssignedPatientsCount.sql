﻿CREATE PROCEDURE [dbo].[unt_GetAssignedPatientsCount]
	@UnitId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(1) AS Result
	FROM dbo.Patients
	WHERE RoomId = @UnitId OR WardId = @UnitId
END