﻿CREATE PROCEDURE [dbo].[sync_GetSupportedEventTypes]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT EventType FROM tech.sync_SupportedEventTypes
END