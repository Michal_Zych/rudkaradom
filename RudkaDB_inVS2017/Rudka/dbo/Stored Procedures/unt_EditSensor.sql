﻿
CREATE PROCEDURE [dbo].[unt_EditSensor]
(
		@Id smallint
		, @SocketID int
		, @Interval int
		, @LoRange real
		, @UpRange real
		, @GuiRangeDelay smallint
		, @GuiCommDelay smallint
		, @SmsEnabled bit 
		, @SmsRangeDelay smallint
		, @SmsCommDelay smallint
		, @MailEnabled bit 
		, @MailRangeDelay smallint
		, @MailCommDelay smallint
		, @AlDisabled bit 
		, @AlDisabledForHours bit 
		, @AlDisableHoursFrom1 time(7)
		, @AlDisableHoursTo1 time(7)
		, @AlDisableHoursFrom2 time(7)
		, @AlDisableHoursTo2 time(7)
		, @AlDisabledForRanges bit 
		, @MU nvarchar(5)
		, @LegendDescription nvarchar(50)
		, @LegendShortcut nvarchar(5)
		, @LegendRangeDescription nvarchar(50)
		, @LineColor varchar(15)
		, @LineWidth tinyint
		, @RangeLineWidth tinyint
		, @LineStyle varchar(50)
		, @RangeLineStyle varchar(50)
		, @AlarmColor varchar(15)
		, @Enabled bit 
		, @Calibration real 
		, @operatorId int
		, @logEntry nvarchar(max)
		, @reason nvarchar(max)
		, @omittedRanges nvarchar(max) --format: 'data'do'data','data'do'data'
		, @smsReceivers nvarchar(max)
		, @emailReceivers nvarchar(max)
		, @IsBitSensor bit
		, @IsHighAlarming bit
		, @HiStateDesc nvarchar(10)
		, @LoStateDesc nvarchar(10)
		, @GuiRangeRepeat bit
		, @GuiRangeRepeatMins int
		, @GuiRangeRepeatBreakable bit
		, @GuiCommRepeat bit
		, @GuiCommRepeatMins int
		, @GuiCommRepeatBreakable bit
		, @SmsRangeEnabled bit
		, @SmsRangeAfterFinish bit			
		, @SmsRangeAfterClose bit			
		, @SmsRangeRepeat bit				
		, @SmsRangeRepeatMins int			
		, @SmsRangeRepeatUntil nchar(2)		
		, @SmsRangeRepeatBreakable bit		
		, @SmsCommEnabled bit 
		, @SmsCommAfterFinish bit			
		, @SmsCommAfterClose bit			
		, @SmsCommRepeat bit				
		, @SmsCommRepeatMins int			
		, @SmsCommRepeatUntil nchar(2)		
		, @SmsCommRepeatBreakable bit	
		, @MailRangeEnabled bit	
		, @MailRangeAfterFinish bit			
		, @MailRangeAfterClose bit			
		, @MailRangeRepeat bit				
		, @MailRangeRepeatMins int			
		, @MailRangeRepeatUntil nchar(2)	
		, @MailRangeRepeatBreakable bit	
		, @MailCommEnabled bit	
		, @MailCommAfterFinish bit			
		, @MailCommAfterClose bit			
		, @MailCommRepeat bit				
		, @MailCommRepeatMins int			
		, @MailCommRepeatUntil nchar(2)		
		, @MailCommRepeatBreakable bit		
)
AS
BEGIN
	DECLARE @OPERATION_DELETE tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_EDIT tinyint = 2

		,@RECORD_UNIT tinyint = 0
		,@RECORD_USER tinyint = 1

		,@CURRENT_DATE datetime = dbo.GetCurrentDate()

		,@HI_VALUE smallint = 1000
		,@LO_VALUE smallint = 0


	DECLARE @DEFAULT_SCALE real = 0.1
		,@socketScale real

	SELECT @socketScale = 1
	IF @IsBitSensor = 0
	BEGIN
		IF @SocketID IS NOT NULL
			SELECT @socketScale = dbo.GetScaleForSocket(@SocketID)
	END
	ELSE 
	BEGIN
		IF @IsHighAlarming = 0 SET @LoRange = 1 ELSE SET @LoRange = 0
		SELECT @UpRange = @LoRange, @socketScale = 1 / @HI_VALUE
	END


	BEGIN TRANSACTION
		BEGIN TRY
			DECLARE @oldLoRange smallint
				,@oldUpRange smallint
				,@newLoRange smallint
				,@newUpRange smallint

			SELECT @oldLoRange = LoRange, @oldUpRange = UpRange FROM dbo.Sensors WHERE Id = @Id		
			
			UPDATE dbo.Sensors SET
				  SocketID = @socketId
				, Interval = @Interval
				, LoRange = CONVERT(smallint, ROUND(@Lorange / COALESCE(Scale, COALESCE(@socketScale, @DEFAULT_SCALE)), 0))
				, UpRange = CONVERT(smallint, ROUND(@UpRange / COALESCE(Scale, COALESCE(@socketScale, @DEFAULT_SCALE)), 0))
				, GuiRangeDelay = @GuiRangeDelay
				, GuiCommDelay = @GuiCommDelay
				, SmsEnabled = @SmsEnabled
				, SmsRangeDelay = @SmsRangeDelay
				, SmsCommDelay = @SmsCommDelay
				, MailEnabled = @MailEnabled
				, MailRangeDelay = @MailRangeDelay
				, MailCommDelay = @MailCommDelay
				, AlDisabled = @AlDisabled
				, AlDisabledFrom = CASE WHEN @AlDisabled = 0 THEN NULL 
									ELSE (CASE WHEN @AlDisabled = AlDisabled THEN AlDisabledFrom ELSE @CURRENT_DATE END) END
				, AlDisabledForHours = @AlDisabledForHours
				, AlDisableHoursFrom1 = @AlDisableHoursFrom1
				, AlDisableHoursTo1 = @AlDisableHoursTo1
				, AlDisableHoursFrom2 = @AlDisableHoursFrom2
				, AlDisableHoursTo2 = @AlDisableHoursTo2
				, AlDisabledForRanges = @AlDisabledForRanges
				, MU = @MU
				, LegendDescription = @LegendDescription
				, LegendShortcut = @LegendShortcut
				, LegendRangeDescription = @LegendRangeDescription
				, LineColor = @LineColor
				, LineWidth = @LineWidth
				, RangeLineWidth = @RangeLineWidth
				, LineStyle = @LineStyle
				, RangeLineStyle = @RangeLineStyle
				, AlarmColor = @AlarmColor
				, [Enabled] = @Enabled
				, Calibration = CONVERT(smallint, ROUND(@Calibration / COALESCE(Scale, COALESCE(@socketScale, @DEFAULT_SCALE)), 0))
				, Scale = COALESCE(Scale, @socketScale)
				, IsBitSensor = @IsBitSensor
				, IsHighAlarming = @IsHighAlarming
				, HiStateDesc				= @HiStateDesc
				, LoStateDesc				= @LoStateDesc
				, GuiRangeRepeat			= @GuiRangeRepeat 
				, GuiRangeRepeatMins		= @GuiRangeRepeatMins 
				, GuiRangeRepeatBreakable	= @GuiRangeRepeatBreakable 
				, GuiCommRepeat				= @GuiCommRepeat 
				, GuiCommRepeatMins			= @GuiCommRepeatMins 
				, GuiCommRepeatBreakable	= @GuiCommRepeatBreakable 
				, SmsRangeEnabled			= @SmsRangeEnabled 
				, SmsRangeAfterFinish		= @SmsRangeAfterFinish			
				, SmsRangeAfterClose		= @SmsRangeAfterClose			
				, SmsRangeRepeat			= @SmsRangeRepeat					
				, SmsRangeRepeatMins		= @SmsRangeRepeatMins			
				, SmsRangeRepeatUntil		= @SmsRangeRepeatUntil				
				, SmsRangeRepeatBreakable	= @SmsRangeRepeatBreakable		
				, SmsCommEnabled			= @SmsCommEnabled
				, SmsCommAfterFinish		= @SmsCommAfterFinish			
				, SmsCommAfterClose			= @SmsCommAfterClose			
				, SmsCommRepeat				= @SmsCommRepeat					
				, SmsCommRepeatMins			= @SmsCommRepeatMins				
				, SmsCommRepeatUntil		= @SmsCommRepeatUntil				
				, SmsCommRepeatBreakable	= @SmsCommRepeatBreakable		
				, MailRangeEnabled			= @MailRangeEnabled
				, MailRangeAfterFinish		= @MailRangeAfterFinish			
				, MailRangeAfterClose		= @MailRangeAfterClose			
				, MailRangeRepeat			= @MailRangeRepeat						
				, MailRangeRepeatMins		= @MailRangeRepeatMins			
				, MailRangeRepeatUntil		= @MailRangeRepeatUntil			
				, MailRangeRepeatBreakable	= @MailRangeRepeatBreakable		
				, MailCommEnabled			= @MailCommEnabled
				, MailCommAfterFinish		= @MailCommAfterFinish			
				, MailCommAfterClose		= @MailCommAfterClose			
				, MailCommRepeat			= @MailCommRepeat					
				, MailCommRepeatMins		= @MailCommRepeatMins			
				, MailCommRepeatUntil		= @MailCommRepeatUntil				
				, MailCommRepeatBreakable	= @MailCommRepeatBreakable		



			WHERE Id = @Id

			SELECT @newLoRange = LoRange, @newUpRange = UpRange FROM dbo.Sensors WHERE Id = @Id		
			IF(@newLoRange <> @oldLoRange) OR (@newUpRange <> @oldUpRange)
			BEGIN
				UPDATE dbo.Sensors SET RangeValidFrom = @CURRENT_DATE WHERE Id = @Id

				UPDATE dbo.MeasurementRanges
					SET DateEnd = @CURRENT_DATE
				WHERE SensorID = @Id AND DateEnd IS NULL

				INSERT dbo.MeasurementRanges(SensorID, DateStart, DateEnd, LoRange, UpRange)
				VALUES (@Id, @CURRENT_DATE, null, @newLoRange, @newUpRange)
			END


			DELETE dbo.AlarmsOmittedRanges WHERE SensorID = @Id
			INSERT dbo.AlarmsOmittedRanges
			
			SELECT @Id
					, LEFT(Value, CHARINDEX('do', Value) - 1)
					, SUBSTRING(Value, CHARINDEX('do', Value)  +2, 999) 
			FROM dbo.fn_Split(@omittedRanges, ',')

			DELETE dbo.AlarmNotifyBySms WHERE Sensorid = @Id
			INSERT dbo.AlarmNotifyBySms
			SELECT @Id, Value
			FROM dbo.fn_Split(@smsReceivers, ',')

			DELETE dbo.AlarmNotifyByEmail WHERE Sensorid = @Id
			INSERT dbo.AlarmNotifyByEmail
			SELECT @Id, Value
			FROM dbo.fn_Split(@emailReceivers, ',')
			
			DECLARE @unitId int
			SELECT @unitId = UnitId FROM dbo.Sensors WHERE Id = @Id
			
			INSERT dbo.Logs(OperationDate, OperatorId, OperationType, ObjectType, ObjectId, Reason, Info)
				VALUES(dbo.GetCurrentDate(), @operatorId, @OPERATION_EDIT, @RECORD_UNIt, @unitId, @reason, @logEntry)
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			SELECT CONVERT(smallint, -1)
			RETURN
		END CATCH

	COMMIT TRANSACTION
	
	SELECT @Id	
END


