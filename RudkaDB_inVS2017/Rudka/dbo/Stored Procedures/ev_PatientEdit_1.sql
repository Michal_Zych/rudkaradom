﻿
CREATE PROCEDURE [dbo].[ev_PatientEdit]
	@PatientId int
	,@Name nvarchar(100)
	,@LastName nvarchar(100)
	,@Pesel char(11)
	
	,@OperatorId int
	,@Reason nvarchar(max) = null		
	,@OperationDateUtc dateTime = null
AS
BEGIN
	SET NOCOUNT ON;

	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
		BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'PatientId=' + COALESCE(CONVERT(nvarchar(100), @PatientId), 'NULL') 
		EXEC Log_Action @@PROCID, @info
	END
	/****************************************************/

	DECLARE
		@oldName nvarchar(100)
		,@oldLastName nvarchar(100)
		,@oldPesel char(11)
		,@peselUsedBy int
		,@patientUnitId int
		,@storeDateUtc dateTime
		,@eventId int

	SET @storeDateUtc = dbo.GetCurrentUTCDate()

	IF @Pesel = '' SELECT @Pesel = NULL
	IF @OperationDateUtc IS NULL SELECT @OperationDateUtc = @storeDateUtc

	SELECT @oldName = Name, @oldLastName = LastName, @oldPesel = Pesel 
		, @patientUnitId = COALESCE(RoomId, WardId)
	FROM dbo.Patients WHERE Id = @PatientId
	
	SELECT @peselUsedBy = Id FROM dbo.Patients WHERE Pesel = @Pesel

	IF @peselUsedBy <> @PatientId
	BEGIN
		SELECT CONVERT(int, null) AS Result, -1 AS ErrorCode, 'PESEL przypisany innemu pacjentowi.' AS ErrorMessage
		RETURN
	END
	
	DECLARE
		@rowCount int = 0

	BEGIN TRY
		BEGIN TRANSACTION
			UPDATE dbo.Patients SET
				Name = @Name
				,LastName = @LastName
				,Pesel = @Pesel
			WHERE Id = @PatientId
			SELECT @rowCount = @@ROWCOUNT

			UPDATE arch.Patients SET
				Name = @name
				,LastName = @lastName
				,Pesel = @pesel
			WHERE Id = @PatientId

			DECLARE @json nvarchar(max) = 'SELECT Id, Name, LastName, Pesel FROM '
			IF @rowCount = 0 SELECT @json = @json + 'arch'
			ELSE SELECT @json = @json + 'dbo'
			SELECT @json = @json + '.Patients WHERE Id = ' + CONVERT(nvarchar(10), @PatientId)
			EXEC @json = dbo.SerializeJSON @json

			INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason)
			SELECT @OperationDateUtc, eventType.[Update], eventSeverity.[Event], null, @PatientId, objectType.Patient, @patientUnitId, @storeDateUtc, @OperationDateUtc, @OperatorId, @json, @Reason
			FROM enum.EventType eventType				
			JOIN enum.EventSeverity eventSeverity ON 1 = 1
			JOIN enum.ObjectType objectType ON 1 = 1
			
			SELECT @eventId = SCOPE_IDENTITY()
			UPDATE ev_Events SET EndingEventId = @eventId WHERE Id = @eventId

			SELECT @PatientId AS Result, NULL as ErrorCode, NULL as ErrorMessage
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		SELECT CONVERT(int, null) AS Result, ERROR_NUMBER() AS ErrorCode, ERROR_MESSAGE() AS ErrorMessage
	END CATCH
END