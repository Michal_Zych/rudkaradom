﻿CREATE PROCEDURE fda_GetBandTestFilteredTotalCount
@UnitId int,
@IdFrom smallint = null
,@IdTo smallint = null
,@BarCode nvarchar(100) = null
,@MacString nvarchar(100) = null
,@UidFrom bigint = null
,@UidTo bigint = null
,@BatteryTypeStr nvarchar(100) = null
,@BatteryAlarmSeverityIds nvarchar(max) = null
,@BatteryAlarmSeverity nvarchar(100) = null
,@BatteryInstallationDateFrom datetime = null
,@BatteryInstallationDateTo datetime = null
,@BatteryDaysToExchangeFrom int = null
,@BatteryDaysToExchangeTo int = null
,@Description nvarchar(100) = null
,@ObjectTypeIds nvarchar(max) = null
,@ObjectType nvarchar(100) = null
,@ObjectDisplayName nvarchar(100) = null
,@ObjectUnitIds nvarchar(max) = null
,@ObjectUnitName nvarchar(100) = null
,@ObjectUnitLocation nvarchar(100) = null
,@CurrentUnitIds nvarchar(max) = null
,@CurrentUnitName nvarchar(100) = null
,@CurrentUnitLocation nvarchar(100) = null
AS
BEGIN
 DECLARE @TreeUnitIds nvarchar(max) = ''
 IF @UnitId IS NOT NULL
 BEGIN
   SELECT @TreeUnitIds = @TreeUnitIds + CONVERT(nvarchar(10), Id) + ',' FROM dbo.GetChildUnits(@UnitId)
   SET @TreeUnitIds = LEFT(@TreeUnitIds, LEN(@TreeUnitIds) - 1)
 END

 DECLARE @cmd nvarchar(max)
 SET @cmd = 'SELECT COUNT(1) '
		+ dbo.dsql_GetBandTest_From()
		+ dbo.dsql_GetBandTest_Where(@TreeUnitIds,
			@IdFrom, @IdTo, @BarCode, @MacString, @UidFrom, @UidTo
			, @BatteryTypeStr, @BatteryAlarmSeverityIds, @BatteryAlarmSeverity, @BatteryInstallationDateFrom, @BatteryInstallationDateTo, @BatteryDaysToExchangeFrom
			, @BatteryDaysToExchangeTo, @Description, @ObjectTypeIds, @ObjectType, @ObjectDisplayName, @ObjectUnitIds
			, @ObjectUnitName, @ObjectUnitLocation, @CurrentUnitIds, @CurrentUnitName, @CurrentUnitLocation)

 EXEC(@cmd)
-- print @cmd
END