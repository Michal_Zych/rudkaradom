﻿
CREATE PROCEDURE [dbo].[fda_GetPatientDetailsFiltered]
@PageNum int
,@PageSize int

,@TreeUnitIds nvarchar(max),
@IdFrom int = null
,@IdTo int = null
,@Name nvarchar(200) = null
,@LastName nvarchar(200) = null
,@DisplayName nvarchar(200) = null
,@Pesel nvarchar(200) = null
,@WardIdFrom int = null
,@WardIdTo int = null
,@WardUnitName nvarchar(200) = null
,@WardUnitLocation nvarchar(200) = null
,@WardAdmissionDateUtcFrom datetime = null
,@WardAdmissionDateUtcTo datetime = null
,@RoomIdFrom int = null
,@RoomIdTo int = null
,@RoomUnitName nvarchar(200) = null
,@RoomUnitLocation nvarchar(200) = null
,@RoomAdmissionDateUtcFrom datetime = null
,@RoomAdmissionDateUtcTo datetime = null
,@BandIdFrom int = null
,@BandIdTo int = null
,@BandBarCode nvarchar(200) = null
,@BandSerialNumber nvarchar(200) = null
,@CurrentUnitName nvarchar(200) = null
,@CurrentUnitLocation nvarchar(200) = null
,@SortOrder nvarchar(100) = null
AS
BEGIN
 DECLARE @cmd nvarchar(max)
 SET @cmd = 'WITH orn AS ( SELECT ROW_NUMBER() OVER ('
		+ dbo.dsql_GetPatientDetails_OrderBY(@SortOrder) + ') AS RowNum, '
		+ dbo.dsql_GetPatientDetails_Select()
		+ dbo.dsql_GetPatientDetails_From()
		+ dbo.dsql_GetPatientDetails_Where(@TreeUnitIds,
			@IdFrom, @IdTo, @Name, @LastName, @DisplayName, @Pesel
			, @WardIdFrom, @WardIdTo, @WardUnitName, @WardUnitLocation, @WardAdmissionDateUtcFrom, @WardAdmissionDateUtcTo
			, @RoomIdFrom, @RoomIdTo, @RoomUnitName, @RoomUnitLocation, @RoomAdmissionDateUtcFrom, @RoomAdmissionDateUtcTo
			, @BandIdFrom, @BandIdTo, @BandBarCode, @BandSerialNumber, @CurrentUnitName, @CurrentUnitLocation
			)
 + ')
		SELECT *
		FROM orn
		WHERE RowNum BETWEEN ' + CONVERT(nvarchar(20), @PageNum * @PageSize +1) + ' AND '
		+ CONVERT(nvarchar(20), (@PageNum + 1) * @PageSize) + ' ORDER BY rowNum'

 EXEC(@cmd)
-- print @cmd
END