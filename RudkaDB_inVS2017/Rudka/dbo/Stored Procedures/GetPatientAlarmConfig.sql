﻿CREATE PROCEDURE dbo.GetPatientAlarmConfig
(
	@PatientId int = 1
)
AS
BEGIN	
	DECLARE
		@RECORD_PATIENT tinyint = 4
		,@RECORD_UNIT tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_UPDATE tinyint = 2

	SELECT PatientId
      ,IsSOScalling
      ,IsACCactive
      ,DMoveWrActive
      ,DMoveWrMins
      ,DMoveAlActive
      ,DMoveAlMins
      ,DNoMoveWrActive
      ,DNoMoveWrMins
      ,DNoMoveAlActive
      ,DNoMoveAlMins
      ,NMoveWrActive
      ,NMoveWrMins
      ,NMoveAlActive
      ,NMoveAlMins
      ,NNoMoveWrActive
      ,NNoMoveWrMins
      ,NNoMoveAlActive
      ,NNoMoveAlMins
      ,OutOfZoneWrActive
      ,OutOfZoneWrMins
      ,OutOfZoneAlActive
      ,OutOfZoneAlMins
      ,InNoGoZoneMins
      ,NoConnectWrActive
      ,NoConnectWrMins
      ,NoConnectAlActive
      ,NoConnectAlMins
	FROM dbo.al_ConfigPatients
	WHERE PatientId = @PatientId

	SELECT Phone, Description
	FROM dbo.al_CallingGroups
	WHERE Id = @PatientId AND ObjectType = @RECORD_PATIENT

	SELECT UnitId, Name, dbo.GetUnitLocationReversed(UnitId, 1, 1, default) AS Location
	FROM dbo.al_NoGoZones z
	JOIN dbo.Units u ON z.UnitId = u.ID
	WHERE z.Id = @PatientId AND z.ObjectType = @RECORD_PATIENT
END