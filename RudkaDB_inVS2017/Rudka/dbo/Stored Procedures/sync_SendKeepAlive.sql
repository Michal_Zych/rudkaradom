﻿CREATE PROCEDURE [dbo].[sync_SendKeepAlive]
	@Status nvarchar(max)
	,@IsRestart bit = 0
	,@ServiceTag nvarchar(50) = null
AS
BEGIN
	SET NOCOUNT ON;
	IF @ServiceTag IS NULL SET  @ServiceTag = 'AssecoConnector'
	EXEC dbo.srv_ServicePing @ServiceTag, null, @IsRestart, @Status

END