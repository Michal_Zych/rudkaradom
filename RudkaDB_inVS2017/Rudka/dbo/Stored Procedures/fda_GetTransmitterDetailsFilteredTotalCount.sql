﻿CREATE PROCEDURE fda_GetTransmitterDetailsFilteredTotalCount
@TreeUnitIds nvarchar(max) = null,
@IdFrom int = null
,@IdTo int = null
,@MacFrom bigint = null
,@MacTo bigint = null
,@MacString nvarchar(200) = null
,@ClientIdFrom int = null
,@ClientIdTo int = null
,@InstallationUnitIdFrom int = null
,@InstallationUnitIdTo int = null
,@InstallationUnitName nvarchar(200) = null
,@Name nvarchar(200) = null
,@BarCode nvarchar(200) = null
,@TypeIdFrom int = null
,@TypeIdTo int = null
,@ReportIntervalSecFrom int = null
,@ReportIntervalSecTo int = null
,@SensitivityFrom int = null
,@SensitivityTo int = null
,@AvgCalcTimeSFrom int = null
,@AvgCalcTimeSTo int = null
,@Msisdn nvarchar(200) = null
,@IsBandUpdaterFrom bit = null
,@IsBandUpdaterTo bit = null
,@Description nvarchar(200) = null
,@RssiTresholdFrom smallint = null
,@RssiTresholdTo smallint = null
,@GroupIdFrom tinyint = null
,@GroupIdTo tinyint = null
,@GroupName nvarchar(200) = null
,@IpAddress nvarchar(200) = null
,@IsConnectedFrom bit = null
,@IsConnectedTo bit = null
,@AlarmOngoingFrom bit = null
,@AlarmOngoingTo bit = null
AS
BEGIN

	IF @TreeUnitIds IS NULL
	BEGIN
      SELECT @TreeUnitIds = ''
	  SELECT @TreeUnitIds = @TreeUnitIds + CONVERT(nvarchar(10), Id) + ',' FROM dbo.Units
	  SET @TreeUnitIds = LEFT(@TreeUnitIds, LEN(@TreeUnitIds) - 1)
	END

 DECLARE @cmd nvarchar(max)
 SET @cmd = 'SELECT COUNT(1) '
		+ dbo.dsql_GetTransmitterDetails_From()
		+ dbo.dsql_GetTransmitterDetails_Where(@TreeUnitIds,
			@IdFrom, @IdTo, @MacFrom, @MacTo, @MacString, @ClientIdFrom
			, @ClientIdTo, @InstallationUnitIdFrom, @InstallationUnitIdTo, @InstallationUnitName, @Name, @BarCode
			, @TypeIdFrom, @TypeIdTo, @ReportIntervalSecFrom, @ReportIntervalSecTo, @SensitivityFrom, @SensitivityTo
			, @AvgCalcTimeSFrom, @AvgCalcTimeSTo, @Msisdn, @IsBandUpdaterFrom, @IsBandUpdaterTo, @Description
			, @RssiTresholdFrom, @RssiTresholdTo, @GroupIdFrom, @GroupIdTo, @GroupName, @IpAddress
			, @IsConnectedFrom, @IsConnectedTo, @AlarmOngoingFrom, @AlarmOngoingTo)

 EXEC(@cmd)
-- print @cmd
END