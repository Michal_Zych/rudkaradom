﻿
-- =============================================
-- Author:		kt
-- Create date: 2015-12-08
-- Description:	usuwa wpis do testowania danego sensora
-- =============================================
CREATE PROCEDURE [dbo].[test_RemoveTest]
	@sensorId int
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE dbo.SensorTests
	SET Active = 0, DateEnded = dbo.GetCurrentDate() 
	WHERE SensorId = @sensorId AND Active = 1

	SELECT @@ROWCOUNT;
	
END

