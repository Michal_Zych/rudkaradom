﻿CREATE PROCEDURE [dbo].[dev_TransmitterConfigurationSet]
	 @TransmitterId int = null -- insert
	,@Mac bigint
	,@BarCode nvarchar(100)
	,@Name nvarchar(100)
	,@TypeId int
	,@ReportIntervalSec int
	,@Sensitivity int
	,@AvgCalcTimeS int
	,@SwitchDelayS int
	,@IsBandUpdater bit = 0
	,@Msisdn nvarchar(40) = null
	,@Description nvarchar(max) = null
	,@RssiTreshold smallint
	,@Group tinyint
	,@IpAddress nvarchar(20)
	,@Transitions nvarchar(max)
	,@OperatorId int
	,@Reason nvarchar(max) = null
AS
BEGIN
	SET NOCOUNT OFF;
	DECLARE @UPDATE_ENDING_EVENT_ID bit = 1

	DECLARE @NEW_LINE nvarchar(2) = CHAR(10)
	DECLARE @ClientId int = 1



	DECLARE
		@EVENT_UPDATE dbo.EVENT_TYPE
		,@EVENT_CREATE dbo.EVENT_TYPE
		,@OBJECT_TYPE_TRANSMITTER dbo.OBJECT_TYPE
		,@SEVERITY_EVENT dbo.EVENT_SEVERITY
		,@eventType dbo.EVENT_TYPE
		,@currentDate datetime
		,@eventId int

		SET @currentDate = dbo.GetCurrentUtcDate()

		SELECT @EVENT_CREATE = [Create], @EVENT_UPDATE = [Update] FROM enum.EventType
		SELECT @OBJECT_TYPE_TRANSMITTER = Transmitter FROM enum.ObjectType
		SELECT @SEVERITY_EVENT = [Event] FROM enum.EventSeverity

	IF @TransmitterId IS NULL SET @eventType = @EVENT_CREATE
	ELSE SET @eventType = @EVENT_UPDATE

	BEGIN --żeby nie zajmować autonumerów, sprawdzenie duplikatów przed transakcją
	DECLARE 
		@errors TABLE(Id smallint, MacId bigint, MacString nvarchar(20), BarCode nvarchar(100))
	DECLARE @errorStr nvarchar(max) = ''
	INSERT @errors
	SELECT Id, Mac, MacString, BarCode
	FROM dbo.dev_Transmitters
	WHERE (Mac = @Mac OR BarCode = @BarCode)
		AND (Id <> @TransmitterId OR @TransmitterId IS NULL)
	SELECT @errorStr = @errorStr + 
		CASE 
			WHEN MacId = @Mac THEN 'MAC ' + MacString + ' jest już przypisany do transmitera Id = ' + CONVERT(nvarchar(10), Id)
			WHEN BarCode = @BarCode THEN 'Kod kreskowy ' + BarCode + ' jest już przypisany do transmitera Id = ' + CONVERT(nvarchar(10), Id)
			ELSE 'Nieznany błąd'
		END + @NEW_LINE
	FROM @errors

	IF @errorStr <>''
	BEGIN
		SELECT CONVERT(int, null) AS Result, -1 AS ErrorCode, @errorStr AS ErrorMessage
		RETURN
	END
	END
		BEGIN TRY
		BEGIN TRANSACTION
			IF @eventType = @EVENT_CREATE
			BEGIN
				INSERT dbo.dev_Transmitters(Mac, BarCode,ClientId, Name, TypeId, ReportIntervalSec, Sensitivity, AvgCalcTimeS, SwitchDelayS, IsBandUpdater, Msisdn, Description
					,RssiTreshold, [Group], IpAddress) VALUES
					(@Mac, @BarCode, @ClientId, @Name, @TypeId, @ReportIntervalSec, @Sensitivity, @AvgCalcTimeS, @SwitchDelayS, @IsBandUpdater, @Msisdn, @Description
					,@RssiTreshold, @Group, @IpAddress)
				SELECT @TransmitterId = SCOPE_IDENTITY()
			END
			ELSE -- update
			BEGIN
				UPDATE dbo.dev_Transmitters SET
					 Mac = @Mac
					,BarCode = @BarCode
					,Name = @Name
					,TypeId = @TypeId
					,ReportIntervalSec = @ReportIntervalSec
					,Sensitivity = @Sensitivity
					,AvgCalcTimeS = @AvgCalcTimeS
					,SwitchDelayS = @SwitchDelayS
					,IsBandUpdater = @IsBandUpdater
					,Msisdn = @Msisdn
					,Description = @Description
					,ClientId = @ClientId
					,RssiTreshold = @RssiTreshold
					,[Group] = @Group
					,IpAddress = @IpAddress
				WHERE Id = @TransmitterId

				DELETE dbo.dev_TransmitterTransitions WHERE TransmitterId = @TransmitterId
			END

			INSERT dbo.dev_TransmitterTransitions(TransmitterId, [Group])
			SELECT @TransmitterId, CONVERT(tinyint, Value) FROM dbo.fn_Split(@Transitions, ',')

			-- event	
			DECLARE @JSON nvarchar(max)
			EXEC @JSON = dbo.SerializeJSON_Transmitter @TransmitterId

			INSERT dbo.ev_Events(DateUtc, Type, Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, OperatorId, [EventData], Reason) 
			SELECT @currentDate, @eventType, @SEVERITY_EVENT, u.UnitId, @TransmitterId, @OBJECT_TYPE_TRANSMITTER, u.UnitID, @currentDate, @currentDate, @OperatorId, @JSON, @Reason
			FROM dbo.dev_Transmitters t
			LEFT JOIN dbo.dev_UnitTransmitter u ON u.TransmitterId = t.Id
			WHERE t.Id = @TransmitterId



			SELECT @eventId = SCOPE_IDENTITY()
			IF @UPDATE_ENDING_EVENT_ID = 1
			BEGIN
				UPDATE dbo.ev_Events SET EndingEventId = @eventId WHERE Id = @eventId
			END

		COMMIT TRANSACTION
		SELECT @TransmitterId AS Result, NULL as ErrorCode, NULL as ErrorMessage
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		SELECT CONVERT(int, null) AS Result, ERROR_NUMBER() AS ErrorCode, ERROR_MESSAGE() AS ErrorMessage
	END CATCH
END