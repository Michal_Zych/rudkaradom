﻿


CREATE procedure [dbo].[_tech_GetColumnTypes]
	@objectName nvarchar(100)
AS 
BEGIN
	SELECT 
	   c.name,
       t.name,
       c.max_length,
       c.precision,
       c.scale,
	   c.is_nullable
	FROM   sys.columns c
       JOIN sys.types t
         ON t.user_type_id = c.user_type_id
            AND t.system_type_id = c.system_type_id
	WHERE  object_id = OBJECT_ID(@objectName) 
END