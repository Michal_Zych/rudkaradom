﻿CREATE PROCEDURE [dbo].[unt_ZonesGet]
AS
BEGIN
	SELECT ut.UnitId, ut.TransmitterId, c.IsNoGoZone
	FROM dbo.dev_UnitTransmitter ut
	LEFT JOIN dbo.al_ConfigUnits c ON ut.UnitId = c.UnitId
END