﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	Edycja hasła użytkownika o podanym ID
-- =============================================
CREATE PROCEDURE [dbo].[usr_EditUserPassword]
(
	  @id int
	, @password USER_PASSWORD
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @OPERATION_DELETE tinyint = 0
		,@OPERATION_CREATE tinyint = 1
		,@OPERATION_EDIT tinyint = 2

		,@RECORD_UNIT tinyint = 0
		,@RECORD_USER tinyint = 1

		--kody błędów
	DECLARE
		@ERROR int = -1

	BEGIN TRANSACTION
		BEGIN TRY
			UPDATE dbo.Users 
			SET Password = @password,PasswordChangeDate = dbo.GetCurrentDate()
			WHERE ID = @id

			INSERT dbo.Logs(OperationDate, OperatorId, OperationType, ObjectType, ObjectId, Reason, Info)
					VALUES(dbo.GetCurrentDate(), @id, @OPERATION_EDIT, @RECORD_USER, @id, '', 'Zmiana hasła')

		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
			SELECT @ERROR
			RETURN
		END CATCH
	COMMIT TRANSACTION

	SELECT @Id
END


