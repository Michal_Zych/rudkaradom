﻿



CREATE PROCEDURE [dbo].[unt_PreviewForUnitGet]
	@UnitId int
	,@UserId int
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @zones TABLE(Id int, RoomName nvarchar(100), Location nvarchar(500), TLX real, TLY real, BRX real, BRY real)
	INSERT @zones
	SELECT uu.Id, uu.Name, dbo.GetUnitLocation(uu.Id, default, default, default), 0, 0, 1, 1
	FROM  dbo.Units_GetUserUnits_Func(@UserId) uu
	JOIN dbo.GetChildUnits(@UnitId) cu ON cu.Id = uu.Id
	JOIN dbo.dev_UnitTransmitter ut ON ut.UnitID = cu.Id


	UPDATE z SET
		TLX = p.TLX
		,TLY = p.TLY
		,BRX = p.BRX
		,BRY = p.BRY
	FROM @zones z
	JOIN dbo.PreviewsUnits p ON p.UnitId = z.Id
	WHERE p.PreviewedUnitID = @UnitId


	SELECT * from @zones
END