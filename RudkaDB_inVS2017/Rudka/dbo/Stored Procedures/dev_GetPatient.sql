﻿
CREATE PROCEDURE [dbo].[dev_GetPatient]
	@PatientId int
AS 
BEGIN
	SET NOCOUNT ON;
	SELECT 
		Id, 
		Name, 
		LastName, 
		Pesel, 
		RoomDateUtc, 
		RoomId,
		WardDateUtc,
		WardId
	FROM dbo.Patients 
	WHERE @PatientId = Id;
END