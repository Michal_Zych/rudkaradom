﻿

CREATE PROCEDURE [dbo].[Settings_GetSettings_old]
	@ClientId int
AS
BEGIN
	SELECT Name, 
		COALESCE(CONVERT (nvarchar(1024), [dbo].[settings_GetInt_old](ClientId, Name))
			, [dbo].[settings_GetString_old](ClientId, Name)) AS Value
	FROM dbo.Settings_old
	WHERE ClientId = @ClientId
	ORDER BY Name
END

