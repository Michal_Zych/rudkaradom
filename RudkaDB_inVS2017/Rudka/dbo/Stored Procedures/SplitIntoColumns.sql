﻿CREATE PROCEDURE dbo.SplitIntoColumns(
		 @text nvarchar(max)
		 ,@cols int
		 , @delimiter char(1) = ','
		 , @delimiterReplacment nvarchar(10) = '&comma;'
)
AS
BEGIN
		DECLARE
			@result TABLE(
				Col1 nvarchar(max), Col2 nvarchar(max), Col3 nvarchar(max), Col4 nvarchar(max), Col5 nvarchar(max),
				Col6 nvarchar(max), Col7 nvarchar(max), Col8 nvarchar(max), Col9 nvarchar(max), Col10 nvarchar(max))


		DECLARE @tempTable TABLE(Id int, Value nvarchar(max))
		INSERT @tempTable SELECT Id,  REPLACE(value, @delimiterReplacment, @delimiter) FROM dbo.fn_SplitWithEmpty(@text, @delimiter)

		INSERT @result
		SELECT A.Value, B.value, C.Value, D.Value, E.Value, 
			F.Value, G.Value, H.Value, I.Value, J.Value
		FROM @tempTable A
		LEFT JOIN @tempTable B ON A.id = B.id - 1
		LEFT JOIN @tempTable C ON B.id = C.id - 1
		LEFT JOIN @tempTable D ON C.id = D.id - 1
		LEFT JOIN @tempTable E ON D.id = E.id - 1
		LEFT JOIN @tempTable F ON E.id = F.id - 1
		LEFT JOIN @tempTable G ON G.id = G.id - 1
		LEFT JOIN @tempTable H ON H.id = H.id - 1
		LEFT JOIN @tempTable I ON I.id = I.id - 1
		LEFT JOIN @tempTable J ON J.id = J.id - 1
		WHERE A.id % @cols = 0
		
		SELECT * from @result
END