﻿CREATE PROCEDURE [dbo].[sync_GetSettings]
	@Key nvarchar(50) = 'DEFAULT'
AS   
BEGIN
    SET NOCOUNT ON;  
	SELECT
		[UserName]
      ,[Password]
      ,[ConnectionString]
      ,[TopicName]
      ,[EscapeValue]
      ,[EscapeValueLine]
      ,[CheckCmdsIntervalMilis]
      ,[NewQueryLimit]
      ,[WebServiceUserName]
      ,[WebServicePassword]
      ,[WebServiceURL]
      ,[ActiveMQConnectionId]
      ,[KeepAliveIntervalS]
	FROM tech.[sync_Configuration] WHERE [Key] = @Key
END