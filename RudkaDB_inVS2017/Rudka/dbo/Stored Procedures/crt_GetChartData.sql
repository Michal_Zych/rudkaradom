﻿
CREATE PROCEDURE [dbo].[crt_GetChartData]
	@dateFrom datetime,
	@dateTo datetime,
	@sensors IDs_list,
	@mode int = 2,
	@showAllAlarms bit = 0,
	@rowCount int = 1000
AS
BEGIN
	SET NOCOUNT ON;
		
	IF @showAllAlarms = 0 SET @mode = 0
	ELSE SET @mode = 2		
		
	DECLARE @SensorIDs TABLE (
		Number smallint identity(1,1),
		ID smallint)		
	
	INSERT @SensorIDs
	SELECT Value FROM dbo.SplitWordsToInt(@sensors)
	
	DECLARE @sensor smallint
		,@number smallint
	
	SELECT TOP(1) @number = Number, @sensor = ID FROM @SensorIDs
	
	WHILE @sensor is not null
	BEGIN
		exec dbo.crt_GetSensorChartData @sensor, @number, @dateFrom, @dateTo, @mode, @showAllAlarms, @rowCount
		
		SET @sensor = NULL
		SELECT TOP(1) @number = Number, @sensor = ID FROM @SensorIDs WHERE Number > @number
	END
	
END

