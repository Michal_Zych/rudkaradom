﻿

CREATE PROCEDURE [dbo].[sync_Send_TAG]
	@PatientId int
	,@OperatorId int
	,@OperationDateUtc dateTime = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE 
		@DEFAULT_USER_ID nvarchar(100) = '1'
		,@RECORD_TYPE char(4) = 'TAG'

	IF @OperationDateUtc IS NULL SET @OperationDateUtc = dbo.GetCurrentUTCDate()

	DECLARE 
		@bandCode nvarchar(100)
		,@assignDateUtc dateTime
		,@extModificationDate datetime
		,@extUserId nvarchar(100)
		,@extPatientId nvarchar(100)
		,@record nvarchar(max)

	SELECT @bandCode = b.BarCode, @assignDateUtc = bs.AssignDateUtc
	FROM dbo.Bands b
	JOIN dbo.ev_BandStatus bs ON b.Id = bs.BandId
	JOIN enum.ObjectType objectType ON bs.ObjectId = objectType.Patient
	WHERE bs.ObjectId = @PatientId


	SELECT @extModificationDate = COALESCE(@assignDateUtc, @OperationDateUtc, dbo.GetCurrentUtcDate())
	SELECT @extModificationDate = DATEADD(mi, DATEDIFF(mi, dbo.GetCurrentUTCDate(), dbo.GetCurrentDate()), @extModificationDate)

	SELECT @extUserId = ExtId
	FROM arch.ext_Users
	WHERE M2mId = @OperatorId
	
	SELECT @extPatientId = ExtId
	FROM arch.ext_Users
	WHERE M2mId = @PatientId

	

	IF @extPatientId IS NOT NULL
	BEGIN
		SET @record = 'MODIFICATION_DATE=''' + CONVERT(nvarchar(16), @extModificationDate, 121) 
				+ ''';PATIENT_ID=''' + @extPatientId 
				+ ''';TAG_ID=''' + COALESCE(@bandCode, '')
				+ ''';USER_ID=''' + COALESCE(@extUserId, @DEFAULT_USER_ID)
				+ ''''
		EXEC dbo.sync_SendRecord @RECORD_TYPE, @record
	END
	ELSE
	BEGIN
		SET @record = 'MODIFICATION_DATE=''' + CONVERT(nvarchar(16), @extModificationDate, 121) 
			+ ''';m2mPatientId=''' + CONVERT(nvarchar(10), @PatientId)
			+ ''';TAG_ID=''' + COALESCE(@bandCode, '')
			+ ''';USER_ID=''' + COALESCE(@extUserId, @DEFAULT_USER_ID)
			+ ''''
		INSERT tech.sync_RecordsOUT(RecType, DateUtc, Record, Try, TryAgain, SentDateUtc, Error) 
		VALUES(@RECORD_TYPE, @OperationDateUtc, @record, 1, 0, @OperationDateUtc, 'Niezamapowany pacjent')
	END


END