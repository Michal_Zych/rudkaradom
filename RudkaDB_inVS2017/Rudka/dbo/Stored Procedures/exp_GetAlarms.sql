﻿

-- =============================================
-- Author:		
-- Create date: 
-- Description:	zwraca magazyny danego klienta
-- =============================================
CREATE PROCEDURE [dbo].[exp_GetAlarms]
		@ClientID int		
		,@MaxRows int
		,@UnitID int
		,@DateFrom datetime
		,@DateTo datetime
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Alarms TABLE(
		DateStart datetime
		,DateEnd datetime
		,LoRange real
		,UpRange real
		,Value real
		,UserStatus nvarchar(200)
		,DateStatus datetime
		,CommentStatus nvarchar(2048))		

	INSERT @Alarms
	SELECT TOP(@MaxRows) a.Timestamp, a.DateEnd, a.LoRange * s.Scale, a.UpRange * s.Scale
		,m.Value * s.Scale, usr.LastName + ' ' + usr.Name, a.DateStatus, a.CommentStatus
	FROM dbo.Alarms a WITH(NOLOCK)
	JOIN dbo.Sensors s WITH(NOLOCK) ON s.Id = a.SensorID
	JOIN dbo.Units u WITH(NOLOCK) ON s.Unitid = u.Id AND u.ClientID = @ClientID AND u.ID = @UnitID
	LEFT JOIN dbo.Measurements m WITH(NOLOCK) ON m.SensorID = a.SensorId AND m.MeasurementTime = a.Timestamp
	LEFT JOIN dbo.Users usr WITH(NOLOCK) ON usr.ID = a.UserStatus
	WHERE a.Timestamp BETWEEN @DateFrom AND @DateTo
		OR ((a.Timestamp < @DateFrom) AND (COALESCE(a.DateEnd, dbo.GetCurrentDate()) > @DateFrom))
	ORDER BY a.Timestamp
	
	DECLARE @rows int

	SELECT @rows = COUNT(1)
	FROM dbo.Alarms a WITH(NOLOCK)
	JOIN dbo.Sensors s WITH(NOLOCK) ON s.Id = a.SensorID
	JOIN dbo.Units u WITH(NOLOCK) ON s.Unitid = u.Id AND u.ClientID = @ClientID AND u.ID = @UnitID
	WHERE a.Timestamp BETWEEN @DateFrom AND @DateTo
		OR ((a.Timestamp < @DateFrom) AND (COALESCE(a.DateEnd, dbo.GetCurrentDate()) > @DateFrom))
	
	IF @MaxRows < @rows
		INSERT @Alarms VALUES(null, null, null, null, null, null, null, null)
		
	SELECT * FROM @Alarms
END


