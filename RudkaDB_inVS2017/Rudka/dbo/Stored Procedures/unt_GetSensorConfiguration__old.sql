﻿
-- =============================================
-- Author:		
-- Create date: 
-- Description:	zwraca czujniki skojarzone z unitem (lub jego bezpośrednimi podunitami)
-- =============================================
CREATE PROCEDURE [dbo].[unt_GetSensorConfiguration__old]
	@unitId int
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @DEFAULT_SCALE real = 0.1

	SELECT ID, SocketID, Interval, (Calibration * COALESCE(Scale, @DEFAULT_SCALE)) AS Calibration, [Enabled], MU, 
		(LoRange * COALESCE(Scale, @DEFAULT_SCALE)) AS LoRange,
		(UpRange * COALESCE(Scale, @DEFAULT_SCALE)) AS UpRange,
		LegendDescription, LegendShortcut, LegendRangeDescription,
		LineColor, LineWidth, LineStyle,
		RangeLineWidth, RangeLineStyle,
		AlarmColor, AlarmCommColor,
		AlDisabled, AlDisabledForRanges, AlDisabledForHours,
		CONVERT(datetime, AlDisableHoursFrom1) AS AlDisableHoursFrom1, 
		CONVERT(datetime, AlDisableHoursTo1) AS AlDisableHoursTo1, 
		CONVERT(datetime, AlDisableHoursFrom2) AS AlDisableHoursFrom2, 
		CONVERT(datetime, AlDisableHoursTo2) AS AlDisableHoursTo2,
		GuiRangeDelay, GuiCommDelay, 
		SmsEnabled, SmsRangeDelay, SmsCommDelay, 
		MailEnabled, MailRangeDelay, MailCommDelay
	FROM dbo.Sensors
	WHERE Unitid =@unitId

END

