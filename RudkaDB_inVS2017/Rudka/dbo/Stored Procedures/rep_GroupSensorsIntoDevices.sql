﻿CREATE  PROCEDURE [dbo].[rep_GroupSensorsIntoDevices]
	@UserReportID int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @UnitSensor TABLE(
		UnitId int,
		SensorId smallint
	)		
	DECLARE @DeviceSensors TABLE(
		DeviceID int,
		Location nvarchar(1024),
		Sensors nvarchar(max)
	)		

	INSERT @UnitSensor
	SELECT u.ParentUnitID, s.Id
	FROM UserReportSensors r
	JOIN Sensors s ON s.Id = r.SensorID
	JOIN Units u ON s.UnitId = u.ID
	WHERE r.UserReportID = @UserReportID

	INSERT @DeviceSensors
	SELECT DISTINCT US2.UnitId, dbo.GetUnitLocation(US2.UnitId, 1, 0, default), 
		(
			SELECT CONVERT(nvarchar(4), US1.SensorID) +','  
			FROM @UnitSensor US1
			WHERE US1.UnitId = US2.UnitId
			FOR XML PATH('')
		)
	FROM @UnitSensor US2

	SELECT DeviceID AS UnitId, s.SensorId, Location, LEFT(Sensors, LEN(Sensors) - 1) AS Sensors
	FROM @DeviceSensors d
	JOIN @UnitSensor s ON d.DeviceID = s.UnitId

END

