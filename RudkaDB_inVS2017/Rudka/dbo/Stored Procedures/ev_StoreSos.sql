﻿CREATE PROCEDURE [dbo].[ev_StoreSos]
	@BandId smallint
	,@EventDateUtc dateTime = NULL
AS
BEGIN
	SET NOCOUNT ON;

/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
	BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'BandId=' + COALESCE(CONVERT(nvarchar(100), @BandId), 'NULL') 
					+ ';EventDateUtc=' + COALESCE(CONVERT(nvarchar(50), @EventDateUtc, 121), 'NULL')
		EXEC Log_Action @@PROCID, @info
	END
/****************************************************/


	DECLARE
		@UPDATE_ENDING_EVENT_ID bit = 1

	DECLARE
		@currentDateUtc datetime = dbo.GetCurrentUtcDate()

	IF @EventDateUtc IS NULL SET @EventDateUtc = @currentDateUtc


BEGIN -- zmienne konfiguracji
	DECLARE
		 @objectType				dbo.OBJECT_TYPE   
		,@objectId					dbo.OBJECT_ID  
		,@objectUnitId				int  
		,@currentUnitId				int  
		,@inCurrentUnitUtc			dateTime  
		,@previousUnitId			int  
		,@inPreviousUnitUtc			dateTime  
		,@topAlarmId				int  
		,@topAlarmType				dbo.EVENT_TYPE   
		,@topAlarmSeverity			dbo.EVENT_SEVERITY   
		,@topAlarmDateUtc			dateTime 
		,@topEventDateUtc			dateTime  
		,@severity1Count			int  
		,@severity2Count			int  
		,@ongNoGoZoneInEventId		int  
		,@ongNoGoZoneInAlarmId		int  
		,@ongDisconnectedEventId	int  
		,@ongDisconnectedAlarmId	int  
		,@ongZoneOutEventId			int  
		,@ongZoneOutAlarmId			int  
		,@ongNoMovingEventId		int  
		,@ongNoMovingAlarmId		int  
		,@ongMovingEventId			int  
		,@ongMovingAlarmId			int  
		,@ongLowBatteryEventId		int  
		,@ongLowBatteryAlarmId		int 

		,@isPatientSosCalling bit
		,@isUnitSosCalling bit

		,@currentEventType			dbo.EVENT_TYPE
		,@currentEventSeverity		dbo.EVENT_SEVERITY
		,@numbersCount int
END

BEGIN	-- pobranie konfiguracji
	SELECT
		 @objectType					= bs.ObjectType			
		,@objectId						= bs.ObjectId				
		,@objectUnitId					= bs.ObjectUnitId			
		,@currentUnitId					= bs.CurrentUnitId			
		,@inCurrentUnitUtc				= bs.InCurrentUnitUtc		
		,@previousUnitId				= bs.PreviousUnitId		
		,@inPreviousUnitUtc				= bs.InPreviousUnitUtc		
		,@topAlarmId					= bs.TopAlarmId			
		,@topAlarmType					= bs.TopAlarmType			
		,@topAlarmSeverity				= bs.TopAlarmSeverity		  
		,@topAlarmDateUtc				= bs.TopAlarmDateUtc		
		,@topEventDateUtc				= bs.TopEventDateUtc		
		,@severity1Count				= bs.Severity1Count		
		,@severity2Count				= bs.Severity2Count		
		,@ongNoGoZoneInEventId			= bs.OngNoGoZoneInEventId  
		,@ongNoGoZoneInAlarmId			= bs.OngNoGoZoneInAlarmId  
		,@ongDisconnectedEventId		= bs.OngDisconnectedEventId
		,@ongDisconnectedAlarmId		= bs.OngDisconnectedAlarmId
		,@ongZoneOutEventId				= bs.OngZoneOutEventId		
		,@ongZoneOutAlarmId				= bs.OngZoneOutAlarmId		
		,@ongNoMovingEventId			= bs.OngNoMovingEventId	
		,@ongNoMovingAlarmId			= bs.OngNoMovingAlarmId	
		,@ongMovingEventId				= bs.OngMovingEventId		
		,@ongMovingAlarmId				= bs.OngMovingAlarmId		
		,@ongLowBatteryEventId			= bs.OngLowBatteryEventId  
		,@ongLowBatteryAlarmId			= bs.OngLowBatteryAlarmId  

		,@isPatientSosCalling = COALESCE(cp.IsSosCalling, 0)
		,@isUnitSosCalling = COALESCE(cu.IsSosCalling, 0)

		,@currentEventType				= eventType.SOS
		,@currentEventSeverity			= eventSeverity.Alarm
	FROM enum.EventType eventType  
	JOIN enum.ObjectType objectType ON 1 = 1
	JOIN enum.EventSeverity eventSeverity ON 1 = 1
	LEFT JOIN dbo.ev_BandStatus bs ON bs.BandId = @BandId
	LEFT JOIN dbo.al_ConfigPatients cp ON bs.ObjectId = cp.PatientId AND bs.ObjectType = objectType.Patient
	LEFT JOIN dbo.al_ConfigUnits cu  ON COALESCE(bs.CurrentUnitId, bs.ObjectUnitid) = cu.UnitId
END	

	--zakończ brak łączności
	IF @ongDisconnectedEventId IS NOT NULL
	BEGIN
		EXEC [dbo].[ev_SetBandConnected]
			@BandId						= @BandId
			,@currentDateUtc			= @currentDateUtc
			,@CurrentEventDateUtc		= @EventDateUtc
			,@CurrentEventUnitId		= @currentUnitId
			,@ObjectId					= @objectId				
			,@ObjectType				= @objectType
			,@ObjectUnitId				= @objectUnitId
			,@CurrentUnitId				= @currentUnitId			OUTPUT
			,@InCurrentUnitUtc			= @inCurrentUnitUtc			OUTPUT
			,@PreviousUnitId			= @previousUnitId			OUTPUT
			,@InPreviousUnitUtc			= @inPreviousUnitUtc		OUTPUT
			,@OngDisconnectedEventId	= @ongDisconnectedEventId	OUTPUT
			,@OngDisconnectedAlarmId	= @ongDisconnectedAlarmId	OUTPUT
	END
	
	-- zarejestrowanie rekordu statusu dla opaski
	IF @objectId IS NULL
	BEGIN
		EXEC dbo.ev_AssignBandToItself
			@EventDateUtc		= @EventDateUtc
			,@BandId			= @BandId
			,@ObjectId			= @objectId			OUTPUT
			,@ObjectType		= @objectType		OUTPUT
			,@InCurrentUnitUtc	= @inCurrentUnitUtc	OUTPUT
			,@severity1Count	= @severity1Count	OUTPUT
			,@severity2Count	= @severity2Count	OUTPUT
	END

	-- generuj alarm
	DECLARE
		@currentEventId int
	
	INSERT ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc, EndDateUtc, EndingEventId)
	SELECT @EventDateUtc, @currentEventType, @currentEventSeverity, @currentUnitId, @objectId, @objectType, @objectUnitId, @currentDateUtc, @EventDateUtc, null
	SET @currentEventId = SCOPE_IDENTITY()
	IF @UPDATE_ENDING_EVENT_ID = 1
	BEGIN
		UPDATE dbo.ev_Events SET EndingEventId = @currentEventId WHERE Id = @currentEventId
	END

	INSERT dbo.ev_Alarms(EventId, RaisingEventId) VALUES (@currentEventId, @currentEventId)
	SELECT @Severity2Count = @Severity2Count + 1

	-- dzwonienie
	IF @isPatientSosCalling = 1 OR @isUnitSosCalling = 1
	BEGIN
		INSERT dbo.ev_CallOrdersDetails
		SELECT @currentEventId, 1, SequenceNr, Phone
		FROM dbo.al_CallingGroups
		JOIN enum.ObjectType objectType ON  ObjectType = objectType.Unit
		WHERE @isUnitSosCalling = 1 AND COALESCE(@currentUnitId, -1) <> @objectUnitId AND Id = COALESCE(@currentUnitId, @objectUnitId)
		UNION
		SELECT @currentEventId, 0, SequenceNr, Phone
		FROM dbo.al_CallingGroups
		WHERE @isPatientSosCalling = 1 AND ObjectType = @objectType AND Id = @objectId
		SELECT @numbersCount = @@ROWCOUNT
		IF @numbersCount >  0
		BEGIN
			INSERT dbo.ev_CallOrders VALUES
			(@currentEventId, @EventDateUtc, dbo.ev_GetSosMessage(COALESCE(@currentUnitId, @objectUnitId), @EventDateUtc, @objectType, @objectId, @objectUnitId))
			
			DECLARE @eventData nvarchar(max)
			EXEC @eventData = dbo.SerializeJSON_CallOrders @currentEventId
			UPDATE ev_Events SET [EventData] = @eventData WHERE Id = @currentEventId
		END
	END

	-- uaktualnij status
	EXEC dbo.ev_GenerateBandStatus
		 @BandId					= @BandId 
		,@CurrentUnitId 			= @CurrentUnitId 
		,@InCurrentUnitUtc			= @InCurrentUnitUtc
		,@PreviousUnitId 			= @PreviousUnitId 
		,@InPreviousUnitUtc 		= @InPreviousUnitUtc 
		,@TopAlarmId 				= @TopAlarmId 
		,@TopAlarmType  			= @TopAlarmType  
		,@TopAlarmSeverity 			= @TopAlarmSeverity 
		,@TopAlarmDateUtc 			= @TopAlarmDateUtc 
		,@TopEventDateUtc 			= @TopEventDateUtc 
		,@Severity1Count 			= @Severity1Count 
		,@Severity2Count 			= @Severity2Count 
		,@OngNoGoZoneInEventId 		= @OngNoGoZoneInEventId 
		,@OngNoGoZoneInAlarmId  	= @OngNoGoZoneInAlarmId  
		,@OngDisconnectedEventId  	= @OngDisconnectedEventId 
		,@OngDisconnectedAlarmId  	= @OngDisconnectedAlarmId 
		,@OngZoneOutEventId  		= @OngZoneOutEventId  
		,@OngZoneOutAlarmId  		= @OngZoneOutAlarmId  
		,@OngNoMovingEventId  		= @OngNoMovingEventId  
		,@OngNoMovingAlarmId  		= @OngNoMovingAlarmId  
		,@OngMovingEventId  		= @OngMovingEventId  
		,@OngMovingAlarmId  		= @OngMovingAlarmId  
		,@OngLowBatteryEventId  	= @OngLowBatteryEventId  
		,@OngLowBatteryAlarmId 		= @OngLowBatteryAlarmId 
		,@CurrentEventId			= @currentEventId
		,@CurrentEventType 			= @CurrentEventType 
		,@CurrentEventDateUtc 		= @EventDateUtc 
		,@GeneratedAlarmId			= @currentEventId
		,@GeneratedAlarmSeverity 	= @currentEventSeverity 
		,@GeneratedWarningDateUtc 	= null
		,@GeneratedAlarmDateUtc 	= @EventDateUtc 
END