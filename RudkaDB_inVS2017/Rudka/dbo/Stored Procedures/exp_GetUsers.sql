﻿

-- =============================================
-- Author:		
-- Create date: 
-- Description:	zwraca użytkowników danego klienta
-- =============================================
CREATE PROCEDURE dbo.[exp_GetUsers]
		@ClientID int		
AS
BEGIN
	SET NOCOUNT ON;
		
	SELECT ID, LastName, Name
	FROM dbo.Users
	WHERE ClientID = @ClientID AND Active = 1
	ORDER BY ID
END


