﻿CREATE PROCEDURE [dbo].[ev_AssignPatientToWardRoom]
	@PatientId int
	,@CodeType nvarchar(10)
	,@Code nvarchar(100)

	,@NewWardId int
	,@NewRoomId int

	,@OperatorId int
	,@Reason nvarchar(max) = null

	,@IsExternalEvent bit = 0  
	,@OperationDateUtc dateTime = null
AS
BEGIN
	SET NOCOUNT ON;

	/****************************************************/
	DECLARE @info nvarchar(max)
	SET @info = 'PatientId=' + COALESCE(CONVERT(nvarchar(10), @PatientId), 'NULL') 
		+ ';CodeType=' + COALESCE(@CodeType, 'NULL')
		+ ';Code=' + COALESCE(@Code, 'NULL')
		+ ';NewWardId=' + COALESCE(CONVERT(nvarchar(10), @NewWardId), 'NULL')
		+ ';NewRoomId=' + COALESCE(CONVERT(nvarchar(10), @NewRoomId), 'NULL')
		+ ';OperatorId=' + COALESCE(CONVERT(nvarchar(10), @OperatorId), 'NULL')
		+ ';Reason=' + COALESCE(@Reason, 'NULL')
		+ ';IsExternalEvent=' + COALESCE(CONVERT(nvarchar(10), @IsExternalEvent), 'NULL')
		+ ';OperationDateUtc=' + COALESCE(CONVERT(nvarchar(30), @OperationDateUtc, 121), 'NULL')
	EXEC Log_Action @@PROCID, @info
	/****************************************************/

	BEGIN TRY
		BEGIN TRANSACTION

		EXEC dbo.ev_AssignPatientToWardRoom_core @PatientId, @CodeType, @Code, @NewWardId, @NewRoomId, @OperatorId, @Reason, @IsExternalEvent, @OperationDateUtc

		SELECT @PatientId AS Result, NULL as ErrorCode, NULL as ErrorMessage
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		SELECT CONVERT(int, null) AS Result, ERROR_NUMBER() AS ErrorCode, ERROR_MESSAGE() AS ErrorMessage
	END CATCH	
END