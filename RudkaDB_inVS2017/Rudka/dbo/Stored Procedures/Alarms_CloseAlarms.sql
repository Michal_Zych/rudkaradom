﻿



CREATE  PROCEDURE [dbo].[Alarms_CloseAlarms]
	@ids IDs_LIST,
	@comment ALARM_COMMENT,
	@userid int,
	@tEscalation int = 0
AS
BEGIN
	SET NOCOUNT ON;
	
DECLARE	
		 @closeTime datetime = [dbo].[GetCurrentDate]()
			--typy powiadomień
		, @GUI			tinyint = 0
		, @SMS			tinyint = 1
		, @EMAIL		tinyint = 2

-- dbo.FormatAlarmNotifyText(@SensorId, @SMS, @MeasurementTime, 'C', IsRangeAlarm, null, null, null, null)	

DECLARE @notifications TABLE(
	  SensorId smallint
	, AlarmID int
	, Type tinyint not null
	, SentAfterDate datetime
	, IsRangeAlarm bit
	, ValueStr nvarchar(10)
	, LoRangeStr nvarchar(10)
	, UpRangeStr nvarchar(10)
	, UserName nvarchar(100)
)

DECLARE @AlarmIDs TABLE (
	ID int not null
)
INSERT @AlarmIDs --tabela identyfikatorów zamykanych alarmów
SELECT Value FROM dbo.SplitWordsToInt(@ids)


	INSERT @notifications
	SELECT --sms range
		a.SensorID
		,a.ID
		,@SMS
		,@closeTime
		,1
		,CASE WHEN s.IsBitSensor = 1 AND mc.HyperValue = 0 THEN s.LoStateDesc
			  WHEN s.IsBitSensor = 1 AND mc.HyperValue <> 0 THEN s.HiStateDesc
			ELSE REPLACE(CONVERT(nvarchar(20), mc.HyperValue * s.Scale), '.', ',')
		  END
		,CASE WHEN s.IsBitSensor = 1 AND a.LoRange = 0 THEN s.LoStateDesc
			  WHEN s.IsBitSensor = 1 AND a.LoRange <> 0 THEN s.HiStateDesc
			ELSE REPLACE(CONVERT(nvarchar(20), a.LoRange * s.Scale), '.', ',')
		  END
		,CASE WHEN s.IsBitSensor = 1 AND a.UpRange = 0 THEN s.LoStateDesc
			  WHEN s.IsBitSensor = 1 AND a.UpRange <> 0 THEN s.HiStateDesc
			ELSE REPLACE(CONVERT(nvarchar(20), a.UpRange * s.Scale), '.', ',')
		  END
		, COALESCE(u.Name + ' ', '') + COALESCE(u.LastName, '')
	FROM dbo.Alarms a
	JOIN dbo.AlarmsOngoing ao WITH(NOLOCK) on a.SensorID = ao.SensorID and ao.MeasureAlID = a.Id and ao.MeasureAlLastSmsTime IS NOT NULL
	JOIN dbo.Sensors s WITH(NOLOCK) on s.Id = a.SensorID AND s.SmsRangeAfterClose = 1
	JOIN dbo.MeasurementsCurrent mc WITH(NOLOCK) on mc.SensorID = a.SensorID
	JOIN dbo.Users u WITH(NOLOCK) on u.Id = @userid
	WHERE a.ID IN (SELECT ID FROM @AlarmIDs)
	UNION --email range
		SELECT 
		a.SensorID
		,a.ID
		,@EMAIL
		,@closeTime
		,1
		,CASE WHEN s.IsBitSensor = 1 AND mc.HyperValue = 0 THEN s.LoStateDesc
			  WHEN s.IsBitSensor = 1 AND mc.HyperValue <> 0 THEN s.HiStateDesc
			ELSE REPLACE(CONVERT(nvarchar(20), mc.HyperValue * s.Scale), '.', ',')
		  END
		,CASE WHEN s.IsBitSensor = 1 AND a.LoRange = 0 THEN s.LoStateDesc
			  WHEN s.IsBitSensor = 1 AND a.LoRange <> 0 THEN s.HiStateDesc
			ELSE REPLACE(CONVERT(nvarchar(20), a.LoRange * s.Scale), '.', ',')
		  END
		,CASE WHEN s.IsBitSensor = 1 AND a.UpRange = 0 THEN s.LoStateDesc
			  WHEN s.IsBitSensor = 1 AND a.UpRange <> 0 THEN s.HiStateDesc
			ELSE REPLACE(CONVERT(nvarchar(20), a.UpRange * s.Scale), '.', ',')
		  END
		, COALESCE(u.Name + ' ', '') + COALESCE(u.LastName, '')
	FROM dbo.Alarms a
	JOIN dbo.AlarmsOngoing ao WITH(NOLOCK) on a.SensorID = ao.SensorID and ao.MeasureAlID = a.Id and ao.MeasureAlLastEmailTime IS NOT NULL
	JOIN dbo.Sensors s WITH(NOLOCK) on s.Id = a.SensorID AND s.MailRangeAfterClose = 1
	JOIN dbo.MeasurementsCurrent mc WITH(NOLOCK) on mc.SensorID = a.SensorID
	JOIN dbo.Users u WITH(NOLOCK) on u.Id = @userid
	WHERE a.ID IN (SELECT ID FROM @AlarmIDs)	
	UNION	
		SELECT --sms comm
		a.SensorID
		,a.ID
		,@SMS
		,@closeTime
		,0
		,CASE WHEN s.IsBitSensor = 1 AND mc.HyperValue = 0 THEN s.LoStateDesc
			  WHEN s.IsBitSensor = 1 AND mc.HyperValue <> 0 THEN s.HiStateDesc
			ELSE REPLACE(CONVERT(nvarchar(20), mc.HyperValue * s.Scale), '.', ',')
		  END
		,CASE WHEN s.IsBitSensor = 1 AND a.LoRange = 0 THEN s.LoStateDesc
			  WHEN s.IsBitSensor = 1 AND a.LoRange <> 0 THEN s.HiStateDesc
			ELSE REPLACE(CONVERT(nvarchar(20), a.LoRange * s.Scale), '.', ',')
		  END
		,CASE WHEN s.IsBitSensor = 1 AND a.UpRange = 0 THEN s.LoStateDesc
			  WHEN s.IsBitSensor = 1 AND a.UpRange <> 0 THEN s.HiStateDesc
			ELSE REPLACE(CONVERT(nvarchar(20), a.UpRange * s.Scale), '.', ',')
		  END
		, COALESCE(u.Name + ' ', '') + COALESCE(u.LastName, '')
	FROM dbo.Alarms a
	JOIN dbo.AlarmsOngoing ao WITH(NOLOCK) on a.SensorID = ao.SensorID and ao.CommAlID = a.Id and ao.CommAlLastSmsTime IS NOT NULL
	JOIN dbo.Sensors s WITH(NOLOCK) on s.Id = a.SensorID AND s.SmsCommAfterClose = 1
	JOIN dbo.MeasurementsCurrent mc WITH(NOLOCK) on mc.SensorID = a.SensorID
	JOIN dbo.Users u WITH(NOLOCK) on u.Id = @userid
	WHERE a.ID IN (SELECT ID FROM @AlarmIDs)
	UNION	
		SELECT --email comm
		a.SensorID
		,a.ID
		,@EMAIL
		,@closeTime
		,0
		,CASE WHEN s.IsBitSensor = 1 AND mc.HyperValue = 0 THEN s.LoStateDesc
			  WHEN s.IsBitSensor = 1 AND mc.HyperValue <> 0 THEN s.HiStateDesc
			ELSE REPLACE(CONVERT(nvarchar(20), mc.HyperValue * s.Scale), '.', ',')
		  END
		,CASE WHEN s.IsBitSensor = 1 AND a.LoRange = 0 THEN s.LoStateDesc
			  WHEN s.IsBitSensor = 1 AND a.LoRange <> 0 THEN s.HiStateDesc
			ELSE REPLACE(CONVERT(nvarchar(20), a.LoRange * s.Scale), '.', ',')
		  END
		,CASE WHEN s.IsBitSensor = 1 AND a.UpRange = 0 THEN s.LoStateDesc
			  WHEN s.IsBitSensor = 1 AND a.UpRange <> 0 THEN s.HiStateDesc
			ELSE REPLACE(CONVERT(nvarchar(20), a.UpRange * s.Scale), '.', ',')
		  END
		, COALESCE(u.Name + ' ', '') + COALESCE(u.LastName, '')
	FROM dbo.Alarms a
	JOIN dbo.AlarmsOngoing ao WITH(NOLOCK) on a.SensorID = ao.SensorID and ao.CommAlID = a.Id and ao.CommAlLastEmailTime IS NOT NULL
	JOIN dbo.Sensors s WITH(NOLOCK) on s.Id = a.SensorID AND s.MailCommAfterClose = 1
	JOIN dbo.MeasurementsCurrent mc WITH(NOLOCK) on mc.SensorID = a.SensorID
	JOIN dbo.Users u WITH(NOLOCK) on u.Id = @userid
	WHERE a.ID IN (SELECT ID FROM @AlarmIDs)

BEGIN --OBSŁUGA POWIADOMIEŃ
	INSERT dbo.AlarmNotifications
	SELECT
		SensorId, AlarmId, SentAfterDate, null, Type,
		dbo.FormatAlarmNotifyText(SensorId, Type, SentAfterDate, 'C', IsRangeAlarm, ValueStr, LoRangeStr, UpRangeStr, UserName)	
		,null, null, CONVERT(nvarchar(30), @closeTime, 121) + ':Alarms_CloseAlarms(Dodano SentDate=CloseTime)'
	FROM @notifications
END


BEGIN--ZAMYKANIE ALARMÓW	
	UPDATE a
	SET
		Status = 1,
		UserStatus = COALESCE(UserStatus, @userid),
		DateStatus = COALESCE(DateStatus, @closetime),
		CommentStatus = CASE 
							WHEN COALESCE(Status, 0) = 0 
							THEN COALESCE(CommentStatus + CHAR(13), '')
								+ CONVERT(varchar(25), @closeTime, 120) + ' Zamknięcie' 
								+ CHAR(13) + COALESCE(u.Name, '') + ' ' + COALESCE(u.LastName, '')
								+ COALESCE(CHAR(13) + @comment, '')
							ELSE COALESCE(CommentStatus, '')
								+ CHAR(13) + CONVERT(varchar(25), @closeTime, 120) + ' Ponowne zamknięcie'
								+ CHAR(13) + COALESCE(u.Name, '') + ' ' + COALESCE(u.LastName, '')
								+ COALESCE(CHAR(13) + @comment, '')
						END
	FROM dbo.Alarms a
	LEFT JOIN dbo.Users u ON u.ID = @userid
	JOIN @AlarmIDs ids ON a.ID = ids.ID
END

	SELECT 1
END
