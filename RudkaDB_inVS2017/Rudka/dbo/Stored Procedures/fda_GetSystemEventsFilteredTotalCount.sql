﻿CREATE PROCEDURE [dbo].[fda_GetSystemEventsFilteredTotalCount]
@IdFrom int = null
,@IdTo int = null
,@DateUtcFrom datetime = null
,@DateUtcTo datetime = null
,@TypeFrom tinyint = null
,@TypeTo tinyint = null
,@TypeDescription nvarchar(200) = null
,@UnitIdFrom int = null
,@UnitIdTo int = null
,@UnitLocation nvarchar(200) = null
,@ObjectIdFrom int = null
,@ObjectIdTo int = null
,@ObjectTypeFrom tinyint = null
,@ObjectTypeTo tinyint = null
,@ObjectTypeDescription nvarchar(200) = null
,@ObjectName nvarchar(200) = null
,@OperatorIdFrom int = null
,@OperatorIdTo int = null
,@OperatorName nvarchar(200) = null
,@EventData nvarchar(200) = null
,@Reason nvarchar(200) = null
,@FromArchive bit = 0
AS
BEGIN

 DECLARE @cmd nvarchar(max)
 SET @cmd = 'SELECT COUNT(1) '
		+ dbo.dsql_GetSystemEvent_From(@FromArchive)
		+ dbo.dsql_GetSystemEvent_Where(
			@IdFrom, @IdTo, @DateUtcFrom, @DateUtcTo, @TypeFrom, @TypeTo
			, @TypeDescription, @UnitIdFrom, @UnitIdTo, @UnitLocation, @ObjectIdFrom, @ObjectIdTo
			, @ObjectTypeFrom, @ObjectTypeTo, @ObjectTypeDescription, @ObjectName, @OperatorIdFrom, @OperatorIdTo
			, @OperatorName, @EventData, @Reason)

 EXEC(@cmd)
--print @cmd
END