﻿
CREATE PROCEDURE [dbo].[al_CheckUnitsWithDisconnectedTransmitter]
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT ut.UnitID
	FROM ev_TransmitterStatus t
	JOIN dev_UnitTransmitter ut ON ut.TransmitterId = t.TransmitterId
	WHERE t.OngDisconnectedEventId IS NOT NULL
END