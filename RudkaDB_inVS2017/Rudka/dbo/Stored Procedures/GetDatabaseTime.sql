﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- ================================================
CREATE PROCEDURE [dbo].[GetDatabaseTime]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT dbo.GetCurrentDate() AS DateTime
END

