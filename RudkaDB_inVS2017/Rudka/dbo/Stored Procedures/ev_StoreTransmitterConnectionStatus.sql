﻿CREATE PROCEDURE [dbo].[ev_StoreTransmitterConnectionStatus]
	@TransmitterId smallint
	,@EventDateUtc dateTime = NULL
	,@IsConnected bit = 1
AS
BEGIN
	SET NOCOUNT ON;

	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
	BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'TransmitterId=' + COALESCE(CONVERT(nvarchar(100), @TransmitterId), 'NULL') 
					+ ';EventDateUtc=' + COALESCE(CONVERT(nvarchar(50), @EventDateUtc, 121), 'NULL')
					+ ';IsConnected=' + COALESCE(CONVERT(nvarchar(100), @IsConnected), 'NULL')
		EXEC Log_Action @@PROCID, @info
	END
/****************************************************/

	DECLARE
		@currentDateUtc datetime = dbo.GetCurrentUtcDate()
		,@openedEventId int

	IF @EventDateUtc IS NULL SET @EventDateUtc = @currentDateUtc

BEGIN -- zmiennne konfiguracji
	DECLARE
		@OBJECT_TYPE_TRANSMITTER	dbo.OBJECT_TYPE

		,@ongDisconnectedEventId	int
		,@ongDisconnectedAlarmId	int
		,@ongDisconnectedAlarmSeverity dbo.EVENT_SEVERITY
		,@OngConnectedEventId		int

		,@warningActive				bit     
		,@warningDelayMins			smallint
		,@alarmActive				bit     
		,@alarmDelayMins			smallint
		,@currentEventType			dbo.EVENT_TYPE
		,@connectedEventType		dbo.EVENT_TYPE
		,@disconnectedEventType		dbo.EVENT_TYPE
		,@warningAutoCloseMessage	nvarchar(max)
		,@unitId					int
		,@idFromStatus				smallint
END

BEGIN -- pobranie konfiguracji
	SELECT
		@idFromStatus				= ts.TransmitterId
		,@ongDisconnectedEventId	= ts.OngDisconnectedEventId
		,@ongDisconnectedAlarmId	= ts.OngDisconnectedAlarmId
		,@ongDisconnectedAlarmSeverity = ts.OngDisconnectedAlarmSeverity
		,@OngConnectedEventId		= ts.OngConnectedEventId

		,@warningActive				= COALESCE(cu.NoTransmitterWrActive , st.NoTransmitterWrActive)
		,@warningDelayMins			= COALESCE(cu.NoTransmitterWrMins	, st.NoTransmitterWrMins)
		,@alarmActive				= COALESCE(cu.NoTransmitterAlActive	, st.NoTransmitterAlActive)
		,@alarmDelayMins			= COALESCE(cu.NoTransmitterAlMins	, st.NoTransmitterAlMins)
		,@currentEventType			= CASE WHEN @IsConnected = 1 THEN eventType.Connected ELSE eventType.Disconnected END
		,@connectedEventType		= eventType.Connected
		,@disconnectedEventType		= eventType.Disconnected
		,@OBJECT_TYPE_TRANSMITTER	= objectType.Transmitter
		,@warningAutoCloseMessage	= st.WarningAutoCloseMessage
		,@unitId					= ut.UnitId
	FROM dbo.dev_Transmitters t
	LEFT JOIN dbo.ev_TransmitterStatus ts ON ts.TransmitterId = t.Id
	LEFT JOIN dbo.dev_UnitTransmitter ut ON ut.TransmitterId = t.Id
	LEFT JOIN dbo.al_ConfigUnits cu ON cu.UnitId = COALESCE(ut.UnitId, 0)
	JOIN dbo.ev_Settings st ON 1 = 1
	JOIN enum.EventType eventType  ON 1 = 1
	JOIN enum.ObjectType objectType ON 1 = 1
	WHERE t.Id = @TransmitterId
END

	IF (@IsConnected = 1 AND @ongConnectedEventId IS NOT NULL)
		OR (@IsConnected = 0 AND @ongDisconnectedEventId IS NOT NULL)
	BEGIN
		RETURN
	END

	IF @IsCOnnected = 1
	BEGIN -- Connected
		EXEC dbo.ev_GenerateEvent
			@currentDateUtc 	= @currentDateUtc 
			,@EventDateUtc 		= @EventDateUtc 
			,@EventType 		= @currentEventType 
			,@EventUnitId 		= @unitId 
			,@EventObjectId 	= @TransmitterId 
			,@EventObjectType 	= @OBJECT_TYPE_TRANSMITTER 
			,@EventObjectUnitId = @unitId 
			,@EventData 		= NULL 
			,@IsLongEvent 		= 1 
			,@FinishedEventId 	= @ongDisconnectedEventId OUTPUT
			,@FinishedAlarmId 	= @ongDisconnectedAlarmId OUTPUT
			,@EventId 			= @ongConnectedEventId OUTPUT
		SELECT @ongDisconnectedAlarmSeverity = NULL

		/*
		DELETE dbo.ev_EventsToRaise
		WHERE RaisingObjectType = @OBJECT_TYPE_TRANSMITTER 
			AND RaisingObjectId = @TransmitterId
			AND RaiseDateUtc >= @EventDateUtc

		-- wygeneruj zdarzenie Transmitter Connected
		INSERT dbo.ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc) 
		SELECT @EventDateUtc, @currentEventType, EventSeverity.Event, @unitId, @TransmitterId, @OBJECT_TYPE_TRANSMITTER, @unitId, @currentDateUtc
		FROM enum.EventSeverity EventSeverity
		SET @ongConnectedEventId = SCOPE_IDENTITY()
		
		IF @ongDisconnectedEventId IS NOT NULL
		BEGIN -- zakończ zdarzenia/alarmy transmitter disconnected
			UPDATE dbo.ev_Events SET
				EndDateUtc = @EventDateUtc
				,EndingEventId = @ongConnectedEventId
			WHERE Id >= @ongDisconnectedEventId AND Type = @disconnectedEventType AND ObjectId = @TransmitterId AND ObjectType = @OBJECT_TYPE_TRANSMITTER AND EndingEventId IS NULL
			SELECT @ongDisconnectedEventId = NULL, @ongDisconnectedAlarmId = NULL, @ongDisconnectedAlarmSeverity = NULL
		END
		*/
	END
	ELSE 
	BEGIN -- Disconnected
		-- wygeneruj zdarzenie Transmittery DisConnected
		DECLARE @ongConnectedAlarmId int = NULL
		EXEC dbo.ev_GenerateEvent
			@currentDateUtc 	= @currentDateUtc 
			,@EventDateUtc 		= @EventDateUtc 
			,@EventType 		= @currentEventType 
			,@EventUnitId 		= @unitId 
			,@EventObjectId 	= @TransmitterId 
			,@EventObjectType 	= @OBJECT_TYPE_TRANSMITTER 
			,@EventObjectUnitId = @unitId 
			,@EventData 		= NULL 
			,@IsLongEvent 		= 1 
			,@FinishedEventId 	= @ongConnectedEventId OUTPUT
			,@FinishedAlarmId 	= @ongConnectedAlarmId OUTPUT
			,@EventId 			= @ongDisconnectedEventId OUTPUT
		SELECT @ongDisconnectedAlarmSeverity = NULL
		/*



		INSERT ev_Events(DateUtc, [Type], Severity, UnitId, ObjectId, ObjectType, ObjectUnitId, StoreDateUtc)
		SELECT @EventDateUtc, @currentEventType, eventSeverity.Event, @unitId, @TransmitterId, @OBJECT_TYPE_TRANSMITTER, @unitId, @currentDateUtc
		FROM enum.EventSeverity eventSeverity
		SET @ongDisconnectedEventId = SCOPE_IDENTITY()

		IF @ongConnectedEventId IS NOT NULL
		BEGIN -- zakońć zdarzenie transmitter connected
			UPDATE dbo.ev_Events SET
				EndDateUtc = @EventDateUtc
				,EndingEventId = @ongDisconnectedEventId
			WHERE Id = @ongConnectedEventId
			SELECT @ongConnectedEventId = NULL
		END
		*/
		
		-- wygeneruj alarmy dla zdarzenia
		DECLARE @warningDateUtc dateTime
				,@alarmDateUtc dateTime
				,@severity1Count int
				,@severity2Count int
			
		EXEC dbo.ev_GenerateAlarmsForEvent
			 @EventObjectType			= @OBJECT_TYPE_TRANSMITTER
			,@EventObjectId				= @TransmitterId
			,@currentDateUtc			= @currentDateUtc
			,@CurrentEventDateUtc		= @EventDateUtc
			,@currentEventId			= @ongDisconnectedEventId
			,@currentEventType			= @currentEventType
			,@currentUnitId				= @unitId
			,@objectId					= @TransmitterId
			,@objectType				= @OBJECT_TYPE_TRANSMITTER
			,@objectUnitId				= @unitId

			,@warningCloseMessage		= @warningAutoCloseMessage

			,@WarningActive				= @WarningActive
			,@WarningDelayMins			= @WarningDelayMins
			,@AlarmActive				= @AlarmActive
			,@AlarmDelayMins			= @AlarmDelayMins

			,@CurrrentAlarmSeverity		= @ongDisconnectedAlarmSeverity	OUTPUT
			,@CurrrentAlarmId			= @ongDisconnectedAlarmId		OUTPUT
			,@warningDateUtc			= @warningDateUtc				OUTPUT
			,@alarmDateUtc				= @alarmDateUtc					OUTPUT
			,@severity1Count			= @severity1Count				OUTPUT
			,@severity2Count			= @severity2Count				OUTPUT
	END

	-- uaktualnij status Transmitera
	IF @idFromStatus IS NULL
		INSERT ev_TransmitterStatus (TransmitterId, OngDisconnectedEventId, OngDisconnectedAlarmId, OngDisconnectedAlarmSeverity, OngConnectedEventId) VALUES
		(@TransmitterId, @ongDisconnectedEventId, @ongDisconnectedAlarmId, @ongDisconnectedAlarmSeverity, @ongConnectedEventId)
	ELSE
		UPDATE ev_TransmitterStatus SET
			OngDisconnectedEventId = @ongDisconnectedEventId
			,OngDisconnectedAlarmId = @ongDisconnectedAlarmId
			,OngDisconnectedAlarmSeverity = @ongDisconnectedAlarmSeverity
			,OngConnectedEventId = @ongConnectedEventId
		WHERE TransmitterId = @TransmitterId

END
