﻿CREATE  PROCEDURE [dbo].[_Alarms_CloseAlarm]
	@id int,
	@comment nvarchar(1024),
	@userid int
AS
BEGIN
	SET NOCOUNT ON;

	update Alarms set
		Status = 1,
		UserStatus = @userid,
		DateStatus = dbo.GetCurrentDate(),
		CommentStatus = @comment
	where ID = @id
	
	select 1
	
END

