﻿CREATE PROCEDURE fda_GetServiceStatusFilteredTotalCount
@TreeUnitIds nvarchar(max),
@ServiceName nvarchar(200) = null
,@Description nvarchar(200) = null
,@StartTimeUtcFrom datetime = null
,@StartTimeUtcTo datetime = null
,@PingTimeUtcFrom datetime = null
,@PingTimeUtcTo datetime = null
,@PreviousPingTimeUtcFrom datetime = null
,@PreviousPingTimeUtcTo datetime = null
,@HasPartnerFrom bit = null
,@HasPartnerTo bit = null
,@PartnerPingTimeUtcFrom datetime = null
,@PartnerPingTimeUtcTo datetime = null
,@InfoTxt nvarchar(200) = null
AS
BEGIN

 DECLARE @cmd nvarchar(max)
 SET @cmd = 'SELECT COUNT(1) '
		+ dbo.dsql_GetServiceStatus_From()
		+ dbo.dsql_GetServiceStatus_Where(@TreeUnitIds,
			@ServiceName, @Description, @StartTimeUtcFrom, @StartTimeUtcTo, @PingTimeUtcFrom, @PingTimeUtcTo
			, @PreviousPingTimeUtcFrom, @PreviousPingTimeUtcTo, @HasPartnerFrom, @HasPartnerTo, @PartnerPingTimeUtcFrom, @PartnerPingTimeUtcTo
			, @InfoTxt)

 EXEC(@cmd)
-- print @cmd
END