﻿

--exec [dbo].[rep_MeasureArray] 7, 1

CREATE PROCEDURE [dbo].[rep_MeasureReport1H]
	@UserReportID int
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @precision int = 3 -- do ilu miejsc po przecinku zaokrąglać wyniki

	DECLARE
		@dateFrom datetime
		,@dateTo datetime
	
	SELECT @dateFrom = DateFrom, @dateTo = DateTo FROM dbo.UsersReports WHERE ID = @UserReportId

	DECLARE @Data TABLE(
		SensorID smallint,
		DateDay datetime,
		Value0h smallint,  Value1h smallint,  Value2h smallint,  Value3h smallint,  Value4h smallint,  Value5h smallint,  Value6h smallint,  Value7h smallint,  Value8h smallint,  Value9h smallint,  Value10h smallint, Value11h smallint, 
		Value12h smallint, Value13h smallint, Value14h smallint, Value15h smallint, Value16h smallint, Value17h smallint, Value18h smallint, Value19h smallint, Value20h smallint, Value21h smallint, Value22h smallint, Value23h smallint)

	INSERT @Data
	EXEC [dbo].[rep_MeasureArray] @UserReportId, 1

	DECLARE @Statistics TABLE(
		SensorID smallint,
		MinValue smallint,
		MaxValue smallint,
		AvgValue smallint,
		Alarms int)

	INSERT @Statistics
	SELECT SensorId, Min(Value), Max(value), avg(value), null
	FROM (
			SELECT SensorId, Value
			FROM 
				(SELECT SensorId, Value0h,  Value1h,  Value2h,  Value3h,  Value4h,  Value5h,  Value6h,  Value7h,  Value8h,  Value9h,  Value10h, Value11h,
								  Value12h, Value13h, Value14h, Value15h, Value16h, Value17h, Value18h, Value19h, Value20h, Value21h, Value22h, Value23h
				FROM @Data) d
			UNPIVOT
				(Value FOR H IN 
					( Value0h,  Value1h,  Value2h,  Value3h,  Value4h,  Value5h,  Value6h,  Value7h,  Value8h,  Value9h,  Value10h, Value11h,
					  Value12h, Value13h, Value14h, Value15h, Value16h, Value17h, Value18h, Value19h, Value20h, Value21h, Value22h, Value23h)
			)as unpv
		) as A
	GROUP BY SensorID

	UPDATE s
	 SET Alarms = AlarmsCount
	FROM @Statistics s 
	LEFT JOIN dbo.UserReportSensors urs WITH(NOLOCK) ON urs.SensorID = s.SensorID AND urs.UserReportID = @UserReportId

	DECLARE @Ranges TABLE(
		SensorID smallint,
		DateDay datetime,
		FromHour int,
		ToHour int,
		LoRange smallint,
		UpRange smallint)
	INSERT @Ranges
	EXEC [dbo].[rep_GetMeasureRangesByDay] @UserReportId, @dateFrom, @dateTo


	DECLARE @Devices TABLE(
		UnitID int,
		SensorId smallint,
		Location nvarchar(1024),
		Sensors nvarchar(max))
	INSERT @Devices
	EXEC dbo.rep_GroupSensorsIntoDevices @UserReportId



	SELECT 
		dev.Location, dev.Sensors,
		s.Id AS SensorId, u.Name, s.LegendShortcut, s.MU, 
		ROUND(st.MinValue *s.Scale, @precision) AS MinValue, ROUND(st.MaxValue * s.Scale, @precision) AS MaxValue, ROUND(st.AvgValue * s.Scale, @precision) AS AvgValue, st.Alarms,
		r.DateDay, ROUND(r.LoRange * s.Scale, @precision) AS LoRange, ROUND(r.UpRange * s.Scale, @precision) AS UpRange,
		CASE WHEN 0 = FromHour THEN Value0h * s.Scale ELSE NULL END AS Value0h,
		CASE WHEN 1 >= FromHour AND  1 < ToHour THEN ROUND(Value1h * s.Scale, @precision) ELSE NULL END AS Value1h,
		CASE WHEN 2 >= FromHour AND  2 < ToHour THEN ROUND(Value2h * s.Scale, @precision) ELSE NULL END AS Value2h,
		CASE WHEN 3 >= FromHour AND  3 < ToHour THEN ROUND(Value3h * s.Scale, @precision) ELSE NULL END AS Value3h,
		CASE WHEN 4 >= FromHour AND  4 < ToHour THEN ROUND(Value4h * s.Scale, @precision) ELSE NULL END AS Value4h,
		CASE WHEN 5 >= FromHour AND  5 < ToHour THEN ROUND(Value5h * s.Scale, @precision) ELSE NULL END AS Value5h,
		CASE WHEN 6 >= FromHour AND  6 < ToHour THEN ROUND(Value6h * s.Scale, @precision) ELSE NULL END AS Value6h,
		CASE WHEN 7 >= FromHour AND  7 < ToHour THEN ROUND(Value7h * s.Scale, @precision) ELSE NULL END AS Value7h,
		CASE WHEN 8 >= FromHour AND  8 < ToHour THEN ROUND(Value8h * s.Scale, @precision) ELSE NULL END AS Value8h,
		CASE WHEN 9 >= FromHour AND  9 < ToHour THEN ROUND(Value9h * s.Scale, @precision) ELSE NULL END AS Value9h,
		CASE WHEN 10 >= FromHour AND  10 < ToHour THEN ROUND(Value10h * s.Scale, @precision) ELSE NULL END AS Value10h,
		CASE WHEN 11 >= FromHour AND  11 < ToHour THEN ROUND(Value11h * s.Scale, @precision) ELSE NULL END AS Value11h,
		CASE WHEN 12 >= FromHour AND  12 < ToHour THEN ROUND(Value12h * s.Scale, @precision) ELSE NULL END AS Value12h,
		CASE WHEN 13 >= FromHour AND  13 < ToHour THEN ROUND(Value13h * s.Scale, @precision) ELSE NULL END AS Value13h,
		CASE WHEN 14 >= FromHour AND  14 < ToHour THEN ROUND(Value14h * s.Scale, @precision) ELSE NULL END AS Value14h,
		CASE WHEN 15 >= FromHour AND  15 < ToHour THEN ROUND(Value15h * s.Scale, @precision) ELSE NULL END AS Value15h,
		CASE WHEN 16 >= FromHour AND  16 < ToHour THEN ROUND(Value16h * s.Scale, @precision) ELSE NULL END AS Value16h,
		CASE WHEN 17 >= FromHour AND  17 < ToHour THEN ROUND(Value17h * s.Scale, @precision) ELSE NULL END AS Value17h,
		CASE WHEN 18 >= FromHour AND  18 < ToHour THEN ROUND(Value18h * s.Scale, @precision) ELSE NULL END AS Value18h,
		CASE WHEN 19 >= FromHour AND  19 < ToHour THEN ROUND(Value19h * s.Scale, @precision) ELSE NULL END AS Value19h,
		CASE WHEN 20 >= FromHour AND  20 < ToHour THEN ROUND(Value20h * s.Scale, @precision) ELSE NULL END AS Value20h,
		CASE WHEN 21 >= FromHour AND  21 < ToHour THEN ROUND(Value21h * s.Scale, @precision) ELSE NULL END AS Value21h,
		CASE WHEN 22 >= FromHour AND  22 < ToHour THEN ROUND(Value22h * s.Scale, @precision) ELSE NULL END AS Value22h,
		CASE WHEN 23 >= FromHour AND  23 <= ToHour THEN ROUND(Value22h * s.Scale, @precision) ELSE NULL END AS Value23h,
		s.IsBitSensor,
		s.LoStateDesc,
		s.HiStateDesc
	FROM @Data d
	JOIN @Ranges r ON d.SensorID = r.SensorID AND d.DateDay = r.DateDay
	JOIN dbo.Sensors s WITH(NOLOCK) ON d.SensorId = s.Id
	JOIN dbo.Units u WITH(NOLOCK) ON u.ID = s.UnitId
	LEFT JOIN @Statistics st ON d.SensorID = st.SensorID
	JOIN @Devices dev ON dev.SensorId = d.SensorID
	Order BY dev.Location, d.SensorID, r.DateDay, r.FromHour

END
