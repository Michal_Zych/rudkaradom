﻿CREATE PROCEDURE fda_GetEventsDetailsFilteredTotalCount
@TreeUnitIds nvarchar(max),
@IdFrom int = null
,@IdTo int = null
,@DateUtcFrom datetime = null
,@DateUtcTo datetime = null
,@TypeFrom tinyint = null
,@TypeTo tinyint = null
,@TypeDescription nvarchar(200) = null
,@SeverityFrom tinyint = null
,@SeverityTo tinyint = null
,@SeverityDescription nvarchar(200) = null
,@UnitIdFrom int = null
,@UnitIdTo int = null
,@UnitName nvarchar(200) = null
,@UnitLocation nvarchar(200) = null
,@ObjectIdFrom int = null
,@ObjectIdTo int = null
,@ObjectTypeFrom tinyint = null
,@ObjectTypeTo tinyint = null
,@ObjectTypeDescription nvarchar(200) = null
,@ObjectName nvarchar(200) = null
,@ObjectUnitIdFrom int = null
,@ObjectUnitIdTo int = null
,@ObjectUnitName nvarchar(200) = null
,@ObjectUnitLocation nvarchar(200) = null
,@EndDateUtcFrom datetime = null
,@EndDateUtcTo datetime = null
,@EndingEventIdFrom int = null
,@EndingEventIdTo int = null
,@OperatorIdFrom int = null
,@OperatorIdTo int = null
,@OperatorName nvarchar(200) = null
,@OperatorLastName nvarchar(200) = null
,@EventData nvarchar(200) = null
,@Reason nvarchar(200) = null
,@RaisingEventIdFrom int = null
,@RaisingEventIdTo int = null
,@IsClosedFrom bit = null
,@IsClosedTo bit = null
,@ClosingDateUtcFrom datetime = null
,@ClosingDateUtcTo datetime = null
,@ClosingUserIdFrom int = null
,@ClosingUserIdTo int = null
,@ClosingUserName nvarchar(200) = null
,@ClosingUserLastName nvarchar(200) = null
,@ClosingAlarmIdFrom int = null
,@ClosingAlarmIdTo int = null
,@ClosingComment nvarchar(200) = null
,@UserMessageIdFrom int = null
,@UserMessageIdTo int = null
,@SenderIdFrom int = null
,@SenderIdTo int = null
,@SenderName nvarchar(200) = null
,@SenderLastName nvarchar(200) = null
,@Message nvarchar(200) = null
AS
BEGIN

 DECLARE @cmd nvarchar(max)
 SET @cmd = 'SELECT COUNT(1) '
		+ dbo.dsql_GetEventsDetails_From()
		+ dbo.dsql_GetEventsDetails_Where(@TreeUnitIds,
			@IdFrom, @IdTo, @DateUtcFrom, @DateUtcTo, @TypeFrom, @TypeTo
			, @TypeDescription, @SeverityFrom, @SeverityTo, @SeverityDescription, @UnitIdFrom, @UnitIdTo
			, @UnitName, @UnitLocation, @ObjectIdFrom, @ObjectIdTo, @ObjectTypeFrom, @ObjectTypeTo
			, @ObjectTypeDescription, @ObjectName, @ObjectUnitIdFrom, @ObjectUnitIdTo, @ObjectUnitName, @ObjectUnitLocation
			, @EndDateUtcFrom, @EndDateUtcTo, @EndingEventIdFrom, @EndingEventIdTo, @OperatorIdFrom, @OperatorIdTo
			, @OperatorName, @OperatorLastName, @EventData, @Reason, @RaisingEventIdFrom, @RaisingEventIdTo
			, @IsClosedFrom, @IsClosedTo, @ClosingDateUtcFrom, @ClosingDateUtcTo, @ClosingUserIdFrom, @ClosingUserIdTo
			, @ClosingUserName, @ClosingUserLastName, @ClosingAlarmIdFrom, @ClosingAlarmIdTo, @ClosingComment, @UserMessageIdFrom
			, @UserMessageIdTo, @SenderIdFrom, @SenderIdTo, @SenderName, @SenderLastName, @Message
			)

 EXEC(@cmd)
-- print @cmd
END