﻿CREATE  PROCEDURE [dbo].[rep_AlarmsReport]
	@UserReportID int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @result TABLE(
		UnitID int,
		SensorID smallint,
		Location nvarchar(1024),
		Sensors nvarchar(max))

	INSERT @result
	EXEC dbo.rep_GroupSensorsIntoDevices @userReportID


	SELECT *
	FROM 
	(
		SELECT  UnitID, Location, Sensors, SUM(AlarmsCount) AS Suma
		FROM @result r
		JOIN dbo.UserReportSensors s ON s.UserReportID = @UserReportID AND s.SensorID = r.SensorID
		GROUP BY UnitID, Location, Sensors
	) Sub
	WHERE Suma > 0
	ORDER BY Location

END

