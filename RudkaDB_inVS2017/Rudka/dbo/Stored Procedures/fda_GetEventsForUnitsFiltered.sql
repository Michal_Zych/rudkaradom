﻿CREATE PROCEDURE [dbo].[fda_GetEventsForUnitsFiltered]
@PageNum int
,@PageSize int

,@TreeUnitIds nvarchar(max),
@IdFrom int = null
,@IdTo int = null
,@DateUtcFrom datetime = null
,@DateUtcTo datetime = null
,@TypeFrom tinyint = null
,@TypeTo tinyint = null
,@TypeDescription nvarchar(200) = null
,@SeverityFrom tinyint = null
,@SeverityTo tinyint = null
,@SeverityDescription nvarchar(200) = null
,@UnitName nvarchar(200) = null
,@UnitLocation nvarchar(200) = null
,@ObjectId nvarchar(200) = null
,@ObjectTypeFrom int = null
,@ObjectTypeTo int = null
,@ObjectTypeDescriptionFrom tinyint = null
,@ObjectTypeDescriptionTo tinyint = null
,@ObjectName nvarchar(200) = null
,@ObjectUnitId nvarchar(200) = null
,@ObjectUnitNameFrom int = null
,@ObjectUnitNameTo int = null
,@ObjectUnitLocation nvarchar(200) = null
--,@EndDateUtc nvarchar(200) = null
,@EndDateUtcFrom datetime = null
,@EndDateUtcTo datetime = null
--JZ
,@EndingEventIdFrom datetime = null
,@EndingEventIdTo datetime = null
,@OperatorIdFrom int = null
,@OperatorIdTo int = null
,@OperatorNameFrom int = null
,@OperatorNameTo int = null
,@OperatorLastName nvarchar(200) = null
,@EventData nvarchar(200) = null
,@Reason nvarchar(200) = null
,@RaisingEventId nvarchar(200) = null
,@IsClosedFrom int = null
,@IsClosedTo int = null
,@ClosingDateUtcFrom bit = null
,@ClosingDateUtcTo bit = null
,@ClosingUserIdFrom datetime = null
,@ClosingUserIdTo datetime = null
,@ClosingUserNameFrom int = null
,@ClosingUserNameTo int = null
,@ClosingUserLastName nvarchar(200) = null
,@ClosingAlarmId nvarchar(200) = null
,@ClosingCommentFrom int = null
,@ClosingCommentTo int = null
,@UserMessageId nvarchar(200) = null
,@SenderIdFrom int = null
,@SenderIdTo int = null
,@SenderNameFrom int = null
,@SenderNameTo int = null
,@SenderLastName nvarchar(200) = null
,@Message nvarchar(200) = null
,@SortOrder nvarchar(100) = null
AS
BEGIN

	declare @info nvarchar(max)
	SET @info = 'PageNum=' + COALESCE(CONVERT(nvarchar(10), @PageNum), 'null') + ';'
			+'PageSize=' + COALESCE(CONVERT(nvarchar(10), @PageSize), 'null') + ';'
			+'TreeUnitIds=' + COALESCE(@TreeUnitIds, 'null') + ';'
	INSERT _LogTemp (LogDate, Info) values (GETDATE(), @info)


 DECLARE @cmd nvarchar(max)

 SET @cmd = 'WITH orn AS ( SELECT ROW_NUMBER() OVER ('
		+ dbo.dsql_GetEventsForUnits_OrderBY(@SortOrder) + ') AS RowNum, A.* FROM (SELECT '
		+ dbo.dsql_GetEventsForUnits_Select()
		+ dbo.dsql_GetEventsForUnits_From()
		+ dbo.dsql_GetEventsForUnits_Where(@TreeUnitIds,
			@IdFrom, @IdTo, @DateUtcFrom, @DateUtcTo, @TypeFrom, @TypeTo
			, @TypeDescription, @SeverityFrom, @SeverityTo, @SeverityDescription, @UnitName
			, @UnitLocation, @ObjectId, @ObjectTypeFrom, @ObjectTypeTo, @ObjectTypeDescriptionFrom, @ObjectTypeDescriptionTo
			, @ObjectName, @ObjectUnitId, @ObjectUnitNameFrom, @ObjectUnitNameTo, @ObjectUnitLocation, @EndDateUtcFrom, @EndDateUtcTo --@EndDateUtc --JZ
			, @EndingEventIdFrom, @EndingEventIdTo, @OperatorIdFrom, @OperatorIdTo, @OperatorNameFrom, @OperatorNameTo
			, @OperatorLastName, @EventData, @Reason, @RaisingEventId, @IsClosedFrom, @IsClosedTo
			, @ClosingDateUtcFrom, @ClosingDateUtcTo, @ClosingUserIdFrom, @ClosingUserIdTo, @ClosingUserNameFrom, @ClosingUserNameTo
			, @ClosingUserLastName, @ClosingAlarmId, @ClosingCommentFrom, @ClosingCommentTo, @UserMessageId, @SenderIdFrom
			, @SenderIdTo, @SenderNameFrom, @SenderNameTo, @SenderLastName, @Message)
		+ 'UNION ALL SELECT '
		+ dbo.dsql_GetEventsForUnits_Select()
		+ REPLACE(REPLACE(REPLACE(dbo.dsql_GetEventsForUnits_From(), 'dbo.ev_Events', 'arch.ev_Events'), 'dbo.ev_Alarms', 'arch.ev_Alarms'), 'e.ev_UsersAlarms', 'arch.ev_UsersAlarms')
		+ dbo.dsql_GetEventsForUnits_Where(@TreeUnitIds,
			@IdFrom, @IdTo, @DateUtcFrom, @DateUtcTo, @TypeFrom, @TypeTo
			, @TypeDescription, @SeverityFrom, @SeverityTo, @SeverityDescription, @UnitName
			, @UnitLocation, @ObjectId, @ObjectTypeFrom, @ObjectTypeTo, @ObjectTypeDescriptionFrom, @ObjectTypeDescriptionTo
			, @ObjectName, @ObjectUnitId, @ObjectUnitNameFrom, @ObjectUnitNameTo, @ObjectUnitLocation, @EndDateUtcFrom, @EndDateUtcTo --@EndDateUtc --JZ
			, @EndingEventIdFrom, @EndingEventIdTo, @OperatorIdFrom, @OperatorIdTo, @OperatorNameFrom, @OperatorNameTo
			, @OperatorLastName, @EventData, @Reason, @RaisingEventId, @IsClosedFrom, @IsClosedTo
			, @ClosingDateUtcFrom, @ClosingDateUtcTo, @ClosingUserIdFrom, @ClosingUserIdTo, @ClosingUserNameFrom, @ClosingUserNameTo
			, @ClosingUserLastName, @ClosingAlarmId, @ClosingCommentFrom, @ClosingCommentTo, @UserMessageId, @SenderIdFrom
			, @SenderIdTo, @SenderNameFrom, @SenderNameTo, @SenderLastName, @Message)
		+ ') A )
		SELECT *
		FROM orn
		WHERE RowNum BETWEEN ' + CONVERT(nvarchar(20), @PageNum * @PageSize +1) + ' AND '
		+ CONVERT(nvarchar(20), (@PageNum + 1) * @PageSize) + ' ORDER BY rowNum'

 EXEC(@cmd)
-- print @cmd
END