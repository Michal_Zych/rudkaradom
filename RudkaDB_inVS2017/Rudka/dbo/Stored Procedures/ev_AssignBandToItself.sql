﻿
CREATE PROCEDURE [dbo].[ev_AssignBandToItself]
	@EventDateUtc dateTime
	,@BandId smallint
	,@ObjectId dbo.OBJECT_ID		OUTPUT
	,@ObjectType dbo.OBJECT_TYPE	OUTPUT
	,@InCurrentUnitUtc dateTime		OUTPUT
	,@Severity1Count int			OUTPUT
	,@Severity2Count int			OUTPUT
AS
BEGIN
	SET NOCOUNT OFF;

	/****************************************************/
	DECLARE @logAction bit 
	SELECT @logAction = COALESCE(SqlLoggingIsOn, 0) FROM dbo.ev_Settings

	IF @logAction = 1
		BEGIN
		DECLARE @info nvarchar(max)
		SET @info = 'BandId=' + COALESCE(CONVERT(nvarchar(100), @BandId), 'NULL') 
		EXEC Log_Action @@PROCID, @info
	END
	/****************************************************/

	DECLARE @SYSTEM_USER_ID int = 0
		,@OBJECT_TYPE_BAND dbo.OBJECT_TYPE = (SELECT Band FROM enum.ObjectType)

	EXEC dbo.ev_AssignBandToObject_core @SYSTEM_USER_ID, @EventDateUtc, NULL, @BandId, NULL, @BandId, @OBJECT_TYPE_BAND, 0

	SELECT @ObjectId = @BandId, @ObjectType  = @OBJECT_TYPE_BAND, @InCurrentUnitUtc = @EventDateUtc
		,@Severity1Count = 0, @Severity2Count = 0
END