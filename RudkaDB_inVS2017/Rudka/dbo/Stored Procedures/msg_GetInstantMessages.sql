﻿

CREATE PROCEDURE [dbo].[msg_GetInstantMessages] @userId INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE
		  @INSTANT_MESSAGE tinyint	= 0
		, @LOGIN_MESSAGE tinyint	= 1

	DECLARE @msg TABLE(ID int, Message nvarchar(MAX));

	DECLARE
		@currentDate dateTime = dbo.GetCurrentDate()


	INSERT @msg
	SELECT Id, Message 
	FROM dbo.Messages 
	WHERE ClientId = (SELECT TOP(1) ClientID FROM dbo.Users WHERE ID = @userId)
		AND Mode = @INSTANT_MESSAGE
		AND @currentDate BETWEEN ActiveFrom AND ActiveTo
		AND NOT EXISTS  (SELECT 1 FROM dbo.MessagesLog WHERE MessageID = ID AND ReceiverID = @userId)

	INSERT dbo.MessagesLog 
	SELECT ID, @userId, @currentDate, 1
	FROM @msg
	
	SELECT Message FROM @msg
END
