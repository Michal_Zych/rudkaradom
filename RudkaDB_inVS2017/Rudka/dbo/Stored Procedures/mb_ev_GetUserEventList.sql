﻿-- =============================================
-- Author:		kt
-- Create date: 2017-11-29
-- Description:	gets events for user (staff)
-- =============================================
CREATE PROCEDURE [dbo].[mb_ev_GetUserEventList]
    @userId   int,
    @hoursBack int = 1
AS
  BEGIN
    SET NOCOUNT ON;

	DECLARE @ids varchar(1000)

	SET @ids = ''
	SELECT @ids = CONVERT(nvarchar(10), Id) + ',' + @ids FROM dbo.Units_GetUserUnits_Func(@userId)
	SET @ids = LEFT(@ids, LEN(@ids) - 1)

	DECLARE @events TABLE (
		RowNum                  bigint,
		Id                      bigint,
		DateUtc					datetime,	
		Type					int,
		TypeDescription			nvarchar(max),
		Severity				int,
		SeverityDescription		nvarchar(max),
		UnitId					int,
		UnitName				nvarchar(max),
		UnitLocation			nvarchar(max),
		ObjectId				int,
		ObjectType				int,
		ObjectTypeDescription	nvarchar(max),
		ObjectName				nvarchar(max),
		ObjectUnitId			int,
		ObjectUnitName			nvarchar(max),
		ObjectUnitLocation		nvarchar(max),
		EndDateUtc				datetime,
		EndingEventId			int,
		OperatorId				int,
		OperatorName			nvarchar(max),
		OperatorLastName		nvarchar(max),
		EventData				nvarchar(max),
		Reason					nvarchar(max),
		RaisingEventId			int,
		IsClosed				bit,
		ClosingDateUtc			datetime,
		ClosingUserId			int,
		ClosingUserName			nvarchar(max),
		ClosingUserLastName		nvarchar(max),
		ClosingAlarmId			int,
		ClosingComment			nvarchar(max),
		UserMessageId			int,
		SenderId				int,
		SenderName				nvarchar(max),
		SenderLastName			nvarchar(max),
		Message					nvarchar(max)
	);

	INSERT INTO @events
	exec [dbo].[fda_GetEventsForUnitsFiltered] --0, 50 , @ids
	@PageNum = 0
	,@PageSize = 50
	,@TreeUnitIds = @ids
	,@ObjectTypeFrom = 4
	,@SortOrder = '-Id'

	SELECT TOP 50
		Id,
		Type,
		Severity,
		DateUtc                   AS StartTime,
		EndDateUtc                AS EndTime,
		ObjectName			      AS PatientDisplayName,
		ObjectId                  AS PatientId,
		UnitName                  AS UnitName,
		UnitLocation              AS ParentUnitName
	FROM @events
	WHERE ObjectName IS NOT NULL

	/*
    DECLARE @ids varchar(1000)

    DECLARE @patients TABLE(
      RowNum                     bigint,
      Id                         bigint,
      BarCode                    nvarchar(MAX),
      MacString                  nvarchar(MAX),
      Uid                        bigint,
      SerialNumber               nvarchar(MAX),
      BatteryTypeId              int,
      BatteryTypeName            nvarchar(MAX),
      BatteryAlarmSeverityId     int,
      BatteryAlarmSeverityName   nvarchar(MAX),
      BatteryInstallationDateUtc datetime,
      BatteryDaysToExchange      int,
      Description                nvarchar(MAX),
      ObjectTypeId               int,
      ObjectTypeName             nvarchar(MAX),
      ObjectDisplayName          nvarchar(MAX),
      UnitId                     int,
      UnitName                   nvarchar(MAX),
      UnitLocation               nvarchar(MAX),
      CurrentUnitId              int,
      CurrentUnitName            nvarchar(MAX),
      CurrentUnitLocation        nvarchar(MAX)
    );

    SELECT @ids = @ids + CONVERT(nvarchar(10), UnitId) + ','
    FROM UsersUnitsRoles
    WHERE userid = @userId
    SET @ids = LEFT(@ids, LEN(@ids) - 1)

    INSERT INTO @patients
    EXEC [dbo].[fda_GetBandsInfoFiltered] 0, 100, @ids

    SELECT TOP 50
      e.Id,
      Type,
      Severity,
      DateUtc                   AS StartTime,
      EndDateUtc                AS EndTime,
      tp.ObjectDisplayName      AS PatientDisplayName,
      p.Id                      AS PatientId,
      u.[Name]                  AS UnitName,
      pu.[Name]                 AS ParentUnitName
    FROM dbo.ev_Events e
      INNER JOIN enum.ObjectType ot ON 1 = 1
      INNER JOIN dbo.Patients p ON e.ObjectId = p.id

      INNER JOIN dbo.ev_BandStatus bs ON p.Id = bs.ObjectId
      INNER JOIN @patients tp ON bs.BandId = tp.Id

      LEFT JOIN dbo.Units u ON e.UnitId = u.id
      LEFT JOIN dbo.Units pu ON u.ParentUnitID = pu.ID
    WHERE
      e.ObjectType = ot.Patient
      AND tp.ObjectTypeId = 4
      AND Type <> 18
      AND DateUtc > DATEADD(DD, -1 * @hoursBack, dbo.GetCurrentDate())
      --AND DateUtc > DATEADD(DD, -1 * 10, dbo.GetCurrentDate())
    ORDER BY
      e.Id DESC
	  */
  END
