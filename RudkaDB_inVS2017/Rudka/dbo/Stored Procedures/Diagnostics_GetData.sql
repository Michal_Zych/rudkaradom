﻿
CREATE  PROCEDURE [dbo].[Diagnostics_GetData]
	@sensorUnit int,
	@date_from datetime,
	@date_to datetime
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @clientID int = 1

	DECLARE @maxRows int
	SELECT @maxRows = MaxRowsRawData FROM dbo.ev_Settings

    select top(@maxRows)
		SensorID,
		MeasurementTime,
		Value * s.Scale AS Value
	from Measurements m
	join Sensors s ON s.Id = m.SensorID AND s.Unitid = @sensorUnit
	WHERE MeasurementTime between @date_from and @date_to
	order by MeasurementTime desc
    
END
