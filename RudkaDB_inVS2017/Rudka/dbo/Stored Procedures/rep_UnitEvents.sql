﻿-- zdarzenia w unicie
CREATE PROCEDURE [dbo].[rep_UnitEvents]
	@UserReportId int
AS
BEGIN
	DECLARE @REPORT_TYPE tinyint = 1 -- zdarzenia w unicie

	DECLARE
		@dateStart datetime
		,@dateEnd datetime
		,@unitId int		
		,@userId int

		,@currentDate dateTime = dbo.GetCurrentDate()
		,@currentUtcDate dateTime = dbo.GetCurrentUtcDate()
	
	SELECT @dateStart = DATEADD(MI, DATEDIFF(MI, @currentDate,@currentUtcDate), DateFrom) 
		,@dateEnd = DATEADD(MI, DATEDIFF(MI, @currentDate, @currentUtcDate), DateTo)
		,@unitId = UnitId
		,@userId = UserID
	FROM dbo.UsersReports WHERE Id = @UserReportId

	IF @dateStart IS NULL SET @dateStart = CONVERT(datetime, 1)
	IF @dateEnd IS NULL SET @dateEnd = DATEADD(YEAR, 1, @currentDate)

	DECLARE @unitIds TABLE(Id int, Name nvarchar(200))
	INSERT @unitIds(Id, Name)
	SELECT u.Id, uu.Name
	FROM dbo.GetChildUnits(@unitId) u
	JOIN dbo.Units_GetUserUnits_Func(@userId) uu ON u.Id = uu.Id


	SELECT * FROM (
		SELECT DateUtc
			,DATEADD(MI, DATEDIFF(MI, @currentUtcDate, @currentDate), e.DateUtc) AS Date
			,et.Description AS EventType
			,es.Description AS EventSeverity
			,e.ObjectId AS ObjectId
			,e.ObjectType AS ObjectType
			,ot.Description AS ObjectDescription
			, dbo.GetObjectName(e.ObjectId, e.ObjectType) AS ObjectName
			,e.UnitId AS UnitId
			,un.Name AS UnitName
			,COALESCE(dbo.GetUnitLocation(e.UnitId, 0, 0, default), 'nieznana') AS Location
			,COALESCE(usr.LastName, '') + ' ' + COALESCE(usr.Name, '') AS Operator
		FROM dbo.ev_Events e
			JOIN @unitIds un ON e.UnitId = un.Id
			JOIN dbo.ReportTypeEvents rte ON rte.TypeId = @REPORT_TYPE AND e.Type = rte.EventTypeId
			JOIN dbo.enum_ObjectTypeDictionaryPl ot ON ot.Value = e.ObjectType
			JOIN dbo.enum_EventTypeDictionaryPl et ON et.Value = e.Type
			JOIN dbo.enum_EventSeverityDictionaryPl es ON es.Value = e.Severity
			LEFT JOIN dbo.Users usr ON e.OperatorId = usr.ID
			WHERE DateUtc BETWEEN @dateStart AND @dateEnd
		UNION ALL
		SELECT DateUtc
			,DATEADD(MI, DATEDIFF(MI, @currentUtcDate, @currentDate), e.DateUtc) AS Date
			,et.Description AS EventType
			,es.Description AS EventSeverity
			,e.ObjectId AS ObjectId
			,e.ObjectType AS ObjectType
			,ot.Description AS ObjectDescription
			, dbo.GetObjectName(e.ObjectId, e.ObjectType) AS ObjectName
			,e.UnitId AS UnitId
			,un.Name AS UnitName
			,COALESCE(dbo.GetUnitLocation(e.UnitId, 0, 0, default), 'nieznana') AS Location
			,COALESCE(usr.LastName, '') + ' ' + COALESCE(usr.Name, '') AS Operator
		FROM arch.ev_Events e
			JOIN @unitIds un ON e.UnitId = un.Id
			JOIN dbo.ReportTypeEvents rte ON rte.TypeId = @REPORT_TYPE AND e.Type = rte.EventTypeId
			JOIN dbo.enum_ObjectTypeDictionaryPl ot ON ot.Value = e.ObjectType
			JOIN dbo.enum_EventTypeDictionaryPl et ON et.Value = e.Type
			JOIN dbo.enum_EventSeverityDictionaryPl es ON es.Value = e.Severity
			LEFT JOIN dbo.Users usr ON e.OperatorId = usr.ID
			WHERE DateUtc BETWEEN @dateStart AND @dateEnd
		) a
		ORDER BY Location, UnitName, DateUtc
END