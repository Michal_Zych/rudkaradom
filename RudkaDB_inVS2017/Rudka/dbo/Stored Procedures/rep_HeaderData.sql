﻿CREATE PROCEDURE [dbo].[rep_HeaderData]
(
	@UserReportID INT
)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @DATEFROM_TEMPLATE nvarchar(20) = '<DateFrom>'
		,@DATETO_TEMPLATE nvarchar(20)		= '<DateTo>'
		,@DEF_CHART_WIDTH varchar(10)		= '1200'
		,@DEF_CHART_HEIGHT varchar(10)		= '470'
	
	DECLARE
		@dateFrom datetime
		,@dateTo datetime
		,@userName nvarchar(200)
		,@chartAddress nvarchar(128)
		,@hideCharts bit
		,@canSeeAllAlarms bit
		,@title nvarchar(200)
		,@reportId int
		,@chartParams nvarchar (300)
		,@createdBy nvarchar(300)
		,@mono bit
		,@currentDate dateTime = dbo.GetCurrentDate()

	SELECT @title = r.Title
		, @chartAddress = a.Charts
		, @reportId = ReportId
		, @dateFrom = DateFrom
		, @dateTo = DateTo
		, @hideCharts = COALESCE(HideCharts, 0)
		, @userName = u.LastName + ' '+ u.Name
		,@canSeeAllAlarms = COALESCE(ur.HyperPermission, 0)
		,@mono = COALESCE(Mono, 0)
	FROM dbo.UsersReports ur WITH(NOLOCK)
	JOIN dbo.Reports r WITH(NOLOCK) ON ur.ReportID = r.ID
	JOIN dbo.Users u WITH(NOLOCK) ON ur.UserID = u.ID
	JOIN dbo.Applications a WITH(NOLOCK) ON a.id = 1
	WHERE ur.Id = @UserReportID


	
	SET @title = REPLACE(REPLACE(@title, @DATEFROM_TEMPLATE, CONVERT(nvarchar(19), @dateFrom, 120)), @DATETO_TEMPLATE, CONVERT(nvarchar(19), @dateTo, 120))

	SET @createdBy = 'Sporządził'
	IF SUBSTRING(@userName, LEN(@userName), 1) = 'A'	
		SET @createdBy = @createdBy + 'a'
	SET @createdBy = @createdBy + ' ' + @userName + ' dnia ' + CONVERT(nvarchar(10), @currentDate, 121) + ' godz. ' + SUBSTRING(CONVERT(nvarchar(20), @currentDate, 120), 12, 99)

	IF(@hideCharts = 1)
		SET @chartParams = 'no'
	ELSE
		set @chartParams = @chartAddress + 'DeviceChart.ashx?' 
				+ '&From=' + convert(varchar(30), @dateFrom, 120)
				+ '&To=' + convert(varchar(30), @dateTo, 120)
				+ '&Width=' + @DEF_CHART_WIDTH
				+ '&Height=' + @DEF_CHART_HEIGHT
				+ '&ShowAllAlarms=' + CONVERT(char(1), @canSeeAllAlarms)
				+ '&Mono=' + CONVERT(char(1), @mono)
				+ '&Sensors='
	SELECT @title AS Title
		,@createdBy AS CreatedBy
		,@chartParams AS Picture
		,@mono AS IsMono
		,@dateFrom AS DateFrom
		,@dateTo AS DateTo
		,@canSeeAllAlarms AS HyperPermission
END


