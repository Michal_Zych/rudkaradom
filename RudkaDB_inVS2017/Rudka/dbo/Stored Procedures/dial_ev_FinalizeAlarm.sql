﻿-- =============================================
-- Author:		kt
-- Create date: 2018-02-13
-- Description:	clears up alarms for dial service
-- =============================================
CREATE PROCEDURE [dbo].[dial_ev_FinalizeAlarm]
	@eventId int,
	@status int,
	@time datetime
AS
BEGIN

	BEGIN TRAN

	DELETE FROM dbo.ev_CallOrders WHERE EventId = @eventId;
	DELETE FROM dbo.ev_CallOrdersDetails WHERE EventId = @eventId;

	INSERT INTO dbo.ev_CallOrdersLog(EventId, Type, Status, Time) VALUES (@eventId, 2, @status, @time)

	COMMIT TRAN
	
END