﻿-- =============================================
-- Author:		kt
-- Create date: 2018-05-21
-- Description:	loads current location state of senses
-- =============================================
CREATE PROCEDURE dbo.dev_GetCurrentBandLocations
AS
BEGIN
	SET NOCOUNT ON;

	SELECT BandId, ut.TransmitterId, currentState.RSSI, currentState.MeasureDate, t.Mac AS TransmitterMac
	FROM ev_BandStatus bs
	INNER JOIN bands b on b.Id = bs.BandId 
	INNER JOIN dev_UnitTransmitter ut on ut.UnitID = bs.CurrentUnitId
	INNER JOIN dev_Transmitters t on ut.TransmitterId = t.Id
	INNER JOIN 
		(SELECT TOP 1 
			senseid, rssi, measuredate 
			FROM dev_TransmitterSenses 
			WHERE 
				MeasureDate > DATEADD(dd, -1, dbo.GetCurrentDate()) ORDER BY id DESC
		) currentState ON b.Mac = currentState.SenseId
	WHERE objecttype = 2 or objecttype = 4
	ORDER BY BandId

END