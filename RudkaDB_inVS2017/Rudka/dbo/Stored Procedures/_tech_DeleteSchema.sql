﻿CREATE PROCEDURE [dbo].[_tech_DeleteSchema]
	@schema varchar(20)
AS
BEGIN
IF @schema = 'dbo' OR @schema = 'ISBT'
	BEGIN
		RETURN
	END
ELSE
	BEGIN
		DECLARE @SqlStatement NVARCHAR(MAX)
		SELECT @SqlStatement = 
			COALESCE(@SqlStatement, '') + 'DROP TABLE [' + @schema + '].' + QUOTENAME(TABLE_NAME) + ';' + CHAR(13)
		FROM INFORMATION_SCHEMA.TABLES
		WHERE TABLE_SCHEMA = @schema

		EXEC sp_executesql @SqlStatement
	END
END

