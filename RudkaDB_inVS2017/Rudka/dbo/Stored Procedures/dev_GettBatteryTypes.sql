﻿CREATE PROCEDURE [dbo].[dev_GettBatteryTypes]
AS
BEGIN
	SELECT Id
      ,Name
      ,LifeMonths
      ,Capacity
      ,Voltage
      ,Description
  FROM [dbo].[BatteryTypes]
END
