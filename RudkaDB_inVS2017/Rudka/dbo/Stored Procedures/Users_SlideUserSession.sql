﻿CREATE  PROCEDURE [dbo].[Users_SlideUserSession]
	@user int
AS
BEGIN
	SET NOCOUNT ON;
        
    declare @last_ping int
    
    select top(1) @last_ping = Id from UsersLogs where UserID = @user order by Id desc
        
	update UsersLogs set LastPingDate = dbo.GetCurrentDate()
	where Id = @last_ping
	
	
	
	update Users set LastPingID = @last_ping 
	where ID = @user 
	AND LastPingID != @last_ping --zabezpieczenie przed bezsensownym podbijaniem timestampa
																
    
END

