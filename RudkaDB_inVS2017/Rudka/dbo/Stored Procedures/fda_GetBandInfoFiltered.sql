﻿CREATE PROCEDURE fda_GetBandInfoFiltered
@PageNum int
,@PageSize int

,@TreeUnitIds nvarchar(max),
@IdFrom smallint = null
,@IdTo smallint = null
,@BarCode nvarchar(100) = null
,@MacString nvarchar(100) = null
,@UidFrom bigint = null
,@UidTo bigint = null
,@BatteryTypeStr nvarchar(100) = null
,@BatteryAlarmSeverityIds nvarchar(max) = null
,@BatteryAlarmSeverity nvarchar(100) = null
,@BatteryInstallationDateFrom datetime = null
,@BatteryInstallationDateTo datetime = null
,@BatteryDaysToExchangeFrom int = null
,@BatteryDaysToExchangeTo int = null
,@Description nvarchar(100) = null
,@ObjectTypeIds nvarchar(max) = null
,@ObjectType nvarchar(100) = null
,@ObjectDisplayName nvarchar(100) = null
,@UnitIds nvarchar(max) = null
,@BandUnitName nvarchar(100) = null
,@BandUnitLocation nvarchar(100) = null
,@CurrentUnitIds nvarchar(max) = null
,@CurrentUnitName nvarchar(100) = null
,@CurrentUnitLocation nvarchar(100) = null
,@SortOrder nvarchar(100) = null
AS
BEGIN

 DECLARE @cmd nvarchar(max)
 SET @cmd = 'WITH orn AS ( SELECT ROW_NUMBER() OVER ('
		+ dbo.dsql_GetBandInfo_OrderBY(@SortOrder) + ') AS RowNum, '
		+ dbo.dsql_GetBandInfo_Select()
		+ dbo.dsql_GetBandInfo_From()
		+ dbo.dsql_GetBandInfo_Where(@TreeUnitIds,
			@IdFrom, @IdTo, @BarCode, @MacString, @UidFrom, @UidTo
			, @BatteryTypeStr, @BatteryAlarmSeverityIds, @BatteryAlarmSeverity, @BatteryInstallationDateFrom, @BatteryInstallationDateTo, @BatteryDaysToExchangeFrom
			, @BatteryDaysToExchangeTo, @Description, @ObjectTypeIds, @ObjectType, @ObjectDisplayName, @UnitIds
			, @BandUnitName, @BandUnitLocation, @CurrentUnitIds, @CurrentUnitName, @CurrentUnitLocation)
 + ')
		SELECT *
		FROM orn
		WHERE RowNum BETWEEN ' + CONVERT(nvarchar(20), @PageNum * @PageSize +1) + ' AND '
		+ CONVERT(nvarchar(20), (@PageNum + 1) * @PageSize) + ' ORDER BY rowNum'

 EXEC(@cmd)
-- print @cmd
END