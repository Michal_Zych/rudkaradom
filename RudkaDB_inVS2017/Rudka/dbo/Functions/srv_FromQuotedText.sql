﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[srv_FromQuotedText]
(
	@text nvarchar(max)
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @result nvarchar(max)
	IF SUBSTRING(@text, 1, 1) = ''''
	BEGIN
		SET @result = REPLACE(@text, '&quot;', '''')
		RETURN SUBSTRING(@result, 2, LEN(@result) - 2)
	END
	RETURN @text
END

