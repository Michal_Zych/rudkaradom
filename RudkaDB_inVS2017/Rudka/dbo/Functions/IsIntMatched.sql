﻿CREATE FUNCTION [dbo].IsIntMatched
(
	@Value int
	,@Pattern nvarchar(max) 
)
RETURNS int
AS
BEGIN
	DECLARE @results int = 0
		,@str nvarchar(10) = CONVERT(nvarchar(10), @Value)
	
	SELECT @results = @results + CASE WHEN @str = LTRIM(RTRIM(Value)) THEN 1 ELSE 0 END
	FROM dbo.fn_Split(@Pattern, '|')

	RETURN @results
END