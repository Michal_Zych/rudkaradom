﻿CREATE FUNCTION [dbo].[GetChildUnits]
( 
	@id INT
)
RETURNS TABLE 
AS
RETURN 
(
	WITH SimpleRecursive(Id,Name,IdParent)
	AS
	(
		SELECT Id,Name,ParentUnitID
		FROM Units
		WHERE Id = @id
		UNION ALL
		SELECT J.Id,J.Name,J.ParentUnitID
		FROM Units J
			INNER JOIN SimpleRecursive S ON S.Id = J.ParentUnitId
	)
	SELECT Id FROM SimpleRecursive
)

