﻿CREATE FUNCTION [dbo].[fnGetStringToSha256]
(@Str NVARCHAR (1000))
RETURNS VARBINARY (8000)
AS
 EXTERNAL NAME [InnoDll].[fnEnCryptSHA].[GetStringToSha256]

