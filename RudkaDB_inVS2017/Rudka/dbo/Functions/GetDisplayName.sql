﻿CREATE FUNCTION dbo.GetDisplayName
(
	@ObjectId dbo.OBJECT_ID
	,@ObjectType dbo.OBJECT_TYPE
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @displayName nvarchar(max)
	IF @ObjectType = (SELECT Patient FROM enum.ObjectType)
		SELECT @displayName = COALESCE(Name + ' ', '') + COALESCE(LEFT(LastName, 1) + '.', '')
		FROM dbo.Patients
		WHERE Id = @ObjectId
	ELSE IF @ObjectType = (SELECT Band FROM enum.ObjectType)
		SELECT @displayName = MacString
		FROM dbo.Bands
		WHERE Id = @ObjectId
	ELSE
		SELECT @displayName = CONVERT(nvarchar(10), @ObjectId)


	RETURN @displayName
END