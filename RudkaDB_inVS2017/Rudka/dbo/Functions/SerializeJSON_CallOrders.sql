﻿CREATE FUNCTION [dbo].[SerializeJSON_CallOrders]
(
	@EventId int
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @JSON nvarchar(max)

	SET @JSON = 'SELECT EventId, DateUtc, Message, ''$PhoneNrs$'' AS PhoneNrs FROM dbo.ev_CallOrders WHERE EventId = ' + CONVERT(nvarchar(10), @EventId)
	EXEC @JSON = dbo.SerializeJSON @JSON

	DECLARE @PhoneNrs nvarchar(max)
	SET @PhoneNrs = 'SELECT PhoneNr FROM dbo.ev_CallOrdersDetails WHERE EventId = ' + CONVERT(nvarchar(10), @EventId) + ' ORDER BY IsNrFromEventUnit DESC, SequenceNr'
	EXEC @PhoneNrs = dbo.SerializeJSON @PhoneNrs
	SET @JSON = REPLACE(@JSON, '"$PhoneNrs$"', @PhoneNrs)

	RETURN @JSON
END