﻿-- =============================================
-- Author:		pi	
-- Create date: 21-09-2012
-- Description:	Parse prep full type  
-- =============================================
CREATE FUNCTION [dbo].[ParseFullType] --fullType =  (fullType != null && fullType.Contains("|")) ? fullType.Substring(0, fullType.IndexOf("|")) : String.Empty;
(
	@FullType nvarchar(400)
)
RETURNS nvarchar(400)
AS
BEGIN
	
	DECLARE @type nvarchar(400)
	SET @type = ''
	
	IF 
		@FullType IS NOT NULL 
		AND 
		@FullType like '%|%'
	BEGIN
		DECLARE @idx int
		SET @idx = CHARINDEX('|', @FullType)
		
		SET @type = SUBSTRING(@FullType, 0, @idx)
	END	
	--**RS**--
	ELSE RETURN NULL
	
	RETURN @type

END

