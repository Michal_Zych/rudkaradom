﻿CREATE FUNCTION dsql_GetPatientDetails_Select
()
RETURNS nvarchar(max)
AS
BEGIN
 RETURN
'p.Id as Id
,p.Name as Name
,p.LastName as LastName
,dbo.GetDisplayName(p.Id, objectType.Patient) as DisplayName
,p.Pesel as Pesel
,p.WardId as WardId
,pu.Name as WardUnitName
,dbo.GetUnitLocation(p.WardId, default, default, default) as WardUnitLocation
,p.WardDateUtc as WardAdmissionDateUtc
,p.RoomId as RoomId
,pr.Name as RoomUnitName
,dbo.GetUnitLocation(p.RoomId, default, default, default) as RoomUnitLocation
,p.RoomDateUtc as RoomAdmissionDateUtc
,b.Id as BandId
,b.BarCode as BandBarCode
,b.SerialNumber as BandSerialNumber
,cu.Name as CurrentUnitName
,dbo.GetUnitLocation(cu.ID, default, default, default) as CurrentUnitLocation
'
END