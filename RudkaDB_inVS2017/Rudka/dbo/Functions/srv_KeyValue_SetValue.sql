﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[srv_KeyValue_SetValue]
(
	@text nvarchar(max)
	,@key nvarchar(50)
	,@param nvarchar(50)
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @result nvarchar(max)
	SET @result = COALESCE(@text, '')
	
	DECLARE @keyStart int
		,@valueStart int
		,@valueLen int
	SET @keyStart = PATINDEX('%' + @key + '=%', @result COLLATE SQL_Latin1_General_Cp1_CS_AS)
	
	SET @param = dbo.srv_ToQuotedText(@param)
	
	IF @keyStart = 0
	BEGIN
		IF @result <> '' SET @result = @result + ';'
		SET @result = @result + @key + '=' + COALESCE(@param, 'NULL')
	END
	ELSE
	BEGIN
		SET @valueStart = @keyStart + LEN(@key) + 1
		SET @valueLen = PATINDEX('%;%', SUBSTRING(@result, @valueStart, 999)) - 1
		IF @valueLen = -1 SET @valueLen = 999
		SET @result = STUFF(@result, @valueStart, @valueLen, COALESCE(@param, 'NULL'))
	END
	
	RETURN @result
END

