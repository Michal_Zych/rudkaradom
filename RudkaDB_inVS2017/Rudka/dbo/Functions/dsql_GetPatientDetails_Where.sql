﻿CREATE FUNCTION [dbo].[dsql_GetPatientDetails_Where]
(
@TreeUnitIds nvarchar(max),
@IdFrom int = null
,@IdTo int = null
,@Name nvarchar(200) = null
,@LastName nvarchar(200) = null
,@DisplayName nvarchar(200) = null
,@Pesel nvarchar(200) = null
,@WardIdFrom int = null
,@WardIdTo int = null
,@WardUnitName nvarchar(200) = null
,@WardUnitLocation nvarchar(200) = null
,@WardAdmissionDateUtcFrom datetime = null
,@WardAdmissionDateUtcTo datetime = null
,@RoomIdFrom int = null
,@RoomIdTo int = null
,@RoomUnitName nvarchar(200) = null
,@RoomUnitLocation nvarchar(200) = null
,@RoomAdmissionDateUtcFrom datetime = null
,@RoomAdmissionDateUtcTo datetime = null
,@BandIdFrom int = null
,@BandIdTo int = null
,@BandBarCode nvarchar(200) = null
,@BandSerialNumber nvarchar(200) = null
,@CurrentUnitName nvarchar(200) = null
,@CurrentUnitLocation nvarchar(200) = null
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @useMatchFunction bit = 1
 DECLARE @r nvarchar(max) = ' WHERE '
 IF COALESCE(@TreeUnitIds, '') <> ''
   SET @r = @r + '(p.WardId IN(' + @TreeUnitIds + ') OR p.RoomId IN(' + @TreeUnitIds + ')) AND '

 IF @IdFrom IS NOT NULL AND @IdTo IS NOT NULL
  SET @r = @r + '(p.Id  BETWEEN ' + CONVERT(nvarchar(20), @IdFrom) + ' AND ' + CONVERT(nvarchar(20), @IdTo) + ') AND '
 ELSE IF @IdFrom IS NOT NULL AND @IdTo IS NULL
  SET @r = @r + 'p.Id  = ' + CONVERT(nvarchar(20), @IdFrom) + ' AND '
 ELSE IF @IdFrom IS NULL AND @IdTo IS NOT NULL
  SET @r = @r + 'p.Id  IS NULL AND '

 IF @Name IS NOT NULL
  IF @Name = '?'
   SET @r = @r + 'COALESCE(p.Name , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(p.Name , ''' + @Name + ''') > 0 AND '
    ELSE
     SET @r = @r + 'p.Name  LIKE ''' + @Name + ''' AND '
  END

 IF @LastName IS NOT NULL
  IF @LastName = '?'
   SET @r = @r + 'COALESCE(p.LastName , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(p.LastName , ''' + @LastName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'p.LastName  LIKE ''' + @LastName + ''' AND '
  END

 IF @DisplayName IS NOT NULL
  IF @DisplayName = '?'
   SET @r = @r + 'COALESCE(dbo.GetDisplayName(p.Id, objectType.Patient) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(dbo.GetDisplayName(p.Id, objectType.Patient) , ''' + @DisplayName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'dbo.GetDisplayName(p.Id, objectType.Patient)  LIKE ''' + @DisplayName + ''' AND '
  END

 IF @Pesel IS NOT NULL
  IF @Pesel = '?'
   SET @r = @r + 'COALESCE(p.Pesel , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(p.Pesel , ''' + @Pesel + ''') > 0 AND '
    ELSE
     SET @r = @r + 'p.Pesel  LIKE ''' + @Pesel + ''' AND '
  END

 IF @WardIdFrom IS NOT NULL AND @WardIdTo IS NOT NULL
  SET @r = @r + '(p.WardId  BETWEEN ' + CONVERT(nvarchar(20), @WardIdFrom) + ' AND ' + CONVERT(nvarchar(20), @WardIdTo) + ') AND '
 ELSE IF @WardIdFrom IS NOT NULL AND @WardIdTo IS NULL
  SET @r = @r + 'p.WardId  = ' + CONVERT(nvarchar(20), @WardIdFrom) + ' AND '
 ELSE IF @WardIdFrom IS NULL AND @WardIdTo IS NOT NULL
  SET @r = @r + 'p.WardId  IS NULL AND '

 IF @WardUnitName IS NOT NULL
  IF @WardUnitName = '?'
   SET @r = @r + 'COALESCE(pu.Name , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(pu.Name , ''' + @WardUnitName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'pu.Name  LIKE ''' + @WardUnitName + ''' AND '
  END

 IF @WardUnitLocation IS NOT NULL
  IF @WardUnitLocation = '?'
   SET @r = @r + 'COALESCE(dbo.GetUnitLocation(p.WardId, default, default, default) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(dbo.GetUnitLocation(p.WardId, default, default, default) , ''' + @WardUnitLocation + ''') > 0 AND '
    ELSE
     SET @r = @r + 'dbo.GetUnitLocation(p.WardId, default, default, default)  LIKE ''' + @WardUnitLocation + ''' AND '
  END

 IF @WardAdmissionDateUtcFrom IS NOT NULL
  SET @r = @r + 'p.WardDateUtc  >=''' + CONVERT(nvarchar(30), @WardAdmissionDateUtcFrom, 121) + ''' AND '
 IF @WardAdmissionDateUtcTo IS NOT NULL
  SET @r = @r + 'p.WardDateUtc  <=''' + CONVERT(nvarchar(30), @WardAdmissionDateUtcTo, 121) + ''' AND '

 IF @RoomIdFrom IS NOT NULL AND @RoomIdTo IS NOT NULL
  SET @r = @r + '(p.RoomId  BETWEEN ' + CONVERT(nvarchar(20), @RoomIdFrom) + ' AND ' + CONVERT(nvarchar(20), @RoomIdTo) + ') AND '
 ELSE IF @RoomIdFrom IS NOT NULL AND @RoomIdTo IS NULL
  SET @r = @r + 'p.RoomId  = ' + CONVERT(nvarchar(20), @RoomIdFrom) + ' AND '
 ELSE IF @RoomIdFrom IS NULL AND @RoomIdTo IS NOT NULL
  SET @r = @r + 'p.RoomId  IS NULL AND '

 IF @RoomUnitName IS NOT NULL
  IF @RoomUnitName = '?'
   SET @r = @r + 'COALESCE(pr.Name , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(pr.Name , ''' + @RoomUnitName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'pr.Name  LIKE ''' + @RoomUnitName + ''' AND '
  END

 IF @RoomUnitLocation IS NOT NULL
  IF @RoomUnitLocation = '?'
   SET @r = @r + 'COALESCE(dbo.GetUnitLocation(p.RoomId, default, default, default) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(dbo.GetUnitLocation(p.RoomId, default, default, default) , ''' + @RoomUnitLocation + ''') > 0 AND '
    ELSE
     SET @r = @r + 'dbo.GetUnitLocation(p.RoomId, default, default, default)  LIKE ''' + @RoomUnitLocation + ''' AND '
  END

 IF @RoomAdmissionDateUtcFrom IS NOT NULL
  SET @r = @r + 'p.RoomDateUtc  >=''' + CONVERT(nvarchar(30), @RoomAdmissionDateUtcFrom, 121) + ''' AND '
 IF @RoomAdmissionDateUtcTo IS NOT NULL
  SET @r = @r + 'p.RoomDateUtc  <=''' + CONVERT(nvarchar(30), @RoomAdmissionDateUtcTo, 121) + ''' AND '

 IF @BandIdFrom IS NOT NULL AND @BandIdTo IS NOT NULL
  SET @r = @r + '(b.Id  BETWEEN ' + CONVERT(nvarchar(20), @BandIdFrom) + ' AND ' + CONVERT(nvarchar(20), @BandIdTo) + ') AND '
 ELSE IF @BandIdFrom IS NOT NULL AND @BandIdTo IS NULL
  SET @r = @r + 'b.Id  = ' + CONVERT(nvarchar(20), @BandIdFrom) + ' AND '
 ELSE IF @BandIdFrom IS NULL AND @BandIdTo IS NOT NULL
  SET @r = @r + 'b.Id  IS NULL AND '

 IF @BandBarCode IS NOT NULL
  IF @BandBarCode = '?'
   SET @r = @r + 'COALESCE(b.BarCode , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(b.BarCode , ''' + @BandBarCode + ''') > 0 AND '
    ELSE
     SET @r = @r + 'b.BarCode  LIKE ''' + @BandBarCode + ''' AND '
  END

 IF @BandSerialNumber IS NOT NULL
  IF @BandSerialNumber = '?'
   SET @r = @r + 'COALESCE(b.SerialNumber , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(b.SerialNumber , ''' + @BandSerialNumber + ''') > 0 AND '
    ELSE
     SET @r = @r + 'b.SerialNumber  LIKE ''' + @BandSerialNumber + ''' AND '
  END

 IF @CurrentUnitName IS NOT NULL
  IF @CurrentUnitName = '?'
   SET @r = @r + 'COALESCE(cu.Name , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(cu.Name , ''' + @CurrentUnitName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'cu.Name  LIKE ''' + @CurrentUnitName + ''' AND '
  END

 IF @CurrentUnitLocation IS NOT NULL
  IF @CurrentUnitLocation = '?'
   SET @r = @r + 'COALESCE(dbo.GetUnitLocation(cu.ID, default, default, default) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(dbo.GetUnitLocation(cu.ID, default, default, default) , ''' + @CurrentUnitLocation + ''') > 0 AND '
    ELSE
     SET @r = @r + 'dbo.GetUnitLocation(cu.ID, default, default, default)  LIKE ''' + @CurrentUnitLocation + ''' AND '
  END

 SET @r = @r + ' 1=1'
 RETURN @r
END