﻿


CREATE FUNCTION [dbo].[settings_GetInt] (
	@ClientId int
	,@Name nvarchar(50)
) 
RETURNS int 
AS
BEGIN
	
	DECLARE @ERROR_VALUE int = NULL
	
	DECLARE @defValue int
	DECLARE @value nvarchar(1024)

	SET @defValue = CASE @Name
		WHEN 'MaxPasswordAgeDays'			THEN 30
		WHEN 'ShowReasonField'				THEN 1
		WHEN 'PingSessionIntervalSecs'		THEN 20
		WHEN 'MonitoringSoundIntervalSecs'	THEN 30
		WHEN 'RangeColorIntensityPercent'	THEN 50
		WHEN 'MaxRwosLogs'					THEN 500
		WHEN 'MaxRwosMessages'				THEN 200
		WHEN 'DefMessageActivityMinutes'	THEN 30
		WHEN 'MaxRowsAlarmsPopup'			THEN 200
		WHEN 'MaxRowsAlarms'				THEN 200
		WHEN 'MaxRowsRawData'				THEN 1000
		WHEN 'RoleDefForNewUser'			THEN 3
		WHEN 'MaxDaysReports'				THEN 33
		WHEN 'MaxDaysRawData'				THEN 5
		WHEN 'SearchMessagesDaysBefore'		THEN 2
		WHEN 'SearchMessagesDaysAfter'		THEN 1
		WHEN 'SearchLogsDaysBefore'			THEN 2
		WHEN 'SearchLogsDaysAfter'			THEN 1
		WHEN 'SearchRawDataDaysBefore'		THEN 1
		WHEN 'SearchRawDataDaysAfter'		THEN 1
		WHEN 'SearchAlarmsDaysBefore'		THEN 2
		WHEN 'SearchAlarmsDaysAfter'		THEN 1
		WHEN 'SearchChartDataHoursBefore'	THEN 24
		WHEN 'SearchChartDataHoursAfter'	THEN 24
		WHEN 'ChartImageWidth'				THEN 800
		WHEN 'ChartImageHeight'				THEN 600
		WHEN 'ShowUnitId'					THEN 1
		WHEN 'EnableMonitoringStatusButton' THEN 0
		WHEN 'OldDataTimeMinutes'			THEN 10
		ELSE @ERROR_VALUE
	END

	SELECT @value = Value
	FROM dbo.Settings WHERE ClientID = @ClientId AND Name = @Name

	IF ISNUMERIC(@Value + '.e0') = 1		
		SET @defValue = CONVERT(int, @value) 
	
	RETURN @defValue
END

