﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[srv_KeyValue_GetValue]
(
	@text nvarchar(max)
	,@param nvarchar(50)
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @result nvarchar(max)
	
	DECLARE @keyStart int
		,@valueStart int
		,@valueEnd int
	SET @keyStart = PATINDEX('%' + @param + '=%', @text COLLATE SQL_Latin1_General_Cp1_CS_AS)
	
	IF @keyStart = 0 RETURN NULL
	
	SET @valueStart = @keyStart + LEN(@param) + 1
	
	IF SUBSTRING(@text, @valueStart, 1) = ''''
		SET @valueEnd = PATINDEX('%''%', SUBSTRING(@text, @valueStart + 1, 999)) +1
	ELSE
		SET @valueEnd = PATINDEX('%;%', SUBSTRING(@text, @valueStart, 999)) - 1
	IF @valueEnd = -1 SET @valueEnd = 999
	SET @result = dbo.srv_FromQuotedText(SUBSTRING(@text,@valueStart, @valueEnd))
	--SET @result = SUBSTRING(@text, @valueStart, @valueEnd)
	RETURN @result
END

