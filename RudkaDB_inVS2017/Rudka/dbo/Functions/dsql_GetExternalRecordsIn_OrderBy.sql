﻿CREATE FUNCTION dsql_GetExternalRecordsIn_OrderBy
(
 @SortOrder nvarchar(100)
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @result nvarchar(max)
 SET @result = ' ORDER BY ' +
  CASE @SortOrder
   WHEN '+Type' THEN 'Type  ASC'
   WHEN '-Type' THEN 'Type  DESC'
   WHEN '+Id' THEN 'Id  ASC'
   WHEN '-Id' THEN 'Id  DESC'
   WHEN '+RequestId' THEN 'RequestId  ASC'
   WHEN '-RequestId' THEN 'RequestId  DESC'
   WHEN '+StoreDateUtc' THEN 'StoreDateUtc  ASC'
   WHEN '-StoreDateUtc' THEN 'StoreDateUtc  DESC'
   WHEN '+RecordType' THEN 'RecordType  ASC'
   WHEN '-RecordType' THEN 'RecordType  DESC'
   WHEN '+RecordId' THEN 'RecordId  ASC'
   WHEN '-RecordId' THEN 'RecordId  DESC'
   WHEN '+Record' THEN 'Record  ASC'
   WHEN '-Record' THEN 'Record  DESC'
   WHEN '+Processed' THEN 'Processed  ASC'
   WHEN '-Processed' THEN 'Processed  DESC'
   WHEN '+Try' THEN 'Try  ASC'
   WHEN '-Try' THEN 'Try  DESC'
   WHEN '+ProcessedDateUtc' THEN 'ProcessedDateUtc  ASC'
   WHEN '-ProcessedDateUtc' THEN 'ProcessedDateUtc  DESC'
   WHEN '+Error' THEN 'Error  ASC'
   WHEN '-Error' THEN 'Error  DESC'
   ELSE 'Type '
  END
 RETURN @result + ' '
END