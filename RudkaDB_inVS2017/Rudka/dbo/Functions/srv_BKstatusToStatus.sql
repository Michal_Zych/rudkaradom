﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[srv_BKstatusToStatus]
(
	@Id char(1)
)
RETURNS int
AS
BEGIN
	DECLARE @result int
	
	SET @result = CASE @id
					WHEN 'R' THEN 5		-- wydanie na zewnątrz
					WHEN 'P' THEN 6		-- przetworzony
					WHEN 'Z' THEN 7		-- zniszczony
					ELSE 2				-- nie obsługują innych statusów
				  END
	
	RETURN @result	
END

