﻿CREATE FUNCTION [dbo].[dsql_GetSystemEvent_From]
(
	@FromArchive bit = 0
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @statement nvarchar(max)
	SELECT @statement =  ' FROM dbo.ev_Events e
	JOIN dbo.enum_EventTypeDictionaryPl eventType ON e.Type = eventType.Value
	JOIN dbo.enum_ObjectTypeDictionaryPl objectType ON e.ObjectType = objectType.Value
	LEFT JOIN dbo.Users AS usr ON e.OperatorId = usr.ID
	JOIN dbo.ev_SystemEventTypes sev ON sev.EventTypeId = e.Type
 '
 IF @FromArchive = 1
	SELECT @statement = REPLACE(@statement, 'dbo.ev_Events', 'arch.ev_Events')

 RETURN @statement

END