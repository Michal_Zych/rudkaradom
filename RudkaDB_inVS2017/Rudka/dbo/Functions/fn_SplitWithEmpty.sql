﻿
CREATE FUNCTION fn_SplitWithEmpty(	
	@text nvarchar(max) 
	, @delimiter char(1) = ' '
)
RETURNS @Strings TABLE ( 
   id int,
   value nvarchar(max) 
   ) 
AS 
BEGIN 
	DECLARE 
		@index int = -1
		,@id int = 0

		-- len changed to datalength because i didn't count trailing spaces
       WHILE (DATALENGTH(@text)/2 > 0) 
       BEGIN 
           SET @index = CHARINDEX(@delimiter , @text) 
           IF (@index = 0) AND (DATALENGTH(@text) / 2 > 0) 
           BEGIN 
               INSERT INTO @Strings VALUES (@id, @text) 
               BREAK 
           END 
           IF (@index >= 1) 
           BEGIN 
               INSERT INTO @Strings VALUES (@id, LEFT(@text, @index - 1)) 
			   SET @id = @id + 1
               SET @text = RIGHT(@text, (DATALENGTH(@text) / 2 - @index)) 
           END 
           ELSE 
               SET @text = RIGHT(@text, (DATALENGTH(@text) / 2 - @index)) 
       END 

	   RETURN
END