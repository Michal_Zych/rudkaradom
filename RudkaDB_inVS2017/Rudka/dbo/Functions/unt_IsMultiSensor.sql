﻿


CREATE FUNCTION [dbo].[unt_IsMultiSensor]
(
  @UnitID int
)
RETURNS bit
AS
BEGIN
	DECLARE @result bit

	SELECT @result = 
		CASE WHEN EXISTS (SELECT 1 
							FROM  dbo.Units u WITH(NOLOCK) JOIN dbo.UnitTypesV2 t WITH(NOLOCK) ON u.UnitTypeIdv2 = t.ID 
							WHERE (u.ParentUnitID = @UnitID) AND t.IsSensor = 1
			) THEN 1
			ELSE 0 
		END

	RETURN @result
END



