﻿CREATE FUNCTION dsql_GetExternalUnitMap_Select
()
RETURNS nvarchar(max)
AS
BEGIN
 RETURN
'eu.ExtId AS ExtId
,eu.ExtDescription AS ExtDescription
,eu.M2mId AS M2mId
,dbo.GetUnitLocation(eu.M2mId, 1, 1, default) AS M2mDescription
'
END