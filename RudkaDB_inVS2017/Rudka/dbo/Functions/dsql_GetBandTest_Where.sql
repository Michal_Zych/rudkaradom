﻿CREATE FUNCTION dsql_GetBandTest_Where
(
@TreeUnitIds nvarchar(max),
@IdFrom smallint = null
,@IdTo smallint = null
,@BarCode nvarchar(100) = null
,@MacString nvarchar(100) = null
,@UidFrom bigint = null
,@UidTo bigint = null
,@BatteryTypeStr nvarchar(100) = null
,@BatteryAlarmSeverityIds nvarchar(max) = null
,@BatteryAlarmSeverity nvarchar(100) = null
,@BatteryInstallationDateFrom datetime = null
,@BatteryInstallationDateTo datetime = null
,@BatteryDaysToExchangeFrom int = null
,@BatteryDaysToExchangeTo int = null
,@Description nvarchar(100) = null
,@ObjectTypeIds nvarchar(max) = null
,@ObjectType nvarchar(100) = null
,@ObjectDisplayName nvarchar(100) = null
,@ObjectUnitIds nvarchar(max) = null
,@ObjectUnitName nvarchar(100) = null
,@ObjectUnitLocation nvarchar(100) = null
,@CurrentUnitIds nvarchar(max) = null
,@CurrentUnitName nvarchar(100) = null
,@CurrentUnitLocation nvarchar(100) = null
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @useMatchFunction bit = 0
 DECLARE @r nvarchar(max) = ' WHERE ' + 'b.Id > 0 AND '

-- IF COALESCE(@TreeUnitIds, '') <> ''
--   SET @r = @r + 'UnitId IN(''' + @TreeUnitIds + ''') AND '

 IF @IdFrom IS NOT NULL AND @IdTo IS NOT NULL
  SET @r = @r + '(b.Id  BETWEEN ' + CONVERT(nvarchar(20), @IdFrom) + ' AND ' + CONVERT(nvarchar(20), @IdTo) + ') AND '
 ELSE IF @IdFrom IS NOT NULL AND @IdTo IS NULL
  SET @r = @r + 'b.Id  = ' + CONVERT(nvarchar(20), @IdFrom) + ' AND '
 ELSE IF @IdFrom IS NOT NULL AND @IdTo IS NOT NULL
  SET @r = @r + 'b.Id  IS NULL AND '

 IF @BarCode IS NOT NULL
  IF @BarCode = '?'
   SET @r = @r + 'COALESCE(b.BarCode , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(b.BarCode , ''' + @BarCode + ''') > 0 AND '
    ELSE
     SET @r = @r + 'b.BarCode  LIKE ''' + @BarCode + ''' AND '
  END

 IF @MacString IS NOT NULL
  IF @MacString = '?'
   SET @r = @r + 'COALESCE(b.MacString , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(b.MacString , ''' + @MacString + ''') > 0 AND '
    ELSE
     SET @r = @r + 'b.MacString  LIKE ''' + @MacString + ''' AND '
  END

 IF @UidFrom IS NOT NULL AND @UidTo IS NOT NULL
  SET @r = @r + '(b.Uid  BETWEEN ' + CONVERT(nvarchar(20), @UidFrom) + ' AND ' + CONVERT(nvarchar(20), @UidTo) + ') AND '
 ELSE IF @UidFrom IS NOT NULL AND @UidTo IS NULL
  SET @r = @r + 'b.Uid  = ' + CONVERT(nvarchar(20), @UidFrom) + ' AND '
 ELSE IF @UidFrom IS NOT NULL AND @UidTo IS NOT NULL
  SET @r = @r + 'b.Uid  IS NULL AND '

 IF @BatteryTypeStr IS NOT NULL
  IF @BatteryTypeStr = '?'
   SET @r = @r + 'COALESCE(bt.Name , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(bt.Name , ''' + @BatteryTypeStr + ''') > 0 AND '
    ELSE
     SET @r = @r + 'bt.Name  LIKE ''' + @BatteryTypeStr + ''' AND '
  END

 IF @BatteryAlarmSeverityIds IS NOT NULL AND @BatteryAlarmSeverityIds <> ''
   SET @r = @r + 'COALESCE(e.Severity, CASE WHEN bs.OngLowBatteryEventId IS NOT NULL THEN 1 ELSE NULL END)  IN(' + @BatteryAlarmSeverityIds + ') AND '

 IF @BatteryAlarmSeverity IS NOT NULL
  IF @BatteryAlarmSeverity = '?'
   SET @r = @r + 'COALESCE(es.Description , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(es.Description , ''' + @BatteryAlarmSeverity + ''') > 0 AND '
    ELSE
     SET @r = @r + 'es.Description  LIKE ''' + @BatteryAlarmSeverity + ''' AND '
  END

 IF @BatteryInstallationDateFrom IS NOT NULL
  SET @r = @r + 'b.BatteryInstallationDateUtc  >=''' + CONVERT(nvarchar(30), @BatteryInstallationDateFrom, 121) + ''' AND '
 IF @BatteryInstallationDateTo IS NOT NULL
  SET @r = @r + 'b.BatteryInstallationDateUtc  <=''' + CONVERT(nvarchar(30), @BatteryInstallationDateTo, 121) + ''' AND '

 IF @BatteryDaysToExchangeFrom IS NOT NULL AND @BatteryDaysToExchangeTo IS NOT NULL
  SET @r = @r + '(DATEDIFF(DAY, dbo.GetCurrentUtcDate(), DATEADD(MONTH, bt.LifeMonths, b.BatteryInstallationDateUtc))  BETWEEN ' + CONVERT(nvarchar(20), @BatteryDaysToExchangeFrom) + ' AND ' + CONVERT(nvarchar(20), @BatteryDaysToExchangeTo) + ') AND '
 ELSE IF @BatteryDaysToExchangeFrom IS NOT NULL AND @BatteryDaysToExchangeTo IS NULL
  SET @r = @r + 'DATEDIFF(DAY, dbo.GetCurrentUtcDate(), DATEADD(MONTH, bt.LifeMonths, b.BatteryInstallationDateUtc))  = ' + CONVERT(nvarchar(20), @BatteryDaysToExchangeFrom) + ' AND '
 ELSE IF @BatteryDaysToExchangeFrom IS NOT NULL AND @BatteryDaysToExchangeTo IS NOT NULL
  SET @r = @r + 'DATEDIFF(DAY, dbo.GetCurrentUtcDate(), DATEADD(MONTH, bt.LifeMonths, b.BatteryInstallationDateUtc))  IS NULL AND '

 IF @Description IS NOT NULL
  IF @Description = '?'
   SET @r = @r + 'COALESCE(b.Description , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(b.Description , ''' + @Description + ''') > 0 AND '
    ELSE
     SET @r = @r + 'b.Description  LIKE ''' + @Description + ''' AND '
  END

 IF @ObjectTypeIds IS NOT NULL AND @ObjectTypeIds <> ''
   SET @r = @r + 'bs.ObjectType  IN(' + @ObjectTypeIds + ') AND '

 IF @ObjectType IS NOT NULL
  IF @ObjectType = '?'
   SET @r = @r + 'COALESCE(ot.Description , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(ot.Description , ''' + @ObjectType + ''') > 0 AND '
    ELSE
     SET @r = @r + 'ot.Description  LIKE ''' + @ObjectType + ''' AND '
  END

 IF @ObjectDisplayName IS NOT NULL
  IF @ObjectDisplayName = '?'
   SET @r = @r + 'COALESCE(dbo.GetDisplayName(bs.ObjectId, bs.ObjectType) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(dbo.GetDisplayName(bs.ObjectId, bs.ObjectType) , ''' + @ObjectDisplayName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'dbo.GetDisplayName(bs.ObjectId, bs.ObjectType)  LIKE ''' + @ObjectDisplayName + ''' AND '
  END

 IF @ObjectUnitIds IS NOT NULL AND @ObjectUnitIds <> ''
   SET @r = @r + 'ou.ID  IN(' + @ObjectUnitIds + ') AND '

 IF @ObjectUnitName IS NOT NULL
  IF @ObjectUnitName = '?'
   SET @r = @r + 'COALESCE(ou.Name , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(ou.Name , ''' + @ObjectUnitName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'ou.Name  LIKE ''' + @ObjectUnitName + ''' AND '
  END

 IF @ObjectUnitLocation IS NOT NULL
  IF @ObjectUnitLocation = '?'
   SET @r = @r + 'COALESCE(dbo.GetUnitLocation(ou.Id, default, default, default) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(dbo.GetUnitLocation(ou.Id, default, default, default) , ''' + @ObjectUnitLocation + ''') > 0 AND '
    ELSE
     SET @r = @r + 'dbo.GetUnitLocation(ou.Id, default, default, default)  LIKE ''' + @ObjectUnitLocation + ''' AND '
  END

 IF @CurrentUnitIds IS NOT NULL AND @CurrentUnitIds <> ''
   SET @r = @r + 'cu.ID  IN(' + @CurrentUnitIds + ') AND '

 IF @CurrentUnitName IS NOT NULL
  IF @CurrentUnitName = '?'
   SET @r = @r + 'COALESCE(cu.Name , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(cu.Name , ''' + @CurrentUnitName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'cu.Name  LIKE ''' + @CurrentUnitName + ''' AND '
  END

 IF @CurrentUnitLocation IS NOT NULL
  IF @CurrentUnitLocation = '?'
   SET @r = @r + 'COALESCE(dbo.GetUnitLocation(cu.Id, default, default, default) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(dbo.GetUnitLocation(cu.Id, default, default, default) , ''' + @CurrentUnitLocation + ''') > 0 AND '
    ELSE
     SET @r = @r + 'dbo.GetUnitLocation(cu.Id, default, default, default)  LIKE ''' + @CurrentUnitLocation + ''' AND '
  END

 SET @r = @r + ' 1=1'
 RETURN @r
END