﻿CREATE FUNCTION dsql_GetExternalUnitMap_OrderBy
(
 @SortOrder nvarchar(100)
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @result nvarchar(max)
 SET @result = ' ORDER BY ' +
  CASE @SortOrder
   WHEN '+ExtId' THEN 'eu.ExtId  ASC'
   WHEN '-ExtId' THEN 'eu.ExtId  DESC'
   WHEN '+ExtDescription' THEN 'eu.ExtDescription  ASC'
   WHEN '-ExtDescription' THEN 'eu.ExtDescription  DESC'
   WHEN '+M2mId' THEN 'eu.M2mId  ASC'
   WHEN '-M2mId' THEN 'eu.M2mId  DESC'
   WHEN '+M2mDescription' THEN 'dbo.GetUnitLocation(eu.M2mId, 1, 1, default)  ASC'
   WHEN '-M2mDescription' THEN 'dbo.GetUnitLocation(eu.M2mId, 1, 1, default)  DESC'
   ELSE 'eu.ExtId '
  END
 RETURN @result + ' '
END