﻿CREATE FUNCTION [dbo].[formatMAC]
(
	@Input nvarchar(50)
)
RETURNS nvarchar(20)
AS
BEGIN
	DECLARE @i int = 3
	WHILE @i < LEN(@input) + 1
	BEGIN
		SELECT @input = STUFF(@input, @i, 0, ':'), @i = @i + 3
	END
	RETURN @input
END
