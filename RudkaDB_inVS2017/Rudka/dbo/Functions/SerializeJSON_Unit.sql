﻿


CREATE FUNCTION [dbo].[SerializeJSON_Unit]
(
	@RecordId int
)
RETURNS nvarchar(max)
AS
BEGIN

	DECLARE @JSON nvarchar(max)

	SET @JSON = 'SELECT u.Id, u.Name, dbo.GetUnitLocation(u.Id, default, default, default) AS Location, u.ParentUnitId, u.UnitTypeId, ut.Name as UnitTypeName
			, u.Active, u.UnitTypeIdV2, ut2.[Description] as UnitTypeV2Name, u.PictureId, u.Description, u.PreviewPictureId
	FROM dbo.Units u
	LEFT JOIN dbo.UnitTypesV2 ut2 ON u.UnitTypeIdV2 = ut2.Id
	LEFT JOIN dbo.UnitTypes ut ON u.UnitTypeId = ut.id
	WHERE u.Id = ' + CONVERT(nvarchar(10), @RecordId)
	EXEC @JSON = dbo.SerializeJSON @JSON

	RETURN @JSON
END