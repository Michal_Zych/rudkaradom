﻿CREATE FUNCTION [dbo].[dsql_GetArchiveData_From]
()
RETURNS nvarchar(max)
AS
BEGIN
 RETURN
 ' FROM [$(RudkaArch)].dbo.ev_Events AS e 
	JOIN enum.ObjectType objectType ON 1 = 1
	LEFT JOIN [$(RudkaArch)].dbo.ev_Alarms AS a ON a.EventId = e.Id 
	LEFT JOIN [$(RudkaArch)].dbo.ev_UsersAlarms AS ua ON a.EventId = ua.EventId 
	LEFT JOIN dbo.Units AS u ON e.UnitId = u.ID 
	LEFT JOIN dbo.Units AS uo ON e.ObjectUnitId = uo.ID 
	LEFT JOIN dbo.Users AS usr ON e.OperatorId = usr.ID 
	LEFT JOIN dbo.Users AS ausr ON a.ClosingUserId = ausr.ID 
	LEFT JOIN dbo.Users AS musr ON ua.UserId = musr.ID 
	LEFT JOIN dbo.enum_EventTypeDictionaryPl AS typeEnum ON typeEnum.Value = e.Type 
	LEFT JOIN dbo.enum_EventSeverityDictionaryPl AS severityEnum ON severityEnum.Value = e.Severity 
	LEFT JOIN dbo.enum_ObjectTypeDictionaryPl AS objectTypeEnum ON objectTypeEnum.Value = e.ObjectType
	LEFT JOIN [$(RudkaArch)].dbo.Patients AS patients ON e.ObjectId = patients.Id
	 '
END