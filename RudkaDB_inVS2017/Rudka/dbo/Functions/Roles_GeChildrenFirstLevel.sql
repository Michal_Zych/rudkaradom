﻿CREATE function [dbo].[Roles_GeChildrenFirstLevel](@roleId as int)
	returns table
AS
RETURN
	select *
	from [dbo].[Roles] as r
	where r.parentroleid = @roleId

