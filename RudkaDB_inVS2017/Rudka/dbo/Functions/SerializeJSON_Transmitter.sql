﻿



CREATE FUNCTION [dbo].[SerializeJSON_Transmitter]
(
	@RecordId int
)
RETURNS nvarchar(max)
AS
BEGIN

	DECLARE @JSON nvarchar(max)


	SET @JSON = 'SELECT t.Id, t.Mac, t.MacString, t.BarCode, t.Name, t.TypeId, t.ReportIntervalSec, t.Sensitivity, t.AvgCalcTimeS, t.SwitchDelayS, t.Msisdn, t.IsBandUpdater, t.Description
		,t.RssiTreshold, t.[Group], gr.Description AS GroupName, t.IpAddress, ''$Transitions$'' AS Tansitions
		,dbo.GetUnitLocation(u.ID, default, default, default) AS TransmitterUnitLocation, ut.UnitID AS UnitID
	FROM dbo.dev_Transmitters t
	LEFT JOIN dev_UnitTransmitter ut ON t.Id = ut.TransmitterId
	LEFT JOIN Units u ON ut.UnitID = u.ID
	LEFT JOIN dbo.dev_TransmitterGroups gr ON gr.[Group] = t.[Group]
	WHERE t.Id = ' + CONVERT(nvarchar(10), @RecordId)

	EXEC @JSON = dbo.SerializeJSON @JSON

	DECLARE @Transitions nvarchar(max)
	SET @Transitions = 'SELECT g.[Group] AS Id, g.[Description] AS Name FROM dbo.dev_TransmitterTransitions t LEFT JOIN dbo.dev_TransmitterGroups g ON t.[Group] = g.[Group] WHERE TransmitterId = ' + CONVERT(nvarchar(10), @RecordId)
	EXEC @Transitions = dbo.SerializeJSON @Transitions
	SET @JSON = REPLACE(@JSON, '"$Transitions$"', @Transitions)

	RETURN @JSON
END