﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetBKUnitType]
(
	@UnitID int
)
RETURNS char(2)
AS
BEGIN
	DECLARE @STALY_NADRZEDNY char(2) = 'SN'
		   ,@STALY_OSTATECZNY char(2) = 'SO'	
		   ,@RUCHOMY_OSTATECZNY char(2) = 'RU'
	DECLARE @result char(2)
		,@unitTypeID int
		
	SELECT @unitTypeID = unitTypeId FROM dbo.Units WHERE ID = @UnitID
			
	SET @result = CASE @unitTypeID
					WHEN 13 THEN @RUCHOMY_OSTATECZNY
					WHEN 12 THEN @STALY_OSTATECZNY
					ELSE @STALY_NADRZEDNY
				END			
		
	RETURN @result

END

