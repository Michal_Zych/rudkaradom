﻿CREATE FUNCTION [dbo].[SerializeJSON_User]
(
	@RecordId int
)
RETURNS nvarchar(max)
AS
BEGIN

	DECLARE @JSON nvarchar(max)
	SET @JSON = 'SELECT _tb.ID, _tb.Login, _tb.ClientID, _tb.UnitID, 
				_tb.Name, _tb.LastName, _tb.Phone, _tb.LastPingID, _tb.Settings, 
				_tb.RfidUid, _tb.Active, _tb.PasswordChangeDate, _tb.eMail, c.Name AS ClientName,  u.Name AS UnitName, ''$Callings$'' AS Callings, ''$NoGoZones$'' AS NoGoZones
			 FROM Users _tb 
			 LEFT  JOIN Clients c ON c.Id = _tb.ClientId
			 LEFT JOIN Units u ON u.Id = _tb.UnitId WHERE _tb.ID = ' + CONVERT(nvarchar(10), @RecordId)
	SET @JSON = dbo.SerializeJSON(@JSON)

	RETURN @JSON
END