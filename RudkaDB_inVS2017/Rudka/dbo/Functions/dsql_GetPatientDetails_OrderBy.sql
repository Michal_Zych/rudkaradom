﻿CREATE FUNCTION [dbo].[dsql_GetPatientDetails_OrderBy]
(
 @SortOrder nvarchar(100)
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @result nvarchar(max)
 SET @result = ' ORDER BY ' +
  CASE @SortOrder
   WHEN '+Id' THEN 'p.Id  ASC'
   WHEN '-Id' THEN 'p.Id  DESC'
   WHEN '+Name' THEN 'p.Name  ASC'
   WHEN '-Name' THEN 'p.Name  DESC'
   WHEN '+LastName' THEN 'p.LastName  ASC'
   WHEN '-LastName' THEN 'p.LastName  DESC'
   WHEN '+DisplayName' THEN 'dbo.GetDisplayName(p.Id, objectType.Patient)  ASC'
   WHEN '-DisplayName' THEN 'dbo.GetDisplayName(p.Id, objectType.Patient)  DESC'
   WHEN '+Pesel' THEN 'p.Pesel  ASC'
   WHEN '-Pesel' THEN 'p.Pesel  DESC'
   WHEN '+WardId' THEN 'p.WardId  ASC'
   WHEN '-WardId' THEN 'p.WardId  DESC'
   WHEN '+WardUnitName' THEN 'pu.Name  ASC'
   WHEN '-WardUnitName' THEN 'pu.Name  DESC'
   WHEN '+WardUnitLocation' THEN 'dbo.GetUnitLocation(p.WardId, default, default, default)  ASC'
   WHEN '-WardUnitLocation' THEN 'dbo.GetUnitLocation(p.WardId, default, default, default)  DESC'
   WHEN '+WardAdmissionDate' THEN 'p.WardDateUtc  ASC'
   WHEN '-WardAdmissionDate' THEN 'p.WardDateUtc  DESC'
   WHEN '+RoomId' THEN 'p.RoomId  ASC'
   WHEN '-RoomId' THEN 'p.RoomId  DESC'
   WHEN '+RoomUnitName' THEN 'pr.Name  ASC'
   WHEN '-RoomUnitName' THEN 'pr.Name  DESC'
   WHEN '+RoomUnitLocation' THEN 'dbo.GetUnitLocation(p.RoomId, default, default, default)  ASC'
   WHEN '-RoomUnitLocation' THEN 'dbo.GetUnitLocation(p.RoomId, default, default, default)  DESC'
   WHEN '+RoomAdmissionDate' THEN 'p.RoomDateUtc  ASC'
   WHEN '-RoomAdmissionDate' THEN 'p.RoomDateUtc  DESC'
   WHEN '+BandId' THEN 'b.Id  ASC'
   WHEN '-BandId' THEN 'b.Id  DESC'
   WHEN '+BandBarCode' THEN 'b.BarCode  ASC'
   WHEN '-BandBarCode' THEN 'b.BarCode  DESC'
   WHEN '+BandSerialNumber' THEN 'b.SerialNumber  ASC'
   WHEN '-BandSerialNumber' THEN 'b.SerialNumber  DESC'
   WHEN '+CurrentUnitName' THEN 'cu.Name  ASC'
   WHEN '-CurrentUnitName' THEN 'cu.Name  DESC'
   WHEN '+CurrentUnitLocation' THEN 'dbo.GetUnitLocation(cu.ID, default, default, default)  ASC'
   WHEN '-CurrentUnitLocation' THEN 'dbo.GetUnitLocation(cu.ID, default, default, default)  DESC'
   ELSE 'p.Id '
  END
 RETURN @result + ' '
END