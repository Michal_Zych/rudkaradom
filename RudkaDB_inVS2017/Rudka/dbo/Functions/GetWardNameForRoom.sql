﻿CREATE FUNCTION [dbo].[GetWardNameForRoom]
(
	@RoomId int
)
RETURNS nvarchar(100)
AS
BEGIN
	DECLARE
		@wardName nvarchar(100)

	SELECT @wardName = Name
	FROM dbo.Units WHERE Id = dbo.GetWardIdForUnit(@RoomId)

	RETURN @wardName
END
