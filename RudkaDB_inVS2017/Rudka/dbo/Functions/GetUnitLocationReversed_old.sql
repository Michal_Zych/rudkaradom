﻿

CREATE FUNCTION [dbo].[GetUnitLocationReversed_old](
	@UnitID INT
	,@FullPath bit = null
	,@ShowRoot bit = null
	,@Separator nvarchar(20) = null
	)
RETURNS nvarchar(1024)
AS
BEGIN
	DECLARE @ParentUnitID int
	DECLARE @Name nvarchar(1024)
	DECLARE @Location nvarchar(1024)

	SELECT @Name = Name, @ParentUnitID = ParentUnitID  
	FROM dbo.Units
	WHERE ID = @UnitID
	
	IF @Name IS NULL
	BEGIN
		RETURN NULL
	END

	
	IF @Separator IS NULL 
	BEGIN
		SELECT @Separator = [dbo].[settings_GetString_old](1, 'UnitsLocationSeparator') 
	END
	
	
	IF (ISNULL(@FullPath, 0) = 0) 
	BEGIN
		SET @Name = ''
	END

	SET @Location =  @Name
 
	WHILE @ParentUnitID IS NOT NULL
	BEGIN
		SET @UnitID = @ParentUnitID
		SELECT @Name = Name, @ParentUnitID = ParentUnitID  
		FROM dbo.Units
		WHERE ID = @UnitID
		
		IF (@ParentUnitID IS NOT NULL) OR (@ShowRoot = 1)
		BEGIN
			SET @Location = @Location + @Separator + @Name
		END
	END

	IF ISNULL(@FullPath, 0) = 0
	BEGIN
		IF LEN(@Location) >= LEN(@separator)
		BEGIN
			SET @Location = SUBSTRING(@location, 1, LEN(@location) - LEN(@separator))
		END			
	END

	RETURN @Location 
END
