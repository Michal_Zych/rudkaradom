﻿CREATE FUNCTION dsql_GetExternalRecordsOut_Select
()
RETURNS nvarchar(max)
AS
BEGIN
 RETURN
'Id AS Id
,EventId AS EventId
,DateUtc AS DateUtc
,RecType AS RecordType
,Record AS Record
,ParentId AS ParentId
,Try AS Try
,TryAgain AS TryAgain
,SentDateUtc AS SentDateUtc
,Error AS Error
'
END