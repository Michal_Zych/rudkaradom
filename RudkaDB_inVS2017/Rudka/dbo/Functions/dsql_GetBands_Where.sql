﻿CREATE FUNCTION dsql_GetBands_Where
(
@TreeUnitIds nvarchar(max),
@IdFrom smallint = null
,@IdTo smallint = null
,@BarCode nvarchar(200) = null
,@MacString nvarchar(200) = null
,@UidFrom bigint = null
,@UidTo bigint = null
,@SerialNumber nvarchar(200) = null
,@BatteryTypeIdFrom tinyint = null
,@BatteryTypeIdTo tinyint = null
,@BatteryTypeName nvarchar(200) = null
,@BatteryAlarmSeverityIdFrom int = null
,@BatteryAlarmSeverityIdTo int = null
,@BatteryAlarmSeverityName nvarchar(200) = null
,@BatteryInstallationDateUtcFrom datetime = null
,@BatteryInstallationDateUtcTo datetime = null
,@BatteryDaysToExchangeFrom int = null
,@BatteryDaysToExchangeTo int = null
,@Description nvarchar(200) = null
,@ObjectTypeIdFrom tinyint = null
,@ObjectTypeIdTo tinyint = null
,@ObjectTypeName nvarchar(200) = null
,@ObjectDisplayName nvarchar(200) = null
,@UnitIdFrom int = null
,@UnitIdTo int = null
,@UnitName nvarchar(200) = null
,@UnitLocation nvarchar(200) = null
,@CurrentUnitIdFrom int = null
,@CurrentUnitIdTo int = null
,@CurrentUnitName nvarchar(200) = null
,@CurrentUnitLocation nvarchar(200) = null
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @useMatchFunction bit = 1
 DECLARE @r nvarchar(max) = ' WHERE ' + 'b.Id > 0 AND '

-- IF COALESCE(@TreeUnitIds, '') <> ''
--   SET @r = @r + 'UnitId IN(''' + @TreeUnitIds + ''') AND '

 IF @IdFrom IS NOT NULL AND @IdTo IS NOT NULL
  SET @r = @r + '(b.Id  BETWEEN ' + CONVERT(nvarchar(20), @IdFrom) + ' AND ' + CONVERT(nvarchar(20), @IdTo) + ') AND '
 ELSE IF @IdFrom IS NOT NULL AND @IdTo IS NULL
  SET @r = @r + 'b.Id  = ' + CONVERT(nvarchar(20), @IdFrom) + ' AND '
 ELSE IF @IdFrom IS NOT NULL AND @IdTo IS NOT NULL
  SET @r = @r + 'b.Id  IS NULL AND '

 IF @BarCode IS NOT NULL
  IF @BarCode = '?'
   SET @r = @r + 'COALESCE(b.BarCode , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(b.BarCode , ''' + @BarCode + ''') > 0 AND '
    ELSE
     SET @r = @r + 'b.BarCode  LIKE ''' + @BarCode + ''' AND '
  END

 IF @MacString IS NOT NULL
  IF @MacString = '?'
   SET @r = @r + 'COALESCE(b.MacString , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(b.MacString , ''' + @MacString + ''') > 0 AND '
    ELSE
     SET @r = @r + 'b.MacString  LIKE ''' + @MacString + ''' AND '
  END

 IF @UidFrom IS NOT NULL AND @UidTo IS NOT NULL
  SET @r = @r + '(b.Uid  BETWEEN ' + CONVERT(nvarchar(20), @UidFrom) + ' AND ' + CONVERT(nvarchar(20), @UidTo) + ') AND '
 ELSE IF @UidFrom IS NOT NULL AND @UidTo IS NULL
  SET @r = @r + 'b.Uid  = ' + CONVERT(nvarchar(20), @UidFrom) + ' AND '
 ELSE IF @UidFrom IS NOT NULL AND @UidTo IS NOT NULL
  SET @r = @r + 'b.Uid  IS NULL AND '

 IF @SerialNumber IS NOT NULL
  IF @SerialNumber = '?'
   SET @r = @r + 'COALESCE(b.SerialNumber , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(b.SerialNumber , ''' + @SerialNumber + ''') > 0 AND '
    ELSE
     SET @r = @r + 'b.SerialNumber  LIKE ''' + @SerialNumber + ''' AND '
  END

 IF @BatteryTypeIdFrom IS NOT NULL AND @BatteryTypeIdTo IS NOT NULL
  SET @r = @r + '(bt.Id  BETWEEN ' + CONVERT(nvarchar(20), @BatteryTypeIdFrom) + ' AND ' + CONVERT(nvarchar(20), @BatteryTypeIdTo) + ') AND '
 ELSE IF @BatteryTypeIdFrom IS NOT NULL AND @BatteryTypeIdTo IS NULL
  SET @r = @r + 'bt.Id  = ' + CONVERT(nvarchar(20), @BatteryTypeIdFrom) + ' AND '
 ELSE IF @BatteryTypeIdFrom IS NOT NULL AND @BatteryTypeIdTo IS NOT NULL
  SET @r = @r + 'bt.Id  IS NULL AND '

 IF @BatteryTypeName IS NOT NULL
  IF @BatteryTypeName = '?'
   SET @r = @r + 'COALESCE(bt.Name , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(bt.Name , ''' + @BatteryTypeName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'bt.Name  LIKE ''' + @BatteryTypeName + ''' AND '
  END

 IF @BatteryAlarmSeverityIdFrom IS NOT NULL AND @BatteryAlarmSeverityIdTo IS NOT NULL
  SET @r = @r + '(COALESCE(e.Severity, CASE WHEN bs.OngLowBatteryEventId IS NOT NULL THEN 1 ELSE NULL END)  BETWEEN ' + CONVERT(nvarchar(20), @BatteryAlarmSeverityIdFrom) + ' AND ' + CONVERT(nvarchar(20), @BatteryAlarmSeverityIdTo) + ') AND '
 ELSE IF @BatteryAlarmSeverityIdFrom IS NOT NULL AND @BatteryAlarmSeverityIdTo IS NULL
  SET @r = @r + 'COALESCE(e.Severity, CASE WHEN bs.OngLowBatteryEventId IS NOT NULL THEN 1 ELSE NULL END)  = ' + CONVERT(nvarchar(20), @BatteryAlarmSeverityIdFrom) + ' AND '
 ELSE IF @BatteryAlarmSeverityIdFrom IS NOT NULL AND @BatteryAlarmSeverityIdTo IS NOT NULL
  SET @r = @r + 'COALESCE(e.Severity, CASE WHEN bs.OngLowBatteryEventId IS NOT NULL THEN 1 ELSE NULL END)  IS NULL AND '

 IF @BatteryAlarmSeverityName IS NOT NULL
  IF @BatteryAlarmSeverityName = '?'
   SET @r = @r + 'COALESCE(es.Description , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(es.Description , ''' + @BatteryAlarmSeverityName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'es.Description  LIKE ''' + @BatteryAlarmSeverityName + ''' AND '
  END

 IF @BatteryInstallationDateUtcFrom IS NOT NULL
  SET @r = @r + 'b.BatteryInstallationDateUtc  >=''' + CONVERT(nvarchar(30), @BatteryInstallationDateUtcFrom, 121) + ''' AND '
 IF @BatteryInstallationDateUtcTo IS NOT NULL
  SET @r = @r + 'b.BatteryInstallationDateUtc  <=''' + CONVERT(nvarchar(30), @BatteryInstallationDateUtcTo, 121) + ''' AND '

 IF @BatteryDaysToExchangeFrom IS NOT NULL AND @BatteryDaysToExchangeTo IS NOT NULL
  SET @r = @r + '(DATEDIFF(DAY, dbo.GetCurrentUtcDate(), DATEADD(MONTH, bt.LifeMonths, b.BatteryInstallationDateUtc))  BETWEEN ' + CONVERT(nvarchar(20), @BatteryDaysToExchangeFrom) + ' AND ' + CONVERT(nvarchar(20), @BatteryDaysToExchangeTo) + ') AND '
 ELSE IF @BatteryDaysToExchangeFrom IS NOT NULL AND @BatteryDaysToExchangeTo IS NULL
  SET @r = @r + 'DATEDIFF(DAY, dbo.GetCurrentUtcDate(), DATEADD(MONTH, bt.LifeMonths, b.BatteryInstallationDateUtc))  = ' + CONVERT(nvarchar(20), @BatteryDaysToExchangeFrom) + ' AND '
 ELSE IF @BatteryDaysToExchangeFrom IS NOT NULL AND @BatteryDaysToExchangeTo IS NOT NULL
  SET @r = @r + 'DATEDIFF(DAY, dbo.GetCurrentUtcDate(), DATEADD(MONTH, bt.LifeMonths, b.BatteryInstallationDateUtc))  IS NULL AND '

 IF @Description IS NOT NULL
  IF @Description = '?'
   SET @r = @r + 'COALESCE(b.Description , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(b.Description , ''' + @Description + ''') > 0 AND '
    ELSE
     SET @r = @r + 'b.Description  LIKE ''' + @Description + ''' AND '
  END

 IF @ObjectTypeIdFrom IS NOT NULL AND @ObjectTypeIdTo IS NOT NULL
  SET @r = @r + '(bs.ObjectType  BETWEEN ' + CONVERT(nvarchar(20), @ObjectTypeIdFrom) + ' AND ' + CONVERT(nvarchar(20), @ObjectTypeIdTo) + ') AND '
 ELSE IF @ObjectTypeIdFrom IS NOT NULL AND @ObjectTypeIdTo IS NULL
  SET @r = @r + 'bs.ObjectType  = ' + CONVERT(nvarchar(20), @ObjectTypeIdFrom) + ' AND '
 ELSE IF @ObjectTypeIdFrom IS NOT NULL AND @ObjectTypeIdTo IS NOT NULL
  SET @r = @r + 'bs.ObjectType  IS NULL AND '

 IF @ObjectTypeName IS NOT NULL
  IF @ObjectTypeName = '?'
   SET @r = @r + 'COALESCE(ot.Description , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(ot.Description , ''' + @ObjectTypeName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'ot.Description  LIKE ''' + @ObjectTypeName + ''' AND '
  END

 IF @ObjectDisplayName IS NOT NULL
  IF @ObjectDisplayName = '?'
   SET @r = @r + 'COALESCE(dbo.GetDisplayName(bs.ObjectId, bs.ObjectType) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(dbo.GetDisplayName(bs.ObjectId, bs.ObjectType) , ''' + @ObjectDisplayName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'dbo.GetDisplayName(bs.ObjectId, bs.ObjectType)  LIKE ''' + @ObjectDisplayName + ''' AND '
  END

 IF @UnitIdFrom IS NOT NULL AND @UnitIdTo IS NOT NULL
  SET @r = @r + '(b.UnitId  BETWEEN ' + CONVERT(nvarchar(20), @UnitIdFrom) + ' AND ' + CONVERT(nvarchar(20), @UnitIdTo) + ') AND '
 ELSE IF @UnitIdFrom IS NOT NULL AND @UnitIdTo IS NULL
  SET @r = @r + 'b.UnitId  = ' + CONVERT(nvarchar(20), @UnitIdFrom) + ' AND '
 ELSE IF @UnitIdFrom IS NOT NULL AND @UnitIdTo IS NOT NULL
  SET @r = @r + 'b.UnitId  IS NULL AND '

 IF @UnitName IS NOT NULL
  IF @UnitName = '?'
   SET @r = @r + 'COALESCE(bu.Name , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(bu.Name , ''' + @UnitName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'bu.Name  LIKE ''' + @UnitName + ''' AND '
  END

 IF @UnitLocation IS NOT NULL
  IF @UnitLocation = '?'
   SET @r = @r + 'COALESCE(dbo.GetUnitLocation(bu.Id, 1, 0, default) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(dbo.GetUnitLocation(bu.Id, 1, 0, default) , ''' + @UnitLocation + ''') > 0 AND '
    ELSE
     SET @r = @r + 'dbo.GetUnitLocation(bu.Id, 1, 0, default)  LIKE ''' + @UnitLocation + ''' AND '
  END

 IF @CurrentUnitIdFrom IS NOT NULL AND @CurrentUnitIdTo IS NOT NULL
  SET @r = @r + '(cu.ID  BETWEEN ' + CONVERT(nvarchar(20), @CurrentUnitIdFrom) + ' AND ' + CONVERT(nvarchar(20), @CurrentUnitIdTo) + ') AND '
 ELSE IF @CurrentUnitIdFrom IS NOT NULL AND @CurrentUnitIdTo IS NULL
  SET @r = @r + 'cu.ID  = ' + CONVERT(nvarchar(20), @CurrentUnitIdFrom) + ' AND '
 ELSE IF @CurrentUnitIdFrom IS NOT NULL AND @CurrentUnitIdTo IS NOT NULL
  SET @r = @r + 'cu.ID  IS NULL AND '

 IF @CurrentUnitName IS NOT NULL
  IF @CurrentUnitName = '?'
   SET @r = @r + 'COALESCE(cu.Name , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(cu.Name , ''' + @CurrentUnitName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'cu.Name  LIKE ''' + @CurrentUnitName + ''' AND '
  END

 IF @CurrentUnitLocation IS NOT NULL
  IF @CurrentUnitLocation = '?'
   SET @r = @r + 'COALESCE(dbo.GetUnitLocation(cu.Id, 1, 0, default) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(dbo.GetUnitLocation(cu.Id, 1, 0, default) , ''' + @CurrentUnitLocation + ''') > 0 AND '
    ELSE
     SET @r = @r + 'dbo.GetUnitLocation(cu.Id, 1, 0, default)  LIKE ''' + @CurrentUnitLocation + ''' AND '
  END

 SET @r = @r + ' 1=1'
 RETURN @r
END