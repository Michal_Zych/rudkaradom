﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[srv_StatusToBKstatus]
(
	@Id int
)
RETURNS nvarchar(10)
AS
BEGIN
	DECLARE @result nvarchar(10)
	
	SET @result = CASE @id
					WHEN 5 THEN 'R'		-- wydanie na zewnątrz
					WHEN 6 THEN 'P'		-- przetworzony
					WHEN 7 THEN 'Z'		-- zniszczony
					ELSE ''				-- nie obsługują innych statusów
				  END
	
	RETURN @result	
END

