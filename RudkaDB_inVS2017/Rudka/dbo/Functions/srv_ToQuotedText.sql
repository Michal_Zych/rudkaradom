﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[srv_ToQuotedText]
(
	@text nvarchar(max)
)
RETURNS nvarchar(max)
AS
BEGIN
	RETURN '''' + REPLACE(@text, '''', '&quot;') + ''''
END

