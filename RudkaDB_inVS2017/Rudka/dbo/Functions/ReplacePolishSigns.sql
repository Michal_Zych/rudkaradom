﻿CREATE FUNCTION [dbo].[ReplacePolishSigns]
(
	@polish nvarchar(max)
)
RETURNS nvarchar(max)
AS
BEGIN

	declare @return nvarchar(max)
	
	set @return = @polish
	
	set @return = REPLACE(@return,'ę','e');
	set @return = REPLACE(@return,'ó','o');
	set @return = REPLACE(@return,'ą','a');
	set @return = REPLACE(@return,'ś','s');
	set @return = REPLACE(@return,'ł','l');
	set @return = REPLACE(@return,'ż','z');
	set @return = REPLACE(@return,'ź','z');
	set @return = REPLACE(@return,'ć','c');
	set @return = REPLACE(@return,'ń','n');
	
	RETURN @return

END

