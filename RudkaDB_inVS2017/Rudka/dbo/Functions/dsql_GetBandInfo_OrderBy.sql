﻿CREATE FUNCTION dsql_GetBandInfo_OrderBy
(
 @SortOrder nvarchar(100)
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @result nvarchar(max)
 SET @result = ' ORDER BY ' +
  CASE @SortOrder
   WHEN '+Id' THEN 'b.Id  ASC'
   WHEN '-Id' THEN 'b.Id  DESC'
   WHEN '+BarCode' THEN 'b.BarCode  ASC'
   WHEN '-BarCode' THEN 'b.BarCode  DESC'
   WHEN '+MacString' THEN 'b.MacString  ASC'
   WHEN '-MacString' THEN 'b.MacString  DESC'
   WHEN '+Uid' THEN 'b.Uid  ASC'
   WHEN '-Uid' THEN 'b.Uid  DESC'
   WHEN '+SerialNumber' THEN 'b.SerialNumber  ASC'
   WHEN '-SerialNumber' THEN 'b.SerialNumber  DESC'
   WHEN '+BatteryTypeId' THEN 'bt.Id  ASC'
   WHEN '-BatteryTypeId' THEN 'bt.Id  DESC'
   WHEN '+BatteryTypeName' THEN 'bt.Name  ASC'
   WHEN '-BatteryTypeName' THEN 'bt.Name  DESC'
   WHEN '+BatteryAlarmSeverityId' THEN 'COALESCE(e.Severity, CASE WHEN bs.OngLowBatteryEventId IS NOT NULL THEN 1 ELSE NULL END)  ASC'
   WHEN '-BatteryAlarmSeverityId' THEN 'COALESCE(e.Severity, CASE WHEN bs.OngLowBatteryEventId IS NOT NULL THEN 1 ELSE NULL END)  DESC'
   WHEN '+BatteryAlarmSeverityName' THEN 'es.Description  ASC'
   WHEN '-BatteryAlarmSeverityName' THEN 'es.Description  DESC'
   WHEN '+BatteryInstallationDateUtc' THEN 'b.BatteryInstallationDateUtc  ASC'
   WHEN '-BatteryInstallationDateUtc' THEN 'b.BatteryInstallationDateUtc  DESC'
   WHEN '+BatteryDaysToExchange' THEN 'DATEDIFF(DAY, dbo.GetCurrentUtcDate(), DATEADD(MONTH, bt.LifeMonths, b.BatteryInstallationDateUtc))  ASC'
   WHEN '-BatteryDaysToExchange' THEN 'DATEDIFF(DAY, dbo.GetCurrentUtcDate(), DATEADD(MONTH, bt.LifeMonths, b.BatteryInstallationDateUtc))  DESC'
   WHEN '+Description' THEN 'b.Description  ASC'
   WHEN '-Description' THEN 'b.Description  DESC'
   WHEN '+ObjectTypeId' THEN 'bs.ObjectType  ASC'
   WHEN '-ObjectTypeId' THEN 'bs.ObjectType  DESC'
   WHEN '+ObjectTypeName' THEN 'ot.Description  ASC'
   WHEN '-ObjectTypeName' THEN 'ot.Description  DESC'
   WHEN '+ObjectDisplayName' THEN 'dbo.GetDisplayName(bs.ObjectId, bs.ObjectType)  ASC'
   WHEN '-ObjectDisplayName' THEN 'dbo.GetDisplayName(bs.ObjectId, bs.ObjectType)  DESC'
   WHEN '+UnitId' THEN 'b.UnitId  ASC'
   WHEN '-UnitId' THEN 'b.UnitId  DESC'
   WHEN '+UnitName' THEN 'bu.Name  ASC'
   WHEN '-UnitName' THEN 'bu.Name  DESC'
   WHEN '+UnitLocation' THEN 'dbo.GetUnitLocation(bu.Id, 1, 0, default)  ASC'
   WHEN '-UnitLocation' THEN 'dbo.GetUnitLocation(bu.Id, 1, 0, default)  DESC'
   WHEN '+CurrentUnitId' THEN 'cu.ID  ASC'
   WHEN '-CurrentUnitId' THEN 'cu.ID  DESC'
   WHEN '+CurrentUnitName' THEN 'cu.Name  ASC'
   WHEN '-CurrentUnitName' THEN 'cu.Name  DESC'
   WHEN '+CurrentUnitLocation' THEN 'dbo.GetUnitLocation(cu.Id, 1, 0, default)  ASC'
   WHEN '-CurrentUnitLocation' THEN 'dbo.GetUnitLocation(cu.Id, 1, 0, default)  DESC'
   ELSE 'b.Id '
  END
 RETURN @result + ' '
END