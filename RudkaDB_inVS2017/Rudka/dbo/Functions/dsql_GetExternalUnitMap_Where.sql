﻿CREATE FUNCTION dsql_GetExternalUnitMap_Where
(
@TreeUnitIds nvarchar(max),
@ExtId nvarchar(200) = null
,@ExtDescription nvarchar(200) = null
,@M2mIdFrom int = null
,@M2mIdTo int = null
,@M2mDescription nvarchar(200) = null
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @useMatchFunction bit = 1
 DECLARE @r nvarchar(max) = ' WHERE ' + '1 = 1 AND '

-- IF COALESCE(@TreeUnitIds, '') <> ''
--   SET @r = @r + 'UnitId IN(''' + @TreeUnitIds + ''') AND '

 IF @ExtId IS NOT NULL
  IF @ExtId = '?'
   SET @r = @r + 'COALESCE(eu.ExtId , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(eu.ExtId , ''' + @ExtId + ''') > 0 AND '
    ELSE
     SET @r = @r + 'eu.ExtId  LIKE ''' + @ExtId + ''' AND '
  END

 IF @ExtDescription IS NOT NULL
  IF @ExtDescription = '?'
   SET @r = @r + 'COALESCE(eu.ExtDescription , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(eu.ExtDescription , ''' + @ExtDescription + ''') > 0 AND '
    ELSE
     SET @r = @r + 'eu.ExtDescription  LIKE ''' + @ExtDescription + ''' AND '
  END

 IF @M2mIdFrom IS NOT NULL AND @M2mIdTo IS NOT NULL
  SET @r = @r + '(eu.M2mId  BETWEEN ' + CONVERT(nvarchar(20), @M2mIdFrom) + ' AND ' + CONVERT(nvarchar(20), @M2mIdTo) + ') AND '
 ELSE IF @M2mIdFrom IS NOT NULL AND @M2mIdTo IS NULL
  SET @r = @r + 'eu.M2mId  = ' + CONVERT(nvarchar(20), @M2mIdFrom) + ' AND '
 ELSE IF @M2mIdFrom IS NULL AND @M2mIdTo IS NOT NULL
  SET @r = @r + 'eu.M2mId  IS NULL AND '

 IF @M2mDescription IS NOT NULL
  IF @M2mDescription = '?'
   SET @r = @r + 'COALESCE(dbo.GetUnitLocation(eu.M2mId, 1, 1, default) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(dbo.GetUnitLocation(eu.M2mId, 1, 1, default) , ''' + @M2mDescription + ''') > 0 AND '
    ELSE
     SET @r = @r + 'dbo.GetUnitLocation(eu.M2mId, 1, 1, default)  LIKE ''' + @M2mDescription + ''' AND '
  END

 SET @r = @r + ' 1=1'
 RETURN @r
END