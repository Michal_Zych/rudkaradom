﻿-- =============================================
-- Author:		pi	
-- Create date: 10-10-2012
-- Description:	Returns nearest Common Ancestor
-- =============================================
CREATE FUNCTION [dbo].[Units_NearestCommonAncestor]
(
	@units nvarchar(MAX)
)
RETURNS INT
AS
BEGIN
	DECLARE @return INT
	DECLARE @count INT
	
	--ids
	DECLARE @ids TABLE (Id INT NOT NULL);
	INSERT INTO @ids
	SELECT value FROM dbo.SplitWordsToInt(@units)

	--count
	SELECT @count = COUNT(1) FROM @ids;

	WITH Nodes(Id, ParentId, Depth) AS
	(
		 SELECT pc.Id , pc.ParentUnitId , 0 AS DEPTH
		 FROM Units pc
		 JOIN @ids i ON pc.Id = i.Id

		 UNION ALL

		 -- Recursively find parent nod es for each starting node.
		 SELECT pc.Id , pc.ParentUnitId , n.Depth - 1
		 FROM Units pc
		 JOIN Nodes n ON pc.Id = n.ParentId
	)
	--nearest common ancestor
	SELECT TOP 1 @return = n.Id
	FROM Nodes n
	GROUP BY n.Id
	HAVING COUNT(n.Id) = @count
	ORDER BY MIN(n.Depth) DESC
	
	RETURN @return
END

