﻿

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetM2mUnitType]
(
	@unitTypeCode nvarchar(20)
)
RETURNS int
AS
BEGIN
	DECLARE @STALY_NADRZEDNY char(2) = 'SN'
		   ,@STALY_OSTATECZNY char(2) = 'SO'	
		   ,@RUCHOMY_OSTATECZNY char(2) = 'RU'
		   ,@UNIT_POLKA int = 12
		   ,@UNIT_KUWETA int = 13
	DECLARE @result int
		
	SET @result = NULL
	IF @unitTypeCode = @STALY_OSTATECZNY SET @result = @UNIT_POLKA	
	IF @unitTypeCode = @RUCHOMY_OSTATECZNY SET @result = @UNIT_KUWETA
		
	RETURN @result

END



