﻿
CREATE FUNCTION dbo.GetUtcHour 
(
	@localTimeHour time
)
RETURNS time
AS
BEGIN
	 RETURN DATEADD(HOUR, DATEDIFF(HOUR, GETDATE(), GETUTCDATE()), @localTimeHour )
END