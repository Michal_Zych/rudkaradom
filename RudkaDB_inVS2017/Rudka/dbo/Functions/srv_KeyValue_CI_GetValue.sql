﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[srv_KeyValue_CI_GetValue]
(
	@text nvarchar(max)
	,@param nvarchar(50)
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @NULL_VALUE nvarchar(5) = 'null'
		,@TO_END int = 99999

	DECLARE @result nvarchar(max)
	DECLARE @uText nvarchar(max) 

	SELECT @uText = ';' + UPPER(@text), @param = ';' + UPPER(@param)


	DECLARE @keyStart int
		,@valueStart int
		,@valueEnd int

	SET @keyStart = CHARINDEX(@param + '=', @uText COLLATE SQL_Latin1_General_Cp1_CS_AS)
	
	IF @keyStart = 0 RETURN NULL
	
	SET @valueStart = @keyStart + LEN(@param) 
	
	IF SUBSTRING(@text, @valueStart, 1) = ''''
		SET @valueEnd = CHARINDEX('''', SUBSTRING(@text, @valueStart + 1, @TO_END)) +1
	ELSE
		SET @valueEnd = CHARINDEX(';', SUBSTRING(@text, @valueStart, @TO_END)) - 1
	IF @valueEnd = -1 SET @valueEnd = @TO_END
	SET @result = dbo.srv_FromQuotedText(SUBSTRING(@text,@valueStart, @valueEnd))
	RETURN NULLIF(@result, @NULL_VALUE)
END