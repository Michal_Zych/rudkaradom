﻿


CREATE FUNCTION [dbo].[FormatAlarmNotifyText]
(
	@SensorId int
	,@Type int
	,@EventDate datetime
	,@EventType nvarchar(1) -- null alarm trwa, 'F' koniec, 'C' zamknięcie
	,@IsRangeAlarm bit  
	,@Value nvarchar(10)
	,@LoRange nvarchar(10)
	,@UpRange nvarchar(10)
	,@UserName nvarchar(200)
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE			--typy powiadomień
		  @GUI			tinyint = 0
		, @SMS			tinyint = 1
		, @EMAIL		tinyint = 2

		,@VALUE_TO_LOW tinyint = 1
		,@VALUE_TO_HIGH tinyint = 2
		,@NO_COMMUNICATION tinyint = 3
		,@NO_SENSOR tinyint = 3
		,@NO_DEVICE tinyint = 4
		,@NO_HUB tinyint = 5
		
		,@CURRENT_DATE datetime = [dbo].[GetCurrentDate]()

	DECLARE
		@alarmText nvarchar(500)
		,@sensorName nvarchar(100)
		,@unitId int

	SELECT @unitId = UnitId FROM dbo.Sensors WHERE ID = @SensorId

	IF @Type = @SMS SELECT @sensorName = up.Name + '>' + u.Name 
					FROM dbo.Units u 
						JOIN dbo.Units up WITH(NOLOCK) ON up.ID = u.ParentUnitID 
					WHERE u.Id = @unitId
	ELSE			SELECT @sensorName = dbo.GetUnitLocation(@unitId, 1, default, default) 
	
	
	SELECT @alarmText = 'Alarm: ' + @sensorName + ' Data: '+ CONVERT(nvarchar(30), COALESCE(@EventDate, @CURRENT_DATE), 120) + ' '

	IF @eventType = 'F'
		SELECT @alarmText = @alarmText + 'Koniec alarmu ' 
			+ CASE
					WHEN @IsRangeAlarm = 1 THEN 'przekroczenia zakresu'
					ELSE 'braku łączności'
				END
	ELSE IF @EventType = 'C'
		BEGIN
			SELECT @alarmText = @alarmText + 'Zamknięcie alarmu'
			IF @UserName IS NULL SELECT @alarmText = @alarmText + '-alarmowanie wyłączone'
			ELSE SELECT @alarmText = @alarmText + ' przez ' + @UserName
		END
	ELSE
		BEGIN
			IF (@IsRangeAlarm = 0) OR (@Value IS NULL)
			BEGIN
				SELECT @alarmText = @alarmText + ' Brak łączności'
			END
			ELSE
			BEGIN
				SELECT @alarmText = @alarmText + ' Wartość: ' + @Value
				IF @LoRange IS NOT NULL
					SELECT @alarmText  = @alarmText + ' (Zakres ' + @LoRange + ' do ' + @UpRange + ')'
			END
		END
	
	if @Type = @SMS SET @alarmText = LEFT(dbo.ReplacePolishSigns(@alarmText), 160)
	
	RETURN @alarmText
END
