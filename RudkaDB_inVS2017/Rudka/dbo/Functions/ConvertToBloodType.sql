﻿-- =============================================
-- Author:		pi
-- Create date: 20-09-2012
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[ConvertToBloodType](@FullName varchar(4))
RETURNS nvarchar(3)
AS
BEGIN
	
	DECLARE @BloodType nvarchar(3)
	
	IF @FullName IS NOT NULL AND NOT @FullName = ' '
	BEGIN
		DECLARE @sub nvarchar(2)
		SET @sub = SUBSTRING(@FullName, 0, 3)
		SET @BloodType =
			CASE @sub
				WHEN N'06' THEN N'A-'
				WHEN N'62' THEN N'A+'
				WHEN N'17' THEN N'B-'
				WHEN N'73' THEN N'B+'
				 
				WHEN N'28' THEN N'AB-'
				WHEN N'84' THEN N'AB+'
				WHEN N'95' THEN N'0-'
				WHEN N'51' THEN N'0+'

				WHEN N'66' THEN N'A'
				WHEN N'77' THEN N'B'
				WHEN N'88' THEN N'AB'
				WHEN N'55' THEN N'0'
			END
	END
	RETURN @BloodType
END

