﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	dala xml'a w stylu select * from aaa for xml auto, elements xsinil
-- =============================================
CREATE FUNCTION [dbo].[SerializeXMLToCSV]
(
	@row XML
)
RETURNS nvarchar(max)
AS
BEGIN

	DECLARE @txt nvarchar(max)
	SET @txt = CONVERT(nvarchar(max), @row)

	DECLARE @result nvarchar(max) = ''
	DECLARE @start int
			,@fclose int
			,@fopen int
			,@token nvarchar(max)

	SET @start = 1

	WHILE 1 = 1
	BEGIN
		SET @start = CHARINDEX('><', @txt, @start)
		SET @fclose = CHARINDEX('>', @txt, @start + 1)
		SET @fopen = CHARINDEX('<', @txt, @start  + 2)
	
		IF @fopen = 0 BREAK
	
		SET @token = SUBSTRING(@txt, @fclose + 1, @fopen - @fclose-1 )
		SET @token = REPLACE(@token, '&lt;', '<')
		SET @token = REPLACE(@token, '&gt;', '>')
		
		IF SUBSTRING(@txt, @fclose -2, 2) = '"/' SET @token = 'null'
	
	
		SET	@result = @result + ';' 
			+ CASE 
				WHEN ISNUMERIC(@token) = 1 THEN @token
				WHEN ISDATE(@token) = 1 THEN '''' + CONVERT(nvarchar(20), CONVERT(datetime, @token), 120) + ''''
				WHEN @token = 'null' THEN @token
			  ELSE '''' + @token + ''''
			END
		IF @fopen = @fclose + 1 SET @fopen = @fclose
	
		SET	@start = @fopen
END
	RETURN STUFF(@result, 1, 1, '')
END

