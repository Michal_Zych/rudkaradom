﻿

CREATE FUNCTION [dbo].[SerializeJSON_Band]
(
	@RecordId int
)
RETURNS nvarchar(max)
AS
BEGIN

	DECLARE @JSON nvarchar(max)

	SET @JSON ='SELECT b.ID, b.MacString AS MAC, b.Uid, b.BarCode, b.TransmissionPower, b.AccPercent, b.SensitivityPercent, b.BroadcastingIntervalMs, b.Description
			, b.BatteryInstallationDateUtc, ''$Battery$'' AS Battery, ''$Parameters$'' AS [Parameters], ''$Unit$'' AS [Unit]
		FROM Bands b
		JOIN enum.ObjectType objectType ON 1 = 1
		WHERE b.Id = ' + CONVERT(nvarchar(10), @RecordId)

	EXEC @JSON = dbo.SerializeJSON @JSON

	DECLARE @Battery nvarchar(max)
	SET @Battery = 'SELECT bt.Id, bt.Name, bt.LifeMonths, bt.Capacity, CONVERT(nvarchar(10), bt.Voltage) as Voltage, bt.Description FROM BatteryTypes bt JOIN Bands b ON bt.Id = b.BatteryTypeId WHERE b.Id =' + CONVERT(nvarchar(10), @RecordId)
	EXEC @Battery = dbo.SerializeJSON @Battery
	SET @JSON = REPLACE(@JSON, '"$Battery$"', @Battery)
	
	DECLARE @Parameters nvarchar(max)
	SET @Parameters = 'SELECT [Key], Value FROM BandSettings WHERE BandId =' + CONVERT(nvarchar(10), @RecordId)
	EXEC @Parameters = dbo.SerializeJSON @Parameters
	SET @JSON = REPLACE(@JSON, '"$Parameters$"', @Parameters)

	DECLARE @Unit nvarchar(max)
	SET @Unit = 'SELECT u.Id, u.Name, dbo.GetUnitLocation(u.Id, default, default, default) As Location FROM	dbo.Bands b JOIN dbo.Units u ON b.UnitId = u.Id WHERE b.Id = ' + CONVERT(nvarchar(10), @RecordId)
	EXEC @Unit = dbo.SerializeJSON @Unit
	SET @JSON = REPLACE(@JSON, '"$Unit$"', @Unit)

	RETURN @JSON
END