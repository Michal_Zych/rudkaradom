﻿-- =============================================
-- Author:		pi
-- Create date: 12-10-2012
-- Description: Create basket name
-- =============================================
CREATE FUNCTION [dbo].[CreateAndGetBasketName]
(
	-- Add the parameters for the function here
	@userId int
)
RETURNS nvarchar(50)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @resultVar nvarchar(50)
	
	SELECT @resultVar = Name + ' ' + LastName + ' ' + '(' + [Login] + ')'
	FROM Users
	WHERE Id = @userId

	RETURN @resultVar

END

