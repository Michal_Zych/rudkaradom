﻿
CREATE FUNCTION [dbo].[GetWardIdForUnit]
(
	@UnitId int
)
RETURNS int
AS
BEGIN
	DECLARE
		@WARD_UNIT_TYPE int = 29

	DECLARE @unitTypeIdv2 int
		,@parentId int
		,@wardId int

	SELECT @unitTypeIdV2 = UnitTypeIdV2, @parentId = ParentUnitID FROM dbo.Units WHERE Id = @UnitId
	IF @unitTypeIdv2 = @WARD_UNIT_TYPE SELECT @wardId = @UnitId


	WHILE @parentId IS NOT NULL AND @wardId IS NULL
	BEGIN
		SELECT @UnitId = @parentId
		SELECT @unitTypeIdV2 = UnitTypeIdV2, @parentId = ParentUnitID FROM dbo.Units WHERE Id = @UnitId
		IF @unitTypeIdv2 = @WARD_UNIT_TYPE SELECT @wardId = @UnitId
	END

	RETURN @wardId
END