﻿
CREATE FUNCTION [dbo].[SplitTo2colums](
	@text nvarchar(max)
	, @delimiter char(1) = ','
	, @delimiterReplacment nvarchar(10) = '&comma;'
)
RETURNS @RESULT TABLE ( 
	A nvarchar(max),	
	B nvarchar(max) 
)
AS 
BEGIN 
	DECLARE @tempTable TABLE(Id int, Value nvarchar(max))

	INSERT @tempTable SELECT * FROM dbo.fn_SplitWithEmpty(@text, @delimiter)

	INSERT @RESULT
	SELECT nr.Value, REPLACE(ds.value, @delimiterReplacment, @delimiter)
	FROM @tempTable nr
	LEFT JOIN @tempTable ds ON nr.id = ds.id - 1
	WHERE nr.id % 2 = 0

	RETURN
END


