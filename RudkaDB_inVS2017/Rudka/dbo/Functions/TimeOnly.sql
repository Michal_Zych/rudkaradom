﻿create function [dbo].[TimeOnly](@DateTime DateTime)
returns datetime
as
begin
	return dateadd(day, -datediff(day, 0, @datetime), @datetime)
end

