﻿CREATE FUNCTION [dbo].[GetBandConfigurationString] 
(
	@BandId smallint
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @SEPARATOR char = ','
		,@SEPARATOR_ESCAPE nvarchar(10) = '&comma;'
		,@NULL_STRING nvarchar(10) = '';
	DECLARE @a nvarchar(max) = ''
		
	SELECT @a = @a + CONVERT(nvarchar(10), BandId) + @SEPARATOR 
		+ COALESCE(REPLACE([Key], @SEPARATOR, @SEPARATOR_ESCAPE), @NULL_STRING) + @SEPARATOR
		+ COALESCE(REPLACE([Description], @SEPARATOR, @SEPARATOR_ESCAPE), @NULL_STRING) + @SEPARATOR
		+ COALESCE(REPLACE([Value], @SEPARATOR, @SEPARATOR_ESCAPE), @NULL_STRING) + @SEPARATOR
	FROM dbo.BandSettings
	WHERE BandId = @BandId

	RETURN @a
END