﻿

-- =============================================
-- Author:		
-- Create date: 
-- Description:	Dla testów funkcja zwarca jako aktualny czas wartość CurrentTime z tabeli _TestCurrentTime
--				Jeśli nie ma takiej tabeli lub kolumn CurrentTime zawiera null - zwaraca aktualną dategodzinę
-- =============================================
CREATE FUNCTION [dbo].[GetCurrentUTCDate] 
(
)
RETURNS datetime
AS
BEGIN
	DECLARE @TEST_MODE bit = 0

	IF @TEST_MODE = 0 RETURN GETUtcDATE()

	DECLARE @currentDate datetime

	IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = '_TestCurrentTime'))
	BEGIN
		SELECT TOP 1 @currentDate = CurrentTime FROM dbo._TestCurrentTime
	END

	SET @currentDate = COALESCE(@currentDate, GETUtcDATE())

	RETURN @currentDate
END




