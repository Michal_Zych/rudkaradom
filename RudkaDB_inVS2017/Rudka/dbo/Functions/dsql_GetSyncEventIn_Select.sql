﻿CREATE FUNCTION dsql_GetSyncEventIn_Select
()
RETURNS nvarchar(max)
AS
BEGIN
 RETURN
'e.Id as eEventId
      ,e.StoreDateUtc as eStoreDateUtc
      ,e.EventDateUtc as eEventDateUtc
      ,e.EventType as eEventType
      ,e.EventRecordId as eEventRecordId
      ,e.Recordas eRecord
      ,e.Error as eError
  '
END