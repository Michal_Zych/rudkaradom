﻿


CREATE FUNCTION [dbo].[SerializeJSON_BandDescription]
(
	@RecordId int
)
RETURNS nvarchar(max)
AS
BEGIN

	DECLARE @JSON nvarchar(max)

	SET @JSON ='SELECT ''$Band$'' AS [Band]'
	EXEC @JSON = dbo.SerializeJSON @JSON

	DECLARE @Band nvarchar(max)
	SET @Band = 'SELECT Id, MacString AS MAC, Uid, BarCode FROM dbo.Bands WHERE Id = ' + COALESCE(CONVERT(nvarchar(10), @RecordId), 'NULL')
	EXEC @Band = dbo.SerializeJSON @Band
	SET @JSON = REPLACE(@JSON, '"$Band$"', @Band)

	RETURN @JSON
END