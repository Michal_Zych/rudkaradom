﻿CREATE FUNCTION [dbo].[GetAlarmNotifyText]
(
	@AlarmID int,
	@Type int
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE			--typy powiadomień
		  @GUI			tinyint = 0
		, @SMS			tinyint = 1
		, @EMAIL		tinyint = 2

		,@VALUE_TO_LOW tinyint = 1
		,@VALUE_TO_HIGH tinyint = 2
		,@NO_COMMUNICATION tinyint = 3
		,@NO_SENSOR tinyint = 3
		,@NO_DEVICE tinyint = 4
		,@NO_HUB tinyint = 5

	--ALARM RCKiK Białystok->Oddział Suwałki->Magazyn krwi->Zamrażarka 1: zakres góra powietrze.
	DECLARE 
		@alarmText nvarchar(500)
		,@eventType tinyint
		,@location nvarchar(200)
		,@name nvarchar(160)

	

	SELECT @eventType = EventType
		, @location = dbo.GetUnitLocation(s.UnitId, 1, default, default) 
		, @name = up.Name + '>' + u.Name
	FROM dbo.Alarms a WITH(NOLOCK)
	JOIN dbo.Sensors s WITH(NOLOCK) ON a.SensorID = s.Id
	JOIN dbo.Units u WITH(NOLOCK) ON s.UnitId = u.ID
	JOIN dbo.Units up WITH(NOLOCK) ON up.ID = u.ParentUnitID
	WHERE a.ID = @AlarmID

	
	SET @alarmText = 'ALARM ' 
		+ CASE
			WHEN @Type = @SMS THEN @name
			ELSE @location
		  END
	+ ': ' 
		+ CASE
			WHEN @eventType = @VALUE_TO_LOW THEN 'zakres dół'
			WHEN @eventType = @VALUE_TO_HIGH THEN 'zakres góra'
			ELSE 'brak łączności'
		 END

	if @Type = @SMS SET @alarmText = LEFT(dbo.ReplacePolishSigns(@alarmText), 160)
	
	RETURN @alarmText
END

