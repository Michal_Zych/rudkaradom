﻿CREATE FUNCTION [dbo].[GetObjectName]
(
	@ObjectId dbo.OBJECT_ID
	,@ObjectType dbo.OBJECT_TYPE
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @name nvarchar(max)

	IF @ObjectType = (SELECT PatientAlarmConfiguration FROM enum.ObjectType)
	BEGIN
		SELECT @ObjectType = (SELECT TOP 1 Patient FROM enum.ObjectType)
	END
	ELSE IF @ObjectType = (SELECT UnitAlarmConfiguration FROM enum.ObjectType)
	BEGIN
		SELECT @ObjectType = (SELECT TOP 1 Unit FROM enum.ObjectType)
	END

	IF @ObjectType = (SELECT Patient FROM enum.ObjectType)
	BEGIN
		SELECT @name = COALESCE(Name + ' ', '') + COALESCE(LastName, '')
		FROM dbo.Patients
		WHERE Id = @ObjectId
		
		IF COALESCE(@name , '') = ''
			SELECT @name = COALESCE(Name + ' ', '') + COALESCE(LastName, '')
			FROM arch.Patients
			WHERE Id = @ObjectId
	END
	ELSE IF @ObjectType = (SELECT [User] FROM enum.ObjectType)
		SELECT @name = COALESCE(Name + ' ', '') + COALESCE(LastName, '')
		FROM dbo.Users
		WHERE Id = @ObjectId
	ELSE IF @ObjectType = (SELECT Band FROM enum.ObjectType)
		SELECT @name = MacString
		FROM dbo.Bands
		WHERE Id = @ObjectId
	ELSE IF @ObjectType = (SELECT Unit FROM enum.ObjectType)
		SELECT @name = Name
		FROM dbo.Units
		WHERE Id = @ObjectId
	ELSE
		SELECT @name = CONVERT(nvarchar(10), @ObjectId)


	RETURN @name
END