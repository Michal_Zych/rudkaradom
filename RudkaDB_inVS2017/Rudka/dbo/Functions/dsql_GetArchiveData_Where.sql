﻿CREATE FUNCTION dsql_GetArchiveData_Where
(
@TreeUnitIds nvarchar(max),
@EventIdFrom int = null
,@EventIdTo int = null
,@PatientIdFrom int = null
,@PatientIdTo int = null
,@Name nvarchar(200) = null
,@LastName nvarchar(200) = null
,@DisplayName nvarchar(200) = null
,@Pesel char(200) = null
,@DateUtcFrom datetime = null
,@DateUtcTo datetime = null
,@TypeFrom tinyint = null
,@TypeTo tinyint = null
,@TypeDescription nvarchar(200) = null
,@SeverityFrom tinyint = null
,@SeverityTo tinyint = null
,@SeverityDescription nvarchar(200) = null
,@UnitNameFrom int = null
,@UnitNameTo int = null
,@UnitLocation nvarchar(200) = null
,@ObjectId nvarchar(200) = null
,@ObjectTypeFrom int = null
,@ObjectTypeTo int = null
,@ObjectTypeDescriptionFrom tinyint = null
,@ObjectTypeDescriptionTo tinyint = null
,@ObjectName nvarchar(200) = null
,@ObjectUnitId nvarchar(200) = null
,@ObjectUnitNameFrom int = null
,@ObjectUnitNameTo int = null
,@ObjectUnitLocation nvarchar(200) = null
,@EndDateUtc nvarchar(200) = null
,@EndingEventIdFrom datetime = null
,@EndingEventIdTo datetime = null
,@OperatorIdFrom int = null
,@OperatorIdTo int = null
,@OperatorNameFrom int = null
,@OperatorNameTo int = null
,@OperatorLastName nvarchar(200) = null
,@EventData nvarchar(200) = null
,@Reason nvarchar(200) = null
,@RaisingEventId nvarchar(200) = null
,@IsClosedFrom int = null
,@IsClosedTo int = null
,@ClosingDateUtcFrom bit = null
,@ClosingDateUtcTo bit = null
,@ClosingUserIdFrom datetime = null
,@ClosingUserIdTo datetime = null
,@ClosingUserNameFrom int = null
,@ClosingUserNameTo int = null
,@ClosingUserLastName nvarchar(200) = null
,@ClosingAlarmId nvarchar(200) = null
,@ClosingCommentFrom int = null
,@ClosingCommentTo int = null
,@UserMessageId nvarchar(200) = null
,@SenderIdFrom int = null
,@SenderIdTo int = null
,@SenderNameFrom int = null
,@SenderNameTo int = null
,@SenderLastName nvarchar(200) = null
,@Message nvarchar(200) = null
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @useMatchFunction bit = 1
 DECLARE @r nvarchar(max) = ' WHERE ' + 'e.ObjectType = 4 AND '

-- IF COALESCE(@TreeUnitIds, '') <> ''
--   SET @r = @r + 'UnitId IN(''' + @TreeUnitIds + ''') AND '

 IF @EventIdFrom IS NOT NULL AND @EventIdTo IS NOT NULL
  SET @r = @r + '(e.Id  BETWEEN ' + CONVERT(nvarchar(20), @EventIdFrom) + ' AND ' + CONVERT(nvarchar(20), @EventIdTo) + ') AND '
 ELSE IF @EventIdFrom IS NOT NULL AND @EventIdTo IS NULL
  SET @r = @r + 'e.Id  = ' + CONVERT(nvarchar(20), @EventIdFrom) + ' AND '
 ELSE IF @EventIdFrom IS NOT NULL AND @EventIdTo IS NOT NULL
  SET @r = @r + 'e.Id  IS NULL AND '

 IF @PatientIdFrom IS NOT NULL AND @PatientIdTo IS NOT NULL
  SET @r = @r + '(patients.Id  BETWEEN ' + CONVERT(nvarchar(20), @PatientIdFrom) + ' AND ' + CONVERT(nvarchar(20), @PatientIdTo) + ') AND '
 ELSE IF @PatientIdFrom IS NOT NULL AND @PatientIdTo IS NULL
  SET @r = @r + 'patients.Id  = ' + CONVERT(nvarchar(20), @PatientIdFrom) + ' AND '
 ELSE IF @PatientIdFrom IS NOT NULL AND @PatientIdTo IS NOT NULL
  SET @r = @r + 'patients.Id  IS NULL AND '

 IF @Name IS NOT NULL
  IF @Name = '?'
   SET @r = @r + 'COALESCE(patients.Name , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(patients.Name , ''' + @Name + ''') > 0 AND '
    ELSE
     SET @r = @r + 'patients.Name  LIKE ''' + @Name + ''' AND '
  END

 IF @LastName IS NOT NULL
  IF @LastName = '?'
   SET @r = @r + 'COALESCE(patients.LastName , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(patients.LastName , ''' + @LastName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'patients.LastName  LIKE ''' + @LastName + ''' AND '
  END

 IF @DisplayName IS NOT NULL
  IF @DisplayName = '?'
   SET @r = @r + 'COALESCE(dbo.GetDisplayName(patients.Id, objectType.Patient) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(dbo.GetDisplayName(patients.Id, objectType.Patient) , ''' + @DisplayName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'dbo.GetDisplayName(patients.Id, objectType.Patient)  LIKE ''' + @DisplayName + ''' AND '
  END

 IF @Pesel IS NOT NULL
  IF @Pesel = '?'
   SET @r = @r + 'COALESCE(patients.Pesel , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(patients.Pesel , ''' + @Pesel + ''') > 0 AND '
    ELSE
     SET @r = @r + 'patients.Pesel  LIKE ''' + @Pesel + ''' AND '
  END

 IF @DateUtcFrom IS NOT NULL
  SET @r = @r + 'e.DateUtc  >=''' + CONVERT(nvarchar(30), @DateUtcFrom, 121) + ''' AND '
 IF @DateUtcTo IS NOT NULL
  SET @r = @r + 'e.DateUtc  <=''' + CONVERT(nvarchar(30), @DateUtcTo, 121) + ''' AND '

 IF @TypeFrom IS NOT NULL AND @TypeTo IS NOT NULL
  SET @r = @r + '(e.Type  BETWEEN ' + CONVERT(nvarchar(20), @TypeFrom) + ' AND ' + CONVERT(nvarchar(20), @TypeTo) + ') AND '
 ELSE IF @TypeFrom IS NOT NULL AND @TypeTo IS NULL
  SET @r = @r + 'e.Type  = ' + CONVERT(nvarchar(20), @TypeFrom) + ' AND '
 ELSE IF @TypeFrom IS NOT NULL AND @TypeTo IS NOT NULL
  SET @r = @r + 'e.Type  IS NULL AND '

 IF @TypeDescription IS NOT NULL
  IF @TypeDescription = '?'
   SET @r = @r + 'COALESCE(typeEnum.Description , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(typeEnum.Description , ''' + @TypeDescription + ''') > 0 AND '
    ELSE
     SET @r = @r + 'typeEnum.Description  LIKE ''' + @TypeDescription + ''' AND '
  END

 IF @SeverityFrom IS NOT NULL AND @SeverityTo IS NOT NULL
  SET @r = @r + '(e.Severity  BETWEEN ' + CONVERT(nvarchar(20), @SeverityFrom) + ' AND ' + CONVERT(nvarchar(20), @SeverityTo) + ') AND '
 ELSE IF @SeverityFrom IS NOT NULL AND @SeverityTo IS NULL
  SET @r = @r + 'e.Severity  = ' + CONVERT(nvarchar(20), @SeverityFrom) + ' AND '
 ELSE IF @SeverityFrom IS NOT NULL AND @SeverityTo IS NOT NULL
  SET @r = @r + 'e.Severity  IS NULL AND '

 IF @SeverityDescription IS NOT NULL
  IF @SeverityDescription = '?'
   SET @r = @r + 'COALESCE( severityEnum.Description , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( severityEnum.Description , ''' + @SeverityDescription + ''') > 0 AND '
    ELSE
     SET @r = @r + ' severityEnum.Description  LIKE ''' + @SeverityDescription + ''' AND '
  END

 IF @UnitNameFrom IS NOT NULL AND @UnitNameTo IS NOT NULL
  SET @r = @r + '( e.UnitId, u.Name  BETWEEN ' + CONVERT(nvarchar(20), @UnitNameFrom) + ' AND ' + CONVERT(nvarchar(20), @UnitNameTo) + ') AND '
 ELSE IF @UnitNameFrom IS NOT NULL AND @UnitNameTo IS NULL
  SET @r = @r + ' e.UnitId, u.Name  = ' + CONVERT(nvarchar(20), @UnitNameFrom) + ' AND '
 ELSE IF @UnitNameFrom IS NOT NULL AND @UnitNameTo IS NOT NULL
  SET @r = @r + ' e.UnitId, u.Name  IS NULL AND '

 IF @UnitLocation IS NOT NULL
  IF @UnitLocation = '?'
   SET @r = @r + 'COALESCE( dbo.GetUnitLocation(e.UnitId, DEFAULT, DEFAULT, DEFAULT) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( dbo.GetUnitLocation(e.UnitId, DEFAULT, DEFAULT, DEFAULT) , ''' + @UnitLocation + ''') > 0 AND '
    ELSE
     SET @r = @r + ' dbo.GetUnitLocation(e.UnitId, DEFAULT, DEFAULT, DEFAULT)  LIKE ''' + @UnitLocation + ''' AND '
  END

 IF @ObjectId IS NOT NULL
  IF @ObjectId = '?'
   SET @r = @r + 'COALESCE( e.ObjectId , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( e.ObjectId , ''' + @ObjectId + ''') > 0 AND '
    ELSE
     SET @r = @r + ' e.ObjectId  LIKE ''' + @ObjectId + ''' AND '
  END

 IF @ObjectTypeFrom IS NOT NULL AND @ObjectTypeTo IS NOT NULL
  SET @r = @r + '( e.ObjectType  BETWEEN ' + CONVERT(nvarchar(20), @ObjectTypeFrom) + ' AND ' + CONVERT(nvarchar(20), @ObjectTypeTo) + ') AND '
 ELSE IF @ObjectTypeFrom IS NOT NULL AND @ObjectTypeTo IS NULL
  SET @r = @r + ' e.ObjectType  = ' + CONVERT(nvarchar(20), @ObjectTypeFrom) + ' AND '
 ELSE IF @ObjectTypeFrom IS NOT NULL AND @ObjectTypeTo IS NOT NULL
  SET @r = @r + ' e.ObjectType  IS NULL AND '

 IF @ObjectTypeDescriptionFrom IS NOT NULL AND @ObjectTypeDescriptionTo IS NOT NULL
  SET @r = @r + '( objectTypeEnum.Description  BETWEEN ' + CONVERT(nvarchar(20), @ObjectTypeDescriptionFrom) + ' AND ' + CONVERT(nvarchar(20), @ObjectTypeDescriptionTo) + ') AND '
 ELSE IF @ObjectTypeDescriptionFrom IS NOT NULL AND @ObjectTypeDescriptionTo IS NULL
  SET @r = @r + ' objectTypeEnum.Description  = ' + CONVERT(nvarchar(20), @ObjectTypeDescriptionFrom) + ' AND '
 ELSE IF @ObjectTypeDescriptionFrom IS NOT NULL AND @ObjectTypeDescriptionTo IS NOT NULL
  SET @r = @r + ' objectTypeEnum.Description  IS NULL AND '

 IF @ObjectName IS NOT NULL
  IF @ObjectName = '?'
   SET @r = @r + 'COALESCE( dbo.GetDisplayName(e.ObjectId, e.ObjectType) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( dbo.GetDisplayName(e.ObjectId, e.ObjectType) , ''' + @ObjectName + ''') > 0 AND '
    ELSE
     SET @r = @r + ' dbo.GetDisplayName(e.ObjectId, e.ObjectType)  LIKE ''' + @ObjectName + ''' AND '
  END

 IF @ObjectUnitId IS NOT NULL
  IF @ObjectUnitId = '?'
   SET @r = @r + 'COALESCE( e.ObjectUnitId , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( e.ObjectUnitId , ''' + @ObjectUnitId + ''') > 0 AND '
    ELSE
     SET @r = @r + ' e.ObjectUnitId  LIKE ''' + @ObjectUnitId + ''' AND '
  END

 IF @ObjectUnitNameFrom IS NOT NULL AND @ObjectUnitNameTo IS NOT NULL
  SET @r = @r + '( uo.Name  BETWEEN ' + CONVERT(nvarchar(20), @ObjectUnitNameFrom) + ' AND ' + CONVERT(nvarchar(20), @ObjectUnitNameTo) + ') AND '
 ELSE IF @ObjectUnitNameFrom IS NOT NULL AND @ObjectUnitNameTo IS NULL
  SET @r = @r + ' uo.Name  = ' + CONVERT(nvarchar(20), @ObjectUnitNameFrom) + ' AND '
 ELSE IF @ObjectUnitNameFrom IS NOT NULL AND @ObjectUnitNameTo IS NOT NULL
  SET @r = @r + ' uo.Name  IS NULL AND '

 IF @ObjectUnitLocation IS NOT NULL
  IF @ObjectUnitLocation = '?'
   SET @r = @r + 'COALESCE( dbo.GetUnitLocation(e.ObjectUnitId, DEFAULT, DEFAULT, DEFAULT) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( dbo.GetUnitLocation(e.ObjectUnitId, DEFAULT, DEFAULT, DEFAULT) , ''' + @ObjectUnitLocation + ''') > 0 AND '
    ELSE
     SET @r = @r + ' dbo.GetUnitLocation(e.ObjectUnitId, DEFAULT, DEFAULT, DEFAULT)  LIKE ''' + @ObjectUnitLocation + ''' AND '
  END

 IF @EndDateUtc IS NOT NULL
  IF @EndDateUtc = '?'
   SET @r = @r + 'COALESCE( e.EndDateUtc , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( e.EndDateUtc , ''' + @EndDateUtc + ''') > 0 AND '
    ELSE
     SET @r = @r + ' e.EndDateUtc  LIKE ''' + @EndDateUtc + ''' AND '
  END

 IF @EndingEventIdFrom IS NOT NULL
  SET @r = @r + ' e.EndingEventId  >=''' + CONVERT(nvarchar(30), @EndingEventIdFrom, 121) + ''' AND '
 IF @EndingEventIdTo IS NOT NULL
  SET @r = @r + ' e.EndingEventId  <=''' + CONVERT(nvarchar(30), @EndingEventIdTo, 121) + ''' AND '

 IF @OperatorIdFrom IS NOT NULL AND @OperatorIdTo IS NOT NULL
  SET @r = @r + '( e.OperatorId  BETWEEN ' + CONVERT(nvarchar(20), @OperatorIdFrom) + ' AND ' + CONVERT(nvarchar(20), @OperatorIdTo) + ') AND '
 ELSE IF @OperatorIdFrom IS NOT NULL AND @OperatorIdTo IS NULL
  SET @r = @r + ' e.OperatorId  = ' + CONVERT(nvarchar(20), @OperatorIdFrom) + ' AND '
 ELSE IF @OperatorIdFrom IS NOT NULL AND @OperatorIdTo IS NOT NULL
  SET @r = @r + ' e.OperatorId  IS NULL AND '

 IF @OperatorNameFrom IS NOT NULL AND @OperatorNameTo IS NOT NULL
  SET @r = @r + '( usr.Name  BETWEEN ' + CONVERT(nvarchar(20), @OperatorNameFrom) + ' AND ' + CONVERT(nvarchar(20), @OperatorNameTo) + ') AND '
 ELSE IF @OperatorNameFrom IS NOT NULL AND @OperatorNameTo IS NULL
  SET @r = @r + ' usr.Name  = ' + CONVERT(nvarchar(20), @OperatorNameFrom) + ' AND '
 ELSE IF @OperatorNameFrom IS NOT NULL AND @OperatorNameTo IS NOT NULL
  SET @r = @r + ' usr.Name  IS NULL AND '

 IF @OperatorLastName IS NOT NULL
  IF @OperatorLastName = '?'
   SET @r = @r + 'COALESCE( usr.LastName , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( usr.LastName , ''' + @OperatorLastName + ''') > 0 AND '
    ELSE
     SET @r = @r + ' usr.LastName  LIKE ''' + @OperatorLastName + ''' AND '
  END

 IF @EventData IS NOT NULL
  IF @EventData = '?'
   SET @r = @r + 'COALESCE( e.EventData , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( e.EventData , ''' + @EventData + ''') > 0 AND '
    ELSE
     SET @r = @r + ' e.EventData  LIKE ''' + @EventData + ''' AND '
  END

 IF @Reason IS NOT NULL
  IF @Reason = '?'
   SET @r = @r + 'COALESCE( e.Reason , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( e.Reason , ''' + @Reason + ''') > 0 AND '
    ELSE
     SET @r = @r + ' e.Reason  LIKE ''' + @Reason + ''' AND '
  END

 IF @RaisingEventId IS NOT NULL
  IF @RaisingEventId = '?'
   SET @r = @r + 'COALESCE( a.RaisingEventId , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( a.RaisingEventId , ''' + @RaisingEventId + ''') > 0 AND '
    ELSE
     SET @r = @r + ' a.RaisingEventId  LIKE ''' + @RaisingEventId + ''' AND '
  END

 IF @IsClosedFrom IS NOT NULL AND @IsClosedTo IS NOT NULL
  SET @r = @r + '( a.IsClosed  BETWEEN ' + CONVERT(nvarchar(20), @IsClosedFrom) + ' AND ' + CONVERT(nvarchar(20), @IsClosedTo) + ') AND '
 ELSE IF @IsClosedFrom IS NOT NULL AND @IsClosedTo IS NULL
  SET @r = @r + ' a.IsClosed  = ' + CONVERT(nvarchar(20), @IsClosedFrom) + ' AND '
 ELSE IF @IsClosedFrom IS NOT NULL AND @IsClosedTo IS NOT NULL
  SET @r = @r + ' a.IsClosed  IS NULL AND '

 IF @ClosingDateUtcFrom IS NOT NULL AND @ClosingDateUtcTo IS NOT NULL
  SET @r = @r + '( a.ClosingDateUtc  BETWEEN ' + CONVERT(nvarchar(20), @ClosingDateUtcFrom) + ' AND ' + CONVERT(nvarchar(20), @ClosingDateUtcTo) + ') AND '
 ELSE IF @ClosingDateUtcFrom IS NOT NULL AND @ClosingDateUtcTo IS NULL
  SET @r = @r + ' a.ClosingDateUtc  = ' + CONVERT(nvarchar(20), @ClosingDateUtcFrom) + ' AND '
 ELSE IF @ClosingDateUtcFrom IS NOT NULL AND @ClosingDateUtcTo IS NOT NULL
  SET @r = @r + ' a.ClosingDateUtc  IS NULL AND '

 IF @ClosingUserIdFrom IS NOT NULL
  SET @r = @r + ' a.ClosingUserId  >=''' + CONVERT(nvarchar(30), @ClosingUserIdFrom, 121) + ''' AND '
 IF @ClosingUserIdTo IS NOT NULL
  SET @r = @r + ' a.ClosingUserId  <=''' + CONVERT(nvarchar(30), @ClosingUserIdTo, 121) + ''' AND '

 IF @ClosingUserNameFrom IS NOT NULL AND @ClosingUserNameTo IS NOT NULL
  SET @r = @r + '( ausr.Name  BETWEEN ' + CONVERT(nvarchar(20), @ClosingUserNameFrom) + ' AND ' + CONVERT(nvarchar(20), @ClosingUserNameTo) + ') AND '
 ELSE IF @ClosingUserNameFrom IS NOT NULL AND @ClosingUserNameTo IS NULL
  SET @r = @r + ' ausr.Name  = ' + CONVERT(nvarchar(20), @ClosingUserNameFrom) + ' AND '
 ELSE IF @ClosingUserNameFrom IS NOT NULL AND @ClosingUserNameTo IS NOT NULL
  SET @r = @r + ' ausr.Name  IS NULL AND '

 IF @ClosingUserLastName IS NOT NULL
  IF @ClosingUserLastName = '?'
   SET @r = @r + 'COALESCE( ausr.LastName , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( ausr.LastName , ''' + @ClosingUserLastName + ''') > 0 AND '
    ELSE
     SET @r = @r + ' ausr.LastName  LIKE ''' + @ClosingUserLastName + ''' AND '
  END

 IF @ClosingAlarmId IS NOT NULL
  IF @ClosingAlarmId = '?'
   SET @r = @r + 'COALESCE( a.ClosingAlarmId , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( a.ClosingAlarmId , ''' + @ClosingAlarmId + ''') > 0 AND '
    ELSE
     SET @r = @r + ' a.ClosingAlarmId  LIKE ''' + @ClosingAlarmId + ''' AND '
  END

 IF @ClosingCommentFrom IS NOT NULL AND @ClosingCommentTo IS NOT NULL
  SET @r = @r + '( a.ClosingComment  BETWEEN ' + CONVERT(nvarchar(20), @ClosingCommentFrom) + ' AND ' + CONVERT(nvarchar(20), @ClosingCommentTo) + ') AND '
 ELSE IF @ClosingCommentFrom IS NOT NULL AND @ClosingCommentTo IS NULL
  SET @r = @r + ' a.ClosingComment  = ' + CONVERT(nvarchar(20), @ClosingCommentFrom) + ' AND '
 ELSE IF @ClosingCommentFrom IS NOT NULL AND @ClosingCommentTo IS NOT NULL
  SET @r = @r + ' a.ClosingComment  IS NULL AND '

 IF @UserMessageId IS NOT NULL
  IF @UserMessageId = '?'
   SET @r = @r + 'COALESCE( ua.UserMessageId , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( ua.UserMessageId , ''' + @UserMessageId + ''') > 0 AND '
    ELSE
     SET @r = @r + ' ua.UserMessageId  LIKE ''' + @UserMessageId + ''' AND '
  END

 IF @SenderIdFrom IS NOT NULL AND @SenderIdTo IS NOT NULL
  SET @r = @r + '( ua.UserId  BETWEEN ' + CONVERT(nvarchar(20), @SenderIdFrom) + ' AND ' + CONVERT(nvarchar(20), @SenderIdTo) + ') AND '
 ELSE IF @SenderIdFrom IS NOT NULL AND @SenderIdTo IS NULL
  SET @r = @r + ' ua.UserId  = ' + CONVERT(nvarchar(20), @SenderIdFrom) + ' AND '
 ELSE IF @SenderIdFrom IS NOT NULL AND @SenderIdTo IS NOT NULL
  SET @r = @r + ' ua.UserId  IS NULL AND '

 IF @SenderNameFrom IS NOT NULL AND @SenderNameTo IS NOT NULL
  SET @r = @r + '( ausr.Name  BETWEEN ' + CONVERT(nvarchar(20), @SenderNameFrom) + ' AND ' + CONVERT(nvarchar(20), @SenderNameTo) + ') AND '
 ELSE IF @SenderNameFrom IS NOT NULL AND @SenderNameTo IS NULL
  SET @r = @r + ' ausr.Name  = ' + CONVERT(nvarchar(20), @SenderNameFrom) + ' AND '
 ELSE IF @SenderNameFrom IS NOT NULL AND @SenderNameTo IS NOT NULL
  SET @r = @r + ' ausr.Name  IS NULL AND '

 IF @SenderLastName IS NOT NULL
  IF @SenderLastName = '?'
   SET @r = @r + 'COALESCE( ausr.LastName , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( ausr.LastName , ''' + @SenderLastName + ''') > 0 AND '
    ELSE
     SET @r = @r + ' ausr.LastName  LIKE ''' + @SenderLastName + ''' AND '
  END

 IF @Message IS NOT NULL
  IF @Message = '?'
   SET @r = @r + 'COALESCE( ua.Message , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( ua.Message , ''' + @Message + ''') > 0 AND '
    ELSE
     SET @r = @r + ' ua.Message  LIKE ''' + @Message + ''' AND '
  END

 SET @r = @r + ' 1=1'
 RETURN @r
END