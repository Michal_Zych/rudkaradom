﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[IsTransmitterConnected](@TransmitterId dbo.[OBJECT_ID])
RETURNS bit
AS
BEGIN
	DECLARE @connected bit
	
	SELECT @connected = COALESCE(CONVERT(bit, OngConnectedEventId), 0)
	FROM dbo.ev_TransmitterStatus 
	WHERE TransmitterId = @TransmitterId

	RETURN COALESCE(@connected, 1)
END