﻿CREATE FUNCTION dsql_GetSystemEvent_Select
()
RETURNS nvarchar(max)
AS
BEGIN
 RETURN
'e.Id AS Id
, e.DateUtc AS DateUtc
, e.Type AS Type
, eventType.Description AS TypeDescription
, e.UnitId AS UnitId
, dbo.GetUnitLocation(e.UnitId, 1, 1, default) AS UnitLocation
, e.ObjectId AS ObjectId
, e.ObjectType AS ObjectType
, objectType.Description AS ObjectTypeDescription
, dbo.GetObjectName(e.ObjectId, e.ObjectType) AS ObjectName
, e.OperatorId AS OperatorId
, COALESCE(usr.LastName, '''') + COALESCE('' '' + usr.Name, '''') AS OperatorName
, CONVERT(nvarchar(200), e.[EventData]) as [EventData]
, CONVERT(nvarchar(200), e.Reason) AS Reason
'
END