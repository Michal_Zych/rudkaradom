﻿CREATE FUNCTION [dbo].[GetCallingGroupString] 
(
	@ObjectType dbo.OBJECT_TYPE
	,@ObjectId dbo.OBJECT_ID
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @SEPARATOR char = ','
		,@SEPARATOR_ESCAPE nvarchar(10) = '&comma;'
		,@NULL_STRING nvarchar(10) = '';
	DECLARE @a nvarchar(max) = ''
		
	SELECT @a = @a + CONVERT(nvarchar(10), SequenceNr) + @SEPARATOR 
		+ COALESCE(REPLACE([Phone], @SEPARATOR, @SEPARATOR_ESCAPE), @NULL_STRING) + @SEPARATOR
		+ COALESCE(REPLACE([Description], @SEPARATOR, @SEPARATOR_ESCAPE), @NULL_STRING) + @SEPARATOR
	FROM dbo.al_CallingGroups
	WHERE Id = @ObjectId AND ObjectType	= @ObjectType

	RETURN @a
END