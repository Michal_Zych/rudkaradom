﻿
CREATE FUNCTION [dbo].[GetSensorHardwarePath]
(
	@SensorId smallint,
	@MessageType int
)
RETURNS nvarchar(160)
AS
BEGIN
	DECLARE 
		--typy powiadomień
		 @GUI			tinyint = 0
		, @SMS			tinyint = 1
		, @EMAIL		tinyint = 2

	DECLARE @ipPrefix nvarchar(50) = ''
		,@addressPrefix nvarchar(50) = '/'
		,@channelPrefix nvarchar(50) = '/'
		,@message nvarchar(200)

	IF(@MessageType = @EMAIL) SELECT @ipPrefix = 'adres IP: ', @addressPrefix = ' / adres urządzenia:', @channelPrefix = ' / numer kanału:'

	SELECT @message = @ipPrefix + h.IPAddress 
		+ @addressPrefix + CONVERT(nvarchar(5), d.Address)
		+ @channelPrefix + CONVERT(nvarchar(5),so.DeviceChannelNumber)
	FROM dbo.Sensors s WITH(NOLOCK)
		JOIN dbo.Sockets so WITH(NOLOCK) ON s.SocketID = so.Id
		JOIN dbo.Devices d WITH(NOLOCK) ON d.id = so.DeviceId
		JOIN dbo.Hubs h WITH(NOLOCK) ON h.Id = d.HubId
	WHERE s.Id = @SensorId

	IF(@MessageType = @SMS)
		SET @message = dbo.ReplacePolishSigns(@message)

	RETURN @message
END
