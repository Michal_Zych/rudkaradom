﻿

CREATE FUNCTION [dbo].[GetScaleForSocket]
(
  @SocketID int
)
RETURNS real
AS
BEGIN
	DECLARE @MAX smallint = 32767
	,@MIN smallint = -32768

	,@loRange smallint
	,@hiRange smallint

	SELECT @loRange = LoRange, @hiRange = HiRange 
	FROM dbo.Sockets 
	WHERE Id = @SocketId

	DECLARE @s int = 1
		
	WHILE (@s * @loRange > @MIN) AND (@s * @hiRange < @MAX)
		SET @s = @s * 10

	RETURN 10.0 / @s
END