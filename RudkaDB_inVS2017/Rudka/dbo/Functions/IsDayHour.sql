﻿
CREATE FUNCTION dbo.IsDayHour 
(
	@time datetime
)
RETURNS bit
AS
BEGIN
	IF @time IS NULL SET @time = dbo.GetCurrentUtcDate()

	DECLARE
		 @isDay bit = 1
		,@timeOfNow time
		,@dayStartTime time
		,@nightStartTime time

	SELECT @timeOfNow = CONVERT(time, @time)
		,@dayStartTime = DayStartHour
		,@nightStartTime = DayEndHour
	FROM dbo.ev_Settings
	
	IF @timeOfNow < @dayStartTime OR @timeOfNow >= @nightStartTime SET @isDay = 0

	RETURN @isDay
END




