﻿CREATE FUNCTION dsql_GetServiceStatus_Select
()
RETURNS nvarchar(max)
AS
BEGIN
 RETURN
'ServiceName AS ServiceName
      ,Description AS Description
      ,StartTimeUtc AS StartTimeUtc
      ,PingTimeUtc AS PingTimeUtc
      ,PreviousPingTimeUtc AS PreviousPingTimeUtc
      ,HasPartner AS HasPartner
      ,PartnerPingTimeUtc AS PartnerPingTimeUtc
      ,InfoTxt AS InfoTxt
'
END