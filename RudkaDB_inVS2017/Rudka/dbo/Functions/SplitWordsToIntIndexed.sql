﻿CREATE FUNCTION [dbo].[SplitWordsToIntIndexed](@text IDs_LIST)
   RETURNS @ints TABLE (
      pos smallint primary key,
      value int,
      [index] smallint
   )
AS
BEGIN
   DECLARE
      @pos smallint,
      @i smallint,
      @j smallint,
      @s IDs_LIST,
      @index smallint
      
   SET @index = 1
   SET @pos = 1
   
   WHILE @pos <= LEN(@text) 
   BEGIN 
      SET @i = CHARINDEX(' ', @text, @pos)
      SET @j = CHARINDEX(',', @text, @pos)
      IF @i > 0 OR @j > 0
      BEGIN
         IF @i = 0 OR (@j > 0 AND @j < @i)
            SET @i = @j

         IF @i > @pos
         BEGIN
            -- @i now holds the earliest delimiter in the string
            SET @s = SUBSTRING(@text, @pos, @i - @pos)

            INSERT INTO @ints
            VALUES (@pos, convert(int,@s), @index)
            
            SET @index = @index + 1
         END 
         SET @pos = @i + 1

         WHILE @pos < LEN(@text) 
            AND SUBSTRING(@text, @pos, 1) IN (' ', ',')
            SET @pos = @pos + 1 
      END 
      ELSE 
      BEGIN 
         INSERT INTO @ints 
         VALUES (@pos, convert(int,SUBSTRING(@text, @pos, LEN(@text) - @pos + 1)), @index)

         SET @pos = LEN(@text) + 1 
         SET @index = @index + 1
      END 
   END 
   RETURN 
END

