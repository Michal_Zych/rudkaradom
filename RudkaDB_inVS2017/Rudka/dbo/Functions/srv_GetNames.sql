﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	funkcja zwraca wartość stałych  - do przerobienia na parametry z tabeli settings
-- =============================================
CREATE FUNCTION [dbo].[srv_GetNames]
(
	@clientId int
	,@name varchar(50)
)
RETURNS nvarchar(50)
AS
BEGIN
	DECLARE @result varchar(50)
	
	IF @clientId = 2
	BEGIN
		SET @result =
		CASE @name
			WHEN '@SERVICE1' THEN 'S_00_00'
			WHEN '@SERVICE2' THEN 'S_00_01'
			WHEN '@LOGDEVICE1' THEN 'L_00_00'
			WHEN '@LOGDEVICE2' THEN 'L_00_01'
		END
	END

	RETURN @result

END

