﻿CREATE FUNCTION dsql_GetExternalRecordsIn_Select
()
RETURNS nvarchar(max)
AS
BEGIN
 RETURN
'Type AS Type
,Id AS Id
,RequestId AS RequestId
,StoreDateUtc AS StoreDateUtc
,RecordType AS RecordType
,RecordId AS RecordId
,Record AS Record
,Processed AS Processed
,Try AS Try
,ProcessedDateUtc AS ProcessedDateUtc
,Error AS Error
'
END