﻿CREATE FUNCTION dsql_GetBands_Select
()
RETURNS nvarchar(max)
AS
BEGIN
 RETURN
'b.Id as Id
,b.BarCode as BarCode
,b.MacString as MacString
,b.Uid as Uid
,b.SerialNumber as SerialNumber
,bt.Id as BatteryTypeId
,bt.Name as BatteryTypeName
,COALESCE(e.Severity, CASE WHEN bs.OngLowBatteryEventId IS NOT NULL THEN 1 ELSE NULL END) as BatteryAlarmSeverityId
,es.Description as BatteryAlarmSeverityName
,b.BatteryInstallationDateUtc as BatteryInstallationDateUtc
,DATEDIFF(DAY, dbo.GetCurrentUtcDate(), DATEADD(MONTH, bt.LifeMonths, b.BatteryInstallationDateUtc)) as BatteryDaysToExchange
,b.Description as Description
,bs.ObjectType as ObjectTypeId
,ot.Description as ObjectTypeName
,dbo.GetDisplayName(bs.ObjectId, bs.ObjectType) as ObjectDisplayName
,b.UnitId as UnitId
,bu.Name AS UnitName
,dbo.GetUnitLocation(bu.Id, 1, 0, default) as UnitLocation
,cu.ID as CurrentUnitId
,cu.Name as CurrentUnitName
,dbo.GetUnitLocation(cu.Id, 1, 0, default) as CurrentUnitLocation
'
END