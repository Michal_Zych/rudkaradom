﻿CREATE FUNCTION [dbo].[srv_GetPrepDescription] 
(
	 @class PREP_CLASS
	,@division PREP_DIVISION
	,@finalCode PREP_FINALCODE
	,@dateCollection datetime
)
RETURNS NVARCHAR(200)
AS
BEGIN

	RETURN @class + ',' 
			+ @division + ','
			+ COALESCE(@finalCode, '?????') + ',' 
			+ COALESCE(CONVERT(varchar(10), @dateCollection, 120), '')
END

