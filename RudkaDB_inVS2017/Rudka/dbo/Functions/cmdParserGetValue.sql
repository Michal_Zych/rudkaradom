﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[cmdParserGetValue]
(
	@cmd nvarchar(max)
	,@param nvarchar(30) 
)
RETURNS nvarchar(100)
AS
BEGIN
	DECLARE  @start int
			,@end int
			,@pattern varchar(50)
			
	SET @pattern = '"' + @param +	'","Value":"'		
			
			
	SET @start = PATINDEX('%' + @pattern + '%', @cmd) + LEN(@pattern)
	SET @end = CHARINDEX('"', @cmd, @start)
	RETURN SUBSTRING(@cmd, @start, @end - @start)

END

