﻿CREATE FUNCTION [dbo].[fn_Split](@text nvarchar(max), @delimiter char(1) = ' ') 
   RETURNS @Strings TABLE 
   ( 
   value nvarchar(max) 
   ) 
   AS 
   BEGIN 
       DECLARE @index int 
       SET @index = -1 

		-- len changed to datalength because i didn't count trailing spaces
       WHILE (DATALENGTH(@text)/2 > 0) 
       BEGIN 
           SET @index = CHARINDEX(@delimiter , @text) 
           IF (@index = 0) AND (DATALENGTH(@text) / 2 > 0) 
           BEGIN 
               INSERT INTO @Strings VALUES (@text) 
               BREAK 
           END 
           IF (@index > 1) 
           BEGIN 
               INSERT INTO @Strings VALUES (LEFT(@text, @index - 1)) 
               SET @text = RIGHT(@text, (DATALENGTH(@text) / 2 - @index)) 
           END 
           ELSE 
               SET @text = RIGHT(@text, (DATALENGTH(@text) / 2 - @index)) 
       END 
       RETURN 
	END

