﻿CREATE FUNCTION [dbo].[dsql_GetTransmitterDetails_Where]
(
@TreeUnitIds nvarchar(max),
@IdFrom int = null
,@IdTo int = null
,@MacFrom bigint = null
,@MacTo bigint = null
,@MacString nvarchar(200) = null
,@ClientIdFrom int = null
,@ClientIdTo int = null
,@InstallationUnitIdFrom int = null
,@InstallationUnitIdTo int = null
,@InstallationUnitName nvarchar(200) = null
,@Name nvarchar(200) = null
,@BarCode nvarchar(200) = null
,@TypeIdFrom int = null
,@TypeIdTo int = null
,@ReportIntervalSecFrom int = null
,@ReportIntervalSecTo int = null
,@SensitivityFrom int = null
,@SensitivityTo int = null
,@AvgCalcTimeSFrom int = null
,@AvgCalcTimeSTo int = null
,@Msisdn nvarchar(200) = null
,@IsBandUpdaterFrom bit = null
,@IsBandUpdaterTo bit = null
,@Description nvarchar(200) = null
,@RssiTresholdFrom smallint = null
,@RssiTresholdTo smallint = null
,@GroupIdFrom tinyint = null
,@GroupIdTo tinyint = null
,@GroupName nvarchar(200) = null
,@IpAddress nvarchar(200) = null
,@IsConnectedFrom bit = null
,@IsConnectedTo bit = null
,@AlarmOngoingFrom bit = null
,@AlarmOngoingTo bit = null
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @useMatchFunction bit = 1
 DECLARE @r nvarchar(max) = ' WHERE IsActive = 1 AND '
-- IF COALESCE(@TreeUnitIds, '') <> ''
--   SET @r = @r + 'UnitId IN(''' + @TreeUnitIds + ''') AND '

 IF @IdFrom IS NOT NULL AND @IdTo IS NOT NULL
  SET @r = @r + '(bs.Id  BETWEEN ' + CONVERT(nvarchar(20), @IdFrom) + ' AND ' + CONVERT(nvarchar(20), @IdTo) + ') AND '
 ELSE IF @IdFrom IS NOT NULL AND @IdTo IS NULL
  SET @r = @r + 'bs.Id  = ' + CONVERT(nvarchar(20), @IdFrom) + ' AND '
 ELSE IF @IdFrom IS NULL AND @IdTo IS NOT NULL
  SET @r = @r + 'bs.Id  IS NULL AND '

 IF @MacFrom IS NOT NULL AND @MacTo IS NOT NULL
  SET @r = @r + '(bs.Mac  BETWEEN ' + CONVERT(nvarchar(20), @MacFrom) + ' AND ' + CONVERT(nvarchar(20), @MacTo) + ') AND '
 ELSE IF @MacFrom IS NOT NULL AND @MacTo IS NULL
  SET @r = @r + 'bs.Mac  = ' + CONVERT(nvarchar(20), @MacFrom) + ' AND '
 ELSE IF @MacFrom IS NULL AND @MacTo IS NOT NULL
  SET @r = @r + 'bs.Mac  IS NULL AND '

 IF @MacString IS NOT NULL
  IF @MacString = '?'
   SET @r = @r + 'COALESCE(bs.MacString , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(bs.MacString , ''' + @MacString + ''') > 0 AND '
    ELSE
     SET @r = @r + 'bs.MacString  LIKE ''' + @MacString + ''' AND '
  END

 IF @ClientIdFrom IS NOT NULL AND @ClientIdTo IS NOT NULL
  SET @r = @r + '(bs.ClientId  BETWEEN ' + CONVERT(nvarchar(20), @ClientIdFrom) + ' AND ' + CONVERT(nvarchar(20), @ClientIdTo) + ') AND '
 ELSE IF @ClientIdFrom IS NOT NULL AND @ClientIdTo IS NULL
  SET @r = @r + 'bs.ClientId  = ' + CONVERT(nvarchar(20), @ClientIdFrom) + ' AND '
 ELSE IF @ClientIdFrom IS NULL AND @ClientIdTo IS NOT NULL
  SET @r = @r + 'bs.ClientId  IS NULL AND '

 IF @InstallationUnitIdFrom IS NOT NULL AND @InstallationUnitIdTo IS NOT NULL
  SET @r = @r + '(u.ID  BETWEEN ' + CONVERT(nvarchar(20), @InstallationUnitIdFrom) + ' AND ' + CONVERT(nvarchar(20), @InstallationUnitIdTo) + ') AND '
 ELSE IF @InstallationUnitIdFrom IS NOT NULL AND @InstallationUnitIdTo IS NULL
  SET @r = @r + 'u.ID  = ' + CONVERT(nvarchar(20), @InstallationUnitIdFrom) + ' AND '
 ELSE IF @InstallationUnitIdFrom IS NULL AND @InstallationUnitIdTo IS NOT NULL
  SET @r = @r + 'u.ID  IS NULL AND '

 IF @InstallationUnitName IS NOT NULL
  IF @InstallationUnitName = '?'
   SET @r = @r + 'COALESCE(u.Name , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(u.Name , ''' + @InstallationUnitName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'u.Name  LIKE ''' + @InstallationUnitName + ''' AND '
  END

 IF @Name IS NOT NULL
  IF @Name = '?'
   SET @r = @r + 'COALESCE(bs.Name , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(bs.Name , ''' + @Name + ''') > 0 AND '
    ELSE
     SET @r = @r + 'bs.Name  LIKE ''' + @Name + ''' AND '
  END

 IF @BarCode IS NOT NULL
  IF @BarCode = '?'
   SET @r = @r + 'COALESCE(bs.BarCode , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(bs.BarCode , ''' + @BarCode + ''') > 0 AND '
    ELSE
     SET @r = @r + 'bs.BarCode  LIKE ''' + @BarCode + ''' AND '
  END

 IF @TypeIdFrom IS NOT NULL AND @TypeIdTo IS NOT NULL
  SET @r = @r + '(bs.TypeId  BETWEEN ' + CONVERT(nvarchar(20), @TypeIdFrom) + ' AND ' + CONVERT(nvarchar(20), @TypeIdTo) + ') AND '
 ELSE IF @TypeIdFrom IS NOT NULL AND @TypeIdTo IS NULL
  SET @r = @r + 'bs.TypeId  = ' + CONVERT(nvarchar(20), @TypeIdFrom) + ' AND '
 ELSE IF @TypeIdFrom IS NULL AND @TypeIdTo IS NOT NULL
  SET @r = @r + 'bs.TypeId  IS NULL AND '

 IF @ReportIntervalSecFrom IS NOT NULL AND @ReportIntervalSecTo IS NOT NULL
  SET @r = @r + '(bs.ReportIntervalSec  BETWEEN ' + CONVERT(nvarchar(20), @ReportIntervalSecFrom) + ' AND ' + CONVERT(nvarchar(20), @ReportIntervalSecTo) + ') AND '
 ELSE IF @ReportIntervalSecFrom IS NOT NULL AND @ReportIntervalSecTo IS NULL
  SET @r = @r + 'bs.ReportIntervalSec  = ' + CONVERT(nvarchar(20), @ReportIntervalSecFrom) + ' AND '
 ELSE IF @ReportIntervalSecFrom IS NULL AND @ReportIntervalSecTo IS NOT NULL
  SET @r = @r + 'bs.ReportIntervalSec  IS NULL AND '

 IF @SensitivityFrom IS NOT NULL AND @SensitivityTo IS NOT NULL
  SET @r = @r + '(bs.Sensitivity  BETWEEN ' + CONVERT(nvarchar(20), @SensitivityFrom) + ' AND ' + CONVERT(nvarchar(20), @SensitivityTo) + ') AND '
 ELSE IF @SensitivityFrom IS NOT NULL AND @SensitivityTo IS NULL
  SET @r = @r + 'bs.Sensitivity  = ' + CONVERT(nvarchar(20), @SensitivityFrom) + ' AND '
 ELSE IF @SensitivityFrom IS NULL AND @SensitivityTo IS NOT NULL
  SET @r = @r + 'bs.Sensitivity  IS NULL AND '

 IF @AvgCalcTimeSFrom IS NOT NULL AND @AvgCalcTimeSTo IS NOT NULL
  SET @r = @r + '(bs.AvgCalcTimeS  BETWEEN ' + CONVERT(nvarchar(20), @AvgCalcTimeSFrom) + ' AND ' + CONVERT(nvarchar(20), @AvgCalcTimeSTo) + ') AND '
 ELSE IF @AvgCalcTimeSFrom IS NOT NULL AND @AvgCalcTimeSTo IS NULL
  SET @r = @r + 'bs.AvgCalcTimeS  = ' + CONVERT(nvarchar(20), @AvgCalcTimeSFrom) + ' AND '
 ELSE IF @AvgCalcTimeSFrom IS NULL AND @AvgCalcTimeSTo IS NOT NULL
  SET @r = @r + 'bs.AvgCalcTimeS  IS NULL AND '

 IF @Msisdn IS NOT NULL
  IF @Msisdn = '?'
   SET @r = @r + 'COALESCE(bs.Msisdn , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(bs.Msisdn , ''' + @Msisdn + ''') > 0 AND '
    ELSE
     SET @r = @r + 'bs.Msisdn  LIKE ''' + @Msisdn + ''' AND '
  END

 IF @IsBandUpdaterFrom IS NOT NULL AND @IsBandUpdaterTo IS NOT NULL
  SET @r = @r + '(bs.IsBandUpdater  BETWEEN ' + CONVERT(nvarchar(20), @IsBandUpdaterFrom) + ' AND ' + CONVERT(nvarchar(20), @IsBandUpdaterTo) + ') AND '
 ELSE IF @IsBandUpdaterFrom IS NOT NULL AND @IsBandUpdaterTo IS NULL
  SET @r = @r + 'bs.IsBandUpdater  = ' + CONVERT(nvarchar(20), @IsBandUpdaterFrom) + ' AND '
 ELSE IF @IsBandUpdaterFrom IS NULL AND @IsBandUpdaterTo IS NOT NULL
  SET @r = @r + 'bs.IsBandUpdater  IS NULL AND '

 IF @Description IS NOT NULL
  IF @Description = '?'
   SET @r = @r + 'COALESCE(bs.[Description] , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(bs.[Description] , ''' + @Description + ''') > 0 AND '
    ELSE
     SET @r = @r + 'bs.[Description]  LIKE ''' + @Description + ''' AND '
  END

 IF @RssiTresholdFrom IS NOT NULL AND @RssiTresholdTo IS NOT NULL
  SET @r = @r + '(bs.RssiTreshold  BETWEEN ' + CONVERT(nvarchar(20), @RssiTresholdFrom) + ' AND ' + CONVERT(nvarchar(20), @RssiTresholdTo) + ') AND '
 ELSE IF @RssiTresholdFrom IS NOT NULL AND @RssiTresholdTo IS NULL
  SET @r = @r + 'bs.RssiTreshold  = ' + CONVERT(nvarchar(20), @RssiTresholdFrom) + ' AND '
 ELSE IF @RssiTresholdFrom IS NULL AND @RssiTresholdTo IS NOT NULL
  SET @r = @r + 'bs.RssiTreshold  IS NULL AND '

 IF @GroupIdFrom IS NOT NULL AND @GroupIdTo IS NOT NULL
  SET @r = @r + '(bs.[Group]  BETWEEN ' + CONVERT(nvarchar(20), @GroupIdFrom) + ' AND ' + CONVERT(nvarchar(20), @GroupIdTo) + ') AND '
 ELSE IF @GroupIdFrom IS NOT NULL AND @GroupIdTo IS NULL
  SET @r = @r + 'bs.[Group]  = ' + CONVERT(nvarchar(20), @GroupIdFrom) + ' AND '
 ELSE IF @GroupIdFrom IS NULL AND @GroupIdTo IS NOT NULL
  SET @r = @r + 'bs.[Group]  IS NULL AND '

 IF @GroupName IS NOT NULL
  IF @GroupName = '?'
   SET @r = @r + 'COALESCE(tg.Description , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(tg.Description , ''' + @GroupName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'tg.Description  LIKE ''' + @GroupName + ''' AND '
  END

 IF @IpAddress IS NOT NULL
  IF @IpAddress = '?'
   SET @r = @r + 'COALESCE(bs.IpAddress , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(bs.IpAddress , ''' + @IpAddress + ''') > 0 AND '
    ELSE
     SET @r = @r + 'bs.IpAddress  LIKE ''' + @IpAddress + ''' AND '
  END

 IF @IsConnectedFrom IS NOT NULL AND @IsConnectedTo IS NOT NULL
  SET @r = @r + '(CONVERT(bit, COALESCE(ts.OngConnectedEventId, 0))  BETWEEN ' + CONVERT(nvarchar(20), @IsConnectedFrom) + ' AND ' + CONVERT(nvarchar(20), @IsConnectedTo) + ') AND '
 ELSE IF @IsConnectedFrom IS NOT NULL AND @IsConnectedTo IS NULL
  SET @r = @r + 'CONVERT(bit, COALESCE(ts.OngConnectedEventId, 0))  = ' + CONVERT(nvarchar(20), @IsConnectedFrom) + ' AND '
 ELSE IF @IsConnectedFrom IS NULL AND @IsConnectedTo IS NOT NULL
  SET @r = @r + 'CONVERT(bit, COALESCE(ts.OngConnectedEventId, 0))  IS NULL AND '

 IF @AlarmOngoingFrom IS NOT NULL AND @AlarmOngoingTo IS NOT NULL
  SET @r = @r + '(CONVERT(bit, COALESCE(ts.OngDisconnectedAlarmId,0))  BETWEEN ' + CONVERT(nvarchar(20), @AlarmOngoingFrom) + ' AND ' + CONVERT(nvarchar(20), @AlarmOngoingTo) + ') AND '
 ELSE IF @AlarmOngoingFrom IS NOT NULL AND @AlarmOngoingTo IS NULL
  SET @r = @r + 'CONVERT(bit, COALESCE(ts.OngDisconnectedAlarmId,0))  = ' + CONVERT(nvarchar(20), @AlarmOngoingFrom) + ' AND '
 ELSE IF @AlarmOngoingFrom IS NULL AND @AlarmOngoingTo IS NOT NULL
  SET @r = @r + 'CONVERT(bit, COALESCE(ts.OngDisconnectedAlarmId,0))  IS NULL AND '

 SET @r = @r + ' 1=1'
 RETURN @r
END