﻿


CREATE FUNCTION [dbo].[SerializeJSON_Patient]
(
	@RecordId int
)
RETURNS nvarchar(max)
AS
BEGIN

	DECLARE @JSON nvarchar(max)

	SET @JSON = 'SELECT p.Id, p.Name, p.LastName, p.Pesel, p.HospitalDateUtc, p.WardId, w.Name as WardName, p.WardDateUtc, p.RoomId, r.Name as RoomName, 
				p.RoomDateUtc, ''$ExtMappings$'' as ExtMappings
		FROM dbo.Patients p
		LEFT JOIN dbo.Units w ON w.ID = p.WardId
		LEFT JOIN dbo.Units r ON r.ID = p.RoomId
		WHERE p.Id = ' + CONVERT(nvarchar(10), @RecordId)
	EXEC @JSON = dbo.SerializeJSON @JSON

	DECLARE @ExtMappings nvarchar(max)
	SET @ExtMappings = 'SELECT ExtId AS AssecoKey FROM arch.Ext_Patients WHERE M2mId = ' + CONVERT(nvarchar(10), @RecordId)
	EXEC @ExtMappings = dbo.SerializeJSON @ExtMappings
	SET @JSON = REPLACE(@JSON, '"$ExtMappings$"', @ExtMappings)

	RETURN @JSON
END