﻿CREATE FUNCTION [dbo].[GetAlarmNotifyText_old]
(
	@location nvarchar(1024),
	@name nvarchar(256),
	@event int,
	@device_type int
)
RETURNS nvarchar(160)
AS
BEGIN

	--ALARM RCKiK Białystok->Oddział Suwałki->Magazyn krwi->Zamrażarka 1: zakres góra powietrze.
	declare @alarm_text nvarchar(160)
	declare @device_t0 nvarchar(32)
	declare @device_t1 nvarchar(32)
	
	set @device_t0 = 'glicerol'
	set @device_t1 = 'powietrze'
	
	if	@device_type = 2
	begin
		set @device_t0 = 'wilgotnosc'
	end
	
	set @alarm_text = 'ALARM ' + @location + '->' + @name + ':'
	
	set @alarm_text = @alarm_text + 
			case
			when @event = 1 then 'zakres dol ' + @device_t0
			when @event = 2 then 'zakres gora ' + @device_t0
			when @event = 4 then 'zakres dol ' + @device_t1
			when @event = 5 then 'zakres dol ' + @device_t0 + ', zakres dol ' + @device_t1
			when @event = 8 then 'zakres gora ' + @device_t1
			when @event = 9 then 'zakres dol ' + @device_t0 + ', zakres gora ' + @device_t1
			when @event = 10 then 'zakres gora ' + @device_t0 + ', zakres dol ' + @device_t1
			when @event = 16 then 'brak czujnika ' + @device_t0
			when @event = 32 then 'brak czujnika ' + @device_t1
			when @event = 48 then 'brak czujnikow ' + @device_t0 + ' ' + @device_t1
			when @event = 64 then 'brak urzadzenia'
			else 'Alarm urzadzenia'
			end
	
	RETURN dbo.ReplacePolishSigns(@alarm_text)

END

