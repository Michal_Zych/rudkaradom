﻿CREATE FUNCTION dsql_GetBands_From
()
RETURNS nvarchar(max)
AS
BEGIN
 RETURN
 ' FROM dbo.Bands b
	LEFT JOIN dbo.BatteryTypes bt ON b.BatteryTypeId = bt.Id
	LEFT JOIN dbo.ev_BandStatus bs ON b.Id = bs.BandId
	LEFT JOIN dbo.ev_Events e ON bs.OngLowBatteryAlarmId = e.Id
	LEFT JOIN dbo.Units bu ON bu.Id = b.UnitId
	LEFT JOIN dbo.Units cu ON cu.Id = bs.CurrentUnitId
	LEFT JOIN dbo.enum_EventSeverityDictionaryPl es ON COALESCE(e.Severity, CASE WHEN bs.OngLowBatteryEventId IS NOT NULL THEN 1 ELSE NULL END) = es.Value
	LEFT JOIN dbo.enum_ObjectTypeDictionaryPl ot ON bs.ObjectType = ot.Value
	 '
END