﻿CREATE FUNCTION dsql_GetPatientDetails_From
()
RETURNS nvarchar(max)
AS
BEGIN
 RETURN
 ' FROM dbo.Patients p
	JOIN enum.ObjectType objectType ON 1 = 1
	LEFT JOIN dbo.Units pu ON p.WardId = pu.ID
	LEFT JOIN dbo.Units pr ON p.RoomId = pr.ID
	LEFT JOIN dbo.ev_BandStatus bs ON bs.ObjectId = p.Id AND bs.ObjectType = objectType.Patient
	LEFT JOIN dbo.Bands b ON b.Id = bs.BandId
	LEFT JOIN dbo.Units cu ON cu.ID = bs.CurrentUnitId 

 '
END