﻿

CREATE FUNCTION [dbo].[SplitWordsToBigint]
(
   @List       dbo.IDs_LIST
)
RETURNS  @Result TABLE(Value BIGINT)  
AS  
BEGIN  
      DECLARE @x XML   
      SELECT @x = CAST('<A>'+ REPLACE(@List,',','</A><A>')+ '</A>' AS XML)  
      INSERT INTO @Result              
      SELECT t.value('.', 'bigint') AS inVal  
      FROM @x.nodes('/A') AS x(t)  
    RETURN  
END 



