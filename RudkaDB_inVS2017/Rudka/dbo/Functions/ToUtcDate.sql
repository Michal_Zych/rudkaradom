﻿
CREATE FUNCTION [dbo].[ToUtcDate]
(
	@date dateTime
)
RETURNS dateTime
AS
BEGIN
	RETURN DATEADD(SECOND, DATEDIFF(SECOND, dbo.GetCurrentDate(), dbo.GetCurrentUtcDate()), @date)
END