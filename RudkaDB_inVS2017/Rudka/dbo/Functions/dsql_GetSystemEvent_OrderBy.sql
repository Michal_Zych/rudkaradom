﻿CREATE FUNCTION dsql_GetSystemEvent_OrderBy
(
 @SortOrder nvarchar(100)
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @result nvarchar(max)
 SET @result = ' ORDER BY ' +
  CASE @SortOrder
   WHEN '+Id' THEN 'e.Id  ASC'
   WHEN '-Id' THEN 'e.Id  DESC'
   WHEN '+DateUtc' THEN ' e.DateUtc  ASC'
   WHEN '-DateUtc' THEN ' e.DateUtc  DESC'
   WHEN '+Type' THEN ' e.Type  ASC'
   WHEN '-Type' THEN ' e.Type  DESC'
   WHEN '+TypeDescription' THEN ' eventType.Description  ASC'
   WHEN '-TypeDescription' THEN ' eventType.Description  DESC'
   WHEN '+UnitId' THEN ' e.UnitId  ASC'
   WHEN '-UnitId' THEN ' e.UnitId  DESC'
   WHEN '+UnitLocation' THEN ' dbo.GetUnitLocation(e.UnitId, 1, 1, default)  ASC'
   WHEN '-UnitLocation' THEN ' dbo.GetUnitLocation(e.UnitId, 1, 1, default)  DESC'
   WHEN '+ObjectId' THEN ' e.ObjectId  ASC'
   WHEN '-ObjectId' THEN ' e.ObjectId  DESC'
   WHEN '+ObjectType' THEN ' e.ObjectType  ASC'
   WHEN '-ObjectType' THEN ' e.ObjectType  DESC'
   WHEN '+ObjectTypeDescription' THEN ' objectType.Description  ASC'
   WHEN '-ObjectTypeDescription' THEN ' objectType.Description  DESC'
   WHEN '+ObjectName' THEN ' dbo.GetObjectName(e.ObjectId, e.ObjectType)  ASC'
   WHEN '-ObjectName' THEN ' dbo.GetObjectName(e.ObjectId, e.ObjectType)  DESC'
   WHEN '+OperatorId' THEN ' e.OperatorId  ASC'
   WHEN '-OperatorId' THEN ' e.OperatorId  DESC'
   WHEN '+OperatorName' THEN ' COALESCE(usr.LastName, '''''''') + COALESCE('''' '''' + usr.Name, '''''''')  ASC'
   WHEN '-OperatorName' THEN ' COALESCE(usr.LastName, '''''''') + COALESCE('''' '''' + usr.Name, '''''''')  DESC'
   WHEN '+EventData' THEN ' CONVERT(nvarchar(200), e.[EventData])  ASC'
   WHEN '-EventData' THEN ' CONVERT(nvarchar(200), e.[EventData])  DESC'
   WHEN '+Reason' THEN ' CONVERT(nvarchar(200), e.Reason)  ASC'
   WHEN '-Reason' THEN ' CONVERT(nvarchar(200), e.Reason)  DESC'
   ELSE 'e.Id '
  END
 RETURN @result + ' '
END