﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	Zwraca ID użytkownika o podanym loginie, u danego klienta
-- =============================================
CREATE FUNCTION [dbo].[usr_GetIdForLogin]
(
	  @login  USER_LOGIN
	, @clientId int
)
RETURNS int
AS
BEGIN
	DECLARE @id as integer 
	SELECT @id = ID FROM dbo.Users WHERE Login = @login AND ClientID = @clientId
	
	RETURN @id
	
END

