﻿CREATE FUNCTION [dbo].[GetNoGoZonesString] 
(
	@ObjectType dbo.OBJECT_TYPE
	,@ObjectId dbo.OBJECT_ID
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @SEPARATOR char = ','
		,@SEPARATOR_ESCAPE nvarchar(10) = '&comma;'
		,@NULL_STRING nvarchar(10) = '';
	DECLARE @a nvarchar(max) = ''
		
	SELECT @a = @a + CONVERT(nvarchar(10), UnitId) + @SEPARATOR 
	FROM dbo.al_NoGoZones
	WHERE Id = @ObjectId AND ObjectType	= @ObjectType

	RETURN @a
END