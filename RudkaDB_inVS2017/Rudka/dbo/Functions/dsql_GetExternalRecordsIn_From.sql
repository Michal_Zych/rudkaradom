﻿CREATE FUNCTION dsql_GetExternalRecordsIn_From
()
RETURNS nvarchar(max)
AS
BEGIN
 RETURN
 ' FROM 
	(SELECT ''Z'' AS Type
		,Id				AS Id
		,NULL			AS RequestId
		,StoreDateUtc	AS StoreDateUtc
		,EventType		AS RecordType
		,EventRecordId	AS RecordId
		,Record			AS Record
		,1				AS Processed
		,1				AS Try
		,StoreDateUtc	AS ProcessedDateUtc
		,Error			AS Error
	FROM tech.sync_EventsIN
	UNION ALL
	SELECT ''O''
		,Id
		,OutRecordId
		,StoreDateUtc
		,RecType
		,RecId
		,Record
		,Processed
		,Try
		,ProcessedDateUtc
		,Error
	FROM tech.sync_RecordsIN) A



 '
END