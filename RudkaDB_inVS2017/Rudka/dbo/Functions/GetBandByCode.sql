﻿CREATE FUNCTION [dbo].[GetBandByCode]
(
	@Code nvarchar(100)
)
RETURNS smallint
AS
BEGIN
	DECLARE @result smallint

	SELECT @result = Id
	FROM dbo.Bands 
	WHERE BarCode = @Code OR CONVERT(nvarchar(20), Uid) = @Code OR SerialNumber = @Code
	
	RETURN @result
END
