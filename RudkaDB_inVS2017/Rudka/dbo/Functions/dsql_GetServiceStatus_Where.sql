﻿CREATE FUNCTION dsql_GetServiceStatus_Where
(
@TreeUnitIds nvarchar(max),
@ServiceName nvarchar(200) = null
,@Description nvarchar(200) = null
,@StartTimeUtcFrom datetime = null
,@StartTimeUtcTo datetime = null
,@PingTimeUtcFrom datetime = null
,@PingTimeUtcTo datetime = null
,@PreviousPingTimeUtcFrom datetime = null
,@PreviousPingTimeUtcTo datetime = null
,@HasPartnerFrom bit = null
,@HasPartnerTo bit = null
,@PartnerPingTimeUtcFrom datetime = null
,@PartnerPingTimeUtcTo datetime = null
,@InfoTxt nvarchar(200) = null
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @useMatchFunction bit = 1
 DECLARE @r nvarchar(max) = ' WHERE ' + '1 = 1 AND '

-- IF COALESCE(@TreeUnitIds, '') <> ''
--   SET @r = @r + 'UnitId IN(''' + @TreeUnitIds + ''') AND '

 IF @ServiceName IS NOT NULL
  IF @ServiceName = '?'
   SET @r = @r + 'COALESCE(ServiceName , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(ServiceName , ''' + @ServiceName + ''') > 0 AND '
    ELSE
     SET @r = @r + 'ServiceName  LIKE ''' + @ServiceName + ''' AND '
  END

 IF @Description IS NOT NULL
  IF @Description = '?'
   SET @r = @r + 'COALESCE(Description , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(Description , ''' + @Description + ''') > 0 AND '
    ELSE
     SET @r = @r + 'Description  LIKE ''' + @Description + ''' AND '
  END

 IF @StartTimeUtcFrom IS NOT NULL
  SET @r = @r + 'StartTimeUtc  >=''' + CONVERT(nvarchar(30), @StartTimeUtcFrom, 121) + ''' AND '
 IF @StartTimeUtcTo IS NOT NULL
  SET @r = @r + 'StartTimeUtc  <=''' + CONVERT(nvarchar(30), @StartTimeUtcTo, 121) + ''' AND '

 IF @PingTimeUtcFrom IS NOT NULL
  SET @r = @r + 'PingTimeUtc  >=''' + CONVERT(nvarchar(30), @PingTimeUtcFrom, 121) + ''' AND '
 IF @PingTimeUtcTo IS NOT NULL
  SET @r = @r + 'PingTimeUtc  <=''' + CONVERT(nvarchar(30), @PingTimeUtcTo, 121) + ''' AND '

 IF @PreviousPingTimeUtcFrom IS NOT NULL
  SET @r = @r + 'PreviousPingTimeUtc  >=''' + CONVERT(nvarchar(30), @PreviousPingTimeUtcFrom, 121) + ''' AND '
 IF @PreviousPingTimeUtcTo IS NOT NULL
  SET @r = @r + 'PreviousPingTimeUtc  <=''' + CONVERT(nvarchar(30), @PreviousPingTimeUtcTo, 121) + ''' AND '

 IF @HasPartnerFrom IS NOT NULL AND @HasPartnerTo IS NOT NULL
  SET @r = @r + '(HasPartner  BETWEEN ' + CONVERT(nvarchar(20), @HasPartnerFrom) + ' AND ' + CONVERT(nvarchar(20), @HasPartnerTo) + ') AND '
 ELSE IF @HasPartnerFrom IS NOT NULL AND @HasPartnerTo IS NULL
  SET @r = @r + 'HasPartner  = ' + CONVERT(nvarchar(20), @HasPartnerFrom) + ' AND '
 ELSE IF @HasPartnerFrom IS NULL AND @HasPartnerTo IS NOT NULL
  SET @r = @r + 'HasPartner  IS NULL AND '

 IF @PartnerPingTimeUtcFrom IS NOT NULL
  SET @r = @r + 'PartnerPingTimeUtc  >=''' + CONVERT(nvarchar(30), @PartnerPingTimeUtcFrom, 121) + ''' AND '
 IF @PartnerPingTimeUtcTo IS NOT NULL
  SET @r = @r + 'PartnerPingTimeUtc  <=''' + CONVERT(nvarchar(30), @PartnerPingTimeUtcTo, 121) + ''' AND '

 IF @InfoTxt IS NOT NULL
  IF @InfoTxt = '?'
   SET @r = @r + 'COALESCE(InfoTxt , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(InfoTxt , ''' + @InfoTxt + ''') > 0 AND '
    ELSE
     SET @r = @r + 'InfoTxt  LIKE ''' + @InfoTxt + ''' AND '
  END

 SET @r = @r + ' 1=1'
 RETURN @r
END