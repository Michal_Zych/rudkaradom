﻿CREATE FUNCTION dsql_GetExternalRecordsOut_Where
(
@TreeUnitIds nvarchar(max),
@IdFrom int = null
,@IdTo int = null
,@EventIdFrom int = null
,@EventIdTo int = null
,@DateUtcFrom datetime = null
,@DateUtcTo datetime = null
,@RecordType char(200) = null
,@Record nvarchar(200) = null
,@ParentIdFrom int = null
,@ParentIdTo int = null
,@TryFrom int = null
,@TryTo int = null
,@TryAgainFrom bit = null
,@TryAgainTo bit = null
,@SentDateUtcFrom datetime = null
,@SentDateUtcTo datetime = null
,@Error nvarchar(200) = null
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @useMatchFunction bit = 1
 DECLARE @r nvarchar(max) = ' WHERE '
-- IF COALESCE(@TreeUnitIds, '') <> ''
--   SET @r = @r + 'UnitId IN(''' + @TreeUnitIds + ''') AND '

 IF @IdFrom IS NOT NULL AND @IdTo IS NOT NULL
  SET @r = @r + '(Id  BETWEEN ' + CONVERT(nvarchar(20), @IdFrom) + ' AND ' + CONVERT(nvarchar(20), @IdTo) + ') AND '
 ELSE IF @IdFrom IS NOT NULL AND @IdTo IS NULL
  SET @r = @r + 'Id  = ' + CONVERT(nvarchar(20), @IdFrom) + ' AND '
 ELSE IF @IdFrom IS NULL AND @IdTo IS NOT NULL
  SET @r = @r + 'Id  IS NULL AND '

 IF @EventIdFrom IS NOT NULL AND @EventIdTo IS NOT NULL
  SET @r = @r + '(EventId  BETWEEN ' + CONVERT(nvarchar(20), @EventIdFrom) + ' AND ' + CONVERT(nvarchar(20), @EventIdTo) + ') AND '
 ELSE IF @EventIdFrom IS NOT NULL AND @EventIdTo IS NULL
  SET @r = @r + 'EventId  = ' + CONVERT(nvarchar(20), @EventIdFrom) + ' AND '
 ELSE IF @EventIdFrom IS NULL AND @EventIdTo IS NOT NULL
  SET @r = @r + 'EventId  IS NULL AND '

 IF @DateUtcFrom IS NOT NULL
  SET @r = @r + 'DateUtc  >=''' + CONVERT(nvarchar(30), @DateUtcFrom, 121) + ''' AND '
 IF @DateUtcTo IS NOT NULL
  SET @r = @r + 'DateUtc  <=''' + CONVERT(nvarchar(30), @DateUtcTo, 121) + ''' AND '

 IF @RecordType IS NOT NULL
  IF @RecordType = '?'
   SET @r = @r + 'COALESCE(RecType , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(RecType , ''' + @RecordType + ''') > 0 AND '
    ELSE
     SET @r = @r + 'RecType  LIKE ''' + @RecordType + ''' AND '
  END

 IF @Record IS NOT NULL
  IF @Record = '?'
   SET @r = @r + 'COALESCE(Record , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(Record , ''' + @Record + ''') > 0 AND '
    ELSE
     SET @r = @r + 'Record  LIKE ''' + @Record + ''' AND '
  END

 IF @ParentIdFrom IS NOT NULL AND @ParentIdTo IS NOT NULL
  SET @r = @r + '(ParentId  BETWEEN ' + CONVERT(nvarchar(20), @ParentIdFrom) + ' AND ' + CONVERT(nvarchar(20), @ParentIdTo) + ') AND '
 ELSE IF @ParentIdFrom IS NOT NULL AND @ParentIdTo IS NULL
  SET @r = @r + 'ParentId  = ' + CONVERT(nvarchar(20), @ParentIdFrom) + ' AND '
 ELSE IF @ParentIdFrom IS NULL AND @ParentIdTo IS NOT NULL
  SET @r = @r + 'ParentId  IS NULL AND '

 IF @TryFrom IS NOT NULL AND @TryTo IS NOT NULL
  SET @r = @r + '(Try  BETWEEN ' + CONVERT(nvarchar(20), @TryFrom) + ' AND ' + CONVERT(nvarchar(20), @TryTo) + ') AND '
 ELSE IF @TryFrom IS NOT NULL AND @TryTo IS NULL
  SET @r = @r + 'Try  = ' + CONVERT(nvarchar(20), @TryFrom) + ' AND '
 ELSE IF @TryFrom IS NULL AND @TryTo IS NOT NULL
  SET @r = @r + 'Try  IS NULL AND '

 IF @TryAgainFrom IS NOT NULL AND @TryAgainTo IS NOT NULL
  SET @r = @r + '(TryAgain  BETWEEN ' + CONVERT(nvarchar(20), @TryAgainFrom) + ' AND ' + CONVERT(nvarchar(20), @TryAgainTo) + ') AND '
 ELSE IF @TryAgainFrom IS NOT NULL AND @TryAgainTo IS NULL
  SET @r = @r + 'TryAgain  = ' + CONVERT(nvarchar(20), @TryAgainFrom) + ' AND '
 ELSE IF @TryAgainFrom IS NULL AND @TryAgainTo IS NOT NULL
  SET @r = @r + 'TryAgain  IS NULL AND '

 IF @SentDateUtcFrom IS NOT NULL
  SET @r = @r + 'SentDateUtc  >=''' + CONVERT(nvarchar(30), @SentDateUtcFrom, 121) + ''' AND '
 IF @SentDateUtcTo IS NOT NULL
  SET @r = @r + 'SentDateUtc  <=''' + CONVERT(nvarchar(30), @SentDateUtcTo, 121) + ''' AND '

 IF @Error IS NOT NULL
  IF @Error = '?'
   SET @r = @r + 'COALESCE(Error , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(Error , ''' + @Error + ''') > 0 AND '
    ELSE
     SET @r = @r + 'Error  LIKE ''' + @Error + ''' AND '
  END

 SET @r = @r + ' 1=1'
 RETURN @r
END