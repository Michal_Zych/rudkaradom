﻿CREATE FUNCTION dsql_GetExternalRecordsIn_Where
(
@TreeUnitIds nvarchar(max),
@Type varchar(200) = null
,@IdFrom int = null
,@IdTo int = null
,@RequestIdFrom int = null
,@RequestIdTo int = null
,@StoreDateUtcFrom datetime = null
,@StoreDateUtcTo datetime = null
,@RecordType nvarchar(200) = null
,@RecordId nvarchar(200) = null
,@Record nvarchar(200) = null
,@ProcessedFrom int = null
,@ProcessedTo int = null
,@TryFrom int = null
,@TryTo int = null
,@ProcessedDateUtcFrom datetime = null
,@ProcessedDateUtcTo datetime = null
,@Error nvarchar(200) = null
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @useMatchFunction bit = 1
 DECLARE @r nvarchar(max) = ' WHERE '
-- IF COALESCE(@TreeUnitIds, '') <> ''
--   SET @r = @r + 'UnitId IN(''' + @TreeUnitIds + ''') AND '

 IF @Type IS NOT NULL
  IF @Type = '?'
   SET @r = @r + 'COALESCE(Type , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(Type , ''' + @Type + ''') > 0 AND '
    ELSE
     SET @r = @r + 'Type  LIKE ''' + @Type + ''' AND '
  END

 IF @IdFrom IS NOT NULL AND @IdTo IS NOT NULL
  SET @r = @r + '(Id  BETWEEN ' + CONVERT(nvarchar(20), @IdFrom) + ' AND ' + CONVERT(nvarchar(20), @IdTo) + ') AND '
 ELSE IF @IdFrom IS NOT NULL AND @IdTo IS NULL
  SET @r = @r + 'Id  = ' + CONVERT(nvarchar(20), @IdFrom) + ' AND '
 ELSE IF @IdFrom IS NULL AND @IdTo IS NOT NULL
  SET @r = @r + 'Id  IS NULL AND '

 IF @RequestIdFrom IS NOT NULL AND @RequestIdTo IS NOT NULL
  SET @r = @r + '(RequestId  BETWEEN ' + CONVERT(nvarchar(20), @RequestIdFrom) + ' AND ' + CONVERT(nvarchar(20), @RequestIdTo) + ') AND '
 ELSE IF @RequestIdFrom IS NOT NULL AND @RequestIdTo IS NULL
  SET @r = @r + 'RequestId  = ' + CONVERT(nvarchar(20), @RequestIdFrom) + ' AND '
 ELSE IF @RequestIdFrom IS NULL AND @RequestIdTo IS NOT NULL
  SET @r = @r + 'RequestId  IS NULL AND '

 IF @StoreDateUtcFrom IS NOT NULL
  SET @r = @r + 'StoreDateUtc  >=''' + CONVERT(nvarchar(30), @StoreDateUtcFrom, 121) + ''' AND '
 IF @StoreDateUtcTo IS NOT NULL
  SET @r = @r + 'StoreDateUtc  <=''' + CONVERT(nvarchar(30), @StoreDateUtcTo, 121) + ''' AND '

 IF @RecordType IS NOT NULL
  IF @RecordType = '?'
   SET @r = @r + 'COALESCE(RecordType , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(RecordType , ''' + @RecordType + ''') > 0 AND '
    ELSE
     SET @r = @r + 'RecordType  LIKE ''' + @RecordType + ''' AND '
  END

 IF @RecordId IS NOT NULL
  IF @RecordId = '?'
   SET @r = @r + 'COALESCE(RecordId , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(RecordId , ''' + @RecordId + ''') > 0 AND '
    ELSE
     SET @r = @r + 'RecordId  LIKE ''' + @RecordId + ''' AND '
  END

 IF @Record IS NOT NULL
  IF @Record = '?'
   SET @r = @r + 'COALESCE(Record , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(Record , ''' + @Record + ''') > 0 AND '
    ELSE
     SET @r = @r + 'Record  LIKE ''' + @Record + ''' AND '
  END

 IF @ProcessedFrom IS NOT NULL AND @ProcessedTo IS NOT NULL
  SET @r = @r + '(Processed  BETWEEN ' + CONVERT(nvarchar(20), @ProcessedFrom) + ' AND ' + CONVERT(nvarchar(20), @ProcessedTo) + ') AND '
 ELSE IF @ProcessedFrom IS NOT NULL AND @ProcessedTo IS NULL
  SET @r = @r + 'Processed  = ' + CONVERT(nvarchar(20), @ProcessedFrom) + ' AND '
 ELSE IF @ProcessedFrom IS NULL AND @ProcessedTo IS NOT NULL
  SET @r = @r + 'Processed  IS NULL AND '

 IF @TryFrom IS NOT NULL AND @TryTo IS NOT NULL
  SET @r = @r + '(Try  BETWEEN ' + CONVERT(nvarchar(20), @TryFrom) + ' AND ' + CONVERT(nvarchar(20), @TryTo) + ') AND '
 ELSE IF @TryFrom IS NOT NULL AND @TryTo IS NULL
  SET @r = @r + 'Try  = ' + CONVERT(nvarchar(20), @TryFrom) + ' AND '
 ELSE IF @TryFrom IS NULL AND @TryTo IS NOT NULL
  SET @r = @r + 'Try  IS NULL AND '

 IF @ProcessedDateUtcFrom IS NOT NULL
  SET @r = @r + 'ProcessedDateUtc  >=''' + CONVERT(nvarchar(30), @ProcessedDateUtcFrom, 121) + ''' AND '
 IF @ProcessedDateUtcTo IS NOT NULL
  SET @r = @r + 'ProcessedDateUtc  <=''' + CONVERT(nvarchar(30), @ProcessedDateUtcTo, 121) + ''' AND '

 IF @Error IS NOT NULL
  IF @Error = '?'
   SET @r = @r + 'COALESCE(Error , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(Error , ''' + @Error + ''') > 0 AND '
    ELSE
     SET @r = @r + 'Error  LIKE ''' + @Error + ''' AND '
  END

 SET @r = @r + ' 1=1'
 RETURN @r
END