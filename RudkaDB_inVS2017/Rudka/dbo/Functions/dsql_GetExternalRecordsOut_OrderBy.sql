﻿CREATE FUNCTION dsql_GetExternalRecordsOut_OrderBy
(
 @SortOrder nvarchar(100)
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @result nvarchar(max)
 SET @result = ' ORDER BY ' +
  CASE @SortOrder
   WHEN '+Id' THEN 'Id  ASC'
   WHEN '-Id' THEN 'Id  DESC'
   WHEN '+EventId' THEN 'EventId  ASC'
   WHEN '-EventId' THEN 'EventId  DESC'
   WHEN '+DateUtc' THEN 'DateUtc  ASC'
   WHEN '-DateUtc' THEN 'DateUtc  DESC'
   WHEN '+RecordType' THEN 'RecType  ASC'
   WHEN '-RecordType' THEN 'RecType  DESC'
   WHEN '+Record' THEN 'Record  ASC'
   WHEN '-Record' THEN 'Record  DESC'
   WHEN '+ParentId' THEN 'ParentId  ASC'
   WHEN '-ParentId' THEN 'ParentId  DESC'
   WHEN '+Try' THEN 'Try  ASC'
   WHEN '-Try' THEN 'Try  DESC'
   WHEN '+TryAgain' THEN 'TryAgain  ASC'
   WHEN '-TryAgain' THEN 'TryAgain  DESC'
   WHEN '+SentDateUtc' THEN 'SentDateUtc  ASC'
   WHEN '-SentDateUtc' THEN 'SentDateUtc  DESC'
   WHEN '+Error' THEN 'Error  ASC'
   WHEN '-Error' THEN 'Error  DESC'
   ELSE 'Id '
  END
 RETURN @result + ' '
END