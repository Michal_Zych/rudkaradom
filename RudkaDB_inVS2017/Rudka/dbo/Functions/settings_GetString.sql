﻿

CREATE FUNCTION [dbo].[settings_GetString] (
	@ClientId int
	,@Name nvarchar(50)
) 
RETURNS nvarchar(1024) 
AS
BEGIN
	DECLARE @ERROR_VALUE nvarchar(1024) = 'Error'

	DECLARE @defValue nvarchar(1024)
	DECLARE @value nvarchar(1024)

	SET @defValue = CASE @Name
		WHEN 'UnitsLocationSeparator'		THEN ' > '
		WHEN 'OldDataColor'					THEN '255,255,150,35'
		ELSE @ERROR_VALUE
	END

	SELECT @value = Value
	FROM dbo.Settings WHERE ClientID = @ClientId AND Name = @Name
	
	RETURN COALESCE(@value, @defValue)
END

