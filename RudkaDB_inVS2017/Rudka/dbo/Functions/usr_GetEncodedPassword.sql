﻿CREATE FUNCTION [dbo].[usr_GetEncodedPassword] (@Str nvarchar(100)) RETURNS nvarchar(64) 
AS
begin
declare @s nvarchar(64)
--set @s= (LOWER(CONVERT(VARCHAR(64), dbo.fnGetBinaryToSha256(CONVERT(VARBINARY(100),@Str)),2)))
set @s= (LOWER(CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', CONVERT(VARBINARY(100),@Str)),2)))

return @s

end