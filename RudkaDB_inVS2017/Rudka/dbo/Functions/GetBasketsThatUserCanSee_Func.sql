﻿-- =============================================
-- Author:		pi
-- Create date: 09-10-2012
-- Description: Return the baskets that user can see
-- =============================================
CREATE FUNCTION [dbo].[GetBasketsThatUserCanSee_Func]
(
	@userId int
)
RETURNS @returnTable table(UnitId int)
AS
BEGIN
	DECLARE @role int;
	
	SET @role = (select distinct roleid
					from UsersUnitsRoles
					where userid = @userid)

	IF @role in (2,4,1) --admin, superadmin, nadzorca
	BEGIN
		INSERT INTO @returnTable
		select u.Id
		from [dbo].[Units] u
		where u.UserId in (select UserId from User_GetDescendantsAndSiblings(@userId)	)
	END
	ELSE --user
	BEGIN
		INSERT INTO @returnTable
		select Id
		from [dbo].[Units]
		where userid = @userid 
	END 
	RETURN 
END

