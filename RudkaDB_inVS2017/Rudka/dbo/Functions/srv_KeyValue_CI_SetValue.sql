﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[srv_KeyValue_CI_SetValue]
(
	@text nvarchar(max)
	,@key nvarchar(50)
	,@param nvarchar(50)
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @NULL_VALUE nvarchar(5) = 'null'
		,@TO_END int = 99999

	DECLARE @result nvarchar(max)
		,@uText nvarchar(max)
		,@uKey nvarchar(50)

	SELECT @result = COALESCE(@text, '')
		, @uText = ';' + UPPER(COALESCE(@text,'')) + ';'
		, @uKey = ';' + UPPER(@key)
		, @param = COALESCE(dbo.srv_ToQuotedText(@param), '''' + @NULL_VALUE + '''')
	
	DECLARE @keyStart int
		,@valueStart int
		,@valueLen int
	SET @keyStart = CHARINDEX(@ukey + '=', @uText COLLATE SQL_Latin1_General_Cp1_CS_AS)
		
	IF @keyStart = 0
	BEGIN
		IF @uText = ''
			SET @result = @key + '=' + @param + ''
		ELSE
			SET @result = @text + ';' + @key + '=' + @param + ''
	END
	ELSE
	BEGIN
		SET @valueStart = @keyStart + LEN(@uKey)
		SET @valueLen = CHARINDEX(';', SUBSTRING(@uText, @valueStart, @TO_END)) - 2
		IF @valueLen = -1 SET @valueLen = @TO_END
		--SET @result = STUFF(@result, @valueStart, @valueLen, COALESCE(@param, 'NULL'))
		SET @result = STUFF(@text, @valueStart, @valueLen, @param)
	END
	
	RETURN @result
END