﻿

CREATE FUNCTION [dbo].[GetDepartmentIdForUnit](
	@UnitID INT
	)
RETURNS INT
AS
BEGIN
	DECLARE @DEPARTMENT_UNIT_TYPEV2 int = 29


	DECLARE @ParentUnitID int
	DECLARE @UnitTypeIdv2 int

	SELECT @ParentUnitID = ParentUnitID, @UnitTypeIdv2 = UnitTypeIdv2
	FROM dbo.Units
	WHERE ID = @UnitID
	
	IF @UnitTypeIdv2=@DEPARTMENT_UNIT_TYPEV2
	BEGIN
		RETURN @UnitId
	END

	WHILE (@ParentUnitID IS NOT NULL) AND (@UnitTypeIdv2 != @DEPARTMENT_UNIT_TYPEV2)
	BEGIN
		SET @UnitID = @ParentUnitID
		SELECT @UnitTypeIdv2 = UnitTypeIdv2, @ParentUnitID = ParentUnitID  
		FROM dbo.Units
		WHERE ID = @UnitID
	END

	RETURN @UnitId;
END