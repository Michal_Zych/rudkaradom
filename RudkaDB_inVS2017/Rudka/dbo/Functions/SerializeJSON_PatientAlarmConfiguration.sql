﻿


CREATE FUNCTION [dbo].[SerializeJSON_PatientAlarmConfiguration]
(
	@RecordId int
)
RETURNS nvarchar(max)
AS
BEGIN

	DECLARE @JSON nvarchar(max)

	SET @JSON = 'SELECT PatientId, ''$Patient$'' AS Patient, IsSOScalling, IsACCactive, DMoveWrActive, DMoveWrMins, DMoveAlActive, DMoveAlMins, DNoMoveWrActive
      ,DNoMoveWrMins, DNoMoveAlActive, DNoMoveAlMins, NMoveWrActive, NMoveWrMins, NMoveAlActive, NMoveAlMins, NNoMoveWrActive, NNoMoveWrMins, NNoMoveAlActive
      ,NNoMoveAlMins, OutOfZoneWrActive, OutOfZoneWrMins, OutOfZoneAlActive, OutOfZoneAlMins, InNoGoZoneMins, NoConnectWrActive, NoConnectWrMins, NoConnectAlActive
      ,NoConnectAlMins, ''$Callings$'' AS Callings, ''$NoGoZones$'' AS NoGoZones
	FROM dbo.al_ConfigPatients WHERE PatientId = ' + CONVERT(nvarchar(10), @RecordId)
	EXEC @JSON = dbo.SerializeJSON @JSON

	DECLARE @Patient nvarchar(max)
	SET @Patient = 'SELECT Name, LastName, PESEL FROM dbo.Patients WHERE Id = ' + CONVERT(nvarchar(10), @RecordId)
	EXEC @Patient = dbo.SerializeJSON @Patient
	SET @JSON = REPLACE(@JSON, '"$Patient$"', @Patient)

	DECLARE @Callings nvarchar(max)
	SET @Callings = 'SELECT Phone, Description FROM al_CallingGroups , [enum].[ObjectType] WHERE ObjectType = [enum].[ObjectType].[Unit] AND Id =' + CONVERT(nvarchar(10), @RecordId)
	EXEC @Callings = dbo.SerializeJSON @Callings
	SET @JSON = REPLACE(@JSON, '"$Callings$"', @Callings)
	
	DECLARE @NoGoZones nvarchar(max)
	SET @NoGoZones = 'SELECT UnitId FROM al_NoGoZones , [enum].[ObjectType] WHERE ObjectType = [enum].[ObjectType].[Unit] AND ID =' + CONVERT(nvarchar(10), @RecordId)
	EXEC @NoGoZones = dbo.SerializeJSON @NoGoZones
	SET @JSON = REPLACE(@JSON, '"$NoGoZones$"', @NoGoZones)

	RETURN @JSON
END