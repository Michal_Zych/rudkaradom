﻿-- =============================================
-- Author:		
-- Create date: 
-- =============================================
CREATE FUNCTION [dbo].[Units_GetUserUnits_Func_old](@userId INT)
RETURNS @toReturn TABLE
(Id int, Name nvarchar(256), UnitTypeId int, ParentUnitId int, ClientId int)
AS
BEGIN
 DECLARE @Units IDs_LIST;
 DECLARE @CommonAncestor INT;
 DECLARE @Role INT;
 --Declare @tab table (id int);
 
 --insert into @tab select id from GetALLChildUnitsForUserF(@userid)

 SELECT @Units = COALESCE(@Units + ',', '') + CAST(UnitId  as varchar(10))
  ,@Role = RoleId
 FROM UsersUnitsRoles WITH (NOLOCK)
 WHERE userId = @userid

 SELECT @CommonAncestor = dbo.Units_NearestCommonAncestor(@Units);

 WITH Hierachy (ID,NAME,UnitTypeID,ParentUnitID, UserId, Active, ClientId)--, LEVEL)
 AS (
  /*SELECT ID,NAME,UnitTypeID,ParentUnitID, UserId, Active--, 0 AS LEVEL
  FROM Units e WITH (NOLOCK)
  WHERE e.ID = @CommonAncestor
  */
  (
   SELECT ID,NAME,UnitTypeID,ParentUnitID, r.UserID, Active, ClientId
   FROM UsersUnitsRoles r WITH (NOLOCK)
   inner join units u on u.id=r.UnitID
   WHERE r.userId = @userid and active=1
  union   
   SELECT ID,NAME,UnitTypeID,ParentUnitID, e.UserId, e.Active, e.ClientID
   FROM Units e WITH (NOLOCK)
   WHERE e.ParentUnitID = @CommonAncestor and e.UnitTypeID=9
     
  )
  UNION ALL
   
  SELECT e.ID,e.NAME,e.UnitTypeID,e.ParentUnitID, e.UserId, e.Active, e.ClientID--, eh.LEVEL + 1
  FROM Units e WITH (NOLOCK)
  INNER JOIN Hierachy eh ON e.ParentUnitID = eh.ID
  where e.Active = 1
  --where e.ID in (select Id from @tab)
  
 )
 INSERT INTO @toReturn(Id, Name, UnitTypeId, ParentUnitId, ClientId)
 SELECT ID,NAME,UnitTypeID,ParentUnitID, ClientID
  FROM Units e WITH (NOLOCK)
  WHERE e.ID = @CommonAncestor and not Exists(select id from Hierachy where e.id=id)
  union all
 SELECT h.ID, h.NAME, h.UnitTypeID,h.ParentUnitID, h.ClientId
 FROM Hierachy h WITH (NOLOCK)
 WHERE
  (
  ((@Role = 5 AND h.UserId = @userId) OR h.UnitTypeId != 10) --ignoruje koszyki innych userow
  OR
  (@Role != 5)
  )
  AND 
  (
   h.UnitTypeId in (1,2,3,4,5,6,8,10,11,12,13)
   OR
   --wywala te unity które nie mają dzieci (unitów) 
   Exists(select * from Units t2 where t2.ParentUnitId = h.id)
  )
  AND 
   h.Active = 1 --tylko aktywne
  AND
  (
   --filtracja koszyków i kontenerów koszyków dla ról nie RFID
   @Role not in (1,2,3) 
   OR
   h.UnitTypeId not in (9,10)
  )
 RETURN;
END

