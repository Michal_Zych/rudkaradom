﻿
CREATE FUNCTION [dbo].[srv_KeyValue_CSVtoNumberedValues]
(
	@values nvarchar(max)
)
RETURNS nvarchar(max)
AS
BEGIN
	DECLARE @number int = 1
	DECLARE @separatorPos int = 0

	SET @values = CONVERT(nvarchar(2), @number) + '=' + @values
	SET @number = @number + 1
	SET @separatorPos = 1

	WHILE @separatorPos > 0 
	BEGIN
		SET @separatorPos = CHARINDEX(';', @values,  @separatorPos + 1)
		IF @separatorPos > 0
		BEGIN
			SET @values = STUFF(@values, @separatorPos, 1, ';' + CONVERT(nvarchar(2), @number) + '=')
			SET @number = @number + 1
		END
	END
	
	RETURN @values
END





