﻿CREATE FUNCTION dsql_GetServiceStatus_OrderBy
(
 @SortOrder nvarchar(100)
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @result nvarchar(max)
 SET @result = ' ORDER BY ' +
  CASE @SortOrder
   WHEN '+ServiceName' THEN 'ServiceName  ASC'
   WHEN '-ServiceName' THEN 'ServiceName  DESC'
   WHEN '+Description' THEN 'Description  ASC'
   WHEN '-Description' THEN 'Description  DESC'
   WHEN '+StartTimeUtc' THEN 'StartTimeUtc  ASC'
   WHEN '-StartTimeUtc' THEN 'StartTimeUtc  DESC'
   WHEN '+PingTimeUtc' THEN 'PingTimeUtc  ASC'
   WHEN '-PingTimeUtc' THEN 'PingTimeUtc  DESC'
   WHEN '+PreviousPingTimeUtc' THEN 'PreviousPingTimeUtc  ASC'
   WHEN '-PreviousPingTimeUtc' THEN 'PreviousPingTimeUtc  DESC'
   WHEN '+HasPartner' THEN 'HasPartner  ASC'
   WHEN '-HasPartner' THEN 'HasPartner  DESC'
   WHEN '+PartnerPingTimeUtc' THEN 'PartnerPingTimeUtc  ASC'
   WHEN '-PartnerPingTimeUtc' THEN 'PartnerPingTimeUtc  DESC'
   WHEN '+InfoTxt' THEN 'InfoTxt  ASC'
   WHEN '-InfoTxt' THEN 'InfoTxt  DESC'
   ELSE 'ServiceName '
  END
 RETURN @result + ' '
END