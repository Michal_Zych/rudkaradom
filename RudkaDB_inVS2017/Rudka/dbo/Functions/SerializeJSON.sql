﻿
 
CREATE function [dbo].[SerializeJSON] (
	@ParameterSQL AS VARCHAR(MAX)
)
RETURNS nvarchar(max)
AS
BEGIN
    DECLARE @SQL NVARCHAR(MAX)
    DECLARE @XMLString VARCHAR(MAX)
    DECLARE @XML XML
    DECLARE @Paramlist NVARCHAR(1000)
 
    SET @Paramlist = N'@XML XML OUTPUT'
    SET @SQL = 'WITH PrepareTable (XMLString)'
    SET @SQL = @SQL + 'AS('
    SET @SQL = @SQL + @ParameterSQL + ' FOR XML RAW,TYPE,ELEMENTS XSINIL'
    SET @SQL = @SQL + ')'
    SET @SQL = @SQL + 'SELECT @XML=[XMLString]FROM[PrepareTable]'
 
    EXEC sp_executesql @SQL
        , @Paramlist
        , @XML = @XML OUTPUT
 
    SET @XMLString = CAST(@XML AS VARCHAR(MAX))
 --select @XMLString
    DECLARE @JSON VARCHAR(MAX)
    DECLARE @Row VARCHAR(MAX)
    DECLARE @RowStart INT
    DECLARE @RowEnd INT
    DECLARE @FieldStart INT
    DECLARE @FieldEnd INT
    DECLARE @KEY VARCHAR(MAX)
    DECLARE @Value VARCHAR(MAX)
    DECLARE @StartRoot VARCHAR(100);
 
    SET @StartRoot =  '<row xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' --'<row>'
    DECLARE @EndRoot VARCHAR(100);
 
    SET @EndRoot = '</row>'
 
    DECLARE @StartField VARCHAR(100);
 
    SET @StartField = '<'
 
    DECLARE @EndField VARCHAR(100);
 
    SET @EndField = '>'
    SET @RowStart = CHARINDEX(@StartRoot, @XMLString, 0)
    SET @JSON = ''
 
    WHILE @RowStart > 0
    BEGIN
        SET @RowStart = @RowStart + LEN(@StartRoot)
        SET @RowEnd = CHARINDEX(@EndRoot, @XMLString, @RowStart)
        SET @Row = SUBSTRING(@XMLString, @RowStart, @RowEnd - @RowStart)
        SET @JSON = @JSON + '{'
        -- for each row
        SET @FieldStart = CHARINDEX(@StartField, @Row, 0)
 
        WHILE @FieldStart > 0
        BEGIN
            -- parse node key
            SET @FieldStart = @FieldStart + LEN(@StartField)
            SET @FieldEnd = CHARINDEX(@EndField, @Row, @FieldStart)
			IF SUBSTRING(@Row, @FieldEnd-1, 1) = '/' 
			BEGIN -- empty tag
				SET @KEY = SUBSTRING(@Row, @FieldStart, @FieldEnd - @FieldStart - 1)

				IF CHARINDEX(' xsi:nil="true"', @KEY) > 0
				BEGIN
					SET @KEY = SUBSTRING(@KEY, 0, CHARINDEX(' xsi:nil="true"', @KEY))
					SET @JSON = @JSON + '"' + @KEY + '":null,'
				END
				ELSE
					SET @JSON = @JSON + '"' + @KEY + '":"",'
				SET @FieldStart = CHARINDEX(@StartField, @Row, @FieldEnd)
			END 
			ELSE 
			BEGIN
				SET @KEY = SUBSTRING(@Row, @FieldStart, @FieldEnd - @FieldStart)
				SET @JSON = @JSON + '"' + @KEY + '":'
				-- parse node value
				SET @FieldStart = @FieldEnd + 1
				SET @FieldEnd = CHARINDEX('</', @Row, @FieldStart)
				SET @Value = SUBSTRING(@Row, @FieldStart, @FieldEnd - @FieldStart)

				SET @value = REPLACE(@value, '\', '\\')
				SET @value = REPLACE(@value, '/', '\/')
				SET @value = REPLACE(@value, '"', '\"')
				SET @value = REPLACE(@value, CHAR(9), '\t')
				SET @value = REPLACE(@value, '&#x0D;', '\r')
				SET @value = REPLACE(@value, CHAR(10), '\n')
				SET @value = REPLACE(@value, '&lt;', '<')
				SET @value = REPLACE(@value, '&gt;', '>')
				SET @value = REPLACE(@value, '&amp;', '&')

				SET @JSON = @JSON + '"' + @Value + '",'
				SET @FieldStart = @FieldStart + LEN(@StartField)
				SET @FieldEnd = CHARINDEX(@EndField, @Row, @FieldStart)
				SET @FieldStart = CHARINDEX(@StartField, @Row, @FieldEnd)
			END 
        END
 
        IF LEN(@JSON) > 0
            SET @JSON = SUBSTRING(@JSON, 0, LEN(@JSON))
        SET @JSON = @JSON + '},'
        --/ for each row
        SET @RowStart = CHARINDEX(@StartRoot, @XMLString, @RowEnd)
    END
 
    IF LEN(@JSON) > 0
        SET @JSON = SUBSTRING(@JSON, 0, LEN(@JSON))
    SET @JSON = '[' + @JSON + ']'
 
    RETURN @JSON
END