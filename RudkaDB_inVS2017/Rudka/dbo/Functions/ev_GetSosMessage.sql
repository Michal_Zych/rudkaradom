﻿CREATE FUNCTION [dbo].[ev_GetSosMessage]
(
	@EventUnitId int
	,@EventDateUtc dateTime
	,@ObjectType dbo.OBJECT_TYPE
	,@ObjectId dbo.OBJECT_ID
	,@ObjectUnitId int
)
RETURNS nvarchar(1024)
AS
BEGIN
	DECLARE
		@result nvarchar(1024)
		,@objectType_Patient tinyint
		,@eventWardName nvarchar(100)
		,@eventRoomName nvarchar(100)
		,@patientWardName nvarchar(100)
		,@patientName nvarchar(100)

	SELECT @objectType_Patient = Patient FROM enum.ObjectType

	SELECT @eventRoomName = Name FROM dbo.Units WHERE Id = @EventUnitId

	IF @ObjectType = @objectType_Patient
	BEGIN
		SELECT @patientName = Name + ' ' + LastName FROM dbo.Patients WHERE Id = @Objectid
		IF @EventUnitId = @ObjectUnitId
		BEGIN
			SELECT @eventRoomName = Name FROM dbo.Units WHERE Id = @ObjectUnitId
		END
		ELSE
		BEGIN
			SELECT 
				@eventWardName = dbo.GetWardNameForRoom(@EventUnitId)
				,@patientWardName = dbo.GetWardNameForRoom(@ObjectUnitId)
			SELECT @eventRoomName = Name FROM dbo.Units WHERE Id = @EventUnitId
		END

	END


	SET @result = COALESCE('Oddział ' + @eventWardName + ' ', '')
		+ 'sala ' + @eventRoomName + ' '
		+ 'pacjent ' + COALESCE(@patientName + ' ', '')
		+ COALESCE('z oddziału ' + @patientWardName + ' ', '')
		+ 'wzywa pomocy.'


	RETURN @result
END
