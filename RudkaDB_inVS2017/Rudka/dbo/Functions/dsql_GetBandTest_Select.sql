﻿CREATE FUNCTION dsql_GetBandTest_Select
()
RETURNS nvarchar(max)
AS
BEGIN
 RETURN
'b.Id as Id
,b.BarCode as BarCode
,b.MacString as MacString
,b.Uid as Uid
,bt.Name as BatteryTypeStr
,COALESCE(e.Severity, CASE WHEN bs.OngLowBatteryEventId IS NOT NULL THEN 1 ELSE NULL END) as BatteryAlarmSeverityIds
,es.Description as BatteryAlarmSeverity
,b.BatteryInstallationDateUtc as BatteryInstallationDate
,DATEDIFF(DAY, dbo.GetCurrentUtcDate(), DATEADD(MONTH, bt.LifeMonths, b.BatteryInstallationDateUtc)) as BatteryDaysToExchange
,b.Description as Description
,bs.ObjectType as ObjectTypeIds
,ot.Description as ObjectType
,dbo.GetDisplayName(bs.ObjectId, bs.ObjectType) as ObjectDisplayName
,ou.ID as ObjectUnitIds
,ou.Name as ObjectUnitName
,dbo.GetUnitLocation(ou.Id, default, default, default) as ObjectUnitLocation
,cu.ID as CurrentUnitIds
,cu.Name as CurrentUnitName
,dbo.GetUnitLocation(cu.Id, default, default, default) as CurrentUnitLocation
'
END