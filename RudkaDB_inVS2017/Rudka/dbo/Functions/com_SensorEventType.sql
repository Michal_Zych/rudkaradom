﻿-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [dbo].[com_SensorEventType] 
(
	  @value MEASURE_DT
	, @aL MEASURE_DT
	, @aH MEASURE_DT
)
RETURNS tinyint
AS
BEGIN
	-- stałe jak w procedurze com_StoreData
	DECLARE
		  @OK			tinyint = 0x00
		, @NO_SENSOR	tinyint = 0x01
		, @TO_HIGH		tinyint = 0x02
		, @TO_LOW		tinyint = 0x03
	
	DECLARE @eventType tinyint

	IF @value IS NULL SET @eventType = @NO_SENSOR
	ELSE IF @value > @aH SET @eventType = @TO_HIGH
	ELSE IF @value < @aL SET @eventType = @TO_LOW
	ELSE SET @eventType = @OK 	

	RETURN @eventType

END

