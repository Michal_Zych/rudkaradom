﻿-- =============================================
-- Author:		pi
-- Create date: 09-10-2012
-- Description:	Returns user descendant and siblings, based on permissions
-- =============================================
CREATE FUNCTION [dbo].[User_GetDescendantsAndSiblings] (@userId INT)
RETURNS @return TABLE(UserId int, RoleId int, UnitId int)
AS
BEGIN
      DECLARE @roleId INT;

      SELECT TOP 1 @roleId = roleid
      FROM   usersunitsroles
      WHERE  userid = @userId;

      WITH tmpunits(unitid)
           AS (SELECT unitid
               FROM   usersunitsroles
               WHERE  userid = @userId),
           hierachy (id, unittypeid, parentunitid, level)
           AS (SELECT id, unittypeid, parentunitid, 0 AS LEVEL
               FROM   units e
               WHERE  e.id IN (SELECT * FROM   tmpunits)
               UNION ALL
               SELECT e.id, e.unittypeid, e.parentunitid, eh.level + 1 AS LEVEL
               FROM   units e
							INNER JOIN hierachy eh
                              ON e.parentunitid = eh.id),
           filtereduser
           AS (SELECT uur.* FROM  usersunitsroles uur
						INNER JOIN Users u 
							ON uur.UserId = u.Id
               WHERE  
               uur.unitid IN  (SELECT id FROM   hierachy)
					AND 
					uur.roleid IN (SELECT * FROM   Roles_GetDescendants (@roleId))
					AND
					u.Active = 1
					)
					
		INSERT INTO @return(UserId, RoleId, UnitId)             
      SELECT DISTINCT UserId, RoleId, UnitId
      FROM   filtereduser
      
      RETURN;
END

