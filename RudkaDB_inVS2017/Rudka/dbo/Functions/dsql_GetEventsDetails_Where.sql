﻿CREATE FUNCTION dsql_GetEventsDetails_Where
(
@TreeUnitIds nvarchar(max),
@IdFrom int = null
,@IdTo int = null
,@DateUtcFrom datetime = null
,@DateUtcTo datetime = null
,@TypeFrom tinyint = null
,@TypeTo tinyint = null
,@TypeDescription nvarchar(200) = null
,@SeverityFrom tinyint = null
,@SeverityTo tinyint = null
,@SeverityDescription nvarchar(200) = null
,@UnitIdFrom int = null
,@UnitIdTo int = null
,@UnitName nvarchar(200) = null
,@UnitLocation nvarchar(200) = null
,@ObjectIdFrom int = null
,@ObjectIdTo int = null
,@ObjectTypeFrom tinyint = null
,@ObjectTypeTo tinyint = null
,@ObjectTypeDescription nvarchar(200) = null
,@ObjectName nvarchar(200) = null
,@ObjectUnitIdFrom int = null
,@ObjectUnitIdTo int = null
,@ObjectUnitName nvarchar(200) = null
,@ObjectUnitLocation nvarchar(200) = null
,@EndDateUtcFrom datetime = null
,@EndDateUtcTo datetime = null
,@EndingEventIdFrom int = null
,@EndingEventIdTo int = null
,@OperatorIdFrom int = null
,@OperatorIdTo int = null
,@OperatorName nvarchar(200) = null
,@OperatorLastName nvarchar(200) = null
,@EventData nvarchar(200) = null
,@Reason nvarchar(200) = null
,@RaisingEventIdFrom int = null
,@RaisingEventIdTo int = null
,@IsClosedFrom bit = null
,@IsClosedTo bit = null
,@ClosingDateUtcFrom datetime = null
,@ClosingDateUtcTo datetime = null
,@ClosingUserIdFrom int = null
,@ClosingUserIdTo int = null
,@ClosingUserName nvarchar(200) = null
,@ClosingUserLastName nvarchar(200) = null
,@ClosingAlarmIdFrom int = null
,@ClosingAlarmIdTo int = null
,@ClosingComment nvarchar(200) = null
,@UserMessageIdFrom int = null
,@UserMessageIdTo int = null
,@SenderIdFrom int = null
,@SenderIdTo int = null
,@SenderName nvarchar(200) = null
,@SenderLastName nvarchar(200) = null
,@Message nvarchar(200) = null
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @useMatchFunction bit = 1
 DECLARE @r nvarchar(max) = ' WHERE '
-- IF COALESCE(@TreeUnitIds, '') <> ''
--   SET @r = @r + 'UnitId IN(''' + @TreeUnitIds + ''') AND '

 IF @IdFrom IS NOT NULL AND @IdTo IS NOT NULL
  SET @r = @r + '(e.Id  BETWEEN ' + CONVERT(nvarchar(20), @IdFrom) + ' AND ' + CONVERT(nvarchar(20), @IdTo) + ') AND '
 ELSE IF @IdFrom IS NOT NULL AND @IdTo IS NULL
  SET @r = @r + 'e.Id  = ' + CONVERT(nvarchar(20), @IdFrom) + ' AND '
 ELSE IF @IdFrom IS NULL AND @IdTo IS NOT NULL
  SET @r = @r + 'e.Id  IS NULL AND '

 IF @DateUtcFrom IS NOT NULL
  SET @r = @r + 'e.DateUtc  >=''' + CONVERT(nvarchar(30), @DateUtcFrom, 121) + ''' AND '
 IF @DateUtcTo IS NOT NULL
  SET @r = @r + 'e.DateUtc  <=''' + CONVERT(nvarchar(30), @DateUtcTo, 121) + ''' AND '

 IF @TypeFrom IS NOT NULL AND @TypeTo IS NOT NULL
  SET @r = @r + '(e.Type  BETWEEN ' + CONVERT(nvarchar(20), @TypeFrom) + ' AND ' + CONVERT(nvarchar(20), @TypeTo) + ') AND '
 ELSE IF @TypeFrom IS NOT NULL AND @TypeTo IS NULL
  SET @r = @r + 'e.Type  = ' + CONVERT(nvarchar(20), @TypeFrom) + ' AND '
 ELSE IF @TypeFrom IS NULL AND @TypeTo IS NOT NULL
  SET @r = @r + 'e.Type  IS NULL AND '

 IF @TypeDescription IS NOT NULL
  IF @TypeDescription = '?'
   SET @r = @r + 'COALESCE(typeEnum.Description , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched(typeEnum.Description , ''' + @TypeDescription + ''') > 0 AND '
    ELSE
     SET @r = @r + 'typeEnum.Description  LIKE ''' + @TypeDescription + ''' AND '
  END

 IF @SeverityFrom IS NOT NULL AND @SeverityTo IS NOT NULL
  SET @r = @r + '(e.Severity  BETWEEN ' + CONVERT(nvarchar(20), @SeverityFrom) + ' AND ' + CONVERT(nvarchar(20), @SeverityTo) + ') AND '
 ELSE IF @SeverityFrom IS NOT NULL AND @SeverityTo IS NULL
  SET @r = @r + 'e.Severity  = ' + CONVERT(nvarchar(20), @SeverityFrom) + ' AND '
 ELSE IF @SeverityFrom IS NULL AND @SeverityTo IS NOT NULL
  SET @r = @r + 'e.Severity  IS NULL AND '

 IF @SeverityDescription IS NOT NULL
  IF @SeverityDescription = '?'
   SET @r = @r + 'COALESCE( severityEnum.Description , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( severityEnum.Description , ''' + @SeverityDescription + ''') > 0 AND '
    ELSE
     SET @r = @r + ' severityEnum.Description  LIKE ''' + @SeverityDescription + ''' AND '
  END

 IF @UnitIdFrom IS NOT NULL AND @UnitIdTo IS NOT NULL
  SET @r = @r + '( e.UnitId  BETWEEN ' + CONVERT(nvarchar(20), @UnitIdFrom) + ' AND ' + CONVERT(nvarchar(20), @UnitIdTo) + ') AND '
 ELSE IF @UnitIdFrom IS NOT NULL AND @UnitIdTo IS NULL
  SET @r = @r + ' e.UnitId  = ' + CONVERT(nvarchar(20), @UnitIdFrom) + ' AND '
 ELSE IF @UnitIdFrom IS NULL AND @UnitIdTo IS NOT NULL
  SET @r = @r + ' e.UnitId  IS NULL AND '

 IF @UnitName IS NOT NULL
  IF @UnitName = '?'
   SET @r = @r + 'COALESCE( u.Name , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( u.Name , ''' + @UnitName + ''') > 0 AND '
    ELSE
     SET @r = @r + ' u.Name  LIKE ''' + @UnitName + ''' AND '
  END

 IF @UnitLocation IS NOT NULL
  IF @UnitLocation = '?'
   SET @r = @r + 'COALESCE( dbo.GetUnitLocation(e.UnitId, DEFAULT, DEFAULT, DEFAULT) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( dbo.GetUnitLocation(e.UnitId, DEFAULT, DEFAULT, DEFAULT) , ''' + @UnitLocation + ''') > 0 AND '
    ELSE
     SET @r = @r + ' dbo.GetUnitLocation(e.UnitId, DEFAULT, DEFAULT, DEFAULT)  LIKE ''' + @UnitLocation + ''' AND '
  END

 IF @ObjectIdFrom IS NOT NULL AND @ObjectIdTo IS NOT NULL
  SET @r = @r + '( e.ObjectId  BETWEEN ' + CONVERT(nvarchar(20), @ObjectIdFrom) + ' AND ' + CONVERT(nvarchar(20), @ObjectIdTo) + ') AND '
 ELSE IF @ObjectIdFrom IS NOT NULL AND @ObjectIdTo IS NULL
  SET @r = @r + ' e.ObjectId  = ' + CONVERT(nvarchar(20), @ObjectIdFrom) + ' AND '
 ELSE IF @ObjectIdFrom IS NULL AND @ObjectIdTo IS NOT NULL
  SET @r = @r + ' e.ObjectId  IS NULL AND '

 IF @ObjectTypeFrom IS NOT NULL AND @ObjectTypeTo IS NOT NULL
  SET @r = @r + '( e.ObjectType  BETWEEN ' + CONVERT(nvarchar(20), @ObjectTypeFrom) + ' AND ' + CONVERT(nvarchar(20), @ObjectTypeTo) + ') AND '
 ELSE IF @ObjectTypeFrom IS NOT NULL AND @ObjectTypeTo IS NULL
  SET @r = @r + ' e.ObjectType  = ' + CONVERT(nvarchar(20), @ObjectTypeFrom) + ' AND '
 ELSE IF @ObjectTypeFrom IS NULL AND @ObjectTypeTo IS NOT NULL
  SET @r = @r + ' e.ObjectType  IS NULL AND '

 IF @ObjectTypeDescription IS NOT NULL
  IF @ObjectTypeDescription = '?'
   SET @r = @r + 'COALESCE( objectTypeEnum.Description , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( objectTypeEnum.Description , ''' + @ObjectTypeDescription + ''') > 0 AND '
    ELSE
     SET @r = @r + ' objectTypeEnum.Description  LIKE ''' + @ObjectTypeDescription + ''' AND '
  END

 IF @ObjectName IS NOT NULL
  IF @ObjectName = '?'
   SET @r = @r + 'COALESCE( dbo.GetDisplayName(e.ObjectId, e.ObjectType) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( dbo.GetDisplayName(e.ObjectId, e.ObjectType) , ''' + @ObjectName + ''') > 0 AND '
    ELSE
     SET @r = @r + ' dbo.GetDisplayName(e.ObjectId, e.ObjectType)  LIKE ''' + @ObjectName + ''' AND '
  END

 IF @ObjectUnitIdFrom IS NOT NULL AND @ObjectUnitIdTo IS NOT NULL
  SET @r = @r + '( e.ObjectUnitId  BETWEEN ' + CONVERT(nvarchar(20), @ObjectUnitIdFrom) + ' AND ' + CONVERT(nvarchar(20), @ObjectUnitIdTo) + ') AND '
 ELSE IF @ObjectUnitIdFrom IS NOT NULL AND @ObjectUnitIdTo IS NULL
  SET @r = @r + ' e.ObjectUnitId  = ' + CONVERT(nvarchar(20), @ObjectUnitIdFrom) + ' AND '
 ELSE IF @ObjectUnitIdFrom IS NULL AND @ObjectUnitIdTo IS NOT NULL
  SET @r = @r + ' e.ObjectUnitId  IS NULL AND '

 IF @ObjectUnitName IS NOT NULL
  IF @ObjectUnitName = '?'
   SET @r = @r + 'COALESCE( uo.Name , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( uo.Name , ''' + @ObjectUnitName + ''') > 0 AND '
    ELSE
     SET @r = @r + ' uo.Name  LIKE ''' + @ObjectUnitName + ''' AND '
  END

 IF @ObjectUnitLocation IS NOT NULL
  IF @ObjectUnitLocation = '?'
   SET @r = @r + 'COALESCE( dbo.GetUnitLocation(e.ObjectUnitId, DEFAULT, DEFAULT, DEFAULT) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( dbo.GetUnitLocation(e.ObjectUnitId, DEFAULT, DEFAULT, DEFAULT) , ''' + @ObjectUnitLocation + ''') > 0 AND '
    ELSE
     SET @r = @r + ' dbo.GetUnitLocation(e.ObjectUnitId, DEFAULT, DEFAULT, DEFAULT)  LIKE ''' + @ObjectUnitLocation + ''' AND '
  END

 IF @EndDateUtcFrom IS NOT NULL
  SET @r = @r + ' e.EndDateUtc  >=''' + CONVERT(nvarchar(30), @EndDateUtcFrom, 121) + ''' AND '
 IF @EndDateUtcTo IS NOT NULL
  SET @r = @r + ' e.EndDateUtc  <=''' + CONVERT(nvarchar(30), @EndDateUtcTo, 121) + ''' AND '

 IF @EndingEventIdFrom IS NOT NULL AND @EndingEventIdTo IS NOT NULL
  SET @r = @r + '( e.EndingEventId  BETWEEN ' + CONVERT(nvarchar(20), @EndingEventIdFrom) + ' AND ' + CONVERT(nvarchar(20), @EndingEventIdTo) + ') AND '
 ELSE IF @EndingEventIdFrom IS NOT NULL AND @EndingEventIdTo IS NULL
  SET @r = @r + ' e.EndingEventId  = ' + CONVERT(nvarchar(20), @EndingEventIdFrom) + ' AND '
 ELSE IF @EndingEventIdFrom IS NULL AND @EndingEventIdTo IS NOT NULL
  SET @r = @r + ' e.EndingEventId  IS NULL AND '

 IF @OperatorIdFrom IS NOT NULL AND @OperatorIdTo IS NOT NULL
  SET @r = @r + '( e.OperatorId  BETWEEN ' + CONVERT(nvarchar(20), @OperatorIdFrom) + ' AND ' + CONVERT(nvarchar(20), @OperatorIdTo) + ') AND '
 ELSE IF @OperatorIdFrom IS NOT NULL AND @OperatorIdTo IS NULL
  SET @r = @r + ' e.OperatorId  = ' + CONVERT(nvarchar(20), @OperatorIdFrom) + ' AND '
 ELSE IF @OperatorIdFrom IS NULL AND @OperatorIdTo IS NOT NULL
  SET @r = @r + ' e.OperatorId  IS NULL AND '

 IF @OperatorName IS NOT NULL
  IF @OperatorName = '?'
   SET @r = @r + 'COALESCE( usr.Name , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( usr.Name , ''' + @OperatorName + ''') > 0 AND '
    ELSE
     SET @r = @r + ' usr.Name  LIKE ''' + @OperatorName + ''' AND '
  END

 IF @OperatorLastName IS NOT NULL
  IF @OperatorLastName = '?'
   SET @r = @r + 'COALESCE( usr.LastName , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( usr.LastName , ''' + @OperatorLastName + ''') > 0 AND '
    ELSE
     SET @r = @r + ' usr.LastName  LIKE ''' + @OperatorLastName + ''' AND '
  END

 IF @EventData IS NOT NULL
  IF @EventData = '?'
   SET @r = @r + 'COALESCE( e.EventData , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( e.EventData , ''' + @EventData + ''') > 0 AND '
    ELSE
     SET @r = @r + ' e.EventData  LIKE ''' + @EventData + ''' AND '
  END

 IF @Reason IS NOT NULL
  IF @Reason = '?'
   SET @r = @r + 'COALESCE( e.Reason , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( e.Reason , ''' + @Reason + ''') > 0 AND '
    ELSE
     SET @r = @r + ' e.Reason  LIKE ''' + @Reason + ''' AND '
  END

 IF @RaisingEventIdFrom IS NOT NULL AND @RaisingEventIdTo IS NOT NULL
  SET @r = @r + '( a.RaisingEventId  BETWEEN ' + CONVERT(nvarchar(20), @RaisingEventIdFrom) + ' AND ' + CONVERT(nvarchar(20), @RaisingEventIdTo) + ') AND '
 ELSE IF @RaisingEventIdFrom IS NOT NULL AND @RaisingEventIdTo IS NULL
  SET @r = @r + ' a.RaisingEventId  = ' + CONVERT(nvarchar(20), @RaisingEventIdFrom) + ' AND '
 ELSE IF @RaisingEventIdFrom IS NULL AND @RaisingEventIdTo IS NOT NULL
  SET @r = @r + ' a.RaisingEventId  IS NULL AND '

 IF @IsClosedFrom IS NOT NULL AND @IsClosedTo IS NOT NULL
  SET @r = @r + '( a.IsClosed  BETWEEN ' + CONVERT(nvarchar(20), @IsClosedFrom) + ' AND ' + CONVERT(nvarchar(20), @IsClosedTo) + ') AND '
 ELSE IF @IsClosedFrom IS NOT NULL AND @IsClosedTo IS NULL
  SET @r = @r + ' a.IsClosed  = ' + CONVERT(nvarchar(20), @IsClosedFrom) + ' AND '
 ELSE IF @IsClosedFrom IS NULL AND @IsClosedTo IS NOT NULL
  SET @r = @r + ' a.IsClosed  IS NULL AND '

 IF @ClosingDateUtcFrom IS NOT NULL
  SET @r = @r + ' a.ClosingDateUtc  >=''' + CONVERT(nvarchar(30), @ClosingDateUtcFrom, 121) + ''' AND '
 IF @ClosingDateUtcTo IS NOT NULL
  SET @r = @r + ' a.ClosingDateUtc  <=''' + CONVERT(nvarchar(30), @ClosingDateUtcTo, 121) + ''' AND '

 IF @ClosingUserIdFrom IS NOT NULL AND @ClosingUserIdTo IS NOT NULL
  SET @r = @r + '( a.ClosingUserId  BETWEEN ' + CONVERT(nvarchar(20), @ClosingUserIdFrom) + ' AND ' + CONVERT(nvarchar(20), @ClosingUserIdTo) + ') AND '
 ELSE IF @ClosingUserIdFrom IS NOT NULL AND @ClosingUserIdTo IS NULL
  SET @r = @r + ' a.ClosingUserId  = ' + CONVERT(nvarchar(20), @ClosingUserIdFrom) + ' AND '
 ELSE IF @ClosingUserIdFrom IS NULL AND @ClosingUserIdTo IS NOT NULL
  SET @r = @r + ' a.ClosingUserId  IS NULL AND '

 IF @ClosingUserName IS NOT NULL
  IF @ClosingUserName = '?'
   SET @r = @r + 'COALESCE( ausr.Name , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( ausr.Name , ''' + @ClosingUserName + ''') > 0 AND '
    ELSE
     SET @r = @r + ' ausr.Name  LIKE ''' + @ClosingUserName + ''' AND '
  END

 IF @ClosingUserLastName IS NOT NULL
  IF @ClosingUserLastName = '?'
   SET @r = @r + 'COALESCE( ausr.LastName , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( ausr.LastName , ''' + @ClosingUserLastName + ''') > 0 AND '
    ELSE
     SET @r = @r + ' ausr.LastName  LIKE ''' + @ClosingUserLastName + ''' AND '
  END

 IF @ClosingAlarmIdFrom IS NOT NULL AND @ClosingAlarmIdTo IS NOT NULL
  SET @r = @r + '( a.ClosingAlarmId  BETWEEN ' + CONVERT(nvarchar(20), @ClosingAlarmIdFrom) + ' AND ' + CONVERT(nvarchar(20), @ClosingAlarmIdTo) + ') AND '
 ELSE IF @ClosingAlarmIdFrom IS NOT NULL AND @ClosingAlarmIdTo IS NULL
  SET @r = @r + ' a.ClosingAlarmId  = ' + CONVERT(nvarchar(20), @ClosingAlarmIdFrom) + ' AND '
 ELSE IF @ClosingAlarmIdFrom IS NULL AND @ClosingAlarmIdTo IS NOT NULL
  SET @r = @r + ' a.ClosingAlarmId  IS NULL AND '

 IF @ClosingComment IS NOT NULL
  IF @ClosingComment = '?'
   SET @r = @r + 'COALESCE( a.ClosingComment , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( a.ClosingComment , ''' + @ClosingComment + ''') > 0 AND '
    ELSE
     SET @r = @r + ' a.ClosingComment  LIKE ''' + @ClosingComment + ''' AND '
  END

 IF @UserMessageIdFrom IS NOT NULL AND @UserMessageIdTo IS NOT NULL
  SET @r = @r + '( ua.UserMessageId  BETWEEN ' + CONVERT(nvarchar(20), @UserMessageIdFrom) + ' AND ' + CONVERT(nvarchar(20), @UserMessageIdTo) + ') AND '
 ELSE IF @UserMessageIdFrom IS NOT NULL AND @UserMessageIdTo IS NULL
  SET @r = @r + ' ua.UserMessageId  = ' + CONVERT(nvarchar(20), @UserMessageIdFrom) + ' AND '
 ELSE IF @UserMessageIdFrom IS NULL AND @UserMessageIdTo IS NOT NULL
  SET @r = @r + ' ua.UserMessageId  IS NULL AND '

 IF @SenderIdFrom IS NOT NULL AND @SenderIdTo IS NOT NULL
  SET @r = @r + '( ua.UserId  BETWEEN ' + CONVERT(nvarchar(20), @SenderIdFrom) + ' AND ' + CONVERT(nvarchar(20), @SenderIdTo) + ') AND '
 ELSE IF @SenderIdFrom IS NOT NULL AND @SenderIdTo IS NULL
  SET @r = @r + ' ua.UserId  = ' + CONVERT(nvarchar(20), @SenderIdFrom) + ' AND '
 ELSE IF @SenderIdFrom IS NULL AND @SenderIdTo IS NOT NULL
  SET @r = @r + ' ua.UserId  IS NULL AND '

 IF @SenderName IS NOT NULL
  IF @SenderName = '?'
   SET @r = @r + 'COALESCE( ausr.Name , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( ausr.Name , ''' + @SenderName + ''') > 0 AND '
    ELSE
     SET @r = @r + ' ausr.Name  LIKE ''' + @SenderName + ''' AND '
  END

 IF @SenderLastName IS NOT NULL
  IF @SenderLastName = '?'
   SET @r = @r + 'COALESCE( ausr.LastName , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( ausr.LastName , ''' + @SenderLastName + ''') > 0 AND '
    ELSE
     SET @r = @r + ' ausr.LastName  LIKE ''' + @SenderLastName + ''' AND '
  END

 IF @Message IS NOT NULL
  IF @Message = '?'
   SET @r = @r + 'COALESCE( ua.Message , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( ua.Message , ''' + @Message + ''') > 0 AND '
    ELSE
     SET @r = @r + ' ua.Message  LIKE ''' + @Message + ''' AND '
  END

 SET @r = @r + ' 1=1'
 RETURN @r
END