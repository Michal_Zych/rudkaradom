﻿
CREATE FUNCTION IsUtcDateTimeInDay
(
	@DateTimeUtc dateTime
	,@DayStartHour time
	,@DayEndHour time
)
RETURNS bit
AS
BEGIN
	IF CONVERT(time, @DateTimeUtc) >= dbo.GetUtcHour(@dayStartHour) 
		AND CONVERT(time, @DateTimeUtc) < dbo.GetUtcHour(@dayEndHour) 
		RETURN 1
	
	RETURN 0
END