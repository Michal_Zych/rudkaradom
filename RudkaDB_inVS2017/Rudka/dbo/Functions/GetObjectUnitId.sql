﻿

CREATE FUNCTION [dbo].[GetObjectUnitId]
(
	@ObjectType dbo.OBJECT_TYPE
	,@ObjectId dbo.OBJECT_ID
)
RETURNS int
AS
BEGIN
	DECLARE @result int
	IF @ObjectType = (SELECT Patient FROM enum.ObjectType)
		SELECT @result = COALESCE(RoomId, WardId) FROM dbo.Patients WHERE Id = @ObjectId
	ELSE IF @ObjectType = (SELECT Band FROM enum.ObjectType)
		SELECT @result = UnitId FROM dbo.Bands WHERE Id = @ObjectId
	
	RETURN @result

END