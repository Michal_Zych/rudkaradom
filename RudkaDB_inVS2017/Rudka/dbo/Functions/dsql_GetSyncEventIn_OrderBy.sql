﻿CREATE FUNCTION dsql_GetSyncEventIn_OrderBy
(
 @SortOrder nvarchar(100)
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @result nvarchar(max)
 SET @result = ' ORDER BY ' +
  CASE @SortOrder
   WHEN '+eEventId' THEN 'e.Id  ASC'
   WHEN '-eEventId' THEN 'e.Id  DESC'
   WHEN '+eStoreDateUtc' THEN 'e.StoreDateUtc  ASC'
   WHEN '-eStoreDateUtc' THEN 'e.StoreDateUtc  DESC'
   WHEN '+eEventDateUtc' THEN 'e.EventDateUtc  ASC'
   WHEN '-eEventDateUtc' THEN 'e.EventDateUtc  DESC'
   WHEN '+eEventType' THEN 'e.EventType  ASC'
   WHEN '-eEventType' THEN 'e.EventType  DESC'
   WHEN '+eEventRecordId' THEN 'e.EventRecordId  ASC'
   WHEN '-eEventRecordId' THEN 'e.EventRecordId  DESC'
   WHEN '+    ,e.Recordas eRecord' THEN ' ASC'
   WHEN '-    ,e.Recordas eRecord' THEN ' DESC'
   WHEN '+eError' THEN 'e.Error  ASC'
   WHEN '-eError' THEN 'e.Error  DESC'
   ELSE 'e.Id '
  END
 RETURN @result + ' '
END