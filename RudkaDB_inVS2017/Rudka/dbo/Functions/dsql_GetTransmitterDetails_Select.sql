﻿CREATE FUNCTION [dbo].[dsql_GetTransmitterDetails_Select]
()
RETURNS nvarchar(max)
AS
BEGIN
 RETURN
'bs.Id AS Id
,bs.Mac AS Mac
,bs.MacString AS MacString
,bs.ClientId AS ClientId
,u.ID AS InstallationUnitId
--,u.Name AS InstallationUnitName --JZ
,dbo.GetUnitLocationReversed(u.ID, 1, default, default) as InstallationUnitName
,bs.Name AS Name
,bs.BarCode AS BarCode
,bs.TypeId AS TypeId
,bs.ReportIntervalSec AS ReportIntervalSec
,bs.Sensitivity AS Sensitivity
,bs.AvgCalcTimeS AS AvgCalcTimeS
,bs.Msisdn AS Msisdn
,bs.IsBandUpdater AS IsBandUpdater
,bs.[Description] AS [Description]
,bs.RssiTreshold AS RssiTreshold
,bs.[Group] AS [GroupId]
,tg.Description AS GroupName
,bs.IpAddress AS IpAddress
,CONVERT(bit, COALESCE(ts.OngConnectedEventId, 0)) AS IsConnected
,CONVERT(bit, COALESCE(ts.OngDisconnectedAlarmId,0)) AS AlarmOngoing
'
END