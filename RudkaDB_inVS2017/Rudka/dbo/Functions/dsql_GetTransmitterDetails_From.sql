﻿CREATE FUNCTION [dbo].[dsql_GetTransmitterDetails_From]
()
RETURNS nvarchar(max)
AS
BEGIN
 RETURN
 ' FROM dbo.dev_Transmitters bs
LEFT JOIN dev_UnitTransmitter AS ut on bs.Id = ut.TransmitterID
LEFT JOIN Units AS u on ut.UnitID = u.ID
LEFT JOIN dbo.dev_TransmitterGroups tg ON tg.[Group] = bs.[Group]
LEFT JOIN dbo.ev_TransmitterStatus ts ON ts.TransmitterId = bs.Id

 '
END