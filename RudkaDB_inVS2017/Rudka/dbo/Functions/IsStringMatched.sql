﻿
CREATE FUNCTION [dbo].[IsStringMatched]
(
	@Value nvarchar(max)
	,@Pattern nvarchar(max) 
)
RETURNS int
AS
BEGIN
	DECLARE @results int = 0
	
	SELECT @results = @results + CASE WHEN @Value LIKE Value THEN 1 ELSE 0 END
	FROM dbo.fn_Split(@Pattern, '|')

	RETURN @results
END