﻿CREATE FUNCTION dsql_GetArchiveData_Select
()
RETURNS nvarchar(max)
AS
BEGIN
 RETURN
'e.Id AS EventId
,patients.Id AS PatientId
,patients.Name AS Name
,patients.LastName AS LastName
,dbo.GetDisplayName(patients.Id, objectType.Patient) as DisplayName
,patients.Pesel AS Pesel
,e.DateUtc AS DateUtc
,e.Type AS [Type]
,typeEnum.Description AS TypeDescription
,e.Severity AS Severity
, severityEnum.Description AS SeverityDescription
, e.UnitId, u.Name AS UnitName
, dbo.GetUnitLocation(e.UnitId, DEFAULT, DEFAULT, DEFAULT) AS UnitLocation
, e.ObjectId AS ObjectId
, e.ObjectType AS ObjectType
, objectTypeEnum.Description AS ObjectTypeDescription
, dbo.GetDisplayName(e.ObjectId, e.ObjectType) AS ObjectName
, e.ObjectUnitId AS ObjectUnitId
, uo.Name AS ObjectUnitName
, dbo.GetUnitLocation(e.ObjectUnitId, DEFAULT, DEFAULT, DEFAULT) AS ObjectUnitLocation
, e.EndDateUtc AS EndDateUtc
, e.EndingEventId AS EndingEventId
, e.OperatorId AS OperatorId
, usr.Name AS OperatorName
, usr.LastName AS OperatorLastName
, e.EventData AS [EventData]
, e.Reason AS Reason
, a.RaisingEventId AS RaisingEventId
, a.IsClosed AS IsClosed
, a.ClosingDateUtc AS ClosingDateUtc
, a.ClosingUserId AS ClosingUserId
, ausr.Name AS ClosingUserName
, ausr.LastName AS ClosingUserLastName
, a.ClosingAlarmId AS ClosingAlarmId
, a.ClosingComment AS ClosingComment
, ua.UserMessageId AS UserMessageId
, ua.UserId AS SenderId
, ausr.Name AS SenderName
, ausr.LastName AS SenderLastName
, ua.Message AS [Message]
'
END