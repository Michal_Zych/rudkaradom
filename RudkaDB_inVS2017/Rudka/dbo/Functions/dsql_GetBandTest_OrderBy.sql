﻿CREATE FUNCTION dsql_GetBandTest_OrderBy
(
 @SortOrder nvarchar(100)
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @result nvarchar(max)
 SET @result = ' ORDER BY ' +
  CASE @SortOrder
   WHEN '+Id' THEN 'b.Id  ASC'
   WHEN '-Id' THEN 'b.Id  DESC'
   WHEN '+BarCode' THEN 'b.BarCode  ASC'
   WHEN '-BarCode' THEN 'b.BarCode  DESC'
   WHEN '+MacString' THEN 'b.MacString  ASC'
   WHEN '-MacString' THEN 'b.MacString  DESC'
   WHEN '+Uid' THEN 'b.Uid  ASC'
   WHEN '-Uid' THEN 'b.Uid  DESC'
   WHEN '+BatteryTypeStr' THEN 'bt.Name  ASC'
   WHEN '-BatteryTypeStr' THEN 'bt.Name  DESC'
   WHEN '+BatteryAlarmSeverityIds' THEN 'COALESCE(e.Severity, CASE WHEN bs.OngLowBatteryEventId IS NOT NULL THEN 1 ELSE NULL END)  ASC'
   WHEN '-BatteryAlarmSeverityIds' THEN 'COALESCE(e.Severity, CASE WHEN bs.OngLowBatteryEventId IS NOT NULL THEN 1 ELSE NULL END)  DESC'
   WHEN '+BatteryAlarmSeverity' THEN 'es.Description  ASC'
   WHEN '-BatteryAlarmSeverity' THEN 'es.Description  DESC'
   WHEN '+BatteryInstallationDate' THEN 'b.BatteryInstallationDateUtc  ASC'
   WHEN '-BatteryInstallationDate' THEN 'b.BatteryInstallationDateUtc  DESC'
   WHEN '+BatteryDaysToExchange' THEN 'DATEDIFF(DAY, dbo.GetCurrentUtcDate(), DATEADD(MONTH, bt.LifeMonths, b.BatteryInstallationDateUtc))  ASC'
   WHEN '-BatteryDaysToExchange' THEN 'DATEDIFF(DAY, dbo.GetCurrentUtcDate(), DATEADD(MONTH, bt.LifeMonths, b.BatteryInstallationDateUtc))  DESC'
   WHEN '+Description' THEN 'b.Description  ASC'
   WHEN '-Description' THEN 'b.Description  DESC'
   WHEN '+ObjectTypeIds' THEN 'bs.ObjectType  ASC'
   WHEN '-ObjectTypeIds' THEN 'bs.ObjectType  DESC'
   WHEN '+ObjectType' THEN 'ot.Description  ASC'
   WHEN '-ObjectType' THEN 'ot.Description  DESC'
   WHEN '+ObjectDisplayName' THEN 'dbo.GetDisplayName(bs.ObjectId, bs.ObjectType)  ASC'
   WHEN '-ObjectDisplayName' THEN 'dbo.GetDisplayName(bs.ObjectId, bs.ObjectType)  DESC'
   WHEN '+ObjectUnitIds' THEN 'ou.ID  ASC'
   WHEN '-ObjectUnitIds' THEN 'ou.ID  DESC'
   WHEN '+ObjectUnitName' THEN 'ou.Name  ASC'
   WHEN '-ObjectUnitName' THEN 'ou.Name  DESC'
   WHEN '+ObjectUnitLocation' THEN 'dbo.GetUnitLocation(ou.Id, default, default, default)  ASC'
   WHEN '-ObjectUnitLocation' THEN 'dbo.GetUnitLocation(ou.Id, default, default, default)  DESC'
   WHEN '+CurrentUnitIds' THEN 'cu.ID  ASC'
   WHEN '-CurrentUnitIds' THEN 'cu.ID  DESC'
   WHEN '+CurrentUnitName' THEN 'cu.Name  ASC'
   WHEN '-CurrentUnitName' THEN 'cu.Name  DESC'
   WHEN '+CurrentUnitLocation' THEN 'dbo.GetUnitLocation(cu.Id, default, default, default)  ASC'
   WHEN '-CurrentUnitLocation' THEN 'dbo.GetUnitLocation(cu.Id, default, default, default)  DESC'
   ELSE 'b.Id '
  END
 RETURN @result + ' '
END