﻿-- =============================================
-- Author:		pi
-- Create date: 09-10-2012
-- Description: Returns the roles lower in the hierarchy (and itself)
-- =============================================
CREATE FUNCTION [dbo].[Roles_GetDescendants] (@roleId INT)
RETURNS TABLE
AS
RETURN

WITH tmpRoles(Id, Parentroleid) As
(
	select Id, Parentroleid
	from [dbo].[Roles] as r
	where r.id = @roleId
	UNION ALL
	select r.Id, r.Parentroleid
	from [dbo].Roles as r 
	INNER JOIN tmpRoles rol on r.parentroleid = rol.Id
)
Select Id --,ParentRoleId
from tmpRoles

