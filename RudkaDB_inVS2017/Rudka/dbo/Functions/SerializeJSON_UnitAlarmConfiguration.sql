﻿
CREATE FUNCTION [dbo].[SerializeJSON_UnitAlarmConfiguration]
(
	@RecordId int
)
RETURNS nvarchar(max)
AS
BEGIN

	DECLARE @JSON nvarchar(max)

	SET @JSON = 'SELECT c.UnitId, u.Name, dbo.GetUnitLocation(c.UnitId, default, default, default) AS Location, IsSOScalling, IsACCactive
				, DMoveWrActive, DMoveWrMins, DMoveAlActive
				, DMoveAlMins, DNoMoveWrActive, DNoMoveWrMins, DNoMoveAlActive, DNoMoveAlMins, NMoveWrActive
				, NMoveWrMins, NMoveAlActive, NMoveAlMins, NNoMoveWrActive, NNoMoveWrMins, NNoMoveAlActive
				, NNoMoveAlMins, OutOfZoneWrActive, OutOfZoneWrMins, OutOfZoneAlActive, OutOfZoneAlMins, InNoGoZoneMins
				, NoConnectWrActive, NoConnectWrMins, NoConnectAlActive, NoConnectAlMins, NoTransmitterWrActive
				, NoTransmitterWrMins, NoTransmitterAlActive, NoTransmitterAlMins, MoveSettingsPriority, ''$Callings$'' AS Callings, ''$NoGoZones$'' AS NoGoZones
	FROM dbo.al_ConfigUnits c
	JOIN dbo.Units u ON c.UnitId = u.ID
	WHERE c.UnitId = ' + CONVERT(nvarchar(10), @RecordId)
	EXEC @JSON = dbo.SerializeJSON @JSON

	DECLARE @Callings nvarchar(max)
	SET @Callings = 'SELECT Phone, Description FROM al_CallingGroups , [enum].[ObjectType] WHERE ObjectType = [enum].[ObjectType].[Unit] AND Id =' + CONVERT(nvarchar(10), @RecordId)
	EXEC @Callings = dbo.SerializeJSON @Callings
	SET @JSON = REPLACE(@JSON, '"$Callings$"', @Callings)
	
	DECLARE @NoGoZones nvarchar(max)
	SET @NoGoZones = 'SELECT UnitId FROM al_NoGoZones , [enum].[ObjectType] WHERE ObjectType = [enum].[ObjectType].[Unit] AND ID =' + CONVERT(nvarchar(10), @RecordId)
	EXEC @NoGoZones = dbo.SerializeJSON @NoGoZones
	SET @JSON = REPLACE(@JSON, '"$NoGoZones$"', @NoGoZones)

	RETURN @JSON
END