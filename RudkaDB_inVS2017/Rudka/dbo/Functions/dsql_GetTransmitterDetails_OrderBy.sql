﻿CREATE FUNCTION dsql_GetTransmitterDetails_OrderBy
(
 @SortOrder nvarchar(100)
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @result nvarchar(max)
 SET @result = ' ORDER BY ' +
  CASE @SortOrder
   WHEN '+Id' THEN 'bs.Id  ASC'
   WHEN '-Id' THEN 'bs.Id  DESC'
   WHEN '+Mac' THEN 'bs.Mac  ASC'
   WHEN '-Mac' THEN 'bs.Mac  DESC'
   WHEN '+MacString' THEN 'bs.MacString  ASC'
   WHEN '-MacString' THEN 'bs.MacString  DESC'
   WHEN '+ClientId' THEN 'bs.ClientId  ASC'
   WHEN '-ClientId' THEN 'bs.ClientId  DESC'
   WHEN '+InstallationUnitId' THEN 'u.ID  ASC'
   WHEN '-InstallationUnitId' THEN 'u.ID  DESC'
   --WHEN '+InstallationUnitName' THEN 'u.Name  ASC' -- JZ
   --WHEN '-InstallationUnitName' THEN 'u.Name  DESC'
   WHEN '+InstallationUnitName' THEN 'dbo.GetUnitLocationReversed(u.ID, 1, default, default)  ASC'
   WHEN '-InstallationUnitName' THEN 'dbo.GetUnitLocationReversed(u.ID, 1, default, default)  DESC'
   WHEN '+Name' THEN 'bs.Name  ASC'
   WHEN '-Name' THEN 'bs.Name  DESC'
   WHEN '+BarCode' THEN 'bs.BarCode  ASC'
   WHEN '-BarCode' THEN 'bs.BarCode  DESC'
   WHEN '+TypeId' THEN 'bs.TypeId  ASC'
   WHEN '-TypeId' THEN 'bs.TypeId  DESC'
   WHEN '+ReportIntervalSec' THEN 'bs.ReportIntervalSec  ASC'
   WHEN '-ReportIntervalSec' THEN 'bs.ReportIntervalSec  DESC'
   WHEN '+Sensitivity' THEN 'bs.Sensitivity  ASC'
   WHEN '-Sensitivity' THEN 'bs.Sensitivity  DESC'
   WHEN '+AvgCalcTimeS' THEN 'bs.AvgCalcTimeS  ASC'
   WHEN '-AvgCalcTimeS' THEN 'bs.AvgCalcTimeS  DESC'
   WHEN '+Msisdn' THEN 'bs.Msisdn  ASC'
   WHEN '-Msisdn' THEN 'bs.Msisdn  DESC'
   WHEN '+IsBandUpdater' THEN 'bs.IsBandUpdater  ASC'
   WHEN '-IsBandUpdater' THEN 'bs.IsBandUpdater  DESC'
   WHEN '+Description' THEN 'bs.[Description]  ASC'
   WHEN '-Description' THEN 'bs.[Description]  DESC'
   WHEN '+RssiTreshold' THEN 'bs.RssiTreshold  ASC'
   WHEN '-RssiTreshold' THEN 'bs.RssiTreshold  DESC'
   WHEN '+GroupId' THEN 'bs.[Group]  ASC'
   WHEN '-GroupId' THEN 'bs.[Group]  DESC'
   WHEN '+GroupName' THEN 'tg.Description  ASC'
   WHEN '-GroupName' THEN 'tg.Description  DESC'
   WHEN '+IpAddress' THEN 'bs.IpAddress  ASC'
   WHEN '-IpAddress' THEN 'bs.IpAddress  DESC'
   WHEN '+IsConnected' THEN 'CONVERT(bit, COALESCE(ts.OngConnectedEventId, 0))  ASC'
   WHEN '-IsConnected' THEN 'CONVERT(bit, COALESCE(ts.OngConnectedEventId, 0))  DESC'
   WHEN '+AlarmOngoing' THEN 'CONVERT(bit, COALESCE(ts.OngDisconnectedAlarmId,0))  ASC'
   WHEN '-AlarmOngoing' THEN 'CONVERT(bit, COALESCE(ts.OngDisconnectedAlarmId,0))  DESC'
   ELSE 'bs.Id '
  END
 RETURN @result + ' '
END