﻿CREATE FUNCTION [dbo].[dsql_GetSystemEvent_Where]
(
@IdFrom int = null
,@IdTo int = null
,@DateUtcFrom datetime = null
,@DateUtcTo datetime = null
,@TypeFrom tinyint = null
,@TypeTo tinyint = null
,@TypeDescription nvarchar(200) = null
,@UnitIdFrom int = null
,@UnitIdTo int = null
,@UnitLocation nvarchar(200) = null
,@ObjectIdFrom int = null
,@ObjectIdTo int = null
,@ObjectTypeFrom tinyint = null
,@ObjectTypeTo tinyint = null
,@ObjectTypeDescription nvarchar(200) = null
,@ObjectName nvarchar(200) = null
,@OperatorIdFrom int = null
,@OperatorIdTo int = null
,@OperatorName nvarchar(200) = null
,@EventData nvarchar(200) = null
,@Reason nvarchar(200) = null
)
RETURNS nvarchar(max)
AS
BEGIN
 DECLARE @useMatchFunction bit = 1
 DECLARE @r nvarchar(max) = ' WHERE '

 IF @IdFrom IS NOT NULL AND @IdTo IS NOT NULL
  SET @r = @r + '(e.Id  BETWEEN ' + CONVERT(nvarchar(20), @IdFrom) + ' AND ' + CONVERT(nvarchar(20), @IdTo) + ') AND '
 ELSE IF @IdFrom IS NOT NULL AND @IdTo IS NULL
  SET @r = @r + 'e.Id  = ' + CONVERT(nvarchar(20), @IdFrom) + ' AND '
 ELSE IF @IdFrom IS NOT NULL AND @IdTo IS NOT NULL
  SET @r = @r + 'e.Id  IS NULL AND '

 IF @DateUtcFrom IS NOT NULL
  SET @r = @r + ' e.DateUtc  >=''' + CONVERT(nvarchar(30), @DateUtcFrom, 121) + ''' AND '
 IF @DateUtcTo IS NOT NULL
  SET @r = @r + ' e.DateUtc  <=''' + CONVERT(nvarchar(30), @DateUtcTo, 121) + ''' AND '

 IF @TypeFrom IS NOT NULL AND @TypeTo IS NOT NULL
  SET @r = @r + '( e.Type  BETWEEN ' + CONVERT(nvarchar(20), @TypeFrom) + ' AND ' + CONVERT(nvarchar(20), @TypeTo) + ') AND '
 ELSE IF @TypeFrom IS NOT NULL AND @TypeTo IS NULL
  SET @r = @r + ' e.Type  = ' + CONVERT(nvarchar(20), @TypeFrom) + ' AND '
 ELSE IF @TypeFrom IS NOT NULL AND @TypeTo IS NOT NULL
  SET @r = @r + ' e.Type  IS NULL AND '

 IF @TypeDescription IS NOT NULL
  IF @TypeDescription = '?'
   SET @r = @r + 'COALESCE( eventType.Description , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( eventType.Description , ''' + @TypeDescription + ''') > 0 AND '
    ELSE
     SET @r = @r + ' eventType.Description  LIKE ''' + @TypeDescription + ''' AND '
  END

 IF @UnitIdFrom IS NOT NULL AND @UnitIdTo IS NOT NULL
  SET @r = @r + '( e.UnitId  BETWEEN ' + CONVERT(nvarchar(20), @UnitIdFrom) + ' AND ' + CONVERT(nvarchar(20), @UnitIdTo) + ') AND '
 ELSE IF @UnitIdFrom IS NOT NULL AND @UnitIdTo IS NULL
  SET @r = @r + ' e.UnitId  = ' + CONVERT(nvarchar(20), @UnitIdFrom) + ' AND '
 ELSE IF @UnitIdFrom IS NOT NULL AND @UnitIdTo IS NOT NULL
  SET @r = @r + ' e.UnitId  IS NULL AND '

 IF @UnitLocation IS NOT NULL
  IF @UnitLocation = '?'
   SET @r = @r + 'COALESCE( dbo.GetUnitLocation(e.UnitId, 1, 1, default) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( dbo.GetUnitLocation(e.UnitId, 1, 1, default) , ''' + @UnitLocation + ''') > 0 AND '
    ELSE
     SET @r = @r + ' dbo.GetUnitLocation(e.UnitId, 1, 1, default)  LIKE ''' + @UnitLocation + ''' AND '
  END

 IF @ObjectIdFrom IS NOT NULL AND @ObjectIdTo IS NOT NULL
  SET @r = @r + '( e.ObjectId  BETWEEN ' + CONVERT(nvarchar(20), @ObjectIdFrom) + ' AND ' + CONVERT(nvarchar(20), @ObjectIdTo) + ') AND '
 ELSE IF @ObjectIdFrom IS NOT NULL AND @ObjectIdTo IS NULL
  SET @r = @r + ' e.ObjectId  = ' + CONVERT(nvarchar(20), @ObjectIdFrom) + ' AND '
 ELSE IF @ObjectIdFrom IS NOT NULL AND @ObjectIdTo IS NOT NULL
  SET @r = @r + ' e.ObjectId  IS NULL AND '

 IF @ObjectTypeFrom IS NOT NULL AND @ObjectTypeTo IS NOT NULL
  SET @r = @r + '( e.ObjectType  BETWEEN ' + CONVERT(nvarchar(20), @ObjectTypeFrom) + ' AND ' + CONVERT(nvarchar(20), @ObjectTypeTo) + ') AND '
 ELSE IF @ObjectTypeFrom IS NOT NULL AND @ObjectTypeTo IS NULL
  SET @r = @r + ' e.ObjectType  = ' + CONVERT(nvarchar(20), @ObjectTypeFrom) + ' AND '
 ELSE IF @ObjectTypeFrom IS NOT NULL AND @ObjectTypeTo IS NOT NULL
  SET @r = @r + ' e.ObjectType  IS NULL AND '

 IF @ObjectTypeDescription IS NOT NULL
  IF @ObjectTypeDescription = '?'
   SET @r = @r + 'COALESCE( objectType.Description , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( objectType.Description , ''' + @ObjectTypeDescription + ''') > 0 AND '
    ELSE
     SET @r = @r + ' objectType.Description  LIKE ''' + @ObjectTypeDescription + ''' AND '
  END

 IF @ObjectName IS NOT NULL
  IF @ObjectName = '?'
   SET @r = @r + 'COALESCE( dbo.GetObjectName(e.ObjectId, e.ObjectType) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( dbo.GetObjectName(e.ObjectId, e.ObjectType) , ''' + @ObjectName + ''') > 0 AND '
    ELSE
     SET @r = @r + ' dbo.GetObjectName(e.ObjectId, e.ObjectType)  LIKE ''' + @ObjectName + ''' AND '
  END

 IF @OperatorIdFrom IS NOT NULL AND @OperatorIdTo IS NOT NULL
  SET @r = @r + '( e.OperatorId  BETWEEN ' + CONVERT(nvarchar(20), @OperatorIdFrom) + ' AND ' + CONVERT(nvarchar(20), @OperatorIdTo) + ') AND '
 ELSE IF @OperatorIdFrom IS NOT NULL AND @OperatorIdTo IS NULL
  SET @r = @r + ' e.OperatorId  = ' + CONVERT(nvarchar(20), @OperatorIdFrom) + ' AND '
 ELSE IF @OperatorIdFrom IS NOT NULL AND @OperatorIdTo IS NOT NULL
  SET @r = @r + ' e.OperatorId  IS NULL AND '

 IF @OperatorName IS NOT NULL
  IF @OperatorName = '?'
   SET @r = @r + 'COALESCE( COALESCE(usr.LastName, '''''''') + COALESCE('''' '''' + usr.Name, '''''''') , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( COALESCE(usr.LastName, '''''''') + COALESCE('''' '''' + usr.Name, '''''''') , ''' + @OperatorName + ''') > 0 AND '
    ELSE
     SET @r = @r + ' COALESCE(usr.LastName, '''''''') + COALESCE('''' '''' + usr.Name, '''''''')  LIKE ''' + @OperatorName + ''' AND '
  END

 IF @EventData IS NOT NULL
  IF @EventData = '?'
   SET @r = @r + 'COALESCE( CONVERT(nvarchar(200), e.[EventData]) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( CONVERT(nvarchar(200), e.[EventData]) , ''' + @EventData + ''') > 0 AND '
    ELSE
     SET @r = @r + ' CONVERT(nvarchar(200), e.[EventData])  LIKE ''' + @EventData + ''' AND '
  END

 IF @Reason IS NOT NULL
  IF @Reason = '?'
   SET @r = @r + 'COALESCE( CONVERT(nvarchar(200), e.Reason) , '''') = '''' AND '
  ELSE
  BEGIN
    IF @useMatchFunction = 1 
      SET @r = @r + 'dbo.IsStringMatched( CONVERT(nvarchar(200), e.Reason) , ''' + @Reason + ''') > 0 AND '
    ELSE
     SET @r = @r + ' CONVERT(nvarchar(200), e.Reason)  LIKE ''' + @Reason + ''' AND '
  END

 SET @r = @r + ' 1=1'
 RETURN @r
END