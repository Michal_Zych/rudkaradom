﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

DECLARE @REAL_UNITS bit = 0



DECLARE @bandSubCode nvarchar(10) = 'ABC'
	,@htSubCode nvarchar(10) = 'HT0'
INSERT dev_SenseTypes (SubCode, Description) VALUES
 (@bandSubCode, 'Wristband RBEFO')
,(@htSubCode, 'Pill HT')

DECLARE @sensesForModel TABLE(
	SubCode nvarchar(10),
	SensorNr int,
	PhysicalUnit char,
	Scale int,
	Offset int,
	LoRange int,
	HiRange int,
	ShortestIntervalS int,
	Resolution int,
	IsEvent bit)
INSERT @sensesForModel VALUES
  (@bandSubCode, 1, 'R', 1, 0, -100, 0, 1, 1, 0)
, (@bandSubCode, 2, 'B', 1, 0, 0, 24, 1, 1, 0)
, (@bandSubCode, 3, 'E', 1, 0, 0, 1, 1, 1, 1)
, (@bandSubCode, 4, 'F', 1, 0, 0, 1, 1, 1, 1)
, (@bandSubCode, 5, 'O', 1, 0, 0, 1, 1, 1, 1)
, (@htSubCode, 1, 'R', 1, 0, -100, 0, 1, 1, 0)
, (@htSubCode, 2, 'B', 1, 0, 0, 4, 1, 1, 0)
, (@htSubCode, 3, 'H', 1, 0, 0, 100, 1, 1, 0)
, (@htSubCode, 4, 'T', 1, 0, -100, 120, 1, 1, 0)


INSERT dev_ModelSenses(TypeId, SensorNr, PhysicalUnit, Scale, Offset, LoRange, HiRange, ShortestIntervalS, Resolution, IsEvent)
SELECT st.Id, s.SensorNr, s.PhysicalUnit, s.Scale, s.Offset, s.LoRange, s.HiRange, s.ShortestIntervalS, s.Resolution, s.IsEvent
FROM @sensesForModel s
JOIN dev_SenseTypes st ON s.SubCode = st.SubCode







INSERT ev_SettingsHistory(StartDateUtc, UserId) VALUES('2017-01-01', 0)

INSERT BatteryTypes(Name, LifeMonths, Capacity, Voltage, Description) VALUES
	('Bateria mała', 12, 150, 3, 'mała bateria'),
	('Bateria duża', 24, 300, 3.4, 'duża bateria')

INSERT dbo.ev_SystemEventTypes values(0), (1), (2), (23), (24), (25), (255)

SET IDENTITY_INSERT dbo.Bands ON;
INSERT dbo.Bands(Id, Mac, Uid, BarCode, SerialNumber, TransmissionPower, AccPercent, SensitivityPercent, BroadcastingIntervalMs,BatteryTypeId, BatteryInstallationDateUtc)
SELECT 0, 0, null, null, '000', 4, 80, 100, 2500, 1, '2001-01-01'
SET IDENTITY_INSERT dbo.Bands OFF;


--domyślne ustawienia opaski
INSERT BandSettings(BandId, [Key], [Description], Value) VALUES
(0, 'Key1', 'klucz 1', 'value 1'),
(0, 'Key2', 'klucz 2', 'value 2'),
(0, 'Key3', 'klucz 3', 'value 3')




--słowniki enum
INSERT [dbo].[enum_EventSeverityDictionaryPl] VALUES
 (0, 'Zdarzenie')
,(1, 'Ostrzeżenie')
,(2, 'Alarm')

INSERT [dbo].[enum_EventTypeDictionaryPl] VALUES
(255,'Błąd bazy danych')
,(0,'Kasowanie')
,(1,'Tworzenie')
,(2,'Edycja')
,(3,'Przyjęcie do szpitala')
,(4,'Wypisanie ze szpitala')
,(5,'Przyjęcie na oddział')
,(6,'Wypisanie z oddziału')
,(7,'Przyjęcie na salę')
,(8,'Wypisanie z sali')
,(9,'Przypisanie opaski')
,(10,'Bateria OK')
,(11,'Połączono')
,(12,'Wejście do strefy')
,(13,'Wyjście ze strefy zakazanej')
,(14,'Alarmu żytkownika')
,(15,'Niski poziom baterii')
,(16,'Pacjent się porusza')
,(17,'Pacjent się nie porusza')
,(18,'Wyjście ze strefy')
,(19,'Alarm z akcelerometru')
,(20,'Rozłączenie')
,(21,'Wejście do strefyz akazanej')
,(22,'Alarm SOS')
,(23, 'Przypisanie transmitera')
,(24, 'Zalogowanie użytkownika')
,(25, 'Wylogowanie użytkownika')
,(26, 'Wymiana baterii')

INSERT [dbo].[enum_ObjectTypeDictionaryPl] VALUES
(0, 'Jednostka organizacyjna')
,(1, 'Użytkownik')
,(2, 'Opaska')
,(3, 'Transmiter')
,(4, 'Pacjent')
,(5, 'Alarmowanie w JO')
,(6, 'Alarmowanie dla Pacjenta')
,(7, 'Mapowanie JO')


INSERT UnitTypesV2 VALUES
('BŁĄD - typ nieużywany', 'Bug',0)
,('Pudełko', 'Box',0)
,('Budynek', 'Budynek',0)
,('Dział', 'Dzial',0)
,('Kuweta', 'Kuweta',0)
,('Lodówka', 'Lodowka',0)
,('LodówkaTemp', 'LodowkaTemp',0)
,('Mroźnia', 'Mroznia',0)
,('Oddział', 'Oddział',0)
,('Paleta', 'Paleta',0)
,('Piętro', 'Pietro',0)
,('Pojemnik TB', 'PojemnikTB',0)
,('Pojemnik TB Temp', 'PojemnikTBTemp',0)
,('Pokój', 'Pokoj',0)
,('Pokój Temp', 'PokojTemp',0)
,('Pokój THG', 'PokojTHG',0)
,('Półka', 'Polka',0)
,('Regał', 'Regal',0)
,('Szuflada', 'Szuflada',0)
,('Higrometr', 'Termohigrometr',1)
,('Termometr', 'Termometr',1)
,('Zamrażarka', 'Zamrazarka',0)
,('Zamrażarka', 'ZamrazarkaTemp',0)
,('Ambulans', 'Ambulance',0)
,('Archiwum', 'Archiwum',0)
,('Koszyk', 'Basket',0)
,('Zasilanie', 'Zasilanie',0)
,('Pstryczek', 'Pstryczek',1)
,('Przełącznik', 'Przełącznik',1)
,('Oddzial szpitalny', 'Dzial',0)

--domyślne alarmowanie dla unita
INSERT dbo.al_ConfigUnits 
SELECT 
--unitId
0, 
--SOS, ACC
1, 1, 
--Day
0, 10, 0, 20, 0, 10, 0, 20, 
--Night
0, 10, 0, 20, 0, 10, 0, 20, 
--OutOfZone
1, 10, 1, 20,
--noGoZoneMins
0, 10,
--noConnect
1, 10, 1, 20,
-- noTransmitter
1, 10, 1, 20,
--MoveSettingsPriority
0

exec [dbo].[unt_UnitCreate] 'Szpital Rudka (*)', null, 1, NULL, NULL, 0, NULL
EXEC unt_UnitCreate 'Pacjenci nieprzypisani (N)', 1, 5, NULL, NULL , 0, NULL
EXEC unt_UnitCreate 'Parter - część wschodnia/środek', 0
EXEC unt_UnitCreate 'Izba przyjęć (R)', 1, 4, NULL, NULL , 0, NULL


CREATE Table [dbo].[UnitTemp](
[UnitId][int] IDENTITY(1,1) NOT NULL,
[Name][varchar](25) NULL,
[Department][varchar](25) NULL,
[Level][varchar](25) NULL,
[Zone][varchar](25) NULL,
[TransmitterId][int] NOT NULL);




--Unity

IF @REAL_UNITS = 0
BEGIN 
	exec [dbo].[unt_UnitCreate] 'Szpital Dev (*)', null, 1, NULL, NULL, 0, NULL
	EXEC unt_UnitCreate 'Pacjenci nieprzypisani (N)', 1, 5, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Rejestracja (R)', 1, 4, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Pokój 1 (S)', 3, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Izba przyjęć (S)', 3, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Pokój pielęgniarek (S)', 3, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Park (O)', 1, 3, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Strefa 1 (S)', 7, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'strefa 2 (S)', 7, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Palarnia (S)', 7, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Budynek 1', 1, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Kardiologia (O)', 11, 3, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Piętro 1', 12, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Sala 1 (S)', 13, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Sala 2 (S)', 13, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Pokój pielęgniarek (S)', 13, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'WC (S)', 13, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Piętro 2', 12, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Sala 1 (S)', 18, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Sala 2 (S)', 18, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'WC (S)', 18, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Neurologia (O)', 11, 3, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Sala 1 (S)', 22, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Sala 2 (S)', 22, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Pokój pielęgniarek (S)', 22, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'WC (S)', 22, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Pediatria (O)', 1, 3, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'sala 1 (S)', 27, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Sala 2 (S)', 27, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'Pokój pielęgniarek (S)', 27, 6, NULL, NULL , 0, NULL
	EXEC unt_UnitCreate 'wc (S)', 27, 6, NULL, NULL , 0, NULL

	UPDATE Units SET UnitTypeIdv2 =
		CASE
				WHEN CHARINDEX('(O)', Name) > 0 THEN 29
				WHEN CHARINDEX('(R)', Name) > 0 THEN 29 
				ELSE 3
		END
	UPDATE Units SET UnitTypeID = 1 WHERE CHARINDEX('(*)', Name) > 1
	
	UPDATE cu SET IsNoGoZone  = 1
	FROM al_ConfigUnits cu
	JOIN Units u on cu.UnitId = u.id
	WHERE u.Name like '%pielęgniarek%'
 
	INSERT al_NoGoZones (Id, ObjectType, UnitId)
	SELECT u.ID, (SELECT Unit FROM enum.ObjectType), (SELECT Id from Units WHERE Name like '%Palarnia%')
	FROM units u
	JOIN units pu ON u.ParentUnitID = pu.ID
	WHERE u.Name like '%Pediatria%'
		OR pu.Name like '%Pediatria%'


	INSERT dev_Transmitters(ClientId, Name, ReportIntervalSec)
	SELECT 1, 'Transmiter - ' + CONVERT(nvarchar(10), Id) + ' ' + SUBSTRING(Name, 1, CHARINDEX('(S)', Name) - 1), 60
	FROM Units
	WHERE CHARINDEX('(S)', Name) > 0

	INSERT dev_UnitTransmitter(UnitId, TransmitterId)
	SELECT u.id, t.Id
	FROM dev_Transmitters t
	JOIN Units u on CHARINDEX(' - ' + CONVERT(nvarchar(10), u.Id) + ' ', t.Name) > 0

	UPDATE Clients SET 
	RootUnitID = (SELECT Id FROM Units WHERE Name Like '%(*)%')
	,RegistrationWardId = (SELECT ID FROM Units WHERE Name LIKE '%(R)%')
	,UnassignedPatientsUnitId = (SELECT ID FROM Units WHERE Name LIKE '%(N)%')


	update dev_Transmitters set Mac = 0x00126F6D3BD1 where id = 7
	update dev_Transmitters set Mac = 0x00126F6D3A41 where id = 8
END
ELSE -- Unity realne
BEGIN
	DECLARE @unity TABLE(Nr int identity(1,1), Oddzial nvarchar(200), Budynek nvarchar(200), Pietro nvarchar(200), Strefa nvarchar(200), NrTransmitera nvarchar(100))
	INSERT @unity VALUES
	('Izba przyjęć', 'Budynek główny', 'Parter - część wschodnia/środek', 'Izba przjęć', '2')
,('Izba przyjęć', 'Budynek główny', 'Parter - część wschodnia/środek', 'Gabinet lekarski hall', '3')
,('Izba przyjęć', 'Budynek główny', 'Parter - część wschodnia/środek', 'Hall część zachodnia', '4')
,('Fizjoterapia', 'Budynek główny', 'Parter - część wschodnia/środek', 'Hall część wschodnia', '5')
,('Fizjoterapia', 'Budynek główny', 'Parter - część wschodnia/środek', 'Hall klatka schodowa', '6')
,('Fizjoterapia', 'Budynek główny', 'Parter - część wschodnia', 'Korytarz rentgen', '7')
,('Fizjoterapia', 'Budynek główny', 'Parter - część wschodnia', 'Korytarz toaleta', '8')
,('Fizjoterapia', 'Budynek główny', 'Parter - część wschodnia', 'Korytarz fizjoterapia', '9')
,('Fizjoterapia', 'Budynek główny', 'Parter - część wschodnia', 'Korytarz apteka', '10')
,('Fizjoterapia', 'Budynek główny', 'Parter - część wschodnia', 'Wschodnia klatka schodowa', '11')
,('Fizjoterapia', 'Budynek główny', 'Parter - część wschodnia', 'Wyjście ewakuacyjne 1', '12')
,('Oddział rehabilitacyjny', 'Budynek główny', 'Parter - część zachodnia', 'Korytarz gabinet USG', '13')
,('Oddział rehabilitacyjny', 'Budynek główny', 'Parter - część zachodnia', 'Korytarz pracownia EKG', '14')
,('Oddział chemioterapii ', 'Budynek główny', 'Parter - część zachodnia', 'Korytarz pokój przygotowania', '15')
,('Poradnia chorób płuc', 'Budynek główny', 'Parter - część zachodnia', 'Pracownia spirometrii', '16')
,('Poradnia chorób płuc', 'Budynek główny', 'Parter - część zachodnia', 'Pracownia bronchoskopii przy klatce', '17')
,('Poradnia chorób płuc', 'Budynek główny', 'Parter - część zachodnia', 'Zachodnia klatka schodowa', '18')
,('Poradnia chorób płuc', 'Budynek główny', 'Parter - część zachodnia', 'Wyjście ewakuacyjne 2', '19')
,('Oddział rehabilitacyjny', 'Budynek główny', 'Piętro 1 - część północna', 'Klatka schodowa', '20')
,('Oddział rehabilitacyjny', 'Budynek główny', 'Piętro 1 - część północna', ' Sala gimnastyczna', '21')
,('Oddział rehabilitacyjny', 'Budynek główny', 'Piętro 1 - część północna', 'Wyjście ewakuacyjne 3', '22')
,('Oddział rehabilitacyjny wsch.', 'Budynek główny', 'Piętro 1 - część wschodnia', 'Klatka schodowa', '23')
,('Oddział rehabilitacyjny wsch.', 'Budynek główny', 'Piętro 1 - część wschodnia', 'Korytarz gabinety lekarskie Hall', '24')
,('Oddział rehabilitacyjny wsch.', 'Budynek główny', 'Piętro 1 - część wschodnia', 'Pokój 113', '25')
,('Oddział rehabilitacyjny wsch.', 'Budynek główny', 'Piętro 1 - część wschodnia', 'Pokój 112', '26')
,('Oddział rehabilitacyjny wsch.', 'Budynek główny', 'Piętro 1 - część wschodnia', 'Pokój 111', '27')
,('Oddział rehabilitacyjny wsch.', 'Budynek główny', 'Piętro 1 - część wschodnia', 'Pokój 110', '28')
,('Oddział rehabilitacyjny wsch.', 'Budynek główny', 'Piętro 1 - część wschodnia', 'Pokój 109', '29')
,('Oddział rehabilitacyjny wsch.', 'Budynek główny', 'Piętro 1 - część wschodnia', 'Łazienka', '30')
,('Oddział rehabilitacyjny wsch.', 'Budynek główny', 'Piętro 1 - część wschodnia', 'Pokój 106', '31')
,('Oddział rehabilitacyjny wsch.', 'Budynek główny', 'Piętro 1 - część wschodnia', 'Pokój 105', '32')
,('Oddział rehabilitacyjny wsch.', 'Budynek główny', 'Piętro 1 - część wschodnia', 'Pokój 104', '33')
,('Oddział rehabilitacyjny wsch.', 'Budynek główny', 'Piętro 1 - część wschodnia', 'Pokój 103', '34')
,('Oddział rehabilitacyjny wsch.', 'Budynek główny', 'Piętro 1 - część wschodnia', 'Pokój 102', '35')
,('Oddział rehabilitacyjny wsch.', 'Budynek główny', 'Piętro 1 - część wschodnia', 'Pokój 101', '36')
,('Oddział rehabilitacyjny wsch.', 'Budynek główny', 'Piętro 1 - część wschodnia', 'Korytrarz klatka schodowa', '37')
,('Oddział rehabilitacyjny wsch.', 'Budynek główny', 'Piętro 1 - część wschodnia', 'Sala ćwiczeń', '38')
,('Oddział rehabilitacyjny wsch.', 'Budynek główny', 'Piętro 1 - część wschodnia', 'Pokój chorych i separatka', '39')
,('Oddział rehabilitacyjny zach.', 'Budynek główny', 'Piętro 1 - część zachodnia', 'Pokój 119', '40')
,('Oddział rehabilitacyjny zach.', 'Budynek główny', 'Piętro 1 - część zachodnia', 'Pokój 120', '41')
,('Oddział rehabilitacyjny zach.', 'Budynek główny', 'Piętro 1 - część zachodnia', 'Pokój 123', '42')
,('Oddział rehabilitacyjny zach.', 'Budynek główny', 'Piętro 1 - część zachodnia', 'Pokój 124', '43')
,('Oddział rehabilitacyjny zach.', 'Budynek główny', 'Piętro 1 - część zachodnia', 'Łazienka', '44')
,('Oddział rehabilitacyjny zach.', 'Budynek główny', 'Piętro 1 - część zachodnia', 'Pokój 127', '45')
,('Oddział rehabilitacyjny zach.', 'Budynek główny', 'Piętro 1 - część zachodnia', 'Pokój 128', '46')
,('Oddział rehabilitacyjny zach.', 'Budynek główny', 'Piętro 1 - część zachodnia', 'Pokój 131', '47')
,('Oddział rehabilitacyjny zach.', 'Budynek główny', 'Piętro 1 - część zachodnia', 'Pokój 132', '48')
,('Oddział rehabilitacyjny zach.', 'Budynek główny', 'Piętro 1 - część zachodnia', 'Pokój 133', '49')
,('Oddział rehabilitacyjny zach.', 'Budynek główny', 'Piętro 1 - część zachodnia', 'Pokój 134', '50')
,('Oddział rehabilitacyjny zach.', 'Budynek główny', 'Piętro 1 - część zachodnia', 'Korytarz sala terapii', '51')
,('Oddział rehabilitacyjny zach.', 'Budynek główny', 'Piętro 1 - część zachodnia', 'Zachodnia klatka schodowa', '52')
,('Laboratorium ', 'Budynek główny', 'Piętro 2 - część północna', 'Korytarz punkt pobrań', '53')
,('Laboratorium ', 'Budynek główny', 'Piętro 2 - część północna', 'Korytarz pracownia hematologii', '54')
,('Oddział chorób płuc II', 'Budynek główny', 'Piętro 2 - część wschodnia', 'Klatka schodowa środkowa', '55')
,('Oddział chorób płuc II', 'Budynek główny', 'Piętro 2 - część wschodnia', 'Korytarz brudownik hall', '56')
,('', '', '', '', '57')
,('Oddział chorób płuc II', 'Budynek główny', 'Piętro 2 - część wschodnia', 'Pokój 213', '58')
,('Oddział chorób płuc II', 'Budynek główny', 'Piętro 2 - część wschodnia', 'Pokój 212', '59')
,('Oddział chorób płuc II', 'Budynek główny', 'Piętro 2 - część wschodnia', 'Pokój 211', '60')
,('Oddział chorób płuc II', 'Budynek główny', 'Piętro 2 - część wschodnia', 'Pokój 210', '61')
,('Oddział chorób płuc II', 'Budynek główny', 'Piętro 2 - część wschodnia', 'Pokój 209', '62')
,('Oddział chorób płuc II', 'Budynek główny', 'Piętro 2 - część wschodnia', 'Łazienka', '63')
,('Oddział chorób płuc II', 'Budynek główny', 'Piętro 2 - część wschodnia', 'Pokój 206', '64')
,('Oddział chorób płuc II', 'Budynek główny', 'Piętro 2 - część wschodnia', 'Pokój 205', '65')
,('Oddział chorób płuc II', 'Budynek główny', 'Piętro 2 - część wschodnia', 'Pokój 204', '66')
,('Oddział chorób płuc II', 'Budynek główny', 'Piętro 2 - część wschodnia', 'Pokój 203', '67')
,('Oddział chorób płuc II', 'Budynek główny', 'Piętro 2 - część wschodnia', 'Pokój 202', '68')
,('Oddział chorób płuc II', 'Budynek główny', 'Piętro 2 - część wschodnia', 'Korytarz klatka schodowa wschodnia', '69')
,('Oddział chorób płuc II', 'Budynek główny', 'Piętro 2 - część wschodnia', 'Pokój 200', '70')
,('Oddział chorób płuc II', 'Budynek główny', 'Piętro 2 - część wschodnia', 'Węzeł sanitarny', '71')
,('Oddział chorób płuc II', 'Budynek główny', 'Piętro 2 - część wschodnia', 'Separatka', '72')
,('Oddział chorób płuc II', 'Budynek główny', 'Piętro 2 - część wschodnia', 'Pokój i pokój inhalacji 198', '73')
,('Oddział chorób płuc I', 'Budynek główny', 'Piętro 2 - część zachodnia', 'Pokój 219', '74')
,('Oddział chorób płuc I', 'Budynek główny', 'Piętro 2 - część zachodnia', 'Pokój 220', '75')
,('Oddział chorób płuc I', 'Budynek główny', 'Piętro 2 - część zachodnia', 'Pokój 221', '76')
,('Oddział chorób płuc I', 'Budynek główny', 'Piętro 2 - część zachodnia', 'Pokój 222', '77')
,('Oddział chorób płuc I', 'Budynek główny', 'Piętro 2 - część zachodnia', 'Pokój 224', '78')
,('Oddział chorób płuc I', 'Budynek główny', 'Piętro 2 - część zachodnia', 'Pokój 225', '79')
,('Oddział chorób płuc I', 'Budynek główny', 'Piętro 2 - część zachodnia', 'Łazienka', '80')
,('Oddział chorób płuc I', 'Budynek główny', 'Piętro 2 - część zachodnia', 'Pokój 226', '81')
,('Oddział chorób płuc I', 'Budynek główny', 'Piętro 2 - część zachodnia', 'Pokój 227', '82')
,('Oddział chorób płuc I', 'Budynek główny', 'Piętro 2 - część zachodnia', 'Pokój 228', '83')
,('Oddział chorób płuc I', 'Budynek główny', 'Piętro 2 - część zachodnia', 'Pokój 229', '84')
,('Oddział chorób płuc I', 'Budynek główny', 'Piętro 2 - część zachodnia', 'Pokój 230', '85')
,('Oddział chorób płuc I', 'Budynek główny', 'Piętro 2 - część zachodnia', 'Pokój 231', '86')
,('Oddział chorób płuc I', 'Budynek główny', 'Piętro 2 - część zachodnia', 'Pokój 232', '87')
,('Oddział chorób płuc I', 'Budynek główny', 'Piętro 2 - część zachodnia', 'Pokój 233', '88')
,('Oddział chorób płuc I', 'Budynek główny', 'Piętro 2 - część zachodnia', 'Korytarz kaplica', '89')
,('Oddział chorób płuc I', 'Budynek główny', 'Piętro 2 - część zachodnia', 'Klatka schodowa zachodnia', '90')
,('Dyrekcja', 'Budynek główny', 'Poddasze - część administracyjna', 'Korytarz sala konferencyjna', '91')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Parter', 'Korytarz szatnia portiernia', '92')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Parter', 'Wyjście ewakuacyjne 1', '93')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Parter', 'Pokój 2-łóżkowy k. kuchni', '94')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Parter', 'Pokój 4-łóżkowy 1', '95')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Parter', 'Pokój 3-łóżkowy 2', '96')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Parter', 'Pokój 3-łóżkowy 3', '97')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Parter', 'Pokój 3-łóżkowy 4', '98')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Parter', 'Pokój 3-łóżkowy 5', '99')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Parter', 'Pokój 2-łóżkowy k. brudownika', '100')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Parter', 'Toaleta przy brudowniku', '101')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Parter', 'Wyjście ewakuacyjne 2', '102')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Piętro 1', 'Korytarz pokój dziennego pobytu', '103')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Piętro 1', 'Pokój 3-łóżkowy 11', '104')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Piętro 1', 'Pokój 3-łóżkowy 12', '105')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Piętro 1', 'Pokój 3-łóżkowy 13', '106')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Piętro 1', 'Pokój 3-łóżkowy 14', '107')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Piętro 1', 'Pokój 3-łóżkowy 15', '108')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Piętro 1', 'Pokój 3-łóżkowy 16', '109')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Piętro 1', 'Pokój 3-łóżkowy 17', '110')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Piętro 1', 'Pokój 4-łóżkowy 18', '111')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Piętro 1', 'Toaleta przy brudowniku', '112')
,('', '', '', '', '')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Poddasze', 'Korytarz klatka schodowa', '113')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Poddasze', 'Toaleta przy pokoju 2-łóżkowym', '114')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Poddasze', 'Korytarz kaplica', '115')
,('Zakład opiekuńczo-leczniczy', 'Budynek leczniczy', 'Poddasze', 'Korytarz gabinet psychologa', '116')
,('Zewnętrzny', '', '', 'Zewnętrzny lecznictwo', '117')
,('Zewnętrzny', '', '', 'Wejście główne', '118')
,('Zewnętrzny', '', '', 'Słup 1', 'GSM1')
,('Zewnętrzny', '', '', 'Słup 3', 'GSM2')
,('Zewnętrzny', '', '', 'Słup 5', 'GSM3')
,('Zewnętrzny', '', '', 'Słup 7', 'GSM4')
,('Zewnętrzny', '', '', 'Słup 12A', 'GSM5')
,('Zewnętrzny', '', '', 'Słup 23', 'GSM6')
,('Zewnętrzny', '', '', 'Słup 25', 'GSM7')
,('Zewnętrzny', '', '', 'Słup 13', 'GSM8')
,('Zewnętrzny', '', '', 'Słup 19', 'GSM9')
,('Zewnętrzny', '', '', 'Słup 29', 'GSM10')
,('Zewnętrzny', '', '', 'Słup 34', 'GSM11')
,('Zewnętrzny', '', '', 'Słup 31', 'GSM12')
,('Zewnętrzny', '', '', 'Słup 37', 'GSM13')
,('Zewnętrzny', '', '', 'Słup 40', 'GSM14')
,('Zewnętrzny', '', '', 'Słup 42', 'GSM15')
	
	TRUNCATE TABLE Units
	TRUNCATE TABLE dev_Transmitters
	TRUNCATE TABLE dev_UnitTransmitter
	
	
	DECLARE @root nvarchar(100) = 'Szpital'
		,@unassignedPatients nvarchar(100) = 'Pacjenci nieprzypisani'
		,@registration nvarchar(100) = 'Izba przyjęć'
	
	DECLARE
		@rootId int
		exec [dbo].[unt_UnitCreate] @root, null, 1, NULL, NULL, 0, NULL
		SELECT @rootId = Id FROM Units where Name = @root
		
		UPDATE Units SET UnitTypeID = 1 WHERE Id = @rootId
		exec dbo.unt_UnitCreate @unassignedPatients, @rootId, 5, NULL, NULL, 0, NULL
	
	DECLARE
			@nr int
			,@oddzial nvarchar(200)
			,@oddzialId int
			,@budynek nvarchar(200)
			,@budynekId int
			,@pietro nvarchar(200)
			,@pietroId int
			,@strefa nvarchar(200)
			,@strefaId int
			,@bcTransmitera nvarchar(100)
			,@parentId int
			,@transmitterId int
	
	DECLARE @results TABLE(Result int, ErrorCode int, ErrorMessage nvarchar(max))

	SELECT TOP 1 @nr = Nr, @oddzial = Oddzial, @budynek = Budynek, @pietro = Pietro, @strefa = Strefa, @bcTransmitera = NrTransmitera FROM @unity ORDER BY Nr
	WHILE @nr IS NOT NULL
	BEGIN
		SELECT @budynek = null
		SELECT @pietro = null
		SELECT @parentId = @rootId
		IF COALESCE(@oddzial, '') <> ''
		BEGIN
			SET @oddzialId = null
			SELECT @oddzialId = ID FROM Units WHERE Name = @oddzial AND ParentUnitID = @parentId
			IF @oddzialId IS NULL 
			BEGIN
				INSERT @results
				EXEC [dbo].[unt_UnitCreate] @oddzial, @parentId, 29, NULL, NULL, 0, NULL
				SELECT @oddzialId = MAX(Id) FROM Units
			END
			SELECT @parentId = @oddzialId
		
	
			IF COALESCE(@budynek, '') <> ''
			BEGIN
				SET @budynekId = null
				SELECT @budynekId = ID FROM Units WHERE Name = @budynek AND ParentUnitId = @parentId
				IF @budynekId IS NULL
				BEGIN
					INSERT @results
					EXEC dbo.unt_UnitCreate @budynek, @parentId, 6, NULL, NULL, 0, NULL
					SELECT @budynekId = MAX(Id) FROM Units
				END
				SELECT @parentId = @budynekId
			END
	
			IF COALESCE(@pietro, '') <> ''
			BEGIN
				SET @pietroId = null
				SELECT @pietroId = ID FROM Units WHERE Name = @pietro AND ParentUnitId = @parentId
				IF @pietroId IS NULL
				BEGIN
				INSERT @results
					EXEC dbo.unt_UnitCreate @pietro, @parentId, 6, NULL, NULL, 0, NULL
					SELECT @pietroId = MAX(Id) FROM Units
				END
				SELECT @parentId = @pietroId
			END
	
			INSERT @results
			EXEC dbo.unt_UnitCreate @strefa, @parentId, 6, NULL, NULL, 0, NULL
			SELECT @strefaId = MAX(Id) FROM Units
	
	
			INSERT dev_Transmitters(ClientId, Name, ReportIntervalSec, BarCode) VALUES
					(1, 'Transmiter - ' + @strefa, 60, CONVERT(nvarchar(10), @bcTransmitera))
			SELECT @transmitterId = SCOPE_IDENTITY()
			INSERT dev_UnitTransmitter(UnitId, TransmitterId) VALUES(@strefaId, @transmitterId)
		END

		DELETE @unity WHERE Nr = @nr
		SELECT @nr = NULL
		SELECT TOP 1 @nr = Nr, @oddzial = Oddzial, @budynek = Budynek, @pietro = Pietro, @strefa = Strefa, @bcTransmitera = NrTransmitera FROM @unity ORDER BY Nr
	END
	
	UPDATE Clients SET 
		RootUnitID = (SELECT Id FROM Units WHERE Name = @root)
		,RegistrationWardId = (SELECT ID FROM Units WHERE Name = @registration)
		,UnassignedPatientsUnitId = (SELECT ID FROM Units WHERE Name = @unassignedPatients)
	
	
	DECLARE @TransmitterMac TABLE(Mac bigint, UnitName nvarchar(100), ParentName nvarchar(100))
	INSERT @TransmitterMac VALUES
	 (0x00126F6D3BD1, '', '')
	,(0x00126F6D3A41, '', '')
	--transmitery realne
END



-- opaski
DECLARE @opaski TABLE(Nr int, unitId int)   
INSERT @opaski VALUES(1, 4)-- 4 przypisanie do rejestracji pokój 1
, (2, 4)
, (3, 4) 
DECLARE @opaskaId int, @opaskaUnitId int, @opaskaSerial nvarchar(10), @opaskaBarCode nvarchar(10)
SELECT TOP 1 @opaskaId  = Nr, @opaskaUnitId = unitId FROM @opaski ORDER BY Nr
WHILE @opaskaId IS NOT NULL
BEGIN
	SELECT @opaskaSerial = RIGHT('000' + CONVERT(nvarchar(10), @opaskaId), 3), @opaskaBarCode = CONVERT(nvarchar(10), @opaskaId)
	EXEC dev_BandConfigurationSet null, @opaskaId, @opaskaId, @opaskaBarCode, @opaskaSerial,
		4, 80, 100, 2500, 1, '2017-12-01',  
		null, null,
		@opaskaUnitId, 0, 'start systemu'

	DELETE FROM @opaski WHERE Nr = @opaskaId
	SELECT @opaskaId = null
	SELECT TOP 1 @opaskaId  = Nr, @opaskaUnitId = unitId FROM @opaski ORDER BY Nr
END

-- pacjenci
DECLARE @pacjenci TABLE(Name nvarchar(100), LastName nvarchar(100), WardId int, RoomId int, BandId int)
INSERT @pacjenci(Name, LastName, WardId, RoomId, BandId) VALUES
	('Jan', 'Kowalski', 3, 4, 1)
	,('Anna', 'Nowak', 12, 13, 2)
DECLARE @pacjentName nvarchar(100), @pacjentLastName nvarchar(100), @pacjentWardId int, @pacjentRoomId int, @pacjentBandId int
SELECT TOP 1 @pacjentName = Name, @pacjentLastName = LastName, @pacjentWardId = WardId, @pacjentRoomId = RoomId, @pacjentBandId = BandId FROM @pacjenci
WHILE @pacjentName IS NOT NULL
BEGIN
	EXEC ev_PatientToHospital @pacjentName, @pacjentLastName, null, @pacjentWardId, @pacjentRoomId, @pacjentBandId, null, 0

	DELETE FROM @pacjenci WHERE Name = @pacjentName AND LastName = @pacjentLastName
	SELECT @pacjentName = null
	SELECT TOP 1 @pacjentName = Name, @pacjentLastName = LastName, @pacjentWardId = WardId, @pacjentRoomId = RoomId, @pacjentBandId = BandId FROM @pacjenci
END



-- raporty
INSERT [dbo].[Reports] ([ID], [Name], [Description], [Rdl], [Type], [Picture], [Title]) VALUES (1, N'Raport pomiarowy', N'Szczegółowe wyniki pomiarów w postaci tabelarycznej', N'RaportPomiarowy_v3', 0, N'raport0.png', N'Raport monitorowania od <DateFrom> do <DateTo>')
GO
INSERT [dbo].[Reports] ([ID], [Name], [Description], [Rdl], [Type], [Picture], [Title]) VALUES (2, N'Raport pomiaru chwilowego', N'Raport przedstawia wyniki pomiarów jakie odnotowano o zadanej porze', N'RaportAktualnychPomiarow_v3', 0, N'raport0.png', N'Raport wartości chwilowych z <DateFrom>')
GO
INSERT [dbo].[Reports] ([ID], [Name], [Description], [Rdl], [Type], [Picture], [Title]) VALUES (3, N'Raport alarmowy', N'Raport przedstawia listę wszystkich zdarzeń alarmowych wygenerowanych przez urządzenia wraz z informacją o obsłudze', N'RaportAlarmowy_v3', 0, N'raport0.png', N'Zestawienie alarmów od <DateFrom> do <DateTo>')
GO
INSERT [dbo].[Reports] ([ID], [Name], [Description], [Rdl], [Type], [Picture], [Title]) VALUES (4, N'Raport pomiarowy 1H', N'Raport przedstawiający zestawienie z monitorowania urządzeń', N'RaportPomiarowy1H_v3', 0, N'raport0.png', N'Raport pomiarowy jedno-godzinowy od <DateFrom> do <DateTo>')
GO
INSERT [dbo].[Reports] ([ID], [Name], [Description], [Rdl], [Type], [Picture], [Title]) VALUES (5, N'Zdarzenia pacjenta', N'Szczegółowa lista zdarzeń pacjenta', N'RaportRudkaZdarzeniaPacjenta', 2, N'raport0.png', N'Zdarzenia pacjenta <Patient>\nw okresie od <DateFrom> do <DateTo>')
GO
INSERT [dbo].[Reports] ([ID], [Name], [Description], [Rdl], [Type], [Picture], [Title]) VALUES (6, N'Historia pobytu pacjenta', N'Historia pobytu pacjenta w szpitalu', N'RaportRudkaHistoriaPobytu', 3, N'raport0.png', N'Historia pobytu pacjenta <Patient>\nw okresie od <DateFrom> do <DateTo>')
GO
INSERT [dbo].[Reports] ([ID], [Name], [Description], [Rdl], [Type], [Picture], [Title]) VALUES (7, N'Zdarzenia w jednostce organizacyjnej', N'Raport zdarzeń, jakie zaszły w jednostce organizacyjnej', N'RaportRudkaZdarzeniaJednostki', 1, N'raport0.png', N'Zdarzenia w <UnitName>\nw okresie od <DateFrom> do <DateTo>')
GO



/*
exec dev_BandConfigurationSet	null, 1, 1, '1', '001',
	4, 80, 100, 2500, 1, '2017-12-01',  
	null, null,
	2 --przypisanie do rejestracji,  pokój 1
	,0, 'start systemu'
*/


--update bands set barcode = '2586232836' where id = 1
--update bands set mac = 0xFC6017FB6FB0 where id = 1


INSERT tech.sync_Configuration([Key], UserName, Password, ConnectionString, TopicName, EscapeValue, EscapeValueLine, CheckCmdsIntervalMilis, 
		NewQueryLimit, WebServiceUserName , WebServicePassword, WebServiceURL, ActiveMQConnectionId, KeepAliveIntervalS) VALUES (
		'DEFAULT'
		,'M2M'
		,'Int3Grm@Mt34m'
		,'activemq:tcp://192.168.3.89:61616'
		,'pl.asseco.poz.integration.events'
		,'&quot;'
		,'&newline;'
		,'10000'
		,'10'
		,'M2M'
		,'INT3GRM@MT34M'
		,'http://192.168.3.8:8500/platform/ws/'
		,'PLE222'
		,'50')





INSERT [$(RudkaTech)].dbo.amms_Units(UnitCode, UnitName, UnitType, UnitSubType) VALUES
('SZPIT', 'Mazowiecki Szpital im. dr. Teodora Dunina w Rudce spółka z ograniczoną odpowiedzialnością', 'SZP', 'XXX')
,('RPK', 'REJESTRACJA - PORADNIA KARDIOLOGICZNA', 'REJ', 'XXX')
,('RPRTG', 'REJESTRACJA - PRACOWNI RTG', 'REJ', 'PRA')
,('LB', 'PRACOWNIA BRONCHOSKOPII', 'GAB', 'DIA')
,('LU', 'PRACOWNIA USG', 'GAB', 'DIA')
,('LEEKG', 'PRACOWNIA EKG', 'GAB', 'DIA')
,('LWR', 'PRACOWNIA PRÓB WYSIŁKOWYCH', 'GAB', 'DIA')
,('LPR', 'PORADNIA REHABILITACYJNA', 'GAB', 'XXX')
,('21', 'PRACOWNIA FIZYKOTERAPII-WYŁ', 'GAB', 'DIA')
,('22', 'PRACOWNIA KINEZYTERAPII', 'GAB', 'DIA')
,('23', 'PRACOWNIA HYDROTERAPII', 'GAB', 'DIA')
,('LRM', 'PRACOWNIA MASAŻU LECZNICZEGO', 'GAB', 'DIA')
,('25', 'OŚRODEK REHABILITACJI DZIENNEJ -wyłączony', 'GAB', 'XXX')
,('LD', 'ODDZIAŁ DZIENNY CHEMIOTERAPII', 'ODD', 'XXX')
,('27', 'ELEKTROWNIA', 'XXX', 'XXX')
,('28', 'HYDROFORNIA', 'XXX', 'XXX')
,('29', 'KOTŁOWNIA', 'XXX', 'XXX')
,('AGU', 'PRALNIA', 'XXX', 'XXX')
,('AGK', 'KUCHNIA', 'XXX', 'XXX')
,('AGP', 'PORTIERNIA', 'XXX', 'XXX')
,('33', 'ADMINISTRACJA', 'XXX', 'XXX')
,('34', 'WARSZTATY', 'XXX', 'XXX')
,('35', 'OCZYSZCZALNIA', 'XXX', 'XXX')
,('36', 'GARAŻE', 'XXX', 'XXX')
,('37', 'STOŁÓWKI', 'XXX', 'XXX')
,('38', 'KAPLICA', 'XXX', 'XXX')
,('39', 'SZATNIE', 'XXX', 'XXX')
,('40', 'PUNKT GROMADZENIA ODPADÓW', 'XXX', 'XXX')
,('41', 'TEREN', 'XXX', 'XXX')
,('42', 'MIESZKANIA', 'XXX', 'XXX')
,('STATME', 'STATYSTYKA MEDYCZNA', 'ADM', 'STA')
,('44', 'DEZYFNEKCJA I UZDATNIANIE WODY', 'XXX', 'XXX')
,('LRPU', 'PODODDZIAŁ REHABILITACJI PULMONOLOGICZNEJ', 'ODD', 'XXX')
,('LDT', 'PORADNIA DOMOWEGO LECZENIA TLENEM', 'GAB', 'XXX')
,('55', 'PACJENCI PLANOWI ODDZIAŁ CHORÓB PŁUC ODC I', 'ODD', 'XXX')
,('LEHOL', 'PRACOWNIA EKG I HOLTERA', 'GAB', 'DIA')
,('LC', 'PRACOWNIA CZYNNOŚCIOWA PŁUC', 'GAB', 'DIA')
,('45', 'ZAKŁAD PIELĘGNACYJNO - OPIEKUŃCZY', 'XXX', 'XXX')
,('46', 'DYŻURKA LEKARSKA', 'XXX', 'XXX')
,('LRF', 'ZAKŁAD FIZJOTERAPII', 'GAB', 'REH')
,('LLG', 'ZAKŁAD DIAGNOSTYKI LABORATORYJNEJ', 'LAB', 'XXX')
,('LPK', 'PORADNIA KARDIOLOGICZNA', 'GAB', 'XXX')
,('LRNE.', 'PODODDZIAŁ REHABILITACJI NEUROLOGICZNEJ', 'ODD', 'XXX')
,('58', 'PRACOWNIA AUTOPSJI', 'XXX', 'XXX')
,('47', 'PRACOWNIA BEZDECHU SENNEGO', 'GAB', 'DIA')
,('REJ', 'REJESTRACJA', 'REJ', 'XXX')
,('1', 'LABORATORIUM ZEWNĘTRZNE', 'ZEW', 'LAB')
,('LPOII', 'ODDZIAŁ CHORÓB PŁUC-ODC.II', 'ODD', 'XXX')
,('LRRE', 'ODDZIAŁ REHABILITACYJNY', 'ODD', 'XXX')
,('LI', 'IZBA PRZYJĘĆ SZPITALA', 'IZP', 'XXX')
,('LPOI', 'ODDZIAŁ CHORÓB PŁUC-ODC.I', 'ODD', 'XXX')
,('LA', 'APTEKA SZPITALNA', 'APT', 'XXX')
,('9', 'ZESPÓŁ DOMOWEGO LECZENIA TLENEM', 'GAB', 'XXX')
,('LNP', 'PROSEKTORIUM', 'ODD', 'XXX')
,('LOL', 'ZAKŁAD OPIEKUŃCZO-LECZNICZY', 'ODD', 'XXX')
,('LPG', 'PORADNIA GRUŹLICY I CHORÓB PŁUC', 'GAB', 'XXX')
,('LLO', 'ZAKŁAD DIAGNOSTYKI LABORATORYJNEJ', 'LAB', 'XXX')
,('LRTG', 'PRACOWNIA RTG', 'GAB', 'DIA')
,('LRD', 'OŚRODEK REHABILITACJI DZIENNEJ', 'ODD', 'XXX')
,('RPDLT', 'REJESTRACJA - PORADNIA DOMOWEGO LECZENIA TLENEM', 'REJ', 'XXX')
,('RPR', 'REJESTRACJA - PORADNIA REHABILITACYJNA', 'REJ', 'XXX')
,('RPGICP', 'REJESTRACJA - PORADNIA GRUŹLICY I CHORÓB PŁUC', 'REJ', 'XXX')
,('RZDLT', 'REJESTRACJA - ZESPÓŁ DOMOWEGO LECZENIA TLENEM', 'REJ', 'XXX')
,('RPUSG', 'REJESTRACJA - PRACOWNI USG', 'REJ', 'PRA')
,('TEST', 'test', 'GAB', 'REH')
,('POZ', 'PORADNIA LEKARZA POZ', 'GAB', 'XXX')
,('RPPW', 'REJESTRACJA - PRACOWNI PRÓB WYSIŁKOWYCH', 'REJ', 'PRA')
,('OSKOM', 'OBSŁUGA SPRZEDAŻY', 'OSU', 'XXX')
,('LZOD', 'PRACOWNIA ZABURZEŃ ODDYCHANIA', 'GAB', 'DIA')
,('PPPOZ', 'PUNKT POBRAŃ POZ', 'PPO', 'XXX')
,('PPCHPG', 'PUNKT POBRAŃ PORADNI CHORÓB PŁUC I GRUŹLICY', 'PPO', 'XXX')
,('PPDLT', 'PUNKT POBRAŃ PORADNI DOMOWEGO LECZENIA TLENEM', 'PPO', 'XXX')
,('RPB', 'REJESTRACJA - PRACOWNIA BRONCHOSKOPII', 'REJ', 'PRA')
,('PPPB', 'PUNKT POBRAŃ PRACOWNI BRONCHOSKOPII', 'PPO', 'XXX')
,('LAB', 'LABORATORIUM Infomedica', 'LAB', 'XXX')
,('PPCHPI', 'PUNKT POBRAŃ ODDZIAŁU CHORÓB PŁUC-ODC.I', 'PPO', 'XXX')
,('PPCHP2', 'PUNKT POBRAŃ ODDZIAŁU CHORÓB PŁUC-ODC.II', 'PPO', 'XXX')
,('PPIP', 'PUNKT POBRAŃ IZBY PRZYJĘĆ', 'PPO', 'XXX')
,('PPDCH', 'PUNKT POBRAŃ ODDZIAŁU DZIENNEGO CHEMIOTERAPII', 'PPO', 'XXX')
,('PPOR', 'PUNKT POBRAŃ ODDZIAŁU REHABILITACYJNEGO', 'PPO', 'XXX')
,('PPZOL', 'PUNKT POBRAŃ ZAKŁADU OPEKUŃCZO-LECZNICZEGO', 'PPO', 'XXX')
,('PPPOR', 'PUNKT POBRAŃ PORADNI', 'PPO', 'XXX')
,('RPCZP', 'REJESTRACJA - PRACOWNI CZYNNOŚCIOWEJ PŁUC', 'REJ', 'PRA')
,('RPEIH', 'REJESTRACJA - PRACOWNI EKG I HOLTERA', 'REJ', 'PRA')
,('E-REJ', 'E-REJESTRACJA', 'REJ', 'XXX')

INSERT tech.sync_EventsIdentifiers(EventType, IdKey, RecIds, RecQueryKey) VALUES
 ('PATIENT_MODIFIED', 'MODIFIED_PATIENT_ID','PADM;PALC','PATIENT_ID')
,('PATIENT_REGISTERED', 'REGISTERED_PATIENT_ID', 'PADM;PALC', 'PATIENT_ID')
,('String PATIENT_VISIT_START', 'VISIT_ID',	'VISI', 'VISIT_ID')
,('VISIT_END', 'VISIT_ID', 'VISI', 'VISIT_ID')

INSERT tech.sync_RecordsIdentifiers(RecType, IdKey) VALUES
 ('PADM', 'TECHNICAL_IDENTIFIER')
,('PALC', 'TECHNICAL_IDENTIFIER')

--INSERT tech.sync_SupportedEventTypes(EventType) VALUES ()